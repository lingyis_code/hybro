
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 10 "vlgParser.y"

#include "vlg2sExpr.h"

  void yyerror(char *str);
  void YYTRACE(char *str);

  extern char brep[];
  extern int bexp0, bexp1;

  char current_range[MAXSTRLEN];
  int cur_range_start;
  int cur_range_end;
  struct wire_width_tuple wire_width_table[MAXNUMVARS];
  int num_width_table_entries;
  //struct 
  _cfg_node * cfg_temp;
  _cfg_node * cfg_top;
  //_cfg_node * cfg_table[MAXNUMCFGS];

  _instance_node * top_instance_list;
  _instance_node * current_instance_node;
  //_cfg_block * current_cfg_block;
  
  _exptree_node * current_exptree_event;
  
  _cfg_node * current_cfg_node;

  _cfg_node * branch_table[MAXNUMBRAN];
 
  _var_table * current_var_table;
  int var_table_id;
 
  int var_index = 0;
  int var_number = 0;
 
  int branch_var_allocated=0;

  int intemediate_var_allocated = 0;
   
  _var_node * current_lvalue_node;
  bool input_handling;

  //int cfg_stack_pointer = 0;
  FILE * dbg_out;
  FILE * cfg_out; 


/* Line 189 of yacc.c  */
#line 121 "vlgParser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     YYLID = 258,
     YYINUMBER = 259,
     YYINUMBER_BIT = 260,
     YYRNUMBER = 261,
     YYSTRING = 262,
     YYALLPATH = 263,
     YYALWAYS = 264,
     YYAND = 265,
     YYASSIGN = 266,
     YYBEGIN = 267,
     YYBUF = 268,
     YYBUFIF0 = 269,
     YYBUFIF1 = 270,
     YYCASE = 271,
     YYCASEX = 272,
     YYCASEZ = 273,
     YYCMOS = 274,
     YYCONDITIONAL = 275,
     YYDEASSIGN = 276,
     YYDEFAULT = 277,
     YYDEFPARAM = 278,
     YYDISABLE = 279,
     YYEDGE = 280,
     YYEND = 281,
     YYENDCASE = 282,
     YYENDMODULE = 283,
     YYENDFUNCTION = 284,
     YYENDPRIMITIVE = 285,
     YYENDSPECIFY = 286,
     YYENDTABLE = 287,
     YYENDTASK = 288,
     YYENUM = 289,
     YYEVENT = 290,
     YYFOR = 291,
     YYFOREVER = 292,
     YYFORK = 293,
     YYFUNCTION = 294,
     YYGEQ = 295,
     YYHIGHZ0 = 296,
     YYHIGHZ1 = 297,
     YYIF = 298,
     YYELSE = 299,
     YYLOWERTHANELSE = 300,
     YYINITIAL = 301,
     YYINOUT = 302,
     YYINPUT = 303,
     YYINTEGER = 304,
     YYJOIN = 305,
     YYLARGE = 306,
     YYLEADTO = 307,
     YYLEQ = 308,
     YYLOGAND = 309,
     YYCASEEQUALITY = 310,
     YYCASEINEQUALITY = 311,
     YYLOGNAND = 312,
     YYLOGNOR = 313,
     YYLOGOR = 314,
     YYLOGXNOR = 315,
     YYLOGEQUALITY = 316,
     YYLOGINEQUALITY = 317,
     YYLSHIFT = 318,
     YYMACROMODULE = 319,
     YYMEDIUM = 320,
     YYMODULE = 321,
     YYMREG = 322,
     YYNAND = 323,
     YYNBASSIGN = 324,
     YYNEGEDGE = 325,
     YYNMOS = 326,
     YYNOR = 327,
     YYNOT = 328,
     YYNOTIF0 = 329,
     YYNOTIF1 = 330,
     YYOR = 331,
     YYOUTPUT = 332,
     YYPARAMETER = 333,
     YYPMOS = 334,
     YYPOSEDGE = 335,
     YYPRIMITIVE = 336,
     YYPULL0 = 337,
     YYPULL1 = 338,
     YYPULLUP = 339,
     YYPULLDOWN = 340,
     YYRCMOS = 341,
     YYREAL = 342,
     YYREG = 343,
     YYREPEAT = 344,
     YYRIGHTARROW = 345,
     YYRNMOS = 346,
     YYRPMOS = 347,
     YYRSHIFT = 348,
     YYRTRAN = 349,
     YYRTRANIF0 = 350,
     YYRTRANIF1 = 351,
     YYSCALARED = 352,
     YYSMALL = 353,
     YYSPECIFY = 354,
     YYSPECPARAM = 355,
     YYSTRONG0 = 356,
     YYSTRONG1 = 357,
     YYSUPPLY0 = 358,
     YYSUPPLY1 = 359,
     YYSWIRE = 360,
     YYTABLE = 361,
     YYTASK = 362,
     YYTESLATIMER = 363,
     YYTIME = 364,
     YYTRAN = 365,
     YYTRANIF0 = 366,
     YYTRANIF1 = 367,
     YYTRI = 368,
     YYTRI0 = 369,
     YYTRI1 = 370,
     YYTRIAND = 371,
     YYTRIOR = 372,
     YYTRIREG = 373,
     YYuTYPE = 374,
     YYTYPEDEF = 375,
     YYVECTORED = 376,
     YYWAIT = 377,
     YYWAND = 378,
     YYWEAK0 = 379,
     YYWEAK1 = 380,
     YYWHILE = 381,
     YYWIRE = 382,
     YYWOR = 383,
     YYXNOR = 384,
     YYXOR = 385,
     YYsysSETUP = 386,
     YYsysID = 387,
     YYsysND = 388,
     YYUNARYOPERATOR = 389
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 58 "vlgParser.y"

  char id[MAXSTRLEN];
  _cfg_node * NODE;
  _exptree_node * EXPNODE;



/* Line 214 of yacc.c  */
#line 299 "vlgParser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 311 "vlgParser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3313

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  175
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  163
/* YYNRULES -- Number of rules.  */
#define YYNRULES  450
/* YYNRULES -- Number of states.  */
#define YYNSTATES  911

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   389

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   147,     2,   173,     2,   145,   138,     2,
     150,   151,   143,   141,   152,   142,   153,   144,   158,   159,
       2,     2,     2,     2,     2,     2,     2,     2,   135,   149,
     139,   172,   140,   134,   174,     2,   163,     2,     2,     2,
     167,     2,     2,     2,     2,     2,     2,     2,   171,     2,
     169,     2,   165,     2,     2,     2,     2,     2,   161,     2,
       2,   156,     2,   157,   137,     2,     2,     2,   162,     2,
       2,     2,   166,     2,     2,     2,     2,     2,     2,     2,
     170,     2,   168,     2,   164,     2,     2,     2,     2,     2,
     160,     2,     2,   154,   136,   155,   146,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   148
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,     9,    11,    13,    18,    19,
      20,    29,    36,    37,    41,    43,    47,    49,    50,    57,
      58,    60,    62,    66,    68,    72,    74,    79,    86,    87,
      90,    92,    94,    96,    98,   100,   102,   104,   106,   108,
     110,   112,   114,   116,   118,   120,   122,   124,   126,   128,
     129,   140,   142,   145,   147,   149,   151,   155,   157,   159,
     161,   164,   169,   171,   174,   181,   183,   185,   188,   190,
     192,   197,   199,   201,   203,   205,   207,   209,   211,   213,
     215,   217,   219,   221,   223,   225,   227,   229,   231,   233,
     235,   237,   239,   241,   243,   245,   246,   254,   255,   264,
     265,   267,   269,   271,   273,   274,   277,   279,   282,   284,
     286,   288,   290,   292,   294,   296,   298,   300,   302,   304,
     305,   310,   311,   317,   319,   323,   324,   326,   330,   332,
     333,   338,   340,   341,   345,   350,   355,   360,   368,   376,
     384,   386,   388,   390,   392,   394,   396,   398,   400,   402,
     404,   406,   407,   409,   411,   414,   417,   423,   429,   433,
     437,   441,   445,   446,   453,   457,   459,   463,   465,   469,
     471,   478,   480,   482,   486,   488,   489,   491,   495,   499,
     503,   504,   506,   512,   518,   520,   522,   524,   526,   528,
     530,   532,   534,   536,   538,   539,   541,   547,   549,   553,
     558,   559,   562,   564,   566,   568,   570,   572,   574,   576,
     578,   580,   582,   584,   586,   588,   590,   592,   594,   596,
     598,   600,   602,   604,   606,   608,   610,   612,   614,   616,
     618,   620,   622,   626,   630,   635,   637,   639,   643,   645,
     650,   652,   653,   656,   658,   660,   663,   666,   669,   672,
     677,   679,   683,   687,   692,   694,   696,   698,   702,   704,
     708,   709,   711,   717,   718,   722,   723,   727,   728,   730,
     731,   734,   736,   739,   745,   753,   760,   767,   774,   777,
     783,   789,   799,   802,   805,   812,   819,   826,   833,   839,
     843,   845,   847,   849,   851,   852,   857,   861,   865,   870,
     875,   876,   878,   881,   885,   889,   892,   896,   903,   907,
     914,   916,   917,   920,   922,   924,   926,   928,   930,   932,
     933,   937,   938,   945,   948,   954,   956,   960,   961,   964,
     966,   968,   970,   972,   974,   978,   983,   989,   995,   997,
    1001,  1003,  1008,  1015,  1017,  1026,  1028,  1034,  1042,  1056,
    1058,  1063,  1073,  1090,  1097,  1108,  1109,  1111,  1113,  1115,
    1129,  1144,  1161,  1181,  1200,  1221,  1223,  1225,  1227,  1232,
    1239,  1241,  1243,  1247,  1249,  1253,  1259,  1261,  1265,  1269,
    1271,  1274,  1277,  1280,  1283,  1286,  1289,  1292,  1295,  1299,
    1303,  1307,  1311,  1315,  1319,  1323,  1327,  1331,  1335,  1339,
    1343,  1347,  1351,  1355,  1359,  1363,  1367,  1371,  1375,  1381,
    1384,  1387,  1390,  1393,  1396,  1399,  1402,  1405,  1407,  1409,
    1411,  1416,  1423,  1425,  1429,  1436,  1438,  1440,  1444,  1445,
    1447,  1450,  1453,  1456,  1459,  1464,  1471,  1480,  1483,  1486,
    1489,  1492,  1497,  1500,  1505,  1510,  1512,  1515,  1518,  1521,
    1525
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     176,     0,    -1,    -1,   176,   177,    -1,   179,    -1,   192,
      -1,   178,    -1,   120,   221,   220,   149,    -1,    -1,    -1,
      66,     3,   180,   182,   149,   181,   190,    28,    -1,    64,
       3,   182,   149,   190,    28,    -1,    -1,   150,   183,   151,
      -1,   184,    -1,   183,   152,   184,    -1,   186,    -1,    -1,
     153,     3,   185,   150,   186,   151,    -1,    -1,   187,    -1,
     189,    -1,   154,   188,   155,    -1,   189,    -1,   188,   152,
     189,    -1,     3,    -1,     3,   156,   327,   157,    -1,     3,
     156,   327,   135,   327,   157,    -1,    -1,   190,   191,    -1,
     231,    -1,   232,    -1,   233,    -1,   234,    -1,   235,    -1,
     239,    -1,   240,    -1,   241,    -1,   242,    -1,   243,    -1,
     262,    -1,   271,    -1,   246,    -1,   244,    -1,   305,    -1,
     283,    -1,   285,    -1,   211,    -1,   213,    -1,    -1,    81,
       3,   193,   150,   183,   151,   149,   194,   196,    30,    -1,
     195,    -1,   194,   195,    -1,   233,    -1,   239,    -1,   232,
      -1,   106,   197,    32,    -1,   198,    -1,   200,    -1,   199,
      -1,   198,   199,    -1,   202,   135,   208,   149,    -1,   201,
      -1,   200,   201,    -1,   202,   135,   206,   135,   207,   149,
      -1,   203,    -1,   204,    -1,   203,   204,    -1,   209,    -1,
     205,    -1,   150,   209,   209,   151,    -1,   210,    -1,   209,
      -1,   208,    -1,   142,    -1,   158,    -1,   159,    -1,   160,
      -1,   161,    -1,   158,    -1,   159,    -1,   160,    -1,   161,
      -1,   134,    -1,   162,    -1,   163,    -1,   164,    -1,   165,
      -1,   166,    -1,   167,    -1,   168,    -1,   169,    -1,   170,
      -1,   171,    -1,   143,    -1,    -1,   107,     3,   212,   149,
     217,   287,    33,    -1,    -1,    39,   215,     3,   214,   149,
     218,   287,    29,    -1,    -1,   216,    -1,   260,    -1,    49,
      -1,    87,    -1,    -1,   217,   219,    -1,   219,    -1,   218,
     219,    -1,   231,    -1,   232,    -1,   233,    -1,   234,    -1,
     239,    -1,   240,    -1,   241,    -1,   242,    -1,   243,    -1,
       3,    -1,   222,    -1,    -1,    34,   223,   225,   226,    -1,
      -1,    34,   224,   154,   227,   155,    -1,     3,    -1,   154,
     227,   155,    -1,    -1,   228,    -1,   227,   152,   228,    -1,
       3,    -1,    -1,     3,   229,   172,   327,    -1,   119,    -1,
      -1,    78,   261,   149,    -1,    48,   259,   247,   149,    -1,
      77,   259,   247,   149,    -1,    47,   259,   247,   149,    -1,
     230,   236,   255,   237,   332,   261,   149,    -1,   230,   236,
     255,   237,   332,   247,   149,    -1,   230,   118,   253,   237,
     332,   247,   149,    -1,   105,    -1,   127,    -1,   113,    -1,
     115,    -1,   103,    -1,   123,    -1,   116,    -1,   114,    -1,
     104,    -1,   128,    -1,   117,    -1,    -1,   238,    -1,   260,
      -1,    97,   260,    -1,   121,   260,    -1,   230,    88,   259,
     248,   149,    -1,   230,    67,   259,   248,   149,    -1,   109,
     248,   149,    -1,    49,   248,   149,    -1,    87,   247,   149,
      -1,    35,   251,   149,    -1,    -1,    11,   245,   255,   332,
     291,   149,    -1,    23,   261,   149,    -1,   331,    -1,   247,
     152,   331,    -1,   249,    -1,   248,   152,   249,    -1,   250,
      -1,   250,   156,     4,   135,     4,   157,    -1,     3,    -1,
     252,    -1,   251,   152,   252,    -1,     3,    -1,    -1,   254,
      -1,   150,    98,   151,    -1,   150,    65,   151,    -1,   150,
      51,   151,    -1,    -1,   256,    -1,   150,   257,   152,   258,
     151,    -1,   150,   258,   152,   257,   151,    -1,   103,    -1,
     101,    -1,    82,    -1,   124,    -1,    41,    -1,   104,    -1,
     102,    -1,    83,    -1,   125,    -1,    42,    -1,    -1,   260,
      -1,   156,     4,   135,     4,   157,    -1,   291,    -1,   261,
     152,   291,    -1,   265,   263,   266,   149,    -1,    -1,   263,
     264,    -1,   256,    -1,   333,    -1,    10,    -1,    68,    -1,
      76,    -1,    72,    -1,   130,    -1,   129,    -1,    13,    -1,
      14,    -1,    15,    -1,    73,    -1,    74,    -1,    75,    -1,
      85,    -1,    84,    -1,    71,    -1,    79,    -1,    91,    -1,
      92,    -1,    19,    -1,    86,    -1,   110,    -1,    94,    -1,
     111,    -1,    95,    -1,   112,    -1,    96,    -1,   108,    -1,
     267,    -1,   266,   152,   267,    -1,   150,   269,   151,    -1,
     268,   150,   269,   151,    -1,     3,    -1,   270,    -1,   269,
     152,   270,    -1,   327,    -1,   272,   273,   276,   149,    -1,
       3,    -1,    -1,   273,   274,    -1,   256,    -1,   275,    -1,
     173,     4,    -1,   173,     5,    -1,   173,     6,    -1,   173,
     331,    -1,   173,   150,   324,   151,    -1,   277,    -1,   276,
     152,   277,    -1,   150,   278,   151,    -1,   331,   150,   278,
     151,    -1,   279,    -1,   280,    -1,   281,    -1,   279,   152,
     281,    -1,   282,    -1,   280,   152,   282,    -1,    -1,   327,
      -1,   153,   331,   150,   327,   151,    -1,    -1,    46,   284,
     289,    -1,    -1,     9,   286,   289,    -1,    -1,   289,    -1,
      -1,   288,   289,    -1,   149,    -1,   291,   149,    -1,    43,
     150,   327,   151,   289,    -1,    43,   150,   327,   151,   289,
      44,   289,    -1,    16,   150,   327,   151,   293,    27,    -1,
      18,   150,   327,   151,   293,    27,    -1,    17,   150,   327,
     151,   293,    27,    -1,    37,   289,    -1,    89,   150,   327,
     151,   289,    -1,   126,   150,   327,   151,   289,    -1,    36,
     150,   291,   149,   327,   149,   291,   151,   289,    -1,   334,
     289,    -1,   335,   289,    -1,   323,   292,   172,   334,   327,
     149,    -1,   323,   292,   172,   335,   327,   149,    -1,   323,
     292,    69,   334,   327,   149,    -1,   323,   292,    69,   335,
     327,   149,    -1,   122,   150,   327,   151,   289,    -1,    90,
     252,   149,    -1,   295,    -1,   296,    -1,   300,    -1,   303,
      -1,    -1,    24,     3,   290,   149,    -1,    11,   291,   149,
      -1,    21,   323,   149,    -1,   323,   292,   172,   327,    -1,
     323,   292,    69,   327,    -1,    -1,   294,    -1,   293,   294,
      -1,   326,   135,   289,    -1,    22,   135,   289,    -1,    22,
     289,    -1,    12,   288,    26,    -1,    12,   135,   297,   298,
     288,    26,    -1,    38,   288,    50,    -1,    38,   135,   297,
     298,   288,    50,    -1,     3,    -1,    -1,   298,   299,    -1,
     231,    -1,   239,    -1,   241,    -1,   242,    -1,   240,    -1,
     243,    -1,    -1,     3,   301,   149,    -1,    -1,     3,   302,
     150,   326,   151,   149,    -1,   304,   149,    -1,   304,   150,
     326,   151,   149,    -1,   330,    -1,    99,   306,    31,    -1,
      -1,   306,   307,    -1,   308,    -1,   309,    -1,   317,    -1,
     321,    -1,   316,    -1,   100,   261,   149,    -1,   310,   172,
     314,   149,    -1,   150,   312,    52,   312,   151,    -1,   150,
     311,     8,   313,   151,    -1,   312,    -1,   311,   152,   312,
      -1,     3,    -1,     3,   156,   327,   157,    -1,     3,   156,
     327,   149,   327,   157,    -1,   311,    -1,   150,   311,   152,
     312,   320,    20,   327,   151,    -1,   315,    -1,   150,   315,
     152,   315,   151,    -1,   150,   315,   152,   315,   152,   315,
     151,    -1,   150,   315,   152,   315,   152,   315,   152,   315,
     152,   315,   152,   315,   151,    -1,   327,    -1,   131,   150,
     151,   149,    -1,    43,   150,   327,   151,   150,   312,   319,
      52,   318,    -1,    43,   150,   327,   151,   150,   311,   152,
     312,   319,     8,   311,   151,   311,   172,   314,   149,    -1,
     312,   151,   311,   172,   314,   149,    -1,   150,   312,   320,
      20,   327,   151,   151,   172,   314,   149,    -1,    -1,   320,
      -1,   141,    -1,   142,    -1,   150,   312,    52,   150,   312,
     320,    20,   327,   151,   151,   172,   314,   149,    -1,   150,
     322,   312,    52,   150,   312,   320,    20,   327,   151,   151,
     172,   314,   149,    -1,   150,   322,   312,     8,   150,   311,
     152,   312,   320,    20,   327,   151,   151,   172,   314,   149,
      -1,    43,   150,   327,   151,   150,   312,     8,   150,   311,
     152,   312,   320,    20,   327,   151,   151,   172,   314,   149,
      -1,    43,   150,   327,   151,   150,   322,   312,    52,   150,
     312,   320,    20,   327,   151,   151,   172,   314,   149,    -1,
      43,   150,   327,   151,   150,   322,   312,     8,   150,   311,
     152,   312,   320,    20,   327,   151,   151,   172,   314,   149,
      -1,    80,    -1,    70,    -1,   331,    -1,   331,   156,   327,
     157,    -1,   331,   156,   327,   135,   327,   157,    -1,   329,
      -1,   325,    -1,   324,   152,   325,    -1,   327,    -1,   327,
     135,   327,    -1,   327,   135,   327,   135,   327,    -1,   327,
      -1,   326,   152,   327,    -1,   150,   327,   151,    -1,   328,
      -1,   147,   328,    -1,   146,   328,    -1,   138,   328,    -1,
     136,   328,    -1,   137,   328,    -1,    57,   328,    -1,    58,
     328,    -1,    60,   328,    -1,   327,   141,   327,    -1,   327,
     142,   327,    -1,   327,   143,   327,    -1,   327,   144,   327,
      -1,   327,   145,   327,    -1,   327,    61,   327,    -1,   327,
      62,   327,    -1,   327,    54,   327,    -1,   327,    59,   327,
      -1,   327,   139,   327,    -1,   327,   140,   327,    -1,   327,
     138,   327,    -1,   327,   136,   327,    -1,   327,   137,   327,
      -1,   327,    53,   327,    -1,   327,    69,   327,    -1,   327,
      40,   327,    -1,   327,    63,   327,    -1,   327,    93,   327,
      -1,   327,    60,   327,    -1,   327,   134,   327,   135,   327,
      -1,   147,   327,    -1,   146,   327,    -1,   138,   327,    -1,
     136,   327,    -1,   137,   327,    -1,    57,   327,    -1,    58,
     327,    -1,    60,   327,    -1,     4,    -1,     5,    -1,   331,
      -1,   331,   156,   327,   157,    -1,   331,   156,   327,   135,
     327,   157,    -1,   329,    -1,   154,   326,   155,    -1,   154,
       4,   154,   326,   155,   155,    -1,   132,    -1,     3,    -1,
     331,   153,     3,    -1,    -1,   333,    -1,   173,     4,    -1,
     173,     5,    -1,   173,     6,    -1,   173,   331,    -1,   173,
     150,   325,   151,    -1,   173,   150,   325,   152,   325,   151,
      -1,   173,   150,   325,   152,   325,   152,   325,   151,    -1,
     173,     4,    -1,   173,     5,    -1,   173,     6,    -1,   173,
     331,    -1,   173,   150,   324,   151,    -1,   174,   331,    -1,
     174,   150,   336,   151,    -1,   174,   150,   337,   151,    -1,
     327,    -1,    80,   327,    -1,    70,   327,    -1,    25,   327,
      -1,   336,    76,   336,    -1,   337,    76,   336,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   280,   280,   284,   291,   295,   299,   306,   314,   328,
     313,   340,   350,   354,   362,   367,   375,   381,   380,   392,
     396,   404,   409,   417,   422,   431,   436,   442,   453,   456,
     463,   467,   471,   475,   479,   483,   487,   491,   495,   499,
     503,   507,   511,   515,   519,   523,   527,   531,   535,   543,
     542,   555,   559,   566,   570,   574,   581,   588,   592,   599,
     603,   610,   617,   621,   628,   635,   642,   646,   653,   657,
     664,   668,   675,   682,   686,   693,   697,   701,   705,   712,
     716,   720,   724,   728,   732,   736,   743,   747,   751,   755,
     759,   763,   767,   771,   775,   783,   782,   794,   793,   805,
     808,   815,   819,   823,   831,   834,   841,   845,   852,   856,
     860,   864,   868,   872,   876,   880,   884,   891,   898,   905,
     905,   909,   909,   916,   923,   928,   934,   938,   945,   949,
     949,   958,   963,   969,   976,   992,  1006,  1020,  1025,  1037,
    1046,  1050,  1054,  1058,  1062,  1066,  1070,  1074,  1078,  1082,
    1086,  1095,  1101,  1110,  1116,  1122,  1131,  1144,  1151,  1158,
    1165,  1172,  1179,  1179,  1197,  1205,  1211,  1220,  1225,  1233,
    1239,  1249,  1257,  1261,  1268,  1276,  1279,  1286,  1290,  1294,
    1302,  1305,  1312,  1316,  1323,  1327,  1331,  1335,  1339,  1346,
    1350,  1354,  1358,  1362,  1370,  1376,  1386,  1396,  1403,  1413,
    1422,  1425,  1432,  1436,  1443,  1448,  1453,  1458,  1463,  1468,
    1473,  1478,  1483,  1488,  1493,  1498,  1503,  1508,  1513,  1518,
    1523,  1528,  1533,  1538,  1543,  1548,  1553,  1558,  1563,  1568,
    1573,  1581,  1586,  1594,  1599,  1607,  1615,  1620,  1628,  1638,
    1650,  1659,  1663,  1671,  1676,  1684,  1689,  1694,  1699,  1704,
    1712,  1717,  1726,  1732,  1740,  1745,  1754,  1759,  1767,  1773,
    1786,  1790,  1799,  1816,  1816,  1824,  1824,  1835,  1839,  1848,
    1854,  1873,  1881,  1901,  1915,  1948,  1972,  1991,  2009,  2016,
    2023,  2030,  2036,  2042,  2048,  2063,  2078,  2094,  2110,  2115,
    2120,  2125,  2130,  2135,  2141,  2140,  2148,  2162,  2170,  2177,
    2188,  2193,  2198,  2212,  2228,  2237,  2247,  2256,  2265,  2269,
    2276,  2285,  2288,  2295,  2299,  2303,  2307,  2311,  2315,  2324,
    2323,  2331,  2330,  2340,  2344,  2351,  2360,  2366,  2368,  2372,
    2373,  2374,  2375,  2376,  2380,  2384,  2390,  2391,  2395,  2396,
    2400,  2401,  2402,  2406,  2407,  2413,  2414,  2415,  2417,  2423,
    2429,  2433,  2436,  2443,  2444,  2448,  2450,  2454,  2455,  2459,
    2462,  2465,  2469,  2474,  2478,  2486,  2487,  2494,  2505,  2517,
    2531,  2542,  2547,  2555,  2561,  2567,  2579,  2585,  2600,  2605,
    2611,  2617,  2623,  2629,  2635,  2641,  2647,  2653,  2659,  2665,
    2671,  2677,  2683,  2689,  2695,  2701,  2708,  2714,  2720,  2726,
    2732,  2738,  2744,  2750,  2756,  2763,  2769,  2775,  2781,  2788,
    2794,  2800,  2806,  2812,  2818,  2824,  2830,  2840,  2850,  2860,
    2867,  2878,  2888,  2919,  2930,  2960,  2966,  2971,  2980,  2983,
    2990,  2994,  2998,  3002,  3006,  3010,  3014,  3022,  3026,  3030,
    3034,  3038,  3045,  3050,  3055,  3064,  3070,  3076,  3082,  3091,
    3096
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "YYLID", "YYINUMBER", "YYINUMBER_BIT",
  "YYRNUMBER", "YYSTRING", "YYALLPATH", "YYALWAYS", "YYAND", "YYASSIGN",
  "YYBEGIN", "YYBUF", "YYBUFIF0", "YYBUFIF1", "YYCASE", "YYCASEX",
  "YYCASEZ", "YYCMOS", "YYCONDITIONAL", "YYDEASSIGN", "YYDEFAULT",
  "YYDEFPARAM", "YYDISABLE", "YYEDGE", "YYEND", "YYENDCASE", "YYENDMODULE",
  "YYENDFUNCTION", "YYENDPRIMITIVE", "YYENDSPECIFY", "YYENDTABLE",
  "YYENDTASK", "YYENUM", "YYEVENT", "YYFOR", "YYFOREVER", "YYFORK",
  "YYFUNCTION", "YYGEQ", "YYHIGHZ0", "YYHIGHZ1", "YYIF", "YYELSE",
  "YYLOWERTHANELSE", "YYINITIAL", "YYINOUT", "YYINPUT", "YYINTEGER",
  "YYJOIN", "YYLARGE", "YYLEADTO", "YYLEQ", "YYLOGAND", "YYCASEEQUALITY",
  "YYCASEINEQUALITY", "YYLOGNAND", "YYLOGNOR", "YYLOGOR", "YYLOGXNOR",
  "YYLOGEQUALITY", "YYLOGINEQUALITY", "YYLSHIFT", "YYMACROMODULE",
  "YYMEDIUM", "YYMODULE", "YYMREG", "YYNAND", "YYNBASSIGN", "YYNEGEDGE",
  "YYNMOS", "YYNOR", "YYNOT", "YYNOTIF0", "YYNOTIF1", "YYOR", "YYOUTPUT",
  "YYPARAMETER", "YYPMOS", "YYPOSEDGE", "YYPRIMITIVE", "YYPULL0",
  "YYPULL1", "YYPULLUP", "YYPULLDOWN", "YYRCMOS", "YYREAL", "YYREG",
  "YYREPEAT", "YYRIGHTARROW", "YYRNMOS", "YYRPMOS", "YYRSHIFT", "YYRTRAN",
  "YYRTRANIF0", "YYRTRANIF1", "YYSCALARED", "YYSMALL", "YYSPECIFY",
  "YYSPECPARAM", "YYSTRONG0", "YYSTRONG1", "YYSUPPLY0", "YYSUPPLY1",
  "YYSWIRE", "YYTABLE", "YYTASK", "YYTESLATIMER", "YYTIME", "YYTRAN",
  "YYTRANIF0", "YYTRANIF1", "YYTRI", "YYTRI0", "YYTRI1", "YYTRIAND",
  "YYTRIOR", "YYTRIREG", "YYuTYPE", "YYTYPEDEF", "YYVECTORED", "YYWAIT",
  "YYWAND", "YYWEAK0", "YYWEAK1", "YYWHILE", "YYWIRE", "YYWOR", "YYXNOR",
  "YYXOR", "YYsysSETUP", "YYsysID", "YYsysND", "'?'", "':'", "'|'", "'^'",
  "'&'", "'<'", "'>'", "'+'", "'-'", "'*'", "'/'", "'%'", "'~'", "'!'",
  "YYUNARYOPERATOR", "';'", "'('", "')'", "','", "'.'", "'{'", "'}'",
  "'['", "']'", "'0'", "'1'", "'x'", "'X'", "'b'", "'B'", "'r'", "'R'",
  "'f'", "'F'", "'p'", "'P'", "'n'", "'N'", "'='", "'#'", "'@'", "$accept",
  "source_text", "description", "type_declaration", "module", "$@1", "$@2",
  "port_list_opt", "port_list", "port", "$@3", "port_expression_opt",
  "port_expression", "port_ref_list", "port_reference", "module_item_clr",
  "module_item", "primitive", "$@4", "primitive_declaration_eclr",
  "primitive_declaration", "table_definition", "table_entries",
  "combinational_entry_eclr", "combinational_entry",
  "sequential_entry_eclr", "sequential_entry", "input_list",
  "level_symbol_or_edge_eclr", "level_symbol_or_edge", "edge", "state",
  "next_state", "output_symbol", "level_symbol", "edge_symbol", "task",
  "$@5", "function", "$@6", "range_or_type_opt", "range_or_type",
  "tf_declaration_clr", "tf_declaration_eclr", "tf_declaration",
  "type_name", "type_specifier", "enum_specifier", "$@7", "$@8",
  "enum_name", "enum_lst_opt", "enumerator_list", "enumerator", "$@9",
  "type_decorator_opt", "parameter_declaration", "input_declaration",
  "output_declaration", "inout_declaration", "net_declaration", "nettype",
  "expandrange_opt", "expandrange", "reg_declaration", "time_declaration",
  "integer_declaration", "real_declaration", "event_declaration",
  "continuous_assign", "$@10", "parameter_override", "variable_list",
  "register_variable_list", "register_variable", "name_of_register",
  "name_of_event_list", "name_of_event", "charge_strength_opt",
  "charge_strength", "drive_strength_opt", "drive_strength", "strength0",
  "strength1", "range_opt", "range", "assignment_list",
  "gate_instantiation", "drive_delay_clr", "drive_delay", "gatetype",
  "gate_instance_list", "gate_instance", "name_of_gate_instance",
  "terminal_list", "terminal", "module_or_primitive_instantiation",
  "name_of_module_or_primitive", "module_or_primitive_option_clr",
  "module_or_primitive_option", "delay_or_parameter_value_assignment",
  "module_or_primitive_instance_list", "module_or_primitive_instance",
  "module_connection_list", "module_port_connection_list",
  "named_port_connection_list", "module_port_connection",
  "named_port_connection", "initial_statement", "$@11", "always_statement",
  "$@12", "statement_opt", "statement_clr", "statement", "$@13",
  "assignment_ext", "type_action", "case_item_eclr", "case_item",
  "seq_block", "par_block", "name_of_block", "block_declaration_clr",
  "block_declaration", "task_enable", "$@14", "$@15", "system_task_enable",
  "name_of_system_task", "specify_block", "specify_item_clr",
  "specify_item", "specparam_declaration", "path_declaration",
  "path_description", "path_list", "specify_terminal_descriptor",
  "path_list_or_edge_sensitive_path_list", "path_delay_value",
  "path_delay_expression", "system_timing_check",
  "level_sensitive_path_declaration",
  "spec_terminal_desptr_or_edge_sensitive_spec_terminal_desptr",
  "polarity_operator_opt", "polarity_operator",
  "edge_sensitive_path_declaration", "edge_identifier", "lvalue_ext",
  "mintypmax_expression_list", "mintypmax_expression",
  "expression_list_ext", "expression_ext", "primary_ext", "concatenation",
  "system_identifier", "identifier", "delay_opt", "delay", "delay_control",
  "event_control", "event_expression", "ored_event_expression", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,    63,    58,   124,    94,    38,    60,
      62,    43,    45,    42,    47,    37,   126,    33,   389,    59,
      40,    41,    44,    46,   123,   125,    91,    93,    48,    49,
     120,    88,    98,    66,   114,    82,   102,    70,   112,    80,
     110,    78,    61,    35,    64
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   175,   176,   176,   177,   177,   177,   178,   180,   181,
     179,   179,   182,   182,   183,   183,   184,   185,   184,   186,
     186,   187,   187,   188,   188,   189,   189,   189,   190,   190,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   191,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   193,
     192,   194,   194,   195,   195,   195,   196,   197,   197,   198,
     198,   199,   200,   200,   201,   202,   203,   203,   204,   204,
     205,   205,   206,   207,   207,   208,   208,   208,   208,   209,
     209,   209,   209,   209,   209,   209,   210,   210,   210,   210,
     210,   210,   210,   210,   210,   212,   211,   214,   213,   215,
     215,   216,   216,   216,   217,   217,   218,   218,   219,   219,
     219,   219,   219,   219,   219,   219,   219,   220,   221,   223,
     222,   224,   222,   225,   226,   226,   227,   227,   228,   229,
     228,   230,   230,   231,   232,   233,   234,   235,   235,   235,
     236,   236,   236,   236,   236,   236,   236,   236,   236,   236,
     236,   237,   237,   238,   238,   238,   239,   239,   240,   241,
     242,   243,   245,   244,   246,   247,   247,   248,   248,   249,
     249,   250,   251,   251,   252,   253,   253,   254,   254,   254,
     255,   255,   256,   256,   257,   257,   257,   257,   257,   258,
     258,   258,   258,   258,   259,   259,   260,   261,   261,   262,
     263,   263,   264,   264,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   265,   265,   265,   265,   265,   265,   265,   265,   265,
     265,   266,   266,   267,   267,   268,   269,   269,   270,   271,
     272,   273,   273,   274,   274,   275,   275,   275,   275,   275,
     276,   276,   277,   277,   278,   278,   279,   279,   280,   280,
     281,   281,   282,   284,   283,   286,   285,   287,   287,   288,
     288,   289,   289,   289,   289,   289,   289,   289,   289,   289,
     289,   289,   289,   289,   289,   289,   289,   289,   289,   289,
     289,   289,   289,   289,   290,   289,   289,   289,   291,   291,
     292,   293,   293,   294,   294,   294,   295,   295,   296,   296,
     297,   298,   298,   299,   299,   299,   299,   299,   299,   301,
     300,   302,   300,   303,   303,   304,   305,   306,   306,   307,
     307,   307,   307,   307,   308,   309,   310,   310,   311,   311,
     312,   312,   312,   313,   313,   314,   314,   314,   314,   315,
     316,   317,   317,   318,   318,   319,   319,   320,   320,   321,
     321,   321,   321,   321,   321,   322,   322,   323,   323,   323,
     323,   324,   324,   325,   325,   325,   326,   326,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   328,   328,   328,
     328,   328,   328,   329,   329,   330,   331,   331,   332,   332,
     333,   333,   333,   333,   333,   333,   333,   334,   334,   334,
     334,   334,   335,   335,   335,   336,   336,   336,   336,   337,
     337
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     1,     1,     1,     4,     0,     0,
       8,     6,     0,     3,     1,     3,     1,     0,     6,     0,
       1,     1,     3,     1,     3,     1,     4,     6,     0,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
      10,     1,     2,     1,     1,     1,     3,     1,     1,     1,
       2,     4,     1,     2,     6,     1,     1,     2,     1,     1,
       4,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     7,     0,     8,     0,
       1,     1,     1,     1,     0,     2,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       4,     0,     5,     1,     3,     0,     1,     3,     1,     0,
       4,     1,     0,     3,     4,     4,     4,     7,     7,     7,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     0,     1,     1,     2,     2,     5,     5,     3,     3,
       3,     3,     0,     6,     3,     1,     3,     1,     3,     1,
       6,     1,     1,     3,     1,     0,     1,     3,     3,     3,
       0,     1,     5,     5,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     1,     5,     1,     3,     4,
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     3,     4,     1,     1,     3,     1,     4,
       1,     0,     2,     1,     1,     2,     2,     2,     2,     4,
       1,     3,     3,     4,     1,     1,     1,     3,     1,     3,
       0,     1,     5,     0,     3,     0,     3,     0,     1,     0,
       2,     1,     2,     5,     7,     6,     6,     6,     2,     5,
       5,     9,     2,     2,     6,     6,     6,     6,     5,     3,
       1,     1,     1,     1,     0,     4,     3,     3,     4,     4,
       0,     1,     2,     3,     3,     2,     3,     6,     3,     6,
       1,     0,     2,     1,     1,     1,     1,     1,     1,     0,
       3,     0,     6,     2,     5,     1,     3,     0,     2,     1,
       1,     1,     1,     1,     3,     4,     5,     5,     1,     3,
       1,     4,     6,     1,     8,     1,     5,     7,    13,     1,
       4,     9,    16,     6,    10,     0,     1,     1,     1,    13,
      14,    16,    19,    18,    20,     1,     1,     1,     4,     6,
       1,     1,     3,     1,     3,     5,     1,     3,     3,     1,
       2,     2,     2,     2,     2,     2,     2,     2,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     5,     2,
       2,     2,     2,     2,     2,     2,     2,     1,     1,     1,
       4,     6,     1,     3,     6,     1,     1,     3,     0,     1,
       2,     2,     2,     2,     4,     6,     8,     2,     2,     2,
       2,     4,     2,     4,     4,     1,     2,     2,     2,     3,
       3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     3,     6,     4,
       5,    12,     8,    49,   119,     0,   118,    19,     0,    12,
       0,     0,     0,   117,     0,    25,     0,     0,     0,    14,
      16,    20,    21,    28,     0,    19,   123,   125,     0,     7,
       0,    17,     0,    23,    13,    19,   132,     9,     0,     0,
     120,   128,     0,   126,   426,   417,   418,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   379,   422,
     419,     0,     0,    22,    15,   240,   265,   204,   162,   210,
     211,   212,   222,     0,    11,     0,    99,   263,   194,   194,
       0,   205,   218,   207,   213,   214,   215,   206,   194,     0,
     219,   217,   216,   223,     0,   220,   221,   225,   227,   229,
     327,     0,   230,     0,   224,   226,   228,   131,   209,   208,
      29,    47,    48,     0,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    43,    42,    40,   200,    41,   241,
      45,    46,    44,    28,     0,     0,     0,     0,   122,   414,
     379,   415,   379,   416,   379,   412,   379,   413,   379,   411,
     379,   410,   379,   409,   379,     0,   417,     0,   376,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    26,     0,     0,    19,    24,     0,   180,     0,   197,
     300,   370,   367,   174,     0,   172,   102,   103,     0,     0,
     100,   101,     0,     0,   195,     0,   171,     0,   167,   169,
       0,     0,     0,   165,     0,    95,     0,   194,   194,   144,
     148,   140,   142,   147,   143,   146,   150,   175,   145,   141,
     149,   180,     0,     0,   132,   132,   124,     0,   127,   378,
       0,     0,   423,   404,   402,   395,   396,   407,   393,   394,
     405,   403,   406,     0,     0,   400,   401,   399,   397,   398,
     388,   389,   390,   391,   392,   427,     0,     0,   426,     0,
     269,     0,     0,     0,     0,     0,     0,     0,   269,     0,
       0,     0,     0,     0,   425,   271,     0,     0,   266,     0,
     290,   291,   292,   293,     0,   300,   325,     0,     0,     0,
     428,   181,   164,     0,     0,     0,   161,     0,     0,    97,
     264,     0,     0,   159,     0,     0,     0,   133,   160,     0,
     326,     0,     0,     0,     0,   328,   329,   330,     0,   333,
     331,   332,     0,   158,     0,     0,     0,   151,   176,   151,
     235,     0,     0,   202,   201,     0,   231,     0,   203,   260,
       0,   243,   242,   244,     0,   250,     0,    10,   132,    51,
       0,    55,    53,    54,   130,     0,   377,     0,    27,     0,
     420,    18,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   294,     0,   278,     0,     0,     0,     0,     0,     0,
       0,   437,   438,   439,     0,   440,     0,   442,   272,   323,
       0,     0,   282,   283,   188,   193,   186,   191,   185,   190,
     184,   189,   187,   192,     0,     0,     0,   429,   198,     0,
       0,     0,   173,     0,     0,   136,   134,   168,     0,   135,
     166,     0,     0,     0,   340,   366,   365,     0,   338,     0,
       0,   104,     0,     0,     0,     0,     0,     0,     0,   428,
     152,   153,   428,     0,   236,   238,   430,   431,   432,     0,
     433,   199,     0,     0,     0,     0,   254,   255,   256,   258,
     261,   245,   246,   247,     0,   248,   239,     0,   260,     0,
      52,     0,     0,   408,     0,   320,     0,   296,   310,   311,
     306,   270,     0,     0,     0,   297,     0,     0,   311,   308,
       0,     0,   289,     0,     0,     0,   371,   373,     0,     0,
       0,   445,     0,     0,     0,     0,     0,     0,     0,     0,
     299,   298,     0,   368,     0,   132,     0,     0,   334,     0,
       0,     0,     0,     0,     0,     0,     0,   345,   349,   132,
     157,   156,   179,   178,   177,   154,   155,     0,     0,   233,
       0,     0,     0,   232,     0,     0,   252,   260,     0,     0,
     260,   251,     0,    83,    94,     0,    79,    80,    81,    82,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
       0,    57,    59,    58,    62,     0,    65,    66,    69,    68,
      71,    50,   424,   421,     0,   269,     0,     0,     0,   295,
       0,   269,     0,     0,     0,     0,   441,     0,     0,   448,
     447,   446,     0,   443,     0,   444,     0,     0,     0,     0,
       0,     0,     0,   163,     0,   196,   132,   106,   108,   109,
     110,   111,   112,   113,   114,   115,   116,     0,     0,   350,
       0,     0,   343,   338,     0,   339,     0,     0,     0,     0,
       0,   349,   335,   105,     0,   268,     0,     0,     0,   165,
     237,   434,     0,   234,     0,   257,   259,   249,   253,     0,
      56,    60,     0,    63,     0,     0,    67,     0,   313,   314,
     317,   315,   316,   318,     0,   312,     0,     0,   301,     0,
       0,     0,     0,     0,   273,   279,   288,   280,   372,   374,
     449,   450,   324,     0,     0,     0,     0,   182,   183,   369,
     107,     0,   170,     0,     0,   341,     0,   337,     0,   336,
       0,     0,     0,    96,   139,   138,   137,     0,     0,     0,
       0,     0,    75,    76,    77,    78,     0,     0,    72,   322,
     307,     0,   305,   275,   302,     0,   277,   276,     0,   309,
       0,     0,   286,   287,   284,   285,    98,     0,   338,     0,
       0,     0,   357,   358,     0,     0,     0,     0,   435,     0,
     262,    70,    75,    76,    77,    78,     0,    61,   304,   303,
       0,   274,   375,     0,     0,     0,   356,     0,   342,   339,
       0,     0,     0,   346,     0,     0,    74,     0,    73,     0,
     339,     0,     0,     0,     0,     0,     0,   339,     0,     0,
     436,    64,   281,     0,     0,     0,     0,   351,     0,     0,
       0,     0,     0,     0,   347,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   339,     0,
       0,     0,     0,   344,     0,     0,     0,     0,     0,     0,
       0,     0,   339,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   359,     0,     0,     0,     0,     0,
       0,   353,     0,     0,     0,   360,     0,     0,     0,     0,
       0,     0,     0,   348,   352,     0,     0,     0,     0,   361,
       0,     0,     0,     0,     0,   354,     0,   363,   362,     0,
     364
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     7,     8,     9,    19,   143,    18,    28,    29,
      71,    30,    31,    42,    32,    46,   120,    10,    20,   368,
     369,   491,   590,   591,   592,   593,   594,   595,   596,   597,
     598,   746,   807,   747,   599,   600,   121,   342,   122,   434,
     209,   210,   549,   636,   637,    24,    15,    16,    21,    22,
      37,    50,    52,    53,   146,   370,   638,   639,   640,   641,
     128,   241,   459,   460,   642,   643,   644,   645,   646,   134,
     197,   135,   222,   217,   218,   219,   204,   205,   347,   348,
     310,   311,   424,   425,   213,   214,   198,   136,   242,   354,
     137,   355,   356,   357,   463,   464,   138,   139,   243,   362,
     363,   364,   365,   475,   476,   477,   478,   479,   140,   212,
     141,   196,   664,   386,   501,   506,   299,   314,   697,   698,
     300,   301,   499,   605,   695,   302,   382,   383,   303,   304,
     142,   224,   335,   336,   337,   338,   447,   653,   654,   546,
     547,   339,   340,   827,   795,   796,   341,   449,   305,   515,
     516,   699,   548,    68,    69,   306,    70,   426,   427,   307,
     308,   522,   523
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -570
static const yytype_int16 yypact[] =
{
    -570,    37,  -570,    73,    82,    90,    65,  -570,  -570,  -570,
    -570,   -31,  -570,  -570,   -26,   184,  -570,    10,    55,   -31,
      66,   236,    99,  -570,   117,   116,   326,   343,   -45,  -570,
    -570,  -570,  -570,  -570,   156,    10,  -570,   122,   347,  -570,
    1307,  -570,   -74,  -570,  -570,    10,  1492,  -570,    48,   347,
    -570,   169,   -55,  -570,  -570,  -570,  -570,  1307,  1307,  1307,
    1307,  1307,  1307,  1307,  1307,  1307,  1335,  1601,  -570,  -570,
      27,   217,   343,  -570,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,    12,  -570,   368,   133,  -570,   221,   221,
     378,  -570,  -570,  -570,  -570,  -570,  -570,  -570,   221,    12,
    -570,  -570,  -570,  -570,   383,  -570,  -570,  -570,  -570,  -570,
    -570,   385,  -570,   378,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  1431,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,   274,   143,   258,   347,  -570,  -570,
    -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,  -570,  2003,   280,   161,  3034,  1307,
    1307,  1307,  1307,  1307,  1307,  1307,  1307,  1307,  1307,  1307,
    1307,  1307,  1307,  1307,  1307,  1307,  1307,  1307,  1307,  1307,
    1307,  -570,   433,  1307,    24,  -570,  1203,   290,   165,  -570,
    -570,  -570,   172,  -570,   182,  -570,  -570,  -570,   441,   450,
    -570,  -570,  1203,   383,  -570,   383,  -570,   206,  -570,   313,
     383,   245,   262,   336,    74,  -570,   263,   221,   221,  -570,
    -570,  -570,  -570,  -570,  -570,  -570,  -570,   359,  -570,  -570,
    -570,   290,     2,     8,  1604,     7,  -570,  1307,  -570,  -570,
    1307,  1307,  -570,   275,   275,  3142,  3127,  3168,   777,   777,
     410,   275,   410,  2906,  1825,  3153,  3168,   583,   275,   275,
      78,    78,  -570,  -570,  -570,  -570,  1707,   346,   253,    12,
     379,   366,   371,   376,    12,   515,   388,  1203,   400,   392,
     409,   368,   415,   424,  -570,  -570,    47,     6,  -570,   374,
    -570,  -570,  -570,  -570,   327,  -570,  -570,  1203,  1203,   628,
     403,  -570,  -570,    12,   -13,  1307,  -570,   368,   426,  -570,
    -570,   273,   279,  -570,   378,   576,   283,  -570,  -570,   383,
    -570,   435,    12,   451,   106,  -570,  -570,  -570,   446,  -570,
    -570,  -570,   434,  -570,   378,   378,    26,   -29,  -570,   -29,
    -570,  1302,    58,  -570,  -570,   298,  -570,   472,  -570,  1244,
      67,  -570,  -570,  -570,   311,  -570,   312,  -570,   188,  -570,
      -2,  -570,  -570,  -570,  3034,   243,  3034,  1307,  -570,  1307,
    -570,  -570,   475,   481,   480,   630,   912,  1307,  1307,  1307,
     486,  -570,    12,  -570,   630,   957,  1307,  1307,   494,  1307,
    1307,  -570,  -570,  -570,  1307,   336,  1000,   336,  -570,  -570,
    1307,     5,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,   512,   513,    12,  -570,  -570,  1307,
    1307,  1732,  -570,   662,   518,  -570,  -570,  -570,   533,  -570,
     336,  1307,   315,   520,   516,  -570,  -570,    40,   621,   671,
    1350,  -570,   322,   332,   524,   527,   529,   221,   221,   403,
    -570,  -570,   403,   331,  -570,  3034,  -570,  -570,  -570,  1307,
     336,  -570,    35,  1307,   383,   534,   535,   536,  -570,  -570,
    3034,  -570,  -570,  -570,  1307,   336,  -570,    38,   545,  2827,
    -570,   659,   538,  3034,  1860,  -570,  1307,  -570,  -570,  -570,
    -570,  -570,  2038,  2098,  2133,  -570,   547,   548,  -570,  -570,
    2158,  2193,  -570,  2253,  2288,   351,  -570,  2999,  1307,  1307,
    1307,  3034,    14,    18,   373,   110,   110,   302,   372,   551,
    3034,  3034,  1307,  -570,   537,   778,   697,  2313,  -570,   553,
    1307,    39,   671,    43,    46,  1307,   555,  -570,  3034,   758,
    -570,  -570,  -570,  -570,  -570,  -570,  -570,   383,    12,  -570,
    1307,   394,  1307,  -570,   405,   338,  -570,  1307,   552,   440,
     545,  -570,   561,  -570,  -570,   479,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,  -570,
     674,  2827,  -570,  2827,  -570,   578,  2827,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,   460,   668,  1329,  1329,  1329,  -570,
    1307,   668,  1203,  1203,  1203,  1203,  -570,  1307,  1307,  3034,
    3034,  3034,  1000,  -570,  1000,  -570,   565,  1307,  1307,  1307,
    1307,   564,   567,  -570,  1885,  -570,   857,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,  -570,  -570,  -570,   563,   586,  -570,
    1767,   671,   581,  -570,   589,  -570,   671,   591,   587,   594,
     595,  2003,  -570,  -570,   715,  -570,   341,   349,   358,   254,
    -570,  -570,  1307,  -570,  1307,  -570,  -570,  -570,  -570,   479,
    -570,  -570,   615,  -570,   616,   854,  -570,   608,  -570,  -570,
    -570,  -570,  -570,  -570,  1038,  -570,  1096,   246,  -570,   -27,
     305,  1118,  2718,  1145,   714,  -570,  -570,  -570,  -570,  3016,
    -570,  -570,  -570,  2753,  2778,  2871,  2888,  -570,  -570,  -570,
    -570,   730,  -570,   106,  1307,  -570,   610,  -570,   473,  -570,
     671,   671,  1307,  -570,  -570,  -570,  -570,   465,  2348,   612,
     127,   479,   631,   632,   637,   645,   646,   634,  -570,  -570,
    -570,  1203,  -570,  -570,  -570,  1203,  -570,  -570,    12,  -570,
    1203,  1307,  -570,  -570,  -570,  -570,  -570,   613,   103,   671,
    1978,   671,  -570,  -570,   764,   633,   473,   469,  -570,  1307,
    -570,  -570,  -570,  -570,  -570,  -570,   -38,  -570,  -570,  -570,
     635,  -570,  3034,   671,   638,   737,  -570,    50,  -570,   473,
    1307,   671,   779,  -570,  1307,   647,  -570,   648,  -570,  1203,
      20,   671,    44,   650,   652,   788,  2408,   473,  1307,   496,
    -570,  -570,  -570,   801,   658,   671,   660,  -570,   671,   671,
    1307,   661,   794,  2443,  -570,  1307,   671,   671,   473,   671,
     663,   473,  2468,   644,  1307,   667,   669,   499,   473,   799,
     -93,   671,   804,  -570,  1350,  2503,   656,  1307,   671,   811,
    1307,  1350,   473,  1307,   683,   682,  1350,   685,   -77,  1307,
    2563,   689,   814,  2598,  -570,   672,   690,  1307,  1350,  2623,
     692,  -570,  1307,   698,  1350,  -570,   699,   702,   701,   681,
    2658,   686,   705,  -570,  -570,   687,  1350,   706,  1350,  -570,
    1350,   712,   691,   713,   717,  -570,  1350,  -570,  -570,   722,
    -570
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -570,  -570,  -570,  -570,  -570,  -570,  -570,   822,   829,   827,
    -570,   688,  -570,  -570,    16,   733,  -570,  -570,  -570,  -570,
     511,  -570,  -570,  -570,   292,  -570,   295,  -420,  -570,   289,
    -570,  -570,  -570,   105,  -569,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,  -482,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,   840,   749,  -570,   -43,   -42,   -14,    -7,   -16,
    -570,  -570,   549,  -570,   -32,   -30,   -12,   -11,    -1,  -570,
    -570,  -570,  -184,  -103,   575,  -570,  -570,  -234,  -570,  -570,
     670,   411,   375,   381,    -9,   -78,   -97,  -570,  -570,  -570,
    -570,  -570,   429,  -570,   436,   342,  -570,  -570,  -570,  -570,
    -570,  -570,   423,   425,  -570,  -570,   360,   357,  -570,  -570,
    -570,  -570,   278,  -287,    -6,  -570,   -39,   636,    51,  -442,
    -570,  -570,   532,   431,  -570,  -570,  -570,  -570,  -570,  -570,
    -570,  -570,  -570,  -570,  -570,  -570,  -390,  -265,  -570,  -367,
    -505,  -570,  -570,  -570,   132,  -304,  -570,   214,   -50,   456,
    -457,   -59,   -40,  1033,    13,  -570,   -17,    53,   703,   130,
     137,  -370,  -570
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -368
static const yytype_int16 yytable[] =
{
      67,   395,   221,   123,   124,   350,   679,   167,   211,    54,
     226,    54,   561,    25,   129,    54,   130,   149,   151,   153,
     155,   157,   159,   161,   163,   165,   168,    25,  -355,   321,
     127,   322,   125,   200,   131,   132,   326,     2,   350,   126,
     660,    54,   444,    43,   199,   133,   444,   444,   541,   200,
      54,   401,   402,   403,   658,    89,   429,   398,   813,   542,
     199,    54,   466,   467,   468,   227,   202,   663,   457,   448,
      54,   481,   482,   483,   525,   542,    11,   454,    72,   861,
     215,    73,   202,   432,    98,    12,   228,   223,   195,   220,
     622,   455,   458,    13,   624,   878,   201,   147,   659,    14,
     148,     3,   814,     4,   806,   330,    44,    45,   755,   444,
     739,   794,   201,    54,    55,    56,   748,   331,     5,    17,
     782,   783,   784,   785,   456,   251,   117,   208,  -121,   253,
     254,   255,   256,   257,   258,   259,   260,   261,   262,   263,
     264,   265,   266,   267,   268,   269,   270,   271,   272,   273,
     274,   652,   351,   276,   720,  -355,   406,     6,   359,   430,
     708,   772,   773,    26,    27,   623,    66,    57,    58,   625,
      59,   682,   748,   684,   332,   352,   445,   526,    27,   202,
     192,   360,   206,   193,   544,   562,   446,    23,   570,   651,
     298,   375,   542,   656,   825,   202,   223,   404,   223,   144,
      45,   123,   124,   223,    33,   333,   320,   374,   469,   201,
     168,   376,   129,   373,   130,   737,    35,   484,   344,   345,
     207,   188,   189,   190,   334,   201,   366,   777,   127,   200,
     125,   371,   131,   132,   390,   442,    89,   126,   372,    36,
     384,   452,   453,   133,   772,   773,    60,    61,    62,    54,
      55,    56,   710,    38,   711,   754,    63,    64,   754,   754,
      65,   726,   202,   200,    66,    98,    39,   202,   696,   461,
     202,   461,    40,   753,   428,   431,    49,   655,   657,   405,
     407,   393,   200,   296,   297,   782,   783,   784,   785,   208,
     202,   202,   201,   199,   489,   147,   202,   201,   246,   819,
     201,   412,   413,    57,    58,    47,    59,   117,    54,    55,
      56,   465,   440,   251,   312,   202,   252,   313,   694,   480,
     201,   201,   805,  -367,   703,   192,   201,   696,   315,    41,
     846,   316,   756,   767,   317,   470,   373,   493,   176,   494,
     775,  -129,   200,   485,   415,   201,    25,   502,   503,   504,
      51,   524,   867,   507,   371,   323,   510,   511,   324,   513,
     514,   372,    57,    58,   517,    59,   521,   194,   178,   202,
     168,   203,   886,   666,   667,   202,   200,   208,   202,   555,
     556,   216,    60,    61,    62,   417,    54,   529,   225,   530,
     531,   728,    63,    64,   327,   251,    65,   313,   492,   201,
      66,   537,  -319,  -321,   419,   201,   421,   192,   201,   202,
     315,   328,   343,   414,   329,   324,   186,   187,   188,   189,
     190,   824,   435,   245,   774,   329,  -367,   423,   436,   517,
     247,   329,   439,   465,   250,   329,   275,   604,   840,   201,
     309,    60,    61,    62,   517,   318,   847,   471,   480,   850,
     472,    63,    64,   319,   416,    65,   168,   565,   768,    66,
     486,   668,   488,   487,   538,   192,   776,   313,   868,   325,
     366,   550,   802,   418,   324,   420,   409,   410,   619,   620,
     621,   551,   559,   560,   324,   530,   531,   864,   674,   192,
     734,   192,   634,   329,   871,   815,   422,   381,   735,   876,
     650,   329,   616,   617,   797,   661,   799,   736,   200,   346,
     313,   887,   557,   832,   385,   558,   387,   892,   391,   199,
     465,   388,   465,   408,   626,   251,   389,   480,   810,   901,
     480,   903,   202,   904,   849,   394,   817,   852,   392,   909,
     223,   669,   396,   665,   859,   671,   672,   826,    54,    55,
      56,   186,   187,   188,   189,   190,   673,   560,   872,   397,
     838,   433,   201,   688,   841,   399,   168,   168,   168,   688,
     702,   201,   848,   689,   400,   690,   352,   517,   709,   689,
     438,   690,   521,   451,   521,   441,   862,   713,   714,   715,
     716,   677,   617,   691,   692,   202,   202,   202,   202,   691,
     692,   443,    57,    58,   693,    59,   704,   705,   706,   707,
     693,   687,   251,   573,   772,   773,   778,   779,   450,   202,
     803,   804,   473,   169,   495,   201,   201,   201,   201,   497,
     665,   496,   517,   498,   738,   505,   170,   576,   577,   578,
     579,   580,   581,   512,   174,   175,   176,   834,   835,   201,
     858,   542,   177,   353,   361,   627,   629,   168,   700,   701,
     168,   168,   628,   630,   527,   528,   534,   535,   536,   414,
     415,   539,   540,   543,   444,   552,   178,   202,   553,   202,
     554,    60,    61,    62,   770,   566,   202,   567,   568,   601,
     752,    63,    64,   602,   635,    65,   609,   610,   474,    66,
     633,   647,   649,    85,   662,   474,   680,   201,   200,   201,
     416,   417,   678,   685,   712,   717,   201,    90,   718,   790,
     722,   792,   184,   185,   186,   187,   188,   189,   190,   418,
     419,   420,   421,   542,   202,  -132,   723,   730,   202,   517,
     727,   202,   729,   202,   731,   788,    99,   732,   733,   789,
     740,   741,   422,   423,   791,   104,  -132,   749,   760,   766,
     816,   278,   771,   781,   201,   793,   -79,   -80,   201,   279,
     280,   201,   -81,   201,   281,   282,   283,   113,   833,   284,
     -82,   786,   285,   787,   800,   801,   809,   117,   811,   812,
     842,  -267,   202,    85,   286,   287,   288,   821,   820,   818,
     828,   289,   829,   822,   855,    88,    89,    90,   830,   836,
     837,   839,   843,    85,   844,   851,   854,   169,   856,   860,
     870,   857,   201,   873,   863,    88,    89,    90,   866,   879,
     170,   869,   874,   875,   882,    98,    99,   877,   881,   885,
     176,    34,   890,   889,   884,   104,   177,   290,   291,   891,
     893,   894,   895,   896,   899,    98,    99,   902,   898,   900,
     278,   905,   907,   906,    48,   104,   908,   113,   279,   280,
     178,   910,    74,   281,   282,   283,   244,   117,   284,   490,
     292,   285,   277,   681,   293,   686,  -267,   113,   683,   145,
     294,   808,    85,   286,   287,   288,   248,   117,   462,   437,
     289,   563,   670,   632,    88,    89,    90,   295,   631,   564,
     571,   349,    66,   572,   721,   278,   184,   185,   186,   187,
     188,   189,   190,   279,   280,   676,   508,   675,   281,   282,
     283,   296,   297,   284,    98,    99,   285,   769,   500,   611,
     569,   411,   823,     0,   104,   358,   290,   291,   286,   287,
     288,     0,     0,     0,     0,   289,     0,     0,     0,     0,
     278,     0,     0,     0,     0,     0,   113,     0,   279,   280,
       0,     0,     0,   281,   282,   283,   117,     0,   284,   292,
       0,   285,     0,   293,     0,     0,     0,     0,   573,   294,
       0,     0,     0,   286,   287,   288,     0,     0,     0,     0,
     289,   290,   291,    54,    55,    56,   295,   509,     0,     0,
       0,    66,   742,   743,   744,   745,   580,   581,     0,     0,
       0,     0,     0,     0,     0,   518,     0,     0,     0,     0,
     296,   297,     0,     0,   292,     0,     0,     0,   293,     0,
       0,   278,     0,     0,   294,     0,   290,   291,     0,   279,
     280,     0,     0,     0,   281,   282,   283,    57,    58,   284,
      59,   295,   285,     0,   750,     0,    66,     0,     0,     0,
     519,     0,     0,     0,   286,   287,   288,     0,     0,   292,
     520,   289,     0,   293,     0,   296,   297,     0,     0,   294,
     150,   152,   154,   156,   158,   160,   162,   164,     0,   278,
       0,     0,     0,     0,     0,     0,   295,   279,   280,     0,
       0,    66,   281,   282,   283,     0,     0,   284,     0,     0,
     285,    54,    55,    56,     0,     0,     0,   290,   291,     0,
     296,   297,   286,   287,   288,     0,    60,    61,    62,   289,
     696,     0,     0,     0,     0,   757,    63,    64,   278,     0,
      65,     0,     0,     0,    66,     0,   279,   280,     0,     0,
     292,   281,   282,   283,   293,     0,   284,     0,     0,   285,
     294,     0,     0,     0,     0,    57,    58,     0,    59,     0,
       0,   286,   287,   288,     0,   290,   291,   295,   289,     0,
       0,     0,    66,     0,     0,   759,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   278,     0,     0,     0,
       0,   296,   297,     0,   279,   280,     0,     0,   292,   281,
     282,   283,   293,     0,   284,     0,     0,   285,   294,     0,
       0,   751,     0,     0,   290,   291,     0,     0,     0,   286,
     287,   288,     0,     0,     0,   295,   289,    54,    55,    56,
      66,     0,     0,     0,    60,    61,    62,     0,     0,     0,
       0,     0,     0,     0,    63,    64,     0,   292,    65,   296,
     297,   293,    66,     0,     0,     0,     0,   294,     0,     0,
       0,     0,     0,     0,     0,   414,   415,     0,     0,     0,
       0,     0,   290,   291,   295,     0,     0,     0,     0,    66,
       0,    57,    58,     0,    59,    54,    55,    56,     0,     0,
      54,    55,    56,     0,     0,     0,     0,     0,   296,   297,
       0,     0,     0,     0,     0,   292,   416,   417,     0,   293,
       0,     0,    54,    55,    56,   294,     0,     0,    54,   166,
      56,     0,     0,   414,   415,   418,   419,   420,   421,     0,
       0,   696,   295,    54,    55,    56,     0,    66,     0,    57,
      58,     0,    59,     0,    57,    58,     0,    59,   422,   423,
       0,     0,     0,     0,     0,     0,   296,   297,     0,     0,
      60,    61,    62,     0,   416,   417,    57,    58,     0,    59,
      63,    64,    57,    58,    65,    59,     0,   474,    66,     0,
       0,     0,     0,   418,   419,   420,   421,    57,    58,     0,
      59,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   422,   423,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    60,    61,
      62,     0,     0,    60,    61,    62,     0,     0,    63,    64,
       0,     0,    65,    63,    64,     0,    66,    65,     0,     0,
       0,    66,     0,     0,     0,    60,    61,    62,     0,     0,
       0,    60,    61,    62,     0,    63,    64,     0,     0,    65,
       0,    63,    64,    66,     0,    65,    60,    61,    62,    66,
       0,     0,     0,     0,     0,    75,    63,    64,   227,     0,
     545,    76,    77,    78,    66,    79,    80,    81,     0,     0,
       0,    82,     0,     0,     0,    83,     0,     0,     0,   228,
      84,     0,     0,     0,     0,     0,     0,    85,     0,     0,
       0,    86,     0,     0,   229,   230,   231,     0,    87,    88,
      89,    90,     0,     0,   232,   233,   234,   235,   236,   237,
       0,     0,     0,     0,   238,     0,     0,     0,   239,   240,
      91,     0,     0,    92,    93,    94,    95,    96,    97,    98,
      99,   100,     0,     0,     0,     0,   101,   102,   103,   104,
       0,     0,     0,   105,   106,     0,   107,   108,   109,     0,
       0,   110,     0,     0,     0,     0,     0,     0,     0,   111,
     112,   113,   114,   115,   116,     0,     0,    75,     0,     0,
       0,   117,     0,    76,    77,    78,     0,    79,    80,    81,
       0,   118,   119,    82,     0,     0,     0,    83,     0,     0,
       0,     0,   367,     0,     0,     0,     0,     0,     0,    85,
       0,   169,     0,    86,     0,     0,     0,     0,     0,     0,
      87,    88,    89,    90,   170,   171,     0,     0,     0,     0,
     172,   173,   174,   175,   176,     0,     0,     0,     0,     0,
     177,     0,    91,     0,     0,    92,    93,    94,    95,    96,
      97,    98,    99,   100,     0,     0,     0,     0,   101,   102,
     103,   104,     0,     0,   178,   105,   106,     0,   107,   108,
     109,     0,     0,   110,     0,     0,     0,     0,     0,     0,
       0,   111,   112,   113,   114,   115,   116,     0,     0,     0,
       0,     0,     0,   117,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   118,   119,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   169,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   191,     0,
     170,   171,     0,     0,     0,     0,   172,   173,   174,   175,
     176,     0,   169,     0,     0,     0,   177,     0,     0,     0,
       0,     0,     0,     0,     0,   170,   171,     0,     0,     0,
       0,   172,   173,   174,   175,   176,     0,     0,     0,     0,
     178,   177,     0,     0,     0,     0,     0,   169,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     170,   171,     0,     0,     0,   178,   172,   173,   174,   175,
     176,     0,     0,     0,     0,     0,   177,     0,     0,     0,
       0,   179,   379,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,     0,     0,     0,     0,     0,     0,     0,
     178,     0,     0,     0,   380,   169,   179,   532,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,   170,   171,
       0,     0,     0,     0,   172,   173,   174,   175,   176,   533,
       0,     0,     0,     0,   177,     0,     0,     0,     0,     0,
     169,   179,     0,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   170,   171,     0,   724,     0,   178,   172,
     173,   174,   175,   176,   725,   169,     0,     0,     0,   177,
       0,     0,     0,     0,     0,     0,     0,     0,   170,   171,
       0,     0,     0,     0,   172,   173,   174,   175,   176,     0,
       0,     0,     0,   178,   177,     0,     0,     0,     0,   179,
       0,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,     0,     0,     0,     0,     0,     0,     0,   178,     0,
       0,     0,   378,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   179,     0,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   603,   169,   179,
       0,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   170,   171,     0,     0,     0,     0,   172,   173,   174,
     175,   176,   719,   169,     0,     0,     0,   177,     0,     0,
       0,     0,     0,     0,     0,     0,   170,   171,     0,     0,
       0,     0,   172,   173,   174,   175,   176,     0,     0,     0,
       0,   178,   177,     0,     0,     0,     0,     0,   169,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   170,   171,     0,     0,     0,   178,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,     0,
       0,     0,   179,     0,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,     0,     0,     0,     0,
       0,   178,     0,     0,     0,   798,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   249,     0,     0,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   606,
       0,   178,   172,   173,   174,   175,   176,     0,   169,     0,
       0,     0,   177,     0,     0,     0,     0,     0,     0,     0,
       0,   170,   171,     0,     0,     0,     0,   172,   173,   174,
     175,   176,     0,     0,     0,     0,   178,   177,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   607,
       0,   178,   172,   173,   174,   175,   176,     0,     0,     0,
       0,     0,   177,     0,     0,     0,     0,   179,     0,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,     0,     0,     0,   608,     0,   178,     0,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   612,
       0,     0,   172,   173,   174,   175,   176,     0,     0,     0,
       0,     0,   177,     0,     0,     0,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   613,     0,   178,   172,   173,   174,
     175,   176,     0,   169,     0,     0,     0,   177,     0,     0,
       0,     0,     0,     0,     0,     0,   170,   171,     0,     0,
       0,     0,   172,   173,   174,   175,   176,     0,     0,     0,
       0,   178,   177,     0,     0,     0,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   614,     0,   178,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,     0,
       0,     0,   179,     0,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,     0,     0,     0,   615,
       0,   178,     0,     0,     0,     0,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   648,     0,     0,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   780,
       0,   178,   172,   173,   174,   175,   176,     0,   169,     0,
       0,     0,   177,     0,     0,     0,     0,     0,     0,     0,
       0,   170,   171,     0,     0,     0,     0,   172,   173,   174,
     175,   176,     0,     0,     0,     0,   178,   177,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   831,
       0,   178,   172,   173,   174,   175,   176,     0,     0,     0,
       0,     0,   177,     0,     0,     0,     0,   179,     0,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,     0,     0,     0,   845,     0,   178,     0,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   853,
       0,     0,   172,   173,   174,   175,   176,     0,     0,     0,
       0,     0,   177,     0,     0,     0,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   865,     0,   178,   172,   173,   174,
     175,   176,     0,   169,     0,     0,     0,   177,     0,     0,
       0,     0,     0,     0,     0,     0,   170,   171,     0,     0,
       0,     0,   172,   173,   174,   175,   176,     0,     0,     0,
       0,   178,   177,     0,     0,     0,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   880,     0,   178,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,     0,
       0,     0,   179,     0,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,     0,     0,     0,   883,
       0,   178,     0,     0,     0,     0,     0,   179,   169,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,   170,   171,     0,   888,     0,     0,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,     0,
       0,     0,   179,   169,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,   170,   171,     0,   897,
       0,   178,   172,   173,   174,   175,   176,     0,   169,     0,
       0,     0,   177,     0,     0,     0,     0,     0,     0,     0,
       0,   170,   171,     0,     0,     0,     0,   172,   173,   174,
     175,   176,     0,     0,     0,     0,   178,   177,     0,     0,
       0,     0,   179,     0,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,     0,   758,     0,     0,
       0,   178,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   179,     0,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,     0,   762,     0,     0,     0,     0,     0,     0,     0,
       0,   169,   179,     0,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   170,   171,     0,   763,   169,     0,
     172,   173,   174,   175,   176,     0,     0,     0,     0,     0,
     177,   170,   171,     0,     0,     0,   169,   172,   173,   174,
     175,   176,     0,     0,     0,     0,     0,   177,     0,   170,
     171,   573,     0,     0,   178,   172,   173,   174,   175,   176,
     574,     0,     0,     0,     0,   177,     0,   575,     0,     0,
       0,   178,     0,     0,     0,   576,   577,   578,   579,   580,
     581,   582,   583,   584,   585,   586,   587,   588,   589,   178,
       0,     0,     0,     0,     0,   179,     0,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,     0,     0,     0,
     764,     0,   179,     0,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     0,     0,     0,   765,     0,   169,
     179,   377,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   170,   171,     0,     0,   169,     0,   172,   173,
     174,   175,   176,     0,     0,     0,     0,     0,   177,   170,
     171,     0,     0,     0,   169,   172,   173,   174,   175,   176,
       0,     0,     0,     0,     0,   177,     0,   170,   171,     0,
       0,     0,   178,   172,   173,   174,   175,   176,     0,     0,
       0,     0,     0,   177,     0,     0,     0,     0,     0,   178,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   178,     0,     0,
       0,     0,     0,   179,   618,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,     0,     0,     0,     0,     0,
     179,   761,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,     0,     0,     0,     0,     0,   169,   179,     0,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
     170,   171,   169,     0,     0,     0,     0,   173,   174,   175,
     176,     0,     0,   169,     0,   170,   177,     0,     0,     0,
       0,     0,   173,   174,   175,   176,   170,     0,   169,     0,
       0,   177,     0,   173,   174,   175,   176,     0,     0,     0,
     178,   170,   177,     0,     0,     0,     0,     0,     0,   174,
     175,   176,     0,     0,     0,   178,     0,   177,     0,     0,
       0,     0,     0,     0,     0,     0,   178,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   178,     0,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,     0,     0,     0,     0,     0,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,     0,     0,
     182,   183,   184,   185,   186,   187,   188,   189,   190,     0,
       0,     0,     0,     0,     0,     0,   183,   184,   185,   186,
     187,   188,   189,   190
};

static const yytype_int16 yycheck[] =
{
      40,   288,    99,    46,    46,     3,   575,    66,    86,     3,
     113,     3,   469,     3,    46,     3,    46,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,     3,     8,   213,
      46,   215,    46,    83,    46,    46,   220,     0,     3,    46,
     545,     3,     3,    27,    83,    46,     3,     3,     8,    99,
       3,     4,     5,     6,     8,    48,    69,   291,     8,   152,
      99,     3,     4,     5,     6,    67,    83,   549,    97,   334,
       3,     4,     5,     6,    69,   152,     3,    51,   152,   172,
      89,   155,    99,   317,    77,     3,    88,   104,    72,    98,
      76,    65,   121,     3,    76,   172,    83,   152,    52,    34,
     155,    64,    52,    66,   142,    31,   151,   152,   135,     3,
     679,     8,    99,     3,     4,     5,   685,    43,    81,   150,
     158,   159,   160,   161,    98,   152,   119,   156,   154,   169,
     170,   171,   172,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   541,   150,   193,   636,    52,   150,   120,   150,   172,
     617,   141,   142,   153,   154,   151,   154,    57,    58,   151,
      60,   591,   741,   593,   100,   173,    70,   172,   154,   196,
     153,   173,    49,   156,   449,   150,    80,     3,   150,   150,
     196,   250,   152,   150,   150,   212,   213,   150,   215,   151,
     152,   244,   244,   220,   149,   131,   212,   247,   150,   196,
     250,   251,   244,   245,   244,   672,   150,   150,   227,   228,
      87,   143,   144,   145,   150,   212,   243,   732,   244,   279,
     244,   245,   244,   244,   284,   332,    48,   244,   245,     3,
     279,   344,   345,   244,   141,   142,   136,   137,   138,     3,
       4,     5,   622,   154,   624,   697,   146,   147,   700,   701,
     150,   651,   279,   313,   154,    77,   149,   284,    22,   347,
     287,   349,   156,    27,   313,   315,   154,   542,   543,   296,
     297,   287,   332,   173,   174,   158,   159,   160,   161,   156,
     307,   308,   279,   332,   106,   152,   313,   284,   155,   804,
     287,   307,   308,    57,    58,   149,    60,   119,     3,     4,
       5,   351,   329,   152,   149,   332,   155,   152,   605,   359,
     307,   308,   779,    69,   611,   153,   313,    22,   156,     3,
     835,   149,    27,   723,   152,   352,   368,   377,    63,   379,
     730,   172,   392,   360,    42,   332,     3,   387,   388,   389,
       3,   410,   857,   392,   368,   149,   396,   397,   152,   399,
     400,   368,    57,    58,   404,    60,   406,   150,    93,   386,
     410,     3,   877,   557,   558,   392,   426,   156,   395,   457,
     458,     3,   136,   137,   138,    83,     3,   426,     3,   429,
     430,   656,   146,   147,   149,   152,   150,   152,   155,   386,
     154,   441,   149,   150,   102,   392,   104,   153,   395,   426,
     156,   149,   149,    41,   152,   152,   141,   142,   143,   144,
     145,   811,   149,   149,   728,   152,   172,   125,   149,   469,
     172,   152,   149,   473,   154,   152,     3,   496,   828,   426,
     150,   136,   137,   138,   484,     4,   836,   149,   488,   839,
     152,   146,   147,     3,    82,   150,   496,   474,   723,   154,
     149,   558,   150,   152,   149,   153,   731,   152,   858,   156,
     487,   149,   776,   101,   152,   103,   149,   150,   518,   519,
     520,   149,   151,   152,   152,   525,   526,   854,   150,   153,
     149,   153,   532,   152,   861,   799,   124,   151,   149,   866,
     540,   152,   151,   152,   769,   545,   771,   149,   558,   150,
     152,   878,   459,   817,   135,   462,   150,   884,     3,   558,
     560,   150,   562,   149,   151,   152,   150,   567,   793,   896,
     570,   898,   549,   900,   838,   135,   801,   841,   150,   906,
     557,   558,   150,   549,   848,   151,   152,   812,     3,     4,
       5,   141,   142,   143,   144,   145,   151,   152,   862,   150,
     825,   135,   549,   605,   829,   150,   606,   607,   608,   611,
     610,   558,   837,   605,   150,   605,   173,   617,   618,   611,
       4,   611,   622,   149,   624,   150,   851,   627,   628,   629,
     630,   151,   152,   605,   605,   612,   613,   614,   615,   611,
     611,   150,    57,    58,   605,    60,   612,   613,   614,   615,
     611,   151,   152,   134,   141,   142,   151,   152,   172,   636,
     151,   152,   150,    40,   149,   612,   613,   614,   615,   149,
     636,   150,   672,     3,   674,   149,    53,   158,   159,   160,
     161,   162,   163,   149,    61,    62,    63,   151,   152,   636,
     151,   152,    69,   242,   243,   525,   526,   697,   607,   608,
     700,   701,   525,   526,   152,   152,     4,   149,   135,    41,
      42,   151,   156,    52,     3,   151,    93,   694,   151,   696,
     151,   136,   137,   138,   724,   151,   703,   152,   152,    30,
     696,   146,   147,   155,   157,   150,   149,   149,   153,   154,
     149,     4,   149,    35,   149,   153,    32,   694,   758,   696,
      82,    83,   151,   135,   149,   151,   703,    49,   151,   758,
     157,   761,   139,   140,   141,   142,   143,   144,   145,   101,
     102,   103,   104,   152,   751,    67,   150,   150,   755,   779,
     151,   758,   151,   760,   150,   751,    78,   152,    33,   755,
     135,   135,   124,   125,   760,    87,    88,   149,    44,    29,
     800,     3,   152,   151,   751,   152,   135,   135,   755,    11,
      12,   758,   135,   760,    16,    17,    18,   109,   818,    21,
     135,   135,    24,   149,    20,   152,   151,   119,   150,    52,
     830,    33,   809,    35,    36,    37,    38,   149,   151,    20,
     150,    43,   150,   809,   844,    47,    48,    49,    20,     8,
     152,   151,   151,    35,    20,   152,   172,    40,   151,    20,
     860,   152,   809,   863,    20,    47,    48,    49,   172,   869,
      53,    20,   149,   151,    20,    77,    78,   152,   149,   149,
      63,    19,   882,   151,   172,    87,    69,    89,    90,   151,
     151,   149,   151,   172,   149,    77,    78,   151,   172,   172,
       3,   149,   149,   172,    35,    87,   149,   109,    11,    12,
      93,   149,    45,    16,    17,    18,   143,   119,    21,   368,
     122,    24,   194,   591,   126,   596,    29,   109,   593,    49,
     132,   786,    35,    36,    37,    38,   147,   119,   349,   324,
      43,   472,   560,   528,    47,    48,    49,   149,   527,   473,
     487,   241,   154,   488,   636,     3,   139,   140,   141,   142,
     143,   144,   145,    11,    12,   568,   394,   567,    16,    17,
      18,   173,   174,    21,    77,    78,    24,   723,    26,   508,
     484,   305,   810,    -1,    87,   242,    89,    90,    36,    37,
      38,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    -1,
       3,    -1,    -1,    -1,    -1,    -1,   109,    -1,    11,    12,
      -1,    -1,    -1,    16,    17,    18,   119,    -1,    21,   122,
      -1,    24,    -1,   126,    -1,    -1,    -1,    -1,   134,   132,
      -1,    -1,    -1,    36,    37,    38,    -1,    -1,    -1,    -1,
      43,    89,    90,     3,     4,     5,   149,    50,    -1,    -1,
      -1,   154,   158,   159,   160,   161,   162,   163,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    25,    -1,    -1,    -1,    -1,
     173,   174,    -1,    -1,   122,    -1,    -1,    -1,   126,    -1,
      -1,     3,    -1,    -1,   132,    -1,    89,    90,    -1,    11,
      12,    -1,    -1,    -1,    16,    17,    18,    57,    58,    21,
      60,   149,    24,    -1,    26,    -1,   154,    -1,    -1,    -1,
      70,    -1,    -1,    -1,    36,    37,    38,    -1,    -1,   122,
      80,    43,    -1,   126,    -1,   173,   174,    -1,    -1,   132,
      57,    58,    59,    60,    61,    62,    63,    64,    -1,     3,
      -1,    -1,    -1,    -1,    -1,    -1,   149,    11,    12,    -1,
      -1,   154,    16,    17,    18,    -1,    -1,    21,    -1,    -1,
      24,     3,     4,     5,    -1,    -1,    -1,    89,    90,    -1,
     173,   174,    36,    37,    38,    -1,   136,   137,   138,    43,
      22,    -1,    -1,    -1,    -1,    27,   146,   147,     3,    -1,
     150,    -1,    -1,    -1,   154,    -1,    11,    12,    -1,    -1,
     122,    16,    17,    18,   126,    -1,    21,    -1,    -1,    24,
     132,    -1,    -1,    -1,    -1,    57,    58,    -1,    60,    -1,
      -1,    36,    37,    38,    -1,    89,    90,   149,    43,    -1,
      -1,    -1,   154,    -1,    -1,    50,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     3,    -1,    -1,    -1,
      -1,   173,   174,    -1,    11,    12,    -1,    -1,   122,    16,
      17,    18,   126,    -1,    21,    -1,    -1,    24,   132,    -1,
      -1,   135,    -1,    -1,    89,    90,    -1,    -1,    -1,    36,
      37,    38,    -1,    -1,    -1,   149,    43,     3,     4,     5,
     154,    -1,    -1,    -1,   136,   137,   138,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   146,   147,    -1,   122,   150,   173,
     174,   126,   154,    -1,    -1,    -1,    -1,   132,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    41,    42,    -1,    -1,    -1,
      -1,    -1,    89,    90,   149,    -1,    -1,    -1,    -1,   154,
      -1,    57,    58,    -1,    60,     3,     4,     5,    -1,    -1,
       3,     4,     5,    -1,    -1,    -1,    -1,    -1,   173,   174,
      -1,    -1,    -1,    -1,    -1,   122,    82,    83,    -1,   126,
      -1,    -1,     3,     4,     5,   132,    -1,    -1,     3,     4,
       5,    -1,    -1,    41,    42,   101,   102,   103,   104,    -1,
      -1,    22,   149,     3,     4,     5,    -1,   154,    -1,    57,
      58,    -1,    60,    -1,    57,    58,    -1,    60,   124,   125,
      -1,    -1,    -1,    -1,    -1,    -1,   173,   174,    -1,    -1,
     136,   137,   138,    -1,    82,    83,    57,    58,    -1,    60,
     146,   147,    57,    58,   150,    60,    -1,   153,   154,    -1,
      -1,    -1,    -1,   101,   102,   103,   104,    57,    58,    -1,
      60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   124,   125,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   136,   137,
     138,    -1,    -1,   136,   137,   138,    -1,    -1,   146,   147,
      -1,    -1,   150,   146,   147,    -1,   154,   150,    -1,    -1,
      -1,   154,    -1,    -1,    -1,   136,   137,   138,    -1,    -1,
      -1,   136,   137,   138,    -1,   146,   147,    -1,    -1,   150,
      -1,   146,   147,   154,    -1,   150,   136,   137,   138,   154,
      -1,    -1,    -1,    -1,    -1,     3,   146,   147,    67,    -1,
     150,     9,    10,    11,   154,    13,    14,    15,    -1,    -1,
      -1,    19,    -1,    -1,    -1,    23,    -1,    -1,    -1,    88,
      28,    -1,    -1,    -1,    -1,    -1,    -1,    35,    -1,    -1,
      -1,    39,    -1,    -1,   103,   104,   105,    -1,    46,    47,
      48,    49,    -1,    -1,   113,   114,   115,   116,   117,   118,
      -1,    -1,    -1,    -1,   123,    -1,    -1,    -1,   127,   128,
      68,    -1,    -1,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    -1,    -1,    -1,    -1,    84,    85,    86,    87,
      -1,    -1,    -1,    91,    92,    -1,    94,    95,    96,    -1,
      -1,    99,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   107,
     108,   109,   110,   111,   112,    -1,    -1,     3,    -1,    -1,
      -1,   119,    -1,     9,    10,    11,    -1,    13,    14,    15,
      -1,   129,   130,    19,    -1,    -1,    -1,    23,    -1,    -1,
      -1,    -1,    28,    -1,    -1,    -1,    -1,    -1,    -1,    35,
      -1,    40,    -1,    39,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    48,    49,    53,    54,    -1,    -1,    -1,    -1,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    68,    -1,    -1,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    -1,    -1,    -1,    -1,    84,    85,
      86,    87,    -1,    -1,    93,    91,    92,    -1,    94,    95,
      96,    -1,    -1,    99,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   107,   108,   109,   110,   111,   112,    -1,    -1,    -1,
      -1,    -1,    -1,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   129,   130,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    40,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   157,    -1,
      53,    54,    -1,    -1,    -1,    -1,    59,    60,    61,    62,
      63,    -1,    40,    -1,    -1,    -1,    69,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    53,    54,    -1,    -1,    -1,
      -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,
      93,    69,    -1,    -1,    -1,    -1,    -1,    40,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      53,    54,    -1,    -1,    -1,    93,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,
      -1,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,   157,    40,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    53,    54,
      -1,    -1,    -1,    -1,    59,    60,    61,    62,    63,   157,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      40,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    53,    54,    -1,   149,    -1,    93,    59,
      60,    61,    62,    63,   157,    40,    -1,    -1,    -1,    69,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    53,    54,
      -1,    -1,    -1,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    93,    69,    -1,    -1,    -1,    -1,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,
      -1,    -1,   157,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   157,    40,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    53,    54,    -1,    -1,    -1,    -1,    59,    60,    61,
      62,    63,   157,    40,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    53,    54,    -1,    -1,
      -1,    -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    93,    69,    -1,    -1,    -1,    -1,    -1,    40,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    53,    54,    -1,    -1,    -1,    93,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,   134,    -1,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    93,    -1,    -1,    -1,   157,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    93,    59,    60,    61,    62,    63,    -1,    40,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    53,    54,    -1,    -1,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    93,    69,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    93,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,   134,    -1,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    -1,    -1,    -1,   151,    -1,    93,    -1,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    93,    59,    60,    61,
      62,    63,    -1,    40,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    53,    54,    -1,    -1,
      -1,    -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    93,    69,    -1,    -1,    -1,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    93,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,   134,    -1,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,   151,
      -1,    93,    -1,    -1,    -1,    -1,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    93,    59,    60,    61,    62,    63,    -1,    40,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    53,    54,    -1,    -1,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    93,    69,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    93,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,   134,    -1,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    -1,    -1,    -1,   151,    -1,    93,    -1,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    93,    59,    60,    61,
      62,    63,    -1,    40,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    53,    54,    -1,    -1,
      -1,    -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    93,    69,    -1,    -1,    -1,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    93,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,   134,    -1,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,   151,
      -1,    93,    -1,    -1,    -1,    -1,    -1,   134,    40,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    53,    54,    -1,   151,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,   134,    40,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    53,    54,    -1,   151,
      -1,    93,    59,    60,    61,    62,    63,    -1,    40,    -1,
      -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    53,    54,    -1,    -1,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    93,    69,    -1,    -1,
      -1,    -1,   134,    -1,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    -1,   149,    -1,    -1,
      -1,    93,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   134,    -1,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    -1,   149,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    40,   134,    -1,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    53,    54,    -1,   149,    40,    -1,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    53,    54,    -1,    -1,    -1,    40,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    53,
      54,   134,    -1,    -1,    93,    59,    60,    61,    62,    63,
     143,    -1,    -1,    -1,    -1,    69,    -1,   150,    -1,    -1,
      -1,    93,    -1,    -1,    -1,   158,   159,   160,   161,   162,
     163,   164,   165,   166,   167,   168,   169,   170,   171,    93,
      -1,    -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    -1,    -1,
     149,    -1,   134,    -1,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    -1,    -1,    -1,   149,    -1,    40,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,    53,    54,    -1,    -1,    40,    -1,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    53,
      54,    -1,    -1,    -1,    40,    59,    60,    61,    62,    63,
      -1,    -1,    -1,    -1,    -1,    69,    -1,    53,    54,    -1,
      -1,    -1,    93,    59,    60,    61,    62,    63,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,    93,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,
      -1,    -1,    -1,   134,   135,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,
     134,   135,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,    -1,    -1,    -1,    -1,    -1,    40,   134,    -1,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
      53,    54,    40,    -1,    -1,    -1,    -1,    60,    61,    62,
      63,    -1,    -1,    40,    -1,    53,    69,    -1,    -1,    -1,
      -1,    -1,    60,    61,    62,    63,    53,    -1,    40,    -1,
      -1,    69,    -1,    60,    61,    62,    63,    -1,    -1,    -1,
      93,    53,    69,    -1,    -1,    -1,    -1,    -1,    -1,    61,
      62,    63,    -1,    -1,    -1,    93,    -1,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    93,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    -1,    -1,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   138,   139,   140,   141,
     142,   143,   144,   145
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   176,     0,    64,    66,    81,   120,   177,   178,   179,
     192,     3,     3,     3,    34,   221,   222,   150,   182,   180,
     193,   223,   224,     3,   220,     3,   153,   154,   183,   184,
     186,   187,   189,   149,   182,   150,     3,   225,   154,   149,
     156,     3,   188,   189,   151,   152,   190,   149,   183,   154,
     226,     3,   227,   228,     3,     4,     5,    57,    58,    60,
     136,   137,   138,   146,   147,   150,   154,   327,   328,   329,
     331,   185,   152,   155,   184,     3,     9,    10,    11,    13,
      14,    15,    19,    23,    28,    35,    39,    46,    47,    48,
      49,    68,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    84,    85,    86,    87,    91,    92,    94,    95,    96,
      99,   107,   108,   109,   110,   111,   112,   119,   129,   130,
     191,   211,   213,   230,   231,   232,   233,   234,   235,   239,
     240,   241,   242,   243,   244,   246,   262,   265,   271,   272,
     283,   285,   305,   181,   151,   227,   229,   152,   155,   327,
     328,   327,   328,   327,   328,   327,   328,   327,   328,   327,
     328,   327,   328,   327,   328,   327,     4,   326,   327,    40,
      53,    54,    59,    60,    61,    62,    63,    69,    93,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,   157,   153,   156,   150,   189,   286,   245,   261,   291,
     323,   329,   331,     3,   251,   252,    49,    87,   156,   215,
     216,   260,   284,   259,   260,   259,     3,   248,   249,   250,
     259,   261,   247,   331,   306,     3,   248,    67,    88,   103,
     104,   105,   113,   114,   115,   116,   117,   118,   123,   127,
     128,   236,   263,   273,   190,   149,   155,   172,   228,   151,
     154,   152,   155,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   327,   327,   327,   327,   327,   327,   327,   327,
     327,   327,   327,   327,   327,     3,   327,   186,     3,    11,
      12,    16,    17,    18,    21,    24,    36,    37,    38,    43,
      89,    90,   122,   126,   132,   149,   173,   174,   289,   291,
     295,   296,   300,   303,   304,   323,   330,   334,   335,   150,
     255,   256,   149,   152,   292,   156,   149,   152,     4,     3,
     289,   247,   247,   149,   152,   156,   247,   149,   149,   152,
      31,    43,   100,   131,   150,   307,   308,   309,   310,   316,
     317,   321,   212,   149,   259,   259,   150,   253,   254,   255,
       3,   150,   173,   256,   264,   266,   267,   268,   333,   150,
     173,   256,   274,   275,   276,   277,   331,    28,   194,   195,
     230,   232,   233,   239,   327,   326,   327,   135,   157,   135,
     157,   151,   301,   302,   291,   135,   288,   150,   150,   150,
     323,     3,   150,   289,   135,   288,   150,   150,   252,   150,
     150,     4,     5,     6,   150,   331,   150,   331,   149,   149,
     150,   292,   289,   289,    41,    42,    82,    83,   101,   102,
     103,   104,   124,   125,   257,   258,   332,   333,   291,    69,
     172,   327,   252,   135,   214,   149,   149,   249,     4,   149,
     331,   150,   261,   150,     3,    70,    80,   311,   312,   322,
     172,   149,   248,   248,    51,    65,    98,    97,   121,   237,
     238,   260,   237,   269,   270,   327,     4,     5,     6,   150,
     331,   149,   152,   150,   153,   278,   279,   280,   281,   282,
     327,     4,     5,     6,   150,   331,   149,   152,   150,   106,
     195,   196,   155,   327,   327,   149,   150,   149,     3,   297,
      26,   289,   327,   327,   327,   149,   290,   291,   297,    50,
     327,   327,   149,   327,   327,   324,   325,   327,    25,    70,
      80,   327,   336,   337,   326,    69,   172,   152,   152,   291,
     327,   327,   135,   157,     4,   149,   135,   327,   149,   151,
     156,     8,   152,    52,   312,   150,   314,   315,   327,   217,
     149,   149,   151,   151,   151,   260,   260,   332,   332,   151,
     152,   325,   150,   267,   269,   331,   151,   152,   152,   324,
     150,   277,   278,   134,   143,   150,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     197,   198,   199,   200,   201,   202,   203,   204,   205,   209,
     210,    30,   155,   157,   326,   298,   151,   151,   151,   149,
     149,   298,   151,   151,   151,   151,   151,   152,   135,   327,
     327,   327,    76,   151,    76,   151,   151,   334,   335,   334,
     335,   258,   257,   149,   327,   157,   218,   219,   231,   232,
     233,   234,   239,   240,   241,   242,   243,     4,   151,   149,
     327,   150,   311,   312,   313,   312,   150,   312,     8,    52,
     315,   327,   149,   219,   287,   289,   247,   247,   261,   331,
     270,   151,   152,   151,   150,   281,   282,   151,   151,   209,
      32,   199,   202,   201,   202,   135,   204,   151,   231,   239,
     240,   241,   242,   243,   288,   299,    22,   293,   294,   326,
     293,   293,   327,   288,   289,   289,   289,   289,   325,   327,
     336,   336,   149,   327,   327,   327,   327,   151,   151,   157,
     219,   287,   157,   150,   149,   157,   311,   151,   312,   151,
     150,   150,   152,    33,   149,   149,   149,   325,   327,   209,
     135,   135,   158,   159,   160,   161,   206,   208,   209,   149,
      26,   135,   289,    27,   294,   135,    27,    27,   149,    50,
      44,   135,   149,   149,   149,   149,    29,   311,   312,   322,
     327,   152,   141,   142,   320,   311,   312,   315,   151,   152,
     151,   151,   158,   159,   160,   161,   135,   149,   289,   289,
     291,   289,   327,   152,     8,   319,   320,   312,   157,   312,
      20,   152,   320,   151,   152,   325,   142,   207,   208,   151,
     312,   150,    52,     8,    52,   320,   327,   312,    20,   315,
     151,   149,   289,   319,   311,   150,   312,   318,   150,   150,
      20,   151,   320,   327,   151,   152,     8,   152,   312,   151,
     311,   312,   327,   151,    20,   151,   315,   311,   312,   320,
     311,   152,   320,   151,   172,   327,   151,   152,   151,   320,
      20,   172,   312,    20,   314,   151,   172,   315,   311,    20,
     327,   314,   320,   327,   149,   151,   314,   152,   172,   327,
     151,   149,    20,   151,   172,   149,   315,   314,   151,   151,
     327,   151,   314,   151,   149,   151,   172,   151,   172,   149,
     172,   314,   151,   314,   314,   149,   172,   149,   149,   314,
     149
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 280 "vlgParser.y"
    {
	    modstats = NULL;
	    YYTRACE("source_text:");
          ;}
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 285 "vlgParser.y"
    {
              YYTRACE("source_text: source_text description");
          ;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 292 "vlgParser.y"
    {
              YYTRACE("description: module");
          ;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 296 "vlgParser.y"
    {
              YYTRACE("description: primitive");
          ;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 300 "vlgParser.y"
    {
	      YYTRACE("module_item: type_declaration");
          ;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 307 "vlgParser.y"
    {
	      YYTRACE("type_declaration: YYTYPEDEF typesepcifier type_name ';'");
	  ;}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 314 "vlgParser.y"
    {
	    thismodule = make_module ((yyvsp[(2) - (2)].id));
	    fprintf (dbg_out, "\n'((|%s|\n\t   (type . module)\n",(yyvsp[(2) - (2)].id));
	    num_width_table_entries = 0;
            current_var_table = (_var_table *)malloc(MAXNUMVARS*sizeof(_var_table));
            var_table_id = 0;
            current_instance_node = (_instance_node *)malloc(sizeof(_instance_node));
            strcpy(current_instance_node->inst_name,(yyvsp[(2) - (2)].id));
            current_instance_node->p_var_table = current_var_table;
            current_instance_node->p_cfg_block = NULL;
            current_instance_node->instance_node_next = NULL;
            //add the pointer to the module node
          ;}
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 328 "vlgParser.y"
    {
	    fprintf (dbg_out,"\t   (portorder %s)\n",(yyvsp[(4) - (5)].id));
          ;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 333 "vlgParser.y"
    {
	    //	    print_modstats ();
	    //print_wwt ();
	    fprintf (dbg_out, "\t liulingyi )) ;; |%s|\n",(yyvsp[(2) - (8)].id));
            print_var_table(current_var_table,var_table_id);
	    YYTRACE("module: YYMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          ;}
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 343 "vlgParser.y"
    {
	    YYTRACE("module: YYMACROMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          ;}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 350 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("port_list_opt:");
          ;}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 355 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(2) - (3)].id));
	    YYTRACE("port_list_opt: '(' port_list ')'");
          ;}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 363 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port_list: port");
          ;}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 368 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("port_list: port_list ',' port");
          ;}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 376 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port: port_expression_opt");
          ;}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 381 "vlgParser.y"
    {
	  ;}
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 384 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(namedport %s %s)",(yyvsp[(2) - (6)].id),(yyvsp[(5) - (6)].id));
	    YYTRACE("port: ',' YYLID '(' port_expression_opt ')'");
          ;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 392 "vlgParser.y"
    {
	     strcpy ((yyval.id),"");
	     YYTRACE("port_expression_opt:");
           ;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 397 "vlgParser.y"
    {
	     sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	     YYTRACE("port_expression_opt: port_expression");
           ;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 405 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port_expression: port_reference");
          ;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 410 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(2) - (3)].id));
	    YYTRACE("port_expression: '{' port_ref_list '}'");
          ;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 418 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port_ref_list: port_reference");
          ;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 423 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("port_ref_list: port_ref_list ',' port_reference");
          ;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 432 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("port_reference_arg:");
          ;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 437 "vlgParser.y"
    {
            //$$=$1;
	    sprintf ((yyval.id),"(%s)",(yyvsp[(1) - (4)].id));
	    YYTRACE("port_reference_arg: '[' expression ']'");
          ;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 443 "vlgParser.y"
    {
            //$$=$1;
	    sprintf ((yyval.id),"(%s)",(yyvsp[(1) - (6)].id));
	    YYTRACE("port_reference_arg: '[' expression ':' expression ']'");
          ;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 453 "vlgParser.y"
    {
	    YYTRACE("module_item_clr:");
          ;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 457 "vlgParser.y"
    {
	    YYTRACE("module_item_clr: module_item_clr module_item");
          ;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 464 "vlgParser.y"
    {
              YYTRACE("module_item: parameter_declaration");
          ;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 468 "vlgParser.y"
    {
	    YYTRACE("module_item: input_declaration");
          ;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 472 "vlgParser.y"
    {
	    YYTRACE("module_item: output_declaration");
          ;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 476 "vlgParser.y"
    {
              YYTRACE("module_item: inout_declaration");
          ;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 480 "vlgParser.y"
    {
	    YYTRACE("module_item: net_declaration");
          ;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 484 "vlgParser.y"
    {
	    YYTRACE("module_item: reg_declaration");
          ;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 488 "vlgParser.y"
    {
              YYTRACE("module_item: time_declaration");
          ;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 492 "vlgParser.y"
    {
              YYTRACE("module_item: integer_declaration");
          ;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 496 "vlgParser.y"
    {
              YYTRACE("module_item: real_declaration");
          ;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 500 "vlgParser.y"
    {
              YYTRACE("module_item: event_declaration");
          ;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 504 "vlgParser.y"
    {
              YYTRACE("module_item: gate_instantiation");
          ;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 508 "vlgParser.y"
    {
              YYTRACE("module_item: module_or_primitive_instantiation");
          ;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 512 "vlgParser.y"
    {
              YYTRACE("module_item: parameter_override");
          ;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 516 "vlgParser.y"
    {
              YYTRACE("module_item: continous_assign");
          ;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 520 "vlgParser.y"
    {
              YYTRACE("module_item: specify_block");
          ;}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 524 "vlgParser.y"
    {
              YYTRACE("module_item: initial_statement");
          ;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 528 "vlgParser.y"
    {
              YYTRACE("module_item: always_statement");
          ;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 532 "vlgParser.y"
    {
              YYTRACE("module_item: task");
          ;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 536 "vlgParser.y"
    {
              YYTRACE("module_item: function");
          ;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 543 "vlgParser.y"
    {
          ;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 549 "vlgParser.y"
    {
              YYTRACE("primitive: YYPRMITIVE YYLID '(' variable_list ')' ';' primitive_declaration_eclr table_definition YYENDPRIMITIVE");
          ;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 556 "vlgParser.y"
    {
              YYTRACE("primitive_declaration_eclr: primitive_declaration");
          ;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 560 "vlgParser.y"
    {
              YYTRACE("primitive_declaration_eclr: primitive_declaration_eclr primitive_declaration");
          ;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 567 "vlgParser.y"
    {
	    YYTRACE("primitive_declaration: output_declaration");
          ;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 571 "vlgParser.y"
    {
              YYTRACE("primitive_decalration: reg_declaration");
          ;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 575 "vlgParser.y"
    {
              YYTRACE("primitive_decalration: input_declaration");
          ;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 582 "vlgParser.y"
    {
              YYTRACE("table_definition: YYTABLE table_entries YYENDTABLE");
          ;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 589 "vlgParser.y"
    {
              YYTRACE("table_definition: combinational_entry_eclr");
          ;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 593 "vlgParser.y"
    {
              YYTRACE("table_definition: sequential_entry_eclr");
          ;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 600 "vlgParser.y"
    {
              YYTRACE("combinational_entry_eclr: combinational_entry");
          ;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 604 "vlgParser.y"
    {
              YYTRACE("combinational_entry_eclr: combinational_entry_eclr combinational_entry");
          ;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 611 "vlgParser.y"
    {
              YYTRACE("combinational_entry: input_list ':' output_symbol ';'");
          ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 618 "vlgParser.y"
    {
              YYTRACE("sequential_entry_eclr: sequential_entry");
          ;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 622 "vlgParser.y"
    {
              YYTRACE("sequential_entry_eclr: sequential_entry_eclr sequential_entry");
          ;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 629 "vlgParser.y"
    {
              YYTRACE("sequential_entry: input_list ':' state ':' next_state ';'");
          ;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 636 "vlgParser.y"
    {
              YYTRACE("input_list: level_symbol_or_edge_eclr");
          ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 643 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge");
          ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 647 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge_eclr level_symbol_or_edge");
          ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 654 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge: level_symbol");
          ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 658 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge: edge");
          ;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 665 "vlgParser.y"
    {
              YYTRACE("edge: '(' level_symbol level_symbol ')'");
          ;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 669 "vlgParser.y"
    {
              YYTRACE("edge: edge_symbol");
          ;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 676 "vlgParser.y"
    {
              YYTRACE("state: level_symbol");
          ;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 683 "vlgParser.y"
    {
              YYTRACE("next_state: output_symbol");
          ;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 687 "vlgParser.y"
    {
              YYTRACE("next_state: '_'");
          ;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 694 "vlgParser.y"
    {
              YYTRACE("output_symbol: '0'");
          ;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 698 "vlgParser.y"
    {
              YYTRACE("output_symbol: '1'");
          ;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 702 "vlgParser.y"
    {
              YYTRACE("output_symbol: 'x'");
          ;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 706 "vlgParser.y"
    {
              YYTRACE("output_symbol: 'X'");
          ;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 713 "vlgParser.y"
    {
              YYTRACE("level_symbol: '0'");
          ;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 717 "vlgParser.y"
    {
              YYTRACE("level_symbol: '1'");
          ;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 721 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'x'");
          ;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 725 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'X'");
          ;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 729 "vlgParser.y"
    {
              YYTRACE("level_symbol: '?'");
          ;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 733 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'b'");
          ;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 737 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'B'");
          ;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 744 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'r'");
          ;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 748 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'R'");
          ;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 752 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'f'");
          ;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 756 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'F'");
          ;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 760 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'p'");
          ;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 764 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'P'");
          ;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 768 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'n'");
          ;}
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 772 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'N'");
          ;}
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 776 "vlgParser.y"
    {
              YYTRACE("edge_symbol: '*'");
          ;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 783 "vlgParser.y"
    {
	  ;}
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 787 "vlgParser.y"
    {
              YYTRACE("YYTASK YYLID ';' tf_declaration_clr statement_opt YYENDTASK");
          ;}
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 794 "vlgParser.y"
    {
	  ;}
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 798 "vlgParser.y"
    {
              YYTRACE("YYFUNCTION range_or_type_opt YYLID ';' tf_declaration_eclr statement_opt YYENDFUNCTION");
          ;}
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 805 "vlgParser.y"
    {
              YYTRACE("range_or_type_opt:");
          ;}
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 809 "vlgParser.y"
    {
              YYTRACE("range_or_type_opt: range_or_type");
          ;}
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 816 "vlgParser.y"
    {
              YYTRACE("range_or_type: range");
          ;}
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 820 "vlgParser.y"
    {
              YYTRACE("range_or_type: YYINTEGER");
          ;}
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 824 "vlgParser.y"
    {
              YYTRACE("range_or_type: YYREAL");
          ;}
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 831 "vlgParser.y"
    {
              YYTRACE("tf_declaration_clr:");
          ;}
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 835 "vlgParser.y"
    {
              YYTRACE("tf_declaration_clr: tf_declaration_clr tf_declaration");
          ;}
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 842 "vlgParser.y"
    {
              YYTRACE("tf_declaration_eclr: tf_declaration");
          ;}
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 846 "vlgParser.y"
    {
              YYTRACE("tf_declaration_eclr: tf_decalration_eclr tf_declaration");
          ;}
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 853 "vlgParser.y"
    {
              YYTRACE("tf_declaration: parameter_decalration");
          ;}
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 857 "vlgParser.y"
    {
              YYTRACE("tf_declaration: input_declaration");
          ;}
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 861 "vlgParser.y"
    {
              YYTRACE("tf_declaration: output_declaration");
          ;}
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 865 "vlgParser.y"
    {
              YYTRACE("tf_declaration: inout_declaration");
          ;}
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 869 "vlgParser.y"
    {
              YYTRACE("tf_declaration: reg_declaration");
          ;}
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 873 "vlgParser.y"
    {
              YYTRACE("tf_declaration: time_declaration");
          ;}
    break;

  case 114:

/* Line 1455 of yacc.c  */
#line 877 "vlgParser.y"
    {
              YYTRACE("tf_declaration: integer_declaration");
          ;}
    break;

  case 115:

/* Line 1455 of yacc.c  */
#line 881 "vlgParser.y"
    {
              YYTRACE("tf_declaration: real_declaration");
          ;}
    break;

  case 116:

/* Line 1455 of yacc.c  */
#line 885 "vlgParser.y"
    {
              YYTRACE("tf_declaration: event_declaration");
          ;}
    break;

  case 117:

/* Line 1455 of yacc.c  */
#line 892 "vlgParser.y"
    {
	      YYTRACE("type_name: YYLID");
	  ;}
    break;

  case 118:

/* Line 1455 of yacc.c  */
#line 899 "vlgParser.y"
    {
	      YYTRACE("type_specifier: enum_specifier ';'");
	  ;}
    break;

  case 119:

/* Line 1455 of yacc.c  */
#line 905 "vlgParser.y"
    {;}
    break;

  case 120:

/* Line 1455 of yacc.c  */
#line 906 "vlgParser.y"
    {
	      YYTRACE("enum_specifier: YYENUM enum_name enum_lst_opt");
	  ;}
    break;

  case 121:

/* Line 1455 of yacc.c  */
#line 909 "vlgParser.y"
    {;}
    break;

  case 122:

/* Line 1455 of yacc.c  */
#line 910 "vlgParser.y"
    {
	      YYTRACE("enum_specifier: YYENUM '{' enumerator_list '}'");
	  ;}
    break;

  case 123:

/* Line 1455 of yacc.c  */
#line 917 "vlgParser.y"
    {
	      YYTRACE("enum_name: YYLID");
	  ;}
    break;

  case 124:

/* Line 1455 of yacc.c  */
#line 924 "vlgParser.y"
    {
	      YYTRACE("enum_lst_opt: '{' enumerator_list '}'");
	  ;}
    break;

  case 125:

/* Line 1455 of yacc.c  */
#line 928 "vlgParser.y"
    {
	      YYTRACE("enum_lst_opt: ");
	  ;}
    break;

  case 126:

/* Line 1455 of yacc.c  */
#line 935 "vlgParser.y"
    {
	      YYTRACE("enumerator_list: enumerator");
	  ;}
    break;

  case 127:

/* Line 1455 of yacc.c  */
#line 939 "vlgParser.y"
    {
	      YYTRACE("enumerator_list: enumerator_list ',' enumerator");
	  ;}
    break;

  case 128:

/* Line 1455 of yacc.c  */
#line 946 "vlgParser.y"
    {
	      YYTRACE("enumerator: YYLID");
	  ;}
    break;

  case 129:

/* Line 1455 of yacc.c  */
#line 949 "vlgParser.y"
    {;}
    break;

  case 130:

/* Line 1455 of yacc.c  */
#line 950 "vlgParser.y"
    {
	      YYTRACE("enumerator: YYLID '=' expression");
	  ;}
    break;

  case 131:

/* Line 1455 of yacc.c  */
#line 959 "vlgParser.y"
    {
	      YYTRACE("type_decorator_opt: YYuTYPE");
	  ;}
    break;

  case 132:

/* Line 1455 of yacc.c  */
#line 963 "vlgParser.y"
    {
	      YYTRACE("type_decorator_opt: ");
	  ;}
    break;

  case 133:

/* Line 1455 of yacc.c  */
#line 970 "vlgParser.y"
    {
              YYTRACE("parameter_declaration: YYPARAMETER assignment_list ';'");
          ;}
    break;

  case 134:

/* Line 1455 of yacc.c  */
#line 977 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (dbg_out,"\t   (ins %s)\n",(yyvsp[(3) - (4)].id));
	    else 
	      fprintf (dbg_out,"\t   (ins %s %s)\n",(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("input_declaration: YYINPUT range_opt variable_list ';'");
            cur_range_start = get_range_start((yyvsp[(2) - (4)].id));
            cur_range_end = get_range_end((yyvsp[(2) - (4)].id));
            input_handling = true;
            create_varlist_table();
            input_handling = false;
          ;}
    break;

  case 135:

/* Line 1455 of yacc.c  */
#line 993 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (dbg_out,"\t   (outs %s)\n",(yyvsp[(3) - (4)].id));
	    else
	      fprintf (dbg_out,"\t   (outs %s %s)\n",(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("output_declaration: YYOUTPUT range_opt variable_list ';'");
            cur_range_start = get_range_start((yyvsp[(2) - (4)].id));
            cur_range_end = get_range_end((yyvsp[(2) - (4)].id));
            create_varlist_table();
          ;}
    break;

  case 136:

/* Line 1455 of yacc.c  */
#line 1007 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (dbg_out,"\t   (inouts %s)\n",(yyvsp[(3) - (4)].id));
	    else
	      fprintf (dbg_out,"\t   (inouts %s %s)\n",(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("inout_declaration: YYINOUT range_opt variable_list ';'");
            cur_range_start = get_range_start((yyvsp[(2) - (4)].id));
            cur_range_end = get_range_end((yyvsp[(2) - (4)].id));
            create_varlist_table();
          ;}
    break;

  case 137:

/* Line 1455 of yacc.c  */
#line 1022 "vlgParser.y"
    {
              YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt assignment_list ';'");
          ;}
    break;

  case 138:

/* Line 1455 of yacc.c  */
#line 1027 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(4) - (7)].id),"")==0)
	      fprintf (dbg_out,"\t   (wire %s)\n",(yyvsp[(6) - (7)].id));
	    else
	      fprintf (dbg_out,"\t   (wire %s %s)\n",(yyvsp[(4) - (7)].id),(yyvsp[(6) - (7)].id));
            cur_range_start = get_range_start((yyvsp[(4) - (7)].id));
            cur_range_end = get_range_end((yyvsp[(4) - (7)].id));
            create_varlist_table();
	    YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt variable_list ';'");
          ;}
    break;

  case 139:

/* Line 1455 of yacc.c  */
#line 1039 "vlgParser.y"
    {
              YYTRACE("net_declaration: type_decorator_opt YYTRIREG charge_strength_opt expandrange_opt delay_opt variable_list ';'");
          ;}
    break;

  case 140:

/* Line 1455 of yacc.c  */
#line 1047 "vlgParser.y"
    {
              YYTRACE("nettype: YYSWIRE");
          ;}
    break;

  case 141:

/* Line 1455 of yacc.c  */
#line 1051 "vlgParser.y"
    {
              YYTRACE("nettype: YYWIRE");
          ;}
    break;

  case 142:

/* Line 1455 of yacc.c  */
#line 1055 "vlgParser.y"
    {
              YYTRACE("nettype: YYTRI");
          ;}
    break;

  case 143:

/* Line 1455 of yacc.c  */
#line 1059 "vlgParser.y"
    {
              YYTRACE("nettype: YYTRI1");
          ;}
    break;

  case 144:

/* Line 1455 of yacc.c  */
#line 1063 "vlgParser.y"
    {
              YYTRACE("nettype: YYSUPPLY0");
          ;}
    break;

  case 145:

/* Line 1455 of yacc.c  */
#line 1067 "vlgParser.y"
    {
              YYTRACE("nettype: YYWAND");
	  ;}
    break;

  case 146:

/* Line 1455 of yacc.c  */
#line 1071 "vlgParser.y"
    {
	      YYTRACE("nettype: YYTRIAND");
	  ;}
    break;

  case 147:

/* Line 1455 of yacc.c  */
#line 1075 "vlgParser.y"
    {
	      YYTRACE("nettype: YYTRI0");
	  ;}
    break;

  case 148:

/* Line 1455 of yacc.c  */
#line 1079 "vlgParser.y"
    {
	      YYTRACE("nettype: YYSUPPLY1");
	  ;}
    break;

  case 149:

/* Line 1455 of yacc.c  */
#line 1083 "vlgParser.y"
    {
	      YYTRACE("nettype: YYWOR");
          ;}
    break;

  case 150:

/* Line 1455 of yacc.c  */
#line 1087 "vlgParser.y"
    {
              YYTRACE("nettype: YYTRIOR");
          ;}
    break;

  case 151:

/* Line 1455 of yacc.c  */
#line 1095 "vlgParser.y"
    {
	    strcpy ((yyval.id),"(0 0)");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("expandrange_opt:");
            num_width_table_entries = 0;
          ;}
    break;

  case 152:

/* Line 1455 of yacc.c  */
#line 1102 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expandrange_opt: expandrange");
            num_width_table_entries = 0;
          ;}
    break;

  case 153:

/* Line 1455 of yacc.c  */
#line 1111 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expandrange: range");
            num_width_table_entries = 0;
          ;}
    break;

  case 154:

/* Line 1455 of yacc.c  */
#line 1117 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("expandrange: YYSCALARED range");
            num_width_table_entries = 0;
          ;}
    break;

  case 155:

/* Line 1455 of yacc.c  */
#line 1123 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("expandrange: YYVECTORED range");
            num_width_table_entries = 0;
          ;}
    break;

  case 156:

/* Line 1455 of yacc.c  */
#line 1132 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(3) - (5)].id),"")==0)
	      fprintf (dbg_out,"\t   (sts %s)\n",(yyvsp[(4) - (5)].id));
	    else
	      fprintf (dbg_out,"\t   (sts %s %s)\n",(yyvsp[(3) - (5)].id),(yyvsp[(4) - (5)].id));

            cur_range_start = get_range_start((yyvsp[(3) - (5)].id));
            cur_range_end = get_range_end((yyvsp[(3) - (5)].id));
            create_varlist_table();

	    YYTRACE("reg_declaration: type_decorator_opt YYREG range_opt register_variable_list ';'");
          ;}
    break;

  case 157:

/* Line 1455 of yacc.c  */
#line 1145 "vlgParser.y"
    {
              YYTRACE("reg_declaration: type_decorator_opt YYMREG range_opt register_variable_list ';'");
          ;}
    break;

  case 158:

/* Line 1455 of yacc.c  */
#line 1152 "vlgParser.y"
    {
              YYTRACE("time_declaration: YYTIME register_variable_list ';'");
          ;}
    break;

  case 159:

/* Line 1455 of yacc.c  */
#line 1159 "vlgParser.y"
    {
              YYTRACE("integer_declaration: YYINTEGER register_variable_list ';'");
          ;}
    break;

  case 160:

/* Line 1455 of yacc.c  */
#line 1166 "vlgParser.y"
    {
              YYTRACE("real_declaration: YYREAL variable_list ';'");
          ;}
    break;

  case 161:

/* Line 1455 of yacc.c  */
#line 1173 "vlgParser.y"
    {
              YYTRACE("event_declaration: YYEVENT name_of_event_list ';'");
          ;}
    break;

  case 162:

/* Line 1455 of yacc.c  */
#line 1179 "vlgParser.y"
    {;}
    break;

  case 163:

/* Line 1455 of yacc.c  */
#line 1181 "vlgParser.y"
    {
            cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
            cfg_temp->node_type = BLK_AS;
            cfg_temp->p_exptree_node = (yyvsp[(5) - (6)].EXPNODE);
            push_cfg_table(cfg_temp);
            if(current_lvalue_node!=NULL)
            {
              cfg_temp->defined_var_chain = current_lvalue_node;
              current_lvalue_node = NULL;
            }
	    fprintf (dbg_out,"\t(---assign %s)\n",(yyvsp[(5) - (6)].EXPNODE)->number_string);
	    YYTRACE("continuous_assign: YYASSIGN drive_strength_opt delay_opt assignment_list ';'");
          ;}
    break;

  case 164:

/* Line 1455 of yacc.c  */
#line 1198 "vlgParser.y"
    {
	    fprintf (dbg_out,"\t   (defparam %s)\n",(yyvsp[(2) - (3)].id));
	    YYTRACE("parameter_override: YYDEFPARAM assign_list ';'");
          ;}
    break;

  case 165:

/* Line 1455 of yacc.c  */
#line 1206 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    put_wwt_entry ((yyvsp[(1) - (1)].id), current_range);
	    YYTRACE("variable_list: identifier");
          ;}
    break;

  case 166:

/* Line 1455 of yacc.c  */
#line 1212 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    put_wwt_entry ((yyvsp[(3) - (3)].id), current_range);
	    YYTRACE("variable_list: variable_list ',' identifier");
          ;}
    break;

  case 167:

/* Line 1455 of yacc.c  */
#line 1221 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("register_variable_list: register_variable");
          ;}
    break;

  case 168:

/* Line 1455 of yacc.c  */
#line 1226 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("register_variable_list: register_variable_list ',' register_variable");
          ;}
    break;

  case 169:

/* Line 1455 of yacc.c  */
#line 1234 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    put_wwt_entry ((yyvsp[(1) - (1)].id), current_range);
	    YYTRACE("register_variable: name_of_register");
          ;}
    break;

  case 170:

/* Line 1455 of yacc.c  */
#line 1240 "vlgParser.y"
    {
	    sprintf ((yyval.id),"((%s %s) %s)",(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id),(yyvsp[(1) - (6)].id));
	    {char s[MAXSTRLEN]; sprintf (s,"(%s %s)",(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id));
	    put_wwt_entry ((yyvsp[(1) - (6)].id), s);}
	    YYTRACE("register_variable: name_of_register '[' expression ':' expression ']'");
          ;}
    break;

  case 171:

/* Line 1455 of yacc.c  */
#line 1250 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_register: YYLID");
          ;}
    break;

  case 172:

/* Line 1455 of yacc.c  */
#line 1258 "vlgParser.y"
    {
              YYTRACE("name_of_event_list: name_of_event");
          ;}
    break;

  case 173:

/* Line 1455 of yacc.c  */
#line 1262 "vlgParser.y"
    {
              YYTRACE("name_of_event_list: name_of_event_list ',' name_of_event");
          ;}
    break;

  case 174:

/* Line 1455 of yacc.c  */
#line 1269 "vlgParser.y"
    {
              YYTRACE("name_of_event: YYLID");
          ;}
    break;

  case 175:

/* Line 1455 of yacc.c  */
#line 1276 "vlgParser.y"
    {
              YYTRACE("charge_strength_opt:");
          ;}
    break;

  case 176:

/* Line 1455 of yacc.c  */
#line 1280 "vlgParser.y"
    {
              YYTRACE("charge_strength_opt: charge_strength");
          ;}
    break;

  case 177:

/* Line 1455 of yacc.c  */
#line 1287 "vlgParser.y"
    {
              YYTRACE("charge_strength: '(' YYSMALL ')'");
          ;}
    break;

  case 178:

/* Line 1455 of yacc.c  */
#line 1291 "vlgParser.y"
    {
              YYTRACE("charge_strength: '(' YYMEDIUM ')'");
          ;}
    break;

  case 179:

/* Line 1455 of yacc.c  */
#line 1295 "vlgParser.y"
    {
              YYTRACE("charge_strength: '(' YYLARGE ')'");
          ;}
    break;

  case 180:

/* Line 1455 of yacc.c  */
#line 1302 "vlgParser.y"
    {
              YYTRACE("drive_strength_opt:");
          ;}
    break;

  case 181:

/* Line 1455 of yacc.c  */
#line 1306 "vlgParser.y"
    {
              YYTRACE("drive_strength_opt: drive_strength");
          ;}
    break;

  case 182:

/* Line 1455 of yacc.c  */
#line 1313 "vlgParser.y"
    {
              YYTRACE("drive_strength: '(' strength0 ',' strength1 ')'");
          ;}
    break;

  case 183:

/* Line 1455 of yacc.c  */
#line 1317 "vlgParser.y"
    {
              YYTRACE("drive_strength: '(' strength1 ',' strength0 ')'");
          ;}
    break;

  case 184:

/* Line 1455 of yacc.c  */
#line 1324 "vlgParser.y"
    {
              YYTRACE("strength0: YYSUPPLY0");
          ;}
    break;

  case 185:

/* Line 1455 of yacc.c  */
#line 1328 "vlgParser.y"
    {
              YYTRACE("strength0: YYSTRONG0");
          ;}
    break;

  case 186:

/* Line 1455 of yacc.c  */
#line 1332 "vlgParser.y"
    {
              YYTRACE("strength0: YYPULL0");
          ;}
    break;

  case 187:

/* Line 1455 of yacc.c  */
#line 1336 "vlgParser.y"
    {
              YYTRACE("strength0: YYWEAK0");
          ;}
    break;

  case 188:

/* Line 1455 of yacc.c  */
#line 1340 "vlgParser.y"
    {
              YYTRACE("strength0: YYHIGHZ0");
          ;}
    break;

  case 189:

/* Line 1455 of yacc.c  */
#line 1347 "vlgParser.y"
    {
              YYTRACE("strength1: YYSUPPLY1");
          ;}
    break;

  case 190:

/* Line 1455 of yacc.c  */
#line 1351 "vlgParser.y"
    {
              YYTRACE("strength1: YYSTRONG1");
          ;}
    break;

  case 191:

/* Line 1455 of yacc.c  */
#line 1355 "vlgParser.y"
    {
              YYTRACE("strength1: YYPULL1");
          ;}
    break;

  case 192:

/* Line 1455 of yacc.c  */
#line 1359 "vlgParser.y"
    {
              YYTRACE("strength1: YYWEAK1");
          ;}
    break;

  case 193:

/* Line 1455 of yacc.c  */
#line 1363 "vlgParser.y"
    {
              YYTRACE("strength1: YYHIGHZ1");
          ;}
    break;

  case 194:

/* Line 1455 of yacc.c  */
#line 1370 "vlgParser.y"
    {
	    strcpy ((yyval.id),"(0 0)");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("range_opt:");
            num_width_table_entries = 0;
          ;}
    break;

  case 195:

/* Line 1455 of yacc.c  */
#line 1377 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    sprintf (current_range, (yyvsp[(1) - (1)].id));
	    YYTRACE("range_opt: range");
            num_width_table_entries = 0;
          ;}
    break;

  case 196:

/* Line 1455 of yacc.c  */
#line 1387 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s %s)",(yyvsp[(2) - (5)].id),(yyvsp[(4) - (5)].id));
	    sprintf (current_range,"(%s %s)",(yyvsp[(2) - (5)].id),(yyvsp[(4) - (5)].id));
              YYTRACE("range: '[' expression ':' expression ']'");
            num_width_table_entries = 0;
          ;}
    break;

  case 197:

/* Line 1455 of yacc.c  */
#line 1397 "vlgParser.y"
    {
	    //sprintf ($$,$1);
            //$$="";
	    sprintf ((yyval.id),"");
	    YYTRACE("assignment_list: assignment");
          ;}
    break;

  case 198:

/* Line 1455 of yacc.c  */
#line 1404 "vlgParser.y"
    {
            //$$="";
	    sprintf ((yyval.id),"");
	    YYTRACE("assignment_list: assignment_list ',' assignment");
          ;}
    break;

  case 199:

/* Line 1455 of yacc.c  */
#line 1414 "vlgParser.y"
    {
	    fprintf (dbg_out,"\t   (occs %s %s)\n",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("gate_instantiation: gatetype drive_delay_clr gate_instance_list ';'");
          ;}
    break;

  case 200:

/* Line 1455 of yacc.c  */
#line 1422 "vlgParser.y"
    {
              YYTRACE("drive_delay_clr:");
          ;}
    break;

  case 201:

/* Line 1455 of yacc.c  */
#line 1426 "vlgParser.y"
    {
              YYTRACE("drive_delay_clr: drive_delay_clr drive_delay");
          ;}
    break;

  case 202:

/* Line 1455 of yacc.c  */
#line 1433 "vlgParser.y"
    {
              YYTRACE("drive_delay: drive_strength");
          ;}
    break;

  case 203:

/* Line 1455 of yacc.c  */
#line 1437 "vlgParser.y"
    {
              YYTRACE("drive_delay: delay");
          ;}
    break;

  case 204:

/* Line 1455 of yacc.c  */
#line 1444 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYAND");
	  ;}
    break;

  case 205:

/* Line 1455 of yacc.c  */
#line 1449 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNAND");
	  ;}
    break;

  case 206:

/* Line 1455 of yacc.c  */
#line 1454 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYOR");
	  ;}
    break;

  case 207:

/* Line 1455 of yacc.c  */
#line 1459 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOR");
	  ;}
    break;

  case 208:

/* Line 1455 of yacc.c  */
#line 1464 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYXOR");
	  ;}
    break;

  case 209:

/* Line 1455 of yacc.c  */
#line 1469 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYXNOR");
	  ;}
    break;

  case 210:

/* Line 1455 of yacc.c  */
#line 1474 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYBUF");
	  ;}
    break;

  case 211:

/* Line 1455 of yacc.c  */
#line 1479 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYBIFIF0");
	  ;}
    break;

  case 212:

/* Line 1455 of yacc.c  */
#line 1484 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYBIFIF1");
	  ;}
    break;

  case 213:

/* Line 1455 of yacc.c  */
#line 1489 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOT");
	  ;}
    break;

  case 214:

/* Line 1455 of yacc.c  */
#line 1494 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOTIF0");
	  ;}
    break;

  case 215:

/* Line 1455 of yacc.c  */
#line 1499 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOTIF1");
	  ;}
    break;

  case 216:

/* Line 1455 of yacc.c  */
#line 1504 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYPULLDOWN");
	  ;}
    break;

  case 217:

/* Line 1455 of yacc.c  */
#line 1509 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYPULLUP");
	  ;}
    break;

  case 218:

/* Line 1455 of yacc.c  */
#line 1514 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNMOS");
	  ;}
    break;

  case 219:

/* Line 1455 of yacc.c  */
#line 1519 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYPMOS");
	  ;}
    break;

  case 220:

/* Line 1455 of yacc.c  */
#line 1524 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRNMOS");
	  ;}
    break;

  case 221:

/* Line 1455 of yacc.c  */
#line 1529 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRPMOS");
	  ;}
    break;

  case 222:

/* Line 1455 of yacc.c  */
#line 1534 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYCMOS");
	  ;}
    break;

  case 223:

/* Line 1455 of yacc.c  */
#line 1539 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRCMOS");
	  ;}
    break;

  case 224:

/* Line 1455 of yacc.c  */
#line 1544 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTRAN");
	  ;}
    break;

  case 225:

/* Line 1455 of yacc.c  */
#line 1549 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRTRAN");
	  ;}
    break;

  case 226:

/* Line 1455 of yacc.c  */
#line 1554 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTRANIF0");
	  ;}
    break;

  case 227:

/* Line 1455 of yacc.c  */
#line 1559 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRTRANIF0");
	  ;}
    break;

  case 228:

/* Line 1455 of yacc.c  */
#line 1564 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTRANIF1");
	  ;}
    break;

  case 229:

/* Line 1455 of yacc.c  */
#line 1569 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRTRANIF1");
	  ;}
    break;

  case 230:

/* Line 1455 of yacc.c  */
#line 1574 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTESLATIMER");
	  ;}
    break;

  case 231:

/* Line 1455 of yacc.c  */
#line 1582 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("gate_instance_list: gate_instance");
          ;}
    break;

  case 232:

/* Line 1455 of yacc.c  */
#line 1587 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("gate_instance_list: gate_instance_list ',' gate_instance");
          ;}
    break;

  case 233:

/* Line 1455 of yacc.c  */
#line 1595 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s)",(yyvsp[(2) - (3)].id));
	    YYTRACE("gate_instance: '(' terminal_list ')'");
          ;}
    break;

  case 234:

/* Line 1455 of yacc.c  */
#line 1600 "vlgParser.y"
    {
	    sprintf ((yyval.id),"((occname %s) %s)",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("gate_instance: name_of_gate_instance '(' terminal_list ')'");
          ;}
    break;

  case 235:

/* Line 1455 of yacc.c  */
#line 1608 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_gate_instance: YYLID");
          ;}
    break;

  case 236:

/* Line 1455 of yacc.c  */
#line 1616 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("terminal_list: terminal");
          ;}
    break;

  case 237:

/* Line 1455 of yacc.c  */
#line 1621 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("terminal_list: terminal_list ',' terminal");
          ;}
    break;

  case 238:

/* Line 1455 of yacc.c  */
#line 1629 "vlgParser.y"
    {
            //$$="";
	    sprintf ((yyval.id),"");
	    YYTRACE("terminal: expression");
          ;}
    break;

  case 239:

/* Line 1455 of yacc.c  */
#line 1640 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (dbg_out,"\t   (occs %s %s)\n",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id)); 
	    else
	      fprintf (dbg_out,"\t   (occs %s (options %s) %s)\n",(yyvsp[(1) - (4)].id),(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id)); 
	    YYTRACE("module_or_primitive_instantiation: name_of_module_or_primitive module_or_primitive_option_clr module_or_primitive_instance_list ';'");
          ;}
    break;

  case 240:

/* Line 1455 of yacc.c  */
#line 1651 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_module_or_primitive: YYLID");
          ;}
    break;

  case 241:

/* Line 1455 of yacc.c  */
#line 1659 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("module_or_primitive_option_clr:");
          ;}
    break;

  case 242:

/* Line 1455 of yacc.c  */
#line 1664 "vlgParser.y"
    {
	    sprintf ((yyval.id), "%s %s", (yyvsp[(1) - (2)].id), (yyvsp[(2) - (2)].id));
	    YYTRACE("module_or_primitive_option_clr: module_or_primitive_option_clr module_or_primitive_option");
          ;}
    break;

  case 243:

/* Line 1455 of yacc.c  */
#line 1672 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("module_or_primitive_option:");
          ;}
    break;

  case 244:

/* Line 1455 of yacc.c  */
#line 1677 "vlgParser.y"
    {
	    strcpy((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("module_or_primitive_option: delay");
          ;}
    break;

  case 245:

/* Line 1455 of yacc.c  */
#line 1685 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER");
	  ;}
    break;

  case 246:

/* Line 1455 of yacc.c  */
#line 1690 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER_BIT");
	  ;}
    break;

  case 247:

/* Line 1455 of yacc.c  */
#line 1695 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));	
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYRNUMBER");
	  ;}
    break;

  case 248:

/* Line 1455 of yacc.c  */
#line 1700 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' identifier");
	  ;}
    break;

  case 249:

/* Line 1455 of yacc.c  */
#line 1705 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(3) - (4)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' '(' mintypmax_expression_list ')'");
	  ;}
    break;

  case 250:

/* Line 1455 of yacc.c  */
#line 1713 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
            YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance");
          ;}
    break;

  case 251:

/* Line 1455 of yacc.c  */
#line 1718 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance_list ',' module_or_primitive_instance");
          ;}
    break;

  case 252:

/* Line 1455 of yacc.c  */
#line 1727 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s)",(yyvsp[(2) - (3)].id));
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          ;}
    break;

  case 253:

/* Line 1455 of yacc.c  */
#line 1733 "vlgParser.y"
    {
	    sprintf ((yyval.id),"((occname %s) (%s))",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          ;}
    break;

  case 254:

/* Line 1455 of yacc.c  */
#line 1741 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("module_connection_list: module_port_connection_list");
          ;}
    break;

  case 255:

/* Line 1455 of yacc.c  */
#line 1746 "vlgParser.y"
    {
            push_cfg_table((yyvsp[(1) - (1)].NODE));
	    //sprintf ($$,$1->exp_name);
	    YYTRACE("module_connection_list: named_port_connection_list");
          ;}
    break;

  case 256:

/* Line 1455 of yacc.c  */
#line 1755 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("module_port_connection_list: module_port_connection");
          ;}
    break;

  case 257:

/* Line 1455 of yacc.c  */
#line 1760 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("module_port_connection_list: module_port_connection_list ',' module_port_connection");
          ;}
    break;

  case 258:

/* Line 1455 of yacc.c  */
#line 1768 "vlgParser.y"
    {
	    //sprintf ($$,$1);
            (yyval.NODE)=(yyvsp[(1) - (1)].NODE);
	    YYTRACE("named_port_connection_list: named_port_connection");
          ;}
    break;

  case 259:

/* Line 1455 of yacc.c  */
#line 1774 "vlgParser.y"
    {
	    //sprintf ($$,"%s %s",$1,$3);
            cfg_temp = (yyvsp[(1) - (3)].NODE);
            while(cfg_temp->next_node!=NULL)
              cfg_temp=cfg_temp->next_node;
            cfg_temp->next_node = (yyvsp[(3) - (3)].NODE);
            (yyval.NODE)=(yyvsp[(1) - (3)].NODE);
	    YYTRACE("named_port_connection_list: named_port_connection_list ',' name_port_connection");
          ;}
    break;

  case 260:

/* Line 1455 of yacc.c  */
#line 1786 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("module_port_connection:");
          ;}
    break;

  case 261:

/* Line 1455 of yacc.c  */
#line 1791 "vlgParser.y"
    {
            //$$="";
	    sprintf ((yyval.id),"");
	    YYTRACE("module_port_connection: expression");
          ;}
    break;

  case 262:

/* Line 1455 of yacc.c  */
#line 1800 "vlgParser.y"
    {
            char id_temp[MAXSTRLEN];
            //sprintf (id_temp,"%s = %s",$2,$4);
            cfg_temp = build_normal_node((yyvsp[(2) - (5)].id), NULL, NULL, NULL, NULL);
            _exptree_node * p_exptree_temp = new_exptree_node(VAR,NULL,NULL,NULL,-1,(yyvsp[(2) - (5)].id));
            cfg_temp->p_exptree_node = new_exptree_node(NAME_PORT,p_exptree_temp,NULL,(yyvsp[(4) - (5)].EXPNODE),-1,NULL);
            cfg_temp->node_type = PORT_CONNECT;
            (yyval.NODE)=cfg_temp;
	    //sprintf ($$,"(namedport %s %s)",$2,$4);
	    YYTRACE("named_port_connection: '.' identifier '(' expression ')'");
          ;}
    break;

  case 263:

/* Line 1455 of yacc.c  */
#line 1816 "vlgParser.y"
    {;}
    break;

  case 264:

/* Line 1455 of yacc.c  */
#line 1817 "vlgParser.y"
    {
	    fprintf (dbg_out,"\t   (initial\n\t\t%s\n\t   )\n",(yyvsp[(3) - (3)].NODE)->exp_name);
	    YYTRACE("initial_statement: YYINITIAL statement");
          ;}
    break;

  case 265:

/* Line 1455 of yacc.c  */
#line 1824 "vlgParser.y"
    {;}
    break;

  case 266:

/* Line 1455 of yacc.c  */
#line 1825 "vlgParser.y"
    {
            push_cfg_table((yyvsp[(3) - (3)].NODE));
            (yyvsp[(3) - (3)].NODE)->node_type = RESET_IF;
	    fprintf (dbg_out,"\t   (always\n\t\t%s\n\t   )\n",(yyvsp[(3) - (3)].NODE)->exp_name);
  	    YYTRACE("always_statement: YYALWAYS statement");
          ;}
    break;

  case 267:

/* Line 1455 of yacc.c  */
#line 1835 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("statement_opt:");
          ;}
    break;

  case 268:

/* Line 1455 of yacc.c  */
#line 1840 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].NODE)->exp_name);
	    YYTRACE("statement_opt: statement");
          ;}
    break;

  case 269:

/* Line 1455 of yacc.c  */
#line 1848 "vlgParser.y"
    {
	    //strcpy ($$,"");
            //printf("return\n");
            (yyval.NODE)=NULL;
	    YYTRACE("statement_clr:");
          ;}
    break;

  case 270:

/* Line 1455 of yacc.c  */
#line 1855 "vlgParser.y"
    {
	    //sprintf ($$, "%s  %s", $1, $2);
            if((yyvsp[(1) - (2)].NODE)!=NULL)
              set_next_node((yyvsp[(1) - (2)].NODE),(yyvsp[(2) - (2)].NODE));  
            if((yyvsp[(1) - (2)].NODE)!=NULL) 
              (yyval.NODE)=(yyvsp[(1) - (2)].NODE);
            else
              (yyval.NODE)=(yyvsp[(2) - (2)].NODE);
            //==if($1!=NULL){
            //==  printf("......%s---next is",$1->exp_name);
            //==  printf("......%s\n",$1->next_node->exp_name);
            //==}
	    YYTRACE("statement_clr: statement_clr statement");
          ;}
    break;

  case 271:

/* Line 1455 of yacc.c  */
#line 1874 "vlgParser.y"
    {
	    //strcpy ($$,"");
            //$$=(_cfg_node *)malloc(sizeof(_cfg_node));
            //strcpy($$->exp_name,"");
            (yyval.NODE)=NULL;
	    YYTRACE("statement: ';'");
          ;}
    break;

  case 272:

/* Line 1455 of yacc.c  */
#line 1882 "vlgParser.y"
    {
	    //sprintf ($$->exp_name, "%s", $1);
            cfg_top = build_normal_node("",NULL,NULL,NULL,NULL);
            cfg_top->p_exptree_node = (yyvsp[(1) - (2)].EXPNODE);
            cfg_top->branch_number = var_index;
            if((yyvsp[(1) - (2)].EXPNODE)->tree_node_t==ASS_BLK)
              cfg_top->node_type = BLK_AS;
            else
              cfg_top->node_type = NBLK_AS;
  
            if(current_lvalue_node!=NULL)
            {
              cfg_top->defined_var_chain = current_lvalue_node;
              current_lvalue_node = NULL;
            }
            (yyval.NODE)=cfg_top;
            //printf("\ndeduce an statement : \n");
	    YYTRACE("statement: assignment ';'");
          ;}
    break;

  case 273:

/* Line 1455 of yacc.c  */
#line 1902 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",(yyvsp[(5) - (5)].NODE),NULL,cfg_temp,NULL);
            cfg_top->node_type = IFCOND_EXP;
            (yyvsp[(5) - (5)].NODE)->Is_branch_node = true;
            cfg_top->p_exptree_node = (yyvsp[(3) - (5)].EXPNODE);
            (yyvsp[(3) - (5)].EXPNODE)->Is_top_node = true;
            set_next_node((yyvsp[(5) - (5)].NODE),cfg_temp);
            (yyval.NODE)=cfg_top;
	    //sprintf ($$, "%s", $1);
	    //sprintf ($$, "(if %s %s)", $3, $5);
	    YYTRACE("statement: YYIF '(' expression ')' statement");
          ;}
    break;

  case 274:

/* Line 1455 of yacc.c  */
#line 1916 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",(yyvsp[(5) - (7)].NODE),NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = (yyvsp[(3) - (7)].EXPNODE);
            (yyvsp[(5) - (7)].NODE)->brother_node=(yyvsp[(7) - (7)].NODE);
            (yyvsp[(3) - (7)].EXPNODE)->Is_top_node = true;
            //if($3->right!=NULL)
            //printf("\n+++right: 1->idx = %s;\n",current_instance_node->p_var_table[$3->right->var_table_idx].var_name);            
            //if($3->left!=NULL)
            //printf("\n+++left: 1->idx = %s;\n",current_instance_node->p_var_table[$3->left->var_table_idx].var_name);            
            //if(($3->left==NULL)&&($3->right==NULL))
            //printf("\n+++self: 1->idx = %s;\n",current_instance_node->p_var_table[$3->var_table_idx].var_name);            
 
            cfg_top->node_type = IFELSE_EXP;
            set_next_node((yyvsp[(5) - (7)].NODE),cfg_temp);
            set_next_node((yyvsp[(7) - (7)].NODE),cfg_temp);
            (yyvsp[(5) - (7)].NODE)->Is_branch_node = true;
            (yyvsp[(7) - (7)].NODE)->Is_branch_node = true;
            (yyvsp[(5) - (7)].NODE)->bran_converge_node = cfg_temp;
            (yyvsp[(7) - (7)].NODE)->bran_converge_node = cfg_temp;
            (yyvsp[(5) - (7)].NODE)->bran_num = get_branch_number((yyvsp[(5) - (7)].NODE));
            (yyvsp[(7) - (7)].NODE)->bran_num = get_branch_number((yyvsp[(7) - (7)].NODE));
            (yyval.NODE)=cfg_top;
            branch_table[get_branch_number((yyvsp[(5) - (7)].NODE))]=(yyvsp[(5) - (7)].NODE);
            branch_table[get_branch_number((yyvsp[(7) - (7)].NODE))]=(yyvsp[(7) - (7)].NODE);
            //==printf("----%s\n",$3);
            //==printf("----%s\n",$5->exp_name);
            //==printf("----%s\n",$5->next_node->exp_name);
            //==printf("----%s\n",$7->exp_name);
	    //sprintf ($$, "(if %s %s %s)", $3, $5, $7);
	    YYTRACE("statement: YYIF '(' expression ')' statement YYELSE statement");   
	  ;}
    break;

  case 275:

/* Line 1455 of yacc.c  */
#line 1949 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",(yyvsp[(5) - (6)].NODE),NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = (yyvsp[(3) - (6)].EXPNODE);
            cfg_top->node_type = CASECOND_EXP;
            (yyvsp[(3) - (6)].EXPNODE)->Is_top_node = true;
            _cfg_node * p_cfgtemp = (yyvsp[(5) - (6)].NODE);
            while(p_cfgtemp!=NULL)
            {
               set_next_node(p_cfgtemp,cfg_temp); 
               p_cfgtemp->next_node->bran_converge_node = cfg_temp;
               p_cfgtemp = p_cfgtemp->brother_node;          
            }  
            (yyval.NODE)=cfg_top;
            //cfg_temp=$5;
            //while(cfg_temp!=NULL){
            //  printf("++++%s \n",cfg_temp->exp_name);
            //  cfg_temp=cfg_temp->next_node;
            //}
	    //sprintf ($$, "(case %s %s)", $3, $5);
            //printf("\n--statement: YYCASE '(' expression ')' case_item_eclr YYENDCASE--\n");
	    YYTRACE("statement: YYCASE '(' expression ')' case_item_eclr YYENDCASE");
          ;}
    break;

  case 276:

/* Line 1455 of yacc.c  */
#line 1973 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",(yyvsp[(5) - (6)].NODE),NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = (yyvsp[(3) - (6)].EXPNODE);
            (yyvsp[(3) - (6)].EXPNODE)->Is_top_node = true;
            cfg_top->node_type = CASECOND_EXP;
            _cfg_node * p_cfgtemp = (yyvsp[(5) - (6)].NODE);
            while(p_cfgtemp!=NULL)
            {
               set_next_node(p_cfgtemp,cfg_temp); 
               p_cfgtemp->next_node->bran_converge_node = cfg_temp;
               p_cfgtemp = p_cfgtemp->brother_node; 
            }  

            (yyval.NODE)=cfg_top;
            //sprintf ($$, "(casez %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEZ '(' expression ')' case_item_eclr YYENDCASE"); 
          ;}
    break;

  case 277:

/* Line 1455 of yacc.c  */
#line 1992 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",(yyvsp[(5) - (6)].NODE),NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = (yyvsp[(3) - (6)].EXPNODE);
            (yyvsp[(3) - (6)].EXPNODE)->Is_top_node = true;
            cfg_top->node_type = CASECOND_EXP;
            _cfg_node * p_cfgtemp = (yyvsp[(5) - (6)].NODE);
            while(p_cfgtemp!=NULL)
            {
               set_next_node(p_cfgtemp,cfg_temp); 
               p_cfgtemp->next_node->bran_converge_node = cfg_temp;
               p_cfgtemp = p_cfgtemp->brother_node; 
            }  
            (yyval.NODE)=cfg_top;
	    //sprintf ($$, "(casex %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEX '(' expression ')' case_item_eclr YYENDCASE");
          ;}
    break;

  case 278:

/* Line 1455 of yacc.c  */
#line 2010 "vlgParser.y"
    {
              cfg_temp = build_blank_node();
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOREVER statement");
          ;}
    break;

  case 279:

/* Line 1455 of yacc.c  */
#line 2017 "vlgParser.y"
    {
              cfg_temp = build_blank_node();
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYREPEAT '(' expression ')' statement");
          ;}
    break;

  case 280:

/* Line 1455 of yacc.c  */
#line 2024 "vlgParser.y"
    {
              cfg_temp = build_blank_node();
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYWHILE '(' expression ')' statement");
          ;}
    break;

  case 281:

/* Line 1455 of yacc.c  */
#line 2031 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOR '(' assignment ';' expression ';' assignment ')' statement");
          ;}
    break;

  case 282:

/* Line 1455 of yacc.c  */
#line 2037 "vlgParser.y"
    {
              (yyval.NODE)=(yyvsp[(2) - (2)].NODE);
 	      //strcpy ($$,"");
              YYTRACE("statement: delay_control statement");
          ;}
    break;

  case 283:

/* Line 1455 of yacc.c  */
#line 2043 "vlgParser.y"
    {
              (yyval.NODE)=(yyvsp[(2) - (2)].NODE);
 	      //sprintf ($$->exp_name,"%s\n\t\t%s", $1, $2->exp_name);
              YYTRACE("statement: event_control statement");
          ;}
    break;

  case 284:

/* Line 1455 of yacc.c  */
#line 2049 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_BLK,(yyvsp[(1) - (6)].EXPNODE),NULL,(yyvsp[(5) - (6)].EXPNODE),-1,NULL);
              cfg_temp->node_type = BLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s = %s",(yyvsp[(1) - (6)].EXPNODE),(yyvsp[(5) - (6)].EXPNODE));
              (yyval.NODE)=cfg_temp;
              YYTRACE("statement: lvalue '=' delay_control expression ';'");
          ;}
    break;

  case 285:

/* Line 1455 of yacc.c  */
#line 2064 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_BLK,(yyvsp[(1) - (6)].EXPNODE),NULL,(yyvsp[(5) - (6)].EXPNODE),-1,NULL);
              cfg_temp->node_type = BLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s = %s",(yyvsp[(1) - (6)].EXPNODE),(yyvsp[(5) - (6)].EXPNODE));
              (yyval.NODE)=cfg_temp;
              YYTRACE("statement: lvalue '=' event_control expression ';'");
          ;}
    break;

  case 286:

/* Line 1455 of yacc.c  */
#line 2079 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_NBLK,(yyvsp[(1) - (6)].EXPNODE),NULL,(yyvsp[(5) - (6)].EXPNODE),-1,NULL);
              cfg_temp->node_type = NBLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s <= %s",(yyvsp[(1) - (6)].EXPNODE),(yyvsp[(5) - (6)].EXPNODE));
              (yyval.NODE)=cfg_temp;
              //printf("deduce an statement : %s",$$->exp_name);
              YYTRACE("statement: lvalue YYNBASSIGN delay_control expression ';'");
          ;}
    break;

  case 287:

/* Line 1455 of yacc.c  */
#line 2095 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_NBLK,(yyvsp[(1) - (6)].EXPNODE),NULL,(yyvsp[(5) - (6)].EXPNODE),-1,NULL);
              cfg_temp->node_type = NBLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s <= %s",(yyvsp[(1) - (6)].EXPNODE),(yyvsp[(5) - (6)].EXPNODE));
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: lvalue YYNBASSIGN event_control expression ';'");
          ;}
    break;

  case 288:

/* Line 1455 of yacc.c  */
#line 2111 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYWAIT '(' expression ')' statement");
          ;}
    break;

  case 289:

/* Line 1455 of yacc.c  */
#line 2116 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYRIGHTARROW name_of_event ';'");
          ;}
    break;

  case 290:

/* Line 1455 of yacc.c  */
#line 2121 "vlgParser.y"
    {
              (yyval.NODE)=(yyvsp[(1) - (1)].NODE);
              YYTRACE("statement: seq_block");
          ;}
    break;

  case 291:

/* Line 1455 of yacc.c  */
#line 2126 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: par_block");
          ;}
    break;

  case 292:

/* Line 1455 of yacc.c  */
#line 2131 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: task_enable");
          ;}
    break;

  case 293:

/* Line 1455 of yacc.c  */
#line 2136 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: system_task_enable");
          ;}
    break;

  case 294:

/* Line 1455 of yacc.c  */
#line 2141 "vlgParser.y"
    {
	  ;}
    break;

  case 295:

/* Line 1455 of yacc.c  */
#line 2144 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYDISABLE YYLID ';'");
          ;}
    break;

  case 296:

/* Line 1455 of yacc.c  */
#line 2149 "vlgParser.y"
    {
            cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
            cfg_temp->p_exptree_node = (yyvsp[(2) - (3)].EXPNODE);
            cfg_temp->node_type = BLK_AS;
            if(current_lvalue_node!=NULL)
            {
              cfg_temp->defined_var_chain = current_lvalue_node;
              current_lvalue_node = NULL;
              printf("\nassignment definied chain\n");
            }
            (yyval.NODE)=cfg_temp;
	    YYTRACE("statement: YYASSIGN assignment ';'");
          ;}
    break;

  case 297:

/* Line 1455 of yacc.c  */
#line 2163 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYDEASSIGN lvalue ';'");
          ;}
    break;

  case 298:

/* Line 1455 of yacc.c  */
#line 2171 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ASS_BLK,(yyvsp[(1) - (4)].EXPNODE),NULL,(yyvsp[(4) - (4)].EXPNODE),-1,NULL);
            //printf("\n~~~~~~~~~~~%s~~~~~~~~~~~\n",$$->number_string);
	    //sprintf ($$,"(%s %s)",get_wwt_entry($1),$4);
	    YYTRACE("assignment: lvalue '=' expression");
          ;}
    break;

  case 299:

/* Line 1455 of yacc.c  */
#line 2178 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ASS_NBLK,(yyvsp[(1) - (4)].EXPNODE),NULL,(yyvsp[(4) - (4)].EXPNODE),-1,NULL);     
            fprintf(cfg_out,"\n-^^^assignment: lvalue YYNBASSIGN expression-\n");       
	    //sprintf ($$,"(nonblock %s %s)",get_wwt_entry($1),$4);
	    YYTRACE("assignment: lvalue YYNBASSIGN expression");
          ;}
    break;

  case 300:

/* Line 1455 of yacc.c  */
#line 2188 "vlgParser.y"
    {
          ;}
    break;

  case 301:

/* Line 1455 of yacc.c  */
#line 2194 "vlgParser.y"
    {
            (yyval.NODE)=(yyvsp[(1) - (1)].NODE);
	    YYTRACE("case_item_eclr: case_item");
          ;}
    break;

  case 302:

/* Line 1455 of yacc.c  */
#line 2199 "vlgParser.y"
    {
            cfg_temp = (yyvsp[(1) - (2)].NODE);
            while(cfg_temp->brother_node!=NULL)
             cfg_temp = cfg_temp->brother_node; 
            cfg_temp->brother_node = (yyvsp[(2) - (2)].NODE); 
            cfg_temp->next_node->brother_node = (yyvsp[(2) - (2)].NODE)->next_node;
            (yyval.NODE)=(yyvsp[(1) - (2)].NODE);
	    //sprintf ($$,"%s %s", $1,$2);
	    YYTRACE("case_item_eclr: case_item_eclr case_item");
          ;}
    break;

  case 303:

/* Line 1455 of yacc.c  */
#line 2213 "vlgParser.y"
    {
            cfg_temp = build_normal_node("",NULL,NULL,(yyvsp[(3) - (3)].NODE),NULL);
            cfg_temp->p_exptree_node = (yyvsp[(1) - (3)].EXPNODE);
            cfg_temp->node_type = CASE_EXP;
            (yyvsp[(3) - (3)].NODE)->Is_branch_node = true;
            (yyvsp[(3) - (3)].NODE)->bran_num = get_branch_number((yyvsp[(3) - (3)].NODE));
            (yyval.NODE)=cfg_temp;
            branch_table[get_branch_number((yyvsp[(3) - (3)].NODE))]=(yyvsp[(3) - (3)].NODE);
            //while(cfg_temp!=NULL){
            fprintf(cfg_out,"^^^the case next st is %d \n",(yyvsp[(3) - (3)].NODE)->bran_num);
            // cfg_temp=cfg_temp->next_node;
            //}
            //printf("\n-case_item: expression_list ':' statement-\n");
	    YYTRACE("case_item: expression_list ':' statement");
          ;}
    break;

  case 304:

/* Line 1455 of yacc.c  */
#line 2229 "vlgParser.y"
    {
            cfg_temp = build_normal_node("default",NULL,NULL,(yyvsp[(3) - (3)].NODE),NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            (yyvsp[(3) - (3)].NODE)->Is_branch_node = true;
            (yyval.NODE)=cfg_temp;
            branch_table[get_branch_number((yyvsp[(3) - (3)].NODE))]=(yyvsp[(3) - (3)].NODE);
            YYTRACE("case_item: YYDEFAULT ':' statement");
          ;}
    break;

  case 305:

/* Line 1455 of yacc.c  */
#line 2238 "vlgParser.y"
    {
            cfg_temp = build_normal_node("default",NULL,NULL,(yyvsp[(2) - (2)].NODE),NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            (yyval.NODE)=cfg_temp;
	    YYTRACE("case_item: YYDEFAULT statement");
          ;}
    break;

  case 306:

/* Line 1455 of yacc.c  */
#line 2248 "vlgParser.y"
    {             
	    //sprintf ($$,"(%s)", $2);
            (yyval.NODE)=(yyvsp[(2) - (3)].NODE);
            //printf("=dedece to a seq block: %s\n",$$->exp_name);
            //printf("=next is: %s\n",$$->next_node->exp_name);
            //printf("=next is: %s\n",$$->next_node->next_node->exp_name);
	    YYTRACE("seq_block: YYBEGIN statement_clr YYEND");
          ;}
    break;

  case 307:

/* Line 1455 of yacc.c  */
#line 2257 "vlgParser.y"
    {
	    //sprintf ($$,"(|%s| (%s))", $3, $5);
            (yyval.NODE)=NULL;
	    YYTRACE("seq_block: YYBEGIN ':' name_of_block block_declaration_clr statement_clr YYEND");
          ;}
    break;

  case 308:

/* Line 1455 of yacc.c  */
#line 2266 "vlgParser.y"
    {
              YYTRACE("par_block: YYFORK statement_clr YYJOIN");
          ;}
    break;

  case 309:

/* Line 1455 of yacc.c  */
#line 2270 "vlgParser.y"
    {
              YYTRACE("par_block: YYFORK ':' name_of_block block_declaration_clr statement_clr YYJOIN");
          ;}
    break;

  case 310:

/* Line 1455 of yacc.c  */
#line 2277 "vlgParser.y"
    {
	    sprintf ((yyval.id), "%s", (yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_block: YYLID");
          ;}
    break;

  case 311:

/* Line 1455 of yacc.c  */
#line 2285 "vlgParser.y"
    {
              YYTRACE("block_declaration_clr:");
          ;}
    break;

  case 312:

/* Line 1455 of yacc.c  */
#line 2289 "vlgParser.y"
    {
              YYTRACE("block_declaration_clr: block_declaration_clr block_declaration");
          ;}
    break;

  case 313:

/* Line 1455 of yacc.c  */
#line 2296 "vlgParser.y"
    {
              YYTRACE("block_declaration: parameter_declaration");
          ;}
    break;

  case 314:

/* Line 1455 of yacc.c  */
#line 2300 "vlgParser.y"
    {
              YYTRACE("block_declaration: reg_declaration");
          ;}
    break;

  case 315:

/* Line 1455 of yacc.c  */
#line 2304 "vlgParser.y"
    {
              YYTRACE("block_declaration: integer_declaration");
          ;}
    break;

  case 316:

/* Line 1455 of yacc.c  */
#line 2308 "vlgParser.y"
    {
              YYTRACE("block_declaration: real_declaration");
          ;}
    break;

  case 317:

/* Line 1455 of yacc.c  */
#line 2312 "vlgParser.y"
    {
              YYTRACE("block_delcaration: time_declaration");
          ;}
    break;

  case 318:

/* Line 1455 of yacc.c  */
#line 2316 "vlgParser.y"
    {
              YYTRACE("block_declaration: event_declaration");
          ;}
    break;

  case 319:

/* Line 1455 of yacc.c  */
#line 2324 "vlgParser.y"
    {
	  ;}
    break;

  case 320:

/* Line 1455 of yacc.c  */
#line 2327 "vlgParser.y"
    {
              YYTRACE("task_enable: YYLID ';'");
          ;}
    break;

  case 321:

/* Line 1455 of yacc.c  */
#line 2331 "vlgParser.y"
    {
	  ;}
    break;

  case 322:

/* Line 1455 of yacc.c  */
#line 2334 "vlgParser.y"
    {
              YYTRACE("task_enable: YYLID '(' expression_list ')' ';'");
          ;}
    break;

  case 323:

/* Line 1455 of yacc.c  */
#line 2341 "vlgParser.y"
    {
              YYTRACE("system_task_enable: name_of_system_task ';'");
          ;}
    break;

  case 324:

/* Line 1455 of yacc.c  */
#line 2345 "vlgParser.y"
    {
              YYTRACE("system_task_enable: name_of_system_task '(' expression_list ')'");
          ;}
    break;

  case 325:

/* Line 1455 of yacc.c  */
#line 2352 "vlgParser.y"
    {
              YYTRACE("name_of_system_task: system_identifier");
          ;}
    break;

  case 326:

/* Line 1455 of yacc.c  */
#line 2361 "vlgParser.y"
    {
              YYTRACE("specify_block: YYSPECIFY specify_item_clr YYENDSPECIFY");
          ;}
    break;

  case 340:

/* Line 1455 of yacc.c  */
#line 2400 "vlgParser.y"
    {;}
    break;

  case 341:

/* Line 1455 of yacc.c  */
#line 2401 "vlgParser.y"
    {;}
    break;

  case 342:

/* Line 1455 of yacc.c  */
#line 2402 "vlgParser.y"
    {;}
    break;

  case 349:

/* Line 1455 of yacc.c  */
#line 2424 "vlgParser.y"
    {
	  ;}
    break;

  case 367:

/* Line 1455 of yacc.c  */
#line 2495 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(VAR,NULL,NULL,NULL,get_var_entry((yyvsp[(1) - (1)].id)),NULL); 
            current_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
            current_lvalue_node->var_id = get_var_entry((yyvsp[(1) - (1)].id));
            current_lvalue_node->next_node = NULL;
            current_lvalue_node->width_start = current_instance_node->p_var_table[current_lvalue_node->var_id].width_start;
            current_lvalue_node->width_end = current_instance_node->p_var_table[current_lvalue_node->var_id].width_end;
	    //sprintf ($$,get_wwt_entry($1));
	    YYTRACE("primary: identifier");
	  ;}
    break;

  case 368:

/* Line 1455 of yacc.c  */
#line 2506 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(INDEX,(yyvsp[(3) - (4)].EXPNODE),NULL,NULL,get_var_entry((yyvsp[(1) - (4)].id)),(yyvsp[(1) - (4)].id));             
	    //sprintf ($$,"(%s %s)",$3,$1);
            var_index = var_number;
            current_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
            current_lvalue_node->var_id = get_var_entry((yyvsp[(1) - (4)].id));
            current_lvalue_node->next_node = NULL;
            current_lvalue_node->width_start = var_index;
            current_lvalue_node->width_end = var_index;
	    YYTRACE("primary: identifier '[' expression ']'");
	  ;}
    break;

  case 369:

/* Line 1455 of yacc.c  */
#line 2518 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(PARSEL,(yyvsp[(3) - (6)].EXPNODE),NULL,(yyvsp[(5) - (6)].EXPNODE),get_var_entry((yyvsp[(1) - (6)].id)),(yyvsp[(1) - (6)].id));             
	    //sprintf ($$,"((%s %s) %s)",$3,$5,$1);
            current_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
            current_lvalue_node->next_node = NULL;
            current_lvalue_node->var_id = get_var_entry((yyvsp[(1) - (6)].id));
            if(((yyvsp[(3) - (6)].EXPNODE)->number_string!=NULL)&&((yyvsp[(3) - (6)].EXPNODE)->tree_node_t==CNST))
              current_lvalue_node->width_start = atoi((yyvsp[(3) - (6)].EXPNODE)->number_string);
            if(((yyvsp[(5) - (6)].EXPNODE)->number_string!=NULL)&&((yyvsp[(5) - (6)].EXPNODE)->tree_node_t==CNST))
              current_lvalue_node->width_end = atoi((yyvsp[(5) - (6)].EXPNODE)->number_string);
	    YYTRACE("primary: identifier '[' expression ':' expression ']'");
	    fprintf(cfg_out, "\n%s---primary: identifier '[' expression ':' expression ']'",(yyvsp[(1) - (6)].id));
	  ;}
    break;

  case 370:

/* Line 1455 of yacc.c  */
#line 2532 "vlgParser.y"
    {
            (yyval.EXPNODE)=(yyvsp[(1) - (1)].EXPNODE);
            //Set the current_lvaule_node recursively
            //traverse the tree to get the lvalue chain
            compute_defined_var_node((yyvsp[(1) - (1)].EXPNODE));
            YYTRACE("lvalue: concatenation");
          ;}
    break;

  case 371:

/* Line 1455 of yacc.c  */
#line 2543 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("mintypmax_expression_list: mintypmax_expression");
	  ;}
    break;

  case 372:

/* Line 1455 of yacc.c  */
#line 2548 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s", (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("mintypmax_expression_list: mintypmax_expression_list ',' mintypmax_expression");
	  ;}
    break;

  case 373:

/* Line 1455 of yacc.c  */
#line 2556 "vlgParser.y"
    {
            //$$="";
	    sprintf ((yyval.id),"");
	    YYTRACE("mintypmax_expression: expression");
	  ;}
    break;

  case 374:

/* Line 1455 of yacc.c  */
#line 2562 "vlgParser.y"
    {
            //$$="";
	    sprintf ((yyval.id),"(: )");
	    YYTRACE("mintypmax_expression: expression ':' expression");
	  ;}
    break;

  case 375:

/* Line 1455 of yacc.c  */
#line 2568 "vlgParser.y"
    {
            //$$="";
	    sprintf ((yyval.id),"(: )");
	    YYTRACE("mintypmax_expression: expression ':' expression ':' expression");
	  ;}
    break;

  case 376:

/* Line 1455 of yacc.c  */
#line 2580 "vlgParser.y"
    {
            (yyval.EXPNODE)=(yyvsp[(1) - (1)].EXPNODE);
	    //sprintf ($$,$1);
	    YYTRACE("expression_list: expression");
	  ;}
    break;

  case 377:

/* Line 1455 of yacc.c  */
#line 2586 "vlgParser.y"
    {
            //????????????????????????????????????????????????
            //Should not use the concat tree type
            //problem here
            (yyval.EXPNODE)=new_exptree_node(CONCAT,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);            
            (yyval.EXPNODE)->width_start = 1 + ((yyvsp[(1) - (3)].EXPNODE)->width_start)-((yyvsp[(1) - (3)].EXPNODE)->width_end)+((yyvsp[(3) - (3)].EXPNODE)->width_start)-((yyvsp[(3) - (3)].EXPNODE)->width_end);
            (yyval.EXPNODE)->width_end = 0; 
	    //sprintf ($$,"%s %s", $1,$3);
            printf("\nexpression_list: expression_list ',' expression\n");
	    YYTRACE("expression_list: expression_list ',' expression");
	  ;}
    break;

  case 378:

/* Line 1455 of yacc.c  */
#line 2601 "vlgParser.y"
    {
            (yyval.EXPNODE)=(yyvsp[(2) - (3)].EXPNODE);
	    YYTRACE("expression: (expression)");
          ;}
    break;

  case 379:

/* Line 1455 of yacc.c  */
#line 2606 "vlgParser.y"
    {
            (yyval.EXPNODE)=(yyvsp[(1) - (1)].EXPNODE);
	    //sprintf ($$,$1);
	    YYTRACE("expression: primary");
	  ;}
    break;

  case 380:

/* Line 1455 of yacc.c  */
#line 2612 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNOT,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(not %s)",$2);
	    YYTRACE("expression: '!' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 381:

/* Line 1455 of yacc.c  */
#line 2618 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNEG,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(neg %s)",$2);
	    YYTRACE("expression: '~' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 382:

/* Line 1455 of yacc.c  */
#line 2624 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGAND,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uand %s)",$2);
	    YYTRACE("expression: '&' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 383:

/* Line 1455 of yacc.c  */
#line 2630 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uor %s)",$2);
	    YYTRACE("expression: '|' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 384:

/* Line 1455 of yacc.c  */
#line 2636 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGXOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uxor %s,",$2);
	    YYTRACE("expression: '^' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 385:

/* Line 1455 of yacc.c  */
#line 2642 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNAND,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognand %s)",$2);
	    YYTRACE("expression: YYLOGNAND primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 386:

/* Line 1455 of yacc.c  */
#line 2648 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognor %s)",$2);
	    YYTRACE("expression: YYLOGNOR primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 387:

/* Line 1455 of yacc.c  */
#line 2654 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGXNOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulogxnor %s)",$2);
	    YYTRACE("expression: YYLOGXNOR primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 388:

/* Line 1455 of yacc.c  */
#line 2660 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(BADD,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(+ %s %s)",$1,$3);
	    YYTRACE("expression: expression '+' expression");
	  ;}
    break;

  case 389:

/* Line 1455 of yacc.c  */
#line 2666 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(BSUB,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(- %s %s)",$1,$3);
	    YYTRACE("expression: expressio '-' expression");
	  ;}
    break;

  case 390:

/* Line 1455 of yacc.c  */
#line 2672 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(BMUL,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(* %s %s)",$1,$3);
	    YYTRACE("expression: expression '*' expression");
	  ;}
    break;

  case 391:

/* Line 1455 of yacc.c  */
#line 2678 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(BDIV,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(/ %s %s)",$1,$3);
	    YYTRACE("expression: expression '/' expression");
	  ;}
    break;

  case 392:

/* Line 1455 of yacc.c  */
#line 2684 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(BMOD,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(%% %s %s)",$1,$3);
	    YYTRACE("expression: expression '%' expression");
	  ;}
    break;

  case 393:

/* Line 1455 of yacc.c  */
#line 2690 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LOGEQ,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(logequal %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOgEQUALITY expression");
	  ;}
    break;

  case 394:

/* Line 1455 of yacc.c  */
#line 2696 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LOGINEQ,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(loginequal %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGINEQUALITY expression");
	  ;}
    break;

  case 395:

/* Line 1455 of yacc.c  */
#line 2702 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LOGAND,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL); 
            //printf("\n+++1->idx = %d;+++ 2->idx = %d\n",$$->left->var_table_idx,$$->right->var_table_idx);            
	    //sprintf ($$,"(logand %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGAND expression");
	  ;}
    break;

  case 396:

/* Line 1455 of yacc.c  */
#line 2709 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LOGOR,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(lognor %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGOR expression");
	  ;}
    break;

  case 397:

/* Line 1455 of yacc.c  */
#line 2715 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LES,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(< %s %s)",$1,$3);
	    YYTRACE("expression: expression '<' expression");
	  ;}
    break;

  case 398:

/* Line 1455 of yacc.c  */
#line 2721 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(GAR,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(> %s %s)",$1,$3);
	    YYTRACE("expression: expression '>' expression");
	  ;}
    break;

  case 399:

/* Line 1455 of yacc.c  */
#line 2727 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(SAND,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(and %s %s)",$1,$3);
	    YYTRACE("expression: expression '&' expression");
	  ;}
    break;

  case 400:

/* Line 1455 of yacc.c  */
#line 2733 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(SOR,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(or %s %s)",$1,$3);
	    YYTRACE("expression: expression '|' expression");
	  ;}
    break;

  case 401:

/* Line 1455 of yacc.c  */
#line 2739 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(SXOR,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(xor %s %s)",$1,$3);
	    YYTRACE("expression: expression '^' expression");
	  ;}
    break;

  case 402:

/* Line 1455 of yacc.c  */
#line 2745 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LEQ,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(leq %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLEQ expression");
	  ;}
    break;

  case 403:

/* Line 1455 of yacc.c  */
#line 2751 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LEQ,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(leq %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLEQ expression");
	  ;}
    break;

  case 404:

/* Line 1455 of yacc.c  */
#line 2757 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(GEQ,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);            
            //printf("--------%s---------",$$->number_string); 
	    //sprintf ($$,"(geq %s %s)",$1,$3);
	    YYTRACE("expression: expression YYGEQ expression");
	  ;}
    break;

  case 405:

/* Line 1455 of yacc.c  */
#line 2764 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LSFT,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(<< %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLSHIFT expression");
	  ;}
    break;

  case 406:

/* Line 1455 of yacc.c  */
#line 2770 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(RSFT,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(>> %s %s)",$1,$3);
	    YYTRACE("expression: expression YYRSHIFT expression");
	  ;}
    break;

  case 407:

/* Line 1455 of yacc.c  */
#line 2776 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(LOGXNOR,(yyvsp[(1) - (3)].EXPNODE),NULL,(yyvsp[(3) - (3)].EXPNODE),-1,NULL);             
	    //sprintf ($$,"(logxnor %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGXNOR expression");
	  ;}
    break;

  case 408:

/* Line 1455 of yacc.c  */
#line 2782 "vlgParser.y"
    {
	    //sprintf ($$,"(if %s %s %s)",$1,$3,$5,-1,NULL);
            (yyval.EXPNODE)=new_exptree_node(ITE_E,(yyvsp[(1) - (5)].EXPNODE),(yyvsp[(3) - (5)].EXPNODE),(yyvsp[(5) - (5)].EXPNODE),-1,NULL);             
	    YYTRACE("expression: expression '?' expression ':' expression");
	  ;}
    break;

  case 409:

/* Line 1455 of yacc.c  */
#line 2789 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNOT,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(not %s)",$2);
	    YYTRACE("expression: '!' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 410:

/* Line 1455 of yacc.c  */
#line 2795 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNEG,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(neg %s)",$2);
	    YYTRACE("expression: '~' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 411:

/* Line 1455 of yacc.c  */
#line 2801 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGAND,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uand %s)",$2);
	    YYTRACE("expression: '&' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 412:

/* Line 1455 of yacc.c  */
#line 2807 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uor %s)",$2);
	    YYTRACE("expression: '|' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 413:

/* Line 1455 of yacc.c  */
#line 2813 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGXOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uxor %s,",$2);
	    YYTRACE("expression: '^' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 414:

/* Line 1455 of yacc.c  */
#line 2819 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNAND,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognand %s)",$2);
	    YYTRACE("expression: YYLOGNAND primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 415:

/* Line 1455 of yacc.c  */
#line 2825 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGNOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognor %s)",$2);
	    YYTRACE("expression: YYLOGNOR primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 416:

/* Line 1455 of yacc.c  */
#line 2831 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(ULOGXNOR,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulogxnor %s)",$2);
	    YYTRACE("expression: YYLOGXNOR primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 417:

/* Line 1455 of yacc.c  */
#line 2841 "vlgParser.y"
    { 
            (yyval.EXPNODE)=new_exptree_node(CNST,NULL,NULL,NULL,-1,(yyvsp[(1) - (1)].id));             
	    //sprintf ($$,$1);
            var_number = atoi((yyvsp[(1) - (1)].id));
            (yyval.EXPNODE)->width_start = 31;
            (yyval.EXPNODE)->width_end = 0;
            //printf("\n-----the var number is %d--------\n",var_number);
	    YYTRACE("primary: YYINUMBER");
	  ;}
    break;

  case 418:

/* Line 1455 of yacc.c  */
#line 2851 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(CNST,NULL,NULL,NULL,-1,NULL);
            //==========================================================
            (yyval.EXPNODE)->width_start = get_number_width((yyvsp[(1) - (1)].id))-1; //get the width of the number
            (yyval.EXPNODE)->width_end = 0;
            (yyval.EXPNODE)->number_string = get_number_string((yyvsp[(1) - (1)].id)); //translate to decimal
	    //printf ("\n----%s-----\n",$$->number_string);
	    YYTRACE("primary: YYINUMBER_BIT");
	  ;}
    break;

  case 419:

/* Line 1455 of yacc.c  */
#line 2861 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(VAR,NULL,NULL,NULL,get_var_entry((yyvsp[(1) - (1)].id)),NULL);             
	    //sprintf ($$,get_wwt_entry($1));
            //printf("\n=======================the identifier is %s",$1);
	    YYTRACE("primary: identifier");
	  ;}
    break;

  case 420:

/* Line 1455 of yacc.c  */
#line 2868 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(INDEX,(yyvsp[(3) - (4)].EXPNODE),NULL,NULL,get_var_entry((yyvsp[(1) - (4)].id)),(yyvsp[(1) - (4)].id));             
            if(((yyvsp[(3) - (4)].EXPNODE)->tree_node_t==CNST)&&((yyvsp[(3) - (4)].EXPNODE)->number_string!=NULL))
            {
              (yyval.EXPNODE)->width_start = atoi((yyvsp[(3) - (4)].EXPNODE)->number_string);
              (yyval.EXPNODE)->width_end = atoi((yyvsp[(3) - (4)].EXPNODE)->number_string);
            }
	    //sprintf ($$,"(%s %s)",$3,$1);
	    YYTRACE("primary: identifier '[' expression ']'");
	  ;}
    break;

  case 421:

/* Line 1455 of yacc.c  */
#line 2879 "vlgParser.y"
    {
            (yyval.EXPNODE)=new_exptree_node(PARSEL,(yyvsp[(3) - (6)].EXPNODE),NULL,(yyvsp[(5) - (6)].EXPNODE),get_var_entry((yyvsp[(1) - (6)].id)),(yyvsp[(1) - (6)].id));             
            if(((yyvsp[(3) - (6)].EXPNODE)->tree_node_t==CNST)&&((yyvsp[(3) - (6)].EXPNODE)->number_string!=NULL))
              (yyval.EXPNODE)->width_start = atoi((yyvsp[(3) - (6)].EXPNODE)->number_string);
            if(((yyvsp[(5) - (6)].EXPNODE)->tree_node_t==CNST)&&((yyvsp[(5) - (6)].EXPNODE)->number_string!=NULL))
              (yyval.EXPNODE)->width_end = atoi((yyvsp[(5) - (6)].EXPNODE)->number_string);
	    //sprintf ($$,"((%s %s) %s)",$3,$5,$1);
	    YYTRACE("primary: identifier '[' expression ':' expression ']'");
	  ;}
    break;

  case 422:

/* Line 1455 of yacc.c  */
#line 2889 "vlgParser.y"
    {
            (yyval.EXPNODE)=(yyvsp[(1) - (1)].EXPNODE);
            //printf("\n--primay: concatenation--\n");
            YYTRACE("primary: concatenation");
          ;}
    break;

  case 423:

/* Line 1455 of yacc.c  */
#line 2920 "vlgParser.y"
    {
            //$$=new_exptree_node(CONCAT, $2, NULL, NULL, -1, NULL);
            //$$->width_start = $2->width_start;
            //$$->width_end = $2->width_end;
            (yyval.EXPNODE)=(yyvsp[(2) - (3)].EXPNODE);
            (yyval.EXPNODE)->tree_node_t = CONCAT;
            //printf("\nconcatenation: '{' expression_list '}'\n");
            //fprintf(cfg_out, "\n width start = %d, width end = %d \n", $$->width_start, $$->width_end);
	    YYTRACE("concatenation: '{' expression_list '}'");
	  ;}
    break;

  case 424:

/* Line 1455 of yacc.c  */
#line 2931 "vlgParser.y"
    {
            _exptree_node * p_node_num_tmp;
            p_node_num_tmp=new_exptree_node(CNST,NULL,NULL,NULL,-1,(yyvsp[(2) - (6)].id));             
            p_node_num_tmp->width_start = 31;
            p_node_num_tmp->width_end = 0;
            (yyval.EXPNODE)=new_exptree_node(CATCOPY, p_node_num_tmp, NULL, (yyvsp[(4) - (6)].EXPNODE), -1, NULL);
            (yyval.EXPNODE)->width_start = (atoi((yyvsp[(2) - (6)].id)))*(1+((yyvsp[(4) - (6)].EXPNODE)->width_start)-((yyvsp[(4) - (6)].EXPNODE)->width_end))-1;
            (yyval.EXPNODE)->width_end = 0;
          ;}
    break;

  case 426:

/* Line 1455 of yacc.c  */
#line 2967 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("identifier: YYLID");
          ;}
    break;

  case 427:

/* Line 1455 of yacc.c  */
#line 2972 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(element %s %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("identifier: identifier '.' YYLID");
          ;}
    break;

  case 428:

/* Line 1455 of yacc.c  */
#line 2980 "vlgParser.y"
    {
	      YYTRACE("delay_opt:");
	  ;}
    break;

  case 429:

/* Line 1455 of yacc.c  */
#line 2984 "vlgParser.y"
    {
	      YYTRACE("delay_opt: delay");
	  ;}
    break;

  case 430:

/* Line 1455 of yacc.c  */
#line 2991 "vlgParser.y"
    {
	      YYTRACE("delay: '#' YYINUMBER");
	  ;}
    break;

  case 431:

/* Line 1455 of yacc.c  */
#line 2995 "vlgParser.y"
    {
	      YYTRACE("delay: '#' YYINUMBER_BIT");
	  ;}
    break;

  case 432:

/* Line 1455 of yacc.c  */
#line 2999 "vlgParser.y"
    {
	      YYTRACE("delay: '#' YYRNUMBER");
	  ;}
    break;

  case 433:

/* Line 1455 of yacc.c  */
#line 3003 "vlgParser.y"
    {
	      YYTRACE("delay: '#' identifier");
	  ;}
    break;

  case 434:

/* Line 1455 of yacc.c  */
#line 3007 "vlgParser.y"
    {
	      YYTRACE("delay: '#' '(' mintypmax_expression ')'");
	  ;}
    break;

  case 435:

/* Line 1455 of yacc.c  */
#line 3011 "vlgParser.y"
    {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ')'");
	  ;}
    break;

  case 436:

/* Line 1455 of yacc.c  */
#line 3016 "vlgParser.y"
    {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ',' mintypmax_expression ')'");
	  ;}
    break;

  case 437:

/* Line 1455 of yacc.c  */
#line 3023 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' YYINUMBER");
	  ;}
    break;

  case 438:

/* Line 1455 of yacc.c  */
#line 3027 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' YYINUMBER_BIT");
	  ;}
    break;

  case 439:

/* Line 1455 of yacc.c  */
#line 3031 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' YYRNUMBER");
	  ;}
    break;

  case 440:

/* Line 1455 of yacc.c  */
#line 3035 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' identifier");
	  ;}
    break;

  case 441:

/* Line 1455 of yacc.c  */
#line 3039 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' '(' mintypmax_expression ')'");
	  ;}
    break;

  case 442:

/* Line 1455 of yacc.c  */
#line 3046 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(event_control %s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("event_control: '@' identifier");
	  ;}
    break;

  case 443:

/* Line 1455 of yacc.c  */
#line 3051 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(event_control %s)", (yyvsp[(3) - (4)].id));
	    YYTRACE("event_control: '@' '(' event_expression ')'");
	  ;}
    break;

  case 444:

/* Line 1455 of yacc.c  */
#line 3056 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(event_control %s)", (yyvsp[(3) - (4)].id));
	    YYTRACE("event_control: '@' '(' ored_event_expression ')'");
	  ;}
    break;

  case 445:

/* Line 1455 of yacc.c  */
#line 3065 "vlgParser.y"
    {
	    sprintf ((yyval.id), "");
            current_exptree_event = (yyvsp[(1) - (1)].EXPNODE);
	    YYTRACE("event_expression: expression");
	  ;}
    break;

  case 446:

/* Line 1455 of yacc.c  */
#line 3071 "vlgParser.y"
    {
            current_exptree_event = new_exptree_node(POSEGE,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);
	    sprintf ((yyval.id), "(posedge )");
	    YYTRACE("event_expression: YYPOSEDGE expression");
	  ;}
    break;

  case 447:

/* Line 1455 of yacc.c  */
#line 3077 "vlgParser.y"
    {
            current_exptree_event = new_exptree_node(NEGEGE,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);
	    sprintf ((yyval.id), "(negedge )");
	    YYTRACE("event_expression: YYNEGEDGE expression");
	  ;}
    break;

  case 448:

/* Line 1455 of yacc.c  */
#line 3083 "vlgParser.y"
    {
            current_exptree_event = new_exptree_node(EDGE,(yyvsp[(2) - (2)].EXPNODE),NULL,NULL,-1,NULL);
	    sprintf ((yyval.id), "(edge )");
	    YYTRACE("event_expression: YYEDGE expression");
          ;}
    break;

  case 449:

/* Line 1455 of yacc.c  */
#line 3092 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s %s %s)", (yyvsp[(2) - (3)].id), (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("ored_event_expression: event_expression YYOR event_expression");
	  ;}
    break;

  case 450:

/* Line 1455 of yacc.c  */
#line 3097 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s %s %s)", (yyvsp[(2) - (3)].id), (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("ored_event_expression: ored_event_expression YYOR event_expression");
	  ;}
    break;



/* Line 1455 of yacc.c  */
#line 7203 "vlgParser.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 3103 "vlgParser.y"


void yyerror(char * str)
{
    fprintf(stderr, str);
    exit (1);
}

void YYTRACE(char * str)
{
   //return;
   fprintf(cfg_out,"---%s\n",str);
}

_modulestats * make_module (char *name) {
  _modulestats *newmodule;

  if (modstats == NULL) {
    modstats = (_modulestats *) malloc (sizeof (struct __modulestats));
    modstats->next = NULL;
    modstats->numports = 0;
    modstats->numinputs = 0;
    modstats->numoutputs = 0;
    modstats->numregs = 0;
    modstats->numwires = 0;
    return modstats;
  }
  newmodule = modstats;
  while (newmodule->next != NULL) newmodule = newmodule->next;
  newmodule->next = (_modulestats *) malloc (sizeof (struct __modulestats));
  newmodule->next->next = NULL;
  newmodule->next->numports = 0;
  newmodule->next->numinputs = 0;
  newmodule->next->numoutputs = 0;
  newmodule->next->numregs = 0;
  newmodule->next->numwires = 0;
  return newmodule->next;
}

char *get_string_from_var (_var *v) {
  char *outstr;
  int i;
  outstr = (char *) malloc (MAXSTRLEN*(v->end-v->start+1));
  if (v->end-v->start) {
    strcpy (outstr,"");
    for (i=v->start; i<=v->end; i++)
      sprintf (outstr, "%s |%s%d|", outstr, v->name, i);
  } 
  else sprintf (outstr,v->name);
  return outstr;
}

char *get_string_from_varlist (_var **v, int n) {
  char *retstr;
  int i;
  retstr = NULL;
  if (n<=0) return "";
  sprintf (retstr,get_string_from_var(v[0]));
  for (i=1; i<n; i++)
    sprintf (retstr, "%s%s", retstr, get_string_from_var(v[i]));
  return retstr;
}

void print_modstats (void) {
  fprintf (dbg_out, "\t   (ins  %s)\n", get_string_from_varlist (thismodule->input_list, thismodule->numinputs));
  fprintf (dbg_out, "\t   (sts  %s)\n", get_string_from_varlist (thismodule->reg_list, thismodule->numregs));
  fprintf (dbg_out, "\t   (outs  %s)\n", get_string_from_varlist (thismodule->output_list, thismodule->numoutputs));
  fprintf (dbg_out, "\t   (wires  %s)\n", get_string_from_varlist (thismodule->wire_list, thismodule->numwires));
  fprintf (dbg_out, "\t   (occs  %s)\n","");
}

void put_wwt_entry (char *name, char *range) {
  struct wire_width_tuple t;
  strcpy (t.name, name);
  strcpy (t.range, range);
  wire_width_table[num_width_table_entries++] = t;
  return;
}

char *get_wwt_entry (char *name) {
  int i;
  char *rstr = (char *) malloc (MAXSTRLEN);
  for (i=0; i<num_width_table_entries; i++) 
    if (strcmp (wire_width_table[i].name, name)==0) {
      sprintf (rstr, "(%s %s)", wire_width_table[i].range, wire_width_table[i].name);
      return rstr;
    }
  return name;
}

void print_wwt (void) {
  printf ("\n");printf ("\n");
  for (int i=0; i<num_width_table_entries; i++) 
    printf ("(%s %s)\n", wire_width_table[i].name, wire_width_table[i].range);
  printf ("\n");
}

//============================================
//============for building CFG================
//===========liu187@illinois.edu==============
_cfg_node * build_blank_node( )
{ 
  _cfg_node * temp;
  int idx;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, "blank");
  temp->branch_number = 0;
  temp->Is_branch_node = false;
  temp->Is_exit_branch = false;
  temp->frame_num_exhaust = 1000;
  temp->Is_traversed = false;
  temp->node_type = NULL_NODE;
  temp->left_node = NULL;
  temp->right_node = NULL;
  temp->brother_node = NULL;
  temp->next_node = NULL;

  temp->bran_converge_node = NULL;
  temp->bran_num = -1;
  temp->p_dep_list = NULL;
  //temp->Is_covered = 0;
  temp->temp_flag = false;
 
  temp->used_var_chain = NULL;
  temp->defined_var_chain = NULL;
  temp->p_exptree_node = NULL;
  for (idx=0;idx<MAXFRAMES;idx++)
  {
     temp->branch_taken[idx]=false;
     temp->guard_negated[idx]=false;
     temp->exhaust_bran[idx]=false;
     temp->Is_covered[idx]=0;
  }
  return temp;
}

_cfg_node * build_normal_node(char exp[], _cfg_node * left, _cfg_node * right, _cfg_node * next, _cfg_node * brother)
{
  _cfg_node * temp;
  int idx;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, exp);
  temp->left_node = left;
  temp->branch_number = 0;
  temp->Is_branch_node = false;
  temp->Is_exit_branch = false;
  temp->frame_num_exhaust = 1000;
  temp->Is_traversed = false;
  temp->right_node = right;
  temp->brother_node = brother;
  temp->next_node = next; 

  temp->bran_converge_node = NULL;
  temp->bran_num = -1; 
  temp->p_dep_list = NULL;
  //temp->Is_covered = 0;
  temp->temp_flag = false;
  temp->used_var_chain = NULL;
  temp->p_exptree_node = NULL;
  temp->defined_var_chain = NULL;
  for (idx=0;idx<MAXFRAMES;idx++)
  {
     temp->branch_taken[idx]=false;
     temp->guard_negated[idx]=false;
     temp->exhaust_bran[idx]=false;
     temp->Is_covered[idx]=0;
  }
  return temp;
}

int set_next_node(_cfg_node * cfg_x, _cfg_node * cfg_nxt)
{
  _cfg_node * temp;
  temp = cfg_x;
  while(temp->next_node!=NULL)
   temp = temp->next_node;
  temp->next_node = cfg_nxt;
  return 1;
}

//finish a cfg building
int push_cfg_table(_cfg_node * cfg_x)
{
  _cfg_block * p_index = current_instance_node->p_cfg_block;
  _cfg_block * p_temp = (_cfg_block *)malloc(sizeof(_cfg_block));
  p_temp->event_trigger_exp = current_exptree_event;
  p_temp->p_cfg_node = cfg_x;
  p_temp->cfg_block_next = NULL;
  if(p_index == NULL)
    current_instance_node->p_cfg_block = p_temp;
  else 
  {
    while(p_index->cfg_block_next!=NULL) 
      p_index = p_index->cfg_block_next;
    p_index->cfg_block_next = p_temp;
  }
  return 1;
}

void print_all()
{
  _cfg_block * p_temp = current_instance_node->p_cfg_block;
  while(p_temp!=NULL)
  { 
    print_cfg(p_temp->p_cfg_node);
    p_temp = p_temp->cfg_block_next;
  }
}

void print_cfg(_cfg_node * cfg_x)
{
  _cfg_node * cfg_bfs[1000];
  int rd_pointer=0;
  int wr_pointer=0;
  cfg_bfs[wr_pointer++]=cfg_x;
  //make a breadth first search
  while(rd_pointer!=wr_pointer)
  {
    cfg_x = cfg_bfs[rd_pointer];
    rd_pointer=rd_pointer+1;
    rd_pointer=rd_pointer%1000;
    
    if(((cfg_x->p_exptree_node)!=NULL)&&(cfg_x->Is_branch_node==false))
    {
      //&&(cfg_x->p_exptree_node->number_string!=NULL)
      //fprintf(cfg_out,"\n=====the exptree node is not null: type %d, Is traversed %d======\n",cfg_x->node_type,cfg_x->Is_traversed);
      if(cfg_x->Is_traversed==false)
        print_exptree_node(cfg_x->p_exptree_node);
      fprintf(cfg_out,"\n======from the print exptree_node : %s======\n",cfg_x->p_exptree_node->number_string);
      current_cfg_node = cfg_x;
      if(current_cfg_node->used_var_chain==NULL)
      {
         if((current_cfg_node->node_type==IFCOND_EXP)||(current_cfg_node->node_type==IFELSE_EXP)||(current_cfg_node->node_type==CASECOND_EXP)||(current_cfg_node->node_type==CASE_EXP))
           compute_used_var_node(current_cfg_node->p_exptree_node); 
         else if(((current_cfg_node->node_type==BLK_AS)||(current_cfg_node->node_type==NBLK_AS))&&(current_cfg_node->p_exptree_node->right!=NULL))
           compute_used_var_node(current_cfg_node->p_exptree_node->right); 
      }   
      //printf("\n======Node traversed: %s\n========",cfg_x->p_exptree_node->number_string);
      cfg_x->Is_traversed=true;
    }
    if((cfg_x->left_node!=NULL)&&(cfg_x->left_node->temp_flag==false)){
      cfg_bfs[wr_pointer]=cfg_x->left_node;
      cfg_x->left_node->temp_flag = true;
      wr_pointer++;
      wr_pointer=wr_pointer%1000;
      //continue;
    }
    if((cfg_x->brother_node!=NULL)&&(cfg_x->brother_node->temp_flag==false)){
      cfg_bfs[wr_pointer]=cfg_x->brother_node;
      cfg_x->brother_node->temp_flag = true;
      wr_pointer++;
      wr_pointer=wr_pointer%1000;
    }
    if((cfg_x->next_node!=NULL)&&(cfg_x->next_node->temp_flag==false)){
      cfg_bfs[wr_pointer]=cfg_x->next_node;
      cfg_x->next_node->temp_flag = true;
      wr_pointer++;
      wr_pointer=wr_pointer%1000;
     }
  } 
}

void add_to_cfg_defined_chain(int var_idx,int width_start,int width_end)
{
    _var_node * p_lvalue_node;
    p_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
    p_lvalue_node->var_id = var_idx;
    p_lvalue_node->next_node = NULL;
    p_lvalue_node->width_start = width_start;
    p_lvalue_node->width_end = width_end;
    p_lvalue_node->next_node = current_lvalue_node;
    current_lvalue_node = p_lvalue_node;
    //sprintf ($$,get_wwt_entry($1));
}

void compute_defined_var_node(_exptree_node * p_exp_treenode)
{
  if(p_exp_treenode==NULL)
    return;
    if(p_exp_treenode->tree_node_t==PARSEL)
  {
    add_to_cfg_defined_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==INDEX)
  {
    add_to_cfg_defined_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==VAR)
  {
    add_to_cfg_defined_chain(p_exp_treenode->var_table_idx,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_start,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_end); 
  }
  else
  {
    if(p_exp_treenode->left!=NULL)
      compute_defined_var_node(p_exp_treenode->left); 
    if(p_exp_treenode->middle!=NULL)
      compute_defined_var_node(p_exp_treenode->middle);
    if(p_exp_treenode->right!=NULL)
      compute_defined_var_node(p_exp_treenode->right); 
  }  
}


void compute_used_var_node(_exptree_node * p_exp_treenode)
{
  if(p_exp_treenode==NULL)
    return;
  //if(p_exp_treenode->number_string!=NULL)
  //  fprintf(cfg_out,"\n####the computed statement is %s: type %d###\n",p_exp_treenode->number_string,p_exp_treenode->tree_node_t);
  //return; 
  //add_to_cfg_used_chain()
  if(p_exp_treenode->tree_node_t==PARSEL)
  {
    add_to_cfg_used_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==INDEX)
  {
    add_to_cfg_used_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==VAR)
  {
    //fprintf(cfg_out,"\n$$$$the computed statement is %s: type %d###\n",p_exp_treenode->number_string,p_exp_treenode->tree_node_t);
    add_to_cfg_used_chain(p_exp_treenode->var_table_idx,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_start,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_end); 
  }
  else
  {
    if(p_exp_treenode->left!=NULL)
      compute_used_var_node(p_exp_treenode->left); 
    if(p_exp_treenode->middle!=NULL)
      compute_used_var_node(p_exp_treenode->middle);
    if(p_exp_treenode->right!=NULL)
      compute_used_var_node(p_exp_treenode->right); 
  }  //*/
}

void print_exptree_node(_exptree_node * p_exp_treenode)
{
  char * new_var_string;
  //post order to traverse the expression tree
  fprintf(cfg_out,"\n=enter the print_exptree_node=\n");
  if((p_exp_treenode->left!=NULL)&&(p_exp_treenode->left->Is_traversed==false))
  {
    //if(p_exp_treenode->left->Is_traversed==false) 
      fprintf(cfg_out,"\n=enter the print_exptree_node: left=\n");
      print_exptree_node(p_exp_treenode->left);
  } 
  if  ((p_exp_treenode->middle!=NULL)&&(p_exp_treenode->middle->Is_traversed==false))
  { 
    //if(p_exp_treenode->middle->Is_traversed==false) 
      fprintf(cfg_out,"\n=enter the print_exptree_node: middle=\n");
      print_exptree_node(p_exp_treenode->middle);
  }
  if ((p_exp_treenode->right!=NULL)&&(p_exp_treenode->right->Is_traversed==false))
  {
    //if(p_exp_treenode->right->Is_traversed==false)
      fprintf(cfg_out,"\n=enter the print_exptree_node: right=\n");
      print_exptree_node(p_exp_treenode->right);
  }
  fprintf(cfg_out,"\n=enter the print_exptree_node=: self\n");
  
    if(p_exp_treenode->left!=NULL) 
      p_exp_treenode->left->Is_traversed=false;
    if(p_exp_treenode->middle!=NULL) 
      p_exp_treenode->middle->Is_traversed=false;
    if(p_exp_treenode->right!=NULL) 
      p_exp_treenode->right->Is_traversed=false;

    p_exp_treenode->Is_traversed = true;

    if(p_exp_treenode->number_string==NULL)
      p_exp_treenode->number_string = (char *)malloc(50*sizeof(char));
  
    //fprintf(cfg_out,"\n=enter the print_exptree_node=: here\n");

    //traverse this node
    switch(p_exp_treenode->tree_node_t)
    {
      case GEQ      :   // a>=b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s >= %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s >= %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 
           
           fprintf(cfg_out,"\n%s = (%s >= %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LEQ      :   // a<=b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s <= %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s <= %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0;
 
           fprintf(cfg_out,"\n%s = (%s <= %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGAND   :   // a&&b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s && %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s && %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s && %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGOR    :   // a||b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s || %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s || %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s || %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGEQ    :   // a==b 
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s == %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s == %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s == %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGINEQ  :   // a!=b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s != %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s != %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s != %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LSFT     :   // a<<b  
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start= p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end= p_exp_treenode->left->width_end ; 

           fprintf(cfg_out,"\n%s = (%s << %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case RSFT     :   // a>>b
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start= p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end= p_exp_treenode->left->width_end ; 

           fprintf(cfg_out,"\n%s = (%s >> %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case ULOGNOT  :   // !a 
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ! %s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"! (%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

           fprintf(cfg_out,"\n%s = (!%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGNEG  :   // ~a 
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start- p_exp_treenode->left->width_end;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
         
           fprintf(cfg_out,"\n%s = (~%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGAND  :   //&a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n &%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"&(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (&%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGOR   :   //|a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n |%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"|(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (|%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGXOR  :   //^a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ^%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"^(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (^%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //} 
        break;
      case ULOGNAND :   //~&a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~&%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~&(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (~&%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGNOR  :   //~|a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~|%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~|(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (~|%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGXNOR :   //~^a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~^%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~^(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (~^%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case BADD     :   //a+b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         if(p_exp_treenode->left->width_start>p_exp_treenode->left->width_end)
         {
           p_exp_treenode->width_start = p_exp_treenode->left->width_start-p_exp_treenode->left->width_end;
           p_exp_treenode->width_end = 0;
         }
         else
         {
           printf("FATAL ERROR in A+B");
         }
         //p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         //p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->width_end; 

         fprintf(cfg_out,"\n%s = (%s + %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BSUB     :   //a-b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s - %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BMUL     :   //a*b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s * %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BDIV     :   //a/b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s / %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BMOD     :   //a%b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s \% %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case GAR      :   //a>b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s > %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s > %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start = 0; 
           current_var_table[var_table_id-1].width_end = 0;
 
           fprintf(cfg_out,"\n%s = (%s > %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LES      :   //a<b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s < %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s < %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start = 0; 
           current_var_table[var_table_id-1].width_end = 0;

           fprintf(cfg_out,"\n%s = (%s < %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case SAND     :   //a&b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s & %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s & %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start - p_exp_treenode->left->width_end;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s & %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case SOR      :   //a|b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s | %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s | %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s | %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case SXOR     :   //a^b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s ^ %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s ^ %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s ^ %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGXNOR  :   //a^b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s ~^ %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s ~^ %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s ~^ %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ASS_BLK  :   //a=b
        //new_var_string = new_intermediate_variable();
        //p_exp_treenode->var_table_idx = var_table_id-1;
        //p_exp_treenode->width_start = 0;
        //p_exp_treenode->width_end = 0;
        //current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
        //current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
        printf("\n %s = %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        fprintf(cfg_out,"\n %s = %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s = %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case ASS_NBLK :   //a<=b
        //new_var_string = new_intermediate_variable();
        //p_exp_treenode->var_table_idx = var_table_id-1;
        //p_exp_treenode->width_start = 0;
        //p_exp_treenode->width_end = 0;
        //current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
        //current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end;
 
        fprintf(cfg_out,"\n %s <= %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s <= %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case NAME_PORT:   //.a(b)
        fprintf(cfg_out,"\n %s = %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s = %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case POSEGE   :   //@(posedge a)
        fprintf(cfg_out,"\n @posedge"); 
        break;
      case NEGEGE   :   //@(negedge a)
        fprintf(cfg_out,"\n @negedge"); 
        break;
      case EDGE     :   //@(edge a)
        fprintf(cfg_out,"\n @edge"); 
        break;
      case ITE_E      :   // a ? b : c
        new_var_string = new_intermediate_variable();

        p_exp_treenode->var_table_idx = var_table_id-1;
        p_exp_treenode->width_start = p_exp_treenode->right->width_start;
        p_exp_treenode->width_end = p_exp_treenode->right->width_end;
        current_var_table[var_table_id-1].width_start=p_exp_treenode->right->width_start; 
        current_var_table[var_table_id-1].width_end=p_exp_treenode->right->width_end; 

        fprintf(cfg_out,"\n%s = %s ? (%s : %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->middle->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case PARSEL   :   // a[b:c]
        fprintf(cfg_out,"\n--In PARSEL--\n");
        p_exp_treenode->width_start = atoi(p_exp_treenode->left->number_string);
        p_exp_treenode->width_end  = atoi(p_exp_treenode->right->number_string);
        fprintf(cfg_out,"\n %s[%s:%s] ",p_exp_treenode->number_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s[%s:%s]",p_exp_treenode->number_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case INDEX    :   // a[b]
        p_exp_treenode->width_start = atoi(p_exp_treenode->left->number_string);
        p_exp_treenode->width_end = atoi(p_exp_treenode->left->number_string);
        fprintf(cfg_out,"\n %s[%s] ",p_exp_treenode->number_string,p_exp_treenode->left->number_string); 
        sprintf(p_exp_treenode->number_string,"%s[%s]",p_exp_treenode->number_string,p_exp_treenode->left->number_string); 
        break;
      case CNST     :   // 1,2...
        fprintf(cfg_out,"\n %s ",p_exp_treenode->number_string); 
        break;
      case VAR      :   // a
        //fprintf(cfg_out,"\nexecute p_exp_treenode->var_table_idx is %d",p_exp_treenode->var_table_idx);
        p_exp_treenode->width_start = current_var_table[p_exp_treenode->var_table_idx].width_start;
        p_exp_treenode->width_end = current_var_table[p_exp_treenode->var_table_idx].width_end;

        fprintf(cfg_out,"\n %s ",current_instance_node->p_var_table[p_exp_treenode->var_table_idx].var_name); 
        sprintf(p_exp_treenode->number_string,"%s",current_instance_node->p_var_table[p_exp_treenode->var_table_idx].var_name); 
        break;
      case CATCOPY   :   //4{a[1]}
        //$if (p_exp_treenode->Is_top_node)
        //${
        //$  fprintf(cfg_out,"\n %s{%s}\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$  sprintf(p_exp_treenode->number_string,"%s{%s}",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$}
        //$else
        //${
          new_var_string = new_intermediate_variable();
          p_exp_treenode->var_table_idx = var_table_id-1;
          p_exp_treenode->width_start = (1+p_exp_treenode->right->width_start- p_exp_treenode->right->width_end)*(atoi(p_exp_treenode->left->number_string))-1;
          p_exp_treenode->width_end = 0;
          current_var_table[var_table_id-1].width_start=p_exp_treenode->width_start; 
          current_var_table[var_table_id-1].width_end=p_exp_treenode->width_end; 
           
          fprintf(cfg_out,"\n %s = %s{%s}\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
          sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        //$}
        break;
      case CONCAT    :   //
        //$if (p_exp_treenode->Is_top_node)
        //${
        //$  if(p_exp_treenode->right!=NULL)
        //$  {
        //$    fprintf(cfg_out,"\n {%s,%s} ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$    sprintf(p_exp_treenode->number_string,"{%s,%s}",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$  }
        //$  else
        //$  {
        //$    fprintf(cfg_out,"\n {%s} ",p_exp_treenode->left->number_string); 
        //$    sprintf(p_exp_treenode->number_string,"{%s}",p_exp_treenode->left->number_string); 
        //$  }
        //$}  
        //$else
        //${
          new_var_string = new_intermediate_variable();
          p_exp_treenode->var_table_idx = var_table_id-1;
          if(p_exp_treenode->right!=NULL)
          {
            p_exp_treenode->width_start = 1 + (p_exp_treenode->right->width_start)-(p_exp_treenode->right->width_end)+(p_exp_treenode->left->width_start)-(p_exp_treenode->left->width_end);
            p_exp_treenode->width_end = 0;
          }
          else if(p_exp_treenode->left!=NULL)
          {
            p_exp_treenode->width_start = p_exp_treenode->left->width_start;
            p_exp_treenode->width_end = p_exp_treenode->left->width_end;
          }
          current_var_table[var_table_id-1].width_start=p_exp_treenode->width_start; 
          current_var_table[var_table_id-1].width_end=p_exp_treenode->width_end;

          if(p_exp_treenode->right!=NULL)
            fprintf(cfg_out,"\n %s = {%s,%s}\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
          else 
            fprintf(cfg_out,"\n %s = {%s}\n",new_var_string,p_exp_treenode->left->number_string); 
          sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        //$}
        break;
    }
  
}

void add_to_cfg_used_chain(int var_idx,int width_start,int width_end)
{
  _var_node * p_var_node = (_var_node *)malloc(sizeof(_var_node));  
  p_var_node->var_id = var_idx;
  p_var_node->width_start = width_start;
  p_var_node->width_end = width_end;
  p_var_node->Is_input_port = current_instance_node->p_var_table[var_idx].Is_input_port;
  p_var_node->next_node = current_cfg_node->used_var_chain;
  current_cfg_node->used_var_chain = p_var_node;
  //if((current_cfg_node->p_exptree_node->number_string==NULL)||(current_instance_node->p_var_table[var_idx].var_name==NULL))
  //  fprintf(cfg_out,"\n$$$Fatal Error"); 
  //else
  //fprintf(cfg_out,"\n#####successfuly add a cfg node to cfg chain: %s: varname is %s\n",current_cfg_node->p_exptree_node->number_string,current_instance_node->p_var_table[var_idx].var_name);
}

char * new_intermediate_variable()
{
  char * p_string = (char *)malloc(12*sizeof(char));
  sprintf(p_string,"vinter_%d",intemediate_var_allocated);
  intemediate_var_allocated++;
  add_to_var_table(p_string,0,0);
  return p_string;
}
int get_branch_number(_cfg_node * cfg_x)
{
  char * exp_name;
  char bran_num[4];
  return cfg_x->branch_number;
  //exp_name = cfg_x->p_exptree_node->left->left->left->number_string;
  //printf("\n call the branch number-----the expression is %d\n",cfg_x->branch_number);
  ////while((*(exp_name)!=' ')&&(*(exp_name)!='\0'))
  ////  exp_name++;
  ////exp_name++;
  ////exp_name++;
  ////for(idx=0;idx<4;idx++)
  ////{
  ////  if((*(exp_name)!=' ')&&(*(exp_name)!='|'))
  ////   bran_num[idx] = *(exp_name);
  ////  else
  ////  {
  ////   bran_num[idx] = '\0';
  ////   break;
  ////  }
  ////  exp_name++; 
  ////} 
  ////fprintf(dbg_out, "\n the branch number is %d",atoi(bran_num));
  //return atoi(bran_num); 
  //return cfg_x->branch_number;
}
void print_branch()
{
  //extern _cfg_node * branch_table[MAXNUMBRAN]; 
  int idx=0;
  while(branch_table[idx]!=NULL)
  {
    fprintf(stdout,"---%s---\n",branch_table[idx]->exp_name);
    idx++;
  }
}

//==============================================================================================
//build the symbole table

int add_to_var_table(char * var_name, int width_start, int width_end)
{
  int idx;
 
  //fprintf(cfg_out,"\nadd a new variable to list, name %s\n",var_name); 
  for(idx=0;idx<var_table_id;idx++)
  {
    if(strcmp(var_name,current_var_table[idx].var_name)==0)
      return 0;
  }
  
  strcpy(current_var_table[var_table_id].var_name,var_name);
  current_var_table[var_table_id].width_start = width_start;
  current_var_table[var_table_id].width_end = width_end;
  current_var_table[var_table_id].var_id  = var_table_id;
  current_var_table[var_table_id].Is_input_port  = input_handling;
  var_table_id++;
   
  return var_table_id; 
}
void print_var_table(_var_table * current_var_table, int var_table_id)
{
  int idx;
  fprintf(dbg_out,"\n===calling the print_var_table var_table_id=%d",var_table_id);
  for(idx=0;idx<var_table_id;idx++)
    fprintf(dbg_out, "\n---the variable is %s[%d:%d] id=%d",current_var_table[idx].var_name,current_var_table[idx].width_start,current_var_table[idx].width_end,current_var_table[idx].var_id); 
}

int get_range_start(char * var_range)
{
  char str_range[5];
  int idx;
  for(idx=0;idx<5;idx++)  
  {
    if(*(var_range+idx+1)!=' ')
      str_range[idx]=*(var_range+idx+1);
    else
      break;
  }
  str_range[idx]='\0';
  printf("\n--var_range = %s, range_start=%s\n",var_range,str_range);
  return atoi(str_range); 
}
int get_range_end(char * var_range)
{
  char str_range[5];
  int idx_src;
  int idx=0;
  while(*(var_range+idx)!=' ')
    idx++;
  idx++;
  for(idx_src=0;idx_src<5;idx_src++)
  {
    if(*(var_range+idx+idx_src)!=')')
      str_range[idx_src]=*(var_range+idx+idx_src);
    else
      break;
  }  
  str_range[idx_src]='\0';
  printf("\nvar_range = %s, range_end=%s\n",var_range,str_range);
  return atoi(str_range); 
}
int get_var_entry(char * var_name)
{
  int idx;
  for(idx=0;idx<var_table_id;idx++)
  {
    if(strcmp(var_name,current_var_table[idx].var_name)==0)
    {
      //fprintf(cfg_out,"\nYes, find variable: %s\n",var_name);
      return idx;      
    }
  }
  fprintf(cfg_out,"\nFatal error, cannot find variable: %s\n",var_name);
  return -1;
}
void create_varlist_table()
{
   int idx;
   for(idx=0;idx<num_width_table_entries;idx++)
   { 
     //fprintf(cfg_out,"\nadd a new variable to list, name %s\n",wire_width_table[idx].name); 
     add_to_var_table(wire_width_table[idx].name,cur_range_start,cur_range_end);
     idx++;
   }
}
//==============================================================================================
//build the expression tree

_exptree_node * new_exptree_node(tree_node_type tree_node_t, _exptree_node * left, _exptree_node * middle, _exptree_node * right, int var_table_idx, char * number_string)
{
   _exptree_node * p_exptree_node = (_exptree_node *)malloc(sizeof(_exptree_node));
   p_exptree_node->tree_node_t = tree_node_t;
   p_exptree_node->left = left; 
   p_exptree_node->middle = middle; 
   p_exptree_node->right = right;   
   p_exptree_node->var_table_idx = var_table_idx;
   if(number_string!=NULL)
   {
     p_exptree_node->number_string = (char *)malloc(20*sizeof(char));
     strcpy(p_exptree_node->number_string,number_string);
   }
   p_exptree_node->Is_traversed = false;
   p_exptree_node->Is_top_node = false;
   //fprintf(cfg_out,"\n~~~~~a new expression node~~~~~~type: %d, var_table_idx: %d, num_string: %s~~~~~~~~~~\n",tree_node_t,var_table_idx,number_string);
   return p_exptree_node;
} 
//==============================================================================================
int get_number_width(char * number_str) //get the width of the number
{
   char width_str[6];
   int width_dec;
   char base_str[8];
   char number_string[64];
   printf("\nthe number string is %s\n",number_str);
   sscanf(number_str,"%s%d%s%s",width_str,&width_dec,base_str,number_string);
   return width_dec;
}
int binstr2dec(char * number_string)
{
  int idx_char=0;
  int sum_bin=0;
  do{
     if(number_string[idx_char]=='1')
       sum_bin = sum_bin * 2 + 1;
     else
       sum_bin = sum_bin * 2; 
     idx_char ++;
  } while(number_string[idx_char]!='\0');
  return sum_bin;
}

char * get_number_string(char * number_str) //translate to decimal
{
   char width_str[6];
   int width_dec;
   char base_str[8];
   char number_string[64];
   char * ret_str;
   int number_dec;
   sscanf(number_str,"%s%d%s%s",width_str,&width_dec,base_str,number_string);
   if(strcmp(base_str,"binary")==0)
     //sscanf(number_str,"%s %d %s %b",width_str,&width_dec,base_str,&number_dec);
     number_dec = binstr2dec(number_string);
   if(strcmp(base_str,"octal")==0)
     sscanf(number_str,"%s%d%s%o",width_str,&width_dec,base_str,&number_dec);
   if(strcmp(base_str,"decimal")==0)
     sscanf(number_str,"%s%d%s%d",width_str,&width_dec,base_str,&number_dec);
   if(strcmp(base_str,"hex")==0)
     sscanf(number_str,"%s %d %s %x",width_str,&width_dec,base_str,&number_dec);
   ret_str = (char *)malloc(64*sizeof(char));
   printf("\n====%d=====\n",number_dec);
   sprintf(ret_str,"%d",number_dec);
   return ret_str; 
}

