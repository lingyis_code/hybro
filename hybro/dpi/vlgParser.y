/*
 * Verliog Grammar
 *
 * original credit to vl2mv code from VIS 2.0 distribution
 * too many chnages to the parser to document them all
 * -Vinod.
*/


%{
#include "vlg2sExpr.h"

  void yyerror(char *str);
  void YYTRACE(char *str);

  extern char brep[];
  extern int bexp0, bexp1;

  char current_range[MAXSTRLEN];
  int cur_range_start;
  int cur_range_end;
  struct wire_width_tuple wire_width_table[MAXNUMVARS];
  int num_width_table_entries;
  //struct 
  _cfg_node * cfg_temp;
  _cfg_node * cfg_top;
  //_cfg_node * cfg_table[MAXNUMCFGS];

  _instance_node * top_instance_list;
  _instance_node * current_instance_node;
  //_cfg_block * current_cfg_block;
  
  _exptree_node * current_exptree_event;
  
  _cfg_node * current_cfg_node;

  _cfg_node * branch_table[MAXNUMBRAN];
 
  _var_table * current_var_table;
  int var_table_id;
 
  int var_index = 0;
  int var_number = 0;
 
  int branch_var_allocated=0;

  int intemediate_var_allocated = 0;
   
  _var_node * current_lvalue_node;
  bool input_handling;

  //int cfg_stack_pointer = 0;
  FILE * dbg_out;
  FILE * cfg_out; 
%}


%union {
  char id[MAXSTRLEN];
  _cfg_node * NODE;
  _exptree_node * EXPNODE;
}


%start source_text


%token <id> YYLID                      
%token <id> YYINUMBER                 
%token <id> YYINUMBER_BIT                 
%token <id> YYRNUMBER                 
%token <id> YYSTRING                  
%token YYALLPATH                 
%token YYALWAYS                  
%token <id> YYAND                     
%token YYASSIGN                  
%token YYBEGIN                   
%token <id> YYBUF                    
%token <id> YYBUFIF0                  
%token <id> YYBUFIF1                  
%token YYCASE                    
%token YYCASEX                   
%token YYCASEZ                   
%token <id> YYCMOS                    
%token YYCONDITIONAL             
%token YYDEASSIGN                
%token YYDEFAULT                 
%token YYDEFPARAM                
%token YYDISABLE                 
%token YYEDGE                    
%token YYEND                     
%token YYENDCASE                 
%token YYENDMODULE               
%token YYENDFUNCTION             
%token YYENDPRIMITIVE            
%token YYENDSPECIFY              
%token YYENDTABLE                
%token YYENDTASK                 
%token YYENUM                    
%token YYEVENT                   
%token YYFOR                     
%token YYFOREVER                 
%token YYFORK                    
%token YYFUNCTION                
%token YYGEQ                     
%token YYHIGHZ0                  
%token YYHIGHZ1                  
%token YYIF                      
%nonassoc YYELSE
%nonassoc YYLOWERTHANELSE
%token YYINITIAL                 
%token YYINOUT                   
%token YYINPUT                   
%token YYINTEGER                 
%token YYJOIN                    
%token YYLARGE                   
%token YYLEADTO                  
%token YYLEQ                     
%token YYLOGAND                  
%token YYCASEEQUALITY            
%token YYCASEINEQUALITY          
%token YYLOGNAND                 
%token YYLOGNOR                  
%token YYLOGOR                   
%token YYLOGXNOR                 
%token YYLOGEQUALITY             
%token YYLOGINEQUALITY           
%token YYLSHIFT                  
%token YYMACROMODULE             
%token YYMEDIUM                  
%token YYMODULE                  
%token YYMREG   	         
%token <id> YYNAND                    
%token YYNBASSIGN                
%token YYNEGEDGE                 
%token <id> YYNMOS                    
%token <id> YYNOR                     
%token <id> YYNOT                     
%token <id> YYNOTIF0                  
%token <id> YYNOTIF1                  
%token <id> YYOR                      
%token YYOUTPUT                  
%token YYPARAMETER               
%token <id> YYPMOS                    
%token YYPOSEDGE                 
%token YYPRIMITIVE               
%token YYPULL0                   
%token YYPULL1                   
%token <id> YYPULLUP                  
%token <id> YYPULLDOWN                
%token <id> YYRCMOS                   
%token YYREAL                    
%token YYREG                     
%token YYREPEAT                  
%token YYRIGHTARROW              
%token <id> YYRNMOS                   
%token <id> YYRPMOS                   
%token YYRSHIFT                  
%token <id> YYRTRAN                   
%token <id> YYRTRANIF0                
%token <id> YYRTRANIF1                
%token YYSCALARED                
%token YYSMALL                   
%token YYSPECIFY                 
%token YYSPECPARAM               
%token YYSTRONG0                 
%token YYSTRONG1                 
%token YYSUPPLY0                 
%token YYSUPPLY1                 
%token YYSWIRE                   
%token YYTABLE                   
%token YYTASK                    
%token <id> YYTESLATIMER              
%token YYTIME                    
%token <id> YYTRAN                    
%token <id> YYTRANIF0                 
%token <id> YYTRANIF1                 
%token YYTRI                     
%token YYTRI0                    
%token YYTRI1                    
%token YYTRIAND                  
%token YYTRIOR                   
%token YYTRIREG                  
%token YYuTYPE                   
%token YYTYPEDEF                 
%token YYVECTORED                
%token YYWAIT                    
%token YYWAND                    
%token YYWEAK0                   
%token YYWEAK1                   
%token YYWHILE                   
%token YYWIRE                    
%token YYWOR                     
%token <id> YYXNOR                    
%token <id> YYXOR                     
%token YYsysSETUP                
%token YYsysID                   
%token YYsysND                   

%right YYCONDITIONAL
%right '?' ':'
%left YYOR
%left YYLOGOR
%left YYLOGAND
%left '|'
%left '^' YYLOGXNOR
%left '&'
%left YYLOGEQUALITY YYLOGINEQUALITY YYCASEEQUALITY YYCASEINEQUALITY
%left '<' YYLEQ '>' YYGEQ YYNBASSIGN
%left YYLSHIFT YYRSHIFT
%left '+' '-'
%left '*' '/' '%'
%right '~' '!' YYUNARYOPERATOR

%type <id> identifier
%type <id> variable_list
%type <id> register_variable
%type <id> register_variable_list
%type <EXPNODE> primary_ext
%type <EXPNODE> expression_ext
%type <EXPNODE> expression_list_ext
%type <id> input_declaration
%type <id> output_declaration
%type <id> inout_declaration
%type <id> reg_declaration
%type <id> net_declaration
%type <id> range
%type <id> range_opt
%type <id> expandrange
%type <id> expandrange_opt
%type <id> module_or_primitive_instantiation
%type <id> name_of_module_or_primitive
%type <NODE> named_port_connection
%type <id> module_port_connection
%type <NODE> named_port_connection_list
%type <id> module_port_connection_list
%type <id> module_connection_list
%type <id> module_or_primitive_instance
%type <id> module_or_primitive_instance_list
%type <id> gate_instantiation
%type <id> gatetype
%type <id> terminal
%type <id> terminal_list
%type <id> name_of_gate_instance
%type <id> gate_instance
%type <id> gate_instance_list
%type <EXPNODE> concatenation
%type <id> assignment_list
%type <id> continuous_assign
%type <EXPNODE> assignment_ext
%type <EXPNODE> lvalue_ext
%type <id> port_list_opt
%type <id> port_list
%type <id> port
%type <id> port_expression_opt
%type <id> port_expression
%type <id> port_ref_list
%type <id> port_reference
%type <id> name_of_register
%type <id> mintypmax_expression
%type <id> mintypmax_expression_list
%type <NODE> statement
%type <id> statement_opt
%type <NODE> statement_clr
%type <NODE> seq_block
%type <id> name_of_block
%type <NODE> case_item_eclr
%type <NODE> case_item
%type <id> module_or_primitive_option_clr
%type <id> module_or_primitive_option
%type <id> delay_or_parameter_value_assignment
%type <id> event_expression
%type <id> event_control
%type <id> ored_event_expression

%%

source_text
        :
          {
	    modstats = NULL;
	    YYTRACE("source_text:");
          }
        | source_text description
          {
              YYTRACE("source_text: source_text description");
          }
        ;

description 
	: module
          {
              YYTRACE("description: module");
          }
	| primitive
          {
              YYTRACE("description: primitive");
          }
	| type_declaration
          {
	      YYTRACE("module_item: type_declaration");
          }
	;

type_declaration
        : YYTYPEDEF type_specifier type_name ';'
          {
	      YYTRACE("type_declaration: YYTYPEDEF typesepcifier type_name ';'");
	  }
        ;

module 
	: YYMODULE YYLID 
          {
	    thismodule = make_module ($2);
	    fprintf (dbg_out, "\n'((|%s|\n\t   (type . module)\n",$2);
	    num_width_table_entries = 0;
            current_var_table = (_var_table *)malloc(MAXNUMVARS*sizeof(_var_table));
            var_table_id = 0;
            current_instance_node = (_instance_node *)malloc(sizeof(_instance_node));
            strcpy(current_instance_node->inst_name,$2);
            current_instance_node->p_var_table = current_var_table;
            current_instance_node->p_cfg_block = NULL;
            current_instance_node->instance_node_next = NULL;
            //add the pointer to the module node
          } 
          port_list_opt ';'
          {
	    fprintf (dbg_out,"\t   (portorder %s)\n",$4);
          }
          module_item_clr 
	  YYENDMODULE
          {
	    //	    print_modstats ();
	    //print_wwt ();
	    fprintf (dbg_out, "\t liulingyi )) ;; |%s|\n",$2);
            print_var_table(current_var_table,var_table_id);
	    YYTRACE("module: YYMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          }
	| YYMACROMODULE YYLID port_list_opt ';'
	module_item_clr 
	  YYENDMODULE
          {
	    YYTRACE("module: YYMACROMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          }
	;

port_list_opt
	:
          {
	    strcpy ($$,"");
	    YYTRACE("port_list_opt:");
          }
	| '(' port_list ')'
          {
	    sprintf ($$,$2);
	    YYTRACE("port_list_opt: '(' port_list ')'");
          }
	;

port_list
	: port
          {
	    sprintf ($$,$1);
	    YYTRACE("port_list: port");
          }
	| port_list ',' port
          {
	    sprintf ($$,"%s %s",$1, $3);
	    YYTRACE("port_list: port_list ',' port");
          }
	;

port
	: port_expression_opt
          {
	    sprintf ($$,$1);
	    YYTRACE("port: port_expression_opt");
          }
	| '.' YYLID 
          {
	  }
          '(' port_expression_opt ')'
          {
	    sprintf ($$,"(namedport %s %s)",$2,$5);
	    YYTRACE("port: ',' YYLID '(' port_expression_opt ')'");
          }
	;

port_expression_opt
	:
           {
	     strcpy ($$,"");
	     YYTRACE("port_expression_opt:");
           }
	|  port_expression
           {
	     sprintf ($$,$1);
	     YYTRACE("port_expression_opt: port_expression");
           }
	;

port_expression
	: port_reference
          {
	    sprintf ($$,$1);
	    YYTRACE("port_expression: port_reference");
          }
	| '{' port_ref_list '}'
          {
	    sprintf ($$,$2);
	    YYTRACE("port_expression: '{' port_ref_list '}'");
          }
        ;

port_ref_list
	: port_reference
          {
	    sprintf ($$,$1);
	    YYTRACE("port_ref_list: port_reference");
          }
	| port_ref_list ',' port_reference
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("port_ref_list: port_ref_list ',' port_reference");
          }
	;

// Combining port_reference and port_reference_arg
port_reference
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("port_reference_arg:");
          }
        | YYLID '[' expression_ext ']' 
          {
            //$$=$1;
	    sprintf ($$,"(%s)",$1);
	    YYTRACE("port_reference_arg: '[' expression ']'");
          }
        | YYLID '[' expression_ext ':' expression_ext ']' 
          {
            //$$=$1;
	    sprintf ($$,"(%s)",$1);
	    YYTRACE("port_reference_arg: '[' expression ':' expression ']'");
          }
        ;


module_item_clr
        :
          {
	    YYTRACE("module_item_clr:");
          }
        | module_item_clr module_item
          {
	    YYTRACE("module_item_clr: module_item_clr module_item");
          }
        ;

module_item
	: parameter_declaration
          {
              YYTRACE("module_item: parameter_declaration");
          }
	| input_declaration
          {
	    YYTRACE("module_item: input_declaration");
          }
	| output_declaration
          {
	    YYTRACE("module_item: output_declaration");
          }
	| inout_declaration
          {
              YYTRACE("module_item: inout_declaration");
          }
	| net_declaration
          {
	    YYTRACE("module_item: net_declaration");
          }
	| reg_declaration
          {
	    YYTRACE("module_item: reg_declaration");
          }
	| time_declaration
          {
              YYTRACE("module_item: time_declaration");
          }
	| integer_declaration
          {
              YYTRACE("module_item: integer_declaration");
          }
	| real_declaration
          {
              YYTRACE("module_item: real_declaration");
          }
	| event_declaration
          {
              YYTRACE("module_item: event_declaration");
          }
	| gate_instantiation
          {
              YYTRACE("module_item: gate_instantiation");
          }
	| module_or_primitive_instantiation
          {
              YYTRACE("module_item: module_or_primitive_instantiation");
          }
	| parameter_override
          {
              YYTRACE("module_item: parameter_override");
          }
	| continuous_assign
          {
              YYTRACE("module_item: continous_assign");
          }
	| specify_block
          {
              YYTRACE("module_item: specify_block");
          }
	| initial_statement
          {
              YYTRACE("module_item: initial_statement");
          }
	| always_statement
          {
              YYTRACE("module_item: always_statement");
          }
	| task
          {
              YYTRACE("module_item: task");
          }
	| function
          {
              YYTRACE("module_item: function");
          }
	;

primitive
	: YYPRIMITIVE YYLID 
          {
          }
          '(' port_list ')' ';'
	  	primitive_declaration_eclr
		table_definition
	  YYENDPRIMITIVE
          {
              YYTRACE("primitive: YYPRMITIVE YYLID '(' variable_list ')' ';' primitive_declaration_eclr table_definition YYENDPRIMITIVE");
          }
	;

primitive_declaration_eclr
        : primitive_declaration
          {
              YYTRACE("primitive_declaration_eclr: primitive_declaration");
          }
        | primitive_declaration_eclr primitive_declaration
          {
              YYTRACE("primitive_declaration_eclr: primitive_declaration_eclr primitive_declaration");
          }
        ;

primitive_declaration
	: output_declaration
          {
	    YYTRACE("primitive_declaration: output_declaration");
          }
	| reg_declaration
          {
              YYTRACE("primitive_decalration: reg_declaration");
          }
	| input_declaration
          {
              YYTRACE("primitive_decalration: input_declaration");
          }
	;

table_definition
	: YYTABLE table_entries YYENDTABLE
          {
              YYTRACE("table_definition: YYTABLE table_entries YYENDTABLE");
          }
	;

table_entries
	: combinational_entry_eclr
          {
              YYTRACE("table_definition: combinational_entry_eclr");
          }
	| sequential_entry_eclr
          {
              YYTRACE("table_definition: sequential_entry_eclr");
          }
	;

combinational_entry_eclr
	: combinational_entry
          {
              YYTRACE("combinational_entry_eclr: combinational_entry");
          }
	| combinational_entry_eclr combinational_entry
          {
              YYTRACE("combinational_entry_eclr: combinational_entry_eclr combinational_entry");
          }
	;

combinational_entry
	: input_list ':' output_symbol ';'
          {
              YYTRACE("combinational_entry: input_list ':' output_symbol ';'");
          }
	;

sequential_entry_eclr
	: sequential_entry
          {
              YYTRACE("sequential_entry_eclr: sequential_entry");
          }
	| sequential_entry_eclr sequential_entry
          {
              YYTRACE("sequential_entry_eclr: sequential_entry_eclr sequential_entry");
          }
	;

sequential_entry
	: input_list ':' state ':' next_state ';'
          {
              YYTRACE("sequential_entry: input_list ':' state ':' next_state ';'");
          }
	;

input_list
	: level_symbol_or_edge_eclr
          {
              YYTRACE("input_list: level_symbol_or_edge_eclr");
          }
	;

level_symbol_or_edge_eclr
        : level_symbol_or_edge
          {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge");
          }
        | level_symbol_or_edge_eclr level_symbol_or_edge
          {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge_eclr level_symbol_or_edge");
          }
        ;

level_symbol_or_edge
        : level_symbol
          {
              YYTRACE("level_symbol_or_edge: level_symbol");
          }
        | edge
          {
              YYTRACE("level_symbol_or_edge: edge");
          }
        ;

edge
	: '(' level_symbol level_symbol ')'
          {
              YYTRACE("edge: '(' level_symbol level_symbol ')'");
          }
	| edge_symbol
          {
              YYTRACE("edge: edge_symbol");
          }
	;

state
	: level_symbol
          {
              YYTRACE("state: level_symbol");
          }
	;

next_state
	: output_symbol
          {
              YYTRACE("next_state: output_symbol");
          }
	| '-'
          {
              YYTRACE("next_state: '_'");
          }
	;

output_symbol
	: '0'
          {
              YYTRACE("output_symbol: '0'");
          }
        | '1'
          {
              YYTRACE("output_symbol: '1'");
          }
        | 'x'
          {
              YYTRACE("output_symbol: 'x'");
          }
        | 'X'
          {
              YYTRACE("output_symbol: 'X'");
          }
        ;

level_symbol
	: '0'
          {
              YYTRACE("level_symbol: '0'");
          }
        | '1'
          {
              YYTRACE("level_symbol: '1'");
          }
        | 'x'
          {
              YYTRACE("level_symbol: 'x'");
          }
        | 'X'
          {
              YYTRACE("level_symbol: 'X'");
          }
        | '?'
          {
              YYTRACE("level_symbol: '?'");
          }
        | 'b'
          {
              YYTRACE("level_symbol: 'b'");
          }
        | 'B'
          {
              YYTRACE("level_symbol: 'B'");
          }
        ;

edge_symbol
	: 'r'
          {
              YYTRACE("edge_symbol: 'r'");
          }
        | 'R'
          {
              YYTRACE("edge_symbol: 'R'");
          }
        | 'f'
          {
              YYTRACE("edge_symbol: 'f'");
          }
        | 'F'
          {
              YYTRACE("edge_symbol: 'F'");
          }
        | 'p'
          {
              YYTRACE("edge_symbol: 'p'");
          }
        | 'P'
          {
              YYTRACE("edge_symbol: 'P'");
          }
        | 'n'
          {
              YYTRACE("edge_symbol: 'n'");
          }
        | 'N'
          {
              YYTRACE("edge_symbol: 'N'");
          }
        | '*'
          {
              YYTRACE("edge_symbol: '*'");
          }
        ;

task
	: YYTASK YYLID  
          {
	  }
          ';' tf_declaration_clr statement_opt 
          YYENDTASK
          {
              YYTRACE("YYTASK YYLID ';' tf_declaration_clr statement_opt YYENDTASK");
          }
	;

function
        : YYFUNCTION range_or_type_opt YYLID 
          {
	  }
          ';' tf_declaration_eclr statement_opt
          YYENDFUNCTION
          {
              YYTRACE("YYFUNCTION range_or_type_opt YYLID ';' tf_declaration_eclr statement_opt YYENDFUNCTION");
          }
        ;

range_or_type_opt
        :
          {
              YYTRACE("range_or_type_opt:");
          }
        | range_or_type
          {
              YYTRACE("range_or_type_opt: range_or_type");
          }
        ;

range_or_type
        : range
          {
              YYTRACE("range_or_type: range");
          }
        | YYINTEGER
          {
              YYTRACE("range_or_type: YYINTEGER");
          }
        | YYREAL
          {
              YYTRACE("range_or_type: YYREAL");
          }
        ;

tf_declaration_clr
        :
          {
              YYTRACE("tf_declaration_clr:");
          }
        | tf_declaration_clr tf_declaration
          {
              YYTRACE("tf_declaration_clr: tf_declaration_clr tf_declaration");
          }
        ;

tf_declaration_eclr
        : tf_declaration
          {
              YYTRACE("tf_declaration_eclr: tf_declaration");
          }
        | tf_declaration_eclr tf_declaration
          {
              YYTRACE("tf_declaration_eclr: tf_decalration_eclr tf_declaration");
          }
        ;

tf_declaration
        : parameter_declaration
          {
              YYTRACE("tf_declaration: parameter_decalration");
          }
        | input_declaration
          {
              YYTRACE("tf_declaration: input_declaration");
          }
        | output_declaration
          {
              YYTRACE("tf_declaration: output_declaration");
          }
        | inout_declaration
          {
              YYTRACE("tf_declaration: inout_declaration");
          }
        | reg_declaration
          {
              YYTRACE("tf_declaration: reg_declaration");
          }
        | time_declaration
          {
              YYTRACE("tf_declaration: time_declaration");
          }
        | integer_declaration
          {
              YYTRACE("tf_declaration: integer_declaration");
          }
        | real_declaration
          {
              YYTRACE("tf_declaration: real_declaration");
          }
        | event_declaration
          {
              YYTRACE("tf_declaration: event_declaration");
          }
        ;

type_name
        : YYLID
          {
	      YYTRACE("type_name: YYLID");
	  }
        ;

type_specifier
        : enum_specifier
          {
	      YYTRACE("type_specifier: enum_specifier ';'");
	  }
        ;

enum_specifier
        : YYENUM {} enum_name enum_lst_opt
          {
	      YYTRACE("enum_specifier: YYENUM enum_name enum_lst_opt");
	  }
        | YYENUM {} '{' enumerator_list '}'       
          {
	      YYTRACE("enum_specifier: YYENUM '{' enumerator_list '}'");
	  }
        ;

enum_name
        : YYLID
          {
	      YYTRACE("enum_name: YYLID");
	  }
        ;

enum_lst_opt
        : '{' enumerator_list '}'
          {
	      YYTRACE("enum_lst_opt: '{' enumerator_list '}'");
	  }
        | 
          {
	      YYTRACE("enum_lst_opt: ");
	  }
        ;

enumerator_list
        : enumerator
          {
	      YYTRACE("enumerator_list: enumerator");
	  }
        | enumerator_list ',' enumerator
          {
	      YYTRACE("enumerator_list: enumerator_list ',' enumerator");
	  }
        ;

enumerator
        : YYLID
          {
	      YYTRACE("enumerator: YYLID");
	  }
        | YYLID {} '=' expression_ext
          {
	      YYTRACE("enumerator: YYLID '=' expression");
	  }
        ;



type_decorator_opt
        : YYuTYPE
          {
	      YYTRACE("type_decorator_opt: YYuTYPE");
	  }
        | 
          {
	      YYTRACE("type_decorator_opt: ");
	  }
        ;

parameter_declaration
        : YYPARAMETER assignment_list ';'
          {
              YYTRACE("parameter_declaration: YYPARAMETER assignment_list ';'");
          }
        ;

input_declaration
        : YYINPUT range_opt variable_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (dbg_out,"\t   (ins %s)\n",$3);
	    else 
	      fprintf (dbg_out,"\t   (ins %s %s)\n",$2,$3);
	    YYTRACE("input_declaration: YYINPUT range_opt variable_list ';'");
            cur_range_start = get_range_start($2);
            cur_range_end = get_range_end($2);
            input_handling = true;
            create_varlist_table();
            input_handling = false;
          }
        ;

output_declaration
        : YYOUTPUT range_opt variable_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (dbg_out,"\t   (outs %s)\n",$3);
	    else
	      fprintf (dbg_out,"\t   (outs %s %s)\n",$2,$3);
	    YYTRACE("output_declaration: YYOUTPUT range_opt variable_list ';'");
            cur_range_start = get_range_start($2);
            cur_range_end = get_range_end($2);
            create_varlist_table();
          }
        ;

inout_declaration
        : YYINOUT range_opt variable_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (dbg_out,"\t   (inouts %s)\n",$3);
	    else
	      fprintf (dbg_out,"\t   (inouts %s %s)\n",$2,$3);
	    YYTRACE("inout_declaration: YYINOUT range_opt variable_list ';'");
            cur_range_start = get_range_start($2);
            cur_range_end = get_range_end($2);
            create_varlist_table();
          }
        ;

net_declaration
        : type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt 
            assignment_list ';'
          {
              YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt assignment_list ';'");
          }
        | type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt 
            variable_list ';'
          {
	    if (strcmp($4,"")==0)
	      fprintf (dbg_out,"\t   (wire %s)\n",$6);
	    else
	      fprintf (dbg_out,"\t   (wire %s %s)\n",$4,$6);
            cur_range_start = get_range_start($4);
            cur_range_end = get_range_end($4);
            create_varlist_table();
	    YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt variable_list ';'");
          }
        | type_decorator_opt YYTRIREG charge_strength_opt expandrange_opt delay_opt 
            variable_list ';'
          {
              YYTRACE("net_declaration: type_decorator_opt YYTRIREG charge_strength_opt expandrange_opt delay_opt variable_list ';'");
          }
        ;


nettype
        : YYSWIRE
          {
              YYTRACE("nettype: YYSWIRE");
          }
        | YYWIRE
          {
              YYTRACE("nettype: YYWIRE");
          }
        | YYTRI
          {
              YYTRACE("nettype: YYTRI");
          }
        | YYTRI1
          {
              YYTRACE("nettype: YYTRI1");
          }
        | YYSUPPLY0
          {
              YYTRACE("nettype: YYSUPPLY0");
          }
        | YYWAND
	  {
              YYTRACE("nettype: YYWAND");
	  }
        | YYTRIAND
          {
	      YYTRACE("nettype: YYTRIAND");
	  }
        | YYTRI0
          {
	      YYTRACE("nettype: YYTRI0");
	  }
        | YYSUPPLY1
          {
	      YYTRACE("nettype: YYSUPPLY1");
	  } 
        | YYWOR
          {
	      YYTRACE("nettype: YYWOR");
          }
        | YYTRIOR
          {
              YYTRACE("nettype: YYTRIOR");
          }
        ;


expandrange_opt
        :
          {
	    strcpy ($$,"(0 0)");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("expandrange_opt:");
            num_width_table_entries = 0;
          }
        | expandrange
          {
	    sprintf ($$,$1);
	    YYTRACE("expandrange_opt: expandrange");
            num_width_table_entries = 0;
          }
        ;

expandrange
        : range
          {
	    sprintf ($$,$1);
	    YYTRACE("expandrange: range");
            num_width_table_entries = 0;
          }
        | YYSCALARED range
          {
	    strcpy ($$,"");
	    YYTRACE("expandrange: YYSCALARED range");
            num_width_table_entries = 0;
          }
        | YYVECTORED range
          {
	    strcpy ($$,"");
	    YYTRACE("expandrange: YYVECTORED range");
            num_width_table_entries = 0;
          }
        ;

reg_declaration
        : type_decorator_opt YYREG range_opt register_variable_list ';'
          {
	    if (strcmp($3,"")==0)
	      fprintf (dbg_out,"\t   (sts %s)\n",$4);
	    else
	      fprintf (dbg_out,"\t   (sts %s %s)\n",$3,$4);

            cur_range_start = get_range_start($3);
            cur_range_end = get_range_end($3);
            create_varlist_table();

	    YYTRACE("reg_declaration: type_decorator_opt YYREG range_opt register_variable_list ';'");
          }
        | type_decorator_opt YYMREG range_opt register_variable_list ';'
          {
              YYTRACE("reg_declaration: type_decorator_opt YYMREG range_opt register_variable_list ';'");
          }
        ;

time_declaration
        : YYTIME register_variable_list ';'
          {
              YYTRACE("time_declaration: YYTIME register_variable_list ';'");
          }
        ;

integer_declaration
        : YYINTEGER register_variable_list ';'
          {
              YYTRACE("integer_declaration: YYINTEGER register_variable_list ';'");
          }
        ;

real_declaration
        : YYREAL variable_list ';'
          {
              YYTRACE("real_declaration: YYREAL variable_list ';'");
          }
        ;

event_declaration
        : YYEVENT name_of_event_list ';'
          {
              YYTRACE("event_declaration: YYEVENT name_of_event_list ';'");
          }
        ;

continuous_assign
        : YYASSIGN {} 
          drive_strength_opt delay_opt assignment_ext ';'
          {
            cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
            cfg_temp->node_type = BLK_AS;
            cfg_temp->p_exptree_node = $5;
            push_cfg_table(cfg_temp);
            if(current_lvalue_node!=NULL)
            {
              cfg_temp->defined_var_chain = current_lvalue_node;
              current_lvalue_node = NULL;
            }
	    fprintf (dbg_out,"\t(---assign %s)\n",$5->number_string);
	    YYTRACE("continuous_assign: YYASSIGN drive_strength_opt delay_opt assignment_list ';'");
          }
        ;

parameter_override
        : YYDEFPARAM assignment_list ';'
          {
	    fprintf (dbg_out,"\t   (defparam %s)\n",$2);
	    YYTRACE("parameter_override: YYDEFPARAM assign_list ';'");
          }
        ;

variable_list
        : identifier
          {
	    sprintf ($$,"%s",$1);
	    put_wwt_entry ($1, current_range);
	    YYTRACE("variable_list: identifier");
          }
        | variable_list ',' identifier
          {
	    sprintf ($$,"%s %s",$1,$3);
	    put_wwt_entry ($3, current_range);
	    YYTRACE("variable_list: variable_list ',' identifier");
          }
        ;

register_variable_list
        : register_variable
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("register_variable_list: register_variable");
          }
        | register_variable_list ',' register_variable
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("register_variable_list: register_variable_list ',' register_variable");
          }
        ;
        
register_variable
        : name_of_register
          {
	    sprintf ($$,$1);
	    put_wwt_entry ($1, current_range);
	    YYTRACE("register_variable: name_of_register");
          }
        | name_of_register '[' YYINUMBER  ':' YYINUMBER ']'
          {
	    sprintf ($$,"((%s %s) %s)",$3,$5,$1);
	    {char s[MAXSTRLEN]; sprintf (s,"(%s %s)",$3,$5);
	    put_wwt_entry ($1, s);}
	    YYTRACE("register_variable: name_of_register '[' expression ':' expression ']'");
          }
        ;

name_of_register
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("name_of_register: YYLID");
          }
        ;

name_of_event_list
        : name_of_event
          {
              YYTRACE("name_of_event_list: name_of_event");
          }
        | name_of_event_list ',' name_of_event
          {
              YYTRACE("name_of_event_list: name_of_event_list ',' name_of_event");
          }
        ;

name_of_event
        : YYLID
          {
              YYTRACE("name_of_event: YYLID");
          }
        ;

charge_strength_opt
        :
          {
              YYTRACE("charge_strength_opt:");
          }
        | charge_strength
          {
              YYTRACE("charge_strength_opt: charge_strength");
          }
        ;

charge_strength
        : '(' YYSMALL ')'
          {
              YYTRACE("charge_strength: '(' YYSMALL ')'");
          }
        | '(' YYMEDIUM ')'
          {
              YYTRACE("charge_strength: '(' YYMEDIUM ')'");
          }
        | '(' YYLARGE ')'
          {
              YYTRACE("charge_strength: '(' YYLARGE ')'");
          }
        ;

drive_strength_opt
        :
          {
              YYTRACE("drive_strength_opt:");
          }
        | drive_strength
          {
              YYTRACE("drive_strength_opt: drive_strength");
          }
        ;

drive_strength
        : '(' strength0 ',' strength1 ')'
          {
              YYTRACE("drive_strength: '(' strength0 ',' strength1 ')'");
          }
        | '(' strength1 ',' strength0 ')'
          {
              YYTRACE("drive_strength: '(' strength1 ',' strength0 ')'");
          }
        ;

strength0
        : YYSUPPLY0
          {
              YYTRACE("strength0: YYSUPPLY0");
          }
        | YYSTRONG0
          {
              YYTRACE("strength0: YYSTRONG0");
          }
        | YYPULL0
          {
              YYTRACE("strength0: YYPULL0");
          }
        | YYWEAK0
          {
              YYTRACE("strength0: YYWEAK0");
          }
        | YYHIGHZ0
          {
              YYTRACE("strength0: YYHIGHZ0");
          }
        ;

strength1
        : YYSUPPLY1
          {
              YYTRACE("strength1: YYSUPPLY1");
          }
        | YYSTRONG1
          {
              YYTRACE("strength1: YYSTRONG1");
          }
        | YYPULL1
          {
              YYTRACE("strength1: YYPULL1");
          }
        | YYWEAK1
          {
              YYTRACE("strength1: YYWEAK1");
          }
        | YYHIGHZ1
          {
              YYTRACE("strength1: YYHIGHZ1");
          }
        ;

range_opt
        :
          {
	    strcpy ($$,"(0 0)");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("range_opt:");
            num_width_table_entries = 0;
          }
        | range
          {
	    sprintf ($$,$1);
	    sprintf (current_range, $1);
	    YYTRACE("range_opt: range");
            num_width_table_entries = 0;
          }
        ;

range
        : '[' YYINUMBER ':' YYINUMBER ']'
          {
	    sprintf ($$,"(%s %s)",$2,$4);
	    sprintf (current_range,"(%s %s)",$2,$4);
              YYTRACE("range: '[' expression ':' expression ']'");
            num_width_table_entries = 0;
          }
        ;

assignment_list
        : assignment_ext
          {
	    //sprintf ($$,$1);
            //$$="";
	    sprintf ($$,"");
	    YYTRACE("assignment_list: assignment");
          }
        | assignment_list ',' assignment_ext
          {
            //$$="";
	    sprintf ($$,"");
	    YYTRACE("assignment_list: assignment_list ',' assignment");
          }
        ;


gate_instantiation
        : gatetype drive_delay_clr gate_instance_list ';'
          {
	    fprintf (dbg_out,"\t   (occs %s %s)\n",$1,$3);
	    YYTRACE("gate_instantiation: gatetype drive_delay_clr gate_instance_list ';'");
          }
        ;

drive_delay_clr
        :
          {
              YYTRACE("drive_delay_clr:");
          }
        | drive_delay_clr drive_delay
          {
              YYTRACE("drive_delay_clr: drive_delay_clr drive_delay");
          }
        ;

drive_delay
        : drive_strength
          {
              YYTRACE("drive_delay: drive_strength");
          }
        | delay
          {
              YYTRACE("drive_delay: delay");
          }
        ;

gatetype
        : YYAND
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYAND");
	  }
        | YYNAND
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNAND");
	  }
        | YYOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYOR");
	  }
        | YYNOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOR");
	  }
        | YYXOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYXOR");
	  }
        | YYXNOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYXNOR");
	  }
        | YYBUF
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYBUF");
	  }
        | YYBUFIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYBIFIF0");
	  }
        | YYBUFIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYBIFIF1");
	  }
        | YYNOT
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOT");
	  }
        | YYNOTIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOTIF0");
	  }
        | YYNOTIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOTIF1");
	  }
        | YYPULLDOWN
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYPULLDOWN");
	  }
        | YYPULLUP
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYPULLUP");
	  }
        | YYNMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNMOS");
	  }
        | YYPMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYPMOS");
	  }
        | YYRNMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRNMOS");
	  }
        | YYRPMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRPMOS");
	  }
        | YYCMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYCMOS");
	  }
        | YYRCMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRCMOS");
	  }
        | YYTRAN
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTRAN");
	  }
        | YYRTRAN
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRTRAN");
	  }
        | YYTRANIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTRANIF0");
	  }
        | YYRTRANIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRTRANIF0");
	  }
        | YYTRANIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTRANIF1");
	  }
        | YYRTRANIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRTRANIF1");
	  }
	| YYTESLATIMER
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTESLATIMER");
	  } 
        ;

gate_instance_list
        : gate_instance
          {
	    sprintf ($$,$1);
	    YYTRACE("gate_instance_list: gate_instance");
          }
        | gate_instance_list ',' gate_instance
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("gate_instance_list: gate_instance_list ',' gate_instance");
          }
        ;

gate_instance
        : '(' terminal_list ')'
          {
	    sprintf ($$,"(%s)",$2);
	    YYTRACE("gate_instance: '(' terminal_list ')'");
          }
        | name_of_gate_instance '(' terminal_list ')'
          {
	    sprintf ($$,"((occname %s) %s)",$1,$3);
	    YYTRACE("gate_instance: name_of_gate_instance '(' terminal_list ')'");
          }
        ;

name_of_gate_instance
        : YYLID
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("name_of_gate_instance: YYLID");
          }
        ;

terminal_list
        : terminal
          {
	    sprintf ($$,$1);
	    YYTRACE("terminal_list: terminal");
          }
        | terminal_list ',' terminal
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("terminal_list: terminal_list ',' terminal");
          }
        ;

terminal
        : expression_ext
          {
            //$$="";
	    sprintf ($$,"");
	    YYTRACE("terminal: expression");
          }
        ;


module_or_primitive_instantiation
        : name_of_module_or_primitive module_or_primitive_option_clr
             module_or_primitive_instance_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (dbg_out,"\t   (occs %s %s)\n",$1,$3); 
	    else
	      fprintf (dbg_out,"\t   (occs %s (options %s) %s)\n",$1,$2,$3); 
	    YYTRACE("module_or_primitive_instantiation: name_of_module_or_primitive module_or_primitive_option_clr module_or_primitive_instance_list ';'");
          }
        ;

name_of_module_or_primitive
        : YYLID
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("name_of_module_or_primitive: YYLID");
          }
        ;

module_or_primitive_option_clr
        :
          {
	    strcpy ($$,"");
	    YYTRACE("module_or_primitive_option_clr:");
          }
        | module_or_primitive_option_clr module_or_primitive_option
          {
	    sprintf ($$, "%s %s", $1, $2);
	    YYTRACE("module_or_primitive_option_clr: module_or_primitive_option_clr module_or_primitive_option");
          }
        ;

module_or_primitive_option
        : drive_strength
          {
	    strcpy ($$,"");
	    YYTRACE("module_or_primitive_option:");
          }
        | delay_or_parameter_value_assignment
          {
	    strcpy($$,$1);
	    YYTRACE("module_or_primitive_option: delay");
          }
        ;

delay_or_parameter_value_assignment
        : '#' YYINUMBER    
          {
	    sprintf ($$, "(%s)", $2);
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER");
	  }
        | '#' YYINUMBER_BIT    
          {
	    sprintf ($$, "(%s)", $2);
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER_BIT");
	  }
        | '#' YYRNUMBER    
          {
	    sprintf ($$, "(%s)", $2);	
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYRNUMBER");
	  }
        | '#' identifier   
          {
	    sprintf ($$, "(%s)", $2);
	    YYTRACE("delay_or_parameter_value_assignment: '#' identifier");
	  }
        | '#' '(' mintypmax_expression_list ')' 
          {
	    sprintf ($$, "(%s)", $3);
	    YYTRACE("delay_or_parameter_value_assignment: '#' '(' mintypmax_expression_list ')'");
	  }
        ;

module_or_primitive_instance_list
        : module_or_primitive_instance
          {
	    sprintf ($$,$1);
            YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance");
          }
        | module_or_primitive_instance_list ',' module_or_primitive_instance
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance_list ',' module_or_primitive_instance");
          }
        ;


module_or_primitive_instance
        : '(' module_connection_list ')'
          {
	    sprintf ($$,"(%s)",$2);
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          }
//Hack! I'm pretty sure this is not allowed in synth verilog -- just to get mt_saq_array_ct.v to work! --VV
        | identifier  '(' module_connection_list ')'
          {
	    sprintf ($$,"((occname %s) (%s))",$1,$3);
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          }
        ;

module_connection_list
        : module_port_connection_list
          {
	    sprintf ($$,$1);
	    YYTRACE("module_connection_list: module_port_connection_list");
          }
        | named_port_connection_list
          {
            push_cfg_table($1);
	    //sprintf ($$,$1->exp_name);
	    YYTRACE("module_connection_list: named_port_connection_list");
          }
        ;

module_port_connection_list
        : module_port_connection
          {
	    sprintf ($$,$1);
	    YYTRACE("module_port_connection_list: module_port_connection");
          }
        | module_port_connection_list ',' module_port_connection
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("module_port_connection_list: module_port_connection_list ',' module_port_connection");
          }
        ;

named_port_connection_list
        : named_port_connection
          {
	    //sprintf ($$,$1);
            $$=$1;
	    YYTRACE("named_port_connection_list: named_port_connection");
          }
        | named_port_connection_list ',' named_port_connection
          {
	    //sprintf ($$,"%s %s",$1,$3);
            cfg_temp = $1;
            while(cfg_temp->next_node!=NULL)
              cfg_temp=cfg_temp->next_node;
            cfg_temp->next_node = $3;
            $$=$1;
	    YYTRACE("named_port_connection_list: named_port_connection_list ',' name_port_connection");
          };
       
module_port_connection
        :
          {
	    strcpy ($$,"");
	    YYTRACE("module_port_connection:");
          }
        | expression_ext
          {
            //$$="";
	    sprintf ($$,"");
	    YYTRACE("module_port_connection: expression");
          }
        ;

named_port_connection
        : '.' identifier '(' expression_ext ')'
          {
            char id_temp[MAXSTRLEN];
            //sprintf (id_temp,"%s = %s",$2,$4);
            cfg_temp = build_normal_node($2, NULL, NULL, NULL, NULL);
            _exptree_node * p_exptree_temp = new_exptree_node(VAR,NULL,NULL,NULL,-1,$2);
            cfg_temp->p_exptree_node = new_exptree_node(NAME_PORT,p_exptree_temp,NULL,$4,-1,NULL);
            cfg_temp->node_type = PORT_CONNECT;
            $$=cfg_temp;
	    //sprintf ($$,"(namedport %s %s)",$2,$4);
	    YYTRACE("named_port_connection: '.' identifier '(' expression ')'");
          }
        ;



initial_statement
        : YYINITIAL {} statement
          {
	    fprintf (dbg_out,"\t   (initial\n\t\t%s\n\t   )\n",$3->exp_name);
	    YYTRACE("initial_statement: YYINITIAL statement");
          }
        ;

always_statement
        : YYALWAYS {} statement
          {
            push_cfg_table($3);
            $3->node_type = RESET_IF;
	    fprintf (dbg_out,"\t   (always\n\t\t%s\n\t   )\n",$3->exp_name);
  	    YYTRACE("always_statement: YYALWAYS statement");
          }
        ;

statement_opt
        :
          {
	    strcpy ($$,"");
	    YYTRACE("statement_opt:");
          }
        | statement
          {
	    sprintf ($$,"%s",$1->exp_name);
	    YYTRACE("statement_opt: statement");
          }
        ;        

statement_clr
        :
          {
	    //strcpy ($$,"");
            //printf("return\n");
            $$=NULL;
	    YYTRACE("statement_clr:");
          }
        | statement_clr statement
          {
	    //sprintf ($$, "%s  %s", $1, $2);
            if($1!=NULL)
              set_next_node($1,$2);  
            if($1!=NULL) 
              $$=$1;
            else
              $$=$2;
            //==if($1!=NULL){
            //==  printf("......%s---next is",$1->exp_name);
            //==  printf("......%s\n",$1->next_node->exp_name);
            //==}
	    YYTRACE("statement_clr: statement_clr statement");
          }
        ;


statement
        : ';' 
          {
	    //strcpy ($$,"");
            //$$=(_cfg_node *)malloc(sizeof(_cfg_node));
            //strcpy($$->exp_name,"");
            $$=NULL;
	    YYTRACE("statement: ';'");
          }
        | assignment_ext ';'
          {
	    //sprintf ($$->exp_name, "%s", $1);
            cfg_top = build_normal_node("",NULL,NULL,NULL,NULL);
            cfg_top->p_exptree_node = $1;
            cfg_top->branch_number = var_index;
            if($1->tree_node_t==ASS_BLK)
              cfg_top->node_type = BLK_AS;
            else
              cfg_top->node_type = NBLK_AS;
  
            if(current_lvalue_node!=NULL)
            {
              cfg_top->defined_var_chain = current_lvalue_node;
              current_lvalue_node = NULL;
            }
            $$=cfg_top;
            //printf("\ndeduce an statement : \n");
	    YYTRACE("statement: assignment ';'");
          }
        | YYIF '(' expression_ext ')' statement 
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",$5,NULL,cfg_temp,NULL);
            cfg_top->node_type = IFCOND_EXP;
            $5->Is_branch_node = true;
            cfg_top->p_exptree_node = $3;
            $3->Is_top_node = true;
            set_next_node($5,cfg_temp);
            $$=cfg_top;
	    //sprintf ($$, "%s", $1);
	    //sprintf ($$, "(if %s %s)", $3, $5);
	    YYTRACE("statement: YYIF '(' expression ')' statement");
          }
        | YYIF '(' expression_ext ')' statement YYELSE statement 
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",$5,NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = $3;
            $5->brother_node=$7;
            $3->Is_top_node = true;
            //if($3->right!=NULL)
            //printf("\n+++right: 1->idx = %s;\n",current_instance_node->p_var_table[$3->right->var_table_idx].var_name);            
            //if($3->left!=NULL)
            //printf("\n+++left: 1->idx = %s;\n",current_instance_node->p_var_table[$3->left->var_table_idx].var_name);            
            //if(($3->left==NULL)&&($3->right==NULL))
            //printf("\n+++self: 1->idx = %s;\n",current_instance_node->p_var_table[$3->var_table_idx].var_name);            
 
            cfg_top->node_type = IFELSE_EXP;
            set_next_node($5,cfg_temp);
            set_next_node($7,cfg_temp);
            $5->Is_branch_node = true;
            $7->Is_branch_node = true;
            $5->bran_converge_node = cfg_temp;
            $7->bran_converge_node = cfg_temp;
            $5->bran_num = get_branch_number($5);
            $7->bran_num = get_branch_number($7);
            $$=cfg_top;
            branch_table[get_branch_number($5)]=$5;
            branch_table[get_branch_number($7)]=$7;
            //==printf("----%s\n",$3);
            //==printf("----%s\n",$5->exp_name);
            //==printf("----%s\n",$5->next_node->exp_name);
            //==printf("----%s\n",$7->exp_name);
	    //sprintf ($$, "(if %s %s %s)", $3, $5, $7);
	    YYTRACE("statement: YYIF '(' expression ')' statement YYELSE statement");   
	  } 
        | YYCASE '(' expression_ext ')' case_item_eclr YYENDCASE
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",$5,NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = $3;
            cfg_top->node_type = CASECOND_EXP;
            $3->Is_top_node = true;
            _cfg_node * p_cfgtemp = $5;
            while(p_cfgtemp!=NULL)
            {
               set_next_node(p_cfgtemp,cfg_temp); 
               p_cfgtemp->next_node->bran_converge_node = cfg_temp;
               p_cfgtemp = p_cfgtemp->brother_node;          
            }  
            $$=cfg_top;
            //cfg_temp=$5;
            //while(cfg_temp!=NULL){
            //  printf("++++%s \n",cfg_temp->exp_name);
            //  cfg_temp=cfg_temp->next_node;
            //}
	    //sprintf ($$, "(case %s %s)", $3, $5);
            //printf("\n--statement: YYCASE '(' expression ')' case_item_eclr YYENDCASE--\n");
	    YYTRACE("statement: YYCASE '(' expression ')' case_item_eclr YYENDCASE");
          }
        | YYCASEZ '(' expression_ext ')' case_item_eclr YYENDCASE
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",$5,NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = $3;
            $3->Is_top_node = true;
            cfg_top->node_type = CASECOND_EXP;
            _cfg_node * p_cfgtemp = $5;
            while(p_cfgtemp!=NULL)
            {
               set_next_node(p_cfgtemp,cfg_temp); 
               p_cfgtemp->next_node->bran_converge_node = cfg_temp;
               p_cfgtemp = p_cfgtemp->brother_node; 
            }  

            $$=cfg_top;
            //sprintf ($$, "(casez %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEZ '(' expression ')' case_item_eclr YYENDCASE"); 
          }
        | YYCASEX '(' expression_ext ')' case_item_eclr YYENDCASE
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node("",$5,NULL,cfg_temp,NULL);
            cfg_top->p_exptree_node = $3;
            $3->Is_top_node = true;
            cfg_top->node_type = CASECOND_EXP;
            _cfg_node * p_cfgtemp = $5;
            while(p_cfgtemp!=NULL)
            {
               set_next_node(p_cfgtemp,cfg_temp); 
               p_cfgtemp->next_node->bran_converge_node = cfg_temp;
               p_cfgtemp = p_cfgtemp->brother_node; 
            }  
            $$=cfg_top;
	    //sprintf ($$, "(casex %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEX '(' expression ')' case_item_eclr YYENDCASE");
          }
        | YYFOREVER statement
          {
              cfg_temp = build_blank_node();
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOREVER statement");
          }
        | YYREPEAT '(' expression_ext ')' statement
          {
              cfg_temp = build_blank_node();
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYREPEAT '(' expression ')' statement");
          } 
        | YYWHILE '(' expression_ext ')' statement
          {
              cfg_temp = build_blank_node();
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYWHILE '(' expression ')' statement");
          }
        | YYFOR '(' assignment_ext ';' expression_ext ';' assignment_ext ')' statement
          {
              $$=NULL;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOR '(' assignment ';' expression ';' assignment ')' statement");
          }
        | delay_control statement
          {
              $$=$2;
 	      //strcpy ($$,"");
              YYTRACE("statement: delay_control statement");
          }
        | event_control statement
          {
              $$=$2;
 	      //sprintf ($$->exp_name,"%s\n\t\t%s", $1, $2->exp_name);
              YYTRACE("statement: event_control statement");
          }
        | lvalue_ext type_action '=' delay_control expression_ext ';'
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_BLK,$1,NULL,$5,-1,NULL);
              cfg_temp->node_type = BLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s = %s",$1,$5);
              $$=cfg_temp;
              YYTRACE("statement: lvalue '=' delay_control expression ';'");
          }
        | lvalue_ext type_action '=' event_control expression_ext ';'
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_BLK,$1,NULL,$5,-1,NULL);
              cfg_temp->node_type = BLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s = %s",$1,$5);
              $$=cfg_temp;
              YYTRACE("statement: lvalue '=' event_control expression ';'");
          }
        | lvalue_ext type_action YYNBASSIGN delay_control expression_ext ';'
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_NBLK,$1,NULL,$5,-1,NULL);
              cfg_temp->node_type = NBLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s <= %s",$1,$5);
              $$=cfg_temp;
              //printf("deduce an statement : %s",$$->exp_name);
              YYTRACE("statement: lvalue YYNBASSIGN delay_control expression ';'");
          }
        | lvalue_ext type_action YYNBASSIGN event_control expression_ext ';'
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->p_exptree_node = new_exptree_node(ASS_NBLK,$1,NULL,$5,-1,NULL);
              cfg_temp->node_type = NBLK_AS;
              cfg_temp->branch_number = var_index;
              if(current_lvalue_node!=NULL)
              {
                cfg_temp->defined_var_chain = current_lvalue_node;
                current_lvalue_node = NULL;
              }
              sprintf(cfg_temp->exp_name,"%s <= %s",$1,$5);
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: lvalue YYNBASSIGN event_control expression ';'");
          }
        | YYWAIT '(' expression_ext ')' statement
          {
              $$=NULL;
              YYTRACE("statement: YYWAIT '(' expression ')' statement");
          }
        | YYRIGHTARROW name_of_event ';'
          {
              $$=NULL;
              YYTRACE("statement: YYRIGHTARROW name_of_event ';'");
          }
        | seq_block
          {
              $$=$1;
              YYTRACE("statement: seq_block");
          }
        | par_block
          {
              $$=NULL;
              YYTRACE("statement: par_block");
          }
        | task_enable
          {
              $$=NULL;
              YYTRACE("statement: task_enable");
          }
        | system_task_enable
          {
              $$=NULL;
              YYTRACE("statement: system_task_enable");
          }
        | YYDISABLE YYLID 
          {
	  }
          ';'  
          {
              $$=NULL;
              YYTRACE("statement: YYDISABLE YYLID ';'");
          }
        | YYASSIGN assignment_ext ';'
          {
            cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
            cfg_temp->p_exptree_node = $2;
            cfg_temp->node_type = BLK_AS;
            if(current_lvalue_node!=NULL)
            {
              cfg_temp->defined_var_chain = current_lvalue_node;
              current_lvalue_node = NULL;
              printf("\nassignment definied chain\n");
            }
            $$=cfg_temp;
	    YYTRACE("statement: YYASSIGN assignment ';'");
          }
        | YYDEASSIGN lvalue_ext ';'
          {
              $$=NULL;
              YYTRACE("statement: YYDEASSIGN lvalue ';'");
          }
        ;

assignment_ext
        : lvalue_ext type_action '=' expression_ext
          {
            $$=new_exptree_node(ASS_BLK,$1,NULL,$4,-1,NULL);
            //printf("\n~~~~~~~~~~~%s~~~~~~~~~~~\n",$$->number_string);
	    //sprintf ($$,"(%s %s)",get_wwt_entry($1),$4);
	    YYTRACE("assignment: lvalue '=' expression");
          }
        | lvalue_ext type_action YYNBASSIGN expression_ext
          {
            $$=new_exptree_node(ASS_NBLK,$1,NULL,$4,-1,NULL);     
            fprintf(cfg_out,"\n-^^^assignment: lvalue YYNBASSIGN expression-\n");       
	    //sprintf ($$,"(nonblock %s %s)",get_wwt_entry($1),$4);
	    YYTRACE("assignment: lvalue YYNBASSIGN expression");
          }
        ;

type_action
        :
          {
          } 
	;

case_item_eclr
        : case_item
          {
            $$=$1;
	    YYTRACE("case_item_eclr: case_item");
          }
        | case_item_eclr case_item
          {
            cfg_temp = $1;
            while(cfg_temp->brother_node!=NULL)
             cfg_temp = cfg_temp->brother_node; 
            cfg_temp->brother_node = $2; 
            cfg_temp->next_node->brother_node = $2->next_node;
            $$=$1;
	    //sprintf ($$,"%s %s", $1,$2);
	    YYTRACE("case_item_eclr: case_item_eclr case_item");
          }
        ;

case_item
        : expression_list_ext ':' statement
          {
            cfg_temp = build_normal_node("",NULL,NULL,$3,NULL);
            cfg_temp->p_exptree_node = $1;
            cfg_temp->node_type = CASE_EXP;
            $3->Is_branch_node = true;
            $3->bran_num = get_branch_number($3);
            $$=cfg_temp;
            branch_table[get_branch_number($3)]=$3;
            //while(cfg_temp!=NULL){
            fprintf(cfg_out,"^^^the case next st is %d \n",$3->bran_num);
            // cfg_temp=cfg_temp->next_node;
            //}
            //printf("\n-case_item: expression_list ':' statement-\n");
	    YYTRACE("case_item: expression_list ':' statement");
          }
        | YYDEFAULT ':' statement
          {
            cfg_temp = build_normal_node("default",NULL,NULL,$3,NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            $3->Is_branch_node = true;
            $$=cfg_temp;
            branch_table[get_branch_number($3)]=$3;
            YYTRACE("case_item: YYDEFAULT ':' statement");
          }
        | YYDEFAULT statement
          {
            cfg_temp = build_normal_node("default",NULL,NULL,$2,NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            $$=cfg_temp;
	    YYTRACE("case_item: YYDEFAULT statement");
          }
        ;

seq_block
        : YYBEGIN statement_clr YYEND
          {             
	    //sprintf ($$,"(%s)", $2);
            $$=$2;
            //printf("=dedece to a seq block: %s\n",$$->exp_name);
            //printf("=next is: %s\n",$$->next_node->exp_name);
            //printf("=next is: %s\n",$$->next_node->next_node->exp_name);
	    YYTRACE("seq_block: YYBEGIN statement_clr YYEND");
          }
        | YYBEGIN ':' name_of_block block_declaration_clr statement_clr YYEND
          {
	    //sprintf ($$,"(|%s| (%s))", $3, $5);
            $$=NULL;
	    YYTRACE("seq_block: YYBEGIN ':' name_of_block block_declaration_clr statement_clr YYEND");
          }
        ;

par_block
        : YYFORK statement_clr YYJOIN
          {
              YYTRACE("par_block: YYFORK statement_clr YYJOIN");
          }
        | YYFORK ':' name_of_block block_declaration_clr statement_clr YYJOIN
          {
              YYTRACE("par_block: YYFORK ':' name_of_block block_declaration_clr statement_clr YYJOIN");
          }
        ;

name_of_block
        : YYLID
          {
	    sprintf ($$, "%s", $1);
	    YYTRACE("name_of_block: YYLID");
          }
        ;

block_declaration_clr
        :
          {
              YYTRACE("block_declaration_clr:");
          }
        | block_declaration_clr block_declaration
          {
              YYTRACE("block_declaration_clr: block_declaration_clr block_declaration");
          }
        ;

block_declaration
        : parameter_declaration
          {
              YYTRACE("block_declaration: parameter_declaration");
          }
        | reg_declaration
          {
              YYTRACE("block_declaration: reg_declaration");
          }
        | integer_declaration
          {
              YYTRACE("block_declaration: integer_declaration");
          }
        | real_declaration
          {
              YYTRACE("block_declaration: real_declaration");
          }
        | time_declaration
          {
              YYTRACE("block_delcaration: time_declaration");
          }
        | event_declaration
          {
              YYTRACE("block_declaration: event_declaration");
          }
        ;


task_enable
        : YYLID 
          {
	  }
          ';'
          {
              YYTRACE("task_enable: YYLID ';'");
          }
        | YYLID 
          {
	  }
         '(' expression_list_ext ')' ';'
          {
              YYTRACE("task_enable: YYLID '(' expression_list ')' ';'");
          }
        ;

system_task_enable
        : name_of_system_task ';'
          {
              YYTRACE("system_task_enable: name_of_system_task ';'");
          }
        | name_of_system_task '(' expression_list_ext ')' ';'
          {
              YYTRACE("system_task_enable: name_of_system_task '(' expression_list ')'");
          }
        ;

name_of_system_task
        : system_identifier
          {
              YYTRACE("name_of_system_task: system_identifier");
          }
        ;



specify_block
        : YYSPECIFY specify_item_clr YYENDSPECIFY
          {
              YYTRACE("specify_block: YYSPECIFY specify_item_clr YYENDSPECIFY");
          } 
        ;

specify_item_clr
        :
        | specify_item_clr specify_item
        ;

specify_item
        : specparam_declaration
        | path_declaration
        | level_sensitive_path_declaration
        | edge_sensitive_path_declaration
        | system_timing_check
        ;

specparam_declaration
        : YYSPECPARAM assignment_list ';'
        ;

path_declaration
        : path_description '=' path_delay_value ';'
        ;

 
                                         
path_description
        : '(' specify_terminal_descriptor YYLEADTO specify_terminal_descriptor ')'
        | '(' path_list YYALLPATH path_list_or_edge_sensitive_path_list ')'
        ;

path_list
        : specify_terminal_descriptor
        | path_list ',' specify_terminal_descriptor
        ;

specify_terminal_descriptor
        : YYLID {}
        | YYLID '[' expression_ext ']' {}
        | YYLID '[' expression_ext ';' expression_ext ']' {}
        ;

path_list_or_edge_sensitive_path_list
        : path_list
        | '(' path_list ',' specify_terminal_descriptor
              polarity_operator YYCONDITIONAL 
              expression_ext ')'
        ;

path_delay_value
        : path_delay_expression
        | '(' path_delay_expression ',' path_delay_expression ')'
        | '(' path_delay_expression ',' path_delay_expression ',' 
              path_delay_expression ')'
        | '(' path_delay_expression ',' path_delay_expression ','
              path_delay_expression ',' path_delay_expression ','
              path_delay_expression ',' path_delay_expression ')'
        ;

path_delay_expression
        : expression_ext
	  {
	  }
        ;

system_timing_check
        : YYsysSETUP '(' ')' ';'
        ;

level_sensitive_path_declaration
        : YYIF '(' expression_ext ')'
            '(' specify_terminal_descriptor polarity_operator_opt YYLEADTO
                spec_terminal_desptr_or_edge_sensitive_spec_terminal_desptr
        | YYIF '(' expression_ext ')'
            '(' path_list ',' specify_terminal_descriptor 
                polarity_operator_opt YYALLPATH path_list ')'
                path_list '=' path_delay_value ';'
        ;

spec_terminal_desptr_or_edge_sensitive_spec_terminal_desptr
        : specify_terminal_descriptor ')' path_list '=' path_delay_value ';'
        | '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
              expression_ext ')' ')' '=' path_delay_value ';'
        ;

polarity_operator_opt
        :
        | polarity_operator
        ;

polarity_operator
        : '+'
        | '-'
        ;

edge_sensitive_path_declaration
        : '(' specify_terminal_descriptor YYLEADTO
            '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
                expression_ext ')' ')' '=' path_delay_value ';'
        | '(' edge_identifier specify_terminal_descriptor YYLEADTO
            '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
                expression_ext ')' ')' '=' path_delay_value ';'
        | '(' edge_identifier specify_terminal_descriptor YYALLPATH
            '(' path_list ',' specify_terminal_descriptor
                polarity_operator YYCONDITIONAL 
                expression_ext ')' ')' '=' path_delay_value ';'
        | YYIF '(' expression_ext ')'
            '(' specify_terminal_descriptor YYALLPATH
              '(' path_list ',' specify_terminal_descriptor
                  polarity_operator YYCONDITIONAL 
                  expression_ext ')' ')' '=' path_delay_value ';'
        | YYIF '(' expression_ext ')'
            '(' edge_identifier specify_terminal_descriptor YYLEADTO
              '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
                  expression_ext ')' ')' '=' path_delay_value ';'
        | YYIF '(' expression_ext ')'
            '(' edge_identifier specify_terminal_descriptor YYALLPATH
              '(' path_list ',' specify_terminal_descriptor
                  polarity_operator YYCONDITIONAL 
                  expression_ext ')' ')' '=' path_delay_value ';'
        ;

edge_identifier
        : YYPOSEDGE
        | YYNEGEDGE
        ;



//============================================================================
lvalue_ext
        : identifier
          {
            $$=new_exptree_node(VAR,NULL,NULL,NULL,get_var_entry($1),NULL); 
            current_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
            current_lvalue_node->var_id = get_var_entry($1);
            current_lvalue_node->next_node = NULL;
            current_lvalue_node->width_start = current_instance_node->p_var_table[current_lvalue_node->var_id].width_start;
            current_lvalue_node->width_end = current_instance_node->p_var_table[current_lvalue_node->var_id].width_end;
	    //sprintf ($$,get_wwt_entry($1));
	    YYTRACE("primary: identifier");
	  }
        | identifier '[' expression_ext ']'
          {
            $$=new_exptree_node(INDEX,$3,NULL,NULL,get_var_entry($1),$1);             
	    //sprintf ($$,"(%s %s)",$3,$1);
            var_index = var_number;
            current_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
            current_lvalue_node->var_id = get_var_entry($1);
            current_lvalue_node->next_node = NULL;
            current_lvalue_node->width_start = var_index;
            current_lvalue_node->width_end = var_index;
	    YYTRACE("primary: identifier '[' expression ']'");
	  }
        | identifier '[' expression_ext ':'  expression_ext ']'
          {
            $$=new_exptree_node(PARSEL,$3,NULL,$5,get_var_entry($1),$1);             
	    //sprintf ($$,"((%s %s) %s)",$3,$5,$1);
            current_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
            current_lvalue_node->next_node = NULL;
            current_lvalue_node->var_id = get_var_entry($1);
            if(($3->number_string!=NULL)&&($3->tree_node_t==CNST))
              current_lvalue_node->width_start = atoi($3->number_string);
            if(($5->number_string!=NULL)&&($5->tree_node_t==CNST))
              current_lvalue_node->width_end = atoi($5->number_string);
	    YYTRACE("primary: identifier '[' expression ':' expression ']'");
	    fprintf(cfg_out, "\n%s---primary: identifier '[' expression ':' expression ']'",$1);
	  }
        | concatenation
          {
            $$=$1;
            //Set the current_lvaule_node recursively
            //traverse the tree to get the lvalue chain
            compute_defined_var_node($1);
            YYTRACE("lvalue: concatenation");
          }
        ;
//==========================================================================
mintypmax_expression_list
        : mintypmax_expression
          {
	    sprintf ($$,$1);
	    YYTRACE("mintypmax_expression_list: mintypmax_expression");
	  }
        | mintypmax_expression_list ',' mintypmax_expression
          {
	    sprintf ($$,"%s %s", $1, $3);
	    YYTRACE("mintypmax_expression_list: mintypmax_expression_list ',' mintypmax_expression");
	  }
        ;

mintypmax_expression
        : expression_ext
          {
            //$$="";
	    sprintf ($$,"");
	    YYTRACE("mintypmax_expression: expression");
	  }
        | expression_ext ':' expression_ext
          {
            //$$="";
	    sprintf ($$,"(: )");
	    YYTRACE("mintypmax_expression: expression ':' expression");
	  }
        | expression_ext ':' expression_ext ':' expression_ext
          {
            //$$="";
	    sprintf ($$,"(: )");
	    YYTRACE("mintypmax_expression: expression ':' expression ':' expression");
	  }
        ;


//=====================================================================
//extend the expression
expression_list_ext
        : expression_ext
          {
            $$=$1;
	    //sprintf ($$,$1);
	    YYTRACE("expression_list: expression");
	  }
        | expression_list_ext ',' expression_ext
          {
            //????????????????????????????????????????????????
            //Should not use the concat tree type
            //problem here
            $$=new_exptree_node(CONCAT,$1,NULL,$3,-1,NULL);            
            $$->width_start = 1 + ($1->width_start)-($1->width_end)+($3->width_start)-($3->width_end);
            $$->width_end = 0; 
	    //sprintf ($$,"%s %s", $1,$3);
            printf("\nexpression_list: expression_list ',' expression\n");
	    YYTRACE("expression_list: expression_list ',' expression");
	  }
        ;

expression_ext
        : '(' expression_ext ')'
          {
            $$=$2;
	    YYTRACE("expression: (expression)");
          }
        | primary_ext
          {
            $$=$1;
	    //sprintf ($$,$1);
	    YYTRACE("expression: primary");
	  }
        | '!' primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNOT,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(not %s)",$2);
	    YYTRACE("expression: '!' primary %prec YYUNARYOPERATOR");
	  }
        | '~' primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNEG,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(neg %s)",$2);
	    YYTRACE("expression: '~' primary %prec YYUNARYOPERATOR");
	  }
        | '&' primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGAND,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uand %s)",$2);
	    YYTRACE("expression: '&' primary %prec YYUNARYOPERATOR");
	  }
        | '|' primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uor %s)",$2);
	    YYTRACE("expression: '|' primary %prec YYUNARYOPERATOR");
	  }
        | '^' primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGXOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uxor %s,",$2);
	    YYTRACE("expression: '^' primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGNAND primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNAND,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognand %s)",$2);
	    YYTRACE("expression: YYLOGNAND primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGNOR primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognor %s)",$2);
	    YYTRACE("expression: YYLOGNOR primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGXNOR primary_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGXNOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulogxnor %s)",$2);
	    YYTRACE("expression: YYLOGXNOR primary %prec YYUNARYOPERATOR");
	  }
        | expression_ext '+' expression_ext
          {
            $$=new_exptree_node(BADD,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(+ %s %s)",$1,$3);
	    YYTRACE("expression: expression '+' expression");
	  }
        | expression_ext '-' expression_ext
          {
            $$=new_exptree_node(BSUB,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(- %s %s)",$1,$3);
	    YYTRACE("expression: expressio '-' expression");
	  }
        | expression_ext '*' expression_ext
          {
            $$=new_exptree_node(BMUL,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(* %s %s)",$1,$3);
	    YYTRACE("expression: expression '*' expression");
	  }
        | expression_ext '/' expression_ext
          {
            $$=new_exptree_node(BDIV,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(/ %s %s)",$1,$3);
	    YYTRACE("expression: expression '/' expression");
	  }
        | expression_ext '%' expression_ext
          {
            $$=new_exptree_node(BMOD,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(%% %s %s)",$1,$3);
	    YYTRACE("expression: expression '%' expression");
	  }
        | expression_ext YYLOGEQUALITY expression_ext
          {
            $$=new_exptree_node(LOGEQ,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(logequal %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOgEQUALITY expression");
	  }
        | expression_ext YYLOGINEQUALITY expression_ext
          {
            $$=new_exptree_node(LOGINEQ,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(loginequal %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGINEQUALITY expression");
	  }
        | expression_ext YYLOGAND expression_ext
          {
            $$=new_exptree_node(LOGAND,$1,NULL,$3,-1,NULL); 
            //printf("\n+++1->idx = %d;+++ 2->idx = %d\n",$$->left->var_table_idx,$$->right->var_table_idx);            
	    //sprintf ($$,"(logand %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGAND expression");
	  }
        | expression_ext YYLOGOR expression_ext
          {
            $$=new_exptree_node(LOGOR,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(lognor %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGOR expression");
	  }
        | expression_ext '<' expression_ext
          {
            $$=new_exptree_node(LES,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(< %s %s)",$1,$3);
	    YYTRACE("expression: expression '<' expression");
	  }
        | expression_ext '>' expression_ext
          {
            $$=new_exptree_node(GAR,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(> %s %s)",$1,$3);
	    YYTRACE("expression: expression '>' expression");
	  }
        | expression_ext '&' expression_ext
          {
            $$=new_exptree_node(SAND,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(and %s %s)",$1,$3);
	    YYTRACE("expression: expression '&' expression");
	  }
        | expression_ext '|' expression_ext
          {
            $$=new_exptree_node(SOR,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(or %s %s)",$1,$3);
	    YYTRACE("expression: expression '|' expression");
	  }
        | expression_ext '^' expression_ext
          {
            $$=new_exptree_node(SXOR,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(xor %s %s)",$1,$3);
	    YYTRACE("expression: expression '^' expression");
	  }
        | expression_ext YYLEQ expression_ext
          {
            $$=new_exptree_node(LEQ,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(leq %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLEQ expression");
	  }
        | expression_ext YYNBASSIGN expression_ext
          {
            $$=new_exptree_node(LEQ,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(leq %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLEQ expression");
	  }
        | expression_ext YYGEQ expression_ext
          {
            $$=new_exptree_node(GEQ,$1,NULL,$3,-1,NULL);            
            //printf("--------%s---------",$$->number_string); 
	    //sprintf ($$,"(geq %s %s)",$1,$3);
	    YYTRACE("expression: expression YYGEQ expression");
	  }
        | expression_ext YYLSHIFT expression_ext
          {
            $$=new_exptree_node(LSFT,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(<< %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLSHIFT expression");
	  }
        | expression_ext YYRSHIFT expression_ext
          {
            $$=new_exptree_node(RSFT,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(>> %s %s)",$1,$3);
	    YYTRACE("expression: expression YYRSHIFT expression");
	  }
        | expression_ext YYLOGXNOR expression_ext
          {
            $$=new_exptree_node(LOGXNOR,$1,NULL,$3,-1,NULL);             
	    //sprintf ($$,"(logxnor %s %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGXNOR expression");
	  }
        | expression_ext '?' expression_ext ':' expression_ext
          {
	    //sprintf ($$,"(if %s %s %s)",$1,$3,$5,-1,NULL);
            $$=new_exptree_node(ITE_E,$1,$3,$5,-1,NULL);             
	    YYTRACE("expression: expression '?' expression ':' expression");
	  }
//==============================================================================
       | '!' expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNOT,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(not %s)",$2);
	    YYTRACE("expression: '!' primary %prec YYUNARYOPERATOR");
	  }
        | '~' expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNEG,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(neg %s)",$2);
	    YYTRACE("expression: '~' primary %prec YYUNARYOPERATOR");
	  }
        | '&' expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGAND,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uand %s)",$2);
	    YYTRACE("expression: '&' primary %prec YYUNARYOPERATOR");
	  }
        | '|' expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uor %s)",$2);
	    YYTRACE("expression: '|' primary %prec YYUNARYOPERATOR");
	  }
        | '^' expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGXOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(uxor %s,",$2);
	    YYTRACE("expression: '^' primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGNAND expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNAND,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognand %s)",$2);
	    YYTRACE("expression: YYLOGNAND primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGNOR expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGNOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulognor %s)",$2);
	    YYTRACE("expression: YYLOGNOR primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGXNOR expression_ext %prec YYUNARYOPERATOR
          {
            $$=new_exptree_node(ULOGXNOR,$2,NULL,NULL,-1,NULL);             
	    //sprintf ($$,"(ulogxnor %s)",$2);
	    YYTRACE("expression: YYLOGXNOR primary %prec YYUNARYOPERATOR");
	  }
//==============================================================================
         ;

primary_ext
        : YYINUMBER
          { 
            $$=new_exptree_node(CNST,NULL,NULL,NULL,-1,$1);             
	    //sprintf ($$,$1);
            var_number = atoi($1);
            $$->width_start = 31;
            $$->width_end = 0;
            //printf("\n-----the var number is %d--------\n",var_number);
	    YYTRACE("primary: YYINUMBER");
	  }
        | YYINUMBER_BIT
          {
            $$=new_exptree_node(CNST,NULL,NULL,NULL,-1,NULL);
            //==========================================================
            $$->width_start = get_number_width($1)-1; //get the width of the number
            $$->width_end = 0;
            $$->number_string = get_number_string($1); //translate to decimal
	    //printf ("\n----%s-----\n",$$->number_string);
	    YYTRACE("primary: YYINUMBER_BIT");
	  }
        | identifier
          {
            $$=new_exptree_node(VAR,NULL,NULL,NULL,get_var_entry($1),NULL);             
	    //sprintf ($$,get_wwt_entry($1));
            //printf("\n=======================the identifier is %s",$1);
	    YYTRACE("primary: identifier");
	  }
        | identifier '[' expression_ext ']'
          {
            $$=new_exptree_node(INDEX,$3,NULL,NULL,get_var_entry($1),$1);             
            if(($3->tree_node_t==CNST)&&($3->number_string!=NULL))
            {
              $$->width_start = atoi($3->number_string);
              $$->width_end = atoi($3->number_string);
            }
	    //sprintf ($$,"(%s %s)",$3,$1);
	    YYTRACE("primary: identifier '[' expression ']'");
	  }
        | identifier '[' expression_ext ':'  expression_ext ']'
          {
            $$=new_exptree_node(PARSEL,$3,NULL,$5,get_var_entry($1),$1);             
            if(($3->tree_node_t==CNST)&&($3->number_string!=NULL))
              $$->width_start = atoi($3->number_string);
            if(($5->tree_node_t==CNST)&&($5->number_string!=NULL))
              $$->width_end = atoi($5->number_string);
	    //sprintf ($$,"((%s %s) %s)",$3,$5,$1);
	    YYTRACE("primary: identifier '[' expression ':' expression ']'");
	  }
        | concatenation
          {
            $$=$1;
            //printf("\n--primay: concatenation--\n");
            YYTRACE("primary: concatenation");
          } 
        ;

//=====================================================================
nondeterminism_list 
        : event_control
          {
	      YYTRACE("nondeterminism_list : event_control");
	  }
        | expression_ext
          {
	      YYTRACE("nondeterminism_list : expression");
	  }
        | nondeterminism_list ',' event_control
          {
	      YYTRACE("nondeterminism_list : nondeterminism_list ',' event_control");
	  }
        | nondeterminism_list ',' expression_ext
          {
	      YYTRACE("nondeterminism_list : nondeterminism_list ',' expression");
	  }
        ;



concatenation
        : '{' expression_list_ext '}'
          {
            //$$=new_exptree_node(CONCAT, $2, NULL, NULL, -1, NULL);
            //$$->width_start = $2->width_start;
            //$$->width_end = $2->width_end;
            $$=$2;
            $$->tree_node_t = CONCAT;
            //printf("\nconcatenation: '{' expression_list '}'\n");
            //fprintf(cfg_out, "\n width start = %d, width end = %d \n", $$->width_start, $$->width_end);
	    YYTRACE("concatenation: '{' expression_list '}'");
	  }
        | '{' YYINUMBER '{' expression_list_ext '}' '}'
          {
            _exptree_node * p_node_num_tmp;
            p_node_num_tmp=new_exptree_node(CNST,NULL,NULL,NULL,-1,$2);             
            p_node_num_tmp->width_start = 31;
            p_node_num_tmp->width_end = 0;
            $$=new_exptree_node(CATCOPY, p_node_num_tmp, NULL, $4, -1, NULL);
            $$->width_start = (atoi($2))*(1+($4->width_start)-($4->width_end))-1;
            $$->width_end = 0;
          }
        ;

//multiple_concatenation
//        : '{' expression_ext '{' expression_list_ext '}' '}'
//	    {
//              $$="";
//	      //sprintf ($$,"(concatenation %s (concatenation %s))",$2,$4);
//	      YYTRACE("multiple_concatenation: '{' expression '{' expression_list '}' '}'");
//	    }
//        ;

function_call
        : identifier '(' expression_list_ext ')'
          {
	      YYTRACE("function_call: identifier '(' expression_list ')'");
	  }

        ;

system_identifier
        : YYsysID
        ;



identifier
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("identifier: YYLID");
          }
        | identifier '.' YYLID
          {
	    sprintf ($$,"(element %s %s)",$1,$3);
	    YYTRACE("identifier: identifier '.' YYLID");
          }
        ;

delay_opt
        :
          {
	      YYTRACE("delay_opt:");
	  }
        | delay
          {
	      YYTRACE("delay_opt: delay");
	  }
        ;

delay
        : '#' YYINUMBER
          {
	      YYTRACE("delay: '#' YYINUMBER");
	  }
        | '#' YYINUMBER_BIT
          {
	      YYTRACE("delay: '#' YYINUMBER_BIT");
	  }  
        | '#' YYRNUMBER
          {
	      YYTRACE("delay: '#' YYRNUMBER");
	  }  
        | '#' identifier
          {
	      YYTRACE("delay: '#' identifier");
	  }
        | '#' '(' mintypmax_expression ')'
          {
	      YYTRACE("delay: '#' '(' mintypmax_expression ')'");
	  }
        | '#' '(' mintypmax_expression ',' mintypmax_expression ')'
          {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ')'");
	  }
        | '#' '(' mintypmax_expression ',' mintypmax_expression ',' 
                  mintypmax_expression ')'
          {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ',' mintypmax_expression ')'");
	  }
        ;

delay_control
        : '#' YYINUMBER
          {
	      YYTRACE("delay_control: '#' YYINUMBER");
	  }
        | '#' YYINUMBER_BIT
          {
	      YYTRACE("delay_control: '#' YYINUMBER_BIT");
	  }
        | '#' YYRNUMBER
          {
	      YYTRACE("delay_control: '#' YYRNUMBER");
	  }  
        | '#' identifier
          {
	      YYTRACE("delay_control: '#' identifier");
	  }
        | '#' '(' mintypmax_expression_list ')'
          {
	      YYTRACE("delay_control: '#' '(' mintypmax_expression ')'");
	  }
        ;

event_control
        : '@' identifier
          {
	    sprintf ($$,"(event_control %s)", $2);
	    YYTRACE("event_control: '@' identifier");
	  }
        | '@' '(' event_expression ')'
          {
	    sprintf ($$,"(event_control %s)", $3);
	    YYTRACE("event_control: '@' '(' event_expression ')'");
	  }
        | '@' '(' ored_event_expression ')'
          {
	    sprintf ($$,"(event_control %s)", $3);
	    YYTRACE("event_control: '@' '(' ored_event_expression ')'");
	  }
        ;


event_expression
        : expression_ext
          {
	    sprintf ($$, "");
            current_exptree_event = $1;
	    YYTRACE("event_expression: expression");
	  }
        | YYPOSEDGE expression_ext
          {
            current_exptree_event = new_exptree_node(POSEGE,$2,NULL,NULL,-1,NULL);
	    sprintf ($$, "(posedge )");
	    YYTRACE("event_expression: YYPOSEDGE expression");
	  }
        | YYNEGEDGE expression_ext
          {
            current_exptree_event = new_exptree_node(NEGEGE,$2,NULL,NULL,-1,NULL);
	    sprintf ($$, "(negedge )");
	    YYTRACE("event_expression: YYNEGEDGE expression");
	  }
	| YYEDGE expression_ext
          {
            current_exptree_event = new_exptree_node(EDGE,$2,NULL,NULL,-1,NULL);
	    sprintf ($$, "(edge )");
	    YYTRACE("event_expression: YYEDGE expression");
          }
        ;

ored_event_expression
        : event_expression YYOR event_expression
          {
	    sprintf ($$,"(%s %s %s)", $2, $1, $3);
	    YYTRACE("ored_event_expression: event_expression YYOR event_expression");
	  }
        | ored_event_expression YYOR event_expression
          {
	    sprintf ($$,"(%s %s %s)", $2, $1, $3);
	    YYTRACE("ored_event_expression: ored_event_expression YYOR event_expression");
	  }
        ; 

%%

void yyerror(char * str)
{
    fprintf(stderr, str);
    exit (1);
}

void YYTRACE(char * str)
{
   //return;
   fprintf(cfg_out,"---%s\n",str);
}

_modulestats * make_module (char *name) {
  _modulestats *newmodule;

  if (modstats == NULL) {
    modstats = (_modulestats *) malloc (sizeof (struct __modulestats));
    modstats->next = NULL;
    modstats->numports = 0;
    modstats->numinputs = 0;
    modstats->numoutputs = 0;
    modstats->numregs = 0;
    modstats->numwires = 0;
    return modstats;
  }
  newmodule = modstats;
  while (newmodule->next != NULL) newmodule = newmodule->next;
  newmodule->next = (_modulestats *) malloc (sizeof (struct __modulestats));
  newmodule->next->next = NULL;
  newmodule->next->numports = 0;
  newmodule->next->numinputs = 0;
  newmodule->next->numoutputs = 0;
  newmodule->next->numregs = 0;
  newmodule->next->numwires = 0;
  return newmodule->next;
}

char *get_string_from_var (_var *v) {
  char *outstr;
  int i;
  outstr = (char *) malloc (MAXSTRLEN*(v->end-v->start+1));
  if (v->end-v->start) {
    strcpy (outstr,"");
    for (i=v->start; i<=v->end; i++)
      sprintf (outstr, "%s |%s%d|", outstr, v->name, i);
  } 
  else sprintf (outstr,v->name);
  return outstr;
}

char *get_string_from_varlist (_var **v, int n) {
  char *retstr;
  int i;
  retstr = NULL;
  if (n<=0) return "";
  sprintf (retstr,get_string_from_var(v[0]));
  for (i=1; i<n; i++)
    sprintf (retstr, "%s%s", retstr, get_string_from_var(v[i]));
  return retstr;
}

void print_modstats (void) {
  fprintf (dbg_out, "\t   (ins  %s)\n", get_string_from_varlist (thismodule->input_list, thismodule->numinputs));
  fprintf (dbg_out, "\t   (sts  %s)\n", get_string_from_varlist (thismodule->reg_list, thismodule->numregs));
  fprintf (dbg_out, "\t   (outs  %s)\n", get_string_from_varlist (thismodule->output_list, thismodule->numoutputs));
  fprintf (dbg_out, "\t   (wires  %s)\n", get_string_from_varlist (thismodule->wire_list, thismodule->numwires));
  fprintf (dbg_out, "\t   (occs  %s)\n","");
}

void put_wwt_entry (char *name, char *range) {
  struct wire_width_tuple t;
  strcpy (t.name, name);
  strcpy (t.range, range);
  wire_width_table[num_width_table_entries++] = t;
  return;
}

char *get_wwt_entry (char *name) {
  int i;
  char *rstr = (char *) malloc (MAXSTRLEN);
  for (i=0; i<num_width_table_entries; i++) 
    if (strcmp (wire_width_table[i].name, name)==0) {
      sprintf (rstr, "(%s %s)", wire_width_table[i].range, wire_width_table[i].name);
      return rstr;
    }
  return name;
}

void print_wwt (void) {
  printf ("\n");printf ("\n");
  for (int i=0; i<num_width_table_entries; i++) 
    printf ("(%s %s)\n", wire_width_table[i].name, wire_width_table[i].range);
  printf ("\n");
}

//============================================
//============for building CFG================
//===========liu187@illinois.edu==============
_cfg_node * build_blank_node( )
{ 
  _cfg_node * temp;
  int idx;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, "blank");
  temp->branch_number = 0;
  temp->Is_branch_node = false;
  temp->Is_exit_branch = false;
  temp->frame_num_exhaust = 1000;
  temp->Is_traversed = false;
  temp->node_type = NULL_NODE;
  temp->left_node = NULL;
  temp->right_node = NULL;
  temp->brother_node = NULL;
  temp->next_node = NULL;

  temp->bran_converge_node = NULL;
  temp->bran_num = -1;
  temp->p_dep_list = NULL;
  //temp->Is_covered = 0;
  temp->temp_flag = false;
 
  temp->used_var_chain = NULL;
  temp->defined_var_chain = NULL;
  temp->p_exptree_node = NULL;
  for (idx=0;idx<MAXFRAMES;idx++)
  {
     temp->branch_taken[idx]=false;
     temp->guard_negated[idx]=false;
     temp->exhaust_bran[idx]=false;
     temp->Is_covered[idx]=0;
  }
  return temp;
}

_cfg_node * build_normal_node(char exp[], _cfg_node * left, _cfg_node * right, _cfg_node * next, _cfg_node * brother)
{
  _cfg_node * temp;
  int idx;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, exp);
  temp->left_node = left;
  temp->branch_number = 0;
  temp->Is_branch_node = false;
  temp->Is_exit_branch = false;
  temp->frame_num_exhaust = 1000;
  temp->Is_traversed = false;
  temp->right_node = right;
  temp->brother_node = brother;
  temp->next_node = next; 

  temp->bran_converge_node = NULL;
  temp->bran_num = -1; 
  temp->p_dep_list = NULL;
  //temp->Is_covered = 0;
  temp->temp_flag = false;
  temp->used_var_chain = NULL;
  temp->p_exptree_node = NULL;
  temp->defined_var_chain = NULL;
  for (idx=0;idx<MAXFRAMES;idx++)
  {
     temp->branch_taken[idx]=false;
     temp->guard_negated[idx]=false;
     temp->exhaust_bran[idx]=false;
     temp->Is_covered[idx]=0;
  }
  return temp;
}

int set_next_node(_cfg_node * cfg_x, _cfg_node * cfg_nxt)
{
  _cfg_node * temp;
  temp = cfg_x;
  while(temp->next_node!=NULL)
   temp = temp->next_node;
  temp->next_node = cfg_nxt;
  return 1;
}

//finish a cfg building
int push_cfg_table(_cfg_node * cfg_x)
{
  _cfg_block * p_index = current_instance_node->p_cfg_block;
  _cfg_block * p_temp = (_cfg_block *)malloc(sizeof(_cfg_block));
  p_temp->event_trigger_exp = current_exptree_event;
  p_temp->p_cfg_node = cfg_x;
  p_temp->cfg_block_next = NULL;
  if(p_index == NULL)
    current_instance_node->p_cfg_block = p_temp;
  else 
  {
    while(p_index->cfg_block_next!=NULL) 
      p_index = p_index->cfg_block_next;
    p_index->cfg_block_next = p_temp;
  }
  return 1;
}

void print_all()
{
  _cfg_block * p_temp = current_instance_node->p_cfg_block;
  while(p_temp!=NULL)
  { 
    print_cfg(p_temp->p_cfg_node);
    p_temp = p_temp->cfg_block_next;
  }
}

void print_cfg(_cfg_node * cfg_x)
{
  _cfg_node * cfg_bfs[1000];
  int rd_pointer=0;
  int wr_pointer=0;
  cfg_bfs[wr_pointer++]=cfg_x;
  //make a breadth first search
  while(rd_pointer!=wr_pointer)
  {
    cfg_x = cfg_bfs[rd_pointer];
    rd_pointer=rd_pointer+1;
    rd_pointer=rd_pointer%1000;
    
    if(((cfg_x->p_exptree_node)!=NULL)&&(cfg_x->Is_branch_node==false))
    {
      //&&(cfg_x->p_exptree_node->number_string!=NULL)
      //fprintf(cfg_out,"\n=====the exptree node is not null: type %d, Is traversed %d======\n",cfg_x->node_type,cfg_x->Is_traversed);
      if(cfg_x->Is_traversed==false)
        print_exptree_node(cfg_x->p_exptree_node);
      fprintf(cfg_out,"\n======from the print exptree_node : %s======\n",cfg_x->p_exptree_node->number_string);
      current_cfg_node = cfg_x;
      if(current_cfg_node->used_var_chain==NULL)
      {
         if((current_cfg_node->node_type==IFCOND_EXP)||(current_cfg_node->node_type==IFELSE_EXP)||(current_cfg_node->node_type==CASECOND_EXP)||(current_cfg_node->node_type==CASE_EXP))
           compute_used_var_node(current_cfg_node->p_exptree_node); 
         else if(((current_cfg_node->node_type==BLK_AS)||(current_cfg_node->node_type==NBLK_AS))&&(current_cfg_node->p_exptree_node->right!=NULL))
           compute_used_var_node(current_cfg_node->p_exptree_node->right); 
      }   
      //printf("\n======Node traversed: %s\n========",cfg_x->p_exptree_node->number_string);
      cfg_x->Is_traversed=true;
    }
    if((cfg_x->left_node!=NULL)&&(cfg_x->left_node->temp_flag==false)){
      cfg_bfs[wr_pointer]=cfg_x->left_node;
      cfg_x->left_node->temp_flag = true;
      wr_pointer++;
      wr_pointer=wr_pointer%1000;
      //continue;
    }
    if((cfg_x->brother_node!=NULL)&&(cfg_x->brother_node->temp_flag==false)){
      cfg_bfs[wr_pointer]=cfg_x->brother_node;
      cfg_x->brother_node->temp_flag = true;
      wr_pointer++;
      wr_pointer=wr_pointer%1000;
    }
    if((cfg_x->next_node!=NULL)&&(cfg_x->next_node->temp_flag==false)){
      cfg_bfs[wr_pointer]=cfg_x->next_node;
      cfg_x->next_node->temp_flag = true;
      wr_pointer++;
      wr_pointer=wr_pointer%1000;
     }
  } 
}

void add_to_cfg_defined_chain(int var_idx,int width_start,int width_end)
{
    _var_node * p_lvalue_node;
    p_lvalue_node = (_var_node *)malloc(sizeof(_var_node));
    p_lvalue_node->var_id = var_idx;
    p_lvalue_node->next_node = NULL;
    p_lvalue_node->width_start = width_start;
    p_lvalue_node->width_end = width_end;
    p_lvalue_node->next_node = current_lvalue_node;
    current_lvalue_node = p_lvalue_node;
    //sprintf ($$,get_wwt_entry($1));
}

void compute_defined_var_node(_exptree_node * p_exp_treenode)
{
  if(p_exp_treenode==NULL)
    return;
    if(p_exp_treenode->tree_node_t==PARSEL)
  {
    add_to_cfg_defined_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==INDEX)
  {
    add_to_cfg_defined_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==VAR)
  {
    add_to_cfg_defined_chain(p_exp_treenode->var_table_idx,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_start,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_end); 
  }
  else
  {
    if(p_exp_treenode->left!=NULL)
      compute_defined_var_node(p_exp_treenode->left); 
    if(p_exp_treenode->middle!=NULL)
      compute_defined_var_node(p_exp_treenode->middle);
    if(p_exp_treenode->right!=NULL)
      compute_defined_var_node(p_exp_treenode->right); 
  }  
}


void compute_used_var_node(_exptree_node * p_exp_treenode)
{
  if(p_exp_treenode==NULL)
    return;
  //if(p_exp_treenode->number_string!=NULL)
  //  fprintf(cfg_out,"\n####the computed statement is %s: type %d###\n",p_exp_treenode->number_string,p_exp_treenode->tree_node_t);
  //return; 
  //add_to_cfg_used_chain()
  if(p_exp_treenode->tree_node_t==PARSEL)
  {
    add_to_cfg_used_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==INDEX)
  {
    add_to_cfg_used_chain(p_exp_treenode->var_table_idx,p_exp_treenode->width_start,p_exp_treenode->width_end); 
  }
  else if(p_exp_treenode->tree_node_t==VAR)
  {
    //fprintf(cfg_out,"\n$$$$the computed statement is %s: type %d###\n",p_exp_treenode->number_string,p_exp_treenode->tree_node_t);
    add_to_cfg_used_chain(p_exp_treenode->var_table_idx,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_start,current_instance_node->p_var_table[p_exp_treenode->var_table_idx].width_end); 
  }
  else
  {
    if(p_exp_treenode->left!=NULL)
      compute_used_var_node(p_exp_treenode->left); 
    if(p_exp_treenode->middle!=NULL)
      compute_used_var_node(p_exp_treenode->middle);
    if(p_exp_treenode->right!=NULL)
      compute_used_var_node(p_exp_treenode->right); 
  }  //*/
}

void print_exptree_node(_exptree_node * p_exp_treenode)
{
  char * new_var_string;
  //post order to traverse the expression tree
  fprintf(cfg_out,"\n=enter the print_exptree_node=\n");
  if((p_exp_treenode->left!=NULL)&&(p_exp_treenode->left->Is_traversed==false))
  {
    //if(p_exp_treenode->left->Is_traversed==false) 
      fprintf(cfg_out,"\n=enter the print_exptree_node: left=\n");
      print_exptree_node(p_exp_treenode->left);
  } 
  if  ((p_exp_treenode->middle!=NULL)&&(p_exp_treenode->middle->Is_traversed==false))
  { 
    //if(p_exp_treenode->middle->Is_traversed==false) 
      fprintf(cfg_out,"\n=enter the print_exptree_node: middle=\n");
      print_exptree_node(p_exp_treenode->middle);
  }
  if ((p_exp_treenode->right!=NULL)&&(p_exp_treenode->right->Is_traversed==false))
  {
    //if(p_exp_treenode->right->Is_traversed==false)
      fprintf(cfg_out,"\n=enter the print_exptree_node: right=\n");
      print_exptree_node(p_exp_treenode->right);
  }
  fprintf(cfg_out,"\n=enter the print_exptree_node=: self\n");
  
    if(p_exp_treenode->left!=NULL) 
      p_exp_treenode->left->Is_traversed=false;
    if(p_exp_treenode->middle!=NULL) 
      p_exp_treenode->middle->Is_traversed=false;
    if(p_exp_treenode->right!=NULL) 
      p_exp_treenode->right->Is_traversed=false;

    p_exp_treenode->Is_traversed = true;

    if(p_exp_treenode->number_string==NULL)
      p_exp_treenode->number_string = (char *)malloc(50*sizeof(char));
  
    //fprintf(cfg_out,"\n=enter the print_exptree_node=: here\n");

    //traverse this node
    switch(p_exp_treenode->tree_node_t)
    {
      case GEQ      :   // a>=b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s >= %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s >= %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 
           
           fprintf(cfg_out,"\n%s = (%s >= %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LEQ      :   // a<=b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s <= %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s <= %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0;
 
           fprintf(cfg_out,"\n%s = (%s <= %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGAND   :   // a&&b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s && %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s && %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s && %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGOR    :   // a||b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s || %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s || %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s || %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGEQ    :   // a==b 
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s == %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s == %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s == %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGINEQ  :   // a!=b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s != %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s != %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (%s != %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LSFT     :   // a<<b  
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start= p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end= p_exp_treenode->left->width_end ; 

           fprintf(cfg_out,"\n%s = (%s << %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case RSFT     :   // a>>b
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start= p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end= p_exp_treenode->left->width_end ; 

           fprintf(cfg_out,"\n%s = (%s >> %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case ULOGNOT  :   // !a 
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ! %s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"! (%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

           fprintf(cfg_out,"\n%s = (!%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGNEG  :   // ~a 
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start- p_exp_treenode->left->width_end;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
         
           fprintf(cfg_out,"\n%s = (~%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGAND  :   //&a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n &%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"&(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (&%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGOR   :   //|a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n |%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"|(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (|%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGXOR  :   //^a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ^%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"^(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (^%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //} 
        break;
      case ULOGNAND :   //~&a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~&%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~&(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (~&%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGNOR  :   //~|a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~|%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~|(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (~|%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ULOGXNOR :   //~^a
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n ~^%s ",p_exp_treenode->left->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"~^(%s)",p_exp_treenode->left->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=0; 
           current_var_table[var_table_id-1].width_end=0; 

           fprintf(cfg_out,"\n%s = (~^%s)\n",new_var_string,p_exp_treenode->left->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case BADD     :   //a+b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         if(p_exp_treenode->left->width_start>p_exp_treenode->left->width_end)
         {
           p_exp_treenode->width_start = p_exp_treenode->left->width_start-p_exp_treenode->left->width_end;
           p_exp_treenode->width_end = 0;
         }
         else
         {
           printf("FATAL ERROR in A+B");
         }
         //p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         //p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->width_end; 

         fprintf(cfg_out,"\n%s = (%s + %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BSUB     :   //a-b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s - %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BMUL     :   //a*b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s * %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BDIV     :   //a/b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s / %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case BMOD     :   //a%b
         new_var_string = new_intermediate_variable();

         p_exp_treenode->var_table_idx = var_table_id-1;
         p_exp_treenode->width_start = p_exp_treenode->left->width_start;
         p_exp_treenode->width_end = p_exp_treenode->left->width_end;
         current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
         current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 

         fprintf(cfg_out,"\n%s = (%s \% %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case GAR      :   //a>b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s > %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s > %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start = 0; 
           current_var_table[var_table_id-1].width_end = 0;
 
           fprintf(cfg_out,"\n%s = (%s > %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LES      :   //a<b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s < %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s < %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();

           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = 0;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start = 0; 
           current_var_table[var_table_id-1].width_end = 0;

           fprintf(cfg_out,"\n%s = (%s < %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case SAND     :   //a&b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s & %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s & %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start - p_exp_treenode->left->width_end;
           p_exp_treenode->width_end = 0;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s & %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case SOR      :   //a|b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s | %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s | %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s | %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case SXOR     :   //a^b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s ^ %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s ^ %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s ^ %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case LOGXNOR  :   //a^b
         //$if (p_exp_treenode->Is_top_node)
         //${
         //$  fprintf(cfg_out,"\n %s ~^ %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$  sprintf(p_exp_treenode->number_string,"%s ~^ %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
         //$}  
         //$else
         //${ 
           new_var_string = new_intermediate_variable();
           p_exp_treenode->var_table_idx = var_table_id-1;
           p_exp_treenode->width_start = p_exp_treenode->left->width_start;
           p_exp_treenode->width_end = p_exp_treenode->left->width_end;
           current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
           current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
           fprintf(cfg_out,"\n%s = (%s ~^ %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
           sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
         //$} 
        break;
      case ASS_BLK  :   //a=b
        //new_var_string = new_intermediate_variable();
        //p_exp_treenode->var_table_idx = var_table_id-1;
        //p_exp_treenode->width_start = 0;
        //p_exp_treenode->width_end = 0;
        //current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
        //current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end; 
        printf("\n %s = %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        fprintf(cfg_out,"\n %s = %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s = %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case ASS_NBLK :   //a<=b
        //new_var_string = new_intermediate_variable();
        //p_exp_treenode->var_table_idx = var_table_id-1;
        //p_exp_treenode->width_start = 0;
        //p_exp_treenode->width_end = 0;
        //current_var_table[var_table_id-1].width_start=p_exp_treenode->left->width_start; 
        //current_var_table[var_table_id-1].width_end=p_exp_treenode->left->width_end;
 
        fprintf(cfg_out,"\n %s <= %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s <= %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case NAME_PORT:   //.a(b)
        fprintf(cfg_out,"\n %s = %s ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s = %s",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case POSEGE   :   //@(posedge a)
        fprintf(cfg_out,"\n @posedge"); 
        break;
      case NEGEGE   :   //@(negedge a)
        fprintf(cfg_out,"\n @negedge"); 
        break;
      case EDGE     :   //@(edge a)
        fprintf(cfg_out,"\n @edge"); 
        break;
      case ITE_E      :   // a ? b : c
        new_var_string = new_intermediate_variable();

        p_exp_treenode->var_table_idx = var_table_id-1;
        p_exp_treenode->width_start = p_exp_treenode->right->width_start;
        p_exp_treenode->width_end = p_exp_treenode->right->width_end;
        current_var_table[var_table_id-1].width_start=p_exp_treenode->right->width_start; 
        current_var_table[var_table_id-1].width_end=p_exp_treenode->right->width_end; 

        fprintf(cfg_out,"\n%s = %s ? (%s : %s)\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->middle->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        break;
      case PARSEL   :   // a[b:c]
        fprintf(cfg_out,"\n--In PARSEL--\n");
        p_exp_treenode->width_start = atoi(p_exp_treenode->left->number_string);
        p_exp_treenode->width_end  = atoi(p_exp_treenode->right->number_string);
        fprintf(cfg_out,"\n %s[%s:%s] ",p_exp_treenode->number_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        sprintf(p_exp_treenode->number_string,"%s[%s:%s]",p_exp_treenode->number_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        break;
      case INDEX    :   // a[b]
        p_exp_treenode->width_start = atoi(p_exp_treenode->left->number_string);
        p_exp_treenode->width_end = atoi(p_exp_treenode->left->number_string);
        fprintf(cfg_out,"\n %s[%s] ",p_exp_treenode->number_string,p_exp_treenode->left->number_string); 
        sprintf(p_exp_treenode->number_string,"%s[%s]",p_exp_treenode->number_string,p_exp_treenode->left->number_string); 
        break;
      case CNST     :   // 1,2...
        fprintf(cfg_out,"\n %s ",p_exp_treenode->number_string); 
        break;
      case VAR      :   // a
        //fprintf(cfg_out,"\nexecute p_exp_treenode->var_table_idx is %d",p_exp_treenode->var_table_idx);
        p_exp_treenode->width_start = current_var_table[p_exp_treenode->var_table_idx].width_start;
        p_exp_treenode->width_end = current_var_table[p_exp_treenode->var_table_idx].width_end;

        fprintf(cfg_out,"\n %s ",current_instance_node->p_var_table[p_exp_treenode->var_table_idx].var_name); 
        sprintf(p_exp_treenode->number_string,"%s",current_instance_node->p_var_table[p_exp_treenode->var_table_idx].var_name); 
        break;
      case CATCOPY   :   //4{a[1]}
        //$if (p_exp_treenode->Is_top_node)
        //${
        //$  fprintf(cfg_out,"\n %s{%s}\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$  sprintf(p_exp_treenode->number_string,"%s{%s}",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$}
        //$else
        //${
          new_var_string = new_intermediate_variable();
          p_exp_treenode->var_table_idx = var_table_id-1;
          p_exp_treenode->width_start = (1+p_exp_treenode->right->width_start- p_exp_treenode->right->width_end)*(atoi(p_exp_treenode->left->number_string))-1;
          p_exp_treenode->width_end = 0;
          current_var_table[var_table_id-1].width_start=p_exp_treenode->width_start; 
          current_var_table[var_table_id-1].width_end=p_exp_treenode->width_end; 
           
          fprintf(cfg_out,"\n %s = %s{%s}\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
          sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        //$}
        break;
      case CONCAT    :   //
        //$if (p_exp_treenode->Is_top_node)
        //${
        //$  if(p_exp_treenode->right!=NULL)
        //$  {
        //$    fprintf(cfg_out,"\n {%s,%s} ",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$    sprintf(p_exp_treenode->number_string,"{%s,%s}",p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
        //$  }
        //$  else
        //$  {
        //$    fprintf(cfg_out,"\n {%s} ",p_exp_treenode->left->number_string); 
        //$    sprintf(p_exp_treenode->number_string,"{%s}",p_exp_treenode->left->number_string); 
        //$  }
        //$}  
        //$else
        //${
          new_var_string = new_intermediate_variable();
          p_exp_treenode->var_table_idx = var_table_id-1;
          if(p_exp_treenode->right!=NULL)
          {
            p_exp_treenode->width_start = 1 + (p_exp_treenode->right->width_start)-(p_exp_treenode->right->width_end)+(p_exp_treenode->left->width_start)-(p_exp_treenode->left->width_end);
            p_exp_treenode->width_end = 0;
          }
          else if(p_exp_treenode->left!=NULL)
          {
            p_exp_treenode->width_start = p_exp_treenode->left->width_start;
            p_exp_treenode->width_end = p_exp_treenode->left->width_end;
          }
          current_var_table[var_table_id-1].width_start=p_exp_treenode->width_start; 
          current_var_table[var_table_id-1].width_end=p_exp_treenode->width_end;

          if(p_exp_treenode->right!=NULL)
            fprintf(cfg_out,"\n %s = {%s,%s}\n",new_var_string,p_exp_treenode->left->number_string,p_exp_treenode->right->number_string); 
          else 
            fprintf(cfg_out,"\n %s = {%s}\n",new_var_string,p_exp_treenode->left->number_string); 
          sprintf(p_exp_treenode->number_string,"%s ",new_var_string); 
        //$}
        break;
    }
  
}

void add_to_cfg_used_chain(int var_idx,int width_start,int width_end)
{
  _var_node * p_var_node = (_var_node *)malloc(sizeof(_var_node));  
  p_var_node->var_id = var_idx;
  p_var_node->width_start = width_start;
  p_var_node->width_end = width_end;
  p_var_node->Is_input_port = current_instance_node->p_var_table[var_idx].Is_input_port;
  p_var_node->next_node = current_cfg_node->used_var_chain;
  current_cfg_node->used_var_chain = p_var_node;
  //if((current_cfg_node->p_exptree_node->number_string==NULL)||(current_instance_node->p_var_table[var_idx].var_name==NULL))
  //  fprintf(cfg_out,"\n$$$Fatal Error"); 
  //else
  //fprintf(cfg_out,"\n#####successfuly add a cfg node to cfg chain: %s: varname is %s\n",current_cfg_node->p_exptree_node->number_string,current_instance_node->p_var_table[var_idx].var_name);
}

char * new_intermediate_variable()
{
  char * p_string = (char *)malloc(12*sizeof(char));
  sprintf(p_string,"vinter_%d",intemediate_var_allocated);
  intemediate_var_allocated++;
  add_to_var_table(p_string,0,0);
  return p_string;
}
int get_branch_number(_cfg_node * cfg_x)
{
  char * exp_name;
  char bran_num[4];
  return cfg_x->branch_number;
  //exp_name = cfg_x->p_exptree_node->left->left->left->number_string;
  //printf("\n call the branch number-----the expression is %d\n",cfg_x->branch_number);
  ////while((*(exp_name)!=' ')&&(*(exp_name)!='\0'))
  ////  exp_name++;
  ////exp_name++;
  ////exp_name++;
  ////for(idx=0;idx<4;idx++)
  ////{
  ////  if((*(exp_name)!=' ')&&(*(exp_name)!='|'))
  ////   bran_num[idx] = *(exp_name);
  ////  else
  ////  {
  ////   bran_num[idx] = '\0';
  ////   break;
  ////  }
  ////  exp_name++; 
  ////} 
  ////fprintf(dbg_out, "\n the branch number is %d",atoi(bran_num));
  //return atoi(bran_num); 
  //return cfg_x->branch_number;
}
void print_branch()
{
  //extern _cfg_node * branch_table[MAXNUMBRAN]; 
  int idx=0;
  while(branch_table[idx]!=NULL)
  {
    fprintf(stdout,"---%s---\n",branch_table[idx]->exp_name);
    idx++;
  }
}

//==============================================================================================
//build the symbole table

int add_to_var_table(char * var_name, int width_start, int width_end)
{
  int idx;
 
  //fprintf(cfg_out,"\nadd a new variable to list, name %s\n",var_name); 
  for(idx=0;idx<var_table_id;idx++)
  {
    if(strcmp(var_name,current_var_table[idx].var_name)==0)
      return 0;
  }
  
  strcpy(current_var_table[var_table_id].var_name,var_name);
  current_var_table[var_table_id].width_start = width_start;
  current_var_table[var_table_id].width_end = width_end;
  current_var_table[var_table_id].var_id  = var_table_id;
  current_var_table[var_table_id].Is_input_port  = input_handling;
  var_table_id++;
   
  return var_table_id; 
}
void print_var_table(_var_table * current_var_table, int var_table_id)
{
  int idx;
  fprintf(dbg_out,"\n===calling the print_var_table var_table_id=%d",var_table_id);
  for(idx=0;idx<var_table_id;idx++)
    fprintf(dbg_out, "\n---the variable is %s[%d:%d] id=%d",current_var_table[idx].var_name,current_var_table[idx].width_start,current_var_table[idx].width_end,current_var_table[idx].var_id); 
}

int get_range_start(char * var_range)
{
  char str_range[5];
  int idx;
  for(idx=0;idx<5;idx++)  
  {
    if(*(var_range+idx+1)!=' ')
      str_range[idx]=*(var_range+idx+1);
    else
      break;
  }
  str_range[idx]='\0';
  printf("\n--var_range = %s, range_start=%s\n",var_range,str_range);
  return atoi(str_range); 
}
int get_range_end(char * var_range)
{
  char str_range[5];
  int idx_src;
  int idx=0;
  while(*(var_range+idx)!=' ')
    idx++;
  idx++;
  for(idx_src=0;idx_src<5;idx_src++)
  {
    if(*(var_range+idx+idx_src)!=')')
      str_range[idx_src]=*(var_range+idx+idx_src);
    else
      break;
  }  
  str_range[idx_src]='\0';
  printf("\nvar_range = %s, range_end=%s\n",var_range,str_range);
  return atoi(str_range); 
}
int get_var_entry(char * var_name)
{
  int idx;
  for(idx=0;idx<var_table_id;idx++)
  {
    if(strcmp(var_name,current_var_table[idx].var_name)==0)
    {
      //fprintf(cfg_out,"\nYes, find variable: %s\n",var_name);
      return idx;      
    }
  }
  fprintf(cfg_out,"\nFatal error, cannot find variable: %s\n",var_name);
  return -1;
}
void create_varlist_table()
{
   int idx;
   for(idx=0;idx<num_width_table_entries;idx++)
   { 
     //fprintf(cfg_out,"\nadd a new variable to list, name %s\n",wire_width_table[idx].name); 
     add_to_var_table(wire_width_table[idx].name,cur_range_start,cur_range_end);
     idx++;
   }
}
//==============================================================================================
//build the expression tree

_exptree_node * new_exptree_node(tree_node_type tree_node_t, _exptree_node * left, _exptree_node * middle, _exptree_node * right, int var_table_idx, char * number_string)
{
   _exptree_node * p_exptree_node = (_exptree_node *)malloc(sizeof(_exptree_node));
   p_exptree_node->tree_node_t = tree_node_t;
   p_exptree_node->left = left; 
   p_exptree_node->middle = middle; 
   p_exptree_node->right = right;   
   p_exptree_node->var_table_idx = var_table_idx;
   if(number_string!=NULL)
   {
     p_exptree_node->number_string = (char *)malloc(20*sizeof(char));
     strcpy(p_exptree_node->number_string,number_string);
   }
   p_exptree_node->Is_traversed = false;
   p_exptree_node->Is_top_node = false;
   //fprintf(cfg_out,"\n~~~~~a new expression node~~~~~~type: %d, var_table_idx: %d, num_string: %s~~~~~~~~~~\n",tree_node_t,var_table_idx,number_string);
   return p_exptree_node;
} 
//==============================================================================================
int get_number_width(char * number_str) //get the width of the number
{
   char width_str[6];
   int width_dec;
   char base_str[8];
   char number_string[64];
   printf("\nthe number string is %s\n",number_str);
   sscanf(number_str,"%s%d%s%s",width_str,&width_dec,base_str,number_string);
   return width_dec;
}
int binstr2dec(char * number_string)
{
  int idx_char=0;
  int sum_bin=0;
  do{
     if(number_string[idx_char]=='1')
       sum_bin = sum_bin * 2 + 1;
     else
       sum_bin = sum_bin * 2; 
     idx_char ++;
  } while(number_string[idx_char]!='\0');
  return sum_bin;
}

char * get_number_string(char * number_str) //translate to decimal
{
   char width_str[6];
   int width_dec;
   char base_str[8];
   char number_string[64];
   char * ret_str;
   int number_dec;
   sscanf(number_str,"%s%d%s%s",width_str,&width_dec,base_str,number_string);
   if(strcmp(base_str,"binary")==0)
     //sscanf(number_str,"%s %d %s %b",width_str,&width_dec,base_str,&number_dec);
     number_dec = binstr2dec(number_string);
   if(strcmp(base_str,"octal")==0)
     sscanf(number_str,"%s%d%s%o",width_str,&width_dec,base_str,&number_dec);
   if(strcmp(base_str,"decimal")==0)
     sscanf(number_str,"%s%d%s%d",width_str,&width_dec,base_str,&number_dec);
   if(strcmp(base_str,"hex")==0)
     sscanf(number_str,"%s %d %s %x",width_str,&width_dec,base_str,&number_dec);
   ret_str = (char *)malloc(64*sizeof(char));
   printf("\n====%d=====\n",number_dec);
   sprintf(ret_str,"%d",number_dec);
   return ret_str; 
}
