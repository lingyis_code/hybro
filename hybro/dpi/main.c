#include "vlg2sExpr.h"

_modulestats *modstats, *thismodule;

int isExists (char *str) {
  FILE *desc;

  desc = fopen (str,"r");
  if (desc==NULL) return 0;
  fclose (desc);  
  return 1;
}

struct __errStruct make_error (int errnum, char *errstr) {
  struct __errStruct err;
  err.errorNumber = errnum;
  strcpy (err.errString, errstr);
  return err;
}

int printError (struct __errStruct err) {
  switch (err.errorNumber) {
  case 0 : return 0;
  case 1 : fprintf (stderr, err.errString); return -1;
  case 2 : fprintf (stderr, "Invalid input argument filename -- %s\n", err.errString); return -1;
  case 3 : fprintf (stderr, err.errString); return -1;
  case 4 : fprintf (stderr, "vlg2sExpr : %s\n", err.errString); return -1;
  case 5 : fprintf (stderr, err.errString); return -1;
  default: return 0;
  }
}

//the main function
int main (int argc, char *argv[]) {
  time_t tim = time(0);
  size_t t = 256;
  int idx;

  /* Setting up outputs
   * some preliminaries
   */
  char *hostname = (char *) malloc (1024); gethostname (hostname,t);
  char *domainname = (char *) malloc (1024); getdomainname (domainname,t);
  char *loginname = (char *) malloc (80); if (getlogin()!=NULL) strcpy (loginname, getlogin()); else strcpy (loginname, "(null)");

  fprintf (stdout, "#|\n  Verilog to s-expressions Converter Version 0.1\n");
  fprintf (stdout, "  Run initiated by user %s @ %s\n", loginname, hostname);
  fprintf (stdout, "  in %s on %s\n", domainname, ctime(&tim));
  fprintf (stdout, "|#\n\n");
  fprintf (stderr, "#|  Verilog to s-expressions Converter Version 0.1\n");
  fprintf (stderr, "  Run initiated by user %s @ %s\n", loginname, hostname);
  fprintf (stderr, "  in %s on %s\n", domainname, ctime(&tim));
  fprintf (stderr, "|#\n\n");

  switch (argc) {
  case 0:
  case 1: 
    return printError (make_error (5,"vlg2sExpr: Too few arguments ::  Usage -- ./vlg2sExpr <Verilog_filename>\n"));
  case 2: 
    if (isExists (argv[1]) == 0) return printError (make_error (2,argv[1])); 
    break;
  default:  return printError (make_error (5,"vlg2sExpr: Too few arguments ::  Usage -- ./vlg2sExpr <Verilog_filename>\n"));
  }
  dbg_out = fopen("debug.out","w");
  cfg_out = fopen("cfg.out","w");
  //  char *ll = strrchr (argv[1],(int) '/'); 
  //  if (ll==NULL) ll = argv[1]; else ll++;
  //  char *ll = argv[1];
  //initialize the branch table
  for (idx=0; idx<MAXNUMBRAN; idx++)
    branch_table[idx]=NULL;
  fprintf (stderr, "vlg2sExpr: Translating input Verilog file %s...", argv[1]);
  yyin = (argc > 1)? fopen (argv[1], "r") : stdin;
  while (!feof(yyin)) yyparse();
  fclose (yyin);
  fprintf (stderr, " done\n");
  //printf("%d\n",cfg_stack_pointer);
  //print out the CFG of the entire module for testing
  print_all(); 
  fprintf(stdout,"\n--------------------------------\n");
  print_branch(); 
  fprintf(stdout,"\n--------------------------------\n");
  return 0;
}
