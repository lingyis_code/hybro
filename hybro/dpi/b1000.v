`timescale 1ns/10ps

module b1000 (clock , reset, inst_in, instruction_valid, pc, srcA_tmp, srcB_tmp, srcC_tmp);

	  input  clock;
	  input  reset;


       input [31:0] inst_in;
       input instruction_valid;      
       input [31:0] pc;
        
       input [31:0] srcA_tmp;
       input [31:0] srcB_tmp;
       input [31:0] srcC_tmp;
        
       //output [3:0] regA_tmp; 
       //output [3:0] regB_tmp;
       //output [3:0] regC_tmp;
            

	  reg [7:0] branvar[0:100];


       reg [31:0] pc_reg;
       wire [31:0] pc;
       wire [31:0] inst_in;
       reg  [31:0] instruction;
       wire [31:0] srcA_tmp;
       wire [31:0] srcB_tmp;
       wire [31:0] srcC_tmp;
       reg [31:0] srcA_tmp_dly;
       reg [31:0] srcB_tmp_dly;
       reg [31:0] srcC_tmp_dly;

       wire [7:0] opcode_tmp;
       //wire [3:0] regA_tmp;
       //wire [3:0] regB_tmp;
       //wire [3:0] regC_tmp;
       
       wire [15:0] label_tmp_1;
       wire [15:0] label_tmp_2;
       reg  [15:0] label_tmp;
       wire [23:0] longlabel_tmp;


       reg [32:0] branch_target_address;

       assign opcode_tmp = instruction[31:24];
       //assign regA_tmp = instruction[19:16];
       //assign regB_tmp = instruction[15:12];
       //assign regC_tmp = instruction[23:20];

       assign  label_tmp_1 = instruction[15:0];
       assign  label_tmp_2 = 16'hffff - label_tmp_1 + 1;
       assign  longlabel_tmp = instruction[23:0];
     
       assign label_tmp = instruction[15]? label_tmp2 : label_tmp1;
       //always @(*)
       //begin
       //  if(instruction[15])
       //     label_tmp = label_tmp_2;
       //  else
       //     label_tmp = label_tmp_1;    
       //end 

	  always @ (posedge  clock) 
	  begin
	  if( reset) 
	    begin 
           pc_reg <= 32'h0;
           branch_target_address <= 32'h0;
           instruction <= 32'h0;
           srcA_tmp_dly <= 32'h0;
           srcB_tmp_dly <= 32'h0;
           srcC_tmp_dly <= 32'h0;
	    end
	  else if(instruction_valid)
	    begin 
           pc_reg <= pc;
           instruction <= inst_in;
           srcA_tmp_dly <= srcA_tmp;
           srcB_tmp_dly <= srcB_tmp;
           srcC_tmp_dly <= srcC_tmp;
           case(opcode_tmp)
             8'h10:
               begin
                 if(srcA_tmp_dly == srcC_tmp_dly)
                   branch_target_address <= pc_reg + label_tmp;
                 else 
                   branch_target_address <= pc_reg + 1;
               end
             8'h11:
               begin
                 if(srcA_tmp_dly != srcC_tmp_dly)
                   branch_target_address <= pc_reg + label_tmp;
                 else 
                   branch_target_address <= pc_reg + 1;
               end 
             8'h12:
               begin
                 if(srcA_tmp_dly <  srcC_tmp_dly)
                   branch_target_address <= pc_reg + label_tmp;
                 else 
                   branch_target_address <= pc_reg + 1;
               end 
             8'h13:
               begin
                 if(srcA_tmp_dly <=  srcC_tmp_dly)
                   branch_target_address <= pc_reg + label_tmp;
                 else 
                   branch_target_address <= pc_reg + 1;
               end 
             8'h14:
               begin
                 if(srcA_tmp_dly >  srcC_tmp_dly)
                   branch_target_address <= pc_reg + label_tmp;
                 else 
                   branch_target_address <= pc_reg + 1;
               end 
             8'h15:
               begin
                 if(srcC_tmp_dly <=  srcA_tmp_dly)
                   branch_target_address <= pc_reg + label_tmp;
                 else 
                   branch_target_address <= pc_reg + 1;
               end 
             8'h16:
               begin
                   branch_target_address <= longlabel_tmp;
               end 
           endcase
         end
         else 
             begin
                 pc_reg <= pc_reg;
                 branch_target_address <= branch_target_address;
                 instruction <= instruction;
                 srcA_tmp_dly <= srcA_tmp_dly;
                 srcB_tmp_dly <= srcB_tmp_dly;
                 srcC_tmp_dly <= srcC_tmp_dly;
             end
       end
  
endmodule 
