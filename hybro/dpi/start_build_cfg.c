#include "vlg2sExpr.h"
_modulestats *modstats, *thismodule;

_constraint_s * constraint_stack_star[MAXCNST];
int constraint_stack_star_ptr=0;

_constraint_s * constraint_stack_temp_star[MAXCNST];
int constraint_stack_temp_star_ptr=0;

_var_list * used_chain_cur_frame = NULL;
 
FILE * bran_out;

//========statistical information=========
long int constraint_num = 0;
long int reduced_constraint_number = 0;
long int conflict_detect_number = 0;
long int infeasible_path_number = 0;
long int prun_sub_path_number = 0;


void print_constraint_stack()
{
  int s_top = constraint_stack_star_ptr-1;
  for(;s_top>=0;s_top--)
     fprintf(cfg_out,"\nstack[%d] = %s,frame[%d]\n",s_top,constraint_stack_star[s_top]->p_cfg_node_branch->p_exptree_node->number_string,constraint_stack_star[s_top]->frame_num);
}

void print_constraint_temp_stack()
{
  int s_top = constraint_stack_temp_star_ptr-1;
  for(;s_top>=0;s_top--)
     fprintf(cfg_out,"\nstack[%d] = %s,frame[%d]\n",s_top,constraint_stack_temp_star[s_top]->p_cfg_node_branch->p_exptree_node->number_string,constraint_stack_temp_star[s_top]->frame_num);
}



void add_to_cur_used_chain (_var_node * p_used_chain, bool Is_var_node)
{
   _var_node * p_vnode_temp = p_used_chain;
   _var_list * p_cur_used_chain = used_chain_cur_frame;
   
   
   while(p_vnode_temp!=NULL)
   {
     if(p_vnode_temp->Is_input_port)
     {
       p_vnode_temp = p_vnode_temp->next_node;
       continue;
     }
     //detect whether this variable has been existed in the chain
     while(p_cur_used_chain!=NULL)
     {
       if(((p_cur_used_chain->p_var_node->var_id)==(p_vnode_temp->var_id)))
       {
//+++#ifdef  DEBUG0_MODE
//+++      fprintf(bran_out,"\nfind a match, the var name is %s, input: %d\n",current_var_table[p_cur_used_chain->p_var_node->var_id].var_name,p_cur_used_chain->p_var_node->Is_input_port);
//+++#endif
         break;
       }
       p_cur_used_chain = p_cur_used_chain->next;
     }   
     if((p_cur_used_chain == NULL)&&(p_vnode_temp->Is_input_port==false))
     {
        //add the var node to the used chain
        _var_list * p_vlist = (_var_list *)malloc(sizeof(_var_list));
        p_vlist->p_var_node = p_vnode_temp;
        p_vlist->Is_defined = false;
        p_vlist->next = used_chain_cur_frame;
        used_chain_cur_frame = p_vlist;
#ifdef  DEBUG0_MODE
        fprintf(bran_out,"\nsuccessfully add an vairable to current used chain, %s\n",current_var_table[p_vnode_temp->var_id].var_name);
#endif
     }
     //get next var node to detect
     if(Is_var_node==true)
       return;
     p_vnode_temp = p_vnode_temp->next_node;
     p_cur_used_chain = used_chain_cur_frame;
   }
} 

void free_cur_used_chain()
{
   _var_list * p_cur_used_chain = used_chain_cur_frame;
   _var_list * p_cur_used_chain_temp;

   _var_list * p_new_used_chain_head=NULL;
   _var_list * p_new_used_chain_tail=NULL;

   while(p_cur_used_chain != NULL)
   {
     p_cur_used_chain_temp = p_cur_used_chain;
     p_cur_used_chain = p_cur_used_chain->next;
     if(p_cur_used_chain_temp->Is_defined == true)
       free(p_cur_used_chain_temp);
     else
     {
       if(p_new_used_chain_tail==NULL)
       {
         p_cur_used_chain_temp->next = NULL;
         p_new_used_chain_tail = p_cur_used_chain_temp;
         p_new_used_chain_head = p_cur_used_chain_temp;
       }
       else
       {
         p_cur_used_chain_temp->next = NULL;
         p_new_used_chain_tail->next = p_cur_used_chain_temp;
         p_new_used_chain_tail = p_cur_used_chain_temp;
       } 
     }
   }
   used_chain_cur_frame = p_new_used_chain_head;
}

bool Is_in_cur_used_chain(_var_node * p_defined_node)
{
  int v_idx = p_defined_node->var_id;
   _var_list * p_cur_used_chain = used_chain_cur_frame;
  

  while(p_cur_used_chain!=NULL)
  {
    if(v_idx==(p_cur_used_chain->p_var_node->var_id))
    {
      p_cur_used_chain->Is_defined = true;
      break;
    }
    p_cur_used_chain = p_cur_used_chain->next;
  }
  if(p_cur_used_chain==NULL)
    return false;
  else

    return true;

}


int isExists (char *str) {
  FILE *desc;

  desc = fopen (str,"r");
  if (desc==NULL) return 0;
  fclose (desc);  
  return 1;
}

struct __errStruct make_error (int errnum, char *errstr) {
  struct __errStruct err;
  err.errorNumber = errnum;
  strcpy (err.errString, errstr);
  return err;
}

int printError (struct __errStruct err) {
  switch (err.errorNumber) {
  case 0 : return 0;
  case 1 : fprintf (stderr, err.errString); return -1;
  case 2 : fprintf (stderr, "Invalid input argument filename -- %s\n", err.errString); return -1;
  case 3 : fprintf (stderr, err.errString); return -1;
  case 4 : fprintf (stderr, "vlg2sExpr : %s\n", err.errString); return -1;
  case 5 : fprintf (stderr, err.errString); return -1;
  default: return 0;
  }
}

void new_constraint(_cfg_node * p_cfg_node,_cfg_node * p_cfg_node_branch,_var_node * p_var_node_reg , int frame_num, bool Is_expr, bool case_or_if)
{
  //_constraint_s * p_topconstraint;
  _constraint_s * ptr_temp;
  //decide whether the current constraint is in the top of the constraint stack
  //if(constraint_stack_star_ptr!=0)
  //{
  //  p_topconstraint = constraint_stack_star[constraint_stack_star_ptr-1];
  //  if((p_topconstraint->p_cfg_node==p_cfg_node)&&(p_topconstraint->frame_num==frame_num))
  //    return false;
  //}
  constraint_num++;
  ptr_temp= (_constraint_s *)malloc(sizeof(_constraint_s));
  ptr_temp->p_cfg_node = p_cfg_node;
  ptr_temp->p_cfg_node_branch = p_cfg_node_branch;
  ptr_temp->p_var_node_reg = p_var_node_reg;
  ptr_temp->frame_num = frame_num;
  ptr_temp->Is_inverted = false;
  ptr_temp->Is_expr = Is_expr; 
  ptr_temp->case_or_if = case_or_if;
#ifdef DEBUG_MODE
  if(p_cfg_node!=NULL)
  fprintf(cfg_out,"\nconstraint %s is add to the stack with ptr = %d\n",p_cfg_node->p_exptree_node->number_string,constraint_stack_temp_star_ptr);
  else if(p_cfg_node_branch!=NULL)
  fprintf(cfg_out,"\nconstraint %s is add to the stack with ptr = %d\n",p_cfg_node_branch->p_exptree_node->number_string,constraint_stack_temp_star_ptr);
  else
  fprintf(cfg_out,"\nconstraints propagate is add to the stack\n");
#endif
  constraint_stack_temp_star[constraint_stack_temp_star_ptr++]=ptr_temp;
}
void free_constraint()
{
  constraint_stack_star_ptr--;
  _constraint_s * p_temp;
  _cfg_node * p_cfg_node_free;

  p_temp = constraint_stack_star[constraint_stack_star_ptr];
  if((p_temp->p_cfg_node!=NULL)&&(p_temp->Is_expr==true))
  {
    p_temp->p_cfg_node->guard_negated[p_temp->frame_num]=false;
    p_cfg_node_free =  p_temp->p_cfg_node->left_node;
    while(p_cfg_node_free!=NULL)
    {
      p_cfg_node_free->guard_negated[p_temp->frame_num]=false;
      p_cfg_node_free = p_cfg_node_free->brother_node;

      //----Set the explored space----
      //====if(p_cfg_node_free->Is_exit_branch == true)
      //===={
      //====  if(p_cfg_node_free->frame_num_exhaust > p_temp->frame_num)
      //====    p_cfg_node_free->frame_num_exhaust = p_temp->frame_num;
      //====}
      //====if(p_cfg_node_free->next_node->Is_exit_branch == true)
      //===={
      //====  if(p_cfg_node_free->frame_num_exhaust > p_temp->frame_num)
      //====    p_cfg_node_free->frame_num_exhaust = p_temp->frame_num;
      //====} 
    }
    //----Set the explored space----
  }
  free(constraint_stack_star[constraint_stack_star_ptr]);
}

bool Is_imp_branch(_cfg_node * p_bran)
{
  _cfg_node * p_bran_temp = p_bran;
  while((p_bran_temp->node_type!=NULL_NODE))
  {
    p_bran_temp = p_bran_temp->next_node;
    if((p_bran_temp->node_type == IFELSE_EXP)||(p_bran_temp->node_type == CASECOND_EXP))
      return true;
  }
  return false;
}

void set_dep_list(_cfg_node * cur_dep_bran,_cfg_node * bran_tmp)
{
  _cfg_node * p_cfg_tmp = bran_tmp; 
  _dep_list * p_obj_tmp;
  _dep_list * p_dep_tmp;
  if(cur_dep_bran == NULL)
    return;
  p_dep_tmp = (_dep_list *)malloc(sizeof(_dep_list));   
  p_dep_tmp->p_branch_cfg_node = cur_dep_bran;
  p_dep_tmp->p_dep_nxt = NULL;
  p_cfg_tmp->p_dep_list = p_dep_tmp;
#ifdef DEBUG1_MODE
  fprintf(bran_out,"\nbran %d depend on bran %d\n",bran_tmp->branch_number,cur_dep_bran->branch_number);
#endif
  p_obj_tmp = cur_dep_bran->p_dep_list;
  while(p_obj_tmp!=NULL)
  {
     p_dep_tmp->p_dep_nxt = (_dep_list *)malloc(sizeof(_dep_list));
     p_dep_tmp->p_dep_nxt->p_branch_cfg_node = p_obj_tmp->p_branch_cfg_node;
     p_dep_tmp->p_dep_nxt->p_dep_nxt = NULL;
#ifdef DEBUG1_MODE
  fprintf(bran_out,"\nbran %d depend on bran %d\n",bran_tmp->branch_number,p_obj_tmp->p_branch_cfg_node->branch_number);
#endif
     p_obj_tmp = p_obj_tmp-> p_dep_nxt;
     p_dep_tmp = p_dep_tmp->p_dep_nxt;
  }
} 

void build_dep_list()
{
  _cfg_block * p_cfg_block;
  _cfg_node * p_cfg_node_tmp;
  _cfg_node * p_cfg_node_bro;
  _cfg_node * bran_cfg_stack[MAXBRANNUM];
  _cfg_node * current_dep_bran=NULL;
  int bran_cfg_idx = 0;
  p_cfg_block = current_instance_node->p_cfg_block;
  while((p_cfg_block!=NULL))
  {
    p_cfg_node_tmp = p_cfg_block->p_cfg_node;
    while((p_cfg_node_tmp!=NULL))
    {
      if(p_cfg_node_tmp->Is_branch_node)
      {
         //set the dep_list
         set_dep_list(current_dep_bran,p_cfg_node_tmp); 
         //++++++++++++++++
         printf("\nDetecting branch %d\n",p_cfg_node_tmp->branch_number);
         if(Is_imp_branch(p_cfg_node_tmp))
           current_dep_bran = p_cfg_node_tmp;
         bran_cfg_stack[bran_cfg_idx++]= p_cfg_node_tmp;
         p_cfg_node_tmp = p_cfg_node_tmp->next_node;
      }
      else if(p_cfg_node_tmp->node_type == NULL_NODE)
      {
         p_cfg_node_bro = bran_cfg_stack[bran_cfg_idx-1];
         printf("\nbranch %d is converged to NULL node: %d,\n",p_cfg_node_bro->branch_number,p_cfg_node_tmp);
         if(p_cfg_node_bro->brother_node!=NULL)
         {
           bran_cfg_idx--;  
           p_cfg_node_tmp = p_cfg_node_bro->brother_node;
           if(p_cfg_node_bro->p_dep_list!=NULL)
             current_dep_bran = p_cfg_node_bro->p_dep_list->p_branch_cfg_node;
         }
         else
         {
           bran_cfg_idx--;
           p_cfg_node_tmp = p_cfg_node_tmp->next_node;
         }
      }
      else if((p_cfg_node_tmp->node_type == IFELSE_EXP)||(p_cfg_node_tmp->node_type == CASECOND_EXP)||(p_cfg_node_tmp->node_type == RESET_IF))
        p_cfg_node_tmp = p_cfg_node_tmp->left_node;
      else 
        p_cfg_node_tmp = p_cfg_node_tmp->next_node;
    } 
    p_cfg_block = p_cfg_block->cfg_block_next;
  }   

}

void build_invert_dep_list()
{
  int idx;
  int idy;
  _cfg_node * p_bran_cur;
  _dep_list * p_dep_lst;
  _dep_list * p_dep_tmp;
  for (idx=0; (idx<MAXNUMBRAN)&&(branch_table[idx]!=NULL); idx++)
  {
    p_bran_cur = branch_table[idx];
    p_dep_lst = p_bran_cur->p_dep_list;
    while(p_dep_lst!=NULL)
    {
      p_dep_tmp = p_dep_lst;
      p_dep_lst = p_dep_lst->p_dep_nxt;
      free(p_dep_tmp);
      p_bran_cur->p_dep_list = p_dep_lst;
    }
    for(idy=idx; (idy<MAXNUMBRAN)&&(branch_table[idy]!=NULL);idy++)
    {
      p_dep_lst = branch_table[idy]->p_dep_list;
      while(p_dep_lst!=NULL)
      {
        if(p_dep_lst->p_branch_cfg_node==p_bran_cur)
        {
           p_dep_tmp = p_bran_cur->p_dep_list;
           p_bran_cur->p_dep_list = (_dep_list *)malloc(sizeof(_dep_list)); 
           p_bran_cur->p_dep_list->p_branch_cfg_node = branch_table[idy];
           p_bran_cur->p_dep_list->p_dep_nxt = p_dep_tmp;
#ifdef DEBUG1_MODE
           fprintf(bran_out, "\nbran %d can decide bran %d\n",idx,idy);
#endif
           break;
        }  
        p_dep_lst = p_dep_lst->p_dep_nxt;
      }
    }   
  }
}


extern "C" void start_build_cfg(char * vlg_file) 
{
  time_t tim = time(0);
  size_t t = 256;
  int idx;
  int frame_num;

  /* Setting up outputs
   * some preliminaries
   */
  char *hostname = (char *) malloc (1024); gethostname (hostname,t);
  char *domainname = (char *) malloc (1024); getdomainname (domainname,t);
  char *loginname = (char *) malloc (80); if (getlogin()!=NULL) strcpy (loginname, getlogin()); else strcpy (loginname, "(null)");
  const char * vlg_source = (char *)malloc(20*sizeof(char));
#ifdef DEBUG_MODE
  fprintf (stdout, "  The source verilog file is %s\n",vlg_file);
  fprintf (stdout, "#|Verilog to s-expressions Converter Version 0.1\n");
  fprintf (stdout, "  Run initiated by user %s @ %s\n", loginname, hostname);
  fprintf (stdout, "  in %s on %s\n", domainname, ctime(&tim));
  fprintf (stdout, "|#\n\n");
#endif
  //initialize the branch table
  for (idx=0; idx<MAXNUMBRAN; idx++)
    branch_table[idx]=NULL;
  dbg_out = fopen("debug.out","w");
  cfg_out = fopen("cfg.out","w");
  bran_out = fopen("bran.out","w");
#ifdef DEBUG_MODE
  printf("\n---the vlg source code is %s---\n", vlg_file); 
#endif
  yyin = fopen (vlg_file, "r");
  while (!feof(yyin)) yyparse();
  fclose (yyin);
  fprintf (stdout, " done\n");
  //printf("%d\n",cfg_stack_pointer);
  //print out the CFG of the entire module for testing
  print_all(); 
  fprintf(stdout,"\n--------------------------------\n");
  print_branch(); 
  fprintf(stdout,"\n--------------------------------\n");
  for (idx=0; (idx<MAXNUMBRAN)&&(branch_table[idx]!=NULL); idx++)
    for (frame_num=0;frame_num<MAXFRAMES;frame_num++)
      branch_table[idx]->guard_negated[frame_num]=false;

  //exit(-1);
  //build the dependency relationship between branch
  build_dep_list(); 
  build_invert_dep_list(); 
}


bool Is_covered_before(_cfg_node * p_cfg_cond, int frame_no)
{
  _dep_list * p_dep_list_tmp = p_cfg_cond->p_dep_list;
  if(p_cfg_cond->Is_covered[frame_no]==0)
    return false;
  while(p_dep_list_tmp!=NULL)
  {
    if(p_dep_list_tmp->p_branch_cfg_node->Is_covered[frame_no]==0) 
      return false;
    p_dep_list_tmp = p_dep_list_tmp->p_dep_nxt;
  }
  return true; 
}

extern "C" void record_branch(int bran_num, int frame_num)
{
//#ifdef DEBUG_MODE
   fprintf(bran_out, "\n---record the branch number: %d in frame %d---\n",bran_num,frame_num);
//#endif
   branch_table[bran_num]->branch_taken[frame_num] = true;
   branch_table[bran_num]->Is_covered[frame_num]++; 
}

extern "C" void result_stat(int bran_num_max, int frame_num, int pattern_num)
{
  long int sum_covered = 0;
  int i,j;

  for(i=0;i<bran_num_max;i++)
  {
    for(j=0;j<frame_num;j++)
      sum_covered = sum_covered + branch_table[i]->Is_covered[j];
  }  

  printf("\npattern number is %d\n",pattern_num);
  printf("\nconstraint number is %d\n",constraint_num);
  printf("\nreduced constraint number is %d\n",reduced_constraint_number);
  printf("\nconflict_detect_number is %d\n",conflict_detect_number);
  printf("\ninfeasible_path_number %d\n",infeasible_path_number);
  printf("\nprun_sub_path_number %d\n",prun_sub_path_number);
  printf("\nthe average covered number per branch is %d\n", sum_covered);  
  printf("\nthe average covered number per branch is %d\n", bran_num_max);  
  printf("\nthe average covered number per branch is %d\n", sum_covered/bran_num_max);  

}

 
extern "C" int generate_nxt_pattern(int unroll_frame_num, int pat_num)
{
   int idx;
   int frame_num;
   int frame_num_pre;
   int solver_ret;
   _cfg_node * p_exit_branch; 
   _cfg_block * p_cfg_block;
   _cfg_node * p_cfg_node_tmp;
   _cfg_node * p_cfg_branch_tmp;
   _cfg_node * p_cfg_node_tmp_pre;
   _cfg_node * p_chosen_bran;
   _constraint_s * p_top;
   int p_stack_idx=0;
#ifdef RANDOM_GEN
   if(pat_num == PAT_NUM)
     return 0;
   else return 1;
#endif
//#ifdef DEBUG_MODE
   fprintf(bran_out, "\n---start to extract constraint for frame---\n");
//#endif
   //======================================================================
   //==================Collect the current constraints=====================
   //traverse all the CFG and extract constraints
   //from the last frame to the previous frame
   constraint_stack_temp_star_ptr = 0;
#ifdef LEARN_MODE_1
   free_cur_used_chain();
#endif
   if(constraint_stack_star_ptr == 0)
   {
     p_top = NULL;
     frame_num_pre = 0;
   }
   else 
   {
     p_top = constraint_stack_star[constraint_stack_star_ptr-1];
     frame_num_pre = p_top->frame_num;;
   }
   for(idx=unroll_frame_num;idx>=frame_num_pre;idx--) 
   {
     p_cfg_block = current_instance_node->p_cfg_block;
#ifdef DEBUG_MODE
     printf("\n~~~start to calculate constraints for frame %d~~~\n",idx);
#endif
     while((p_cfg_block!=NULL))
     {
       p_cfg_node_tmp = p_cfg_block->p_cfg_node;
       p_exit_branch = NULL; 
       while((p_cfg_node_tmp!=NULL))
       {
#ifdef DEBUG_MODE
          if(p_cfg_node_tmp->p_exptree_node!=NULL)
            fprintf(stdout,"\n==current node is frame: %d---%s==\n",idx,p_cfg_node_tmp->p_exptree_node->number_string);
          else
            fprintf(stdout,"\n==current node is frame: %d---NULL node==\n",idx);
#endif
          switch(p_cfg_node_tmp->node_type)
          {
            case RESET_IF :
              p_cfg_node_tmp_pre = p_cfg_node_tmp;
              p_cfg_node_tmp = p_cfg_node_tmp_pre->left_node;
              if((p_cfg_node_tmp!=NULL) && (p_cfg_node_tmp->branch_taken[idx]==true))
              {
                //fprintf(cfg_out,"\nHere\n");
                new_constraint(p_cfg_node_tmp_pre,p_cfg_node_tmp->next_node,NULL,idx,true,false);
                p_exit_branch = p_cfg_node_tmp;
                p_cfg_node_tmp = p_cfg_node_tmp->next_node;
               
              }
              else if((p_cfg_node_tmp!=NULL) && (p_cfg_node_tmp->branch_taken[idx]==false))
              {
                //fprintf(cfg_out,"\nthere\n");
                p_cfg_node_tmp = p_cfg_node_tmp->brother_node;
                p_exit_branch = p_cfg_node_tmp;
                new_constraint(p_cfg_node_tmp_pre,p_cfg_node_tmp->next_node,NULL,idx,true,false);
                p_cfg_node_tmp = p_cfg_node_tmp->next_node;
              } 
              else
                printf("\n\n===========FATAL ERROR: there is no following node of an RESET IF statement=============\n\n");   
              break;
            case IFCOND_EXP :
              printf("\n\n===========FATAL ERROR: there should not exist the IF SINGLE STATEMENT=============\n\n");  
              break;
            case IFELSE_EXP :
              p_cfg_node_tmp_pre = p_cfg_node_tmp;
              p_cfg_node_tmp = p_cfg_node_tmp_pre->left_node;
              if((p_cfg_node_tmp!=NULL) && (p_cfg_node_tmp->branch_taken[idx]==true))
              {
                //fprintf(cfg_out,"\nHere\n");
                new_constraint(p_cfg_node_tmp_pre,p_cfg_node_tmp->next_node,NULL,idx,true,false);
                p_exit_branch = p_cfg_node_tmp;
                p_cfg_node_tmp = p_cfg_node_tmp->next_node;
               
              }
              else if((p_cfg_node_tmp!=NULL) && (p_cfg_node_tmp->branch_taken[idx]==false))
              {
                //fprintf(cfg_out,"\nthere\n");
                p_cfg_node_tmp = p_cfg_node_tmp->brother_node;
                p_exit_branch = p_cfg_node_tmp;
                new_constraint(p_cfg_node_tmp_pre,p_cfg_node_tmp->next_node,NULL,idx,true,false);
                p_cfg_node_tmp = p_cfg_node_tmp->next_node;
              } 
              else
                printf("\n\n===========FATAL ERROR: there is no following node of an IF statement=============\n\n");   
              break;
            case CASECOND_EXP :
              p_cfg_node_tmp_pre = p_cfg_node_tmp;
              p_cfg_node_tmp = p_cfg_node_tmp_pre->left_node;
              if(p_cfg_node_tmp!=NULL)
              {
                 while((p_cfg_node_tmp!=NULL)&&(p_cfg_node_tmp->next_node->branch_taken[idx]==false))
                   p_cfg_node_tmp = p_cfg_node_tmp->brother_node; 
                 if(p_cfg_node_tmp==NULL)
                   printf("\n\n===========FATAL ERROR: there is no branch taken at this frame,bran_num = %d =============\n\n",p_cfg_node_tmp_pre->left_node->next_node->bran_num);   
                 else
                 {
                   new_constraint(p_cfg_node_tmp_pre,p_cfg_node_tmp,NULL,idx,true,true);
                   p_exit_branch = p_cfg_node_tmp->next_node;
                   p_cfg_node_tmp->guard_negated[idx] = true;
                   p_cfg_node_tmp = p_cfg_node_tmp->next_node->next_node;
                 }  
              }
              else 
                printf("\n\n===========FATAL ERROR: there is no following node of an CASE statement=============\n\n");   
              break;
            case CASE_EXP :
                printf("\n\n===========FATAL ERROR: there should be no following node of an CASE_EXP=============\n\n");   
              break;
            case CASE_DEFAULT :
                printf("\n\n===========FATAL ERROR: there should be no following node of an CASE_DEFAULT=============\n\n");   
              break;
            case BLK_AS :
#ifdef DEBUG_MODE
               printf("\n-------Blocking assignment--%s-----\n",p_cfg_node_tmp->p_exptree_node->number_string);
#endif
               Is_in_cur_used_chain(p_cfg_node_tmp->defined_var_chain);
               new_constraint(NULL,p_cfg_node_tmp,NULL,idx,false,false);
               p_cfg_node_tmp = p_cfg_node_tmp->next_node;
              break;
            case NBLK_AS :
#ifdef DEBUG_MODE
               fprintf(cfg_out,"\n-------Nonblocking assignment: %s--------\n",p_cfg_node_tmp->p_exptree_node->number_string);
#endif
               if(p_cfg_node_tmp->Is_branch_node)
               {
                 p_cfg_node_tmp = p_cfg_node_tmp->next_node;
               }
               else
               { 
#ifdef LEARN_MODE_1
               if(Is_in_cur_used_chain(p_cfg_node_tmp->defined_var_chain))
#endif
                 new_constraint(NULL,p_cfg_node_tmp,NULL,idx,false,false);
#ifdef LEARN_MODE_1
               else
                 reduced_constraint_number++;
#endif
                 p_cfg_node_tmp = p_cfg_node_tmp->next_node;
               }
              break;
            case PORT_CONNECT :
               new_constraint(NULL,p_cfg_node_tmp,NULL,idx,false,false);
               p_cfg_node_tmp = p_cfg_node_tmp->next_node;
              break;
            case NULL_NODE :
               //fprintf(cfg_out,"\nNULL_node\n");
               p_cfg_node_tmp = p_cfg_node_tmp->next_node;
              break; 
            default :
              break; 
          }
       }
       if(p_exit_branch!=NULL)
         p_exit_branch->Is_exit_branch = true;
       //else
       //  printf("\n=====FATAL ERROR: there should be a exit branch node=====\n");
       p_cfg_block = p_cfg_block->cfg_block_next;
     }
     free_cur_used_chain();
     //finish one frame, start to extract the used variable and add them to the used chain
     //add the new constraints  _var_list * p_cur_used_chain = used_chain_cur_frame;
     //++++++++++++++++++++++++++++++++++++++++++++++++
     _var_list * p_cur_used_chain = used_chain_cur_frame;
     while(p_cur_used_chain!=NULL)
     {
       new_constraint(NULL,NULL,p_cur_used_chain->p_var_node,idx,false,false);
#ifdef DEBUG0_MODE
       fprintf(bran_out,"\nPropagate vairable %s,frame = %d\n",current_var_table[p_cur_used_chain->p_var_node->var_id].var_name,idx);
#endif
       used_chain_cur_frame =  used_chain_cur_frame->next;
       free(p_cur_used_chain);
       p_cur_used_chain = used_chain_cur_frame;
     }
     used_chain_cur_frame = NULL;
     //++++++++++++++++++++++++++++++++++++++++++++++++
     if(idx>0)
     {
 
       for(int bound_idx=get_frame_down_bound(idx);bound_idx<=get_frame_up_bound(idx);bound_idx++)
       {
         if(constraint_stack_temp_star[bound_idx]->p_cfg_node!=NULL) 
           add_to_cur_used_chain(constraint_stack_temp_star[bound_idx]->p_cfg_node->used_var_chain,false);
         else if(constraint_stack_temp_star[bound_idx]->p_cfg_node_branch!=NULL)
           add_to_cur_used_chain(constraint_stack_temp_star[bound_idx]->p_cfg_node_branch->used_var_chain,false);
         else
           add_to_cur_used_chain(constraint_stack_temp_star[bound_idx]->p_var_node_reg,true);
       }
     }
   }
   //Merge the constraint with the previous constraints
#ifdef DEBUG_MODE
   fprintf(cfg_out,"\nstart to merge constraints---stack----%d-----\n",constraint_stack_temp_star_ptr);
#endif
   //print_constraint_temp_stack();
   merge_design_constraints(frame_num_pre);
   //print_constraint_stack();
#ifdef DEBUG_MODE
   fprintf(cfg_out,"\nend to merge constraints---stack----%d-----\n",constraint_stack_star_ptr);
#endif

   //====================================================================
   //======================detect the exhaust space======================
#ifdef LEARN_MODE_3
   p_stack_idx = 0;
   while(p_stack_idx<constraint_stack_star_ptr) 
   {
     p_top = constraint_stack_star[p_stack_idx]; 
     if(p_top->Is_expr == true)
     {
       //get the current p_exit_branch
       if(p_top->case_or_if == true)
       {
         //case statement
         if(p_top->p_cfg_node_branch->Is_exit_branch == true)
         {
           if(p_top->p_cfg_node_branch->exhaust_bran[p_top->frame_num]==true)
             break;
             //printf("\ncan prune the space\n");
         }
       }
       else
       {
         //if statement
         if((p_top->p_cfg_node->left_node->next_node)==(p_top->p_cfg_node_branch))
           {
             if(p_top->p_cfg_node->left_node->Is_exit_branch == true)
             {
               if(p_top->p_cfg_node->left_node->exhaust_bran[p_top->frame_num]==true)
                 break;
                 //printf("\ncan prune the space IF\n");
             }
           }
           else
           {  
             if(p_top->p_cfg_node->left_node->brother_node->Is_exit_branch == true)
             {
               if(p_top->p_cfg_node->left_node->brother_node->exhaust_bran[p_top->frame_num]==true)
                 break;
                 //printf("\ncan prune the space ELSE\n");
             } 
           }
 
       }
     } 
     p_stack_idx++;
   }
#endif
   //----------------------------------------------------------------------------------------------------
   //free the constraints
   //constraint_stack_star_ptr
#ifdef LEARN_MODE_3
   if(constraint_stack_star_ptr>(p_stack_idx+1))
     prun_sub_path_number++;
   while(constraint_stack_star_ptr>(p_stack_idx+1))
   {
     constraint_stack_star_ptr--;
     free(constraint_stack_star[constraint_stack_star_ptr]);
   }
//-----------------------------------------------------------------------------------------------------
#endif
   //====================================================================
   //==================start to call the stp solver======================
   //Sustain a constraint stack
   do {

     while(1)
     {
       if (constraint_stack_star_ptr==0) 
         return 0;
       else
         p_top = constraint_stack_star[constraint_stack_star_ptr-1];
       //while((p_top->Is_expr==false) ||(p_top->p_cfg_node->node_type == RESET_IF) ||(p_top->p_cfg_node->guard_negated[p_top->frame_num]==true)||(subspace_prune(p_top)))//||(p_top->p_cfg_node->frame_num_exhaust<=p_top->frame_num))
       while((p_top->Is_expr==false) ||(p_top->p_cfg_node->node_type == RESET_IF) ||(p_top->p_cfg_node->guard_negated[p_top->frame_num]==true))
       {
         //---------------------------------------------------------------------------------------------
         //---------------------------------------------------------------------------------------------
         free_constraint();  

#ifdef DEBUG_MODE
         printf("\nfree a constraints---stack----%d-----\n",constraint_stack_star_ptr);
#endif
         if (constraint_stack_star_ptr==0) 
           return 0;
         p_top = constraint_stack_star[constraint_stack_star_ptr-1]; 
       } 
       //choose the last one to negate
       //p_top->Is_inverted = true;
       if((p_top->p_cfg_node->node_type) == IFELSE_EXP)    
       {
         p_top->p_cfg_node->guard_negated[p_top->frame_num]=true;
         if(p_top->p_cfg_node->frame_num_exhaust > p_top->frame_num)
           p_top->p_cfg_node->frame_num_exhaust = p_top->frame_num;
         //---------------------------------------------------------------------------------------------
         if((p_top->p_cfg_node->left_node->next_node)==(p_top->p_cfg_node_branch))
         { 
            if(p_top->p_cfg_node->left_node->frame_num_exhaust > p_top->frame_num)
              p_top->p_cfg_node->left_node->frame_num_exhaust = p_top->frame_num;
            p_top->p_cfg_node->left_node->exhaust_bran[p_top->frame_num]=true;
            p_top->p_cfg_node_branch = p_top->p_cfg_node->left_node->brother_node->next_node;
            p_chosen_bran = p_top->p_cfg_node->left_node->brother_node;
         }
         else
         {
            if(p_top->p_cfg_node->left_node->brother_node->frame_num_exhaust > p_top->frame_num)
              p_top->p_cfg_node->left_node->brother_node->frame_num_exhaust = p_top->frame_num;
            p_top->p_cfg_node->left_node->brother_node->exhaust_bran[p_top->frame_num]=true;
            p_top->p_cfg_node_branch = p_top->p_cfg_node->left_node->next_node;
            p_chosen_bran = p_top->p_cfg_node->left_node;
         }
         //---------------------------------------------------------------------------------------------
        //printf("\ncovered before IF_Pre\n");
#ifdef BRAN_COV
        if(Is_covered_before(p_chosen_bran,p_top->frame_num))
        {
           //printf("\ncovered before IF\n");
           fprintf(bran_out, "\n---Branch has been covered before---\n");
           free_constraint();  
           continue;
        }
#endif

#ifdef LEARN_MODE_2
         if(detect_local_conflict(p_top,0))
         {
           fprintf(bran_out, "\n---Local conflict---\n");
           free_constraint();  
           continue;
         } 
         else
#endif
           break;
       } 
       else if((p_top->p_cfg_node->node_type) == CASECOND_EXP)    
       {  
         p_cfg_branch_tmp = p_top->p_cfg_node->left_node;
         while((p_cfg_branch_tmp!=NULL)&&(p_cfg_branch_tmp->guard_negated[p_top->frame_num]==true))
           p_cfg_branch_tmp = p_cfg_branch_tmp->brother_node;

         //------------------------------------------------------------------------
         if(p_top->p_cfg_node_branch->frame_num_exhaust > p_top->frame_num)
           p_top->p_cfg_node_branch->frame_num_exhaust = p_top->frame_num;
         p_top->p_cfg_node_branch->exhaust_bran[p_top->frame_num]=true;
         //------------------------------------------------------------------------
         if(p_cfg_branch_tmp==NULL)
         {
           free_constraint();  
           continue;
         } 
         p_cfg_branch_tmp->guard_negated[p_top->frame_num]=true;
         p_top->p_cfg_node_branch = p_cfg_branch_tmp;
         p_chosen_bran = p_cfg_branch_tmp->next_node;

         p_cfg_branch_tmp = p_top->p_cfg_node->left_node;
         while((p_cfg_branch_tmp!=NULL)&&(p_cfg_branch_tmp->guard_negated[p_top->frame_num]==true))
           p_cfg_branch_tmp = p_cfg_branch_tmp->brother_node;
         if(p_cfg_branch_tmp==NULL)
         { 
           p_top->p_cfg_node->guard_negated[p_top->frame_num]=true;
           if(p_top->p_cfg_node->frame_num_exhaust > p_top->frame_num)
             p_top->p_cfg_node->frame_num_exhaust = p_top->frame_num;
           p_top->p_cfg_node->exhaust_bran[p_top->frame_num]=true;
         }

        //printf("\ncovered before CASE_Pre %d,%d\n",p_chosen_bran->Is_branch_node,p_chosen_bran->branch_number);
#ifdef BRAN_COV
        if(Is_covered_before(p_chosen_bran,p_top->frame_num))
        {
           //printf("\ncovered before CASE\n");
           fprintf(bran_out, "\n---Branch has been covered before---\n");
           free_constraint();  
           continue;
        }
#endif
#ifdef LEARN_MODE_2
         if(detect_local_conflict(p_top,1))
         {
           fprintf(bran_out, "\n---Local conflict---\n");
           free_constraint();  
           continue;
         }
         else 
#endif
           break; 
       }
     }

     //printf("\nstart to send constraints: top ---%s----\n",p_top->p_cfg_node->p_exptree_node->number_string);
     //send constraints to the STP solver
     //TO DO optimization: 
     //---(1)cache the pattern---
     //---(2)dynamic programming---   
#ifdef DEBUG_MODE
     printf("\nstart to send constraints: stack ----constraint_stack_star_ptr= %d----\n",constraint_stack_star_ptr);
#endif
//--------------------------------------------
     printf("\nStart to clear the STP solver\n");

     if (constraint_number!=0)
     {
       stp_destroy(var_table_id,unroll_frame_num);
     }

     printf("\nStart to Initial the STP solver\n");

     stp_initial(var_table_id, unroll_frame_num);

     for(idx=0;idx<constraint_stack_star_ptr;idx++)
     {
#ifdef DEBUG_MODE
       if(constraint_stack_star[idx]->p_cfg_node!=NULL)
         fprintf(cfg_out,"\n[send constrain cfg: %s]\n",constraint_stack_star[idx]->p_cfg_node->p_exptree_node->number_string); 
       else if(constraint_stack_star[idx]->p_cfg_node_branch!=NULL)
         fprintf(cfg_out,"\n[send constraint branch %d: %s]\n",constraint_stack_star[idx]->p_cfg_node_branch->node_type,constraint_stack_star[idx]->p_cfg_node_branch->p_exptree_node->number_string); 
       else
         fprintf(cfg_out,"\n[send constraint Var Propagate]\n");
#endif
       send_constraints(constraint_stack_star[idx]->p_cfg_node,constraint_stack_star[idx]->p_cfg_node_branch,constraint_stack_star[idx]->p_var_node_reg,constraint_stack_star[idx]->frame_num);
#ifdef DEBUG_MODE
       fprintf(cfg_out,"\nreturn from send constraints\n");
#endif
     }
     printf("\n--Start to stp solver--\n");
     solver_ret = stp_solver();  
     if(solver_ret == 1) 
     { 
       infeasible_path_number++;
       fprintf(bran_out, "\n---infeasible path---\n");
     } 
   } while(solver_ret == 1);

   //recover the constraint stack
   for(idx=0; idx<constraint_stack_star_ptr; idx++)
     free(constraint_stack_star[idx]);
   constraint_stack_star_ptr = 0;
   //====================================================================
   //==================recover the branch================================
   //Step III: recove the branch
   for (idx=0; (idx<MAXNUMBRAN)&&(branch_table[idx]!=NULL); idx++)
     for (frame_num=0;frame_num<MAXFRAMES;frame_num++)
       branch_table[idx]->branch_taken[frame_num]=false;
   return 1;
}

void send_constraints(_cfg_node * p_cfg_node,_cfg_node * p_cfg_node_branch,_var_node * p_var_node_reg, int frame_num)
{
  //translate the exp_name to constraints 
  //add them to the database
  //fprintf(cfg_out,"\nHere is the send constraint function");
  //if(p_cfg_node!=NULL)
  // fprintf(cfg_out,"\n---the current node at frame %d is %s---\n",frame_num,p_cfg_node->p_exptree_node->number_string);
  //else
  // fprintf(cfg_out,"\n---the current node at frame %d is %s---\n",frame_num,p_cfg_node_branch->p_exptree_node->number_string);

  if(p_cfg_node!=NULL)
  {
    //fprintf(cfg_out,"\n---the current node is %s---\n",p_cfg_node->p_exptree_node->number_string);
    if((p_cfg_node->node_type==IFELSE_EXP) || (p_cfg_node->node_type==RESET_IF))
    {
      if(p_cfg_node_branch == p_cfg_node->left_node->next_node)
      { 
        print_constraints(p_cfg_node->p_exptree_node,p_cfg_node_branch->p_exptree_node,NULL,frame_num,true,false); 
        stp_constraint_gen(p_cfg_node->p_exptree_node,p_cfg_node_branch->p_exptree_node,NULL,frame_num,true,false); 
      }
      else
      {
        print_constraints(p_cfg_node->p_exptree_node,p_cfg_node_branch->p_exptree_node,NULL,frame_num,true,true); 
        stp_constraint_gen(p_cfg_node->p_exptree_node,p_cfg_node_branch->p_exptree_node,NULL,frame_num,true,true); 
      }
    }
    else
    {
      print_constraints(p_cfg_node->p_exptree_node,p_cfg_node_branch->p_exptree_node,NULL,frame_num,false,false); 
      stp_constraint_gen(p_cfg_node->p_exptree_node,p_cfg_node_branch->p_exptree_node,NULL,frame_num,false,false); 
    }
  }
  else if(p_cfg_node_branch!=NULL)
  {
    print_constraints(NULL,p_cfg_node_branch->p_exptree_node,NULL,frame_num,false,false); 
    stp_constraint_gen(NULL,p_cfg_node_branch->p_exptree_node,NULL,frame_num,false,false);
  }
  else
  {
    print_constraints(NULL,NULL,p_var_node_reg,frame_num,false,false); 
    stp_constraint_gen(NULL,NULL,p_var_node_reg,frame_num,false,false);
  }   
#ifdef DEBUG_MODE
  fprintf(cfg_out,"\n-----------successfully return from the stp gen-------------\n");
#endif 
}

void print_constraints(_exptree_node * p_exptree_node,_exptree_node * p_exptree_node_branch,_var_node * p_var_node_reg, int frame_num, bool Is_if_branch,bool t_or_f)
{
  //fprintf(cfg_out,"\ncalling the print constraints function:\n");
  if((p_exptree_node!=NULL))
  {
#ifdef DEBUG0_MODE
    if((p_exptree_node->number_string!=NULL)&&(Is_if_branch==false))
      fprintf(bran_out,"\n at frame %d : %s = %s \n",frame_num,p_exptree_node->number_string,p_exptree_node_branch->number_string);
    else 
    { 
      if(t_or_f==false)
        fprintf(bran_out,"\n at frame %d : %s \n",frame_num,p_exptree_node->number_string);
      else
        fprintf(bran_out,"\n at frame %d : ! (%s) \n",frame_num,p_exptree_node->number_string);
    }
#endif
    
    //if(p_exptree_node->left!=NULL)
    //  print_constraints(NULL,p_exptree_node->left,NULL,frame_num,false,false);
    //if(p_exptree_node->middle!=NULL)
    //  print_constraints(NULL,p_exptree_node->middle,NULL,frame_num,false,false);
    //if(p_exptree_node->right!=NULL)
    //  print_constraints(NULL,p_exptree_node->right,NULL,frame_num,false,false);
    
  }
  else if(p_exptree_node_branch!=NULL)
  {
    //fprintf(cfg_out,"\ncalling the else branch\n");
#ifdef DEBUG0_MODE
    if((p_exptree_node_branch->number_string!=NULL)&&(p_exptree_node_branch->tree_node_t!=VAR)&&(p_exptree_node_branch->tree_node_t!=CNST))
      fprintf(bran_out,"\n at frame %d : %s \n",frame_num,p_exptree_node_branch->number_string);
#endif
    //if(p_exptree_node_branch->left!=NULL)
    //  print_constraints(NULL,p_exptree_node_branch->left,NULL,frame_num,false,false);
    //if(p_exptree_node_branch->middle!=NULL)
    //  print_constraints(NULL,p_exptree_node_branch->middle,NULL,frame_num,false,false);
    //if(p_exptree_node_branch->right!=NULL)
    //  print_constraints(NULL,p_exptree_node_branch->right,NULL,frame_num,false,false);
  }
#ifdef DEBUG0_MODE
  else
  {
    fprintf(bran_out,"\n at frame %d %s[%d]:\n",frame_num, current_var_table[p_var_node_reg->var_id].var_name, frame_num+1);     
  }
#endif
}

void merge_design_constraints(int frame_num)
{
  int unroll_frame_length = constraint_stack_temp_star[0]->frame_num;
  int idx;
  int down_bound;
  for(idx=frame_num;idx<=unroll_frame_length;idx++)
  {
#ifdef DEBUG_MODE
    printf("\n\n[the constraint_stack_temp_star_ptr is %d]\n\n",constraint_stack_temp_star_ptr);
    printf("\n\n[the constraint_stack_star_ptr is %d]\n\n",constraint_stack_star_ptr);
#endif
    down_bound = get_frame_down_bound(idx);
#ifdef DEBUG_MODE
    printf("\n\n[down bound is %d]\n\n",down_bound);
    printf("\n\n[up bound is %d]\n\n",get_frame_up_bound(idx));
#endif   
    push_to_constraint_stack(get_frame_up_bound(idx), get_frame_down_bound(idx));
    //if(idx<unroll_frame_length)
    //  clear_constraint_temp_stack(down_bound,get_frame_up_bound(idx+1)+1);
    //else
    //  clear_constraint_temp_stack(down_bound,0);
  }
  //clear_constraint_temp_stack(constraint_stack_temp_star_ptr,0); 
}


int get_frame_up_bound(int frame_num)
{
  int frame_up_bound = constraint_stack_temp_star_ptr-1;
  while(constraint_stack_temp_star[frame_up_bound]->frame_num!=frame_num)
    frame_up_bound--;
  return frame_up_bound;
}

int get_frame_down_bound(int frame_num)
{
  int frame_down_bound = constraint_stack_temp_star_ptr-1;
  if(frame_down_bound==0)
   return 0;
  while(constraint_stack_temp_star[frame_down_bound]->frame_num!=(frame_num+1))
  {
    if(constraint_stack_temp_star_ptr>0)
    {
      if(constraint_stack_star_ptr>0)
      {
        if((constraint_stack_temp_star[frame_down_bound]->frame_num==frame_num)&&(constraint_stack_temp_star[frame_down_bound]->frame_num == constraint_stack_star[constraint_stack_star_ptr-1]->frame_num)&&(constraint_stack_temp_star[frame_down_bound]->p_cfg_node == constraint_stack_star[constraint_stack_star_ptr-1]->p_cfg_node))
          return (frame_down_bound+1);
      }
    }
    if(frame_down_bound==0)
      return 0;
    frame_down_bound--;
  }
  return (frame_down_bound+1);
}

void push_to_constraint_stack(int up_bound, int down_bound)
{
  int idx;
  for(idx=down_bound;idx<=up_bound;idx++)
  {
    constraint_stack_star[constraint_stack_star_ptr] = constraint_stack_temp_star[idx];
    constraint_stack_star_ptr++;
  }
}

void clear_constraint_temp_stack(int start_idx, int stop_idx)
{
  //if(start_idx==stop_idx)
  //  return;
  for(int temp_idx=stop_idx;temp_idx<start_idx;temp_idx++)
  {
#ifdef DEBUG_MODE
    fprintf(cfg_out,"\n--free temp stack constraitn idx= %d---\n",temp_idx);
#endif
    free(constraint_stack_temp_star[temp_idx]); 
  }
  //==while(constraint_stack_temp_star_ptr!=0)
  //=={
  //==   constraint_stack_temp_star_ptr--;
  //==   free(constraint_stack_temp_star[constraint_stack_temp_star_ptr]);
  //==   constraint_stack_temp_star[constraint_stack_temp_star_ptr] = NULL;
  //==} 
}
bool detect_local_conflict(_constraint_s * p_top, int kind)
{
  //return true: there is conflict
  //return false: cannot make decision
  //kind 0: IF Condition
  //kind 1: CASE Condition
  _var_node * p_used_var_node;
  _var_node * p_used_var_node_temp;
  int found_value=0;
  int total_value=0;
  _constraint_s * p_constraint_pre;
  int frame_num_pre = (p_top->frame_num-1);
  int stack_idx;
  if(frame_num_pre<0) return false;
  //return false;
  if(kind==0)
  {
     p_used_var_node = p_top->p_cfg_node->used_var_chain;
     if(p_used_var_node->Is_input_port)
       return false;
     while(p_used_var_node!=NULL)
     {
       //find the definition of p_used_var_node in previous frame
       //_constraint_s * constraint_stack_star[MAXCNST];
       //int constraint_stack_star_ptr=0;  
       //NBLK_AS
       stack_idx=constraint_stack_star_ptr-1;
       while((stack_idx>=0)&&(constraint_stack_star[stack_idx]->frame_num>(frame_num_pre-1)))
       {
         p_constraint_pre = constraint_stack_star[stack_idx];
         if((p_constraint_pre->p_cfg_node_branch!=NULL)&&((p_constraint_pre->frame_num)==frame_num_pre)&&(p_constraint_pre->p_cfg_node_branch->node_type==NBLK_AS)&&(p_constraint_pre->p_cfg_node_branch->defined_var_chain->var_id==p_used_var_node->var_id))
         {
           if(p_constraint_pre->p_cfg_node_branch->used_var_chain!=NULL)
             return false;
           else 
             found_value++;
         }
         stack_idx--;
       }
       p_used_var_node =  p_used_var_node->next_node;
       total_value++;
     }
     if(total_value==found_value)
     {
       conflict_detect_number++; 
#ifdef DEBUG_MODE
       printf("\n--Successfully find a local conflict: IF--\n"); 
#endif
       return true;
     } 
     else
       return false;
  }
  else
  {
     p_used_var_node = p_top->p_cfg_node->used_var_chain;
     p_used_var_node_temp = p_top->p_cfg_node_branch->used_var_chain;
     if(p_used_var_node_temp!=NULL)
       return false;
     if(p_used_var_node->Is_input_port)
       return false;
     while(p_used_var_node!=NULL)
     {
       stack_idx=constraint_stack_star_ptr-1;
       while((stack_idx>=0)&&(constraint_stack_star[stack_idx]->frame_num>(frame_num_pre-1)))
       {
          p_constraint_pre = constraint_stack_star[stack_idx];
          if((p_constraint_pre->p_cfg_node_branch!=NULL)&&((p_constraint_pre->frame_num)==frame_num_pre)&&(p_constraint_pre->p_cfg_node_branch->node_type==NBLK_AS)&&(p_constraint_pre->p_cfg_node_branch->defined_var_chain->var_id==p_used_var_node->var_id)&&(p_constraint_pre->p_cfg_node==NULL))
          {
            if(p_constraint_pre->p_cfg_node_branch->used_var_chain!=NULL)
              return false;
            else
            {
              //fprintf(cfg_out,"\nthe stack idx is %d  found definition is %s,frame_num %d\n",stack_idx,constraint_stack_star[stack_idx]->p_cfg_node_branch->p_exptree_node->number_string,p_constraint_pre->frame_num);
              found_value++;
            }
          }
          stack_idx--;
       }
       p_used_var_node =  p_used_var_node->next_node;
       total_value++;
     }
     if(total_value==found_value)
     {
       conflict_detect_number++; 
#ifdef DEBUG_MODE
       printf("\n--Successfully find a local conflict: CASE--\n");
#endif 
       return true;
     } 
     else
     {
#ifdef DEBUG_MODE
       printf("\nthe return value is %d, the found value is %d\n",total_value,found_value);
#endif
       return false;
     }
  }    
}


bool subspace_prune(_constraint_s * p_cnst)
{
  _cfg_node * p_cfg_node_temp;
#ifdef UNPRUNE_MODE
  return false;
#endif
  if(p_cnst->Is_expr==false)
    return true;
  if((p_cnst->p_cfg_node->node_type) == IFELSE_EXP)
  {
    if((p_cnst->p_cfg_node->left_node->frame_num_exhaust <= p_cnst->frame_num)&&(p_cnst->p_cfg_node->left_node->brother_node->frame_num_exhaust <= p_cnst->frame_num))
      return true;
    else
      return false;
  } 
  else if((p_cnst->p_cfg_node->node_type) == CASECOND_EXP)    
  {
    p_cfg_node_temp =  p_cnst->p_cfg_node->left_node;
    while(p_cfg_node_temp!=NULL)
    {
      if(p_cfg_node_temp->next_node->frame_num_exhaust >p_cnst->frame_num)
        return false;
      p_cfg_node_temp = p_cfg_node_temp->brother_node;
    }
    return true;
  } 
}


 

//void push_constraint_stack(int stack_no, _constraint_s * p_constraint)
//{
//  if(stack_no==0)
//    constraint_stack_star[constraint_stack_star_ptr++] = p_constraint;
//  else if(stack_no==1)
//    constraint_stack_temp_star[constraint_stack_temp_star_ptr++] = p_constraint;
//    
//}
//
//_constraint_s * pop_constraint_stack(int stack_no)
//{
//  if(stack_no==0)
//  {
//    constraint_stack_star_ptr = constraint_stack_star_ptr - 1;
//    return constraint_stack_star[constraint_stack_star_ptr];
//  }
//  else if(stack_no==1)
//  {
//    constraint_stack_temp_star_ptr = constraint_stack_temp_star_ptr - 1;
//    return constraint_stack_temp_star[constraint_stack_temp_star_ptr];
//  }  
//}
