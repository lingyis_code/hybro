`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num);

	  reg  clock;
	  reg  reset;
	  reg  eoc;
	  reg  dsr;
	  reg [7 : 0] data_in;
	  wire soc ;
	  wire load_dato ;
	  wire add_mpx2 ;
	  wire mux_en ;
	  wire error ;
	  wire data_out ;
	  wire [3 : 0] canale;

reg [7:0] branvar_last[0:300];

integer i;
integer j;
integer k;
integer frame_num;
integer file_branch;
integer var_value;
integer ret_value;
b13 inst (
    .clock(clock),
    .reset(reset),
    .eoc(eoc),
    .dsr(dsr),
    .data_in(data_in[7:0]),
    .soc(soc),
    .load_dato(load_dato),
    .add_mpx2(add_mpx2),
    .mux_en(mux_en),
    .error(error),
    .data_out(data_out),
    .canale(canale[3:0])
    );

initial begin
#1
  start_build_cfg("source_rtl/b13_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=300;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=300;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num<0)
    reset = 1;
  else reset = 0;

  $display("\n---frame_num = %d",frame_num);

  var_value = get_var_assignment("eoc",frame_num+1);
  if(var_value == -1)
     eoc = $random;
  else 
   begin
     eoc = var_value;
     $display("\n---eoc = %d----",eoc);
   end
  
  var_value = get_var_assignment("dsr",frame_num+1);
  if(var_value == -1)
     dsr = $random;
  else 
   begin
     dsr = var_value;
     $display("\n---dsr = %d----",dsr);
   end

  var_value = get_var_assignment("data_in",frame_num+1);
  if(var_value == -1)
     data_in = $random;
  else 
   begin
     data_in = var_value;
     $display("\n---data_in = %d----",data_in);
   end
  i = i + 3;
  frame_num = frame_num + 1;
  #5
  if(frame_num==15)
   begin
     ret_value=generate_nxt_pattern(frame_num);
     if(ret_value==0)
       $finish;
     frame_num=-1;
   end
  //if(i>6000)
  // begin
  //  #4;
  //  //solve_constraint();
  //  $finish;
  // end
  #1;
end

initial begin
  for(i=0;i<=300;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

