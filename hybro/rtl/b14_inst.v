`timescale 1ns/10ps
module b14 (clock , reset , datai , rd , wr , addr , datao );

	  input  clock;
	  input  reset;
	  input [31 : 0] datai;
	  output rd ;
	  output wr ;
	  output [19 : 0] addr;
	  output [31 : 0] datao;
	  wire clock;
	  wire reset;
	  wire [31 : 0] datai;
	  reg rd;
	  reg [7:0] branvar[0:300];
	  reg wr;
	  reg [19 : 0] addr;
	  reg [31 : 0] datao;
	  reg [31 : 0] reg0;
	  reg [31 : 0] reg1;
	  reg [31 : 0] reg2;
	  reg [31 : 0] reg3;
	  reg B;
	  reg [19 : 0] MAR;
	  reg [31 : 0] MBR;
	  reg [1 : 0] mf;
	  reg [2 : 0] df;
	  reg cf;
	  reg [3 : 0] ff;
	  reg [19 : 0] tail;
	  reg [31 : 0] IR;
	  reg state;
	  reg [31 : 0] r;
          reg [31 : 0] m;
          reg [31 : 0] t;
	  reg [31 : 0] d;
	  reg [31 : 0] temp;
	  reg [1 : 0] s;
	  reg [32 : 0] a;
	 always @ (posedge  clock) 
	 begin
	   if( reset) 
	     begin 
	       branvar[0] <= branvar[0] + 1; 
	       begin
	         MAR <=0;
	         MBR <=0;
	         IR <=0;
	         d <=0;
	         r <=0;
	         m <=0;
	         s <=0;
	         temp <=0;
	         mf <=0;
	         df <=0;
	         ff <=0;
	         cf <=0;
	         tail <=0;
	         B <=0;
	         reg0 <=0;
	         reg1 <=0;
	         reg2 <=0;
	         reg3 <=0;
	         addr <= 0;
	         rd <= 0;
	         wr <= 0;
	         datao <= 0;
	         state <=0;
	       end
	     end
	   else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	     begin
	       rd <= 0;
	       wr <= 0;
	       case( state)
	       0: 
	         begin 
	           branvar[2] <= branvar[2] + 1;
	           begin
	             MAR <=(  reg3[19:0] & 20'hfffff);
	             addr <=  MAR;
	             rd <= 1;
	             MBR <= datai;
	             IR <= MBR;
	             state <=1;
	           end
	         end
	       1: 
	         begin 
	           branvar[3] <= branvar[3] + 1;
	           begin
	             if( IR[31]) 
	               begin 
	                 branvar[4] <= branvar[4] + 1; 
	                 IR <=(32'h0 - IR);
	               end
	             else 
	               begin  
	                 branvar[5] <= branvar[5] + 1;   
	               end 
	             mf <=( IR[28:27]  & 3);
	             df <=( IR[26:24]  & 7);
	             ff <=( IR[22:19]  & 15);
	             cf <= IR[23] ;
	             tail <=(  IR[19:0] & 20'hfffff);
	             reg3 <=( (  reg3 & 32'h1fffffff) + 8);
	             s <=IR[30:29] & 3;
	             case( s)
	               0: 
	                 begin 
	                   branvar[6] <= branvar[6] + 1;
	                   r <= reg0;
	                 end
	               1: 
	                 begin 
	                   branvar[7] <= branvar[7] + 1;
	                   r <= reg1;
	                 end
	               2: 
	                 begin 
	                   branvar[8] <= branvar[8] + 1;
	                   r <= reg2;
	                 end
	               3: 
	                 begin 
	                   branvar[9] <= branvar[9] + 1;
	                   r <= reg3;
	                 end
	             endcase
	             case( cf)
	                1: 
	                  begin 
	                    branvar[10] <= branvar[10] + 1;
	                    begin
	                      case( mf)
	                        0: 
	                         begin 
	                           branvar[11] <= branvar[11] + 1;
	                           m <= {12'h0,tail[19:0]};
	                         end
	                        1: 
	                         begin 
	                           branvar[12] <= branvar[12] + 1;
	                             begin
	                               m <= datai;
	                               addr <=  tail;
	                               rd <= 1;
	                             end
	                         end
	                        2: 
	                         begin 
	                           branvar[13] <= branvar[13] + 1;
	                             begin
	                               addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	                               rd <= 1;
	                               m <= datai;
	                             end
	                         end
	                        3: 
	                         begin 
	                           branvar[14] <= branvar[14] + 1;
	                             begin
	                               addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	                               rd <= 1;
	                               m <= datai;
	                             end
	                         end
	                      endcase
	                      case( ff)
	                        0: 
	                            begin 
	                           branvar[15] <= branvar[15] + 1;
	                         begin
	                          a <=( { r[31], r} - { m[31], m});
	                         if( a[32]) 
	                          begin 
	                         branvar[16] <= branvar[16] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[17] <= branvar[17] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          1: 
	                            begin 
	                           branvar[18] <= branvar[18] + 1;
	                         begin
	                          a <=( { r[31], r} - { m[31], m});
	                         if((!  a[32])) 
	                          begin 
	                         branvar[19] <= branvar[19] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[20] <= branvar[20] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          2: 
	                            begin 
	                           branvar[21] <= branvar[21] + 1;
	                         if((  r ==  m)) 
	                          begin 
	                         branvar[22] <= branvar[22] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[23] <= branvar[23] + 1;   
	                          B <=0;
	                          end
	                          end
	                          3: 
	                            begin 
	                           branvar[24] <= branvar[24] + 1;
	                         if((  r !=  m)) 
	                          begin 
	                         branvar[25] <= branvar[25] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[26] <= branvar[26] + 1;   
	                          B <=0;
	                          end
	                          end
	                          4: 
	                            begin 
	                           branvar[27] <= branvar[27] + 1;
	                         begin
	                          a <=( { m[31], m} - { r[31], r});
	                         if((!  a[32])) 
	                          begin 
	                         branvar[28] <= branvar[28] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[29] <= branvar[29] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          5: 
	                            begin 
	                           branvar[30] <= branvar[30] + 1;
	                         begin
	                          a <=( { m[31], m} - { r[31], r});
	                         if( a[32]) 
	                          begin 
	                         branvar[31] <= branvar[31] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[32] <= branvar[32] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          6: 
	                            begin 
	                           branvar[33] <= branvar[33] + 1;
	                         begin
	                         if(((!  r[31]) && (  r[31:0] > 32'h3fffffff))) 
	                          begin 
	                         branvar[34] <= branvar[34] + 1; 
	                          r <=(  r - 32'h40000000);
	                          end
	                         else 
	                         begin  
	                           branvar[35] <= branvar[35] + 1;   
	                           end 
	                          a <=( { r[31], r[31:0]} - { m[31], m[31:0]});
	                         if( a[32]) 
	                          begin 
	                         branvar[36] <= branvar[36] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[37] <= branvar[37] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          7: 
	                            begin 
	                           branvar[38] <= branvar[38] + 1;
	                         begin
	                         if(((!  r[31]) && (  r[31:0] > 32'h3fffffff))) 
	                          begin 
	                         branvar[39] <= branvar[39] + 1; 
	                          r <=(  r - 32'h40000000);
	                          end
	                         else 
	                         begin  
	                           branvar[40] <= branvar[40] + 1;   
	                           end 
	                          a <=( { r[31], r[31:0]} - { m[31], m[31:0]});
	                         if((!  a[32])) 
	                          begin 
	                         branvar[41] <= branvar[41] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[42] <= branvar[42] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          8: 
	                            begin 
	                           branvar[43] <= branvar[43] + 1;
	                         begin
	                          a <=( { r[31], r[31:0]} - { m[31], m[31:0]});
	                         if((  a[32] ||  B)) 
	                          begin 
	                         branvar[44] <= branvar[44] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[45] <= branvar[45] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          9: 
	                            begin 
	                           branvar[46] <= branvar[46] + 1;
	                         begin
	                          a <=( { r[31], r[31:0]} - { m[31], m[31:0]});
	                         if(( (!  a[32]) ||  B)) 
	                          begin 
	                         branvar[47] <= branvar[47] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[48] <= branvar[48] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          10: 
	                            begin 
	                           branvar[49] <= branvar[49] + 1;
	                         if(( (  r ==  m) ||  B)) 
	                          begin 
	                         branvar[50] <= branvar[50] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[51] <= branvar[51] + 1;   
	                          B <=0;
	                          end
	                          end
	                          11: 
	                            begin 
	                           branvar[52] <= branvar[52] + 1;
	                         if(( (  r !=  m) ||  B)) 
	                          begin 
	                         branvar[53] <= branvar[53] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[54] <= branvar[54] + 1;   
	                          B <=0;
	                          end
	                          end
	                          12: 
	                            begin 
	                           branvar[55] <= branvar[55] + 1;
	                         begin
	                          a <=( { m[31], m[31:0]} - { r[31], r[31:0]});
	                         if(( (!  a[32]) ||  B)) 
	                          begin 
	                         branvar[56] <= branvar[56] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[57] <= branvar[57] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          13: 
	                            begin 
	                           branvar[58] <= branvar[58] + 1;
	                         begin
	                          a <=( { m[31], m[31:0]} - { r[31], r[31:0]});
	                         if((  a[32] ||  B)) 
	                          begin 
	                         branvar[59] <= branvar[59] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[60] <= branvar[60] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          14: 
	                            begin 
	                           branvar[61] <= branvar[61] + 1;
	                         begin
	                         if(((!  r[31]) && (  r[31:0] > 32'h3fffffff))) 
	                          begin 
	                         branvar[62] <= branvar[62] + 1; 
	                          r <=(  r - 32'h40000000);
	                          end
	                         else 
	                         begin  
	                           branvar[63] <= branvar[63] + 1;   
	                           end 
	                          a <=( { r[31], r} - { m[31], m});
	                         if((  a[32] ||  B)) 
	                          begin 
	                         branvar[64] <= branvar[64] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[65] <= branvar[65] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                          15: 
	                            begin 
	                           branvar[66] <= branvar[66] + 1;
	                         begin
	                         if(((!  r[31]) && (  r[31:0] > 32'h3fffffff))) 
	                          begin 
	                         branvar[67] <= branvar[67] + 1; 
	                          r <=(  r - 32'h40000000);
	                          end
	                         else 
	                         begin  
	                           branvar[68] <= branvar[68] + 1;   
	                           end 
	                          a <=( { r[31], r} - { m[31], m});
	                         if(( (!  a[32]) ||  B)) 
	                          begin 
	                         branvar[69] <= branvar[69] + 1; 
	                          B <=1;
	                          end
	                         else 
	                           begin 
	                           branvar[70] <= branvar[70] + 1;   
	                          B <=0;
	                          end
	                         end
	                          end
	                      endcase
	                  end
	            end
	         0: 
	          begin 
	            branvar[71] <= branvar[71] + 1;
	          begin
	          if((  df != 7)) 
	           begin 
	          branvar[72] <= branvar[72] + 1; 
	          begin
	          if((  df == 5)) 
	           begin 
	          branvar[73] <= branvar[73] + 1; 
	          begin
	          if((!  B)) 
	           begin 
	          branvar[74] <= branvar[74] + 1; 
	           d <=3;
	           end
	          else 
	          begin  
	            branvar[75] <= branvar[75] + 1;   
	            end 
	          end
	           end
	          else 
	            begin 
	            branvar[76] <= branvar[76] + 1;   
	              if((  df == 4)) 
	                begin 
	                  branvar[77] <= branvar[77] + 1; 
	                  begin
	                    if( B) 
	                     begin 
	                       branvar[78] <= branvar[78] + 1; 
	                       d <=3;
	                     end
	                    else 
	                     begin  
	                       branvar[79] <= branvar[79] + 1;   
	                     end 
	                  end
	                end
	              else 
	                begin 
	                  branvar[80] <= branvar[80] + 1;   
	                  if((  df == 3)) 
	                    begin 
	                      branvar[81] <= branvar[81] + 1; 
	                      d <=3;
	                    end
	                  else 
	                    begin 
	                      branvar[82] <= branvar[82] + 1;   
	                      if(df == 2) 
	                        begin 
	                          branvar[83] <= branvar[83] + 1; 
	                          d <= 2;
	                        end
	                      else 
	                        begin 
	                          branvar[84] <= branvar[84] + 1;   
	                          if((df == 1)) 
	                            begin 
	                              branvar[85] <= branvar[85] + 1; 
	                              d <=1;
	                            end
	                          else 
	                            begin 
	                              branvar[86] <= branvar[86] + 1;   
	                              if((df == 0)) 
	                                begin 
	                                  branvar[87] <= branvar[87] + 1; 
	                                  d <=0;
	                                end
	                              else 
	                                begin  
	                                  branvar[88] <= branvar[88] + 1;   
	                                end 
	                            end
	                        end
	                    end
	                end
	           end
	 case( ff)
	  0: 
	    begin 
	   branvar[89] <= branvar[89] + 1;
	        begin
	                       case( mf)
	                        0: 
	                          begin 
	                         branvar[90] <= branvar[90] + 1;
	                        m <=  {12'h0,tail[19:0]};
	                        end
	                        1: 
	                          begin 
	                         branvar[91] <= branvar[91] + 1;
	                       begin
	                        m <= datai;
	                        addr <=  tail;
	                        rd <= 1;
	                       end
	                        end
	                        2: 
	                          begin 
	                         branvar[92] <= branvar[92] + 1;
	                       begin
	                        addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	                        rd <= 1;
	                        m <= datai;
	                       end
	                        end
	                        3: 
	                          begin 
	                         branvar[93] <= branvar[93] + 1;
	                       begin
	                        addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	                        rd <= 1;
	                        m <= datai;
	                       end
	                        end
	                       endcase
	                       t <=0;
	                       case( d)
	                        0: 
	                          begin 
	                         branvar[94] <= branvar[94] + 1;
	                        reg0 <=(  t -  m);
	                        end
	                        1: 
	                          begin 
	                         branvar[95] <= branvar[95] + 1;
	                        reg1 <=(  t -  m);
	                        end
	                        2: 
	                          begin 
	                         branvar[96] <= branvar[96] + 1;
	                        reg2 <=(  t -  m);
	                        end
	                        3: 
	                          begin 
	                         branvar[97] <= branvar[97] + 1;
	                        reg3 <=(  t -  m);
	                        end
	                       endcase
	      end
	  end
	  1: 
	    begin 
	    branvar[98] <= branvar[98] + 1;
	    begin
	              case( mf)
	               0: 
	                 begin 
	                branvar[99] <= branvar[99] + 1;
	               m <=  {12'h0,tail[19:0]};
	               end
	               1: 
	                 begin 
	                branvar[100] <= branvar[100] + 1;
	              begin
	               m <= datai;
	               addr <=  tail;
	               rd <= 1;
	              end
	               end
	               2: 
	                 begin 
	                branvar[101] <= branvar[101] + 1;
	              begin
	               addr <= ( (  tail +  reg1) & 20'hfffff);
	               rd <= 1;
	               m <= datai;
	              end
	               end
	               3: 
	                 begin 
	                branvar[102] <= branvar[102] + 1;
	              begin
	               addr <= ( (  tail +  reg2) & 20'hfffff);
	               rd <= 1;
	               m <= datai;
	              end
	               end
	              endcase
	               reg2 <= reg3;
	               reg3 <= m;
	    end
	    end
	  2: 
	    begin 
	   branvar[103] <= branvar[103] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[104] <= branvar[104] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[105] <= branvar[105] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[106] <= branvar[106] + 1;
	 begin
	  addr <= ( (  tail +  reg1) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[107] <= branvar[107] + 1;
	 begin
	  addr <= ( (  tail +  reg2) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[108] <= branvar[108] + 1;
	  reg0 <= m;
	  end
	  1: 
	    begin 
	   branvar[109] <= branvar[109] + 1;
	  reg1 <= m;
	  end
	  2: 
	    begin 
	   branvar[110] <= branvar[110] + 1;
	  reg2 <= m;
	  end
	  3: 
	    begin 
	   branvar[111] <= branvar[111] + 1;
	  reg3 <= m;
	  end
	 endcase
	 end
	  end
	  3: 
	    begin 
	   branvar[112] <= branvar[112] + 1;
	 begin
	                case( mf)
	                 0: 
	                   begin 
	                  branvar[113] <= branvar[113] + 1;
	                 //m <= tail;
	                 m <=  {12'h0,tail[19:0]};
	                 end
	                 1: 
	                   begin 
	                  branvar[114] <= branvar[114] + 1;
	                begin
	                 m <= datai;
	                 addr <=  tail;
	                 rd <= 1;
	                end
	                 end
	                 2: 
	                   begin 
	                  branvar[115] <= branvar[115] + 1;
	                begin
	                 addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	                 rd <= 1;
	                 m <= datai;
	                end
	                 end
	                 3: 
	                   begin 
	                  branvar[116] <= branvar[116] + 1;
	                begin
	                 addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	                 rd <= 1;
	                 m <= datai;
	                end
	                 end
	                endcase
	                case( d)
	                 0: 
	                   begin 
	                  branvar[117] <= branvar[117] + 1;
	                 reg0 <= m;
	                 end
	                 1: 
	                   begin 
	                  branvar[118] <= branvar[118] + 1;
	                 reg1 <= m;
	                 end
	                 2: 
	                   begin 
	                  branvar[119] <= branvar[119] + 1;
	                 reg2 <= m;
	                 end
	                 3: 
	                   begin 
	                  branvar[120] <= branvar[120] + 1;
	                 reg3 <= m;
	                 end
	                endcase
	 end
	  end
	  4: 
	    begin 
	   branvar[121] <= branvar[121] + 1;
	 begin
	                case( mf)
	                 0: 
	                   begin 
	                  branvar[122] <= branvar[122] + 1;
	                 //m <= tail;
	                 m <=  {12'h0,tail[19:0]};
	                 end
	                 1: 
	                   begin 
	                  branvar[123] <= branvar[123] + 1;
	                begin
	                 m <= datai;
	                 addr <=  tail;
	                 rd <= 1;
	                end
	                 end
	                 2: 
	                   begin 
	                  branvar[124] <= branvar[124] + 1;
	                begin
	                 addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	                 rd <= 1;
	                 m <= datai;
	                end
	                 end
	                 3: 
	                   begin 
	                  branvar[125] <= branvar[125] + 1;
	                begin
	                 addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	                 rd <= 1;
	                 m <= datai;
	                end
	                 end
	                endcase
	                case( d)
	                 0: 
	                   begin 
	                  branvar[126] <= branvar[126] + 1;
	                 reg0 <=( (  r +  m) & 32'h3fffffff);
	                 end
	                 1: 
	                   begin 
	                  branvar[127] <= branvar[127] + 1;
	                 reg1 <=( (  r +  m) & 32'h3fffffff);
	                 end
	                 2: 
	                   begin 
	                  branvar[128] <= branvar[128] + 1;
	                 reg2 <=( (  r +  m) & 32'h3fffffff);
	                 end
	                 3: 
	                   begin 
	                  branvar[129] <= branvar[129] + 1;
	                 reg3 <=( (  r +  m) & 32'h3fffffff);
	                 end
	                endcase
	 end
	  end
	  5: 
	    begin 
	   branvar[130] <= branvar[130] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[131] <= branvar[131] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[132] <= branvar[132] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[133] <= branvar[133] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[134] <= branvar[134] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[135] <= branvar[135] + 1;
	  reg0 <=( (  r +  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[136] <= branvar[136] + 1;
	  reg1 <=( (  r +  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[137] <= branvar[137] + 1;
	  reg2 <=( (  r +  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[138] <= branvar[138] + 1;
	  reg3 <=( (  r +  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  6: 
	    begin 
	   branvar[139] <= branvar[139] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[140] <= branvar[140] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[141] <= branvar[141] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[142] <= branvar[142] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[143] <= branvar[143] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[144] <= branvar[144] + 1;
	  reg0 <=( (  r -  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[145] <= branvar[145] + 1;
	  reg1 <=( (  r -  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[146] <= branvar[146] + 1;
	  reg2 <=( (  r -  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[147] <= branvar[147] + 1;
	  reg3 <=( (  r -  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  7: 
	    begin 
	   branvar[148] <= branvar[148] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[149] <= branvar[149] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[150] <= branvar[150] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[151] <= branvar[151] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[152] <= branvar[152] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[153] <= branvar[153] + 1;
	  reg0 <=( (  r -  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[154] <= branvar[154] + 1;
	  reg1 <=( (  r -  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[155] <= branvar[155] + 1;
	  reg2 <=( (  r -  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[156] <= branvar[156] + 1;
	  reg3 <=( (  r -  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  8: 
	    begin 
	   branvar[157] <= branvar[157] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[158] <= branvar[158] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[159] <= branvar[159] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[160] <= branvar[160] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[161] <= branvar[161] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[162] <= branvar[162] + 1;
	  reg0 <=( (  r +  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[163] <= branvar[163] + 1;
	  reg1 <=( (  r +  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[164] <= branvar[164] + 1;
	  reg2 <=( (  r +  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[165] <= branvar[165] + 1;
	  reg3 <=( (  r +  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  9: 
	    begin 
	   branvar[166] <= branvar[166] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[167] <= branvar[167] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[168] <= branvar[168] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[169] <= branvar[169] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[170] <= branvar[170] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[171] <= branvar[171] + 1;
	  reg0 <=( (  r -  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[172] <= branvar[172] + 1;
	  reg1 <=( (  r -  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[173] <= branvar[173] + 1;
	  reg2 <=( (  r -  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[174] <= branvar[174] + 1;
	  reg3 <=( (  r -  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  10: 
	    begin 
	   branvar[175] <= branvar[175] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[176] <= branvar[176] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[177] <= branvar[177] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[178] <= branvar[178] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[179] <= branvar[179] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[180] <= branvar[180] + 1;
	  reg0 <=( (  r +  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[181] <= branvar[181] + 1;
	  reg1 <=( (  r +  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[182] <= branvar[182] + 1;
	  reg2 <=( (  r +  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[183] <= branvar[183] + 1;
	  reg3 <=( (  r +  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  11: 
	    begin 
	   branvar[184] <= branvar[184] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[185] <= branvar[185] + 1;
	   //m <= tail;
	   m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[186] <= branvar[186] + 1;
	 begin
	  m <= datai;
	  addr <=  tail;
	  rd <= 1;
	 end
	  end
	  2: 
	    begin 
	   branvar[187] <= branvar[187] + 1;
	 begin
	  addr <= ( (  tail +  reg1[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	  3: 
	    begin 
	   branvar[188] <= branvar[188] + 1;
	 begin
	  addr <= ( (  tail +  reg2[19:0]) & 20'hfffff);
	  rd <= 1;
	  m <= datai;
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[189] <= branvar[189] + 1;
	  reg0 <=( (  r -  m) & 32'h3fffffff);
	  end
	  1: 
	    begin 
	   branvar[190] <= branvar[190] + 1;
	  reg1 <=( (  r -  m) & 32'h3fffffff);
	  end
	  2: 
	    begin 
	   branvar[191] <= branvar[191] + 1;
	  reg2 <=( (  r -  m) & 32'h3fffffff);
	  end
	  3: 
	    begin 
	   branvar[192] <= branvar[192] + 1;
	  reg3 <=( (  r -  m) & 32'h3fffffff);
	  end
	 endcase
	 end
	  end
	  12: 
	    begin 
	   branvar[193] <= branvar[193] + 1;
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[194] <= branvar[194] + 1;
	 if( r[31]) 
	  begin 
	 branvar[195] <= branvar[195] + 1; 
	  t <=(32'h0 - ((32'h0 - r) >> 1));
	  end
	 else 
	   begin 
	   branvar[196] <= branvar[196] + 1;   
	  t <=( r >> 1);
	  end
	  end
	  1: 
	    begin 
	   branvar[197] <= branvar[197] + 1;
	 begin
	 if( r[31]) 
	  begin 
	 branvar[198] <= branvar[198] + 1; 
	  t <=(32'h0 - ((32'h0 - r) >> 1));
	  end
	 else 
	   begin 
	   branvar[199] <= branvar[199] + 1;   
	  t <=( r >> 1);
	  end
	 if( B) 
	  begin 
	 branvar[200] <= branvar[200] + 1; 
	  t <=(  t & 32'h1fffffff);
	  end
	 else 
	 begin  
	   branvar[201] <= branvar[201] + 1;   
	   end 
	 end
	  end
	  2: 
	    begin 
	   branvar[202] <= branvar[202] + 1;
	  t <=((  r & 32'h1fffffff) << 1);
	  end
	  3: 
	    begin 
	   branvar[203] <= branvar[203] + 1;
	 begin
	  t <=((  r & 32'h1fffffff) << 1);
	 if(((!  t[31]) && (  t[31:0] > 32'h3fffffff))) 
	  begin 
	 branvar[204] <= branvar[204] + 1; 
	  B <=1;
	  end
	 else 
	   begin 
	   branvar[205] <= branvar[205] + 1;   
	  B <=0;
	  end
	 end
	  end
	 endcase
	 case( d)
	  0: 
	    begin 
	   branvar[206] <= branvar[206] + 1;
	  reg0 <= t;
	  end
	  1: 
	    begin 
	   branvar[207] <= branvar[207] + 1;
	  reg1 <= t;
	  end
	  2: 
	    begin 
	   branvar[208] <= branvar[208] + 1;
	  reg2 <= t;
	  end
	  3: 
	    begin 
	   branvar[209] <= branvar[209] + 1;
	  reg3 <= t;
	  end
	 endcase
	 end
	  end
	 endcase
	 end
	  end
	 else 
	   begin 
	   branvar[210] <= branvar[210] + 1;   
	 begin
	 if((  df == 7)) 
	  begin 
	 branvar[211] <= branvar[211] + 1; 
	 begin
	 case( mf)
	  0: 
	    begin 
	   branvar[212] <= branvar[212] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  1: 
	    begin 
	   branvar[213] <= branvar[213] + 1;
	  //m <= tail;
	  m <=  {12'h0,tail[19:0]};
	  end
	  2: 
	    begin 
	   branvar[214] <= branvar[214] + 1;
	  m <={12'h0,( (  reg1[19:0] & 20'hfffff) + (  tail[19:0] & 20'hfffff))};
	  end
	  3: 
	    begin 
	   branvar[215] <= branvar[215] + 1;
	  m <={12'h0, ( (  reg2 [19:0]& 20'hfffff) + (  tail[19:0] & 20'hfffff))};
	  end
	 endcase
	  addr <= (  m[19:0] & 20'hfffff);
	  wr <= 1;
	  datao <=  r;
	 end
	  end
	 else 
	 begin  
	   branvar[216] <= branvar[216] + 1;   
	   end 
	 end
	  end
	 end
	  end
	 endcase
	  state <=0;
	 end
	  end
	 endcase
	 end
	  end
	 end
	  endmodule 
