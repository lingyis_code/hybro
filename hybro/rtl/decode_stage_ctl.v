////////////////////////////////////////////////////////////////////////////////
// Decode Pipeline Stage
// Converted from revision 3283
////////////////////////////////////////////////////////////////////////////////
// Decode pipeline stage container
////////////////////////////////////////////////////////////////////////////////

module decode_stage(
	clk,
	reset,
	stall_in_0,
	stall_in_1,
	stall_in_2,
	branch_mispredict,
	fetch2decodeinstr0_out,
	fetch2decodeinstr1_out,
  decode_packet0sreg_t,
  decode_packet0sreg_s,
  decode_packet0dreg,
  decode_packet0acc,
  decode_packet0imm16_value,
  decode_packet0imm26_value,
  decode_packet0imm5_value,
  decode_packet0op_FU_type,
  decode_packet0alu_op,
  decode_packet0fpu_op,
  decode_packet0op_MEM_type,
  decode_packet0op_BR_type,
  decode_packet0shift_type,
  decode_packet0compare_type,
  decode_packet0is_imm26,
  decode_packet0is_imm,
  decode_packet0is_imm5,
  decode_packet0is_signed,
  decode_packet0carry_in,
  decode_packet0carry_out,
  decode_packet0has_sreg_t,
  decode_packet0has_sreg_s,
  decode_packet0has_dreg,
  decode_packet0is_prefetch,
  decode_packet0sprf_dest,
  decode_packet0sprf_src,
  decode_packet0is_atomic,
  decode_packet0is_ldl,
  decode_packet0is_stl,
  decode_packet0is_stc,
  decode_packet0is_valid,
  decode_packet0is_halt,
  decode_packet0is_compare,
  decode_packet0dreg_is_src,
  decode_packet0dreg_is_dest,
  decode_packet1sreg_t,
  decode_packet1sreg_s,
  decode_packet1dreg,
  decode_packet1acc,
  decode_packet1imm16_value,
  decode_packet1imm26_value,
  decode_packet1imm5_value,
  decode_packet1op_FU_type,
  decode_packet1alu_op,
  decode_packet1fpu_op,
  decode_packet1op_MEM_type,
  decode_packet1op_BR_type,
  decode_packet1shift_type,
  decode_packet1compare_type,
  decode_packet1is_imm26,
  decode_packet1is_imm,
  decode_packet1is_imm5,
  decode_packet1is_signed,
  decode_packet1carry_in,
  decode_packet1carry_out,
  decode_packet1has_sreg_t,
  decode_packet1has_sreg_s,
  decode_packet1has_dreg,
  decode_packet1is_prefetch,
  decode_packet1sprf_dest,
  decode_packet1sprf_src,
  decode_packet1is_atomic,
  decode_packet1is_ldl,
  decode_packet1is_stl,
  decode_packet1is_stc,
  decode_packet1is_valid,
  decode_packet1is_halt,
  decode_packet1is_compare,
  decode_packet1dreg_is_src,
  decode_packet1dreg_is_dest,
	fetch2decodepc1,
	fetch2decodepc0,
	fetch2decodepc1_incr,
	fetch2decodepc0_incr,
	fetch2decodeinstr0_valid,
	fetch2decodeinstr1_valid,
	fetch2decodebranch_taken,
	fetch2decodepredicted_pc,
	fetch2decodevalid,
	data_in_bpaddress_0,
	data_in_bpdata_0,
	data_in_bpvalid_0,
	data_in_bpaddress_1,
	data_in_bpdata_1,
	data_in_bpvalid_1,
	data_in_bpaddress_2,
	data_in_bpdata_2,
	data_in_bpvalid_2,
	data_in_bpaddress_3,
	data_in_bpdata_3,
	data_in_bpvalid_3,
	data_in_bpaddress_4,
	data_in_bpdata_4,
	data_in_bpvalid_4,
	data_in_bpaddress_5,
	data_in_bpdata_5,
	data_in_bpvalid_5,
	data_in_bpaddress_6,
	data_in_bpdata_6,
	data_in_bpvalid_6,
	data_in_bpaddress_7,
	data_in_bpdata_7,
	data_in_bpvalid_7,
	data_in_bpaddress_8,
	data_in_bpdata_8,
	data_in_bpvalid_8,
	data_in_bpaddress_9,
	data_in_bpdata_9,
	data_in_bpvalid_9,
	data_in_bpaddress_10,
	data_in_bpdata_10,
	data_in_bpvalid_10,
	write_enable_0,
	write_addr_0,
	write_data_0,
	write_enable_1,
	write_addr_1,
	write_data_1,
	decode2mempc,
	decode2mempc_incr,
	decode2memthread_num,
	decode2membranch_taken,
	decode2mempredicted_pc,
	decode2memvalid,
	decode2memis_oldest,
	decode2memsreg_t_data,
	decode2memsreg_s_data,
	decode2memresult,
	decode2memL1D_hit,
	decode2memdecode_packetsreg_t,
	decode2memdecode_packetsreg_s,
	decode2memdecode_packetdreg,
	decode2memdecode_packetacc,
	decode2memdecode_packetimm16_value,
	decode2memdecode_packetimm26_value,
	decode2memdecode_packetimm5_value,
	decode2memdecode_packetop_FU_type,
	decode2memdecode_packetalu_op,
	decode2memdecode_packetfpu_op,
	decode2memdecode_packetop_MEM_type,
	decode2memdecode_packetop_BR_type,
	decode2memdecode_packetshift_type,
	decode2memdecode_packetcompare_type,
	decode2memdecode_packetis_imm26,
	decode2memdecode_packetis_imm,
	decode2memdecode_packetis_imm5,
	decode2memdecode_packetis_signed,
	decode2memdecode_packetcarry_in,
	decode2memdecode_packetcarry_out,
	decode2memdecode_packethas_sreg_t,
	decode2memdecode_packethas_sreg_s,
	decode2memdecode_packethas_dreg,
	decode2memdecode_packetis_prefetch,
	decode2memdecode_packetsprf_dest,
	decode2memdecode_packetsprf_src,
	decode2memdecode_packetis_atomic,
	decode2memdecode_packetis_ldl,
	decode2memdecode_packetis_stl,
	decode2memdecode_packetis_stc,
	decode2memdecode_packetis_valid,
	decode2memdecode_packetis_halt,
	decode2memdecode_packetis_compare,
	decode2memdecode_packetdreg_is_src,
	decode2memdecode_packetdreg_is_dest,
	decode2fpupc,
	decode2fpupc_incr,
	decode2fputhread_num,
	decode2fpubranch_taken,
	decode2fpupredicted_pc,
	decode2fpuvalid,
	decode2fpuis_oldest,
	decode2fpusreg_t_data,
	decode2fpusreg_s_data,
	decode2fpuresult,
	decode2fpuL1D_hit,
	decode2fpudecode_packetsreg_t,
	decode2fpudecode_packetsreg_s,
	decode2fpudecode_packetdreg,
	decode2fpudecode_packetacc,
	decode2fpudecode_packetimm16_value,
	decode2fpudecode_packetimm26_value,
	decode2fpudecode_packetimm5_value,
	decode2fpudecode_packetop_FU_type,
	decode2fpudecode_packetalu_op,
	decode2fpudecode_packetfpu_op,
	decode2fpudecode_packetop_MEM_type,
	decode2fpudecode_packetop_BR_type,
	decode2fpudecode_packetshift_type,
	decode2fpudecode_packetcompare_type,
	decode2fpudecode_packetis_imm26,
	decode2fpudecode_packetis_imm,
	decode2fpudecode_packetis_imm5,
	decode2fpudecode_packetis_signed,
	decode2fpudecode_packetcarry_in,
	decode2fpudecode_packetcarry_out,
	decode2fpudecode_packethas_sreg_t,
	decode2fpudecode_packethas_sreg_s,
	decode2fpudecode_packethas_dreg,
	decode2fpudecode_packetis_prefetch,
	decode2fpudecode_packetsprf_dest,
	decode2fpudecode_packetsprf_src,
	decode2fpudecode_packetis_atomic,
	decode2fpudecode_packetis_ldl,
	decode2fpudecode_packetis_stl,
	decode2fpudecode_packetis_stc,
	decode2fpudecode_packetis_valid,
	decode2fpudecode_packetis_halt,
	decode2fpudecode_packetis_compare,
	decode2fpudecode_packetdreg_is_src,
	decode2fpudecode_packetdreg_is_dest,
	decode2intpc,
	decode2intpc_incr,
	decode2intthread_num,
	decode2intbranch_taken,
	decode2intpredicted_pc,
	decode2intvalid,
	decode2intis_oldest,
	decode2intsreg_t_data,
	decode2intsreg_s_data,
	decode2intresult,
	decode2intL1D_hit,
	decode2intdecode_packetsreg_t,
	decode2intdecode_packetsreg_s,
	decode2intdecode_packetdreg,
	decode2intdecode_packetacc,
	decode2intdecode_packetimm16_value,
	decode2intdecode_packetimm26_value,
	decode2intdecode_packetimm5_value,
	decode2intdecode_packetop_FU_type,
	decode2intdecode_packetalu_op,
	decode2intdecode_packetfpu_op,
	decode2intdecode_packetop_MEM_type,
	decode2intdecode_packetop_BR_type,
	decode2intdecode_packetshift_type,
	decode2intdecode_packetcompare_type,
	decode2intdecode_packetis_imm26,
	decode2intdecode_packetis_imm,
	decode2intdecode_packetis_imm5,
	decode2intdecode_packetis_signed,
	decode2intdecode_packetcarry_in,
	decode2intdecode_packetcarry_out,
	decode2intdecode_packethas_sreg_t,
	decode2intdecode_packethas_sreg_s,
	decode2intdecode_packethas_dreg,
	decode2intdecode_packetis_prefetch,
	decode2intdecode_packetsprf_dest,
	decode2intdecode_packetsprf_src,
	decode2intdecode_packetis_atomic,
	decode2intdecode_packetis_ldl,
	decode2intdecode_packetis_stl,
	decode2intdecode_packetis_stc,
	decode2intdecode_packetis_valid,
	decode2intdecode_packetis_halt,
	decode2intdecode_packetis_compare,
	decode2intdecode_packetdreg_is_src,
	decode2intdecode_packetdreg_is_dest,
	decode_stall
);

	input  clk;
	input  reset;
	input  stall_in_0;
	input  stall_in_1;
	input  stall_in_2;
	input  branch_mispredict;
	input [31:0] fetch2decodeinstr0_out;
	input [31:0] fetch2decodeinstr1_out;
  input [4:0] decode_packet0sreg_t;
  input [4:0] decode_packet0sreg_s;
  input [4:0] decode_packet0dreg;
  input [1:0] decode_packet0acc;
  input [15:0] decode_packet0imm16_value;
  input [25:0] decode_packet0imm26_value;
  input [4:0] decode_packet0imm5_value;
  input [3:0] decode_packet0op_FU_type;
  input [4:0] decode_packet0alu_op;
  input [4:0] decode_packet0fpu_op;
  input [3:0] decode_packet0op_MEM_type;
  input [1:0] decode_packet0op_BR_type;
  input [1:0] decode_packet0shift_type;
  input [2:0] decode_packet0compare_type;
  input  decode_packet0is_imm26;
  input  decode_packet0is_imm;
  input  decode_packet0is_imm5;
  input  decode_packet0is_signed;
  input  decode_packet0carry_in;
  input  decode_packet0carry_out;
  input  decode_packet0has_sreg_t;
  input  decode_packet0has_sreg_s;
  input  decode_packet0has_dreg;
  input  decode_packet0is_prefetch;
  input  decode_packet0sprf_dest;
  input  decode_packet0sprf_src;
  input  decode_packet0is_atomic;
  input  decode_packet0is_ldl;
  input  decode_packet0is_stl;
  input  decode_packet0is_stc;
  input  decode_packet0is_valid;
  input  decode_packet0is_halt;
  input  decode_packet0is_compare;
  input  decode_packet0dreg_is_src;
  input  decode_packet0dreg_is_dest;
  input [4:0] decode_packet1sreg_t;
  input [4:0] decode_packet1sreg_s;
  input [4:0] decode_packet1dreg;
  input [1:0] decode_packet1acc;
  input [15:0] decode_packet1imm16_value;
  input [25:0] decode_packet1imm26_value;
  input [4:0] decode_packet1imm5_value;
  input [3:0] decode_packet1op_FU_type;
  input [4:0] decode_packet1alu_op;
  input [4:0] decode_packet1fpu_op;
  input [3:0] decode_packet1op_MEM_type;
  input [1:0] decode_packet1op_BR_type;
  input [1:0] decode_packet1shift_type;
  input [2:0] decode_packet1compare_type;
  input  decode_packet1is_imm26;
  input  decode_packet1is_imm;
  input  decode_packet1is_imm5;
  input  decode_packet1is_signed;
  input  decode_packet1carry_in;
  input  decode_packet1carry_out;
  input  decode_packet1has_sreg_t;
  input  decode_packet1has_sreg_s;
  input  decode_packet1has_dreg;
  input  decode_packet1is_prefetch;
  input  decode_packet1sprf_dest;
  input  decode_packet1sprf_src;
  input  decode_packet1is_atomic;
  input  decode_packet1is_ldl;
  input  decode_packet1is_stl;
  input  decode_packet1is_stc;
  input  decode_packet1is_valid;
  input  decode_packet1is_halt;
  input  decode_packet1is_compare;
  input  decode_packet1dreg_is_src;
  input  decode_packet1dreg_is_dest;
	input [31:0] fetch2decodepc1;
	input [31:0] fetch2decodepc0;
	input [31:0] fetch2decodepc1_incr;
	input [31:0] fetch2decodepc0_incr;
	input  fetch2decodeinstr0_valid;
	input  fetch2decodeinstr1_valid;
	input  fetch2decodebranch_taken;
	input [31:0] fetch2decodepredicted_pc;
	input  fetch2decodevalid;
	input [4:0] data_in_bpaddress_0;
	input [31:0] data_in_bpdata_0;
	input  data_in_bpvalid_0;
	input [4:0] data_in_bpaddress_1;
	input [31:0] data_in_bpdata_1;
	input  data_in_bpvalid_1;
	input [4:0] data_in_bpaddress_2;
	input [31:0] data_in_bpdata_2;
	input  data_in_bpvalid_2;
	input [4:0] data_in_bpaddress_3;
	input [31:0] data_in_bpdata_3;
	input  data_in_bpvalid_3;
	input [4:0] data_in_bpaddress_4;
	input [31:0] data_in_bpdata_4;
	input  data_in_bpvalid_4;
	input [4:0] data_in_bpaddress_5;
	input [31:0] data_in_bpdata_5;
	input  data_in_bpvalid_5;
	input [4:0] data_in_bpaddress_6;
	input [31:0] data_in_bpdata_6;
	input  data_in_bpvalid_6;
	input [4:0] data_in_bpaddress_7;
	input [31:0] data_in_bpdata_7;
	input  data_in_bpvalid_7;
	input [4:0] data_in_bpaddress_8;
	input [31:0] data_in_bpdata_8;
	input  data_in_bpvalid_8;
	input [4:0] data_in_bpaddress_9;
	input [31:0] data_in_bpdata_9;
	input  data_in_bpvalid_9;
	input [4:0] data_in_bpaddress_10;
	input [31:0] data_in_bpdata_10;
	input  data_in_bpvalid_10;
	input  write_enable_0;
	input [4:0] write_addr_0;
	input [31:0] write_data_0;
	input  write_enable_1;
	input [4:0] write_addr_1;
	input [31:0] write_data_1;
	output [31:0] decode2mempc;
	output [31:0] decode2mempc_incr;
	output [1:0] decode2memthread_num;
	output  decode2membranch_taken;
	output [31:0] decode2mempredicted_pc;
	output  decode2memvalid;
	output  decode2memis_oldest;
	output [31:0] decode2memsreg_t_data;
	output [31:0] decode2memsreg_s_data;
	output [31:0] decode2memresult;
	output  decode2memL1D_hit;
	output [4:0] decode2memdecode_packetsreg_t;
	output [4:0] decode2memdecode_packetsreg_s;
	output [4:0] decode2memdecode_packetdreg;
	output [1:0] decode2memdecode_packetacc;
	output [15:0] decode2memdecode_packetimm16_value;
	output [25:0] decode2memdecode_packetimm26_value;
	output [4:0] decode2memdecode_packetimm5_value;
	output [3:0] decode2memdecode_packetop_FU_type;
	output [4:0] decode2memdecode_packetalu_op;
	output [4:0] decode2memdecode_packetfpu_op;
	output [3:0] decode2memdecode_packetop_MEM_type;
	output [1:0] decode2memdecode_packetop_BR_type;
	output [1:0] decode2memdecode_packetshift_type;
	output [2:0] decode2memdecode_packetcompare_type;
	output  decode2memdecode_packetis_imm26;
	output  decode2memdecode_packetis_imm;
	output  decode2memdecode_packetis_imm5;
	output  decode2memdecode_packetis_signed;
	output  decode2memdecode_packetcarry_in;
	output  decode2memdecode_packetcarry_out;
	output  decode2memdecode_packethas_sreg_t;
	output  decode2memdecode_packethas_sreg_s;
	output  decode2memdecode_packethas_dreg;
	output  decode2memdecode_packetis_prefetch;
	output  decode2memdecode_packetsprf_dest;
	output  decode2memdecode_packetsprf_src;
	output  decode2memdecode_packetis_atomic;
	output  decode2memdecode_packetis_ldl;
	output  decode2memdecode_packetis_stl;
	output  decode2memdecode_packetis_stc;
	output  decode2memdecode_packetis_valid;
	output  decode2memdecode_packetis_halt;
	output  decode2memdecode_packetis_compare;
	output  decode2memdecode_packetdreg_is_src;
	output  decode2memdecode_packetdreg_is_dest;
	output [31:0] decode2fpupc;
	output [31:0] decode2fpupc_incr;
	output [1:0] decode2fputhread_num;
	output  decode2fpubranch_taken;
	output [31:0] decode2fpupredicted_pc;
	output  decode2fpuvalid;
	output  decode2fpuis_oldest;
	output [31:0] decode2fpusreg_t_data;
	output [31:0] decode2fpusreg_s_data;
	output [31:0] decode2fpuresult;
	output  decode2fpuL1D_hit;
	output [4:0] decode2fpudecode_packetsreg_t;
	output [4:0] decode2fpudecode_packetsreg_s;
	output [4:0] decode2fpudecode_packetdreg;
	output [1:0] decode2fpudecode_packetacc;
	output [15:0] decode2fpudecode_packetimm16_value;
	output [25:0] decode2fpudecode_packetimm26_value;
	output [4:0] decode2fpudecode_packetimm5_value;
	output [3:0] decode2fpudecode_packetop_FU_type;
	output [4:0] decode2fpudecode_packetalu_op;
	output [4:0] decode2fpudecode_packetfpu_op;
	output [3:0] decode2fpudecode_packetop_MEM_type;
	output [1:0] decode2fpudecode_packetop_BR_type;
	output [1:0] decode2fpudecode_packetshift_type;
	output [2:0] decode2fpudecode_packetcompare_type;
	output  decode2fpudecode_packetis_imm26;
	output  decode2fpudecode_packetis_imm;
	output  decode2fpudecode_packetis_imm5;
	output  decode2fpudecode_packetis_signed;
	output  decode2fpudecode_packetcarry_in;
	output  decode2fpudecode_packetcarry_out;
	output  decode2fpudecode_packethas_sreg_t;
	output  decode2fpudecode_packethas_sreg_s;
	output  decode2fpudecode_packethas_dreg;
	output  decode2fpudecode_packetis_prefetch;
	output  decode2fpudecode_packetsprf_dest;
	output  decode2fpudecode_packetsprf_src;
	output  decode2fpudecode_packetis_atomic;
	output  decode2fpudecode_packetis_ldl;
	output  decode2fpudecode_packetis_stl;
	output  decode2fpudecode_packetis_stc;
	output  decode2fpudecode_packetis_valid;
	output  decode2fpudecode_packetis_halt;
	output  decode2fpudecode_packetis_compare;
	output  decode2fpudecode_packetdreg_is_src;
	output  decode2fpudecode_packetdreg_is_dest;
	output [31:0] decode2intpc;
	output [31:0] decode2intpc_incr;
	output [1:0] decode2intthread_num;
	output  decode2intbranch_taken;
	output [31:0] decode2intpredicted_pc;
	output  decode2intvalid;
	output  decode2intis_oldest;
	output [31:0] decode2intsreg_t_data;
	output [31:0] decode2intsreg_s_data;
	output [31:0] decode2intresult;
	output  decode2intL1D_hit;
	output [4:0] decode2intdecode_packetsreg_t;
	output [4:0] decode2intdecode_packetsreg_s;
	output [4:0] decode2intdecode_packetdreg;
	output [1:0] decode2intdecode_packetacc;
	output [15:0] decode2intdecode_packetimm16_value;
	output [25:0] decode2intdecode_packetimm26_value;
	output [4:0] decode2intdecode_packetimm5_value;
	output [3:0] decode2intdecode_packetop_FU_type;
	output [4:0] decode2intdecode_packetalu_op;
	output [4:0] decode2intdecode_packetfpu_op;
	output [3:0] decode2intdecode_packetop_MEM_type;
	output [1:0] decode2intdecode_packetop_BR_type;
	output [1:0] decode2intdecode_packetshift_type;
	output [2:0] decode2intdecode_packetcompare_type;
	output  decode2intdecode_packetis_imm26;
	output  decode2intdecode_packetis_imm;
	output  decode2intdecode_packetis_imm5;
	output  decode2intdecode_packetis_signed;
	output  decode2intdecode_packetcarry_in;
	output  decode2intdecode_packetcarry_out;
	output  decode2intdecode_packethas_sreg_t;
	output  decode2intdecode_packethas_sreg_s;
	output  decode2intdecode_packethas_dreg;
	output  decode2intdecode_packetis_prefetch;
	output  decode2intdecode_packetsprf_dest;
	output  decode2intdecode_packetsprf_src;
	output  decode2intdecode_packetis_atomic;
	output  decode2intdecode_packetis_ldl;
	output  decode2intdecode_packetis_stl;
	output  decode2intdecode_packetis_stc;
	output  decode2intdecode_packetis_valid;
	output  decode2intdecode_packetis_halt;
	output  decode2intdecode_packetis_compare;
	output  decode2intdecode_packetdreg_is_src;
	output  decode2intdecode_packetdreg_is_dest;
	output  decode_stall;
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // local wires and regs
  //////////////////////////////////////////////////////////////////////////////
  //decode_packet_t decode_packet0,decode_packet1; // signals pass through data from decoders
  reg [31:0] sreg_t_data0;         // signals hold data from regfile
  reg [31:0] sreg_t_data1;         // signals hold data from regfile
  reg [31:0] sreg_s_data0;         // signals hold data from regfile
  reg [31:0] sreg_s_data1;         // signals hold data from regfile
  reg [31:0] sprf_sreg_data;                    // signal holds data from sprf
  reg [3:0] sprf_sreg_addr;                     // signal hold address to a register sprf
  reg sprf_src;                                // sprf read enable
  reg sprf_dest;                               // sprf write enable
  reg [31:0] sreg_t_0;   // data coming out of bypass/rf selector mux
  reg [31:0] sreg_s_0;   // data coming out of bypass/rf selector mux
  reg [31:0] sreg_t_1;   // data coming out of bypass/rf selector mux
  reg [31:0] sreg_s_1;   // data coming out of bypass/rf selector mux
  reg instr0_issued;                           // indicate whether the instruction was issued
  reg schedule0;                     // the instr in this slot may be considered for issue
  reg schedule1;                     // the instr in this slot may be considered for issue
  reg issue0;                           // actually issue the slot this cycle?
  reg issue1;                           // actually issue the slot this cycle?
  reg valid_int;             // indicates whether the FU is being used
  reg valid_fpu;             // indicates whether the FU is being used
  reg valid_mem;             // indicates whether the FU is being used
  reg choice_int;          // indicates which slot issues to the pipe
  reg choice_fpu;          // indicates which slot issues to the pipe
  reg choice_mem;          // indicates which slot issues to the pipe
  reg issue_halt;                               // Propagate the HALT signal
  reg sreg_t_0_is_bp;           // Indicate that data was obtained
  reg sreg_t_1_is_bp;           // Indicate that data was obtained
  reg sreg_s_0_is_bp;           // from the bypass network
  reg sreg_s_1_is_bp;           // from the bypass network
  reg sreg_s_0_rdy;
  reg sreg_t_0_rdy;
  reg sreg_s_1_rdy;
  reg sreg_t_1_rdy;
  reg dreg_0_rdy;
  reg dreg_1_rdy;
  reg follows_vld_branch;                       // Indicate instr in slot 1 follows a valid branch from slot 0
  reg exec_stall;
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // Decoder Modules - 1 Decoder per Fetch/Issue Slot
  //////////////////////////////////////////////////////////////////////////////
  // stall signal indicates that not all instructions were issued
  assign decode_stall = !branch_mispredict && (/*(decode_packet0is_halt || decode_packet1is_halt) ||*/ 
        (!issue1 && fetch2decodevalid && fetch2decodeinstr1_valid));
  // set the read enable signal for sprf
  assign sprf_src = decode_packet0sprf_src | decode_packet1sprf_src;
  // set the write enable signal for sprf
  assign sprf_dest = decode_packet0sprf_dest | decode_packet1sprf_dest;
  // set the proper address to the sprf  
  assign sprf_sreg_addr = ( choice_int ) ? decode_packet1sreg_t:decode_packet0sreg_t;
  // set the halt signal flag only if all stages are unstalled and instr0 is a halt and 
  // is issuing this cycle, or if instr1 is a halt and instr0 issued last cycle or if instr0 in invalid
  assign issue_halt = fetch2decodevalid && !exec_stall && 
      ((decode_packet1is_halt && ((instr0_issued && schedule1) || !fetch2decodeinstr0_valid)) || (decode_packet0is_halt && schedule0));
  assign follows_vld_branch = (decode_packet0op_FU_type == 4'b1010 && fetch2decodeinstr0_valid && branch_mispredict);
  assign exec_stall = stall_in_0 || stall_in_2 || stall_in_1;
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // Decoder Modules - 1 Decoder per Fetch/Issue Slot
  //////////////////////////////////////////////////////////////////////////////
/*
  decoder DECODER0(
    // Input from fetch
    fetch2decodeinstr0_out,
    // Decoded instruction packet
    //decode_packet0
    decode_packet0sreg_t,
    decode_packet0sreg_s,
    decode_packet0dreg,
    decode_packet0acc,
    decode_packet0imm16_value,
    decode_packet0imm26_value,
    decode_packet0imm5_value,
    decode_packet0op_FU_type,
    decode_packet0alu_op,
    decode_packet0fpu_op,
    decode_packet0op_MEM_type,
    decode_packet0op_BR_type,
    decode_packet0shift_type,
    decode_packet0compare_type,
    decode_packet0is_imm26,
    decode_packet0is_imm,
    decode_packet0is_imm5,
    decode_packet0is_signed,
    decode_packet0carry_in,
    decode_packet0carry_out,
    decode_packet0has_sreg_t,
    decode_packet0has_sreg_s,
    decode_packet0has_dreg,
    decode_packet0is_prefetch,
    decode_packet0sprf_dest,
    decode_packet0sprf_src,
    decode_packet0is_atomic,
    decode_packet0is_ldl,
    decode_packet0is_stl,
    decode_packet0is_stc,
    decode_packet0is_valid,
    decode_packet0is_halt,
    decode_packet0is_compare,
    decode_packet0dreg_is_src,
    decode_packet0dreg_is_dest
  );
  
  decoder DECODER1(
    // Input from fetch
    fetch2decodeinstr1_out,
    // Decoded instruction packet
    //decode_packet1
    decode_packet1sreg_t,
    decode_packet1sreg_s,
    decode_packet1dreg,
    decode_packet1acc,
    decode_packet1imm16_value,
    decode_packet1imm26_value,
    decode_packet1imm5_value,
    decode_packet1op_FU_type,
    decode_packet1alu_op,
    decode_packet1fpu_op,
    decode_packet1op_MEM_type,
    decode_packet1op_BR_type,
    decode_packet1shift_type,
    decode_packet1compare_type,
    decode_packet1is_imm26,
    decode_packet1is_imm,
    decode_packet1is_imm5,
    decode_packet1is_signed,
    decode_packet1carry_in,
    decode_packet1carry_out,
    decode_packet1has_sreg_t,
    decode_packet1has_sreg_s,
    decode_packet1has_dreg,
    decode_packet1is_prefetch,
    decode_packet1sprf_dest,
    decode_packet1sprf_src,
    decode_packet1is_atomic,
    decode_packet1is_ldl,
    decode_packet1is_stl,
    decode_packet1is_stc,
    decode_packet1is_valid,
    decode_packet1is_halt,
    decode_packet1is_compare,
    decode_packet1dreg_is_src,
    decode_packet1dreg_is_dest
  );
*/
  ///////////////////////////////////////////////////////////////////////////////
  // *** local wires and regs ***
  ///////////////////////////////////////////////////////////////////////////////
  reg dst0_vld; // status indicates that the dreg is valid for locking
  reg dst1_vld;
  // We can only lock the destination register if the instruction has a dreg,
  // if it's being issued this cycle, if it doesn't follow a mispredicted branch,
  // branch, if it's a valid instruction from fetch, and in the case of slot 1,
  // if the instruction in slot 0 is NOT a valid branch (no speculative
  // instruction execution)
  assign dst0_vld = decode_packet0has_dreg && issue0 && schedule0 && !branch_mispredict && fetch2decodeinstr0_valid;
  assign dst1_vld = decode_packet1has_dreg && issue1 && schedule1 && !branch_mispredict && fetch2decodeinstr1_valid &&
                    !follows_vld_branch;
  ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  // Scoreboard
  ///////////////////////////////////////////////////////////////////////////////
/*
  scoreboard SB(
    // control signals
    .clk(clk),
    .reset(reset),
    // Source registers - inputs to read ports
    .src0_addr(decode_packet0sreg_s),
    .src1_addr(decode_packet0sreg_t),
    .src2_addr(decode_packet1sreg_s),
    .src3_addr(decode_packet1sreg_t),
    // Destination registers - inputs to read/write ports
    .dst0_addr(decode_packet0dreg),
    .dst0_vld(dst0_vld),
    .dst1_addr(decode_packet1dreg),
    .dst1_vld(dst1_vld),
    // Writeback access - inputs to write ports
    .wb0_addr(write_addr_0),
    .wb0_vld(write_enable_0),
    .wb1_addr(write_addr_1),
    .wb1_vld(write_enable_1),
    // Outputs from the scoreboard
    .src0_rdy(sreg_s_0_rdy),
    .src1_rdy(sreg_t_0_rdy),
    .src2_rdy(sreg_s_1_rdy),
    .src3_rdy(sreg_t_1_rdy),
    .dst0_rdy(dreg_0_rdy),
    .dst1_rdy(dreg_1_rdy)
  );
*/
  // module SB
  reg sb[0:31];      // scoreboard bits
  reg next_sb     [0:4-1]; // next scoreboard bits
  reg [4:0] next_sb_addr[0:4-1]; // next scoreboard bits
  reg next_sb_vld [0:4-1]; // next scoreboard bits

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** combinational logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_READ
  //////////////////////////////////////////////////////////////////////////////
  // 
  //////////////////////////////////////////////////////////////////////////////
  integer s;
  reg sb_read[0:4-1];

  assign dreg_0_rdy = sb[decode_packet0dreg]!=1'b0;
  assign dreg_1_rdy = sb[decode_packet1dreg]!=1'b0;// && (decode_packet0dreg!=decode_packet1dreg || !dst0_vld || !dst1_vld);

  assign sreg_s_0_rdy = sb[decode_packet0sreg_s] || decode_packet0sreg_s == 5'b0;
  assign sreg_t_0_rdy = sb[decode_packet0sreg_t] || decode_packet0sreg_t == 5'b0;
  assign sreg_s_1_rdy = sb[decode_packet1sreg_s] || decode_packet1sreg_s == 5'b0;
  assign sreg_t_1_rdy = sb[decode_packet1sreg_t] || decode_packet1sreg_t == 5'b0;

  assign next_sb_addr[0] = decode_packet0dreg;
  assign next_sb_addr[1] = decode_packet1dreg;
  assign next_sb_addr[2] = write_addr_0;
  assign next_sb_addr[3] = write_addr_1;
  //////////////////////////////////////////////////////////////////////////////
/*  always_comb begin: SB_READ

    for(s=0;s<4;s++) begin
      sb_read[s] = sb[s]; 
    end
  end
  */
  //////////////////////////////////////////////////////////////////////////////
 
  
  //////////////////////////////////////////////////////////////////////////////
  // SB_UNLOCK
  //////////////////////////////////////////////////////////////////////////////
  integer w;
  integer wp;
  always @ (dst0_vld or
			decode_packet0dreg or
			sb or
			dst1_vld or
			decode_packet1dreg or
			write_enable_0 or
			write_enable_1) begin: SB_UNLOCK
    // decode/issue ports (lock) 
    next_sb[0] = 1'b0;
    if(dst0_vld && sb[decode_packet0dreg]==1'b1) begin
      next_sb_vld[0] = 1'b1;
    end else begin
      next_sb_vld[0] = 1'b0;
    end
    next_sb[1] = 1'b0;
    if(dst1_vld && sb[decode_packet1dreg]==1'b1) begin
      next_sb_vld[1] = 1'b1;
    end else begin
      next_sb_vld[1] = 1'b0;
    end
    // writeback ports (unlock)
    next_sb[2] = 1'b1;
    if(write_enable_0) begin
      next_sb_vld[2] = 1'b1;
    end else begin
      next_sb_vld[2] = 1'b0;
    end
    next_sb[3] = 1'b1;
    if(write_enable_1) begin
      next_sb_vld[3] = 1'b1;
    end else begin
      next_sb_vld[3] = 1'b0;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
 

////////////////////////////////////////////////////////////////////////////////
// *** sequential logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_UPDATE
  //////////////////////////////////////////////////////////////////////////////
  // update the scoreboard
  // for reset
  integer i;
  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk) begin: SB_UPDATE

    // reset
    if( reset == 1 ) begin
      for(i=0;i<32;i=i+1) begin
        sb[i] <= 1'b1; 
      end
    end

    // clock
    else begin

      // for each port, write update if enabled
      for(i=0;i<4;i=i+1) begin
        if(next_sb_vld[i]) begin
          sb[next_sb_addr[i]] <= next_sb[i];
        end
      end

    end

  end

  // endmodule SB
  ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  // Register File - 4 Read Ports, 2 Write Ports
  ///////////////////////////////////////////////////////////////////////////////
/*
  rf_4r2w RF(
    // control signals
    .clk(clk),
    .reset(reset),
    .enable(1'b1),                 // Reg File Enable Signal - used for clock gating
    // write port input signals
    .write_addr_0(write_addr_0),
    .write_addr_1(write_addr_1),
    .write_data_0(write_data_0),
    .write_data_1(write_data_1),
    .write_enable_0(write_enable_0),
    .write_enable_1(write_enable_1),
    // read port input signals
    .read_addr_0(decode_packet0sreg_t),
    .read_addr_1(decode_packet0sreg_s),
    .read_addr_2(decode_packet1sreg_t),
    .read_addr_3(decode_packet1sreg_s),
    .read_enable_0(decode_packet0has_sreg_t),
    .read_enable_1(decode_packet0has_sreg_s),
    .read_enable_2(decode_packet1has_sreg_t),
    .read_enable_3(decode_packet1has_sreg_s),
    // read port data outputs
    .read_data_0(sreg_t_data0),
    .read_data_1(sreg_s_data0),
    .read_data_2(sreg_t_data1),
    .read_data_3(sreg_s_data1)
  );
*/
  // module RF
  reg[31:0] rf[0:31]; 
  reg we_0;
  reg we_1;
//  logic[31:0] d_out_0,d_out_1,d_out_2,d_out_3;
      integer i;
  //////////////////////////////////////////////////////////////////////////////
  // Continuous assignments
  //////////////////////////////////////////////////////////////////////////////
  assign sreg_t_data0 = (decode_packet0has_sreg_t && (decode_packet0sreg_t != 5'b0))? rf[decode_packet0sreg_t] : 32'b0;
  assign sreg_s_data0 = (decode_packet0has_sreg_s && (decode_packet0sreg_s != 5'b0))? rf[decode_packet0sreg_s] : 32'b0;
  assign sreg_t_data1 = (decode_packet1has_sreg_t && (decode_packet1sreg_t != 5'b0))? rf[decode_packet1sreg_t] : 32'b0;
  assign sreg_s_data1 = (decode_packet1has_sreg_s && (decode_packet1sreg_s != 5'b0))? rf[decode_packet1sreg_s] : 32'b0;
  assign we_0 = write_enable_0 && !( write_addr_0 == 5'b0);
  assign we_1 = write_enable_1 && !( write_addr_1 == 5'b0);
  always @ (posedge clk or posedge reset)
  begin

    // reset
    if( reset == 1 ) begin
      for(i=0;i<32;i=i+1) begin
        rf[i] <= 32'b0;    
      end
    end

    // regular clocked operation
    else begin

      // write port 0
      if( we_0 )
        rf[write_addr_0] <= write_data_0;
      // write port 1
      if( we_1 )
        rf[write_addr_1] <= write_data_1;
    end
  end
  // endmodule RF
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  // Special Purpose Register File - aka SPRF
  //////////////////////////////////////////////////////////////////////////////
  /*
  sprf SPRF(
    // control signals
    .clk(clk),
    .reset(reset),
    // read/write input signals
    .read_addr(sprf_sreg_addr),
    .read_enable(sprf_src),       // read from sprf
    .write_enable(sprf_dest),     // write to sprf
    // data output from sprf
    .read_data(sprf_sreg_data)
  );
*/
  // module SPRF
  reg [31:0] sprf[0:16-1];
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // Continuous assignments
  //////////////////////////////////////////////////////////////////////////////
  assign sprf_sreg_data = sprf[sprf_sreg_addr];
  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk or posedge reset) begin: SPRF_CLK

    ////////////////////////////////////////////////////////////////////////////
    // reset
    if( reset == 1'b1 ) begin
      //reset sprf state
      sprf[4'h0] = 32'h00000000; 
      sprf[4'h1] = 32'h00000100;
      sprf[4'h2] = 32'h00000008;
      sprf[4'h3] = 32'h00000010;
      //SPRF_UNDEF_3 = 3,
      sprf[4'h4] = 32'h00000000;
      sprf[4'h5] = 32'h00000100;
      sprf[4'h6] = 32'h00000000;
      sprf[4'h7] = 32'h00001000;
      sprf[4'h8] = 32'h00000004;
      sprf[4'h9] = 32'h000000ff;
      sprf[4'hA] = 32'h00000200;
    end

    // handle writes 
    else begin
//      if(valid_in) begin
        // case(addr_in)
        //   // use ENUMs here common to sim
        // 
        // endcase 

   //   end

    end
    ////////////////////////////////////////////////////////////////////////////
  
  end  

  // endmodule SPRF
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // RF / Bypass Selectors
  //////////////////////////////////////////////////////////////////////////////
  // For each register in each issue slot the modules select whether the data
  // comes from the bypass network or the register file
  //////////////////////////////////////////////////////////////////////////////
  /*
  bypass_select BP_SELECT_S_0(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_s_data0),
    .address_in_rf(decode_packet0sreg_s),
    // Output from the source selector
    .data_out(sreg_s_0),         // Data
    .src_is_bp(sreg_s_0_is_bp)  // Indicator that data obtained from bp network
  );
  bypass_select BP_SELECT_S_1(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_s_data1),
    .address_in_rf(decode_packet1sreg_s),
    // Output from thie source selector
    .data_out(sreg_s_1),
    .src_is_bp(sreg_s_1_is_bp)  // Indicator that data obtained from bp network
  );
  bypass_select BP_SELECT_T_0(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_t_data0),
    .address_in_rf(decode_packet0sreg_t),
    // Output from the source selector
    .data_out(sreg_t_0),
    .src_is_bp(sreg_t_0_is_bp)  // Indicator that data obtained from bp network
  );
  bypass_select BP_SELECT_T_1(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_t_data1),
    .address_in_rf(decode_packet1sreg_t),
    // Output from the source selector
    .data_out(sreg_t_1),
    .src_is_bp(sreg_t_1_is_bp)  // Indicator that data obtained from bp network
  );
*/
  // module BPSELECT_S_0
  always @ (
  decode_packet0sreg_s or
  sreg_s_data0 or
  data_in_bpaddress_0 or
  data_in_bpdata_0 or
  data_in_bpvalid_0 or
  data_in_bpaddress_1 or
  data_in_bpdata_1 or
  data_in_bpvalid_1 or
  data_in_bpaddress_2 or
  data_in_bpdata_2 or
  data_in_bpvalid_2 or
  data_in_bpaddress_3 or
  data_in_bpdata_3 or
  data_in_bpvalid_3 or
  data_in_bpaddress_4 or
  data_in_bpdata_4 or
  data_in_bpvalid_4 or
  data_in_bpaddress_5 or
  data_in_bpdata_5 or
  data_in_bpvalid_5 or
  data_in_bpaddress_6 or
  data_in_bpdata_6 or
  data_in_bpvalid_6 or
  data_in_bpaddress_7 or
  data_in_bpdata_7 or
  data_in_bpvalid_7 or
  data_in_bpaddress_8 or
  data_in_bpdata_8 or
  data_in_bpvalid_8 or
  data_in_bpaddress_9 or
  data_in_bpdata_9 or
  data_in_bpvalid_9 or
  data_in_bpaddress_10 or
  data_in_bpdata_10 or
  data_in_bpvalid_10
  ) begin
    // check each bypass source to see if we can obtain the data from there
    // Note: Never bypass register 0.
/*    if( data_in_bp[0].valid && (data_in_bp[0].address == decode_packet0sreg_s) && 
        data_in_bp[0].address != 5'b0) begin
      sreg_s_0 = data_in_bp[i].data; 
      sreg_s_0_is_bp = 1'b1;
    end
    generate
      for(i=0;i<BYPASS_SIZE;i++) begin
        // check each bypass source to see if we can obtain the data from there
        // Note: Never bypass register 0.
        else if( data_in_bp[i].valid && (data_in_bp[i].address == decode_packet0sreg_s) && 
            data_in_bp[i].address != 5'b0) begin
          sreg_s_0 = data_in_bp[i].data; 
          sreg_s_0_is_bp = 1'b1;
        end
      end
    endgenerate
    else begin
      sreg_s_0 = sreg_s_data0;      // By default read the regfile
      sreg_s_0_is_bp = 1'b0;
    end*/
    // check each bypass source to determine where data is coming from
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == decode_packet0sreg_s) && data_in_bpaddress_0 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_0; // Exec0 - INT
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == decode_packet0sreg_s) && data_in_bpaddress_1 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_1; // Exec0 - MEM
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == decode_packet0sreg_s) && data_in_bpaddress_2 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_2; // Exec0 - FPU
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == decode_packet0sreg_s) && data_in_bpaddress_3 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_3; // Exec0 - FPU
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == decode_packet0sreg_s) && data_in_bpaddress_4 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_4; // Exec0 - FPU
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == decode_packet0sreg_s) && data_in_bpaddress_5 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_5; // Exec0 - FPU
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == decode_packet0sreg_s) && data_in_bpaddress_6 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_6; // WB - slot0
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == decode_packet0sreg_s) && data_in_bpaddress_7 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_7; // WB - slot1
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == decode_packet0sreg_s) && data_in_bpaddress_8 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_8; // Exec0 - FPU
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == decode_packet0sreg_s) && data_in_bpaddress_9 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_9; // WB - slot0
      sreg_s_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == decode_packet0sreg_s) && data_in_bpaddress_10 != 5'b0) begin
      sreg_s_0 = data_in_bpdata_10; // WB - slot1
      sreg_s_0_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      sreg_s_0 = sreg_s_data0;
      sreg_s_0_is_bp = 1'b0;
    end
  end
  // endmodule BPSELECT_S_0
  // module BPSELECT_S_1
  always @ (
  decode_packet1sreg_s or
  sreg_s_data1 or
  data_in_bpaddress_0 or
  data_in_bpdata_0 or
  data_in_bpvalid_0 or
  data_in_bpaddress_1 or
  data_in_bpdata_1 or
  data_in_bpvalid_1 or
  data_in_bpaddress_2 or
  data_in_bpdata_2 or
  data_in_bpvalid_2 or
  data_in_bpaddress_3 or
  data_in_bpdata_3 or
  data_in_bpvalid_3 or
  data_in_bpaddress_4 or
  data_in_bpdata_4 or
  data_in_bpvalid_4 or
  data_in_bpaddress_5 or
  data_in_bpdata_5 or
  data_in_bpvalid_5 or
  data_in_bpaddress_6 or
  data_in_bpdata_6 or
  data_in_bpvalid_6 or
  data_in_bpaddress_7 or
  data_in_bpdata_7 or
  data_in_bpvalid_7 or
  data_in_bpaddress_8 or
  data_in_bpdata_8 or
  data_in_bpvalid_8 or
  data_in_bpaddress_9 or
  data_in_bpdata_9 or
  data_in_bpvalid_9 or
  data_in_bpaddress_10 or
  data_in_bpdata_10 or
  data_in_bpvalid_10
  ) begin
    // check each bypass source to see if we can obtain the data from there
    // Note: Never bypass register 0.
/*    if( data_in_bp[0].valid && (data_in_bp[0].address == decode_packet1sreg_s) && 
        data_in_bp[0].address != 5'b0) begin
      sreg_s_1 = data_in_bp[i].data; 
      sreg_s_1_is_bp = 1'b1;
    end
    generate
      for(i=0;i<BYPASS_SIZE;i++) begin
        // check each bypass source to see if we can obtain the data from there
        // Note: Never bypass register 0.
        else if( data_in_bp[i].valid && (data_in_bp[i].address == decode_packet1sreg_s) && 
            data_in_bp[i].address != 5'b0) begin
          sreg_s_1 = data_in_bp[i].data; 
          sreg_s_1_is_bp = 1'b1;
        end
      end
    endgenerate
    else begin
      sreg_s_1 = sreg_s_data1;      // By default read the regfile
      sreg_s_1_is_bp = 1'b0;
    end*/
    // check each bypass source to determine where data is coming from
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == decode_packet1sreg_s) && data_in_bpaddress_0 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_0; // Exec0 - INT
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == decode_packet1sreg_s) && data_in_bpaddress_1 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_1; // Exec0 - MEM
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == decode_packet1sreg_s) && data_in_bpaddress_2 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_2; // Exec0 - FPU
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == decode_packet1sreg_s) && data_in_bpaddress_3 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_3; // Exec0 - FPU
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == decode_packet1sreg_s) && data_in_bpaddress_4 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_4; // Exec0 - FPU
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == decode_packet1sreg_s) && data_in_bpaddress_5 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_5; // Exec0 - FPU
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == decode_packet1sreg_s) && data_in_bpaddress_6 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_6; // WB - slot0
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == decode_packet1sreg_s) && data_in_bpaddress_7 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_7; // WB - slot1
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == decode_packet1sreg_s) && data_in_bpaddress_8 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_8; // Exec0 - FPU
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == decode_packet1sreg_s) && data_in_bpaddress_9 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_9; // WB - slot0
      sreg_s_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == decode_packet1sreg_s) && data_in_bpaddress_10 != 5'b0) begin
      sreg_s_1 = data_in_bpdata_10; // WB - slot1
      sreg_s_1_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      sreg_s_1 = sreg_s_data1;
      sreg_s_1_is_bp = 1'b0;
    end
  end
  // endmodule BPSELECT_S_1
  // module BPSELECT_T_0
  always @ (
  decode_packet0sreg_t or
  sreg_t_data0 or
  data_in_bpaddress_0 or
  data_in_bpdata_0 or
  data_in_bpvalid_0 or
  data_in_bpaddress_1 or
  data_in_bpdata_1 or
  data_in_bpvalid_1 or
  data_in_bpaddress_2 or
  data_in_bpdata_2 or
  data_in_bpvalid_2 or
  data_in_bpaddress_3 or
  data_in_bpdata_3 or
  data_in_bpvalid_3 or
  data_in_bpaddress_4 or
  data_in_bpdata_4 or
  data_in_bpvalid_4 or
  data_in_bpaddress_5 or
  data_in_bpdata_5 or
  data_in_bpvalid_5 or
  data_in_bpaddress_6 or
  data_in_bpdata_6 or
  data_in_bpvalid_6 or
  data_in_bpaddress_7 or
  data_in_bpdata_7 or
  data_in_bpvalid_7 or
  data_in_bpaddress_8 or
  data_in_bpdata_8 or
  data_in_bpvalid_8 or
  data_in_bpaddress_9 or
  data_in_bpdata_9 or
  data_in_bpvalid_9 or
  data_in_bpaddress_10 or
  data_in_bpdata_10 or
  data_in_bpvalid_10
  ) begin
    // check each bypass source to see if we can obtain the data from there
    // Note: Never bypass register 0.
/*    if( data_in_bp[0].valid && (data_in_bp[0].address == decode_packet0sreg_t) && 
        data_in_bp[0].address != 5'b0) begin
      sreg_t_0 = data_in_bp[i].data; 
      sreg_t_0_is_bp = 1'b1;
    end
    generate
      for(i=0;i<BYPASS_SIZE;i++) begin
        // check each bypass source to see if we can obtain the data from there
        // Note: Never bypass register 0.
        else if( data_in_bp[i].valid && (data_in_bp[i].address == decode_packet0sreg_t) && 
            data_in_bp[i].address != 5'b0) begin
          sreg_t_0 = data_in_bp[i].data; 
          sreg_t_0_is_bp = 1'b1;
        end
      end
    endgenerate
    else begin
      sreg_t_0 = sreg_t_data0;      // By default read the regfile
      sreg_t_0_is_bp = 1'b0;
    end*/
    // check each bypass source to determine where data is coming from
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == decode_packet0sreg_t) && data_in_bpaddress_0 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_0; // Exec0 - INT
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == decode_packet0sreg_t) && data_in_bpaddress_1 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_1; // Exec0 - MEM
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == decode_packet0sreg_t) && data_in_bpaddress_2 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_2; // Exec0 - FPU
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == decode_packet0sreg_t) && data_in_bpaddress_3 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_3; // Exec0 - FPU
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == decode_packet0sreg_t) && data_in_bpaddress_4 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_4; // Exec0 - FPU
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == decode_packet0sreg_t) && data_in_bpaddress_5 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_5; // Exec0 - FPU
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == decode_packet0sreg_t) && data_in_bpaddress_6 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_6; // WB - slot0
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == decode_packet0sreg_t) && data_in_bpaddress_7 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_7; // WB - slot1
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == decode_packet0sreg_t) && data_in_bpaddress_8 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_8; // Exec0 - FPU
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == decode_packet0sreg_t) && data_in_bpaddress_9 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_9; // WB - slot0
      sreg_t_0_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == decode_packet0sreg_t) && data_in_bpaddress_10 != 5'b0) begin
      sreg_t_0 = data_in_bpdata_10; // WB - slot1
      sreg_t_0_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      sreg_t_0 = sreg_t_data0;
      sreg_t_0_is_bp = 1'b0;
    end
  end	
  // endmodule BPSELECT_T_0
  // module BPSELECT_T_1
  always @ (
  decode_packet1sreg_t or
  sreg_t_data1 or
  data_in_bpaddress_0 or
  data_in_bpdata_0 or
  data_in_bpvalid_0 or
  data_in_bpaddress_1 or
  data_in_bpdata_1 or
  data_in_bpvalid_1 or
  data_in_bpaddress_2 or
  data_in_bpdata_2 or
  data_in_bpvalid_2 or
  data_in_bpaddress_3 or
  data_in_bpdata_3 or
  data_in_bpvalid_3 or
  data_in_bpaddress_4 or
  data_in_bpdata_4 or
  data_in_bpvalid_4 or
  data_in_bpaddress_5 or
  data_in_bpdata_5 or
  data_in_bpvalid_5 or
  data_in_bpaddress_6 or
  data_in_bpdata_6 or
  data_in_bpvalid_6 or
  data_in_bpaddress_7 or
  data_in_bpdata_7 or
  data_in_bpvalid_7 or
  data_in_bpaddress_8 or
  data_in_bpdata_8 or
  data_in_bpvalid_8 or
  data_in_bpaddress_9 or
  data_in_bpdata_9 or
  data_in_bpvalid_9 or
  data_in_bpaddress_10 or
  data_in_bpdata_10 or
  data_in_bpvalid_10
  ) begin
    // check each bypass source to see if we can obtain the data from there
    // Note: Never bypass register 0.
/*    if( data_in_bp[0].valid && (data_in_bp[0].address == decode_packet1sreg_t) && 
        data_in_bp[0].address != 5'b0) begin
      sreg_t_1 = data_in_bp[i].data; 
      sreg_t_1_is_bp = 1'b1;
    end
    generate
      for(i=0;i<BYPASS_SIZE;i++) begin
        // check each bypass source to see if we can obtain the data from there
        // Note: Never bypass register 0.
        else if( data_in_bp[i].valid && (data_in_bp[i].address == decode_packet1sreg_t) && 
            data_in_bp[i].address != 5'b0) begin
          sreg_t_1 = data_in_bp[i].data; 
          sreg_t_1_is_bp = 1'b1;
        end
      end
    endgenerate
    else begin
      sreg_t_1 = sreg_t_data1;      // By default read the regfile
      sreg_t_1_is_bp = 1'b0;
    end*/
    // check each bypass source to determine where data is coming from
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == decode_packet1sreg_t) && data_in_bpaddress_0 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_0; // Exec0 - INT
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == decode_packet1sreg_t) && data_in_bpaddress_1 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_1; // Exec0 - MEM
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == decode_packet1sreg_t) && data_in_bpaddress_2 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_2; // Exec0 - FPU
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == decode_packet1sreg_t) && data_in_bpaddress_3 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_3; // Exec0 - FPU
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == decode_packet1sreg_t) && data_in_bpaddress_4 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_4; // Exec0 - FPU
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == decode_packet1sreg_t) && data_in_bpaddress_5 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_5; // Exec0 - FPU
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == decode_packet1sreg_t) && data_in_bpaddress_6 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_6; // WB - slot0
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == decode_packet1sreg_t) && data_in_bpaddress_7 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_7; // WB - slot1
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == decode_packet1sreg_t) && data_in_bpaddress_8 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_8; // Exec0 - FPU
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == decode_packet1sreg_t) && data_in_bpaddress_9 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_9; // WB - slot0
      sreg_t_1_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == decode_packet1sreg_t) && data_in_bpaddress_10 != 5'b0) begin
      sreg_t_1 = data_in_bpdata_10; // WB - slot1
      sreg_t_1_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      sreg_t_1 = sreg_t_data1;
      sreg_t_1_is_bp = 1'b0;
    end
  end
  // endmodule BPSELECT_T_1
  //////////////////////////////////////////////////////////////////////////////

 
  //////////////////////////////////////////////////////////////////////////////
  // ISSUE_0
  //////////////////////////////////////////////////////////////////////////////
  // Determine if instruction from slot 0 may be issued this cycle  It may only 
  // be issued if a valid instruction was fetched into this slot, and when there
  // are no stalls issued by the scoreboard, and if it has not already been 
  // issued in the previous cycle
  //////////////////////////////////////////////////////////////////////////////
  always @ (fetch2decodeinstr0_valid or 
			fetch2decodevalid or 
			exec_stall or
			sreg_t_0_is_bp or 
			sreg_t_0_rdy or 
			decode_packet0has_sreg_t or
			sreg_s_0_is_bp or 
			sreg_s_0_rdy or 
			decode_packet0has_sreg_s or
			dreg_0_rdy or 
			decode_packet0has_dreg or
			instr0_issued) begin: ISSUE_0
    if( fetch2decodeinstr0_valid && fetch2decodevalid && !exec_stall &&
        ( sreg_t_0_is_bp || sreg_t_0_rdy || !decode_packet0has_sreg_t) &&
        ( sreg_s_0_is_bp || sreg_s_0_rdy || !decode_packet0has_sreg_s) && 
        ( dreg_0_rdy || !decode_packet0has_dreg) &&
        !instr0_issued 
      )
      schedule0 = 1'b1;
    else
      schedule0 = 1'b0;
  end
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // ISSUE_1
  //////////////////////////////////////////////////////////////////////////////
  // Determine if instruction from slot 1 may be issued this cycle  It may only
  // be issued if all the following conditions have been met:
  //  - There are no stalls from the scoreboard
  //  - Instruction from slot 0 has issued last cycle or this cycle
  //  - If instruction from slot 0 is issuing this cycle, issue only if:
  //    - Instr1 does not need to go down the same pipe
  //    - instr1 and instr0 do not write to the same register
  //    - instr0 does not write to source registers used by instr1
  //    - The instruction in slot 1 is a valid instruction
  //    - instr1 is not a halt
  //    - Instr1 is a nop
  //  - If instr1 is a halt, issue only if instr0 has been issued previous cycle
  //////////////////////////////////////////////////////////////////////////////
  always @ (exec_stall or 
			sreg_t_1_is_bp or 
			sreg_t_1_rdy or 
			sreg_s_1_is_bp or 
			sreg_s_1_rdy or 
			dreg_1_rdy or
			decode_packet0op_FU_type or 
			decode_packet0dreg or 
			decode_packet0has_dreg or
			decode_packet1has_sreg_s or
			decode_packet1has_sreg_t or 
			decode_packet1dreg or
			decode_packet1has_dreg or
			decode_packet1sreg_s or
			decode_packet1op_FU_type or
			decode_packet1is_halt or
			decode_packet1is_valid or 
			fetch2decodeinstr0_valid or 
			fetch2decodeinstr1_valid or 
			issue0 or 
			instr0_issued or 
			fetch2decodeinstr0_valid) begin: ISSUE_1 
    if( !exec_stall && ( sreg_t_1_is_bp || sreg_t_1_rdy || !decode_packet1has_sreg_t) && 
        ( sreg_s_1_is_bp || sreg_s_1_rdy || !decode_packet1has_sreg_s) && 
        ( dreg_1_rdy || !decode_packet1has_dreg) &&
        (((decode_packet0op_FU_type != decode_packet1op_FU_type) &&
          /* Right now we don't issue a branch in parallel with another
           * instruction, may need to remove/modify the next line in the future 
           * to allow for branch / other op issue in parallel */
          (decode_packet0op_FU_type != 4'b1010) &&
          ((decode_packet0dreg != decode_packet1dreg) || !decode_packet0has_dreg || !decode_packet1has_dreg) &&
          ((decode_packet0dreg != decode_packet1sreg_t) || !decode_packet0has_dreg || !decode_packet1has_sreg_t) && 
          ((decode_packet0dreg != decode_packet1sreg_s) || !decode_packet0has_dreg || !decode_packet1has_sreg_s) &&
          fetch2decodeinstr0_valid && fetch2decodeinstr1_valid && issue0 && !decode_packet1is_halt
         ) || instr0_issued  || (issue0 && !decode_packet1is_valid) || !fetch2decodeinstr0_valid 
        )
      )
      // NOTE TO ONESELF FROM MY PAST ONESELF - If issues arise when sb is being
      // used it's probably due to this if statement  
      schedule1 = 1'b1;
    else
      schedule1 = 1'b0;
  end
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // INT_ISSUE
  //////////////////////////////////////////////////////////////////////////////
  // Control whether an instruction from either slot should be issued to the INT
  // pipe  Instructions from slot 0 always take priority
  // * At this time we issue instructions with the FU_ALU, FU_ALU_SHIFT, and
  // FU_BRANCH functional unit types to this pipe
  //////////////////////////////////////////////////////////////////////////////
  always @ (schedule0 or
			decode_packet0op_FU_type or
			decode_packet0is_valid or 
			stall_in_0 or
			schedule1 or
			decode_packet1op_FU_type or
			decode_packet1is_valid) begin: INT_ISSUE
    if ( schedule0  && ( 
         decode_packet0op_FU_type == 4'b0001 || 
         decode_packet0op_FU_type == 4'b0110 ||
         decode_packet0op_FU_type == 4'b1010 ) &&
         decode_packet0is_valid
       ) begin
      valid_int = !stall_in_0;
      choice_int = 1'b0;
    end else begin
      choice_int = 1'b1;
      valid_int = ( schedule1 && ( 
                    decode_packet1op_FU_type == 4'b0001 || 
                    decode_packet1op_FU_type == 4'b0110 ||
                    decode_packet1op_FU_type == 4'b1010 ) &&
                    decode_packet1is_valid && !stall_in_0
                  );
    end
  end
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // FPU_ISSUE
  //////////////////////////////////////////////////////////////////////////////
  // Control whether an instruction from either slot should be issued to the FPU
  // pipe  Instructions from slot 0 always take priority
  // * At this time we only issue instructions with the FU_FPU functional unit
  // type to this pipe
  //////////////////////////////////////////////////////////////////////////////
  always @ (schedule0 or
			decode_packet0op_FU_type or
			decode_packet0is_valid or
			stall_in_2 or
			schedule1 or
			decode_packet1op_FU_type or
			decode_packet1is_valid) begin: FPU_ISSUE
    if ( schedule0 && ( decode_packet0op_FU_type == 4'b0011) &&
         decode_packet0is_valid
       ) begin
      valid_fpu = !stall_in_2;
      choice_fpu = 1'b0;
    end
    else begin
      valid_fpu = ( schedule1 && ( decode_packet1op_FU_type == 4'b0011) &&
                    decode_packet1is_valid && !stall_in_2
                  );
      choice_fpu = 1'b1;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
 
  //////////////////////////////////////////////////////////////////////////////
  // MEM_ISSUE
  //////////////////////////////////////////////////////////////////////////////
  // Control whether an instruction from either slot should be issued to the MEM
  // pipe  Instructions from slot 0 always take priority
  // * At this time we only issue instructions with the FU_MEM functional unit
  // type to this pipe
  //////////////////////////////////////////////////////////////////////////////
  always @ (schedule0 or
			decode_packet0op_FU_type or
			decode_packet0is_valid or
			stall_in_1 or
			schedule1 or
			decode_packet1op_FU_type or
			decode_packet1is_valid) begin: MEM_ISSUE
     if ( schedule0 && ( decode_packet0op_FU_type == 4'b1000) &&
          decode_packet0is_valid
        ) begin
      valid_mem = !stall_in_1;
      choice_mem = 1'b0;
    end
    else begin
      valid_mem = ( schedule1 && ( decode_packet1op_FU_type == 4'b1000) && 
                    decode_packet1is_valid && !stall_in_1
                  );
      choice_mem = 1'b1;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
  assign issue0 = fetch2decodeinstr0_valid && (!decode_packet0is_valid || 
        (valid_mem &&!choice_mem) || (valid_fpu && !choice_fpu) || (valid_int && !choice_int));
  assign issue1 = fetch2decodeinstr1_valid && (((issue0 || instr0_issued || !fetch2decodeinstr0_valid) && !decode_packet1is_valid) ||
        (valid_mem && choice_mem) || (valid_fpu && choice_fpu) || (valid_int && choice_int));

  //////////////////////////////////////////////////////////////////////////////
  // DECODE_OUT
  //////////////////////////////////////////////////////////////////////////////
  // Latch outputs for the DECODE pipeline stage
  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk or posedge reset) begin: DECODE_OUT
    // Reset the signals
    if(reset) begin
      decode2intdecode_packetis_halt <= 1'b0;
      decode2fpudecode_packetis_halt <= 1'b0;
      decode2memdecode_packetis_halt <= 1'b0;
      instr0_issued <= 1'b0;
      decode2intvalid <= 1'b0;
      decode2fpuvalid <= 1'b0;
      decode2memvalid <= 1'b0;
    end
    else begin 
      if(exec_stall) begin
        //decode2int <= decode2int;
        decode2intpc <= decode2intpc;
        decode2intpc_incr <= decode2intpc_incr;
        decode2intthread_num <= decode2intthread_num;
        decode2intbranch_taken <= decode2intbranch_taken;
        decode2intpredicted_pc <= decode2intpredicted_pc;
        decode2intvalid <= decode2intvalid;
        decode2intis_oldest <= decode2intis_oldest;
        decode2intsreg_t_data <= decode2intsreg_t_data;
        decode2intsreg_s_data <= decode2intsreg_s_data;
        decode2intresult <= decode2intresult;
        decode2intL1D_hit <= decode2intL1D_hit;
        decode2intdecode_packetsreg_t <= decode2intdecode_packetsreg_t;
        decode2intdecode_packetsreg_s <= decode2intdecode_packetsreg_s;
        decode2intdecode_packetdreg <= decode2intdecode_packetdreg;
        decode2intdecode_packetacc <= decode2intdecode_packetacc;
        decode2intdecode_packetimm16_value <= decode2intdecode_packetimm16_value;
        decode2intdecode_packetimm26_value <= decode2intdecode_packetimm26_value;
        decode2intdecode_packetimm5_value <= decode2intdecode_packetimm5_value;
        decode2intdecode_packetop_FU_type <= decode2intdecode_packetop_FU_type;
        decode2intdecode_packetalu_op <= decode2intdecode_packetalu_op;
        decode2intdecode_packetfpu_op <= decode2intdecode_packetfpu_op;
        decode2intdecode_packetop_MEM_type <= decode2intdecode_packetop_MEM_type;
        decode2intdecode_packetop_BR_type <= decode2intdecode_packetop_BR_type;
        decode2intdecode_packetshift_type <= decode2intdecode_packetshift_type;
        decode2intdecode_packetcompare_type <= decode2intdecode_packetcompare_type;
        decode2intdecode_packetis_imm26 <= decode2intdecode_packetis_imm26;
        decode2intdecode_packetis_imm <= decode2intdecode_packetis_imm;
        decode2intdecode_packetis_imm5 <= decode2intdecode_packetis_imm5;
        decode2intdecode_packetis_signed <= decode2intdecode_packetis_signed;
        decode2intdecode_packetcarry_in <= decode2intdecode_packetcarry_in;
        decode2intdecode_packetcarry_out <= decode2intdecode_packetcarry_out;
        decode2intdecode_packethas_sreg_t <= decode2intdecode_packethas_sreg_t;
        decode2intdecode_packethas_sreg_s <= decode2intdecode_packethas_sreg_s;
        decode2intdecode_packethas_dreg <= decode2intdecode_packethas_dreg;
        decode2intdecode_packetis_prefetch <= decode2intdecode_packetis_prefetch;
        decode2intdecode_packetsprf_dest <= decode2intdecode_packetsprf_dest;
        decode2intdecode_packetsprf_src <= decode2intdecode_packetsprf_src;
        decode2intdecode_packetis_atomic <= decode2intdecode_packetis_atomic;
        decode2intdecode_packetis_ldl <= decode2intdecode_packetis_ldl;
        decode2intdecode_packetis_stl <= decode2intdecode_packetis_stl;
        decode2intdecode_packetis_stc <= decode2intdecode_packetis_stc;
        decode2intdecode_packetis_valid <= decode2intdecode_packetis_valid;
        decode2intdecode_packetis_halt <= decode2intdecode_packetis_halt;
        decode2intdecode_packetis_compare <= decode2intdecode_packetis_compare;
        decode2intdecode_packetdreg_is_src <= decode2intdecode_packetdreg_is_src;
        decode2intdecode_packetdreg_is_dest <= decode2intdecode_packetdreg_is_dest;
		//decode2fpu <= decode2fpu;
        decode2fpupc <= decode2fpupc;
        decode2fpupc_incr <= decode2fpupc_incr;
        decode2fputhread_num <= decode2fputhread_num;
        decode2fpubranch_taken <= decode2fpubranch_taken;
        decode2fpupredicted_pc <= decode2fpupredicted_pc;
        decode2fpuvalid <= decode2fpuvalid;
        decode2fpuis_oldest <= decode2fpuis_oldest;
        decode2fpusreg_t_data <= decode2fpusreg_t_data;
        decode2fpusreg_s_data <= decode2fpusreg_s_data;
        decode2fpuresult <= decode2fpuresult;
        decode2fpuL1D_hit <= decode2fpuL1D_hit;
        decode2fpudecode_packetsreg_t <= decode2fpudecode_packetsreg_t;
        decode2fpudecode_packetsreg_s <= decode2fpudecode_packetsreg_s;
        decode2fpudecode_packetdreg <= decode2fpudecode_packetdreg;
        decode2fpudecode_packetacc <= decode2fpudecode_packetacc;
        decode2fpudecode_packetimm16_value <= decode2fpudecode_packetimm16_value;
        decode2fpudecode_packetimm26_value <= decode2fpudecode_packetimm26_value;
        decode2fpudecode_packetimm5_value <= decode2fpudecode_packetimm5_value;
        decode2fpudecode_packetop_FU_type <= decode2fpudecode_packetop_FU_type;
        decode2fpudecode_packetalu_op <= decode2fpudecode_packetalu_op;
        decode2fpudecode_packetfpu_op <= decode2fpudecode_packetfpu_op;
        decode2fpudecode_packetop_MEM_type <= decode2fpudecode_packetop_MEM_type;
        decode2fpudecode_packetop_BR_type <= decode2fpudecode_packetop_BR_type;
        decode2fpudecode_packetshift_type <= decode2fpudecode_packetshift_type;
        decode2fpudecode_packetcompare_type <= decode2fpudecode_packetcompare_type;
        decode2fpudecode_packetis_imm26 <= decode2fpudecode_packetis_imm26;
        decode2fpudecode_packetis_imm <= decode2fpudecode_packetis_imm;
        decode2fpudecode_packetis_imm5 <= decode2fpudecode_packetis_imm5;
        decode2fpudecode_packetis_signed <= decode2fpudecode_packetis_signed;
        decode2fpudecode_packetcarry_in <= decode2fpudecode_packetcarry_in;
        decode2fpudecode_packetcarry_out <= decode2fpudecode_packetcarry_out;
        decode2fpudecode_packethas_sreg_t <= decode2fpudecode_packethas_sreg_t;
        decode2fpudecode_packethas_sreg_s <= decode2fpudecode_packethas_sreg_s;
        decode2fpudecode_packethas_dreg <= decode2fpudecode_packethas_dreg;
        decode2fpudecode_packetis_prefetch <= decode2fpudecode_packetis_prefetch;
        decode2fpudecode_packetsprf_dest <= decode2fpudecode_packetsprf_dest;
        decode2fpudecode_packetsprf_src <= decode2fpudecode_packetsprf_src;
        decode2fpudecode_packetis_atomic <= decode2fpudecode_packetis_atomic;
        decode2fpudecode_packetis_ldl <= decode2fpudecode_packetis_ldl;
        decode2fpudecode_packetis_stl <= decode2fpudecode_packetis_stl;
        decode2fpudecode_packetis_stc <= decode2fpudecode_packetis_stc;
        decode2fpudecode_packetis_valid <= decode2fpudecode_packetis_valid;
        decode2fpudecode_packetis_halt <= decode2fpudecode_packetis_halt;
        decode2fpudecode_packetis_compare <= decode2fpudecode_packetis_compare;
        decode2fpudecode_packetdreg_is_src <= decode2fpudecode_packetdreg_is_src;
        decode2fpudecode_packetdreg_is_dest <= decode2fpudecode_packetdreg_is_dest;
		//decode2mem <= decode2mem;
        decode2mempc <= decode2mempc;
        decode2mempc_incr <= decode2mempc_incr;
        decode2memthread_num <= decode2memthread_num;
        decode2membranch_taken <= decode2membranch_taken;
        decode2mempredicted_pc <= decode2mempredicted_pc;
        decode2memvalid <= decode2memvalid;
        decode2memis_oldest <= decode2memis_oldest;
        decode2memsreg_t_data <= decode2memsreg_t_data;
        decode2memsreg_s_data <= decode2memsreg_s_data;
        decode2memresult <= decode2memresult;
        decode2memL1D_hit <= decode2memL1D_hit;
        decode2memdecode_packetsreg_t <= decode2memdecode_packetsreg_t;
        decode2memdecode_packetsreg_s <= decode2memdecode_packetsreg_s;
        decode2memdecode_packetdreg <= decode2memdecode_packetdreg;
        decode2memdecode_packetacc <= decode2memdecode_packetacc;
        decode2memdecode_packetimm16_value <= decode2memdecode_packetimm16_value;
        decode2memdecode_packetimm26_value <= decode2memdecode_packetimm26_value;
        decode2memdecode_packetimm5_value <= decode2memdecode_packetimm5_value;
        decode2memdecode_packetop_FU_type <= decode2memdecode_packetop_FU_type;
        decode2memdecode_packetalu_op <= decode2memdecode_packetalu_op;
        decode2memdecode_packetfpu_op <= decode2memdecode_packetfpu_op;
        decode2memdecode_packetop_MEM_type <= decode2memdecode_packetop_MEM_type;
        decode2memdecode_packetop_BR_type <= decode2memdecode_packetop_BR_type;
        decode2memdecode_packetshift_type <= decode2memdecode_packetshift_type;
        decode2memdecode_packetcompare_type <= decode2memdecode_packetcompare_type;
        decode2memdecode_packetis_imm26 <= decode2memdecode_packetis_imm26;
        decode2memdecode_packetis_imm <= decode2memdecode_packetis_imm;
        decode2memdecode_packetis_imm5 <= decode2memdecode_packetis_imm5;
        decode2memdecode_packetis_signed <= decode2memdecode_packetis_signed;
        decode2memdecode_packetcarry_in <= decode2memdecode_packetcarry_in;
        decode2memdecode_packetcarry_out <= decode2memdecode_packetcarry_out;
        decode2memdecode_packethas_sreg_t <= decode2memdecode_packethas_sreg_t;
        decode2memdecode_packethas_sreg_s <= decode2memdecode_packethas_sreg_s;
        decode2memdecode_packethas_dreg <= decode2memdecode_packethas_dreg;
        decode2memdecode_packetis_prefetch <= decode2memdecode_packetis_prefetch;
        decode2memdecode_packetsprf_dest <= decode2memdecode_packetsprf_dest;
        decode2memdecode_packetsprf_src <= decode2memdecode_packetsprf_src;
        decode2memdecode_packetis_atomic <= decode2memdecode_packetis_atomic;
        decode2memdecode_packetis_ldl <= decode2memdecode_packetis_ldl;
        decode2memdecode_packetis_stl <= decode2memdecode_packetis_stl;
        decode2memdecode_packetis_stc <= decode2memdecode_packetis_stc;
        decode2memdecode_packetis_valid <= decode2memdecode_packetis_valid;
        decode2memdecode_packetis_halt <= decode2memdecode_packetis_halt;
        decode2memdecode_packetis_compare <= decode2memdecode_packetis_compare;
        decode2memdecode_packetdreg_is_src <= decode2memdecode_packetdreg_is_src;
        decode2memdecode_packetdreg_is_dest <= decode2memdecode_packetdreg_is_dest;
        instr0_issued <= instr0_issued;
      end else begin
      // Valid signals indicate whether a valid instruction has actually been 
      // issued into the pipe
      // TODO: as a temp fix I invalidate instruction in slot1 if a branch inst
      // is in slot0
      decode2intvalid <= valid_int && !issue_halt && !branch_mispredict && fetch2decodevalid;
      // The next two lines disable a pipe for a co-issued instruction when
      // a branch exists in slot 0
      decode2fpuvalid <= valid_fpu && !issue_halt && !branch_mispredict && fetch2decodevalid &&
                          !follows_vld_branch;
      decode2memvalid <= valid_mem && !issue_halt && !branch_mispredict && fetch2decodevalid &&
                          !follows_vld_branch;
      
      // Branches are handled inside the INT pipe
      decode2intbranch_taken <= fetch2decodebranch_taken;
      decode2intpredicted_pc <= fetch2decodepredicted_pc;

      //////////////////////////////////////////////////////////////////////////
      // INT Pipeline Dispatch
      //////////////////////////////////////////////////////////////////////////
      // Select which slot gets issued to the INT pipe  Slot 0 takes priority
      //////////////////////////////////////////////////////////////////////////
      if( choice_int) begin                           // Issue from slot 1
          //decode2intdecode_packet <= decode_packet1;
          decode2intdecode_packetsreg_t <= decode_packet1sreg_t;
          decode2intdecode_packetsreg_s <= decode_packet1sreg_s;
          decode2intdecode_packetdreg <= decode_packet1dreg;
          decode2intdecode_packetacc <= decode_packet1acc;
          decode2intdecode_packetimm16_value <= decode_packet1imm16_value;
          decode2intdecode_packetimm26_value <= decode_packet1imm26_value;
          decode2intdecode_packetimm5_value <= decode_packet1imm5_value;
          decode2intdecode_packetop_FU_type <= decode_packet1op_FU_type;
          decode2intdecode_packetalu_op <= decode_packet1alu_op;
          decode2intdecode_packetfpu_op <= decode_packet1fpu_op;
          decode2intdecode_packetop_MEM_type <= decode_packet1op_MEM_type;
          decode2intdecode_packetop_BR_type <= decode_packet1op_BR_type;
          decode2intdecode_packetshift_type <= decode_packet1shift_type;
          decode2intdecode_packetcompare_type <= decode_packet1compare_type;
          decode2intdecode_packetis_imm26 <= decode_packet1is_imm26;
          decode2intdecode_packetis_imm <= decode_packet1is_imm;
          decode2intdecode_packetis_imm5 <= decode_packet1is_imm5;
          decode2intdecode_packetis_signed <= decode_packet1is_signed;
          decode2intdecode_packetcarry_in <= decode_packet1carry_in;
          decode2intdecode_packetcarry_out <= decode_packet1carry_out;
          decode2intdecode_packethas_sreg_t <= decode_packet1has_sreg_t;
          decode2intdecode_packethas_sreg_s <= decode_packet1has_sreg_s;
          decode2intdecode_packethas_dreg <= decode_packet1has_dreg;
          decode2intdecode_packetis_prefetch <= decode_packet1is_prefetch;
          decode2intdecode_packetsprf_dest <= decode_packet1sprf_dest;
          decode2intdecode_packetsprf_src <= decode_packet1sprf_src;
          decode2intdecode_packetis_atomic <= decode_packet1is_atomic;
          decode2intdecode_packetis_ldl <= decode_packet1is_ldl;
          decode2intdecode_packetis_stl <= decode_packet1is_stl;
          decode2intdecode_packetis_stc <= decode_packet1is_stc;
          decode2intdecode_packetis_valid <= decode_packet1is_valid;
          decode2intdecode_packetis_halt <= decode_packet1is_halt;
          decode2intdecode_packetis_compare <= decode_packet1is_compare;
          decode2intdecode_packetdreg_is_src <= decode_packet1dreg_is_src;
          decode2intdecode_packetdreg_is_dest <= decode_packet1dreg_is_dest;
          if (sprf_src) begin
            decode2intsreg_t_data <= sprf_sreg_data;
          end else begin
            decode2intsreg_t_data <= sreg_t_1;
          end
          decode2intsreg_s_data <= sreg_s_1;
          decode2intpc          <= fetch2decodepc1;
          decode2intpc_incr     <= fetch2decodepc1_incr;
          decode2intis_oldest   <= !issue0;
      end else begin                                 // Issue from slot 0
          // decode2intdecode_packet <= decode_packet0;
          decode2intdecode_packetsreg_t <= decode_packet0sreg_t;
          decode2intdecode_packetsreg_s <= decode_packet0sreg_s;
          decode2intdecode_packetdreg <= decode_packet0dreg;
          decode2intdecode_packetacc <= decode_packet0acc;
          decode2intdecode_packetimm16_value <= decode_packet0imm16_value;
          decode2intdecode_packetimm26_value <= decode_packet0imm26_value;
          decode2intdecode_packetimm5_value <= decode_packet0imm5_value;
          decode2intdecode_packetop_FU_type <= decode_packet0op_FU_type;
          decode2intdecode_packetalu_op <= decode_packet0alu_op;
          decode2intdecode_packetfpu_op <= decode_packet0fpu_op;
          decode2intdecode_packetop_MEM_type <= decode_packet0op_MEM_type;
          decode2intdecode_packetop_BR_type <= decode_packet0op_BR_type;
          decode2intdecode_packetshift_type <= decode_packet0shift_type;
          decode2intdecode_packetcompare_type <= decode_packet0compare_type;
          decode2intdecode_packetis_imm26 <= decode_packet0is_imm26;
          decode2intdecode_packetis_imm <= decode_packet0is_imm;
          decode2intdecode_packetis_imm5 <= decode_packet0is_imm5;
          decode2intdecode_packetis_signed <= decode_packet0is_signed;
          decode2intdecode_packetcarry_in <= decode_packet0carry_in;
          decode2intdecode_packetcarry_out <= decode_packet0carry_out;
          decode2intdecode_packethas_sreg_t <= decode_packet0has_sreg_t;
          decode2intdecode_packethas_sreg_s <= decode_packet0has_sreg_s;
          decode2intdecode_packethas_dreg <= decode_packet0has_dreg;
          decode2intdecode_packetis_prefetch <= decode_packet0is_prefetch;
          decode2intdecode_packetsprf_dest <= decode_packet0sprf_dest;
          decode2intdecode_packetsprf_src <= decode_packet0sprf_src;
          decode2intdecode_packetis_atomic <= decode_packet0is_atomic;
          decode2intdecode_packetis_ldl <= decode_packet0is_ldl;
          decode2intdecode_packetis_stl <= decode_packet0is_stl;
          decode2intdecode_packetis_stc <= decode_packet0is_stc;
          decode2intdecode_packetis_valid <= decode_packet0is_valid;
          decode2intdecode_packetis_halt <= decode_packet0is_halt;
          decode2intdecode_packetis_compare <= decode_packet0is_compare;
          decode2intdecode_packetdreg_is_src <= decode_packet0dreg_is_src;
          decode2intdecode_packetdreg_is_dest <= decode_packet0dreg_is_dest;
          if (sprf_src) begin
            decode2intsreg_t_data <= sprf_sreg_data;
          end else begin
            decode2intsreg_t_data <= sreg_t_0;
          end
          decode2intsreg_s_data <= sreg_s_0;
          decode2intpc          <= fetch2decodepc0;
          decode2intpc_incr     <= fetch2decodepc0_incr;
          decode2intis_oldest   <= 1'b1;
      end
      //////////////////////////////////////////////////////////////////////////
     
      //////////////////////////////////////////////////////////////////////////
      // FPU Pipeline Dispatch
      //////////////////////////////////////////////////////////////////////////
      // Select which slot gets issued to the FPU pipe  Slot 0 takes priority
      //////////////////////////////////////////////////////////////////////////
      if( choice_fpu) begin                         // Issue from slot 1
          //decode2fpudecode_packet <= decode_packet1;
          decode2fpudecode_packetsreg_t <= decode_packet1sreg_t;
          decode2fpudecode_packetsreg_s <= decode_packet1sreg_s;
          decode2fpudecode_packetdreg <= decode_packet1dreg;
          decode2fpudecode_packetacc <= decode_packet1acc;
          decode2fpudecode_packetimm16_value <= decode_packet1imm16_value;
          decode2fpudecode_packetimm26_value <= decode_packet1imm26_value;
          decode2fpudecode_packetimm5_value <= decode_packet1imm5_value;
          decode2fpudecode_packetop_FU_type <= decode_packet1op_FU_type;
          decode2fpudecode_packetalu_op <= decode_packet1alu_op;
          decode2fpudecode_packetfpu_op <= decode_packet1fpu_op;
          decode2fpudecode_packetop_MEM_type <= decode_packet1op_MEM_type;
          decode2fpudecode_packetop_BR_type <= decode_packet1op_BR_type;
          decode2fpudecode_packetshift_type <= decode_packet1shift_type;
          decode2fpudecode_packetcompare_type <= decode_packet1compare_type;
          decode2fpudecode_packetis_imm26 <= decode_packet1is_imm26;
          decode2fpudecode_packetis_imm <= decode_packet1is_imm;
          decode2fpudecode_packetis_imm5 <= decode_packet1is_imm5;
          decode2fpudecode_packetis_signed <= decode_packet1is_signed;
          decode2fpudecode_packetcarry_in <= decode_packet1carry_in;
          decode2fpudecode_packetcarry_out <= decode_packet1carry_out;
          decode2fpudecode_packethas_sreg_t <= decode_packet1has_sreg_t;
          decode2fpudecode_packethas_sreg_s <= decode_packet1has_sreg_s;
          decode2fpudecode_packethas_dreg <= decode_packet1has_dreg;
          decode2fpudecode_packetis_prefetch <= decode_packet1is_prefetch;
          decode2fpudecode_packetsprf_dest <= decode_packet1sprf_dest;
          decode2fpudecode_packetsprf_src <= decode_packet1sprf_src;
          decode2fpudecode_packetis_atomic <= decode_packet1is_atomic;
          decode2fpudecode_packetis_ldl <= decode_packet1is_ldl;
          decode2fpudecode_packetis_stl <= decode_packet1is_stl;
          decode2fpudecode_packetis_stc <= decode_packet1is_stc;
          decode2fpudecode_packetis_valid <= decode_packet1is_valid;
          decode2fpudecode_packetis_halt <= decode_packet1is_halt;
          decode2fpudecode_packetis_compare <= decode_packet1is_compare;
          decode2fpudecode_packetdreg_is_src <= decode_packet1dreg_is_src;
          decode2fpudecode_packetdreg_is_dest <= decode_packet1dreg_is_dest;
          decode2fpusreg_t_data <= sreg_t_1;
          decode2fpusreg_s_data <= sreg_s_1;
          decode2fpupc          <= fetch2decodepc1;
          decode2fpupc_incr     <= fetch2decodepc1_incr;
          decode2fpuis_oldest   <= !issue0;
      end else begin                                // Issue from slot 0
          //decode2fpudecode_packet <= decode_packet0;
          decode2fpudecode_packetsreg_t <= decode_packet0sreg_t;
          decode2fpudecode_packetsreg_s <= decode_packet0sreg_s;
          decode2fpudecode_packetdreg <= decode_packet0dreg;
          decode2fpudecode_packetacc <= decode_packet0acc;
          decode2fpudecode_packetimm16_value <= decode_packet0imm16_value;
          decode2fpudecode_packetimm26_value <= decode_packet0imm26_value;
          decode2fpudecode_packetimm5_value <= decode_packet0imm5_value;
          decode2fpudecode_packetop_FU_type <= decode_packet0op_FU_type;
          decode2fpudecode_packetalu_op <= decode_packet0alu_op;
          decode2fpudecode_packetfpu_op <= decode_packet0fpu_op;
          decode2fpudecode_packetop_MEM_type <= decode_packet0op_MEM_type;
          decode2fpudecode_packetop_BR_type <= decode_packet0op_BR_type;
          decode2fpudecode_packetshift_type <= decode_packet0shift_type;
          decode2fpudecode_packetcompare_type <= decode_packet0compare_type;
          decode2fpudecode_packetis_imm26 <= decode_packet0is_imm26;
          decode2fpudecode_packetis_imm <= decode_packet0is_imm;
          decode2fpudecode_packetis_imm5 <= decode_packet0is_imm5;
          decode2fpudecode_packetis_signed <= decode_packet0is_signed;
          decode2fpudecode_packetcarry_in <= decode_packet0carry_in;
          decode2fpudecode_packetcarry_out <= decode_packet0carry_out;
          decode2fpudecode_packethas_sreg_t <= decode_packet0has_sreg_t;
          decode2fpudecode_packethas_sreg_s <= decode_packet0has_sreg_s;
          decode2fpudecode_packethas_dreg <= decode_packet0has_dreg;
          decode2fpudecode_packetis_prefetch <= decode_packet0is_prefetch;
          decode2fpudecode_packetsprf_dest <= decode_packet0sprf_dest;
          decode2fpudecode_packetsprf_src <= decode_packet0sprf_src;
          decode2fpudecode_packetis_atomic <= decode_packet0is_atomic;
          decode2fpudecode_packetis_ldl <= decode_packet0is_ldl;
          decode2fpudecode_packetis_stl <= decode_packet0is_stl;
          decode2fpudecode_packetis_stc <= decode_packet0is_stc;
          decode2fpudecode_packetis_valid <= decode_packet0is_valid;
          decode2fpudecode_packetis_halt <= decode_packet0is_halt;
          decode2fpudecode_packetis_compare <= decode_packet0is_compare;
          decode2fpudecode_packetdreg_is_src <= decode_packet0dreg_is_src;
          decode2fpudecode_packetdreg_is_dest <= decode_packet0dreg_is_dest;
          decode2fpusreg_t_data <= sreg_t_0;
          decode2fpusreg_s_data <= sreg_s_0;
          decode2fpupc          <= fetch2decodepc0;
          decode2fpupc_incr     <= fetch2decodepc0_incr;
          decode2fpuis_oldest   <= 1'b1;
      end
      //////////////////////////////////////////////////////////////////////////
 
      //////////////////////////////////////////////////////////////////////////
      // MEM Pipeline Dispatch
      //////////////////////////////////////////////////////////////////////////
      // Select which slot gets issued to the MEM pipe  Slot 0 takes priority
      //////////////////////////////////////////////////////////////////////////
      if( choice_mem) begin                         // Issue from slot 1
          //decode2memdecode_packet <= decode_packet1;
          decode2memdecode_packetsreg_t <= decode_packet1sreg_t;
          decode2memdecode_packetsreg_s <= decode_packet1sreg_s;
          decode2memdecode_packetdreg <= decode_packet1dreg;
          decode2memdecode_packetacc <= decode_packet1acc;
          decode2memdecode_packetimm16_value <= decode_packet1imm16_value;
          decode2memdecode_packetimm26_value <= decode_packet1imm26_value;
          decode2memdecode_packetimm5_value <= decode_packet1imm5_value;
          decode2memdecode_packetop_FU_type <= decode_packet1op_FU_type;
          decode2memdecode_packetalu_op <= decode_packet1alu_op;
          decode2memdecode_packetfpu_op <= decode_packet1fpu_op;
          decode2memdecode_packetop_MEM_type <= decode_packet1op_MEM_type;
          decode2memdecode_packetop_BR_type <= decode_packet1op_BR_type;
          decode2memdecode_packetshift_type <= decode_packet1shift_type;
          decode2memdecode_packetcompare_type <= decode_packet1compare_type;
          decode2memdecode_packetis_imm26 <= decode_packet1is_imm26;
          decode2memdecode_packetis_imm <= decode_packet1is_imm;
          decode2memdecode_packetis_imm5 <= decode_packet1is_imm5;
          decode2memdecode_packetis_signed <= decode_packet1is_signed;
          decode2memdecode_packetcarry_in <= decode_packet1carry_in;
          decode2memdecode_packetcarry_out <= decode_packet1carry_out;
          decode2memdecode_packethas_sreg_t <= decode_packet1has_sreg_t;
          decode2memdecode_packethas_sreg_s <= decode_packet1has_sreg_s;
          decode2memdecode_packethas_dreg <= decode_packet1has_dreg;
          decode2memdecode_packetis_prefetch <= decode_packet1is_prefetch;
          decode2memdecode_packetsprf_dest <= decode_packet1sprf_dest;
          decode2memdecode_packetsprf_src <= decode_packet1sprf_src;
          decode2memdecode_packetis_atomic <= decode_packet1is_atomic;
          decode2memdecode_packetis_ldl <= decode_packet1is_ldl;
          decode2memdecode_packetis_stl <= decode_packet1is_stl;
          decode2memdecode_packetis_stc <= decode_packet1is_stc;
          decode2memdecode_packetis_valid <= decode_packet1is_valid;
          decode2memdecode_packetis_halt <= decode_packet1is_halt;
          decode2memdecode_packetis_compare <= decode_packet1is_compare;
          decode2memdecode_packetdreg_is_src <= decode_packet1dreg_is_src;
          decode2memdecode_packetdreg_is_dest <= decode_packet1dreg_is_dest;
          decode2memsreg_t_data <= sreg_t_1;
          decode2memsreg_s_data <= sreg_s_1;
          decode2mempc          <= fetch2decodepc1;
          decode2mempc_incr     <= fetch2decodepc1_incr;
          decode2memis_oldest   <= !issue0;
      end else begin                                // Issue from slot 0
          //decode2memdecode_packet <= decode_packet0;
          decode2memdecode_packetsreg_t <= decode_packet0sreg_t;
          decode2memdecode_packetsreg_s <= decode_packet0sreg_s;
          decode2memdecode_packetdreg <= decode_packet0dreg;
          decode2memdecode_packetacc <= decode_packet0acc;
          decode2memdecode_packetimm16_value <= decode_packet0imm16_value;
          decode2memdecode_packetimm26_value <= decode_packet0imm26_value;
          decode2memdecode_packetimm5_value <= decode_packet0imm5_value;
          decode2memdecode_packetop_FU_type <= decode_packet0op_FU_type;
          decode2memdecode_packetalu_op <= decode_packet0alu_op;
          decode2memdecode_packetfpu_op <= decode_packet0fpu_op;
          decode2memdecode_packetop_MEM_type <= decode_packet0op_MEM_type;
          decode2memdecode_packetop_BR_type <= decode_packet0op_BR_type;
          decode2memdecode_packetshift_type <= decode_packet0shift_type;
          decode2memdecode_packetcompare_type <= decode_packet0compare_type;
          decode2memdecode_packetis_imm26 <= decode_packet0is_imm26;
          decode2memdecode_packetis_imm <= decode_packet0is_imm;
          decode2memdecode_packetis_imm5 <= decode_packet0is_imm5;
          decode2memdecode_packetis_signed <= decode_packet0is_signed;
          decode2memdecode_packetcarry_in <= decode_packet0carry_in;
          decode2memdecode_packetcarry_out <= decode_packet0carry_out;
          decode2memdecode_packethas_sreg_t <= decode_packet0has_sreg_t;
          decode2memdecode_packethas_sreg_s <= decode_packet0has_sreg_s;
          decode2memdecode_packethas_dreg <= decode_packet0has_dreg;
          decode2memdecode_packetis_prefetch <= decode_packet0is_prefetch;
          decode2memdecode_packetsprf_dest <= decode_packet0sprf_dest;
          decode2memdecode_packetsprf_src <= decode_packet0sprf_src;
          decode2memdecode_packetis_atomic <= decode_packet0is_atomic;
          decode2memdecode_packetis_ldl <= decode_packet0is_ldl;
          decode2memdecode_packetis_stl <= decode_packet0is_stl;
          decode2memdecode_packetis_stc <= decode_packet0is_stc;
          decode2memdecode_packetis_valid <= decode_packet0is_valid;
          decode2memdecode_packetis_halt <= decode_packet0is_halt;
          decode2memdecode_packetis_compare <= decode_packet0is_compare;
          decode2memdecode_packetdreg_is_src <= decode_packet0dreg_is_src;
          decode2memdecode_packetdreg_is_dest <= decode_packet0dreg_is_dest;
          decode2memsreg_t_data <= sreg_t_0;
          decode2memsreg_s_data <= sreg_s_0;
          decode2mempc          <= fetch2decodepc0;
          decode2mempc_incr     <= fetch2decodepc0_incr;
          decode2memis_oldest   <= 1'b1;
      end
      //////////////////////////////////////////////////////////////////////////

      // Propagate the HALT signal if needed
      decode2intdecode_packetis_halt <= issue_halt && !branch_mispredict;
      decode2memdecode_packetis_halt <= issue_halt && !branch_mispredict;
      decode2fpudecode_packetis_halt <= issue_halt && !branch_mispredict;

      // Indicate whether both instructions have issued
      // We only want to indicate it was actually issued if we're not recovering 
      // from a branch misprediction, and if it was issued already or if it's
      // free to issue due to operand availability, and resource availability 
      if( !issue1 ) begin
        instr0_issued <= !branch_mispredict && (instr0_issued || issue0);
      end
      else begin
        instr0_issued <= 1'b0;
      end
    end
    end
  end
  //`include "assertions/decode_stage3283_forest4_10k.assert"
/*
  always begin
    assert can_not_issue3 : (!(decode2intvalid == 1 && decode2fpuvalid == 1 && decode2memvalid == 1));
  end
*/
/*
  always begin
    if ((decode2int.valid == 1 && decode2fpu.valid == 0 && decode2mem.valid == 0) ||
    (decode2int.valid == 0 && decode2fpu.valid == 1 && decode2mem.valid == 0) ||
    (decode2int.valid == 0 && decode2fpu.valid == 0 && decode2mem.valid == 1))
    wait(2);
    if ((decode2int.valid == 1 && decode2fpu.valid == 0 && decode2mem.valid == 0) ||
    (decode2int.valid == 0 && decode2fpu.valid == 1 && decode2mem.valid == 0) ||
    (decode2int.valid == 0 && decode2fpu.valid == 0 && decode2mem.valid == 1))
*/

  //////////////////////////////////////////////////////////////////////////////
endmodule
