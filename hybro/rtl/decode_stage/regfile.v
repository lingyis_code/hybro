////////////////////////////////////////////////////////////////////////////////
// register file
// Converted from revision 3283
////////////////////////////////////////////////////////////////////////////////

// simple register file model
// 4 read port, 2 write port
// reset
// read and write enables

////////////////////////////////////////////////////////////////////////////////
module rf_4r2w(
  // control signals
  clk,
  reset,
  enable,
  // read port signals
  read_addr_0,
  read_data_0,
  read_enable_0,
  read_addr_1,
  read_data_1,
  read_enable_1,
  read_addr_2,
  read_data_2,
  read_enable_2,
  read_addr_3,
  read_data_3,
  read_enable_3,
  // write port signals
  write_addr_0,
  write_data_0,
  write_enable_0,
  write_addr_1,
  write_data_1,
  write_enable_1
);

  // control signals
  input  clk;
  input  reset;
  input  enable;
  // read port signals
  input [4:0]   read_addr_0;
  output [31:0] read_data_0;
  input         read_enable_0;
  input [4:0]   read_addr_1;
  output [31:0] read_data_1;
  input         read_enable_1;
  input [4:0]   read_addr_2;
  output [31:0] read_data_2;
  input         read_enable_2;
  input [4:0]   read_addr_3;
  output [31:0] read_data_3;
  input         read_enable_3;
  // write port signals
  input [4:0]   write_addr_0;
  input [31:0]  write_data_0;
  input         write_enable_0;
  input [4:0]   write_addr_1;
  input [31:0]  write_data_1;
  input         write_enable_1;
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // local wires and regs
  //////////////////////////////////////////////////////////////////////////////
  reg[31:0] rf_0; 
  reg[31:0] rf_1; 
  reg[31:0] rf_2; 
  reg[31:0] rf_3; 
  reg[31:0] rf_4; 
  reg[31:0] rf_5; 
  reg[31:0] rf_6; 
  reg[31:0] rf_7; 
  reg[31:0] rf_8; 
  reg[31:0] rf_9; 
  reg[31:0] rf_10; 
  reg[31:0] rf_11; 
  reg[31:0] rf_12; 
  reg[31:0] rf_13; 
  reg[31:0] rf_14; 
  reg[31:0] rf_15; 
  reg[31:0] rf_16; 
  reg[31:0] rf_17; 
  reg[31:0] rf_18; 
  reg[31:0] rf_19; 
  reg[31:0] rf_20; 
  reg[31:0] rf_21; 
  reg[31:0] rf_22; 
  reg[31:0] rf_23; 
  reg[31:0] rf_24; 
  reg[31:0] rf_25; 
  reg[31:0] rf_26; 
  reg[31:0] rf_27; 
  reg[31:0] rf_28; 
  reg[31:0] rf_29; 
  reg[31:0] rf_30; 
  reg[31:0] rf_31; 

  wire we_0;
  wire we_1;
  //////////////////////////////////////////////////////////////////////////////
  // Continuous assignments
  //////////////////////////////////////////////////////////////////////////////
  //assign read_data_0 = (read_enable_0 && (read_addr_0 != 5'b0))? rf[read_addr_0] : 32'b0;
  always@(*)
  begin
    if((read_enable_0 && (read_addr_0 != 5'b0))==0)
      read_data_0 = 32'h0;
    else
     case(read_addr_0)
       5'h0: read_data_0 = rf_0;
       5'h1: read_data_0 = rf_1;
       5'h2: read_data_0 = rf_2;
       5'h3: read_data_0 = rf_3;
       5'h4: read_data_0 = rf_4;
       5'h5: read_data_0 = rf_5;
       5'h6: read_data_0 = rf_6;
       5'h7: read_data_0 = rf_7;
       5'h8: read_data_0 = rf_8;
       5'h9: read_data_0 = rf_9;
       5'h10: read_data_0 = rf_10;
       5'h11: read_data_0 = rf_11;
       5'h12: read_data_0 = rf_12;
       5'h13: read_data_0 = rf_13;
       5'h14: read_data_0 = rf_14;
       5'h15: read_data_0 = rf_15;
       5'h16: read_data_0 = rf_16;
       5'h17: read_data_0 = rf_17;
       5'h18: read_data_0 = rf_18;
       5'h19: read_data_0 = rf_19;
       5'h20: read_data_0 = rf_20;
       5'h21: read_data_0 = rf_21;
       5'h22: read_data_0 = rf_22;
       5'h23: read_data_0 = rf_23;
       5'h24: read_data_0 = rf_24;
       5'h25: read_data_0 = rf_25;
       5'h26: read_data_0 = rf_26;
       5'h27: read_data_0 = rf_27;
       5'h28: read_data_0 = rf_28;
       5'h29: read_data_0 = rf_29;
       5'h30: read_data_0 = rf_30;
       5'h31: read_data_0 = rf_31;
     endcase
  end
 // assign read_data_1 = (read_enable_1 && (read_addr_1 != 5'b0))? rf[read_addr_1] : 32'b0;
always@(*)
  begin
    if((read_enable_1 && (read_addr_1 != 5'b0))==0)
      read_data_1 = 32'h0;
    else
     case(read_addr_1)
       5'h0: read_data_1 = rf_0;
       5'h1: read_data_1 = rf_1;
       5'h2: read_data_1 = rf_2;
       5'h3: read_data_1 = rf_3;
       5'h4: read_data_1 = rf_4;
       5'h5: read_data_1 = rf_5;
       5'h6: read_data_1 = rf_6;
       5'h7: read_data_1 = rf_7;
       5'h8: read_data_1 = rf_8;
       5'h9: read_data_1 = rf_9;
       5'h10: read_data_1 = rf_10;
       5'h11: read_data_1 = rf_11;
       5'h12: read_data_1 = rf_12;
       5'h13: read_data_1 = rf_13;
       5'h14: read_data_1 = rf_14;
       5'h15: read_data_1 = rf_15;
       5'h16: read_data_1 = rf_16;
       5'h17: read_data_1 = rf_17;
       5'h18: read_data_1 = rf_18;
       5'h19: read_data_1 = rf_19;
       5'h20: read_data_1 = rf_20;
       5'h21: read_data_1 = rf_21;
       5'h22: read_data_1 = rf_22;
       5'h23: read_data_1 = rf_23;
       5'h24: read_data_1 = rf_24;
       5'h25: read_data_1 = rf_25;
       5'h26: read_data_1 = rf_26;
       5'h27: read_data_1 = rf_27;
       5'h28: read_data_1 = rf_28;
       5'h29: read_data_1 = rf_29;
       5'h30: read_data_1 = rf_30;
       5'h31: read_data_1 = rf_31;
     endcase
  end

  //assign read_data_2 = (read_enable_2 && (read_addr_2 != 5'b0))? rf[read_addr_2] : 32'b0;
always@(*)
  begin
    if((read_enable_2 && (read_addr_2 != 5'b0))==0)
      read_data_2 = 32'h0;
    else
     case(read_addr_2)
       5'h0: read_data_2 = rf_0;
       5'h1: read_data_2 = rf_1;
       5'h2: read_data_2 = rf_2;
       5'h3: read_data_2 = rf_3;
       5'h4: read_data_2 = rf_4;
       5'h5: read_data_2 = rf_5;
       5'h6: read_data_2 = rf_6;
       5'h7: read_data_2 = rf_7;
       5'h8: read_data_2 = rf_8;
       5'h9: read_data_2 = rf_9;
       5'h10: read_data_2 = rf_10;
       5'h11: read_data_2 = rf_11;
       5'h12: read_data_2 = rf_12;
       5'h13: read_data_2 = rf_13;
       5'h14: read_data_2 = rf_14;
       5'h15: read_data_2 = rf_15;
       5'h16: read_data_2 = rf_16;
       5'h17: read_data_2 = rf_17;
       5'h18: read_data_2 = rf_18;
       5'h19: read_data_2 = rf_19;
       5'h20: read_data_2 = rf_20;
       5'h21: read_data_2 = rf_21;
       5'h22: read_data_2 = rf_22;
       5'h23: read_data_2 = rf_23;
       5'h24: read_data_2 = rf_24;
       5'h25: read_data_2 = rf_25;
       5'h26: read_data_2 = rf_26;
       5'h27: read_data_2 = rf_27;
       5'h28: read_data_2 = rf_28;
       5'h29: read_data_2 = rf_29;
       5'h30: read_data_2 = rf_30;
       5'h31: read_data_2 = rf_31;
     endcase
  end

  //assign read_data_3 = (read_enable_3 && (read_addr_3 != 5'b0))? rf[read_addr_3] : 32'b0;
always@(*)
  begin
    if((read_enable_3 && (read_addr_3 != 5'b0))==0)
      read_data_3 = 32'h0;
    else
     case(read_addr_3)
       5'h0: read_data_3 = rf_0;
       5'h1: read_data_3 = rf_1;
       5'h2: read_data_3 = rf_2;
       5'h3: read_data_3 = rf_3;
       5'h4: read_data_3 = rf_4;
       5'h5: read_data_3 = rf_5;
       5'h6: read_data_3 = rf_6;
       5'h7: read_data_3 = rf_7;
       5'h8: read_data_3 = rf_8;
       5'h9: read_data_3 = rf_9;
       5'h10: read_data_3 = rf_10;
       5'h11: read_data_3 = rf_11;
       5'h12: read_data_3 = rf_12;
       5'h13: read_data_3 = rf_13;
       5'h14: read_data_3 = rf_14;
       5'h15: read_data_3 = rf_15;
       5'h16: read_data_3 = rf_16;
       5'h17: read_data_3 = rf_17;
       5'h18: read_data_3 = rf_18;
       5'h19: read_data_3 = rf_19;
       5'h20: read_data_3 = rf_20;
       5'h21: read_data_3 = rf_21;
       5'h22: read_data_3 = rf_22;
       5'h23: read_data_3 = rf_23;
       5'h24: read_data_3 = rf_24;
       5'h25: read_data_3 = rf_25;
       5'h26: read_data_3 = rf_26;
       5'h27: read_data_3 = rf_27;
       5'h28: read_data_3 = rf_28;
       5'h29: read_data_3 = rf_29;
       5'h30: read_data_3 = rf_30;
       5'h31: read_data_3 = rf_31;
     endcase
  end

  assign we_0 = write_enable_0 && !( write_addr_0 == 5'b0);
  assign we_1 = write_enable_1 && !( write_addr_1 == 5'b0);
  always @ (posedge clk or posedge reset)
  begin
    if( reset == 1 ) begin
      //for(i=0;i<32;i=i+1) begin
      //  rf[i] <= 32'b0;    
      //end
      rf_0 <= 32'h0;
      rf_1 <= 32'h0;
      rf_2 <= 32'h0;
      rf_3 <= 32'h0;
      rf_4 <= 32'h0;
      rf_5 <= 32'h0;
      rf_6 <= 32'h0;
      rf_7 <= 32'h0;
      rf_8 <= 32'h0;
      rf_9 <= 32'h0;
      rf_10 <= 32'h0;
      rf_11 <= 32'h0;
      rf_12 <= 32'h0;
      rf_13 <= 32'h0;
      rf_14 <= 32'h0;
      rf_15 <= 32'h0;
      rf_16 <= 32'h0;
      rf_17 <= 32'h0;
      rf_18 <= 32'h0;
      rf_19 <= 32'h0;
      rf_20 <= 32'h0;
      rf_21 <= 32'h0;
      rf_22 <= 32'h0;
      rf_23 <= 32'h0;
      rf_24 <= 32'h0;
      rf_25 <= 32'h0;
      rf_26 <= 32'h0;
      rf_27 <= 32'h0;
      rf_28 <= 32'h0;
      rf_29 <= 32'h0;
      rf_30 <= 32'h0;
      rf_31 <= 32'h0;

    end

    // regular clocked operation
    else begin

      // write port 0
      if( we_0 )
        begin
        //rf[write_addr_0] <= write_data_0;
        case(write_addr_0)
          5'h0: rf_0 <= write_data_0;
          5'h1: rf_1 <= write_data_0;
          5'h2: rf_2 <= write_data_0;
          5'h3: rf_3 <= write_data_0;
          5'h4: rf_4 <= write_data_0;
          5'h5: rf_5 <= write_data_0;
          5'h6: rf_6 <= write_data_0;
          5'h7: rf_7 <= write_data_0;
          5'h8: rf_8 <= write_data_0;
          5'h9: rf_9 <= write_data_0;
          5'h10: rf_10 <= write_data_0;
          5'h11: rf_11 <= write_data_0;
          5'h12: rf_12 <= write_data_0;
          5'h13: rf_13 <= write_data_0;
          5'h14: rf_14 <= write_data_0;
          5'h15: rf_15 <= write_data_0;
          5'h16: rf_16 <= write_data_0;
          5'h17: rf_17 <= write_data_0;
          5'h18: rf_18 <= write_data_0;
          5'h19: rf_19 <= write_data_0;
          5'h20: rf_20 <= write_data_0;
          5'h21: rf_21 <= write_data_0;
          5'h22: rf_22 <= write_data_0;
          5'h23: rf_23 <= write_data_0;
          5'h24: rf_24 <= write_data_0;
          5'h25: rf_25 <= write_data_0;
          5'h26: rf_26 <= write_data_0;
          5'h27: rf_27 <= write_data_0;
          5'h28: rf_28 <= write_data_0;
          5'h29: rf_29 <= write_data_0;
          5'h30: rf_30 <= write_data_0;
          5'h31: rf_31 <= write_data_0;
         endcase 
        end
      // write port 1
      if( we_1 )
        //rf[write_addr_1] <= write_data_1;
        begin
         case(write_addr_1)
          5'h0: rf_0 <= write_data_1;
          5'h1: rf_1 <= write_data_1;
          5'h2: rf_2 <= write_data_1;
          5'h3: rf_3 <= write_data_1;
          5'h4: rf_4 <= write_data_1;
          5'h5: rf_5 <= write_data_1;
          5'h6: rf_6 <= write_data_1;
          5'h7: rf_7 <= write_data_1;
          5'h8: rf_8 <= write_data_1;
          5'h9: rf_9 <= write_data_1;
          5'h10: rf_10 <= write_data_1;
          5'h11: rf_11 <= write_data_1;
          5'h12: rf_12 <= write_data_1;
          5'h13: rf_13 <= write_data_1;
          5'h14: rf_14 <= write_data_1;
          5'h15: rf_15 <= write_data_1;
          5'h16: rf_16 <= write_data_1;
          5'h17: rf_17 <= write_data_1;
          5'h18: rf_18 <= write_data_1;
          5'h19: rf_19 <= write_data_1;
          5'h20: rf_20 <= write_data_1;
          5'h21: rf_21 <= write_data_1;
          5'h22: rf_22 <= write_data_1;
          5'h23: rf_23 <= write_data_1;
          5'h24: rf_24 <= write_data_1;
          5'h25: rf_25 <= write_data_1;
          5'h26: rf_26 <= write_data_1;
          5'h27: rf_27 <= write_data_1;
          5'h28: rf_28 <= write_data_1;
          5'h29: rf_29 <= write_data_1;
          5'h30: rf_30 <= write_data_1;
          5'h31: rf_31 <= write_data_1;
         endcase
        end
    end
  end
endmodule
