////////////////////////////////////////////////////////////////////////////////
// scoreboard.v (Scoreboard)
// Converted from revision 3283
////////////////////////////////////////////////////////////////////////////////
//
//
////////////////////////////////////////////////////////////////////////////////

//typedef enum logic{
//  SB_INVALID  = 1'b0,
//  SB_READY  = 1'b1
//} scoreboard_t;

//parameter SB_READ_PORTS  = 4;
//parameter SB_WRITE_PORTS = 4;

////////////////////////////////////////////////////////////////////////////////
module scoreboard(
  // control
  clk, 
  reset,
  // source registers (read sb)
  src0_addr,
  src1_addr,
  src2_addr,
  src3_addr,
  // destination registers (read sb)
  dst0_addr,
  dst0_vld,
  dst1_addr,
  dst1_vld,
  // inputs from wb
  wb0_addr,
  wb0_vld,
  wb1_addr,
  wb1_vld,
  // ready outputs (true if ready)
  src0_rdy,
  src1_rdy,
  src2_rdy,
  src3_rdy,
  dst0_rdy,
  dst1_rdy
);

  // control
  input clk; 
  input reset;
  // source registers (read sb)
  input [4:0] src0_addr;
  input [4:0] src1_addr;
  input [4:0] src2_addr;
  input [4:0] src3_addr;
  // destination registers (read sb)
  input [4:0] dst0_addr;
  input dst0_vld;
  input [4:0] dst1_addr;
  input dst1_vld;
  // inputs from wb
  input [4:0] wb0_addr;
  input wb0_vld;
  input [4:0] wb1_addr;
  input wb1_vld;
  // ready outputs (true if ready)
  output src0_rdy;
  output src1_rdy;
  output src2_rdy;
  output src3_rdy;
  output dst0_rdy;
  output dst1_rdy;
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////
  reg [31:0] sb;      // scoreboard bits
  reg [3:0] next_sb; // next scoreboard bits
  reg [4:0] next_sb_addr_0; // next scoreboard bits
  reg [4:0] next_sb_addr_1; // next scoreboard bits
  reg [4:0] next_sb_addr_2; // next scoreboard bits
  reg [4:0] next_sb_addr_3; // next scoreboard bits
  reg [3:0] next_sb_vld; // next scoreboard bits

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** combinational logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_READ
  //////////////////////////////////////////////////////////////////////////////
  // 
  //////////////////////////////////////////////////////////////////////////////

  //assign dst0_rdy = sb[dst0_addr]!=1'b0;
  always@(*)
    begin
      case(dst0_addr)
        5'h0: dst0_rdy = (sb[0]!=1'b0);
        5'h1: dst0_rdy = (sb[1]!=1'b0);
        5'h2: dst0_rdy = (sb[2]!=1'b0);
        5'h3: dst0_rdy = (sb[3]!=1'b0);
        5'h4: dst0_rdy = (sb[4]!=1'b0);
        5'h5: dst0_rdy = (sb[5]!=1'b0);
        5'h6: dst0_rdy = (sb[6]!=1'b0);
        5'h7: dst0_rdy = (sb[7]!=1'b0);
        5'h8: dst0_rdy = (sb[8]!=1'b0);
        5'h9: dst0_rdy = (sb[9]!=1'b0);
        5'ha: dst0_rdy = (sb[10]!=1'b0);
        5'hb: dst0_rdy = (sb[11]!=1'b0);
        5'hc: dst0_rdy = (sb[12]!=1'b0);
        5'hd: dst0_rdy = (sb[13]!=1'b0);
        5'he: dst0_rdy = (sb[14]!=1'b0);
        5'hf: dst0_rdy = (sb[15]!=1'b0);
        5'h10: dst0_rdy = (sb[16]!=1'b0);
        5'h11: dst0_rdy = (sb[17]!=1'b0);
        5'h12: dst0_rdy = (sb[18]!=1'b0);
        5'h13: dst0_rdy = (sb[19]!=1'b0);
        5'h14: dst0_rdy = (sb[20]!=1'b0);
        5'h15: dst0_rdy = (sb[21]!=1'b0);
        5'h16: dst0_rdy = (sb[22]!=1'b0);
        5'h17: dst0_rdy = (sb[23]!=1'b0);
        5'h18: dst0_rdy = (sb[24]!=1'b0);
        5'h19: dst0_rdy = (sb[25]!=1'b0);
        5'h1a: dst0_rdy = (sb[26]!=1'b0);
        5'h1b: dst0_rdy = (sb[27]!=1'b0);
        5'h1c: dst0_rdy = (sb[28]!=1'b0);
        5'h1d: dst0_rdy = (sb[29]!=1'b0);
        5'h1e: dst0_rdy = (sb[30]!=1'b0);
        5'h1f: dst0_rdy = (sb[31]!=1'b0);
      endcase
    end


always@(*)
    begin
      case(dst1_addr)
        5'h0: dst1_rdy = (sb[0]!=1'b0);
        5'h1: dst1_rdy = (sb[1]!=1'b0);
        5'h2: dst1_rdy = (sb[2]!=1'b0);
        5'h3: dst1_rdy = (sb[3]!=1'b0);
        5'h4: dst1_rdy = (sb[4]!=1'b0);
        5'h5: dst1_rdy = (sb[5]!=1'b0);
        5'h6: dst1_rdy = (sb[6]!=1'b0);
        5'h7: dst1_rdy = (sb[7]!=1'b0);
        5'h8: dst1_rdy = (sb[8]!=1'b0);
        5'h9: dst1_rdy = (sb[9]!=1'b0);
        5'ha: dst1_rdy = (sb[10]!=1'b0);
        5'hb: dst1_rdy = (sb[11]!=1'b0);
        5'hc: dst1_rdy = (sb[12]!=1'b0);
        5'hd: dst1_rdy = (sb[13]!=1'b0);
        5'he: dst1_rdy = (sb[14]!=1'b0);
        5'hf: dst1_rdy = (sb[15]!=1'b0);
        5'h10: dst1_rdy = (sb[16]!=1'b0);
        5'h11: dst1_rdy = (sb[17]!=1'b0);
        5'h12: dst1_rdy = (sb[18]!=1'b0);
        5'h13: dst1_rdy = (sb[19]!=1'b0);
        5'h14: dst1_rdy = (sb[20]!=1'b0);
        5'h15: dst1_rdy = (sb[21]!=1'b0);
        5'h16: dst1_rdy = (sb[22]!=1'b0);
        5'h17: dst1_rdy = (sb[23]!=1'b0);
        5'h18: dst1_rdy = (sb[24]!=1'b0);
        5'h19: dst1_rdy = (sb[25]!=1'b0);
        5'h1a: dst1_rdy = (sb[26]!=1'b0);
        5'h1b: dst1_rdy = (sb[27]!=1'b0);
        5'h1c: dst1_rdy = (sb[28]!=1'b0);
        5'h1d: dst1_rdy = (sb[29]!=1'b0);
        5'h1e: dst1_rdy = (sb[30]!=1'b0);
        5'h1f: dst1_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(*)
    begin
      case(src0_addr)
        5'h0: src0_rdy = 1;
        5'h1: src0_rdy = (sb[1]!=1'b0);
        5'h2: src0_rdy = (sb[2]!=1'b0);
        5'h3: src0_rdy = (sb[3]!=1'b0);
        5'h4: src0_rdy = (sb[4]!=1'b0);
        5'h5: src0_rdy = (sb[5]!=1'b0);
        5'h6: src0_rdy = (sb[6]!=1'b0);
        5'h7: src0_rdy = (sb[7]!=1'b0);
        5'h8: src0_rdy = (sb[8]!=1'b0);
        5'h9: src0_rdy = (sb[9]!=1'b0);
        5'ha: src0_rdy = (sb[10]!=1'b0);
        5'hb: src0_rdy = (sb[11]!=1'b0);
        5'hc: src0_rdy = (sb[12]!=1'b0);
        5'hd: src0_rdy = (sb[13]!=1'b0);
        5'he: src0_rdy = (sb[14]!=1'b0);
        5'hf: src0_rdy = (sb[15]!=1'b0);
        5'h10: src0_rdy = (sb[16]!=1'b0);
        5'h11: src0_rdy = (sb[17]!=1'b0);
        5'h12: src0_rdy = (sb[18]!=1'b0);
        5'h13: src0_rdy = (sb[19]!=1'b0);
        5'h14: src0_rdy = (sb[20]!=1'b0);
        5'h15: src0_rdy = (sb[21]!=1'b0);
        5'h16: src0_rdy = (sb[22]!=1'b0);
        5'h17: src0_rdy = (sb[23]!=1'b0);
        5'h18: src0_rdy = (sb[24]!=1'b0);
        5'h19: src0_rdy = (sb[25]!=1'b0);
        5'h1a: src0_rdy = (sb[26]!=1'b0);
        5'h1b: src0_rdy = (sb[27]!=1'b0);
        5'h1c: src0_rdy = (sb[28]!=1'b0);
        5'h1d: src0_rdy = (sb[29]!=1'b0);
        5'h1e: src0_rdy = (sb[30]!=1'b0);
        5'h1f: src0_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(*)
    begin
      case(src1_addr)
        5'h0: src1_rdy = 1;
        5'h1: src1_rdy = (sb[1]!=1'b0);
        5'h2: src1_rdy = (sb[2]!=1'b0);
        5'h3: src1_rdy = (sb[3]!=1'b0);
        5'h4: src1_rdy = (sb[4]!=1'b0);
        5'h5: src1_rdy = (sb[5]!=1'b0);
        5'h6: src1_rdy = (sb[6]!=1'b0);
        5'h7: src1_rdy = (sb[7]!=1'b0);
        5'h8: src1_rdy = (sb[8]!=1'b0);
        5'h9: src1_rdy = (sb[9]!=1'b0);
        5'ha: src1_rdy = (sb[10]!=1'b0);
        5'hb: src1_rdy = (sb[11]!=1'b0);
        5'hc: src1_rdy = (sb[12]!=1'b0);
        5'hd: src1_rdy = (sb[13]!=1'b0);
        5'he: src1_rdy = (sb[14]!=1'b0);
        5'hf: src1_rdy = (sb[15]!=1'b0);
        5'h10: src1_rdy = (sb[16]!=1'b0);
        5'h11: src1_rdy = (sb[17]!=1'b0);
        5'h12: src1_rdy = (sb[18]!=1'b0);
        5'h13: src1_rdy = (sb[19]!=1'b0);
        5'h14: src1_rdy = (sb[20]!=1'b0);
        5'h15: src1_rdy = (sb[21]!=1'b0);
        5'h16: src1_rdy = (sb[22]!=1'b0);
        5'h17: src1_rdy = (sb[23]!=1'b0);
        5'h18: src1_rdy = (sb[24]!=1'b0);
        5'h19: src1_rdy = (sb[25]!=1'b0);
        5'h1a: src1_rdy = (sb[26]!=1'b0);
        5'h1b: src1_rdy = (sb[27]!=1'b0);
        5'h1c: src1_rdy = (sb[28]!=1'b0);
        5'h1d: src1_rdy = (sb[29]!=1'b0);
        5'h1e: src1_rdy = (sb[30]!=1'b0);
        5'h1f: src1_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(*)
    begin
      case(src2_addr)
        5'h0: src2_rdy = 1;
        5'h1: src2_rdy = (sb[1]!=1'b0);
        5'h2: src2_rdy = (sb[2]!=1'b0);
        5'h3: src2_rdy = (sb[3]!=1'b0);
        5'h4: src2_rdy = (sb[4]!=1'b0);
        5'h5: src2_rdy = (sb[5]!=1'b0);
        5'h6: src2_rdy = (sb[6]!=1'b0);
        5'h7: src2_rdy = (sb[7]!=1'b0);
        5'h8: src2_rdy = (sb[8]!=1'b0);
        5'h9: src2_rdy = (sb[9]!=1'b0);
        5'ha: src2_rdy = (sb[10]!=1'b0);
        5'hb: src2_rdy = (sb[11]!=1'b0);
        5'hc: src2_rdy = (sb[12]!=1'b0);
        5'hd: src2_rdy = (sb[13]!=1'b0);
        5'he: src2_rdy = (sb[14]!=1'b0);
        5'hf: src2_rdy = (sb[15]!=1'b0);
        5'h10: src2_rdy = (sb[16]!=1'b0);
        5'h11: src2_rdy = (sb[17]!=1'b0);
        5'h12: src2_rdy = (sb[18]!=1'b0);
        5'h13: src2_rdy = (sb[19]!=1'b0);
        5'h14: src2_rdy = (sb[20]!=1'b0);
        5'h15: src2_rdy = (sb[21]!=1'b0);
        5'h16: src2_rdy = (sb[22]!=1'b0);
        5'h17: src2_rdy = (sb[23]!=1'b0);
        5'h18: src2_rdy = (sb[24]!=1'b0);
        5'h19: src2_rdy = (sb[25]!=1'b0);
        5'h1a: src2_rdy = (sb[26]!=1'b0);
        5'h1b: src2_rdy = (sb[27]!=1'b0);
        5'h1c: src2_rdy = (sb[28]!=1'b0);
        5'h1d: src2_rdy = (sb[29]!=1'b0);
        5'h1e: src2_rdy = (sb[30]!=1'b0);
        5'h1f: src2_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(*)
    begin
      case(src3_addr)
        5'h0: src3_rdy = 1;
        5'h1: src3_rdy = (sb[1]!=1'b0);
        5'h2: src3_rdy = (sb[2]!=1'b0);
        5'h3: src3_rdy = (sb[3]!=1'b0);
        5'h4: src3_rdy = (sb[4]!=1'b0);
        5'h5: src3_rdy = (sb[5]!=1'b0);
        5'h6: src3_rdy = (sb[6]!=1'b0);
        5'h7: src3_rdy = (sb[7]!=1'b0);
        5'h8: src3_rdy = (sb[8]!=1'b0);
        5'h9: src3_rdy = (sb[9]!=1'b0);
        5'ha: src3_rdy = (sb[10]!=1'b0);
        5'hb: src3_rdy = (sb[11]!=1'b0);
        5'hc: src3_rdy = (sb[12]!=1'b0);
        5'hd: src3_rdy = (sb[13]!=1'b0);
        5'he: src3_rdy = (sb[14]!=1'b0);
        5'hf: src3_rdy = (sb[15]!=1'b0);
        5'h10: src3_rdy = (sb[16]!=1'b0);
        5'h11: src3_rdy = (sb[17]!=1'b0);
        5'h12: src3_rdy = (sb[18]!=1'b0);
        5'h13: src3_rdy = (sb[19]!=1'b0);
        5'h14: src3_rdy = (sb[20]!=1'b0);
        5'h15: src3_rdy = (sb[21]!=1'b0);
        5'h16: src3_rdy = (sb[22]!=1'b0);
        5'h17: src3_rdy = (sb[23]!=1'b0);
        5'h18: src3_rdy = (sb[24]!=1'b0);
        5'h19: src3_rdy = (sb[25]!=1'b0);
        5'h1a: src3_rdy = (sb[26]!=1'b0);
        5'h1b: src3_rdy = (sb[27]!=1'b0);
        5'h1c: src3_rdy = (sb[28]!=1'b0);
        5'h1d: src3_rdy = (sb[29]!=1'b0);
        5'h1e: src3_rdy = (sb[30]!=1'b0);
        5'h1f: src3_rdy = (sb[31]!=1'b0);
      endcase
    end

  assign next_sb_addr_0 = dst0_addr;
  assign next_sb_addr_1 = dst1_addr;
  assign next_sb_addr_2 = wb0_addr;
  assign next_sb_addr_3 = wb1_addr;
 
  
  //////////////////////////////////////////////////////////////////////////////
  // SB_UNLOCK
  //////////////////////////////////////////////////////////////////////////////
  always @ (dst0_vld or
			dst0_addr or
			sb or
			dst1_vld or
			dst1_addr or
			wb0_vld or
			wb1_vld) begin: SB_UNLOCK
    // decode/issue ports (lock) 
    //next_sb[0] = 1'b0;
    if(dst0_vld && sb[dst0_addr]==1'b1) begin
      next_sb_vld[0] = 1'b1;
    end else begin
      next_sb_vld[0] = 1'b0;
    end
    //next_sb[1] = 1'b0;
    if(dst1_vld && sb[dst1_addr]==1'b1) begin
      next_sb_vld[1] = 1'b1;
    end else begin
      next_sb_vld[1] = 1'b0;
    end
    // writeback ports (unlock)
    //next_sb[2] = 1'b1;
    if(wb0_vld) begin
      next_sb_vld[2] = 1'b1;
    end else begin
      next_sb_vld[2] = 1'b0;
    end
    //next_sb[3] = 1'b1;
    if(wb1_vld) begin
      next_sb_vld[3] = 1'b1;
    end else begin
      next_sb_vld[3] = 1'b0;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
 

////////////////////////////////////////////////////////////////////////////////
// *** sequential logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_UPDATE
  //////////////////////////////////////////////////////////////////////////////
  // update the scoreboard
  // for reset
  integer i;
  //////////////////////////////////////////////////////////////////////////////
 always @ (posedge clk) begin: SB_UPDATE
    if( reset == 1 ) begin
      sb[31:0] <= 32'hffffffff;
    end
    else begin
      if(next_sb_vld[0])
        begin
          case(next_sb_addr[0])
            5'h0: sb[0] <= 0;
            5'h1: sb[1] <= 0;
            5'h2: sb[2] <= 0;
            5'h3: sb[3] <= 0;
            5'h4: sb[4] <= 0;
            5'h5: sb[5] <= 0;
            5'h6: sb[6] <= 0;
            5'h7: sb[7] <= 0;
            5'h8: sb[8] <= 0;
            5'h9: sb[9] <= 0;
            5'ha: sb[10] <= 0;
            5'hb: sb[11] <= 0;
            5'hc: sb[12] <= 0;
            5'hd: sb[13] <= 0;
            5'he: sb[14] <= 0;
            5'hf: sb[15] <= 0;
            5'h10: sb[16] <= 0;
            5'h11: sb[17] <= 0;
            5'h12: sb[18] <= 0;
            5'h13: sb[19] <= 0;
            5'h14: sb[20] <= 0;
            5'h15: sb[21] <= 0;
            5'h16: sb[22] <= 0;
            5'h17: sb[23] <= 0;
            5'h18: sb[24] <= 0;
            5'h19: sb[25] <= 0;
            5'h1a: sb[26] <= 0;
            5'h1b: sb[27] <= 0;
            5'h1c: sb[28] <= 0;
            5'h1d: sb[29] <= 0;
            5'h1e: sb[30] <= 0;
            5'h1f: sb[31] <= 0;
          endcase
        end
      if(next_sb_vld[1])
        begin
          case(next_sb_addr[1])
            5'h0: sb[0] <= 0;
            5'h1: sb[1] <= 0;
            5'h2: sb[2] <= 0;
            5'h3: sb[3] <= 0;
            5'h4: sb[4] <= 0;
            5'h5: sb[5] <= 0;
            5'h6: sb[6] <= 0;
            5'h7: sb[7] <= 0;
            5'h8: sb[8] <= 0;
            5'h9: sb[9] <= 0;
            5'ha: sb[10] <= 0;
            5'hb: sb[11] <= 0;
            5'hc: sb[12] <= 0;
            5'hd: sb[13] <= 0;
            5'he: sb[14] <= 0;
            5'hf: sb[15] <= 0;
            5'h10: sb[16] <= 0;
            5'h11: sb[17] <= 0;
            5'h12: sb[18] <= 0;
            5'h13: sb[19] <= 0;
            5'h14: sb[20] <= 0;
            5'h15: sb[21] <= 0;
            5'h16: sb[22] <= 0;
            5'h17: sb[23] <= 0;
            5'h18: sb[24] <= 0;
            5'h19: sb[25] <= 0;
            5'h1a: sb[26] <= 0;
            5'h1b: sb[27] <= 0;
            5'h1c: sb[28] <= 0;
            5'h1d: sb[29] <= 0;
            5'h1e: sb[30] <= 0;
            5'h1f: sb[31] <= 0;
          endcase
        end
      if(next_sb_vld[2])
        begin
          case(next_sb_addr[2])
            5'h0: sb[0] <= 1;
            5'h1: sb[1] <= 1;
            5'h2: sb[2] <= 1;
            5'h3: sb[3] <= 1;
            5'h4: sb[4] <= 1;
            5'h5: sb[5] <= 1;
            5'h6: sb[6] <= 1;
            5'h7: sb[7] <= 1;
            5'h8: sb[8] <= 1;
            5'h9: sb[9] <= 1;
            5'ha: sb[10] <= 1;
            5'hb: sb[11] <= 1;
            5'hc: sb[12] <= 1;
            5'hd: sb[13] <= 1;
            5'he: sb[14] <= 1;
            5'hf: sb[15] <= 1;
            5'h10: sb[16] <= 1;
            5'h11: sb[17] <= 1;
            5'h12: sb[18] <= 1;
            5'h13: sb[19] <= 1;
            5'h14: sb[20] <= 1;
            5'h15: sb[21] <= 1;
            5'h16: sb[22] <= 1;
            5'h17: sb[23] <= 1;
            5'h18: sb[24] <= 1;
            5'h19: sb[25] <= 1;
            5'h1a: sb[26] <= 1;
            5'h1b: sb[27] <= 1;
            5'h1c: sb[28] <= 1;
            5'h1d: sb[29] <= 1;
            5'h1e: sb[30] <= 1;
            5'h1f: sb[31] <= 1;
          endcase
        end

      if(next_sb_vld[3])
        begin
          case(next_sb_addr[3])
            5'h0: sb[0] <= 1;
            5'h1: sb[1] <= 1;
            5'h2: sb[2] <= 1;
            5'h3: sb[3] <= 1;
            5'h4: sb[4] <= 1;
            5'h5: sb[5] <= 1;
            5'h6: sb[6] <= 1;
            5'h7: sb[7] <= 1;
            5'h8: sb[8] <= 1;
            5'h9: sb[9] <= 1;
            5'ha: sb[10] <= 1;
            5'hb: sb[11] <= 1;
            5'hc: sb[12] <= 1;
            5'hd: sb[13] <= 1;
            5'he: sb[14] <= 1;
            5'hf: sb[15] <= 1;
            5'h10: sb[16] <= 1;
            5'h11: sb[17] <= 1;
            5'h12: sb[18] <= 1;
            5'h13: sb[19] <= 1;
            5'h14: sb[20] <= 1;
            5'h15: sb[21] <= 1;
            5'h16: sb[22] <= 1;
            5'h17: sb[23] <= 1;
            5'h18: sb[24] <= 1;
            5'h19: sb[25] <= 1;
            5'h1a: sb[26] <= 1;
            5'h1b: sb[27] <= 1;
            5'h1c: sb[28] <= 1;
            5'h1d: sb[29] <= 1;
            5'h1e: sb[30] <= 1;
            5'h1f: sb[31] <= 1;
          endcase
        end
   end
 end
endmodule
