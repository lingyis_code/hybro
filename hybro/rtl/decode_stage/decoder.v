/////////////////////////////////////////////////////////////////////////////
// NOTE: This Decoder is AUTO-GENERATED! DO NOT EDIT BY HAND! 
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// A decoder unit for the Rigel architecture
////////////////////////////////////////////////////////////////////
module decoder(
  fetch2decodeinstr1_out,
  //output decode_packet_t instr_packet
  decode_packet1sreg_t,
  decode_packet1sreg_s,
  decode_packet1dreg,
  decode_packet1acc,
  decode_packet1imm16_value,
  decode_packet1imm26_value,
  decode_packet1imm5_value,
  decode_packet1op_FU_type,
  decode_packet1alu_op,
  decode_packet1fpu_op,
  decode_packet1op_MEM_type,
  decode_packet1op_BR_type,
  decode_packet1shift_type,
  decode_packet1compare_type,
  decode_packet1is_imm26,
  decode_packet1is_imm,
  decode_packet1is_imm5,
  decode_packet1is_signed,
  decode_packet1carry_in,
  decode_packet1carry_out,
  decode_packet1has_sreg_t,
  decode_packet1has_sreg_s,
  decode_packet1has_dreg,
  decode_packet1is_prefetch,
  decode_packet1sprf_dest,
  decode_packet1sprf_src,
  decode_packet1is_atomic,
  decode_packet1is_ldl,
  decode_packet1is_stl,
  decode_packet1is_stc,
  decode_packet1is_valid,
  decode_packet1is_halt,
  decode_packet1is_compare,
  decode_packet1dreg_is_src,
  decode_packet1dreg_is_dest
);

  input [31:0] fetch2decodeinstr1_out;
  //output instr_packet
  output [4:0] decode_packet1sreg_t;
  output [4:0] decode_packet1sreg_s;
  output [4:0] decode_packet1dreg;
  output [1:0] decode_packet1acc;
  output [15:0] decode_packet1imm16_value;
  output [25:0] decode_packet1imm26_value;
  output [4:0] decode_packet1imm5_value;
  output [3:0] decode_packet1op_FU_type;
  output [4:0] decode_packet1alu_op;
  output [4:0] decode_packet1fpu_op;
  output [3:0] decode_packet1op_MEM_type;
  output [1:0] decode_packet1op_BR_type;
  output [1:0] decode_packet1shift_type;
  output [2:0] decode_packet1compare_type;
  output  decode_packet1is_imm26;
  output  decode_packet1is_imm;
  output  decode_packet1is_imm5;
  output  decode_packet1is_signed;
  output  decode_packet1carry_in;
  output  decode_packet1carry_out;
  output  decode_packet1has_sreg_t;
  output  decode_packet1has_sreg_s;
  output  decode_packet1has_dreg;
  output  decode_packet1is_prefetch;
  output  decode_packet1sprf_dest;
  output  decode_packet1sprf_src;
  output  decode_packet1is_atomic;
  output  decode_packet1is_ldl;
  output  decode_packet1is_stl;
  output  decode_packet1is_stc;
  output  decode_packet1is_valid;
  output  decode_packet1is_halt;
  output  decode_packet1is_compare;
  output  decode_packet1dreg_is_src;
  output  decode_packet1dreg_is_dest;

  wire [3:0] decode_optype;
  wire [1:0] decode_opc;
  wire [1:0] decode_opc26;
  wire [10:0] decode_opcode;
  reg has_sreg_s;
  reg has_dreg;

// set instruction-agnostic outputs with continuous assignments 
  assign decode_optype = fetch2decodeinstr1_out[31:28]; 
  assign decode_packet1dreg   = fetch2decodeinstr1_out[27:23]; 
  assign decode_packet1sreg_t = fetch2decodeinstr1_out[22:18]; 
  assign decode_opcode = fetch2decodeinstr1_out[8:0]; 
  assign decode_packet1imm5_value   = fetch2decodeinstr1_out[17:13]; 
  assign decode_opc    = fetch2decodeinstr1_out[17:16]; 
  assign decode_opc26    = fetch2decodeinstr1_out[27:26]; 
  assign decode_packet1imm16_value  = fetch2decodeinstr1_out[15:0]; 
  assign decode_packet1imm26_value  = fetch2decodeinstr1_out[25:0]; 
  assign decode_packet1acc    = fetch2decodeinstr1_out[12:11]; 
 //FIXME: This mux checks if dreg is actually a source register
  assign decode_packet1sreg_s = (decode_packet1dreg_is_src)? fetch2decodeinstr1_out[27:23]:fetch2decodeinstr1_out[17:13];
  assign decode_packet1has_sreg_s = has_sreg_s || decode_packet1dreg_is_src;
  assign decode_packet1has_dreg = has_dreg && (!decode_packet1dreg_is_src || decode_packet1dreg_is_dest);
/////////////////////////////////////////////////////////////////////////////
// Decode on OpType  
/////////////////////////////////////////////////////////////////////////////
	always @ (fetch2decodeinstr1_out) // trigger decode on instruction change 
	begin                        
		case( decode_optype )
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 0 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | sreg_s/imm5  | d| t| s| i|         opcode           |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h0:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = fetch2decodeinstr1_out[12];
			decode_packet1has_sreg_t = fetch2decodeinstr1_out[11];
			has_sreg_s = fetch2decodeinstr1_out[10];
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = fetch2decodeinstr1_out[9];

			// op-specific decode logic
			case ( decode_opcode  )
				//////////////////////////
				// add
				// Signed Addition
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sub
				// Signed Subtraction
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addu
				// Unsigned Addition
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subu
				// Unsigned Subtraction
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addc
				// Signed Addition (Carry In $r1)
				//////////////////////////
				9'h04:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b1;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addg
				// Signed Addition (Carry Out $r1)
				//////////////////////////
				9'h05:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b1;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addcu
				// Unsigned Addition (Carry In $r1)
				//////////////////////////
				9'h06:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b1;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addgu
				// Unsigned Addition (Carry Out $r1)
				//////////////////////////
				9'h07:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b1;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// and
				// Bitwise AND
				//////////////////////////
				9'h08:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// or
				// Bitwise OR
				//////////////////////////
				9'h09:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0101; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// xor
				// Bitwise XOR
				//////////////////////////
				9'h0a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0111; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// nor
				// Bitwise NOR
				//////////////////////////
				9'h0b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0110; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sll
				// Shift Left Logical
				//////////////////////////
				9'h0c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b01; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srl
				// Shift Right Logical
				//////////////////////////
				9'h0d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sra
				// Shift Right Arithmetic
				//////////////////////////
				9'h0e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// slli
				// Shift Left Logical Immediate
				//////////////////////////
				9'h0f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b01; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srli
				// Shift Right Logical Immediate
				//////////////////////////
				9'h10:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srai
				// Shift Right Arithmetic Immediate
				//////////////////////////
				9'h11:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ceq
				// Compare Equal (Signed)
				//////////////////////////
				9'h12:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// clt
				// Compare Less Than (Signed)
				//////////////////////////
				9'h13:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cle
				// Compare Less Than or Equal (Signed)
				//////////////////////////
				9'h14:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltu
				// Compare Less Than (Unsigned)
				//////////////////////////
				9'h15:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cleu
				// Compare Less Than or Equal (Unsigned)
				//////////////////////////
				9'h16:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ceqf
				// Compare Equal (Float)
				//////////////////////////
				9'h17:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltf
				// Compare Less Than (Float)
				//////////////////////////
				9'h18:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltef
				// Compare Less Than or Equal (Float)
				//////////////////////////
				9'h19:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cmoveq
				// Conditional Move if Equal
				//////////////////////////
				9'h1a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cmovneq
				// Conditional Move if Not Equal
				//////////////////////////
				9'h1b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mfsr
				// Move from Special-Purpose Register
				//////////////////////////
				9'h1c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b1;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mtsr
				// Move to Special-Purpose Register
				//////////////////////////
				9'h1d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jmpr
				// Unconditional Indirect Jump
				//////////////////////////
				9'h1e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b01; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jalr
				// Jump and Link Register
				//////////////////////////
				9'h1f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b01; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ldl
				// Load Linked (Local)
				//////////////////////////
				9'h20:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1001; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b1;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// stc
				// Store Conditional (Local)
				//////////////////////////
				9'h21:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1010; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b1;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomcas
				// Atomic Compare and Swap
				//////////////////////////
				9'h22:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomaddu
				// Atomic Unsigned Addition
				//////////////////////////
				9'h23:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// nop
				// No Operation
				//////////////////////////
				9'h24:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// brk
				// Software Break Point
				//////////////////////////
				9'h25:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// hlt
				// Halt Core
				//////////////////////////
				9'h26:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b1;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sync
				// Force all pending operations to complete before executing next the next instruction
				//////////////////////////
				9'h27:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// undef
				// Undefined Opcode
				//////////////////////////
				9'h28:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccwb
				// Writeback Cluster Cache
				//////////////////////////
				9'h29:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccinv
				// Invalidate Cluster Cache
				//////////////////////////
				9'h2a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccflush
				// Writeback and Invalidate Cluster Cache
				//////////////////////////
				9'h2b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// icinv
				// Invalidate Instruction Cache
				//////////////////////////
				9'h2c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mb
				// Full memory barrier
				//////////////////////////
				9'h2d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// rfe
				// Return from exception
				//////////////////////////
				9'h2e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// abort
				// RigelSim-Only: Abort the simulation
				//////////////////////////
				9'h2f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// syscall
				// RigelSim-Only: Call a UNIX system call
				//////////////////////////
				9'h30:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// printreg
				// RigelSim-Only: Print out the contents of a register
				//////////////////////////
				9'h31:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// timerstart
				// RigelSim-Only: Start a timer without a syscall
				//////////////////////////
				9'h32:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// timerstop
				// RigelSim-Only: Stop a timer without a syscall
				//////////////////////////
				9'h33:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// linewb
				// Writeback Cache Line
				//////////////////////////
				9'h34:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1100; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// lineinv
				// Invalidate Cache Line
				//////////////////////////
				9'h35:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1101; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// lineflush
				// Writeback and Invalidate Cache Line
				//////////////////////////
				9'h36:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1011; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqenq
				// Enqueue a Task in the HW Task Queue
				//////////////////////////
				9'h37:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqdeq
				// Dequeue a Task from the HW Task Queue
				//////////////////////////
				9'h38:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqloop
				// Enqueue a Range of Tasks
				//////////////////////////
				9'h39:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqinit
				// Initialize the HW Task Queue
				//////////////////////////
				9'h3a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqend
				// Initialize the HW Task Queue
				//////////////////////////
				9'h3b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fadd
				// Floating-Point Addition
				//////////////////////////
				9'h3c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b0001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fsub
				// Floating-Point Subtraction
				//////////////////////////
				9'h3d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b0010; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmul
				// Floating-Point Multiply
				//////////////////////////
				9'h3e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b1001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// frcp
				// Floating-Point Reciprocal
				//////////////////////////
				9'h3f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10010; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// frsq
				// Floating-Point Reciprocal Square Root
				//////////////////////////
				9'h40:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10011; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fabs
				// Floating-Point Absolute Value (Rounding IDK?!)
				//////////////////////////
				9'h41:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b1110; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmrs
				// REMOVE ME!!  Floating-Point Register-to-Register Move
				//////////////////////////
				9'h42:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10100; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmov
				// Floating-Point Register-to-Register Move
				//////////////////////////
				9'h43:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10100; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fr2a
				// Floating-Point Register-to-Accumulator Move
				//////////////////////////
				9'h44:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fa2r
				// Floating-Point Accumulator-to-Register Move
				//////////////////////////
				9'h45:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// f2i
				// Floating-Point to Signed Integer Conversion
				//////////////////////////
				9'h46:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// i2f
				// Signed Integer to Floating-Point Conversion
				//////////////////////////
				9'h47:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul
				// REMOVE ME!! Multiply (No Carry)
				//////////////////////////
				9'h48:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul32
				// 32-bit Multiply (No Carry)
				//////////////////////////
				9'h49:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16
				// 16-bit Multiply (No Carry)
				//////////////////////////
				9'h4a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16c
				// 16-bit Multiply (Carry In)
				//////////////////////////
				9'h4b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16g
				// 16-bit Multiply (Generate Carry)
				//////////////////////////
				9'h4c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// clz
				// Count Leading Zeros
				//////////////////////////
				9'h4d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// zext8
				// Zero Extend Byte
				//////////////////////////
				9'h4e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sext8
				// Sign Extend Byte
				//////////////////////////
				9'h4f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// zext16
				// Zero Extend 2-Byte
				//////////////////////////
				9'h50:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1011; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sext16
				// Sign Extend 2-Byte
				//////////////////////////
				9'h51:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1011; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vadd
				// Vector Signed Addition
				//////////////////////////
				9'h52:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vsub
				// Vector Signed Subtraction
				//////////////////////////
				9'h53:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfadd
				// Vector Floating-Point Addition
				//////////////////////////
				9'h54:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfsub
				// Vector Floating-Point Subtraction
				//////////////////////////
				9'h55:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfmul
				// Vector Floating-Point Multiply
				//////////////////////////
				9'h56:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 1 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h1:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// addi
				// Signed Addition with Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subi
				// Signed Subtraction with Immediate
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addiu
				// Unsigned Addition with Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subiu
				// Unsigned Subtraction with Immediate
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 2 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h2:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// andi
				// Bitwise AND Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ori
				// Bitwise OR Immediate
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0101; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// xori
				// Bitwise XOR Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0111; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// beq
				// Branch If Equal
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 3 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h3:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// mvui
				// Move to Upper Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jal
				// Jump and Link
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// prefl
				// Line Prefetch
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// prefb
				// Block Prefetch (4 KiB)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 4 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h4:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// bne
				// Branch If Not Equal
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b101; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// pldw
				// Paused Load (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bcastu
				// Broadcast update all cluster caches with new value
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ldw
				// Load Word (Local)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0001; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 5 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |//////////////|    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h5:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// be
				// Branch If Equal to Zero
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bnz
				// Branch If Not Equal to Zero
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b101; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// blt
				// Branch If Less than Zero
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bgt
				// Branch If Greater than Zero
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b001; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 6 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |//////////////|    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h6:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// ble
				// Branch If Less than or Equal to Zero
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bge
				// Branch If Greater than or Equal to Zero
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b011; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jmp
				// Unconditional Jump
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 7 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  | opc |                                    imm26                                    |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h7:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b1;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc26 )
				//////////////////////////
				// lj
				// Unconditional Long Jump
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ljl
				// Long Jump and Link
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 8 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h8:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// prefnga
				// Line Prefetch (No GCache Allocate)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bcasti
				// Broadcast invalidate to all cluster caches
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// strtof
				// RigelSim-Only: Do string to floating-point conversion
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// event
				// RigelSim-Only: event tracking with immediate operand
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 9 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h9:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// stw
				// Store Word (Local)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0011; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// gldw
				// Load Word (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0010; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// gstw
				// Store Word (Global)
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0100; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomdec
				// Atomic Fetch and Decrement (Global)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0111; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE a 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'ha:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// atominc
				// Atomic Fetch and Increment (Global)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0110; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomxchg
				// Atomic Exchange (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0101; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b1;
				end
				//////////////////////////
				// vaddi
				// Vector Signed Addition with Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vsubi
				// Vector Signed Subtraction with Immediate
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE b 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    |    sreg_s    | acc |         opcode                 |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hb:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b1;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opcode  )
				//////////////////////////
				// fmadd
				// Floating-Point Multiply Accumulate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmsub
				// Floating-Point Multiply Subtraction
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE c 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hc:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// vldw
				// Vector Load Word (Local)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vstw
				// Vector Store Word (Local)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE d 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hd:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// prio
				// Set priority of traffic flow with immediate operand Zero is lowest priority
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
		default:
		begin
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
			decode_packet1is_valid       = 1'b0;
			decode_packet1is_signed      = 1'b0;
			decode_packet1carry_in       = 1'b0;
			decode_packet1carry_out      = 1'b0;
			decode_packet1is_prefetch    = 1'b0;
			decode_packet1sprf_dest      = 1'b0;
			decode_packet1sprf_src       = 1'b0;
			decode_packet1is_atomic      = 1'b0;
			decode_packet1is_ldl         = 1'b0;
			decode_packet1is_stl         = 1'b0;
			decode_packet1is_stc         = 1'b0;
			decode_packet1is_halt        = 1'b0;
			decode_packet1is_compare     = 1'b0;
			decode_packet1dreg_is_src    = 1'b0;
			decode_packet1dreg_is_dest   = 1'b0;
		end
		endcase
	end
endmodule
