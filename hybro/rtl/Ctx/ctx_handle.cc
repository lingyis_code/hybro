#include <iostream>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <set>
using namespace std;

int main(int argc, char * argv[])
{
   ifstream pri_input_file;
   ifstream ctx_input_file;
   ofstream ctx_pat_file;
   int brace_counter = 0;
   int cycle_number = -1;
   set<char *> primary_input;
   set<char *>::iterator it;  
   bool sep_flag = false;
   char p_identifier[128];
   bool id_flag =  false;
   if(argc != 4 )
   {
     cout << "Usage Error" << endl;
     return -1;
   }
   pri_input_file.open(argv[1]);
   ctx_input_file.open(argv[2]);
   ctx_pat_file.open(argv[3]);

   //primary input
   while(!pri_input_file.eof())
   {
      char * primary_in_var = new char[50];
      pri_input_file.getline(primary_in_var, 50);
      primary_input.insert(primary_in_var);
   } 
   while(!ctx_input_file.eof())
   {
      char ctx_line[256];
      ctx_input_file.getline(ctx_line, 256);
      if(ctx_line[0] == '{')  {brace_counter++;}
      if(ctx_line[13] == '{') {brace_counter++; cycle_number++;}
      if((ctx_line[0] == '}')) 
      {
         if(brace_counter == 1)
         {  
           if(sep_flag == true)
           {
             ctx_pat_file.write("---------\n",10);
             sep_flag = false;
           }
           cycle_number = -1;
         } 
         brace_counter--;
      }
      char * p_id = strstr(ctx_line, "/* identifier  */");
      if(p_id!=NULL)
      { 
         char * p_brace1 = strchr(ctx_line, '\\'); 
         char * p_brace2 = strchr(ctx_line, '}'); 
         int length = p_brace2 - p_brace1 ;
         strncpy(p_identifier, p_brace1, p_brace2-p_brace1); 
         p_identifier[p_brace2-p_brace1] = '\0';
         id_flag = true;
      }
      if((ctx_line[0] == '\\'))
      {
        char * p = strrchr(ctx_line,'\\');
        int pos = p-ctx_line;
        if(pos==0)
        {
           for(it = primary_input.begin(); it!=primary_input.end(); it++)                  {
              char * p_match = strstr(ctx_line ,*it);
              int length = strlen(*it);
              if(p_match !=NULL)
              { 
                 int match = p_match - ctx_line;
                 if(ctx_line[match-1]=='\\' && (ctx_line[match + length] == ' '))
                 {
                     
                     if(id_flag == true)
                     {
                        ctx_pat_file.write(p_identifier,strlen(p_identifier));
                        ctx_pat_file.put('\n'); 
                        id_flag = false;
                     }
                     char * pat=new char[256];
                     char * p_end = strchr(ctx_line, ',');
                     //int total_length = p_end - p_match;                  
                     sprintf(pat,"%s_%d%s", *it, cycle_number, p_match + length + 1 );
                     //cout << p_match + length << endl;
                     
                     //cout << pat << strlen(pat) << endl;
                     //cout << pat << "_" << cycle_number << endl;
                     ctx_pat_file.write(pat, strlen(pat));
                     ctx_pat_file.put('\n');
                     sep_flag = true;
                     // cout << pat << endl;
                     delete pat;
                 }  
              }
           }
        }
      }  
   } 

   pri_input_file.close();
   ctx_input_file.close();
   ctx_pat_file.close();
}
