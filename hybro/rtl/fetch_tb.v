`timescale 1ns/10ps

module test_top;


reg clock;
reg reset;


reg  stall_in;
reg  [31:0] branch_pc;
reg  [31:0] branch_addr;
reg  branch_mispredict;
reg  [63:0] icache_read_data_i;
reg  icache_read_data_vld_i;
reg  [31:0]  icache_read_req_addr_o;
reg  icache_read_req_vld_o;
reg  stall_out;
reg  [31:0] fetch2decodeinstr0_out;
reg  [31:0] fetch2decodeinstr1_out;
reg  [31:0] fetch2decodepc1;
reg  [31:0] fetch2decodepc0;
reg  [31:0] fetch2decodepc1_incr;
reg  [31:0] fetch2decodepc0_incr;
reg         fetch2decodeinstr0_valid;
reg         fetch2decodeinstr1_valid;
reg         fetch2decodebranch_taken;
reg  [31:0] fetch2decodepredicted_pc;
reg         fetch2decodevalid;
reg         btb_valid;
reg  [31:0] btb_pc;
reg  [31:0] btb_target;
reg  [31:0] pc;

fetch_stage inst(
  .clk(clock),
  .reset(reset),
  .stall_in(stall_in),
  .branch_pc(branch_pc[31:0]),
  .branch_addr(branch_addr[31:0]),
  .branch_mispredict(branch_mispredict),
  .icache_read_req_addr_o(icache_read_req_addr_o[31:0]),
  .icache_read_req_vld_o(icache_read_req_vld_o),
  .icache_read_data_i(icache_read_data_i[63:0]),
  .icache_read_data_vld_i(icache_read_data_vld_i),
  .stall_out(stall_out),
  .fetch2decodeinstr0_out(fetch2decodeinstr0_out[31:0]),
  .fetch2decodeinstr1_out(fetch2decodeinstr1_out[31:0]),
  .fetch2decodepc1(fetch2decodepc1[31:0]),
  .fetch2decodepc0(fetch2decodepc0[31:0]),
  .fetch2decodepc1_incr(fetch2decodepc1_incr[31:0]),
  .fetch2decodepc0_incr(fetch2decodepc0_incr[31:0]),
  .fetch2decodeinstr0_valid(fetch2decodeinstr0_valid),
  .fetch2decodeinstr1_valid(fetch2decodeinstr1_valid),
  .fetch2decodebranch_taken(fetch2decodebranch_taken),
  .fetch2decodepredicted_pc(fetch2decodepredicted_pc[31:0]),
  .fetch2decodevalid(fetch2decodevalid)
);

initial begin
#1
   clock = 1'b0;
end

initial begin
 $dumpfile("fetch.vcd");
 $dumpvars();
end


always 
begin
  #5 clock = ~clock;
end

initial
begin
 #1 
  reset = 1'b1;
 #7 
  reset = 1'b0;
end
 
always
begin
  #4;
stall_in = $random;
branch_pc = $random;
branch_addr = $random;
branch_mispredict = $random;
icache_read_data_i = $random;
icache_read_data_vld_i = $random;
  #6;
end


endmodule
