`timescale 1ns/10ps
module b06 (clock , reset , eql , cont_eql , cc_mux , uscite , enable_count , ackout );

	  input  clock;
	  input  reset;
	  input  eql;
	  input  cont_eql;
	  output [1 : 0] cc_mux;
          output [1:0] uscite;
	  output enable_count, ackout ;
	  wire clock;
	  wire reset;
	  wire eql;
	  wire cont_eql;
	  reg [1 : 0] cc_mux;
          reg [1 : 0] uscite;
	  reg [7:0] branvar[0:100];
	  reg enable_count;
          reg ackout;
	  reg [2 : 0] state;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  state <= 0;
	  cc_mux <= 0;
	  enable_count <= 0;
	  ackout <= 0;
	  uscite <= 0;
	 end
	  end
	 else 
	   begin 
	     branvar[1] <= branvar[1] + 1;   
	       begin
	         if( cont_eql) 
	           begin 
	             branvar[2] <= branvar[2] + 1; 
	             begin
	               ackout <= 0;
	               enable_count <= 0;
	             end
	           end
	         else 
	           begin 
	             branvar[3] <= branvar[3] + 1;   
	               begin
	                 ackout <= 1;
	                 enable_count <= 1;
	               end
	           end
	         case( state)
	  0: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	 begin
	  uscite <= 1;
	  cc_mux <= 1;
	  state <= 1;
	 end
	  end
	  1: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 if( eql) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  uscite <= 0;
	  cc_mux <= 3;
	  state <= 2;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 begin
	  uscite <= 1;
	  cc_mux <= 2;
	  state <= 5;
	 end
	  end
	  end
	  5: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	 if( eql) 
	  begin 
	 branvar[9] <= branvar[9] + 1; 
	 begin
	  uscite <= 0;
	  cc_mux <= 3;
	  state <= 4;
	 end
	  end
	 else 
	   begin 
	   branvar[10] <= branvar[10] + 1;   
	 begin
	  uscite <= 2;
	  cc_mux <= 2;
	  state <= 1;
	 end
	  end
	  end
	  2: 
	    begin 
	     branvar[11] <= branvar[11] + 1;
	     if( eql) 
	       begin 
	        branvar[12] <= branvar[12] + 1; 
	        begin
	         uscite[1:0] <= 0;
	         cc_mux[1:0] <= 3;
	         state <= 2;
	        end
	       end
	     else 
	       begin 
	       branvar[13] <= branvar[13] + 1;   
	        begin
	         uscite[1:0] <= 1;
	         ackout <= 1;
	         enable_count <= 1;
	         cc_mux[1:0] <= 1;
	         state <= 3;
	        end
	       end
	    end
	  3: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	 if( eql) 
	  begin 
	 branvar[15] <= branvar[15] + 1; 
	 begin
	  uscite <= 1;
	  cc_mux <= 1;
	  state <= 3;
	 end
	  end
	 else 
	   begin 
	   branvar[16] <= branvar[16] + 1;   
	 begin
	  uscite <= 1;
	  cc_mux <= 1;
	  state <= 1;
	 end
	  end
	  end
	  4: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	 if( eql) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  uscite <= 0;
	  cc_mux <= 3;
	  state <= 4;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 begin
	  uscite <= 3;
	  cc_mux <= 2;
	  state <= 6;
	 end
	  end
	  end
	  6: 
	    begin 
	   branvar[20] <= branvar[20] + 1;
	 if( eql) 
	  begin 
	 branvar[21] <= branvar[21] + 1; 
	 begin
	  uscite <= 3;
	  cc_mux <= 2;
	  state <= 6;
	 end
	  end
	 else 
	   begin 
	   branvar[22] <= branvar[22] + 1;   
	 begin
	  uscite <= 1;
	  cc_mux <= 1;
	  state <= 1;
	 end
	  end
	  end
	 endcase
	 end
	  end
	 end
	  endmodule 
