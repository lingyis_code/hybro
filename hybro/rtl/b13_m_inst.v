`timescale 1ns/10ps
module b13 (clock , reset , eoc , dsr , data_in , soc , load_dato , add_mpx2 , mux_en , error , data_out , canale );

	  input  clock;
	  input  reset;
	  input  eoc;
	  input  dsr;
	  input [7 : 0] data_in;
	  output soc ;
	  output load_dato ;
	  output add_mpx2 ;
	  output mux_en ;
	  output error ;
	  output data_out ;
	  output [3 : 0] canale;
	  wire clock;
	  wire reset;
	  wire eoc;
	  wire dsr;
	  wire [7 : 0] data_in;
	  reg soc;
	  reg [7:0] branvar[0:100];
	  reg load_dato;
	  reg add_mpx2;
	  reg mux_en;
	  reg error;
	  reg data_out;
	  reg [3 : 0] canale;
	  reg mpx, rdy, tre, send, load, shot, confirm;
	  reg send_en, send_data, tx_end;
	  reg [2 : 0] S1;
	  reg [1 : 0] S2;
	  reg [3 : 0] next_bit;
	  reg [1 : 0] itfc_state;
	  reg [7 : 0] out_reg;
	  reg [9 : 0] tx_conta;
	  reg [3 : 0] conta_tmp;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  S1 <= 0;
	  soc <= 0;
	  canale <= 0;
	  conta_tmp = 0;
	  send_data <= 0;
	  load_dato <= 0;
	  mux_en <= 0;
	  S2 <= 0;
	  rdy <= 0;
	  add_mpx2 <= 0;
	  mpx <= 0;
	  shot <= 0;
	  load <= 0;
	  send <= 0;
	  confirm <= 0;
	  itfc_state <= 0;
	  send_en <= 0;
	  out_reg <= 8'b00000000;
	  tre <= 0;
	  error <= 0;
	  tx_end <= 0;
	  data_out <= 0;
	  next_bit <= 0;
	  tx_conta <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 begin
	 case( S1)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  mux_en <= 1;
	  S1 <= 1;
	 end
	  end
	  1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	  S1 <= 2;
	  end
	  2: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	 begin
	  soc <= 1;
	  S1 <= 5;
	 end
	  end
	  5: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 if( eoc) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	  S1 <= 5;
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 begin
	  load_dato <= 1;
	  S1 <= 6;
	  mux_en <= 0;
	 end
	  end
	  end
	  6: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	 begin
	  load_dato <= 0;
	  soc <= 0;
	  conta_tmp <= (  conta_tmp + 1);
	  canale <=  conta_tmp;
	  S1 <= 7;
	 end
	  end
	  7: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 begin
	  send_data <= 1;
	  S1 <= 4;
	 end
	  end
	  4: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	  S1 <= 3;
	  end
	  3: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	 if((!  rdy)) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	  S1 <= 3;
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 begin
	  S1 <= 0;
	  send_data <= 0;
	 end
	  end
	  end
	 endcase
	 case( S2)
	  0: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	 if( send_data) 
	  begin 
	 branvar[15] <= branvar[15] + 1; 
	 begin
	  rdy <= 1;
	  S2 <= 1;
	 end
	  end
	 else 
	   begin 
	   branvar[16] <= branvar[16] + 1;   
	  S2 <= 0;
	  end
	  end
	  1: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	 begin
	  shot <= 1;
	  S2 <= 2;
	 end
	  end
	  2: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	 if((!  confirm)) 
	  begin 
	 branvar[19] <= branvar[19] + 1; 
	 begin
	  shot <= 0;
	  S2 <= 2;
	 end
	  end
	 else 
	   begin 
	   branvar[20] <= branvar[20] + 1;   
	 begin
	 if((!  mpx)) 
	  begin 
	 branvar[21] <= branvar[21] + 1; 
	 begin
	  add_mpx2 <= 1;
	  mpx <= 1;
	  S2 <= 1;
	 end
	  end
	 else 
	   begin 
	   branvar[22] <= branvar[22] + 1;   
	 begin
	  mpx <= 0;
	  rdy <= 0;
	  S2 <= 3;
	 end
	  end
	 end
	  end
	  end
	  3: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	  S2 <= 0;
	  end
	 endcase
	 case( itfc_state)
	  0: 
	    begin 
	   branvar[24] <= branvar[24] + 1;
	 if( shot) 
	  begin 
	 branvar[25] <= branvar[25] + 1; 
	 begin
	  load <= 1;
	  confirm <= 0;
	  itfc_state <= 1;
	 end
	  end
	 else 
	   begin 
	   branvar[26] <= branvar[26] + 1;   
	 begin
	  confirm <= 0;
	  itfc_state <= 0;
	 end
	  end
	  end
	  1: 
	    begin 
	   branvar[27] <= branvar[27] + 1;
	 begin
	  load <= 0;
	  send <= 1;
	  itfc_state <= 2;
	 end
	  end
	  2: 
	    begin 
	   branvar[28] <= branvar[28] + 1;
	 begin
	  send <= 0;
	  itfc_state <= 3;
	 end
	  end
	  3: 
	    begin 
	   branvar[29] <= branvar[29] + 1;
	 if( tx_end) 
	  begin 
	 branvar[30] <= branvar[30] + 1; 
	 begin
	  confirm <= 1;
	  itfc_state <= 0;
	 end
	  end
	 else 
	 begin  
	   branvar[31] <= branvar[31] + 1;   
	   end 
	  end
	 endcase
	 begin
	 if( tx_end) 
	  begin 
	 branvar[32] <= branvar[32] + 1; 
	 begin
	  send_en <= 0;
	  tre <= 1;
	 end
	  end
	 else 
	 begin  
	   branvar[33] <= branvar[33] + 1;   
	   end 
	 if( load) 
	  begin 
	 branvar[34] <= branvar[34] + 1; 
	 begin
	 if((!  tre)) 
	  begin 
	 branvar[35] <= branvar[35] + 1; 
	 begin
	  out_reg <=  data_in;
	  tre <= 1;
	  error <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[36] <= branvar[36] + 1;   
	  error <= 1;
	  end
	 end
	  end
	 else 
	 begin  
	   branvar[37] <= branvar[37] + 1;   
	   end 
	 if( send) 
	  begin 
	 branvar[38] <= branvar[38] + 1; 
	 begin
	 if(( (!  tre) || (!  dsr))) 
	  begin 
	 branvar[39] <= branvar[39] + 1; 
	  error <= 1;
	  end
	 else 
	   begin 
	   branvar[40] <= branvar[40] + 1;   
	 begin
	  error <= 0;
	  send_en <= 1;
	 end
	  end
	 end
	  end
	 else 
	 begin  
	   branvar[41] <= branvar[41] + 1;   
	   end 
	 end
	 begin
	  tx_end <= 0;
	  data_out <= 1;
	 if( send_en) 
	  begin 
	 branvar[42] <= branvar[42] + 1; 
	 begin
	 if((  tx_conta > 104)) 
	  begin 
	 branvar[43] <= branvar[43] + 1; 
	 begin
	 case( next_bit)
	  0: 
	    begin 
	   branvar[44] <= branvar[44] + 1;
	 begin
	  data_out <= 0;
	  next_bit <= 2;
	 end
	  end
	  2: 
	    begin 
	   branvar[45] <= branvar[45] + 1;
	 begin
	  data_out <=  out_reg[7];
	  next_bit <= 3;
	 end
	  end
	  3: 
	    begin 
	   branvar[46] <= branvar[46] + 1;
	 begin
	  data_out <=  out_reg[6];
	  next_bit <= 4;
	 end
	  end
	  4: 
	    begin 
	   branvar[47] <= branvar[47] + 1;
	 begin
	  data_out <=  out_reg[5];
	  next_bit <= 5;
	 end
	  end
	  5: 
	    begin 
	   branvar[48] <= branvar[48] + 1;
	 begin
	  data_out <=  out_reg[4];
	  next_bit <= 6;
	 end
	  end
	  6: 
	    begin 
	   branvar[49] <= branvar[49] + 1;
	 begin
	  data_out <=  out_reg[3];
	  next_bit <= 7;
	 end
	  end
	  7: 
	    begin 
	   branvar[50] <= branvar[50] + 1;
	 begin
	  data_out <=  out_reg[2];
	  next_bit <= 8;
	 end
	  end
	  8: 
	    begin 
	   branvar[51] <= branvar[51] + 1;
	 begin
	  data_out <=  out_reg[1];
	  next_bit <= 9;
	 end
	  end
	  9: 
	    begin 
	   branvar[52] <= branvar[52] + 1;
	 begin
	  data_out <=  out_reg[0];
	  next_bit <= 1;
	 end
	  end
	  1: 
	    begin 
	   branvar[53] <= branvar[53] + 1;
	 begin
	  data_out <= 1;
	  next_bit <= 0;
	  tx_end <= 1;
	 end
	  end
	 endcase
	  tx_conta <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[54] <= branvar[54] + 1;   
	  tx_conta <= (  tx_conta + 1);
	  end
	 end
	  end
	 else 
	 begin  
	   branvar[55] <= branvar[55] + 1;   
	   end 
	 end
	 end
	  end
	 end
	  endmodule 
