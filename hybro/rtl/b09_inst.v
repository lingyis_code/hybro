`timescale 1ns/10ps
module b09 (clock , reset , x , y );

	  input  clock;
	  input  reset;
	  input  x;
	  output y ;
	  wire clock;
	  wire reset;
	  wire x;
	  reg y;
	  reg [7:0] branvar[0:100];
	  reg [8 : 0] d_in;
	  reg [7 : 0] d_out;
          reg [7 : 0] old;
	  reg [1 : 0] stato;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  stato = 0;
	  d_in <= 0;
	  d_out <= 0;
	  old <= 0;
	  y <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( stato)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  stato = 1;
	  d_in <= 0;
	  d_out <= 0;
	  old <= 0;
	  y <= 0;
	 end
	  end
	  1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 if(  d_in[0] ) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  old <= d_in[8:1];
	  y <= 1;
	  d_out <= d_in[8:1];
	  d_in <= 9'h100;
	  stato = 2;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 begin
	  d_in <= { x, d_in[8:1]};
	  stato = 1;
	 end
	  end
	  end
	  2: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 begin
	 if( d_in[0]) 
	  begin 
	    branvar[7] <= branvar[7] + 1; 
	    begin
	     y <= 0;
	     stato = 3;
	    end
	  end
	 else 
	  begin 
	   branvar[8] <= branvar[8] + 1;   
	   begin
	     d_out <= d_out[7:1];
	     y <=  d_out[0];
	     stato = 2;
	   end
	 end
	  d_in <= { x,d_in[8:1]};
	 end
	end
	  3: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 if((  d_in[0] )) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	 if(d_in[8:1] ==  old) 
	  begin 
	 branvar[11] <= branvar[11] + 1; 
	 begin
	  d_in <= 0;
	  y <= 0;
	  stato = 3;
	 end
	  end
	 else 
	   begin 
	   branvar[12] <= branvar[12] + 1;   
	 begin
	  y <= 1;
	  d_out <= d_in[8:1];
	  d_in <= 9'h100;
	  stato = 2;
	 end
	  end
	  old <= d_in[8:1];
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 begin
	  d_in <= { x,d_in[8:1]};
	  y <= 0;
	  stato = 3;
	 end
	  end
	  end
	 endcase
	  end
	 end
	  endmodule 
