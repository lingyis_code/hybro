`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg reset;
reg line1;
reg line2;

wire outp;
wire overflw;

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;
b01 inst (
    .clock(clock),
    .reset(reset),
    .line1(line1),
    .line2(line2),
    .outp(outp),
    .overflw(overflow)
    );

initial begin
#1
  start_build_cfg("source_rtl/b01_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num = 0;
  inst.stato = 0;
//#10000000 $finish;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num == -1)
   inst.stato = 0;
  var_value = get_var_assignment("reset",frame_num+1);
  //if(frame_num == -1)
  //   reset = 1;
  //if(var_value == -1)
  //   reset = $random;
  //else 
  //   reset = var_value;
  if(frame_num<0)
    reset = 1;
  else reset = 0;

  var_value = get_var_assignment("line1",frame_num+1);
  if(var_value == -1)
     line1 = $random;
  else 
     line1 = var_value;
  
  var_value = get_var_assignment("line2",frame_num+1);
  if(var_value == -1)
     line2 = $random;
  else 
     line2 = var_value;

  //reset = get_var_assignment("reset",frame_num);
  //line1 = get_var_assignment("line1",frame_num);
  //line2 = get_var_assignment("line2",frame_num);
  i = i + 3;
  frame_num = frame_num + 1;
  #5
  if(frame_num==9)
   begin
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(25, 3, pat_num);
       $finish;
     end
     frame_num=-1;
   end
  //if(i>6000)
  // begin
  //  #4;
  //  //solve_constraint();
  //  $finish;
  // end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

