module test(clock, reset, in1, in2, out1, out2);

input clock;
input reset;
input [7:0] in1;
input [7:0] in2;
output [7:0] out1;
output [7:0] out2;
reg [7:0] out1;

assign out2[6:0] = out1[6:0] && in1[6:0];
assign out2[7] = 1'b1;

always @ (posedge clock)
  if(reset)
   out1 <= 8'h0;
  else if(in1 > in2)
   out1 <= in1 && in2;
  else
   out1 <= in1 || in2;



endmodule
