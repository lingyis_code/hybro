
module b04 (CLOCK , RESET , RESTART , AVERAGE , ENABLE , DATA_IN , DATA_OUT );

	  input  CLOCK;
	  input  RESET;
	  input  RESTART;
	  input  AVERAGE;
	  input  ENABLE;
	  input [7 : 0] DATA_IN;
	  output [7 : 0] DATA_OUT;
	  wire CLOCK;
	  wire RESET;
	  wire RESTART;
	  wire AVERAGE;
	  wire ENABLE;
	  wire [7 : 0] DATA_IN;
	  reg [7 : 0] DATA_OUT;
	  reg [7:0] branvar[0:100];
	  reg [1 : 0] stato;
	  reg [7 : 0] RMAX, RMIN, RLAST, REG1, REG2, REG3, REG4, REGD;
	  reg [8 : 0] temp;
	  reg RES, AVE, ENA;
	 always @ (posedge  CLOCK) 
	 begin
	 if( RESET) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  stato <= 0;
	  RMAX <= 0;
	  RMIN <= 0;
	  RLAST <= 0;
	  REG1 <= 0;
	  REG2 <= 0;
	  REG3 <= 0;
	  REG4 <= 0;
	  DATA_OUT <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 begin
	  RES <=  RESTART;
	  ENA <=  ENABLE;
	  AVE <=  AVERAGE;
	 case( stato)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	  stato <= 1;
	  end
	  1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	  RMAX <=  DATA_IN;
	  RMIN <=  DATA_IN;
	  REG1 <= 0;
	  REG2 <= 0;
	  REG3 <= 0;
	  REG4 <= 0;
	  RLAST <= 0;
	  DATA_OUT <= 0;
	  stato <= 2;
	 end
	  end
	  2: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	 begin
	 if( ENA) 
	  begin 
	 branvar[5] <= branvar[5] + 1; 
	  RLAST <=  DATA_IN;
	  end
	 else 
	 begin  
	   branvar[6] <= branvar[6] + 1;   
	   end 
	 if( RES) 
	  begin 
	 branvar[7] <= branvar[7] + 1; 
	 begin
	  temp <= ( { RMAX[7], RMAX} + { RMIN[7], RMIN});
	  REGD <= (  temp & 127);
	 if( temp[8]) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	  DATA_OUT <= (-((- REGD) >> 1));
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	  DATA_OUT <= ( REGD >> 1);
	  end
	 end
	  end
	 else 
	   begin 
	   branvar[10] <= branvar[10] + 1;   
	 if( ENA) 
	  begin 
	 branvar[11] <= branvar[11] + 1; 
	 begin
	 if( AVE) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	  DATA_OUT <=  REG4;
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 begin
	  temp <= ( { DATA_IN[7], DATA_IN} + { REG4[7], REG4});
	  REGD <= (  temp & 127);
	 if( temp[8]) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	  DATA_OUT <= (-((- REGD) >> 1));
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	  DATA_OUT <= ( REGD >> 1);
	  end
	 end
	  end
	 end
	  end
	 else 
	   begin 
	   branvar[16] <= branvar[16] + 1;   
	  DATA_OUT <=  RLAST;
	  end
	  end
	  temp <= ( { RMAX[7], RMAX} - { DATA_IN[7], DATA_IN});
	 if( temp[8]) 
	  begin 
	 branvar[17] <= branvar[17] + 1; 
	 begin
	  RMAX <=  DATA_IN;
	  RMIN <=  DATA_IN;
	 end
	  end
	 else 
	   begin 
	   branvar[18] <= branvar[18] + 1;   
	 begin
	  temp <= ( { DATA_IN[7], DATA_IN} - { RMIN[7], RMIN});
	 end
	  end
	  REG4 <=  REG3;
	  REG3 <=  REG2;
	  REG2 <=  REG1;
	  REG1 <=  DATA_IN;
	  stato <= 2;
	 end
	  end
	 endcase
	 end
	  end
	 end
	  endmodule 
