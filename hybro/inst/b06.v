// b06.v
// Verilog version, rewritten by Li Shen, Aug 2002
// Institute of Computing Technology, Chinese Academy of Sciences
// The original VHDL version is b06.vhd from Politecnico di Torino

module b06 (clock,reset,eql,cont_eql,cc_mux,uscite,enable_count,ackout);
input clock;
input reset;
input eql;
input cont_eql;
output [2:1] cc_mux, uscite;
output enable_count, ackout;
/////
wire clock;
wire reset;
wire eql;
wire cont_eql;
reg [2:1] cc_mux, uscite;
reg enable_count, ackout;
/////
parameter s_init=0, s_wait=1, s_enin=2, s_enin_w=3;
parameter s_intr=4, s_intr_1=5, s_intr_w=6;
parameter cc_nop=2'b01, cc_enin=2'b01, cc_intr=2'b10;
parameter cc_ackin=2'b11, out_norm=2'b01;
reg [2:0] state;
/////
always @(posedge clock)
begin
  if (reset)
  begin
	state = s_init;
	cc_mux <= 2'b00;
	enable_count <= 0;
	ackout <= 0;
	uscite <= 2'b00;
  end
  else
  begin
    if (cont_eql)
    begin
      ackout <= 0;
      enable_count <= 0;
    end
    else
    begin
      ackout <= 1;
      enable_count <= 1;
    end
	
    case (state)
    s_init:
    begin
      uscite <= out_norm;
      cc_mux <= cc_enin;
      state = s_wait;
    end
    s_wait:
      if (eql)
      begin
        uscite <= 2'b00;
	cc_mux <= cc_ackin;
	state = s_enin;
      end
      else
      begin
	uscite <= out_norm;
	cc_mux <= cc_intr;
	state = s_intr_1;
      end
    s_intr_1:
      if (eql)
      begin
	uscite <= 2'b00;
	cc_mux <= cc_ackin;
	state = s_intr;
      end
      else
      begin
	uscite <= out_norm;
	cc_mux <= cc_enin;
	state = s_wait;
      end
    s_enin:
      if (eql)
      begin
	uscite <= 2'b00;
	cc_mux <= cc_ackin;
	state = s_enin;
      end
      else
      begin
	uscite <= 2'b01;
	ackout <= 1;
	enable_count <= 1;
	cc_mux <= cc_enin;
	state = s_enin_w;
      end
    s_enin_w:
      if (eql)
      begin
	uscite <= 2'b01;
	cc_mux <= cc_enin;
	state = s_enin_w;
      end
      else
      begin
	uscite <= out_norm;
	cc_mux <= cc_enin;
	state = s_wait;
      end
    s_intr:
      if (eql)
      begin
	uscite <= 2'b00;
	cc_mux <= cc_ackin;
	state = s_intr;
      end
      else
      begin
	uscite <= 2'b11;
	cc_mux <= cc_intr;
	state = s_intr_w;
      end
    s_intr_w:
      if (eql)
      begin
	uscite <= 2'b11;
	cc_mux <= cc_intr;
	state = s_intr_w;
      end
      else
      begin
	uscite <= out_norm;
	cc_mux <= cc_enin;
	state = s_wait;
      end
    endcase
  end
end
endmodule
