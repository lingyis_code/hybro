
module or1200_dc_fsm (clk , rst , dc_en , dcdmmu_cycstb_i , dcdmmu_ci_i , dcpu_we_i , dcpu_sel_i , tagcomp_miss , biudata_valid , biudata_error , start_addr , saved_addr , dcram_we , biu_read , biu_write , first_hit_ack , first_miss_ack , first_miss_err , burst , tag_we , dc_addr );

	  input  clk;
	  input  rst;
	  input  dc_en;
	  input  dcdmmu_cycstb_i;
	  input  dcdmmu_ci_i;
	  input  dcpu_we_i;
	  input [3 : 0] dcpu_sel_i;
	  input  tagcomp_miss;
	  input  biudata_valid;
	  input  biudata_error;
	  input [31 : 0] start_addr;
	  output [31 : 0] saved_addr;
	  output [3 : 0] dcram_we;
	  output biu_read ;
	  output biu_write ;
	  output first_hit_ack ;
	  output first_miss_ack ;
	  output first_miss_err ;
	  output burst ;
	  output tag_we ;
	  output [31 : 0] dc_addr;
	  reg [31 : 0] saved_addr_r;
	  reg [7:0] branvar[0:100];
	  reg [2 : 0] state;
	  reg [2 : 0] cnt;
	  reg hitmiss_eval;
	  reg store;
	  reg load;
	  reg cache_inhibit;
	  wire first_store_hit_ack;
	 always @ (or (posedge  clk) (posedge  rst))
	 begin
	 if( rst) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  state <= 0;
	  saved_addr_r <= 32'b00000000000000000000000000000000;
	  hitmiss_eval <= 1'b0;
	  store <= 1'b0;
	  load <= 1'b0;
	  cnt <= 3'b000;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( state)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 if(( (  dc_en &  dcdmmu_cycstb_i) &  dcpu_we_i)) 
	  begin 
	 branvar[3] <= branvar[3] + 1; 
	 begin
	  state <= 3;
	  saved_addr_r <=  start_addr;
	  hitmiss_eval <= 1'b1;
	  store <= 1'b1;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[4] <= branvar[4] + 1;   
	 if((  dc_en &  dcdmmu_cycstb_i)) 
	  begin 
	 branvar[5] <= branvar[5] + 1; 
	 begin
	  state <= 1;
	  saved_addr_r <=  start_addr;
	  hitmiss_eval <= 1'b1;
	  store <= 1'b0;
	  load <= 1'b1;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[6] <= branvar[6] + 1;   
	 begin
	  hitmiss_eval <= 1'b0;
	  store <= 1'b0;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	  end
	  end
	  1: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	 begin
	 if((  dcdmmu_cycstb_i &  dcdmmu_ci_i)) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	  cache_inhibit <= 1'b1;
	  end
	 else 
	 begin  
	   branvar[9] <= branvar[9] + 1;   
	   end 
	 if( hitmiss_eval) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	  saved_addr_r[31:13] <= (start_addr[31:13];
	  end
	 else 
	 begin  
	   branvar[11] <= branvar[11] + 1;   
	   end 
	 if(( ( (  hitmiss_eval & (!  dcdmmu_cycstb_i)) ||  biudata_error) || ( (  cache_inhibit |  dcdmmu_ci_i) &  biudata_valid))) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  state <= 0;
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 if((  tagcomp_miss &  biudata_valid)) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  state <= 2;
	  saved_addr_r[3:2] <= ( (saved_addr_r[3:2] + 32'd1);
	  hitmiss_eval <= 1'b0;
	  cnt <= 2;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if(( (!  tagcomp_miss) & (!  dcdmmu_ci_i))) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  state <= 0;
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	  hitmiss_eval <= 1'b0;
	  end
	  end
	  end
	 end
	  end
	  2: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	 begin
	 if(( biudata_valid && ( (  cnt[2] ||  cnt[1]) ||  cnt[0]))) 
	  begin 
	 branvar[19] <= branvar[19] + 1; 
	 begin
	  cnt <= (  cnt - 1);
	  saved_addr_r[3:2] <= ( (saved_addr_r[3:2] + 32'd1);
	 end
	  end
	 else 
	   begin 
	   branvar[20] <= branvar[20] + 1;   
	 if( biudata_valid) 
	  begin 
	 branvar[21] <= branvar[21] + 1; 
	 begin
	  state <= 0;
	  load <= 1'b0;
	 end
	  end
	 else 
	 begin  
	   branvar[22] <= branvar[22] + 1;   
	   end 
	  end
	 end
	  end
	  3: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	 begin
	 if((  dcdmmu_cycstb_i &  dcdmmu_ci_i)) 
	  begin 
	 branvar[24] <= branvar[24] + 1; 
	  cache_inhibit <= 1'b1;
	  end
	 else 
	 begin  
	   branvar[25] <= branvar[25] + 1;   
	   end 
	 if( hitmiss_eval) 
	  begin 
	 branvar[26] <= branvar[26] + 1; 
	  saved_addr_r[31:13] <= (start_addr[31:13];
	  end
	 else 
	 begin  
	   branvar[27] <= branvar[27] + 1;   
	   end 
	 if(( ( (  hitmiss_eval & (!  dcdmmu_cycstb_i)) ||  biudata_error) || ( (  cache_inhibit |  dcdmmu_ci_i) &  biudata_valid))) 
	  begin 
	 branvar[28] <= branvar[28] + 1; 
	 begin
	  state <= 0;
	  hitmiss_eval <= 1'b0;
	  store <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[29] <= branvar[29] + 1;   
	 if((  tagcomp_miss &  biudata_valid)) 
	  begin 
	 branvar[30] <= branvar[30] + 1; 
	 begin
	  state <= 4;
	  hitmiss_eval <= 1'b0;
	  store <= 1'b0;
	  load <= 1'b1;
	  cnt <= 3;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[31] <= branvar[31] + 1;   
	 if( biudata_valid) 
	  begin 
	 branvar[32] <= branvar[32] + 1; 
	 begin
	  state <= 0;
	  hitmiss_eval <= 1'b0;
	  store <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[33] <= branvar[33] + 1;   
	  hitmiss_eval <= 1'b0;
	  end
	  end
	  end
	 end
	  end
	  4: 
	    begin 
	   branvar[34] <= branvar[34] + 1;
	 begin
	 if(( biudata_valid && ( (  cnt[2] ||  cnt[1]) ||  cnt[0]))) 
	  begin 
	 branvar[35] <= branvar[35] + 1; 
	 begin
	  cnt <= (  cnt - 1);
	  saved_addr_r[3:2] <= ( (saved_addr_r[3:2] + 32'd1);
	 end
	  end
	 else 
	   begin 
	   branvar[36] <= branvar[36] + 1;   
	 if( biudata_valid) 
	  begin 
	 branvar[37] <= branvar[37] + 1; 
	 begin
	  state <= 0;
	  load <= 1'b0;
	 end
	  end
	 else 
	 begin  
	   branvar[38] <= branvar[38] + 1;   
	   end 
	  end
	 end
	  end
	 endcase
	  end
	 end
	  endmodule 
