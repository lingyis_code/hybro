`timescale 1ns/10ps

module b1001 (clock , reset, opcode, dina, dinb, add1_tmp, dout);

	  input  clock;
	  input  reset;
 
       input  [3:0] opcode;
       input  [31:0] dina;
       input  [31:0] dinb;
       input  add1_tmp;
       output [31:0] dout;       

      
       reg [31:0] dout;       
       reg [31:0] dout_p1;
       reg [31:0] dout_p2;


       always @ (posedge  clock)
       begin
         if(reset)
           begin
             dout_p1 <= 32'h0;
           end  
         else
           begin
             case(opcode)
               4'h0:
                 begin
                   dout_p1 <= dina;
                 end
               4'h1:
                 begin
                   dout_p1 <= dina + dinb + add1_tmp;
                 end
               4'h2:
                 begin
                   dout_p1 <= dina - dinb - add1_tmp; 
                 end
               4'h3:
                 begin
                   dout_p1 <= dina + dinb; 
                 end
               4'h4:
                 begin
                   dout_p1 <= dina - dinb;
                 end
               4'h5:
                 begin
                   dout_p1 <= dina * dinb;
                 end
               4'h6:
                 begin
                   dout_p1 <= dina / dinb;  
                 end
               4'h7:
                 begin
                   dout_p1 <= ~(dina & dinb); 
                 end
               4'h8:
                 begin
                   dout_p1 <= dina & dinb;
                 end
               4'h9:
                 begin
                   dout_p1 <= dina | dinb; 
                 end
               4'ha:
                 begin
                   dout_p1 <= dina ^ dinb;
                 end
               4'hb:
                 begin
                   dout_p1 <= ~dina;
                 end
               4'hc:
                 begin
                   dout_p1 <= dina << dinb;
                 end
               4'hd:
                 begin
                   dout_p1 <= dina >> dinb;
                 end
               4'he:
                 begin
                   dout_p1 <= dina % dinb;
                 end
               4'hf:
                 begin
                   dout_p1 <= dinb;
                 end
             endcase 
           end
       end

       always @ (posedge  clock)
       begin
         if(reset)
           begin
             dout_p2 <= 32'h0;
             dout <= 32'h0;
           end
         else
           begin
             dout_p2 <= dout_p1;
             dout <= dout_p2;
           end
       end
endmodule 
