%{
/* 
 * Verliog Lexical Tokenizer
 * 
 * original credit to vl2mv code from VIS 2.0 distribution
 * -Vinod.
 */

#include "vlg2sExpr.h"
#include "vlgParser.h"

  extern char *yytext;
  
  char brep[MAXSTRLEN];
  int bexp0, bexp1;
  
  void binbin(char *instr, char *outstr);
  void decbin(char *instr, char *outstr);
  void octbin(char *instr, char *outstr);
  void hexbin(char *instr, char *outstr);
  char *utol(char *str);
  void encodebit(char *instr, int *part1, int *part0);

  void commenthandler (void);

%}

%a 3000
%e 1700
%k 1000
%n 700
%o 4000
%p 5000


Space       [\f\n\r\t\b ]
Alpha       [a-zA-Z]
AlphaU      [a-zA-Z_]
AlphaNum    [a-zA-Z0-9]
AlphaNumU   [a-zA-Z0-9_]
Digit       [0-9]
DigitU      [0-9_]
Number      {Digit}{DigitU}*
Decimal     ({Number})?'[dD][ ]*{Number}
Octal       ({Number})?'[oO][ ]*[0-7xXzZ?][0-7xXzZ?_]*
Hexdecimal  ({Number})?'[hH][ ]*[0-9a-fA-FxXzZ?][0-9a-fA-FxXzZ?_]*
Binary      ({Number})?'[bB][ ]*[01xXzZ?][01xXzZ?_]*

%%

{Space}+           { continue; }

"//".*$            { continue; }
"/*"               { commenthandler(); }
`timescale[^\n]*\n { continue; }
`include[^\n]*\n   { continue; }
`define[^\n]*\n    { continue; }

">="               { return YYGEQ;  }
"=<"               { return YYLEQ;  }
"&&"               { return YYLOGAND;    }
"||"               { return YYLOGOR;     }
"==="              { return YYCASEEQUALITY;  }
"=="               { return YYLOGEQUALITY;   }
"!=="              { return YYCASEINEQUALITY; }
"!="               { return YYLOGINEQUALITY; }
"^~"               { return YYLOGXNOR; }
"~^"               { return YYLOGXNOR; }
"~&"               { return YYLOGNAND;      }
"~|"               { return YYLOGNOR;       }
"<<"               { return YYLSHIFT;      }
">>"               { return YYRSHIFT;      }
"?:"               { return YYCONDITIONAL; }

\"[^"]*\"          { strcpy(yylval.id,yytext);
                     return YYSTRING; }

always             { return YYALWAYS; }
"*>"               { return YYALLPATH; }
and                { strcpy(yylval.id,yytext); return YYAND; }
assign             { return YYASSIGN; }
begin              { return YYBEGIN; }
buf                { strcpy(yylval.id,yytext); return YYBUF; }
bufif0             { strcpy(yylval.id,yytext); return YYBUFIF0; }
bufif1             { strcpy(yylval.id,yytext); return YYBUFIF1; }
case               { return YYCASE; }
casex              { return YYCASEX; }
casez              { return YYCASEZ; }
cmos               { strcpy(yylval.id,yytext); return YYCMOS; }
deassign           { return YYDEASSIGN; }
default            { return YYDEFAULT; }
defparam           { return YYDEFPARAM; }
disable            { return YYDISABLE; }
edge               { return YYEDGE; }
else               { return YYELSE; }
end                { return YYEND; }
endcase            { return YYENDCASE; }
endfunction        { return YYENDFUNCTION; }
endmodule          { return YYENDMODULE; }
endprimitive       { return YYENDPRIMITIVE; }
endspecify         { return YYENDSPECIFY; }
endtask            { return YYENDTASK; }
enum               { return YYENUM; }
event              { return YYEVENT; }
for                { return YYFOR; }
forever            { return YYFOREVER; }
fork               { return YYFORK; }
function           { return YYFUNCTION; }
highz0             { return YYHIGHZ0; }
highz1             { return YYHIGHZ1; }
if                 { return YYIF; }
initial            { return YYINITIAL; }
inout              { return YYINOUT; }
input              { return YYINPUT; }
integer            { return YYINTEGER; }
join               { return YYJOIN; }
large              { return YYLARGE; }
"=>"               { return YYLEADTO; }
macromodule        { return YYMACROMODULE; }
medium             { return YYMEDIUM; }
module             { return YYMODULE; }
mREG               { return YYMREG; }
"<="               { return YYNBASSIGN;  }
nand               { strcpy(yylval.id,yytext); return YYNAND; }
negedge            { return YYNEGEDGE; }
nmos               { strcpy(yylval.id,yytext); return YYNMOS; }
nor                { strcpy(yylval.id,yytext); return YYNOR; }
not                { strcpy(yylval.id,yytext); return YYNOT; }
notif0             { strcpy(yylval.id,yytext); return YYNOTIF0; }
notif1             { strcpy(yylval.id,yytext); return YYNOTIF1; }
or                 { strcpy(yylval.id,yytext); return YYOR; }
output             { return YYOUTPUT; }
parameter          { return YYPARAMETER; }
pmos               { strcpy(yylval.id,yytext); return YYPMOS; }
posedge            { return YYPOSEDGE; }
primitive          { return YYPRIMITIVE; }
pull0              { return YYPULL0; }
pull1              { return YYPULL1; }
pulldown           { strcpy(yylval.id,yytext); return YYPULLDOWN; }
pullup             { strcpy(yylval.id,yytext); return YYPULLUP; }
rcmos              { strcpy(yylval.id,yytext); return YYRCMOS; }
real               { return YYREAL; }
reg                { return YYREG; }
repeat             { return YYREPEAT; }
"->"               { return YYRIGHTARROW; }
rnmos              { strcpy(yylval.id,yytext); return YYRNMOS; }
rpmos              { strcpy(yylval.id,yytext); return YYRPMOS; }
rtran              { strcpy(yylval.id,yytext); return YYRTRAN; }
rtranif0           { strcpy(yylval.id,yytext); return YYRTRANIF0; }
rtranif1           { strcpy(yylval.id,yytext); return YYRTRANIF1; }
scalered           { return YYSCALARED; }
small              { return YYSMALL; }
specify            { return YYSPECIFY; }
specparam          { return YYSPECPARAM; }
strong0            { return YYSTRONG0; }
strong1            { return YYSTRONG1; }
supply0            { return YYSUPPLY0; }
supply1            { return YYSUPPLY1; }
swire              { return YYSWIRE; }
task               { return YYTASK; }
teslaTimer         { strcpy(yylval.id,yytext); return YYTESLATIMER; }
time               { return YYTIME; }
tran               { strcpy(yylval.id,yytext); return YYTRAN; }
tranif0            { strcpy(yylval.id,yytext); return YYTRANIF0; }
tranif1            { strcpy(yylval.id,yytext); return YYTRANIF1; }
tri                { return YYTRI; }
tri0               { return YYTRI0; }
tri1               { return YYTRI1; }
triand             { return YYTRIAND; }
trior              { return YYTRIOR; }
trireg             { return YYTRIREG; }
typedef            { return YYTYPEDEF; }
vectored           { return YYVECTORED; }
wait               { return YYWAIT; }
wand               { return YYWAND; }
weak0              { return YYWEAK0; }
weak1              { return YYWEAK1; }
while              { return YYWHILE; }
wire               { return YYWIRE; }
wor                { return YYWOR; }
xnor               { strcpy(yylval.id,yytext); return YYXNOR; }
xor                { strcpy(yylval.id,yytext); return YYXOR; }

\$setup                { return YYsysSETUP; }
\$NDset                { return YYsysND; }
\$ND                   { return YYsysND; }
\${AlphaU}{AlphaNumU}* { return YYsysID; }

`{AlphaU}{AlphaNumU}* { 
                                strcpy(yylval.id, yytext); 
                                return YYLID; 
                              }
{AlphaU}{AlphaNumU}* { 
                                strcpy(yylval.id, yytext); 
                                return YYLID; 
                              }
\\[^\n\t\b\r\f ]*    { 
                                strcpy(yylval.id, yytext); 
                                return YYLID; 
                              }

{Number}*\.{Number}+ { strcpy(yylval.id,yytext);
                       return YYRNUMBER; }
{Number}+\.{Number}* { strcpy(yylval.id,yytext);
                       return YYRNUMBER; }
{Number}             { strcpy(yylval.id,yytext);
                       return YYINUMBER; }
{Binary}             { 
                                binbin(yytext, brep); 
                                encodebit(brep, &bexp1, &bexp0); 
				//sprintf(yylval.id, "((width %d) (binary %s))",(int) strlen(brep),brep);
				sprintf(yylval.id, "%d'b%s",(int) strlen(brep),brep);
                                return YYINUMBER_BIT; 
                              }
{Octal}              { 
                                octbin(yytext, brep); 
                                encodebit(brep, &bexp1, &bexp0); 
				//sprintf(yylval.id, "((width %d) (octal %o))",(int) strlen(brep),(int) strtol(brep,NULL,2));
				sprintf(yylval.id, "%d'o%o",(int) strlen(brep),(int) strtol(brep,NULL,2));
                                return YYINUMBER_BIT; 
                              }
{Decimal}            { 
                                decbin(yytext, brep); 
                                encodebit(brep, &bexp1, &bexp0); 
				sprintf(yylval.id, "%d'd%d",(int) strlen(brep),(int) strtol(brep,NULL,2));
                                return YYINUMBER_BIT; 
                              }
{Hexdecimal}         { 
                                hexbin(yytext, brep); 
                                encodebit(brep, &bexp1, &bexp0); 
				sprintf(yylval.id, "%d'h%x",(int) strlen(brep),(int) strtol(brep,NULL,2));
                                return YYINUMBER_BIT; 
                              }
`{AlphaU}{AlphaNumU}*{Binary} { strcpy(yylval.id, yytext); return YYLID; } 
`{AlphaU}{AlphaNumU}*{Octal} { strcpy(yylval.id, yytext); return YYLID; } 
`{AlphaU}{AlphaNumU}*{Decimal} { strcpy(yylval.id, yytext); return YYLID; } 
`{AlphaU}{AlphaNumU}*{Hexdecimal} { strcpy(yylval.id, yytext); return YYLID; } 

.             { return yytext[0]; }

%%    

#undef yywrap
int yywrap() {
  /* This line does not harm any code -- it removes the warning of having defined yyunput and never used it */
  yyunput (0,NULL); 
  return 1;
}

void binbin(char *instr, char *outstr)
{
    char *cp, *firstcp;
    int blen = 0, bpos=0;

    blen = atoi(instr);
    blen = (blen==0) ? MAXBITNUM : blen;
    firstcp = strpbrk(instr, "bB")+1;
    cp = instr + strlen(instr) - 1;
    outstr[blen] = '\0';
    for (bpos = blen-1; bpos >=0 && cp >= firstcp; cp--) {
	if (*cp != '_' && *cp != ' ') {
	    outstr[bpos] = *cp;
	    bpos--;
	}
    }
    for (; bpos >=0; bpos--) {
	outstr[bpos] = '0';
    }
}

char *hexnum[16] = {"0000","0001","0010","0011",
		    "0100","0101","0110","0111",
		    "1000","1001","1010","1011",
                    "1100","1101","1110","1111"};

void decbin(char *instr, char *outstr)
{
    char *firstcp, buf[MAXSTRLEN];
    int num, blen = 0;

    utol(instr);
    blen = atoi(instr);
    blen = (blen==0) ? MAXBITNUM : blen;
    firstcp = strpbrk(instr, "dD")+1;
    while (*firstcp==' ') firstcp++;
    num = atoi(firstcp); /* don't put x, z, ? in decimal string */
    sprintf(buf, "%d'h%x", blen, num);
    hexbin(buf, outstr);
}

void octbin(char *instr, char *outstr)
{
    char *cp, *firstcp;
    int blen = 0, bpos=0, i;

    utol(instr);
    blen = atoi(instr);
    blen = (blen==0) ? MAXBITNUM : blen;
    firstcp = strpbrk(instr, "oO")+1;
    cp = instr + strlen(instr) - 1;
    outstr[blen] = '\0';
    for (bpos = blen-1; bpos >=0 && cp >= firstcp; cp--) {
	if (*cp != '_' && *cp != ' ') {
	    for (i = 3; i >= 1; i--) {
		if (bpos >= 0) {
		    if (*cp=='x' || *cp=='z' || *cp=='?') {
			outstr[bpos] = *cp;
			bpos--;
		    } else if (isdigit(*cp)) {
			outstr[bpos] = hexnum[*cp-'0'][i];
			bpos--;
		    }
		}
	    }
	}
    }
    for (; bpos >=0; bpos--) {
	outstr[bpos] = '0';
    }
}

void hexbin(char *instr, char *outstr)
{
    char *cp, *firstcp;
    int blen = 0, bpos=0, i;

    utol(instr);
    blen = atoi(instr);
    blen = (blen==0) ? MAXBITNUM : blen;
    firstcp = strpbrk(instr, "hH")+1;
    cp = instr + strlen(instr) - 1;
    outstr[blen] = '\0';
    for (bpos = blen-1; bpos >=0 && cp >= firstcp; cp--) {
	if (*cp != '_' && *cp != ' ') {
	    for (i = 3; i >= 0; i--) {
		if (bpos >= 0) {
		    if (*cp=='x' || *cp=='z' || *cp=='?') {
			outstr[bpos] = *cp;
			bpos--;
		    } else if (isdigit(*cp)) {
			outstr[bpos] = hexnum[*cp-'0'][i];
			bpos--;
		    } else if (isxdigit(*cp)) {
			outstr[bpos] = hexnum[*cp-'a'+10][i];
			bpos--;
		    }
		}
	    }
	}
    }
    for (; bpos >=0; bpos--) {
	outstr[bpos] = '0';
    }
}

char *utol(char *str)
{
    char *cp;
    
    for (cp = str; *cp!='\0'; cp++) {
	if (isupper(*cp)) *cp = tolower(*cp);
    }
    return str;
}

void encodebit(char *instr, int *part1, int *part0)
{
    int i, bmask;

    *part1 = *part0 = 0;
    i = strlen(instr);
    i = (i > MAXBITNUM) ? MAXBITNUM-1 : i-1;
    for (bmask = 1; i>=0; bmask <<= 1, i--) {
	switch (instr[i]) {
	case '1': *part1 |= bmask; break;
	case '0': *part0 |= bmask; break;
	case 'x': 
	case '?': *part1 |= bmask; *part0 |= bmask; break;
	case 'z': break;
	}
    }
}

void commenthandler (void) 
{
    int done = 0;
    int level = 0;
    char c;

    while (!done) {
      if ((c = yyinput()) == '*') {
	if ((c = yyinput()) == '/') {
	  done = (level-- == 0); 
	} else {
	  yyunput(0,&c);
	}
      } else if (c == '/') {
	if ((c = yyinput()) == '*') {
	  level++;
	} else {
	  yyunput(0,&c);
	}
      }
    }
}



/* TODO
 *
 * 1. handle `define macros
 * 2. handle `include files
 *
 */
