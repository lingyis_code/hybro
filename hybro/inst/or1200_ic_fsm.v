`include "timescale.v"

module or1200_ic_fsm(
	// Clock and reset
	clk, rst,

	// Internal i/f to top level IC
	ic_en, icqmem_cycstb_i, icqmem_ci_i,
	tagcomp_miss, biudata_valid, biudata_error, start_addr, saved_addr,
	icram_we, biu_read, first_hit_ack, first_miss_ack, first_miss_err,
	burst, tag_we
);

//
// I/O
//
input				clk;
input				rst;
input				ic_en;
input				icqmem_cycstb_i;
input				icqmem_ci_i;
input				tagcomp_miss;
input				biudata_valid;
input				biudata_error;
input	[31:0]			start_addr;
output	[31:0]			saved_addr;
output	[3:0]			icram_we;
output				biu_read;
output				first_hit_ack;
output				first_miss_ack;
output				first_miss_err;
output				burst;
output				tag_we;

//
// Internal wires and regs
//
reg	[31:0]			saved_addr_r;
reg	[1:0]			state;
reg	[2:0]			cnt;
reg				hitmiss_eval;
reg				load;
reg				cache_inhibit;

parameter OR1200_ICFSM_IDLE	= 2'd0;
parameter OR1200_ICFSM_CFETCH	= 2'd1;
parameter OR1200_ICFSM_LREFILL3	= 2'd2;
parameter OR1200_ICFSM_IFETCH	= 2'd3;

assign icram_we = {4{biu_read & biudata_valid & !cache_inhibit}};
assign tag_we = biu_read & biudata_valid & !cache_inhibit;

assign biu_read = (hitmiss_eval & tagcomp_miss) | (!hitmiss_eval & load);

assign saved_addr = saved_addr_r;

assign first_hit_ack = (state == OR1200_ICFSM_CFETCH) & hitmiss_eval & !tagcomp_miss & !cache_inhibit & !icqmem_ci_i;
assign first_miss_ack = (state == OR1200_ICFSM_CFETCH) & biudata_valid;
assign first_miss_err = (state == OR1200_ICFSM_CFETCH) & biudata_error;

//
// Assert burst when doing reload of complete cache line
//
assign burst = (state == OR1200_ICFSM_CFETCH) & tagcomp_miss & !cache_inhibit
		| (state == OR1200_ICFSM_LREFILL3);

//
// Main IC FSM
//
always @(posedge clk or posedge rst) begin
	if (rst) begin
		state <=   OR1200_ICFSM_IDLE;
		saved_addr_r <=   32'h0;
		hitmiss_eval <=   1'b0;
		load <=   1'b0;
		cnt <=   3'b000;
		cache_inhibit <=   1'b0;
	end
	else
	case (state)	// synopsys parallel_case
		OR1200_ICFSM_IDLE :
			if (ic_en & icqmem_cycstb_i) begin		// fetch
				state <=   OR1200_ICFSM_CFETCH;
				saved_addr_r <=   start_addr;
				hitmiss_eval <=   1'b1;
				load <=   1'b1;
				cache_inhibit <=   1'b0;
			end
			else begin							// idle
				hitmiss_eval <=   1'b0;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
		OR1200_ICFSM_CFETCH: begin	// fetch
			if (icqmem_cycstb_i & icqmem_ci_i)
				cache_inhibit <=   1'b1;
			if (hitmiss_eval)
				saved_addr_r[31:13] <=   start_addr[31:13];
			if ((!ic_en) ||
			    (hitmiss_eval & !icqmem_cycstb_i) ||	// fetch aborted (usually caused by IMMU)
			    (biudata_error) ||				// fetch terminated with an error
			    (cache_inhibit & biudata_valid)) begin	// fetch from cache-inhibited page
				state <=   OR1200_ICFSM_IDLE;
				hitmiss_eval <=   1'b0;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else if (tagcomp_miss & biudata_valid) begin	// fetch missed, finish current external fetch and refill
				state <=   OR1200_ICFSM_LREFILL3;
				saved_addr_r[3:2] <=   saved_addr_r[3:2] + 1'd1;
				hitmiss_eval <=   1'b0;
				cnt <=   OR1200_ICLS-2;
				cache_inhibit <=   1'b0;
			end
			else if (!tagcomp_miss & !icqmem_ci_i) begin	// fetch hit, finish immediately
				saved_addr_r <=   start_addr;
				cache_inhibit <=   1'b0;
			end
			else if (!icqmem_cycstb_i) begin	// fetch aborted (usually caused by exception)
				state <=   OR1200_ICFSM_IDLE;
				hitmiss_eval <=   1'b0;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else						// fetch in-progress
				hitmiss_eval <=   1'b0;
		end
		OR1200_ICFSM_LREFILL3 : begin
			if (biudata_valid && (|cnt)) begin		// refill ack, more fetchs to come
				cnt <=   cnt - 3'd1;
				saved_addr_r[3:2] <=   saved_addr_r[3:2] + 1'd1;
			end
			else if (biudata_valid) begin			// last fetch of line refill
				state <=   OR1200_ICFSM_IDLE;
				saved_addr_r <=   start_addr;
				hitmiss_eval <=   1'b0;
				load <=   1'b0;
			end
		end
	endcase
end

endmodule
