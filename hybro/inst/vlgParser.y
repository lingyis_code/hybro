/*
 * Verliog Grammar
 *
 * original credit to vl2mv code from VIS 2.0 distribution
 * too many chnages to the parser to document them all
 * -Vinod.
*/


%{
#include "vlg2sExpr.h"

  void yyerror(char *str);
  void YYTRACE(char *str);

  extern char brep[];
  extern int bexp0, bexp1;

  char current_range[MAXSTRLEN];
  struct wire_width_tuple wire_width_table[MAXNUMVARS];
  int num_width_table_entries;
  //struct 
  _cfg_node * cfg_temp;
  _cfg_node * cfg_top;
  _cfg_node * cfg_table[MAXNUMCFGS];
  int cfg_stack_pointer = 0;
  FILE * fp_out;
  int bran_var_idx=0;
  bool bran_flag=true;
  bool parameter_start=false;
  _para_table current_para_table[MAXPARAS]; 
  int para_table_idx=0;
%}


%union {
  char id[MAXSTRLEN];
  _cfg_node * NODE;
}


%start source_text


%token <id> YYLID                      
%token <id> YYINUMBER                 
%token <id> YYINUMBER_BIT                 
%token <id> YYRNUMBER                 
%token <id> YYSTRING                  
%token YYALLPATH                 
%token YYALWAYS                  
%token <id> YYAND                     
%token YYASSIGN                  
%token YYBEGIN                   
%token <id> YYBUF                    
%token <id> YYBUFIF0                  
%token <id> YYBUFIF1                  
%token YYCASE                    
%token YYCASEX                   
%token YYCASEZ                   
%token <id> YYCMOS                    
%token YYCONDITIONAL             
%token YYDEASSIGN                
%token YYDEFAULT                 
%token YYDEFPARAM                
%token YYDISABLE                 
%token YYEDGE                    
%token YYEND                     
%token YYENDCASE                 
%token YYENDMODULE               
%token YYENDFUNCTION             
%token YYENDPRIMITIVE            
%token YYENDSPECIFY              
%token YYENDTABLE                
%token YYENDTASK                 
%token YYENUM                    
%token YYEVENT                   
%token YYFOR                     
%token YYFOREVER                 
%token YYFORK                    
%token YYFUNCTION                
%token YYGEQ                     
%token YYHIGHZ0                  
%token YYHIGHZ1                  
%token YYIF                      
%nonassoc YYELSE
%nonassoc YYLOWERTHANELSE
%token YYINITIAL                 
%token YYINOUT                   
%token YYINPUT                   
%token YYINTEGER                 
%token YYJOIN                    
%token YYLARGE                   
%token YYLEADTO                  
%token YYLEQ                     
%token YYLOGAND                  
%token YYCASEEQUALITY            
%token YYCASEINEQUALITY          
%token YYLOGNAND                 
%token YYLOGNOR                  
%token YYLOGOR                   
%token YYLOGXNOR                 
%token YYLOGEQUALITY             
%token YYLOGINEQUALITY           
%token YYLSHIFT                  
%token YYMACROMODULE             
%token YYMEDIUM                  
%token YYMODULE                  
%token YYMREG   	         
%token <id> YYNAND                    
%token YYNBASSIGN                
%token YYNEGEDGE                 
%token <id> YYNMOS                    
%token <id> YYNOR                     
%token <id> YYNOT                     
%token <id> YYNOTIF0                  
%token <id> YYNOTIF1                  
%token <id> YYOR                      
%token YYOUTPUT                  
%token YYPARAMETER               
%token <id> YYPMOS                    
%token YYPOSEDGE                 
%token YYPRIMITIVE               
%token YYPULL0                   
%token YYPULL1                   
%token <id> YYPULLUP                  
%token <id> YYPULLDOWN                
%token <id> YYRCMOS                   
%token YYREAL                    
%token YYREG                     
%token YYREPEAT                  
%token YYRIGHTARROW              
%token <id> YYRNMOS                   
%token <id> YYRPMOS                   
%token YYRSHIFT                  
%token <id> YYRTRAN                   
%token <id> YYRTRANIF0                
%token <id> YYRTRANIF1                
%token YYSCALARED                
%token YYSMALL                   
%token YYSPECIFY                 
%token YYSPECPARAM               
%token YYSTRONG0                 
%token YYSTRONG1                 
%token YYSUPPLY0                 
%token YYSUPPLY1                 
%token YYSWIRE                   
%token YYTABLE                   
%token YYTASK                    
%token <id> YYTESLATIMER              
%token YYTIME                    
%token <id> YYTRAN                    
%token <id> YYTRANIF0                 
%token <id> YYTRANIF1                 
%token YYTRI                     
%token YYTRI0                    
%token YYTRI1                    
%token YYTRIAND                  
%token YYTRIOR                   
%token YYTRIREG                  
%token YYuTYPE                   
%token YYTYPEDEF                 
%token YYVECTORED                
%token YYWAIT                    
%token YYWAND                    
%token YYWEAK0                   
%token YYWEAK1                   
%token YYWHILE                   
%token YYWIRE                    
%token YYWOR                     
%token <id> YYXNOR                    
%token <id> YYXOR                     
%token YYsysSETUP                
%token YYsysID                   
%token YYsysND                   

%right YYCONDITIONAL
%right '?' ':'
%left YYOR
%left YYLOGOR
%left YYLOGAND
%left '|'
%left '^' YYLOGXNOR
%left '&'
%left YYLOGEQUALITY YYLOGINEQUALITY YYCASEEQUALITY YYCASEINEQUALITY
%left '<' YYLEQ '>' YYGEQ YYNBASSIGN
%left YYLSHIFT YYRSHIFT
%left '+' '-'
%left '*' '/' '%'
%right '~' '!' YYUNARYOPERATOR

%type <id> identifier
%type <id> variable_list
%type <id> register_variable
%type <id> register_variable_list
%type <id> primary
%type <id> expression
%type <id> expression_list
%type <id> input_declaration
%type <id> output_declaration
%type <id> inout_declaration
%type <id> reg_declaration
%type <id> net_declaration
%type <id> range
%type <id> range_opt
%type <id> expandrange
%type <id> expandrange_opt
%type <id> module_or_primitive_instantiation
%type <id> name_of_module_or_primitive
%type <NODE> named_port_connection
%type <id> module_port_connection
%type <NODE> named_port_connection_list
%type <id> module_port_connection_list
%type <id> module_connection_list
%type <id> module_or_primitive_instance
%type <id> module_or_primitive_instance_list
%type <id> gate_instantiation
%type <id> gatetype
%type <id> terminal
%type <id> terminal_list
%type <id> name_of_gate_instance
%type <id> gate_instance
%type <id> gate_instance_list
%type <id> concatenation
%type <id> multiple_concatenation
%type <id> assignment_list
%type <id> continuous_assign
%type <id> assignment
%type <id> lvalue
%type <id> port_list_opt
%type <id> port_list
%type <id> port
%type <id> port_expression_opt
%type <id> port_expression

%type <id> port_ref_list
%type <id> port_reference
%type <id> name_of_register
%type <id> mintypmax_expression
%type <id> mintypmax_expression_list
%type <NODE> statement
%type <NODE> if_branch
%type <id> statement_opt
%type <NODE> statement_clr
%type <NODE> seq_block
%type <id> name_of_block
%type <NODE> case_item_eclr
%type <NODE> case_item
%type <id> module_or_primitive_option_clr
%type <id> module_or_primitive_option
%type <id> delay_or_parameter_value_assignment
%type <id> event_expression
%type <id> event_control
%type <id> ored_event_expression

%%

source_text
        :
          {
	    modstats = NULL;
	    YYTRACE("source_text:");
          }
        | source_text description
          {
              YYTRACE("source_text: source_text description");
          }
        ;

description 
	: module
          {
              YYTRACE("description: module");
          }
	| primitive
          {
              YYTRACE("description: primitive");
          }
	| type_declaration
          {
	      YYTRACE("module_item: type_declaration");
          }
	;

type_declaration
        : YYTYPEDEF type_specifier type_name ';'
          {
	      YYTRACE("type_declaration: YYTYPEDEF typesepcifier type_name ';'");
	  }
        ;

module 
	: YYMODULE YYLID 
          {
	    thismodule = make_module ($2);
            fprintf (stdout, "\n=============================start to instrument================================");
	    fprintf (fp_out, "\nmodule %s ",$2);
            bran_flag=true;
	    //fprintf (fp_out, "\n'((|%s|\n\t   (type . module)\n",$2);
	    num_width_table_entries = 0;
          } 
          port_list_opt ';'
          {
	    fprintf (fp_out,"(%s );\n",$4);
            //fprintf (fp_out,"\t   (portorder %s)\n",$4);
          }
          module_item_clr 
	  YYENDMODULE
          {
	    //	    print_modstats ();
	    //print_wwt ();
	    fprintf (fp_out, "\n\t  endmodule \n");
            fprintf( stdout, "\n===============================end instrument=================================== \n");
            bran_var_idx = 0;
	    YYTRACE("module: YYMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          }
	| YYMACROMODULE YYLID port_list_opt ';'
	module_item_clr 
	  YYENDMODULE
          {
	    YYTRACE("module: YYMACROMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          }
	;

port_list_opt
	:
          {
	    strcpy ($$,"");
	    YYTRACE("port_list_opt:");
          }
	| '(' port_list ')'
          {
	    sprintf ($$,$2);
	    YYTRACE("port_list_opt: '(' port_list ')'");
          }
	;

port_list
	: port
          {
	    sprintf ($$,$1);
	    YYTRACE("port_list: port");
          }
	| port_list ',' port
          {
	    sprintf ($$,"%s , %s",$1, $3);
	    //sprintf ($$,"%s %s",$1, $3);
	    YYTRACE("port_list: port_list ',' port");
          }
	;

port
	: port_expression_opt
          {
	    sprintf ($$,$1);
	    YYTRACE("port: port_expression_opt");
          }
	| '.' YYLID 
          {
	  }
          '(' port_expression_opt ')'
          {
	    sprintf ($$,"(namedport %s %s)",$2,$5);
	    YYTRACE("port: ',' YYLID '(' port_expression_opt ')'");
          }
	;

port_expression_opt
	:
           {
	     strcpy ($$,"");
	     YYTRACE("port_expression_opt:");
           }
	|  port_expression
           {
	     sprintf ($$,$1);
	     YYTRACE("port_expression_opt: port_expression");
           }
	;

port_expression
	: port_reference
          {
	    sprintf ($$,$1);
	    YYTRACE("port_expression: port_reference");
          }
	| '{' port_ref_list '}'
          {
	    sprintf ($$,$2);
	    YYTRACE("port_expression: '{' port_ref_list '}'");
          }
        ;

port_ref_list
	: port_reference
          {
	    sprintf ($$,$1);
	    YYTRACE("port_ref_list: port_reference");
          }
	| port_ref_list ',' port_reference
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("port_ref_list: port_ref_list ',' port_reference");
          }
	;

/*
port_reference
	: YYLID 
          {
	  }
          port_reference_arg
          {
              YYTRACE("port_reference: YYLID port_reference_arg");
          }
	;

port_reference_arg
        :
          {
	    YYTRACE("port_reference_arg:");
          }
        | '[' expression ']' 
          {
	    YYTRACE("port_reference_arg: '[' expression ']'");
          }
        | '[' expression ':' expression ']' 
          {
              YYTRACE("port_reference_arg: '[' expression ':' expression ']'");
          }
        ;
*/
// Combining port_reference and port_reference_arg
port_reference
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    //sprintf ($$,"%s|",$1);
	    YYTRACE("port_reference_arg:");
          }
        | YYLID '[' expression ']' 
          {
	    sprintf ($$,"(%s |%s|)",$3,$1);
	    YYTRACE("port_reference_arg: '[' expression ']'");
          }
        | YYLID '[' expression ':' expression ']' 
          {
	    sprintf ($$,"((%s %s) |%s|)",$3,$5,$1);
	    YYTRACE("port_reference_arg: '[' expression ':' expression ']'");
          }
        ;


module_item_clr
        :
          {
	    YYTRACE("module_item_clr:");
          }
        | module_item_clr module_item
          {
	    YYTRACE("module_item_clr: module_item_clr module_item");
          }
        ;

module_item
	: parameter_declaration
          {
              YYTRACE("module_item: parameter_declaration");
          }
	| input_declaration
          {
	    YYTRACE("module_item: input_declaration");
          }
	| output_declaration
          {
	    YYTRACE("module_item: output_declaration");
          }
	| inout_declaration
          {
              YYTRACE("module_item: inout_declaration");
          }
	| net_declaration//
          {              //
	    YYTRACE("modu//le_item: net_declaration");
          }
	| reg_declaration
          {
              int bvar=0;
              if(bran_flag) 
              {
                //for(bvar=0;bvar<=100;bvar++)
	        fprintf (fp_out,"\n\t  reg [7:0] branvar[0:1000];");
                bran_flag = false;
              }
	    YYTRACE("module_item: reg_declaration");
          }
	| time_declaration
          {
              YYTRACE("module_item: time_declaration");
          }
	| integer_declaration
          {
              YYTRACE("module_item: integer_declaration");
          }
	| real_declaration
          {
              YYTRACE("module_item: real_declaration");
          }
	| event_declaration
          {
              YYTRACE("module_item: event_declaration");
          }
	| gate_instantiation
          {
              YYTRACE("module_item: gate_instantiation");
          }
	| module_or_primitive_instantiation
          {
              YYTRACE("module_item: module_or_primitive_instantiation");
          }
	| parameter_override
          {
              YYTRACE("module_item: parameter_override");
          }
	| continuous_assign
          {
              YYTRACE("module_item: continous_assign");
          }
	| specify_block
          {
              YYTRACE("module_item: specify_block");
          }
	| initial_statement
          {
              YYTRACE("module_item: initial_statement");
          }
	| always_statement
          {
              YYTRACE("module_item: always_statement");
          }
	| task
          {
              YYTRACE("module_item: task");
          }
	| function
          {
              YYTRACE("module_item: function");
          }
	;

primitive
	: YYPRIMITIVE YYLID 
          {
          }
          '(' port_list ')' ';'
	  	primitive_declaration_eclr
		table_definition
	  YYENDPRIMITIVE
          {
              YYTRACE("primitive: YYPRMITIVE YYLID '(' variable_list ')' ';' primitive_declaration_eclr table_definition YYENDPRIMITIVE");
          }
	;

primitive_declaration_eclr
        : primitive_declaration
          {
              YYTRACE("primitive_declaration_eclr: primitive_declaration");
          }
        | primitive_declaration_eclr primitive_declaration
          {
              YYTRACE("primitive_declaration_eclr: primitive_declaration_eclr primitive_declaration");
          }
        ;

primitive_declaration
	: output_declaration
          {
	    YYTRACE("primitive_declaration: output_declaration");
          }
	| reg_declaration
          {
              YYTRACE("primitive_decalration: reg_declaration");
          }
	| input_declaration
          {
              YYTRACE("primitive_decalration: input_declaration");
          }
	;

table_definition
	: YYTABLE table_entries YYENDTABLE
          {
              YYTRACE("table_definition: YYTABLE table_entries YYENDTABLE");
          }
	;

table_entries
	: combinational_entry_eclr
          {
              YYTRACE("table_definition: combinational_entry_eclr");
          }
	| sequential_entry_eclr
          {
              YYTRACE("table_definition: sequential_entry_eclr");
          }
	;

combinational_entry_eclr
	: combinational_entry
          {
              YYTRACE("combinational_entry_eclr: combinational_entry");
          }
	| combinational_entry_eclr combinational_entry
          {
              YYTRACE("combinational_entry_eclr: combinational_entry_eclr combinational_entry");
          }
	;

combinational_entry
	: input_list ':' output_symbol ';'
          {
              YYTRACE("combinational_entry: input_list ':' output_symbol ';'");
          }
	;

sequential_entry_eclr
	: sequential_entry
          {
              YYTRACE("sequential_entry_eclr: sequential_entry");
          }
	| sequential_entry_eclr sequential_entry
          {
              YYTRACE("sequential_entry_eclr: sequential_entry_eclr sequential_entry");
          }
	;

sequential_entry
	: input_list ':' state ':' next_state ';'
          {
              YYTRACE("sequential_entry: input_list ':' state ':' next_state ';'");
          }
	;

input_list
	: level_symbol_or_edge_eclr
          {
              YYTRACE("input_list: level_symbol_or_edge_eclr");
          }
	;

level_symbol_or_edge_eclr
        : level_symbol_or_edge
          {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge");
          }
        | level_symbol_or_edge_eclr level_symbol_or_edge
          {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge_eclr level_symbol_or_edge");
          }
        ;

level_symbol_or_edge
        : level_symbol
          {
              YYTRACE("level_symbol_or_edge: level_symbol");
          }
        | edge
          {
              YYTRACE("level_symbol_or_edge: edge");
          }
        ;

edge
	: '(' level_symbol level_symbol ')'
          {
              YYTRACE("edge: '(' level_symbol level_symbol ')'");
          }
	| edge_symbol
          {
              YYTRACE("edge: edge_symbol");
          }
	;

state
	: level_symbol
          {
              YYTRACE("state: level_symbol");
          }
	;

next_state
	: output_symbol
          {
              YYTRACE("next_state: output_symbol");
          }
	| '-'
          {
              YYTRACE("next_state: '_'");
          }
	;

output_symbol
	: '0'
          {
              YYTRACE("output_symbol: '0'");
          }
        | '1'
          {
              YYTRACE("output_symbol: '1'");
          }
        | 'x'
          {
              YYTRACE("output_symbol: 'x'");
          }
        | 'X'
          {
              YYTRACE("output_symbol: 'X'");
          }
        ;

level_symbol
	: '0'
          {
              YYTRACE("level_symbol: '0'");
          }
        | '1'
          {
              YYTRACE("level_symbol: '1'");
          }
        | 'x'
          {
              YYTRACE("level_symbol: 'x'");
          }
        | 'X'
          {
              YYTRACE("level_symbol: 'X'");
          }
        | '?'
          {
              YYTRACE("level_symbol: '?'");
          }
        | 'b'
          {
              YYTRACE("level_symbol: 'b'");
          }
        | 'B'
          {
              YYTRACE("level_symbol: 'B'");
          }
        ;

edge_symbol
	: 'r'
          {
              YYTRACE("edge_symbol: 'r'");
          }
        | 'R'
          {
              YYTRACE("edge_symbol: 'R'");
          }
        | 'f'
          {
              YYTRACE("edge_symbol: 'f'");
          }
        | 'F'
          {
              YYTRACE("edge_symbol: 'F'");
          }
        | 'p'
          {
              YYTRACE("edge_symbol: 'p'");
          }
        | 'P'
          {
              YYTRACE("edge_symbol: 'P'");
          }
        | 'n'
          {
              YYTRACE("edge_symbol: 'n'");
          }
        | 'N'
          {
              YYTRACE("edge_symbol: 'N'");
          }
        | '*'
          {
              YYTRACE("edge_symbol: '*'");
          }
        ;

task
	: YYTASK YYLID  
          {
	  }
          ';' tf_declaration_clr statement_opt 
          YYENDTASK
          {
              YYTRACE("YYTASK YYLID ';' tf_declaration_clr statement_opt YYENDTASK");
          }
	;

function
        : YYFUNCTION range_or_type_opt YYLID 
          {
	  }
          ';' tf_declaration_eclr statement_opt
          YYENDFUNCTION
          {
              YYTRACE("YYFUNCTION range_or_type_opt YYLID ';' tf_declaration_eclr statement_opt YYENDFUNCTION");
          }
        ;

range_or_type_opt
        :
          {
              YYTRACE("range_or_type_opt:");
          }
        | range_or_type
          {
              YYTRACE("range_or_type_opt: range_or_type");
          }
        ;

range_or_type
        : range
          {
              YYTRACE("range_or_type: range");
          }
        | YYINTEGER
          {
              YYTRACE("range_or_type: YYINTEGER");
          }
        | YYREAL
          {
              YYTRACE("range_or_type: YYREAL");
          }
        ;

tf_declaration_clr
        :
          {
              YYTRACE("tf_declaration_clr:");
          }
        | tf_declaration_clr tf_declaration
          {
              YYTRACE("tf_declaration_clr: tf_declaration_clr tf_declaration");
          }
        ;

tf_declaration_eclr
        : tf_declaration
          {
              YYTRACE("tf_declaration_eclr: tf_declaration");
          }
        | tf_declaration_eclr tf_declaration
          {
              YYTRACE("tf_declaration_eclr: tf_decalration_eclr tf_declaration");
          }
        ;

tf_declaration
        : parameter_declaration
          {
              YYTRACE("tf_declaration: parameter_decalration");
          }
        | input_declaration
          {
              YYTRACE("tf_declaration: input_declaration");
          }
        | output_declaration
          {
              YYTRACE("tf_declaration: output_declaration");
          }
        | inout_declaration
          {
              YYTRACE("tf_declaration: inout_declaration");
          }
        | reg_declaration
          {
              //int bvar=0;
              //for(bvar=0;bvar<=100;bvar++)
	      //  fprintf (fp_out,"\n\t  reg [7:0] branvar_%d;",bvar);
              YYTRACE("tf_declaration: reg_declaration");
          }
        | time_declaration
          {
              YYTRACE("tf_declaration: time_declaration");
          }
        | integer_declaration
          {
              YYTRACE("tf_declaration: integer_declaration");
          }
        | real_declaration
          {
              YYTRACE("tf_declaration: real_declaration");
          }
        | event_declaration
          {
              YYTRACE("tf_declaration: event_declaration");
          }
        ;

type_name
        : YYLID
          {
	      YYTRACE("type_name: YYLID");
	  }
        ;

type_specifier
        : enum_specifier
          {
	      YYTRACE("type_specifier: enum_specifier ';'");
	  }
        ;

enum_specifier
        : YYENUM {} enum_name enum_lst_opt
          {
	      YYTRACE("enum_specifier: YYENUM enum_name enum_lst_opt");
	  }
        | YYENUM {} '{' enumerator_list '}'       
          {
	      YYTRACE("enum_specifier: YYENUM '{' enumerator_list '}'");
	  }
        ;

enum_name
        : YYLID
          {
	      YYTRACE("enum_name: YYLID");
	  }
        ;

enum_lst_opt
        : '{' enumerator_list '}'
          {
	      YYTRACE("enum_lst_opt: '{' enumerator_list '}'");
	  }
        | 
          {
	      YYTRACE("enum_lst_opt: ");
	  }
        ;

enumerator_list
        : enumerator
          {
	      YYTRACE("enumerator_list: enumerator");
	  }
        | enumerator_list ',' enumerator
          {
	      YYTRACE("enumerator_list: enumerator_list ',' enumerator");
	  }
        ;

enumerator
        : YYLID
          {
	      YYTRACE("enumerator: YYLID");
	  }
        | YYLID {} '=' expression
          {
	      YYTRACE("enumerator: YYLID '=' expression");
	  }
        ;



type_decorator_opt
        : YYuTYPE
          {
	      YYTRACE("type_decorator_opt: YYuTYPE");
	  }
        | 
          {
	      YYTRACE("type_decorator_opt: ");
	  }
        ;

parameter_declaration
        : YYPARAMETER { parameter_start = true; } assignment_list ';'
          {
              parameter_start = false;
              YYTRACE("parameter_declaration: YYPARAMETER assignment_list ';'");
          }
        ;

input_declaration
        : YYINPUT range_opt variable_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (fp_out,"\n\t  input  %s;",$3);
	    else 
	      fprintf (fp_out,"\n\t  input %s %s;",$2,$3);
	    YYTRACE("input_declaration: YYINPUT range_opt variable_list ';'");
          }
        ;

output_declaration
        : YYOUTPUT range_opt variable_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (fp_out,"\n\t  output %s ;",$3);
	    else
	      fprintf (fp_out,"\n\t  output %s %s;",$2,$3);
	    YYTRACE("output_declaration: YYOUTPUT range_opt variable_list ';'");
          }
        ;

inout_declaration
        : YYINOUT range_opt variable_list ';'
          {
	    if (strcmp($2,"")==0)
	      fprintf (fp_out,"\t   (inouts %s)\n",$3);
	    else
	      fprintf (fp_out,"\t   (inouts %s %s)\n",$2,$3);
	    YYTRACE("inout_declaration: YYINOUT range_opt variable_list ';'");
          }
        ;

net_declaration
        : type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt 
            assignment_list ';'
          {
              YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt assignment_list ';'");
          }
        | type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt 
            variable_list ';'
          {
	    if (strcmp($4,"")==0)
	      fprintf (fp_out,"\n\t  wire %s;",$6);
	    else
	      fprintf (fp_out,"\n\t  wire %s %s;",$4,$6);
	    YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt variable_list ';'");
          }
        | type_decorator_opt YYTRIREG charge_strength_opt expandrange_opt delay_opt 
            variable_list ';'
          {
              YYTRACE("net_declaration: type_decorator_opt YYTRIREG charge_strength_opt expandrange_opt delay_opt variable_list ';'");
          }
        ;


nettype
        : YYSWIRE
          {
              YYTRACE("nettype: YYSWIRE");
          }
        | YYWIRE
          {
              YYTRACE("nettype: YYWIRE");
          }
        | YYTRI
          {
              YYTRACE("nettype: YYTRI");
          }
        | YYTRI1
          {
              YYTRACE("nettype: YYTRI1");
          }
        | YYSUPPLY0
          {
              YYTRACE("nettype: YYSUPPLY0");
          }
        | YYWAND
	  {
              YYTRACE("nettype: YYWAND");
	  }
        | YYTRIAND
          {
	      YYTRACE("nettype: YYTRIAND");
	  }
        | YYTRI0
          {
	      YYTRACE("nettype: YYTRI0");
	  }
        | YYSUPPLY1
          {
	      YYTRACE("nettype: YYSUPPLY1");
	  } 
        | YYWOR
          {
	      YYTRACE("nettype: YYWOR");
          }
        | YYTRIOR
          {
              YYTRACE("nettype: YYTRIOR");
          }
        ;


expandrange_opt
        :
          {
	    strcpy ($$,"");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("expandrange_opt:");
          }
        | expandrange
          {
	    sprintf ($$,$1);
	    YYTRACE("expandrange_opt: expandrange");
          }
        ;

expandrange
        : range
          {
	    sprintf ($$,$1);
	    YYTRACE("expandrange: range");
          }
        | YYSCALARED range
          {
	    strcpy ($$,"");
	    YYTRACE("expandrange: YYSCALARED range");
          }
        | YYVECTORED range
          {
	    strcpy ($$,"");
	    YYTRACE("expandrange: YYVECTORED range");
          }
        ;

reg_declaration
        : type_decorator_opt YYREG range_opt register_variable_list ';'
          {
	    if (strcmp($3,"")==0)
	      fprintf (fp_out,"\n\t  reg %s;",$4);
	    else
	      fprintf (fp_out,"\n\t  reg %s %s;",$3,$4);
	    YYTRACE("reg_declaration: type_decorator_opt YYREG range_opt register_variable_list ';'");
          }
        | type_decorator_opt YYMREG range_opt register_variable_list ';'
          {
              YYTRACE("reg_declaration: type_decorator_opt YYMREG range_opt register_variable_list ';'");
          }
        ;

time_declaration
        : YYTIME register_variable_list ';'
          {
              YYTRACE("time_declaration: YYTIME register_variable_list ';'");
          }
        ;

integer_declaration
        : YYINTEGER register_variable_list ';'
          {
              YYTRACE("integer_declaration: YYINTEGER register_variable_list ';'");
          }
        ;

real_declaration
        : YYREAL variable_list ';'
          {
              YYTRACE("real_declaration: YYREAL variable_list ';'");
          }
        ;

event_declaration
        : YYEVENT name_of_event_list ';'
          {
              YYTRACE("event_declaration: YYEVENT name_of_event_list ';'");
          }
        ;

continuous_assign
        : YYASSIGN {fprintf(fp_out,"\n\t assign ");} 
          drive_strength_opt delay_opt assignment_list ';'
          {
            cfg_temp = build_normal_node($5,NULL,NULL,NULL,NULL);
            cfg_temp->node_type = BLK_AS;
            push_cfg_table(cfg_temp);
	    //fprintf (fp_out,"\t   (assign %s)\n",$5);
	    YYTRACE("continuous_assign: YYASSIGN drive_strength_opt delay_opt assignment_list ';'");
          }
        ;

parameter_override
        : YYDEFPARAM assignment_list ';'
          {
	    fprintf (fp_out,"\t   (defparam %s)\n",$2);
	    YYTRACE("parameter_override: YYDEFPARAM assign_list ';'");
          }
        ;

variable_list
        : identifier
          {
	    sprintf ($$,"%s",$1);
	    put_wwt_entry ($1, current_range);
	    YYTRACE("variable_list: identifier");
          }
        | variable_list ',' identifier
          {
	    sprintf ($$,"%s, %s",$1,$3);
	    put_wwt_entry ($3, current_range);
	    YYTRACE("variable_list: variable_list ',' identifier");
          }
        ;

register_variable_list
        : register_variable
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("register_variable_list: register_variable");
          }
        | register_variable_list ',' register_variable
          {
	    sprintf ($$,"%s, %s",$1,$3);
	    YYTRACE("register_variable_list: register_variable_list ',' register_variable");
          }
        ;
        
register_variable
        : name_of_register
          {
	    sprintf ($$,$1);
	    put_wwt_entry ($1, current_range);
	    YYTRACE("register_variable: name_of_register");
          }
        | name_of_register '[' expression ':' expression ']'
          {
	    sprintf ($$,"((%s %s) %s)",$3,$5,$1);
	    {char s[MAXSTRLEN]; sprintf (s,"(%s %s)",$3,$5);
	    put_wwt_entry ($1, s);}
	    YYTRACE("register_variable: name_of_register '[' expression ':' expression ']'");
          }
        ;

name_of_register
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("name_of_register: YYLID");
          }
        ;

name_of_event_list
        : name_of_event
          {
              YYTRACE("name_of_event_list: name_of_event");
          }
        | name_of_event_list ',' name_of_event
          {
              YYTRACE("name_of_event_list: name_of_event_list ',' name_of_event");
          }
        ;

name_of_event
        : YYLID
          {
              YYTRACE("name_of_event: YYLID");
          }
        ;

charge_strength_opt
        :
          {
              YYTRACE("charge_strength_opt:");
          }
        | charge_strength
          {
              YYTRACE("charge_strength_opt: charge_strength");
          }
        ;

charge_strength
        : '(' YYSMALL ')'
          {
              YYTRACE("charge_strength: '(' YYSMALL ')'");
          }
        | '(' YYMEDIUM ')'
          {
              YYTRACE("charge_strength: '(' YYMEDIUM ')'");
          }
        | '(' YYLARGE ')'
          {
              YYTRACE("charge_strength: '(' YYLARGE ')'");
          }
        ;

drive_strength_opt
        :
          {
              YYTRACE("drive_strength_opt:");
          }
        | drive_strength
          {
              YYTRACE("drive_strength_opt: drive_strength");
          }
        ;

drive_strength
        : '(' strength0 ',' strength1 ')'
          {
              YYTRACE("drive_strength: '(' strength0 ',' strength1 ')'");
          }
        | '(' strength1 ',' strength0 ')'
          {
              YYTRACE("drive_strength: '(' strength1 ',' strength0 ')'");
          }
        ;

strength0
        : YYSUPPLY0
          {
              YYTRACE("strength0: YYSUPPLY0");
          }
        | YYSTRONG0
          {
              YYTRACE("strength0: YYSTRONG0");
          }
        | YYPULL0
          {
              YYTRACE("strength0: YYPULL0");
          }
        | YYWEAK0
          {
              YYTRACE("strength0: YYWEAK0");
          }
        | YYHIGHZ0
          {
              YYTRACE("strength0: YYHIGHZ0");
          }
        ;

strength1
        : YYSUPPLY1
          {
              YYTRACE("strength1: YYSUPPLY1");
          }
        | YYSTRONG1
          {
              YYTRACE("strength1: YYSTRONG1");
          }
        | YYPULL1
          {
              YYTRACE("strength1: YYPULL1");
          }
        | YYWEAK1
          {
              YYTRACE("strength1: YYWEAK1");
          }
        | YYHIGHZ1
          {
              YYTRACE("strength1: YYHIGHZ1");
          }
        ;

range_opt
        :
          {
	    strcpy ($$,"");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("range_opt:");
          }
        | range
          {
	    sprintf ($$,$1);
	    sprintf (current_range, $1);
	    YYTRACE("range_opt: range");
          }
        ;

range
        : '[' expression ':' expression ']'
          {
	    sprintf ($$,"[%s : %s]",$2,$4);
	    sprintf (current_range,"(%s %s)",$2,$4);
              YYTRACE("range: '[' expression ':' expression ']'");
          }
        ;

assignment_list
        : assignment
          {
	    sprintf ($$,$1);
	    YYTRACE("assignment_list: assignment");
          }
        | assignment_list ',' assignment
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("assignment_list: assignment_list ',' assignment");
          }
        ;


gate_instantiation
        : gatetype drive_delay_clr gate_instance_list ';'
          {
	    fprintf (fp_out,"\t   (occs %s %s)\n",$1,$3);
	    YYTRACE("gate_instantiation: gatetype drive_delay_clr gate_instance_list ';'");
          }
        ;

drive_delay_clr
        :
          {
              YYTRACE("drive_delay_clr:");
          }
        | drive_delay_clr drive_delay
          {
              YYTRACE("drive_delay_clr: drive_delay_clr drive_delay");
          }
        ;

drive_delay
        : drive_strength
          {
              YYTRACE("drive_delay: drive_strength");
          }
        | delay
          {
              YYTRACE("drive_delay: delay");
          }
        ;

gatetype
        : YYAND
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYAND");
	  }
        | YYNAND
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNAND");
	  }
        | YYOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYOR");
	  }
        | YYNOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOR");
	  }
        | YYXOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYXOR");
	  }
        | YYXNOR
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYXNOR");
	  }
        | YYBUF
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYBUF");
	  }
        | YYBUFIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYBIFIF0");
	  }
        | YYBUFIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYBIFIF1");
	  }
        | YYNOT
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOT");
	  }
        | YYNOTIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOTIF0");
	  }
        | YYNOTIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNOTIF1");
	  }
        | YYPULLDOWN
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYPULLDOWN");
	  }
        | YYPULLUP
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYPULLUP");
	  }
        | YYNMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYNMOS");
	  }
        | YYPMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYPMOS");
	  }
        | YYRNMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRNMOS");
	  }
        | YYRPMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRPMOS");
	  }
        | YYCMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYCMOS");
	  }
        | YYRCMOS
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRCMOS");
	  }
        | YYTRAN
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTRAN");
	  }
        | YYRTRAN
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRTRAN");
	  }
        | YYTRANIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTRANIF0");
	  }
        | YYRTRANIF0
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRTRANIF0");
	  }
        | YYTRANIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTRANIF1");
	  }
        | YYRTRANIF1
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYRTRANIF1");
	  }
	| YYTESLATIMER
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("gatetype: YYTESLATIMER");
	  } 
        ;

gate_instance_list
        : gate_instance
          {
	    sprintf ($$,$1);
	    YYTRACE("gate_instance_list: gate_instance");
          }
        | gate_instance_list ',' gate_instance
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("gate_instance_list: gate_instance_list ',' gate_instance");
          }
        ;

gate_instance
        : '(' terminal_list ')'
          {
	    sprintf ($$,"(%s)",$2);
	    YYTRACE("gate_instance: '(' terminal_list ')'");
          }
        | name_of_gate_instance '(' terminal_list ')'
          {
	    sprintf ($$,"((occname %s) %s)",$1,$3);
	    YYTRACE("gate_instance: name_of_gate_instance '(' terminal_list ')'");
          }
        ;

name_of_gate_instance
        : YYLID
          {
	    sprintf ($$,"|%s|",$1);
	    YYTRACE("name_of_gate_instance: YYLID");
          }
        ;

terminal_list
        : terminal
          {
	    sprintf ($$,$1);
	    YYTRACE("terminal_list: terminal");
          }
        | terminal_list ',' terminal
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("terminal_list: terminal_list ',' terminal");
          }
        ;

terminal
        : expression
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("terminal: expression");
          }
        ;


module_or_primitive_instantiation
        : name_of_module_or_primitive {fprintf(fp_out, "\n\t  %s ", $1);} module_or_primitive_option_clr
             module_or_primitive_instance_list ';' {fprintf(fp_out, ";");}
          {
	    //if (strcmp($2,"")==0)
	    //  fprintf (fp_out,"\t   (%s %s)\n",$1,{$3); 
	    //else
	    //  fprintf (fp_out,"\t   (%s (options %s) %s)\n",$1,$2,$3); 
	    YYTRACE("module_or_primitive_instantiation: name_of_module_or_primitive module_or_primitive_option_clr module_or_primitive_instance_list ';'");
          }
        ;

name_of_module_or_primitive
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("name_of_module_or_primitive: YYLID");
          }
        ;

module_or_primitive_option_clr
        :
          {
	    strcpy ($$,"");
	    YYTRACE("module_or_primitive_option_clr:");
          }
        | module_or_primitive_option_clr module_or_primitive_option
          {
	    sprintf ($$, "%s %s", $1, $2);
	    YYTRACE("module_or_primitive_option_clr: module_or_primitive_option_clr module_or_primitive_option");
          }
        ;

module_or_primitive_option
        : drive_strength
          {
	    strcpy ($$,"");
	    YYTRACE("module_or_primitive_option:");
          }
        | delay_or_parameter_value_assignment
          {
	    strcpy($$,$1);
	    YYTRACE("module_or_primitive_option: delay");
          }
        ;

delay_or_parameter_value_assignment
        : '#' YYINUMBER    
          {
	    sprintf ($$, "(%s)", $2);
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER");
	  }
        | '#' YYINUMBER_BIT    
          {
	    sprintf ($$, "(%s)", $2);
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER_BIT");
	  }
        | '#' YYRNUMBER    
          {
	    sprintf ($$, "(%s)", $2);	
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYRNUMBER");
	  }
        | '#' identifier   
          {
	    sprintf ($$, "(%s)", $2);
	    YYTRACE("delay_or_parameter_value_assignment: '#' identifier");
	  }
        | '#' '(' mintypmax_expression_list ')' 
          {
	    sprintf ($$, "(%s)", $3);
	    YYTRACE("delay_or_parameter_value_assignment: '#' '(' mintypmax_expression_list ')'");
	  }
        ;

module_or_primitive_instance_list
        : module_or_primitive_instance
          {
	    sprintf ($$,$1);
            YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance");
          }
        | module_or_primitive_instance_list ',' module_or_primitive_instance
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance_list ',' module_or_primitive_instance");
          }
        ;


module_or_primitive_instance
        : '(' module_connection_list ')'
          {
	    sprintf ($$,"(%s)",$2);
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          }
//Hack! I'm pretty sure this is not allowed in synth verilog -- just to get mt_saq_array_ct.v to work! --VV
        | identifier {fprintf(fp_out,"%s (",$1);} '(' module_connection_list ')'  
          {
            fprintf(fp_out,")");
	    sprintf ($$," %s (%s)",$1,$4);
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          }
        ;

module_connection_list
        : module_port_connection_list
          {
	    sprintf ($$,$1);
	    YYTRACE("module_connection_list: module_port_connection_list");
          }
        | named_port_connection_list
          {
            push_cfg_table($1);
	    sprintf ($$,$1->exp_name);
	    YYTRACE("module_connection_list: named_port_connection_list");
          }
        ;

module_port_connection_list
        : module_port_connection
          {
	    sprintf ($$,$1);
	    YYTRACE("module_port_connection_list: module_port_connection");
          }
        | module_port_connection_list ',' module_port_connection
          {
	    sprintf ($$,"%s %s",$1,$3);
	    YYTRACE("module_port_connection_list: module_port_connection_list ',' module_port_connection");
          }
        ;

named_port_connection_list
        : named_port_connection
          {
	    //sprintf ($$,$1);
            $$=$1;
	    YYTRACE("named_port_connection_list: named_port_connection");
          }
        | named_port_connection_list ',' {fprintf(fp_out, ",");} named_port_connection
          {
	    //sprintf ($$,"%s %s",$1,$3);
            cfg_temp = $1;
            while(cfg_temp->next_node!=NULL)
              cfg_temp=cfg_temp->next_node;
            cfg_temp->next_node = $4;
            $$=$1;
	    YYTRACE("named_port_connection_list: named_port_connection_list ',' name_port_connection");
          }
        ;

module_port_connection
        :
          {
	    strcpy ($$,"");
	    YYTRACE("module_port_connection:");
          }
        | expression
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("module_port_connection: expression");
          }
        ;

named_port_connection
        : '.' identifier '(' expression ')'  {fprintf(fp_out,"\n\t .%s (%s)",$2,$4);}
          {
            char id_temp[MAXSTRLEN];
            sprintf (id_temp,"%s = %s",$2,$4);
            cfg_temp = build_normal_node(id_temp, NULL, NULL, NULL, NULL);
            cfg_temp->node_type = PORT_CONNECT;
            $$=cfg_temp;
	    //sprintf ($$,"(namedport %s %s)",$2,$4);
	    YYTRACE("named_port_connection: '.' identifier '(' expression ')'");
          }
        ;



initial_statement
        : YYINITIAL {} statement
          {
	    fprintf (fp_out,"\t   (initial\n\t\t%s\n\t   )\n",$3->exp_name);
	    YYTRACE("initial_statement: YYINITIAL statement");
          }
        ;

always_statement
        : YYALWAYS {} statement
          {
            push_cfg_table($3);
	    //fprintf (fp_out,"\n   always %s",$3->exp_name);
  	    YYTRACE("always_statement: YYALWAYS statement");
          }
        ;

statement_opt
        :
          {
	    strcpy ($$,"");
	    YYTRACE("statement_opt:");
          }
        | statement
          {
	    sprintf ($$,"%s",$1->exp_name);
	    YYTRACE("statement_opt: statement");
          }
        ;        

statement_clr
        :
          {
	    //strcpy ($$,"");
            //printf("return\n");
            $$=NULL;
	    YYTRACE("statement_clr:");
          }
        | statement_clr statement
          {
	    //sprintf ($$, "%s  %s", $1, $2);
            if($1!=NULL)
              set_next_node($1,$2);  
            if($1!=NULL) 
              $$=$1;
            else
              $$=$2;
            //==if($1!=NULL){
            //==  printf("......%s---next is",$1->exp_name);
            //==  printf("......%s\n",$1->next_node->exp_name);
            //==}
	    YYTRACE("statement_clr: statement_clr statement");
          }
        ;


statement
        : ';' 
          {
	    //strcpy ($$,"");
            //$$=(_cfg_node *)malloc(sizeof(_cfg_node));
            //strcpy($$->exp_name,"");
            $$=NULL;
	    YYTRACE("statement: ';'");
          }
        | assignment ';'
          {
	    //sprintf ($$->exp_name, "%s", $1);
            cfg_top = build_normal_node($1,NULL,NULL,NULL,NULL);
            cfg_top->node_type = BLK_AS;
            $$=cfg_top;
            //printf("deduce an statement : %s",$$->exp_name);
	    YYTRACE("statement: assignment ';'");
          }
        | if_branch
          {
          fprintf(fp_out, "\n\t else \n\t begin  \n\t   %s   \n\t   end ",get_next_inst_statement());
          
          }
        //==| YYIF '(' expression ')' statement 
        //==  {
        //==    cfg_temp = build_blank_node();
        //==    cfg_top = build_normal_node($3,$5,NULL,cfg_temp,NULL);
        //==    cfg_top->node_type = IFCOND_EXP;
        //==    set_next_node($5,cfg_temp);
        //==    $$=cfg_top;
	//==    //sprintf ($$, "%s", $1);
	//==    //sprintf ($$, "(if %s %s)", $3, $5);
	//==    YYTRACE("statement: YYIF '(' expression ')' statement");
        //==  }
        //==| YYIF '(' expression ')' statement YYELSE {fprintf(fp_out,"\n\t else");} statement 
        | if_branch YYELSE {fprintf(fp_out,"\n\t else \n\t   begin \n\t   %s   ",get_next_inst_statement() );} statement {fprintf(fp_out,"\n\t  end");}
          {
            //===cfg_temp = build_blank_node();
            //===cfg_top = build_normal_node($3,$5,NULL,cfg_temp,NULL);
            //===$5->brother_node=$8;
            //===cfg_top->node_type = IFELSE_EXP;
            //===set_next_node($5,cfg_temp);
            //===$$=cfg_top;
            //cfg_temp = build_blank_node();
            //cfg_top = build_normal_node($1->exp_name,$5,NULL,cfg_temp,NULL);
            $1->left_node->brother_node=$4;
            //cfg_top->node_type = IFELSE_EXP;
            //set_next_node($5,cfg_temp);
            $$=cfg_top;

            //==printf("----%s\n",$3);
            //==printf("----%s\n",$5->exp_name);
            //==printf("----%s\n",$5->next_node->exp_name);
            //==printf("----%s\n",$7->exp_name);
	    //sprintf ($$, "(if %s %s %s)", $3, $5, $7);
	    YYTRACE("statement: YYIF '(' expression ')' statement YYELSE statement");   
	  } 
        | YYCASE '(' expression ')' {fprintf(fp_out,"\n\t case(%s)",$3);} case_item_eclr YYENDCASE {fprintf(fp_out,"\n\t endcase");}
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node($3,$6,NULL,cfg_temp,NULL);
            cfg_top->node_type = CASE_EXP;
            set_next_node($6,cfg_temp); 
            $$=cfg_top;
            //cfg_temp=$5;
            //while(cfg_temp!=NULL){
            //  printf("++++%s \n",cfg_temp->exp_name);
            //  cfg_temp=cfg_temp->next_node;
            //}
	    //sprintf ($$, "(case %s %s)", $3, $5);
            //printf("");
	    YYTRACE("statement: YYCASE '(' expression ')' case_item_eclr YYENDCASE");
          }
        | YYCASEZ '(' expression ')' case_item_eclr YYENDCASE
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node($3,$5,NULL,cfg_temp,NULL);
            cfg_top->node_type = CASE_EXP;
            set_next_node($5,cfg_temp); 
            $$=cfg_top;
            //sprintf ($$, "(casez %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEZ '(' expression ')' case_item_eclr YYENDCASE"); 
          }
        | YYCASEX '(' expression ')' case_item_eclr YYENDCASE
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node($3,$5,NULL,cfg_temp,NULL);
            cfg_top->node_type = CASE_EXP;
            set_next_node($5,cfg_temp); 
            $$=cfg_top;
	    //sprintf ($$, "(casex %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEX '(' expression ')' case_item_eclr YYENDCASE");
          }
        | YYFOREVER statement
          {
              cfg_temp = build_blank_node();
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOREVER statement");
          }
        | YYREPEAT '(' expression ')' statement
          {
              cfg_temp = build_blank_node();
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYREPEAT '(' expression ')' statement");
          } 
        | YYWHILE '(' expression ')' statement
          {
              cfg_temp = build_blank_node();
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYWHILE '(' expression ')' statement");
          }
        | YYFOR '(' assignment ';' expression ';' assignment ')' statement
          {
              $$=NULL;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOR '(' assignment ';' expression ';' assignment ')' statement");
          }
        | delay_control statement
          {
              $$=$2;
 	      //strcpy ($$,"");
              YYTRACE("statement: delay_control statement");
          }
        | event_control {fprintf (fp_out,"\n\t %s", $1);} statement
          {
              $$=$3;
 	      //fprintf (fp_out,"\n\t %s", $1);
              YYTRACE("statement: event_control statement");
          }
        | lvalue type_action '=' delay_control expression ';'  {fprintf(fp_out,"\n\t %s = %s;",$1,$5);}
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = BLK_AS;
              sprintf(cfg_temp->exp_name,"%s = %s",$1,$5);
              $$=cfg_temp;
              YYTRACE("statement: lvalue '=' delay_control expression ';'");
          }
        | lvalue type_action '=' event_control expression ';'  {fprintf(fp_out,"\n\t %s = %s;",$1,$5);}
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = BLK_AS;
              sprintf(cfg_temp->exp_name,"%s = %s",$1,$5);
              $$=cfg_temp;
              YYTRACE("statement: lvalue '=' event_control expression ';'");
          }
        | lvalue type_action YYNBASSIGN delay_control expression ';' {fprintf(fp_out,"\n\t %s <= %s;",$1,$5);}
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = NBLK_AS;
              sprintf(cfg_temp->exp_name,"%s <= %s",$1,$5);
              $$=cfg_temp;
              //printf("deduce an statement : %s",$$->exp_name);
              YYTRACE("statement: lvalue YYNBASSIGN delay_control expression ';'");
          }
        | lvalue type_action YYNBASSIGN event_control expression ';' {fprintf(fp_out,"\n\t %s <= %s;",$1,$5);}
          {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = NBLK_AS;
              sprintf(cfg_temp->exp_name,"%s <= %s",$1,$5);
              $$=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: lvalue YYNBASSIGN event_control expression ';'");
          }
        | YYWAIT '(' expression ')' statement
          {
              $$=NULL;
              YYTRACE("statement: YYWAIT '(' expression ')' statement");
          }
        | YYRIGHTARROW name_of_event ';'
          {
              $$=NULL;
              YYTRACE("statement: YYRIGHTARROW name_of_event ';'");
          }
        | seq_block
          {
              $$=$1;
              //fprintf(fp_out, "\n\t begin");
              YYTRACE("statement: seq_block");
          }
        | par_block
          {
              $$=NULL;
              YYTRACE("statement: par_block");
          }
        | task_enable
          {
              $$=NULL;
              YYTRACE("statement: task_enable");
          }
        | system_task_enable
          {
              $$=NULL;
              YYTRACE("statement: system_task_enable");
          }
        | YYDISABLE YYLID 
          {
	  }
          ';'  
          {
              $$=NULL;
              YYTRACE("statement: YYDISABLE YYLID ';'");
          }
        | YYASSIGN assignment ';'
          {
            cfg_temp = build_normal_node($2,NULL,NULL,NULL,NULL);
            cfg_temp->node_type = BLK_AS;
            $$=cfg_temp;
	    YYTRACE("statement: YYASSIGN assignment ';'");
          }
        | YYDEASSIGN lvalue ';'
          {
              $$=NULL;
              YYTRACE("statement: YYDEASSIGN lvalue ';'");
          }
        ;

assignment
        : lvalue type_action '=' expression
          {
	    sprintf ($$,"(%s %s)",get_wwt_entry($1),$4);
            //add the parameter handle program
            if(parameter_start)
            {
              //bool parameter_start=false;
              //_para_table current_para_table[MAXPARAS]; 
              //int para_table_idx=0;
              strcpy(current_para_table[para_table_idx].para_name,$1);
              strcpy(current_para_table[para_table_idx].para_value,$4);
              printf("\n para %s = %s \n",current_para_table[para_table_idx].para_name,current_para_table[para_table_idx].para_value);
              para_table_idx++;
            }
            else  
            fprintf(fp_out,"\n\t  %s = %s;",$1,$4);
	    YYTRACE("assignment: lvalue '=' expression");
          }
        | lvalue type_action YYNBASSIGN expression
          {
            fprintf(fp_out,"\n\t  %s <= %s;",$1,$4);
	    sprintf ($$,"(nonblock %s %s)",get_wwt_entry($1),$4);
	    YYTRACE("assignment: lvalue YYNBASSIGN expression");
          }
        ;
if_branch
       :  YYIF '(' expression {fprintf(fp_out,"\n\t if(%s) \n\t  begin \n\t %s ", $3, get_next_inst_statement());} ')'  statement {fprintf(fp_out,"\n\t  end");}
          {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node($3,$6,NULL,cfg_temp,NULL);
            cfg_top->node_type = IFCOND_EXP;
            set_next_node($6,cfg_temp);
            $$=cfg_top;
          }

type_action
        :
          {
          } 
	;

case_item_eclr
        : case_item
          {
            $$=$1;
	    YYTRACE("case_item_eclr: case_item");
          }
        | case_item_eclr case_item
          {
            cfg_temp = $1;
            while(cfg_temp->brother_node!=NULL)
             cfg_temp = cfg_temp->brother_node; 
            cfg_temp->brother_node = $2; 
            $$=$1;
	    //sprintf ($$,"%s %s", $1,$2);
	    YYTRACE("case_item_eclr: case_item_eclr case_item");
          }
        ;

          //fprintf(fp_out, "\n\t begin  \n\t   %s   \n\t   end ",get_next_inst_statement());
case_item
        : expression_list ':' {fprintf(fp_out, "\n\t  %s: \n\t    begin \n\t   %s", $1,get_next_inst_statement());} statement {fprintf(fp_out,"\n\t  end");}
          {
            cfg_temp = build_normal_node($1,NULL,NULL,$4,NULL);
            cfg_temp->node_type = CASE_EXP;
            $$=cfg_temp;
            //while(cfg_temp!=NULL){
            // printf("^^^the case next st is %s \n",cfg_temp->exp_name);
            // cfg_temp=cfg_temp->next_node;
            //}
            //printf("=====================");
	    YYTRACE("case_item: expression_list ':' statement");
          }
        | YYDEFAULT ':' {fprintf(fp_out, "\n\t  default: \n\t    begin \n\t %s", get_next_inst_statement());} statement {fprintf(fp_out,"\n\t  end");}

          {
            cfg_temp = build_normal_node("default",NULL,NULL,$4,NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            $$=cfg_temp;
            YYTRACE("case_item: YYDEFAULT ':' statement");
          }
        | YYDEFAULT statement
          {
            cfg_temp = build_normal_node("default",NULL,NULL,$2,NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            $$=cfg_temp;
	    YYTRACE("case_item: YYDEFAULT statement");
          }
        ;

seq_block
        : YYBEGIN {fprintf(fp_out, "\n\t begin");} statement_clr YYEND
          {             
	    //sprintf ($$,"(%s)", $2);
            $$=$3;
            fprintf(fp_out, "\n\t end");
            //printf("=dedece to a seq block: %s\n",$$->exp_name);
            //printf("=next is: %s\n",$$->next_node->exp_name);
            //printf("=next is: %s\n",$$->next_node->next_node->exp_name);
	    YYTRACE("seq_block: YYBEGIN statement_clr YYEND");
          }
        | YYBEGIN ':' name_of_block block_declaration_clr statement_clr YYEND
          {
	    //sprintf ($$,"(|%s| (%s))", $3, $5);
            $$=NULL;
	    YYTRACE("seq_block: YYBEGIN ':' name_of_block block_declaration_clr statement_clr YYEND");
          }
        ;

par_block
        : YYFORK statement_clr YYJOIN
          {
              YYTRACE("par_block: YYFORK statement_clr YYJOIN");
          }
        | YYFORK ':' name_of_block block_declaration_clr statement_clr YYJOIN
          {
              YYTRACE("par_block: YYFORK ':' name_of_block block_declaration_clr statement_clr YYJOIN");
          }
        ;

name_of_block
        : YYLID
          {
	    sprintf ($$, "%s", $1);
	    YYTRACE("name_of_block: YYLID");
          }
        ;

block_declaration_clr
        :
          {
              YYTRACE("block_declaration_clr:");
          }
        | block_declaration_clr block_declaration
          {
              YYTRACE("block_declaration_clr: block_declaration_clr block_declaration");
          }
        ;

block_declaration
        : parameter_declaration
          {
              YYTRACE("block_declaration: parameter_declaration");
          }
        | reg_declaration
          {
              YYTRACE("block_declaration: reg_declaration");
          }
        | integer_declaration
          {
              YYTRACE("block_declaration: integer_declaration");
          }
        | real_declaration
          {
              YYTRACE("block_declaration: real_declaration");
          }
        | time_declaration
          {
              YYTRACE("block_delcaration: time_declaration");
          }
        | event_declaration
          {
              YYTRACE("block_declaration: event_declaration");
          }
        ;


task_enable
        : YYLID 
          {
	  }
          ';'
          {
              YYTRACE("task_enable: YYLID ';'");
          }
        | YYLID 
          {
	  }
         '(' expression_list ')' ';'
          {
              YYTRACE("task_enable: YYLID '(' expression_list ')' ';'");
          }
        ;

system_task_enable
        : name_of_system_task ';'
          {
              YYTRACE("system_task_enable: name_of_system_task ';'");
          }
        | name_of_system_task '(' expression_list ')' ';'
          {
              YYTRACE("system_task_enable: name_of_system_task '(' expression_list ')'");
          }
        ;

name_of_system_task
        : system_identifier
          {
              YYTRACE("name_of_system_task: system_identifier");
          }
        ;



specify_block
        : YYSPECIFY specify_item_clr YYENDSPECIFY
          {
              YYTRACE("specify_block: YYSPECIFY specify_item_clr YYENDSPECIFY");
          } 
        ;

specify_item_clr
        :
        | specify_item_clr specify_item
        ;

specify_item
        : specparam_declaration
        | path_declaration
        | level_sensitive_path_declaration
        | edge_sensitive_path_declaration
        | system_timing_check
        ;

specparam_declaration
        : YYSPECPARAM assignment_list ';'
        ;

path_declaration
        : path_description '=' path_delay_value ';'
        ;

 
                                         
path_description
        : '(' specify_terminal_descriptor YYLEADTO specify_terminal_descriptor ')'
        | '(' path_list YYALLPATH path_list_or_edge_sensitive_path_list ')'
        ;

path_list
        : specify_terminal_descriptor
        | path_list ',' specify_terminal_descriptor
        ;

specify_terminal_descriptor
        : YYLID {}
        | YYLID '[' expression ']' {}
        | YYLID '[' expression ';' expression ']' {}
        ;

path_list_or_edge_sensitive_path_list
        : path_list
        | '(' path_list ',' specify_terminal_descriptor
              polarity_operator YYCONDITIONAL 
              expression ')'
        ;

path_delay_value
        : path_delay_expression
        | '(' path_delay_expression ',' path_delay_expression ')'
        | '(' path_delay_expression ',' path_delay_expression ',' 
              path_delay_expression ')'
        | '(' path_delay_expression ',' path_delay_expression ','
              path_delay_expression ',' path_delay_expression ','
              path_delay_expression ',' path_delay_expression ')'
        ;

path_delay_expression
        : expression
	  {
	  }
        ;

system_timing_check
        : YYsysSETUP '(' ')' ';'
        ;

level_sensitive_path_declaration
        : YYIF '(' expression ')'
            '(' specify_terminal_descriptor polarity_operator_opt YYLEADTO
                spec_terminal_desptr_or_edge_sensitive_spec_terminal_desptr
        | YYIF '(' expression ')'
            '(' path_list ',' specify_terminal_descriptor 
                polarity_operator_opt YYALLPATH path_list ')'
                path_list '=' path_delay_value ';'
        ;

spec_terminal_desptr_or_edge_sensitive_spec_terminal_desptr
        : specify_terminal_descriptor ')' path_list '=' path_delay_value ';'
        | '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
              expression ')' ')' '=' path_delay_value ';'
        ;

polarity_operator_opt
        :
        | polarity_operator
        ;

polarity_operator
        : '+'
        | '-'
        ;

edge_sensitive_path_declaration
        : '(' specify_terminal_descriptor YYLEADTO
            '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
                expression ')' ')' '=' path_delay_value ';'
        | '(' edge_identifier specify_terminal_descriptor YYLEADTO
            '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
                expression ')' ')' '=' path_delay_value ';'
        | '(' edge_identifier specify_terminal_descriptor YYALLPATH
            '(' path_list ',' specify_terminal_descriptor
                polarity_operator YYCONDITIONAL 
                expression ')' ')' '=' path_delay_value ';'
        | YYIF '(' expression ')'
            '(' specify_terminal_descriptor YYALLPATH
              '(' path_list ',' specify_terminal_descriptor
                  polarity_operator YYCONDITIONAL 
                  expression ')' ')' '=' path_delay_value ';'
        | YYIF '(' expression ')'
            '(' edge_identifier specify_terminal_descriptor YYLEADTO
              '(' specify_terminal_descriptor polarity_operator YYCONDITIONAL
                  expression ')' ')' '=' path_delay_value ';'
        | YYIF '(' expression ')'
            '(' edge_identifier specify_terminal_descriptor YYALLPATH
              '(' path_list ',' specify_terminal_descriptor
                  polarity_operator YYCONDITIONAL 
                  expression ')' ')' '=' path_delay_value ';'
        ;

edge_identifier
        : YYPOSEDGE
        | YYNEGEDGE
        ;



lvalue
        : identifier
          {
	    sprintf ($$,$1);
	    YYTRACE("lvalue: YYLID");
	  }
        | identifier '[' expression ']'
          {
	    sprintf ($$,"%s[%s]",$1,$3);
	    YYTRACE("lvalue: YYLID '[' expression ']'");
	  }
        | identifier '[' expression ':' expression ']'
          {
	    sprintf ($$,"%s[%s:%s]",$1,$3,$5);
	    YYTRACE("lvalue: YYLID'[' expression ':' expression ']'");
	  }
        | concatenation
          {
	    sprintf ($$,$1);
	    YYTRACE("lvalue: concatenation");
	  }
        ;

mintypmax_expression_list
        : mintypmax_expression
          {
	    sprintf ($$,$1);
	    YYTRACE("mintypmax_expression_list: mintypmax_expression");
	  }
        | mintypmax_expression_list ',' mintypmax_expression
          {
	    sprintf ($$,"%s %s", $1, $3);
	    YYTRACE("mintypmax_expression_list: mintypmax_expression_list ',' mintypmax_expression");
	  }
        ;

mintypmax_expression
        : expression
          {
	    sprintf ($$,$1);
	    YYTRACE("mintypmax_expression: expression");
	  }
        | expression ':' expression
          {
	    sprintf ($$,"(: %s %s)", $1, $3);
	    YYTRACE("mintypmax_expression: expression ':' expression");
	  }
        | expression ':' expression ':' expression
          {
	    sprintf ($$,"(: %s %s %s)", $1, $3,$5);
	    YYTRACE("mintypmax_expression: expression ':' expression ':' expression");
	  }
        ;

expression_list
        : expression
          {
	    sprintf ($$,$1);
	    YYTRACE("expression_list: expression");
	  }
        | expression_list ',' expression
          {
	    sprintf ($$,"%s,%s", $1,$3);
	    YYTRACE("expression_list: expression_list ',' expression");
	  }
        ;

expression
        : primary
          {
	    sprintf ($$,$1);
	    YYTRACE("expression: primary");
	  }
        | '+' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(uplus %s)",$2);
	    YYTRACE("expression: '+' primary %prec YYUNARYOPERATOR");
	  } 
        | '-' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(-%s)",$2);
	    YYTRACE("expression: '-' primary %prec YYUNARYOPERATOR");
	  }
        | '!' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(! %s)",$2);
	    YYTRACE("expression: '!' primary %prec YYUNARYOPERATOR");
	  }
        | '~' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(~ %s)",$2);
	    YYTRACE("expression: '~' primary %prec YYUNARYOPERATOR");
	  }
        | '&' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(& %s)",$2);
	    YYTRACE("expression: '&' primary %prec YYUNARYOPERATOR");
	  }
        | '|' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(| %s)",$2);
	    YYTRACE("expression: '|' primary %prec YYUNARYOPERATOR");
	  }
        | '^' primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(^ %s,",$2);
	    YYTRACE("expression: '^' primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGNAND primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(~& %s)",$2);
	    YYTRACE("expression: YYLOGNAND primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGNOR primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(~| %s)",$2);
	    YYTRACE("expression: YYLOGNOR primary %prec YYUNARYOPERATOR");
	  }
        | YYLOGXNOR primary %prec YYUNARYOPERATOR
          {
	    sprintf ($$,"(~^ %s)",$2);
	    YYTRACE("expression: YYLOGXNOR primary %prec YYUNARYOPERATOR");
	  }
        | expression '+' expression
          {
	    sprintf ($$,"( %s + %s)",$1,$3);
	    YYTRACE("expression: expression '+' expression");
	  }
        | expression '-' expression
          {
	    sprintf ($$,"( %s - %s)",$1,$3);
	    YYTRACE("expression: expressio '-' expression");
	  }
        | expression '*' expression
          {
	    sprintf ($$,"( %s * %s)",$1,$3);
	    YYTRACE("expression: expression '*' expression");
	  }
        | expression '/' expression
          {
	    sprintf ($$,"( %s / %s)",$1,$3);
	    YYTRACE("expression: expression '/' expression");
	  }
        | expression '%' expression
          {
	    sprintf ($$,"( %s %% %s)",$1,$3);
	    YYTRACE("expression: expression '%' expression");
	  }
        | expression YYLOGEQUALITY expression
          {
	    sprintf ($$,"( %s == %s)",$1,$3);
	    YYTRACE("expression: expression YYLOgEQUALITY expression");
	  }
        | expression YYLOGINEQUALITY expression
          {
	    sprintf ($$,"( %s != %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGINEQUALITY expression");
	  }
        | expression YYCASEEQUALITY expression
          {
	    sprintf ($$,"(%s === %s)",$1,$3);
	    YYTRACE("expression: expression YYCASEEQUALITY expression");
	  }
        | expression YYCASEINEQUALITY expression
          {
	    sprintf ($$,"(%s !== %s)",$1,$3);
	    YYTRACE("expression: expression YYCASEINEQUALITY expression");
	  }
        | expression YYLOGAND expression
          {
	    sprintf ($$,"(%s && %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGAND expression");
	  }
        | expression YYLOGOR expression
          {
	    sprintf ($$,"( %s || %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGOR expression");
	  }
        | expression '<' expression
          {
	    sprintf ($$,"( %s < %s)",$1,$3);
	    YYTRACE("expression: expression '<' expression");
	  }
        | expression '>' expression
          {
	    sprintf ($$,"( %s > %s)",$1,$3);
	    YYTRACE("expression: expression '>' expression");
	  }
        | expression '&' expression
          {
	    sprintf ($$,"( %s & %s)",$1,$3);
	    YYTRACE("expression: expression '&' expression");
	  }
        | expression '|' expression
          {
	    sprintf ($$,"( %s | %s)",$1,$3);
	    YYTRACE("expression: expression '|' expression");
	  }
        | expression '^' expression
          {
	    sprintf ($$,"(%s ^ %s)",$1,$3);
	    YYTRACE("expression: expression '^' expression");
	  }
        | expression YYLEQ expression
          {
	    sprintf ($$,"(%s =< %s)",$1,$3);
	    YYTRACE("expression: expression YYLEQ expression");
	  }
        | expression YYNBASSIGN expression
          {
	    sprintf ($$,"(%s <= %s)",$1,$3);
	    YYTRACE("expression: expression YYLEQ expression");
	  }
        | expression YYGEQ expression
          {
	    sprintf ($$,"(%s >= %s)",$1,$3);
	    YYTRACE("expression: expression YYGEQ expression");
	  }
        | expression YYLSHIFT expression
          {
	    sprintf ($$,"(%s << %s)",$1,$3);
	    YYTRACE("expression: expression YYLSHIFT expression");
	  }
        | expression YYRSHIFT expression
          {
	    sprintf ($$,"(%s >> %s)",$1,$3);
	    YYTRACE("expression: expression YYRSHIFT expression");
	  }
        | expression YYLOGXNOR expression
          {
	    sprintf ($$,"(%s ^~ %s)",$1,$3);
	    YYTRACE("expression: expression YYLOGXNOR expression");
	  }
        | expression '?' expression ':' expression
          {
	    sprintf ($$,"( %s ? %s : %s)",$1,$3,$5);
	    YYTRACE("expression: expression '?' expression ':' expression");
	  }
        | YYSTRING
          {
	    sprintf ($$,$1);
	    YYTRACE("expression: YYSTRING");
	  }
        ;

primary
        : YYINUMBER
          {
	    sprintf ($$,$1);
	    YYTRACE("primary: YYINUMBER");
	  }
        | YYINUMBER_BIT
          {
	    sprintf ($$,"%s",$1);
	      YYTRACE("primary: YYINUMBER_BIT");
	  }
        | YYRNUMBER
          {
	    sprintf ($$,$1);
	      YYTRACE("primary: YYRNUMBER");
	  }
        | identifier
          {
	    sprintf ($$,get_wwt_entry($1));
            //check whether the identifier is the parameter
            int idx_temp;
            if(parameter_start==false)
            {
             // int para_table_idx=0;
             //_para_table current_para_table[MAXPARAS]; 
              for(idx_temp=0;idx_temp<para_table_idx;idx_temp++)
              {
                if(strcmp($1,current_para_table[idx_temp].para_name)==0)
                {
                  sprintf($$,"%s", current_para_table[idx_temp].para_value);
                  printf("\n substitute the parameter %s with value %s\n",current_para_table[idx_temp].para_name,$$);  
                 }
              }   
            }
	    YYTRACE("primary: identifier");
	  }
        | identifier '[' expression ']'
          {
	    sprintf ($$," %s[%s]",$1,$3);
	    YYTRACE("primary: identifier '[' expression ']'");
	  }
        | identifier '[' expression ':'  expression ']'
          {
	    sprintf ($$,"%s[%s:%s]",$1,$3,$5);
	    YYTRACE("primary: identifier '[' expression ':' expression ']'");
	  }
        | concatenation
          {
	    strcpy ($$,$1);
	    YYTRACE("primary: concatenation");
	  }
        | multiple_concatenation
	  {
	    strcpy ($$,$1);
	    YYTRACE("primary: multiple_concatenatin");
	  }
        | function_call
          {
	    strcpy ($$,"");
	    YYTRACE("primary: function_call");
	  }
        | '(' mintypmax_expression ')'
          {
	    strcpy ($$,$2);
	    YYTRACE("primary: '(' mintypmax_expression ')'");
	  }
          
	| YYsysID '(' nondeterminism_list ')'
          {
	    strcpy ($$,"");
	    YYTRACE("primary: YYsysID '(' nondeterminism_list ')'");
	  }
        | YYsysND '(' nondeterminism_list ')'
          {
	    strcpy ($$,"");
	  }
        ;

nondeterminism_list 
        : event_control
          {
	      YYTRACE("nondeterminism_list : event_control");
	  }
        | expression
          {
	      YYTRACE("nondeterminism_list : expression");
	  }
        | nondeterminism_list ',' event_control
          {
	      YYTRACE("nondeterminism_list : nondeterminism_list ',' event_control");
	  }
        | nondeterminism_list ',' expression
          {
	      YYTRACE("nondeterminism_list : nondeterminism_list ',' expression");
	  }
        ;



concatenation
        : '{' expression_list '}'
          {
	    sprintf ($$,"{%s}",$2);
	    YYTRACE("concatenation: '{' expression_list '}'");
	  }
        ;

multiple_concatenation
        : '{' expression '{' expression_list '}' '}'
	    {
	      sprintf ($$,"(concatenation %s (concatenation %s))",$2,$4);
	      YYTRACE("multiple_concatenation: '{' expression '{' expression_list '}' '}'");
	    }
        ;

function_call
        : identifier '(' expression_list ')'
          {
	      YYTRACE("function_call: identifier '(' expression_list ')'");
	  }

        ;

system_identifier
        : YYsysID
        ;



identifier
        : YYLID
          {
	    sprintf ($$,"%s",$1);
	    YYTRACE("identifier: YYLID");
          }
        | identifier '.' YYLID
          {
	    sprintf ($$,"(element %s %s)",$1,$3);
	    YYTRACE("identifier: identifier '.' YYLID");
          }
        ;

delay_opt
        :
          {
	      YYTRACE("delay_opt:");
	  }
        | delay
          {
	      YYTRACE("delay_opt: delay");
	  }
        ;

delay
        : '#' YYINUMBER
          {
	      YYTRACE("delay: '#' YYINUMBER");
	  }
        | '#' YYINUMBER_BIT
          {
	      YYTRACE("delay: '#' YYINUMBER_BIT");
	  }  
        | '#' YYRNUMBER
          {
	      YYTRACE("delay: '#' YYRNUMBER");
	  }  
        | '#' identifier
          {
	      YYTRACE("delay: '#' identifier");
	  }
        | '#' '(' mintypmax_expression ')'
          {
	      YYTRACE("delay: '#' '(' mintypmax_expression ')'");
	  }
        | '#' '(' mintypmax_expression ',' mintypmax_expression ')'
          {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ')'");
	  }
        | '#' '(' mintypmax_expression ',' mintypmax_expression ',' 
                  mintypmax_expression ')'
          {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ',' mintypmax_expression ')'");
	  }
        ;

delay_control
        : '#' YYINUMBER
          {
	      YYTRACE("delay_control: '#' YYINUMBER");
	  }
        | '#' YYINUMBER_BIT
          {
	      YYTRACE("delay_control: '#' YYINUMBER_BIT");
	  }
        | '#' YYRNUMBER
          {
	      YYTRACE("delay_control: '#' YYRNUMBER");
	  }  
        | '#' identifier
          {
	      YYTRACE("delay_control: '#' identifier");
	  }
        | '#' '(' mintypmax_expression_list ')'
          {
	      YYTRACE("delay_control: '#' '(' mintypmax_expression ')'");
	  }
        ;

event_control
        : '@' identifier
          {
	    sprintf ($$,"always @ (%s)", $2);
	    YYTRACE("event_control: '@' identifier");
	  }
        | '@' '(' '*' ')'
          {
	    sprintf ($$,"always @ (*)");
	    YYTRACE("event_control: '@' (*)");
          }
        | '@' '(' event_expression ')'
          {
	    sprintf ($$,"always @ (%s) ", $3);
	    YYTRACE("event_control: '@' '(' event_expression ')'");
	  }
        | '@' '(' ored_event_expression ')'
          {
	    sprintf ($$,"always @ (%s)", $3);
	    YYTRACE("event_control: '@' '(' ored_event_expression ')'");
	  }
        ;


event_expression
        : expression
          {
	    sprintf ($$, "(%s)", $1);
	    YYTRACE("event_expression: expression");
	  }
        | YYPOSEDGE expression
          {
	    sprintf ($$, "posedge %s", $2);
	    YYTRACE("event_expression: YYPOSEDGE expression");
	  }
        | YYNEGEDGE expression
          {
	    sprintf ($$, "negedge %s", $2);
	    YYTRACE("event_expression: YYNEGEDGE expression");
	  }
	| YYEDGE expression
          {
	    sprintf ($$, "(edge %s)", $2);
	    YYTRACE("event_expression: YYEDGE expression");
          }
        ;

ored_event_expression
        : event_expression YYOR event_expression
          {
	    sprintf ($$,"%s or %s", $1, $3);
	    YYTRACE("ored_event_expression: event_expression YYOR event_expression");
            //printf("\nored event expression1\n");
	  }
        | ored_event_expression YYOR event_expression
          {
	    sprintf ($$,"%s or %s", $1, $3);
	    YYTRACE("ored_event_expression: ored_event_expression YYOR event_expression");
            //printf("\nored event expression2\n");
	  }
	  
        ; 

%%

void yyerror(char * str)
{
    fprintf(stderr, str);
    exit (1);
}

void YYTRACE(char * str)
{
}

_modulestats * make_module (char *name) {
  _modulestats *newmodule;

  if (modstats == NULL) {
    modstats = (_modulestats *) malloc (sizeof (struct __modulestats));
    modstats->next = NULL;
    modstats->numports = 0;
    modstats->numinputs = 0;
    modstats->numoutputs = 0;
    modstats->numregs = 0;
    modstats->numwires = 0;
    return modstats;
  }
  newmodule = modstats;
  while (newmodule->next != NULL) newmodule = newmodule->next;
  newmodule->next = (_modulestats *) malloc (sizeof (struct __modulestats));
  newmodule->next->next = NULL;
  newmodule->next->numports = 0;
  newmodule->next->numinputs = 0;
  newmodule->next->numoutputs = 0;
  newmodule->next->numregs = 0;
  newmodule->next->numwires = 0;
  return newmodule->next;
}

char *get_string_from_var (_var *v) {
  char *outstr;
  int i;
  outstr = (char *) malloc (MAXSTRLEN*(v->end-v->start+1));
  if (v->end-v->start) {
    strcpy (outstr,"");
    for (i=v->start; i<=v->end; i++)
      sprintf (outstr, "%s |%s%d|", outstr, v->name, i);
  } 
  else sprintf (outstr,v->name);
  return outstr;
}

char *get_string_from_varlist (_var **v, int n) {
  char *retstr;
  int i;
  retstr = NULL;
  if (n<=0) return "";
  sprintf (retstr,get_string_from_var(v[0]));
  for (i=1; i<n; i++)
    sprintf (retstr, "%s%s", retstr, get_string_from_var(v[i]));
  return retstr;
}

void print_modstats (void) {
  fprintf (stdout, "\t   (ins  %s)\n", get_string_from_varlist (thismodule->input_list, thismodule->numinputs));
  fprintf (stdout, "\t   (sts  %s)\n", get_string_from_varlist (thismodule->reg_list, thismodule->numregs));
  fprintf (stdout, "\t   (outs  %s)\n", get_string_from_varlist (thismodule->output_list, thismodule->numoutputs));
  fprintf (stdout, "\t   (wires  %s)\n", get_string_from_varlist (thismodule->wire_list, thismodule->numwires));
  fprintf (stdout, "\t   (occs  %s)\n","");
}

void put_wwt_entry (char *name, char *range) {
  struct wire_width_tuple t;
  strcpy (t.name, name);
  strcpy (t.range, range);
  wire_width_table[num_width_table_entries++] = t;
  return;
}

char *get_wwt_entry (char *name) {
  int i;
  char *rstr = (char *) malloc (MAXSTRLEN);
  for (i=0; i<num_width_table_entries; i++) 
    if (strcmp (wire_width_table[i].name, name)==0) {
      //sprintf (rstr, "(%s %s)", wire_width_table[i].range, wire_width_table[i].name);
      sprintf (rstr, " %s", wire_width_table[i].name);
      return rstr;
    }
  return name;
}

void print_wwt (void) {
  printf ("\n");printf ("\n");
  for (int i=0; i<num_width_table_entries; i++) 
    printf ("(%s %s)\n", wire_width_table[i].name, wire_width_table[i].range);
  printf ("\n");
}

//============================================
//============for building CFG================
//===========liu187@illinois.edu==============
_cfg_node * build_blank_node( )
{ 
  _cfg_node * temp;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, "blank");
  temp->node_type = NULL_NODE;
  temp->left_node = NULL;
  temp->right_node = NULL;
  temp->brother_node = NULL;
  temp->next_node = NULL;
  return temp;
}

_cfg_node * build_normal_node(char exp[], _cfg_node * left, _cfg_node * right, _cfg_node * next, _cfg_node * brother)
{
  _cfg_node * temp;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, exp);
  temp->left_node = left;
  temp->right_node = right;
  temp->brother_node = brother;
  temp->next_node = next; 
  return temp;
}

int set_next_node(_cfg_node * cfg_x, _cfg_node * cfg_nxt)
{
  _cfg_node * temp;
  temp = cfg_x;
  while(temp->next_node!=NULL)
   temp = temp->next_node;
  temp->next_node = cfg_nxt;
  return 1;
}

  //int bran_var_idx=0;
char * get_next_inst_statement()
{
   int idx;
   char * p_next_inst_statement = (char *)malloc(50*sizeof(char));
   for (idx=0;idx<50;idx++)
     *(p_next_inst_statement+idx)='\0';
   sprintf(p_next_inst_statement,"branvar[%d] <= branvar[%d] + 1;",bran_var_idx, bran_var_idx);
   bran_var_idx++;
   return p_next_inst_statement;
}

//finish a cfg building
int push_cfg_table(_cfg_node * cfg_x)
{
  cfg_table[cfg_stack_pointer]=cfg_x;
  cfg_stack_pointer++;
  return 1;
}

void print_all()
{
  int i;
  for(i=0;i<cfg_stack_pointer;i++)
   print_cfg(cfg_table[i]);
}


void print_cfg(_cfg_node * cfg_x)
{
  _cfg_node * cfg_bfs[MAXNUMCFGS];
  int rd_pointer=0;
  int wr_pointer=0;
  cfg_bfs[wr_pointer++]=cfg_x;
  //make a breadth first search
  while(rd_pointer!=wr_pointer)
  {
    cfg_x = cfg_bfs[rd_pointer];
    rd_pointer=rd_pointer+1;
    rd_pointer=rd_pointer%100;
    
    printf("Node: %s\n",cfg_x->exp_name);
    if(cfg_x->left_node!=NULL){
      cfg_bfs[wr_pointer]=cfg_x->left_node;
      wr_pointer++;
      wr_pointer=wr_pointer%100;
      continue;
    }
    if(cfg_x->brother_node!=NULL){
      cfg_bfs[wr_pointer]=cfg_x->brother_node;
      wr_pointer++;
      wr_pointer=wr_pointer%100;
    }
    if(cfg_x->next_node!=NULL){
      cfg_bfs[wr_pointer]=cfg_x->next_node;
      wr_pointer++;
      wr_pointer=wr_pointer%100;
     }
  } 
}
