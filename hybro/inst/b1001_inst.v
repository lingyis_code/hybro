
module b1001 (clock , reset , opcode , dina , dinb , add1_tmp , dout );

	  input  clock;
	  input  reset;
	  input [3 : 0] opcode;
	  input [31 : 0] dina;
	  input [31 : 0] dinb;
	  input  add1_tmp;
	  output [31 : 0] dout;
	  reg [31 : 0] dout;
	  reg [7:0] branvar[0:1000];
	  reg [31 : 0] dout_p1;
	  reg [31 : 0] dout_p2;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  dout_p1 <= 32'h0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 begin
	 case( opcode)
	  4'h0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  dout_p1 <=  dina;
	 end
	  end
	  4'h1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	  dout_p1 <= ( (  dina +  dinb) +  add1_tmp);
	 end
	  end
	  4'h2: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	 begin
	  dout_p1 <= ( (  dina -  dinb) -  add1_tmp);
	 end
	  end
	  4'h3: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 begin
	  dout_p1 <= (  dina +  dinb);
	 end
	  end
	  4'h4: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 begin
	  dout_p1 <= (  dina -  dinb);
	 end
	  end
	  4'h5: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	 begin
	  dout_p1 <= (  dina *  dinb);
	 end
	  end
	  4'h6: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	 begin
	  dout_p1 <= (  dina /  dinb);
	 end
	  end
	  4'h7: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 begin
	  dout_p1 <= (~ (  dina &  dinb));
	 end
	  end
	  4'h8: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	 begin
	  dout_p1 <= (  dina &  dinb);
	 end
	  end
	  4'h9: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	 begin
	  dout_p1 <= (  dina |  dinb);
	 end
	  end
	  4'ha: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	 begin
	  dout_p1 <= ( dina ^  dinb);
	 end
	  end
	  4'hb: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	 begin
	  dout_p1 <= (~  dina);
	 end
	  end
	  4'hc: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	 begin
	  dout_p1 <= ( dina <<  dinb);
	 end
	  end
	  4'hd: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	 begin
	  dout_p1 <= ( dina >>  dinb);
	 end
	  end
	  4'he: 
	    begin 
	   branvar[16] <= branvar[16] + 1;
	 begin
	  dout_p1 <= (  dina %  dinb);
	 end
	  end
	  4'hf: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	 begin
	  dout_p1 <=  dinb;
	 end
	  end
	 endcase
	 end
	  end
	 end
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  dout_p2 <= 32'h0;
	  dout <= 32'h0;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 begin
	  dout_p2 <=  dout_p1;
	  dout <=  dout_p2;
	 end
	  end
	 end
	  endmodule 
