
module b11 (clock , reset , stbi , x_in , x_out );

	  input  clock;
	  input  reset;
	  input  stbi;
	  input [5 : 0] x_in;
	  output [5 : 0] x_out;
	  wire clock;
	  wire reset;
	  wire stbi;
	  wire [5 : 0] x_in;
	  reg [5 : 0] x_out;
	  reg [7:0] branvar[0:100];
	  reg [3 : 0] stato;
	  reg [5 : 0] r_in;
	  reg [5 : 0] cont;
	  reg [8 : 0] cont1;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  stato = 0;
	  r_in = 0;
	  cont = 0;
	  cont1 = 0;
	  x_out <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( stato)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  cont = 0;
	  r_in =  x_in;
	  x_out <= 0;
	  stato = 1;
	 end
	  end
	  1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	  r_in =  x_in;
	 if( stbi) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	  stato = 1;
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	  stato = 2;
	  end
	 end
	  end
	  2: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 if(( (  r_in == 0) || (  r_in == 63))) 
	  begin 
	 branvar[7] <= branvar[7] + 1; 
	 begin
	 if((  cont < 25)) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	  cont = (  cont + 1);
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	  cont = 0;
	  end
	  cont1 =  r_in;
	  stato = 8;
	 end
	  end
	 else 
	   begin 
	   branvar[10] <= branvar[10] + 1;   
	 if(( r_in <= 26)) 
	  begin 
	 branvar[11] <= branvar[11] + 1; 
	  stato = 3;
	  end
	 else 
	   begin 
	   branvar[12] <= branvar[12] + 1;   
	  stato = 1;
	  end
	  end
	  end
	  3: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	 begin
	 if((  r_in & 1)) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	  cont1 = ( cont << 1);
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	  cont1 =  cont;
	  end
	  stato = 4;
	 end
	  end
	  4: 
	    begin 
	   branvar[16] <= branvar[16] + 1;
	 if(((  r_in & 3) >> 1)) 
	  begin 
	 branvar[17] <= branvar[17] + 1; 
	 begin
	  cont1 = (  r_in +  cont1);
	  stato = 5;
	 end
	  end
	 else 
	   begin 
	   branvar[18] <= branvar[18] + 1;   
	 begin
	  cont1 = (  r_in -  cont1);
	  stato = 6;
	 end
	  end
	  end
	  5: 
	    begin 
	   branvar[19] <= branvar[19] + 1;
	 if(((!  cont1[8]) && (  cont1 > 26))) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  cont1 = (  cont1 - 26);
	  stato = 5;
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	  stato = 7;
	  end
	  end
	  6: 
	    begin 
	   branvar[22] <= branvar[22] + 1;
	 begin
	 if(((!  cont1[8]) && (  cont1 > 63))) 
	  begin 
	 branvar[23] <= branvar[23] + 1; 
	 begin
	  cont1 = (  cont1 + 26);
	  stato = 6;
	 end
	  end
	 else 
	   begin 
	   branvar[24] <= branvar[24] + 1;   
	  stato = 7;
	  end
	 end
	  end
	  7: 
	    begin 
	   branvar[25] <= branvar[25] + 1;
	 begin
	 if(( ( ( r_in >> 2) & 3) == 0)) 
	  begin 
	 branvar[26] <= branvar[26] + 1; 
	  cont1 = (  cont1 - 21);
	  end
	 else 
	   begin 
	   branvar[27] <= branvar[27] + 1;   
	 if(( ( ( r_in >> 2) & 3) == 1)) 
	  begin 
	 branvar[28] <= branvar[28] + 1; 
	  cont1 = (  cont1 - 42);
	  end
	 else 
	   begin 
	   branvar[29] <= branvar[29] + 1;   
	 if(( ( ( r_in >> 2) & 3) == 2)) 
	  begin 
	 branvar[30] <= branvar[30] + 1; 
	  cont1 = (  cont1 + 7);
	  end
	 else 
	   begin 
	   branvar[31] <= branvar[31] + 1;   
	  cont1 = (  cont1 + 28);
	  end
	  end
	  end
	  stato = 8;
	 end
	  end
	  8: 
	    begin 
	   branvar[32] <= branvar[32] + 1;
	 begin
	 if( cont1[8]) 
	  begin 
	 branvar[33] <= branvar[33] + 1; 
	  x_out <= ( (- cont1) & 63);
	  end
	 else 
	   begin 
	   branvar[34] <= branvar[34] + 1;   
	  x_out <= (  cont1 & 63);
	  end
	  stato = 1;
	 end
	  end
	 endcase
	  end
	 end
	  endmodule 
