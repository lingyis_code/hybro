
module decode_stage (clk , reset , stall_in_0 , stall_in_1 , stall_in_2 , branch_mispredict , fetch2decodeinstr0_out , fetch2decodeinstr1_out , fetch2decodepc1 , fetch2decodepc0 , fetch2decodepc1_incr , fetch2decodepc0_incr , fetch2decodeinstr0_valid , fetch2decodeinstr1_valid , fetch2decodebranch_taken , fetch2decodepredicted_pc , fetch2decodevalid , data_in_bpaddress_0 , data_in_bpdata_0 , data_in_bpvalid_0 , data_in_bpaddress_1 , data_in_bpdata_1 , data_in_bpvalid_1 , data_in_bpaddress_2 , data_in_bpdata_2 , data_in_bpvalid_2 , data_in_bpaddress_3 , data_in_bpdata_3 , data_in_bpvalid_3 , data_in_bpaddress_4 , data_in_bpdata_4 , data_in_bpvalid_4 , data_in_bpaddress_5 , data_in_bpdata_5 , data_in_bpvalid_5 , data_in_bpaddress_6 , data_in_bpdata_6 , data_in_bpvalid_6 , data_in_bpaddress_7 , data_in_bpdata_7 , data_in_bpvalid_7 , data_in_bpaddress_8 , data_in_bpdata_8 , data_in_bpvalid_8 , data_in_bpaddress_9 , data_in_bpdata_9 , data_in_bpvalid_9 , data_in_bpaddress_10 , data_in_bpdata_10 , data_in_bpvalid_10 , write_enable_0 , write_addr_0 , write_data_0 , write_enable_1 , write_addr_1 , write_data_1 , decode2mempc , decode2mempc_incr , decode2memthread_num , decode2membranch_taken , decode2mempredicted_pc , decode2memvalid , decode2memis_oldest , decode2memsreg_t_data , decode2memsreg_s_data , decode2memresult , decode2memL1D_hit , decode2memdecode_packetsreg_t , decode2memdecode_packetsreg_s , decode2memdecode_packetdreg , decode2memdecode_packetacc , decode2memdecode_packetimm16_value , decode2memdecode_packetimm26_value , decode2memdecode_packetimm5_value , decode2memdecode_packetop_FU_type , decode2memdecode_packetalu_op , decode2memdecode_packetfpu_op , decode2memdecode_packetop_MEM_type , decode2memdecode_packetop_BR_type , decode2memdecode_packetshift_type , decode2memdecode_packetcompare_type , decode2memdecode_packetis_imm26 , decode2memdecode_packetis_imm , decode2memdecode_packetis_imm5 , decode2memdecode_packetis_signed , decode2memdecode_packetcarry_in , decode2memdecode_packetcarry_out , decode2memdecode_packethas_sreg_t , decode2memdecode_packethas_sreg_s , decode2memdecode_packethas_dreg , decode2memdecode_packetis_prefetch , decode2memdecode_packetsprf_dest , decode2memdecode_packetsprf_src , decode2memdecode_packetis_atomic , decode2memdecode_packetis_ldl , decode2memdecode_packetis_stl , decode2memdecode_packetis_stc , decode2memdecode_packetis_valid , decode2memdecode_packetis_halt , decode2memdecode_packetis_compare , decode2memdecode_packetdreg_is_src , decode2memdecode_packetdreg_is_dest , decode2fpupc , decode2fpupc_incr , decode2fputhread_num , decode2fpubranch_taken , decode2fpupredicted_pc , decode2fpuvalid , decode2fpuis_oldest , decode2fpusreg_t_data , decode2fpusreg_s_data , decode2fpuresult , decode2fpuL1D_hit , decode2fpudecode_packetsreg_t , decode2fpudecode_packetsreg_s , decode2fpudecode_packetdreg , decode2fpudecode_packetacc , decode2fpudecode_packetimm16_value , decode2fpudecode_packetimm26_value , decode2fpudecode_packetimm5_value , decode2fpudecode_packetop_FU_type , decode2fpudecode_packetalu_op , decode2fpudecode_packetfpu_op , decode2fpudecode_packetop_MEM_type , decode2fpudecode_packetop_BR_type , decode2fpudecode_packetshift_type , decode2fpudecode_packetcompare_type , decode2fpudecode_packetis_imm26 , decode2fpudecode_packetis_imm , decode2fpudecode_packetis_imm5 , decode2fpudecode_packetis_signed , decode2fpudecode_packetcarry_in , decode2fpudecode_packetcarry_out , decode2fpudecode_packethas_sreg_t , decode2fpudecode_packethas_sreg_s , decode2fpudecode_packethas_dreg , decode2fpudecode_packetis_prefetch , decode2fpudecode_packetsprf_dest , decode2fpudecode_packetsprf_src , decode2fpudecode_packetis_atomic , decode2fpudecode_packetis_ldl , decode2fpudecode_packetis_stl , decode2fpudecode_packetis_stc , decode2fpudecode_packetis_valid , decode2fpudecode_packetis_halt , decode2fpudecode_packetis_compare , decode2fpudecode_packetdreg_is_src , decode2fpudecode_packetdreg_is_dest , decode2intpc , decode2intpc_incr , decode2intthread_num , decode2intbranch_taken , decode2intpredicted_pc , decode2intvalid , decode2intis_oldest , decode2intsreg_t_data , decode2intsreg_s_data , decode2intresult , decode2intL1D_hit , decode2intdecode_packetsreg_t , decode2intdecode_packetsreg_s , decode2intdecode_packetdreg , decode2intdecode_packetacc , decode2intdecode_packetimm16_value , decode2intdecode_packetimm26_value , decode2intdecode_packetimm5_value , decode2intdecode_packetop_FU_type , decode2intdecode_packetalu_op , decode2intdecode_packetfpu_op , decode2intdecode_packetop_MEM_type , decode2intdecode_packetop_BR_type , decode2intdecode_packetshift_type , decode2intdecode_packetcompare_type , decode2intdecode_packetis_imm26 , decode2intdecode_packetis_imm , decode2intdecode_packetis_imm5 , decode2intdecode_packetis_signed , decode2intdecode_packetcarry_in , decode2intdecode_packetcarry_out , decode2intdecode_packethas_sreg_t , decode2intdecode_packethas_sreg_s , decode2intdecode_packethas_dreg , decode2intdecode_packetis_prefetch , decode2intdecode_packetsprf_dest , decode2intdecode_packetsprf_src , decode2intdecode_packetis_atomic , decode2intdecode_packetis_ldl , decode2intdecode_packetis_stl , decode2intdecode_packetis_stc , decode2intdecode_packetis_valid , decode2intdecode_packetis_halt , decode2intdecode_packetis_compare , decode2intdecode_packetdreg_is_src , decode2intdecode_packetdreg_is_dest , decode_stall );

	  input  clk;
	  input  reset;
	  input  stall_in_0;
	  input  stall_in_1;
	  input  stall_in_2;
	  input  branch_mispredict;
	  input [31 : 0] fetch2decodeinstr0_out;
	  input [31 : 0] fetch2decodeinstr1_out;
	  input [31 : 0] fetch2decodepc1;
	  input [31 : 0] fetch2decodepc0;
	  input [31 : 0] fetch2decodepc1_incr;
	  input [31 : 0] fetch2decodepc0_incr;
	  input  fetch2decodeinstr0_valid;
	  input  fetch2decodeinstr1_valid;
	  input  fetch2decodebranch_taken;
	  input [31 : 0] fetch2decodepredicted_pc;
	  input  fetch2decodevalid;
	  input [4 : 0] data_in_bpaddress_0;
	  input [31 : 0] data_in_bpdata_0;
	  input  data_in_bpvalid_0;
	  input [4 : 0] data_in_bpaddress_1;
	  input [31 : 0] data_in_bpdata_1;
	  input  data_in_bpvalid_1;
	  input [4 : 0] data_in_bpaddress_2;
	  input [31 : 0] data_in_bpdata_2;
	  input  data_in_bpvalid_2;
	  input [4 : 0] data_in_bpaddress_3;
	  input [31 : 0] data_in_bpdata_3;
	  input  data_in_bpvalid_3;
	  input [4 : 0] data_in_bpaddress_4;
	  input [31 : 0] data_in_bpdata_4;
	  input  data_in_bpvalid_4;
	  input [4 : 0] data_in_bpaddress_5;
	  input [31 : 0] data_in_bpdata_5;
	  input  data_in_bpvalid_5;
	  input [4 : 0] data_in_bpaddress_6;
	  input [31 : 0] data_in_bpdata_6;
	  input  data_in_bpvalid_6;
	  input [4 : 0] data_in_bpaddress_7;
	  input [31 : 0] data_in_bpdata_7;
	  input  data_in_bpvalid_7;
	  input [4 : 0] data_in_bpaddress_8;
	  input [31 : 0] data_in_bpdata_8;
	  input  data_in_bpvalid_8;
	  input [4 : 0] data_in_bpaddress_9;
	  input [31 : 0] data_in_bpdata_9;
	  input  data_in_bpvalid_9;
	  input [4 : 0] data_in_bpaddress_10;
	  input [31 : 0] data_in_bpdata_10;
	  input  data_in_bpvalid_10;
	  input  write_enable_0;
	  input [4 : 0] write_addr_0;
	  input [31 : 0] write_data_0;
	  input  write_enable_1;
	  input [4 : 0] write_addr_1;
	  input [31 : 0] write_data_1;
	  output [31 : 0] decode2mempc;
	  output [31 : 0] decode2mempc_incr;
	  output [1 : 0] decode2memthread_num;
	  output decode2membranch_taken ;
	  output [31 : 0] decode2mempredicted_pc;
	  output decode2memvalid ;
	  output decode2memis_oldest ;
	  output [31 : 0] decode2memsreg_t_data;
	  output [31 : 0] decode2memsreg_s_data;
	  output [31 : 0] decode2memresult;
	  output decode2memL1D_hit ;
	  output [4 : 0] decode2memdecode_packetsreg_t;
	  output [4 : 0] decode2memdecode_packetsreg_s;
	  output [4 : 0] decode2memdecode_packetdreg;
	  output [1 : 0] decode2memdecode_packetacc;
	  output [15 : 0] decode2memdecode_packetimm16_value;
	  output [25 : 0] decode2memdecode_packetimm26_value;
	  output [4 : 0] decode2memdecode_packetimm5_value;
	  output [3 : 0] decode2memdecode_packetop_FU_type;
	  output [4 : 0] decode2memdecode_packetalu_op;
	  output [4 : 0] decode2memdecode_packetfpu_op;
	  output [3 : 0] decode2memdecode_packetop_MEM_type;
	  output [1 : 0] decode2memdecode_packetop_BR_type;
	  output [1 : 0] decode2memdecode_packetshift_type;
	  output [2 : 0] decode2memdecode_packetcompare_type;
	  output decode2memdecode_packetis_imm26 ;
	  output decode2memdecode_packetis_imm ;
	  output decode2memdecode_packetis_imm5 ;
	  output decode2memdecode_packetis_signed ;
	  output decode2memdecode_packetcarry_in ;
	  output decode2memdecode_packetcarry_out ;
	  output decode2memdecode_packethas_sreg_t ;
	  output decode2memdecode_packethas_sreg_s ;
	  output decode2memdecode_packethas_dreg ;
	  output decode2memdecode_packetis_prefetch ;
	  output decode2memdecode_packetsprf_dest ;
	  output decode2memdecode_packetsprf_src ;
	  output decode2memdecode_packetis_atomic ;
	  output decode2memdecode_packetis_ldl ;
	  output decode2memdecode_packetis_stl ;
	  output decode2memdecode_packetis_stc ;
	  output decode2memdecode_packetis_valid ;
	  output decode2memdecode_packetis_halt ;
	  output decode2memdecode_packetis_compare ;
	  output decode2memdecode_packetdreg_is_src ;
	  output decode2memdecode_packetdreg_is_dest ;
	  output [31 : 0] decode2fpupc;
	  output [31 : 0] decode2fpupc_incr;
	  output [1 : 0] decode2fputhread_num;
	  output decode2fpubranch_taken ;
	  output [31 : 0] decode2fpupredicted_pc;
	  output decode2fpuvalid ;
	  output decode2fpuis_oldest ;
	  output [31 : 0] decode2fpusreg_t_data;
	  output [31 : 0] decode2fpusreg_s_data;
	  output [31 : 0] decode2fpuresult;
	  output decode2fpuL1D_hit ;
	  output [4 : 0] decode2fpudecode_packetsreg_t;
	  output [4 : 0] decode2fpudecode_packetsreg_s;
	  output [4 : 0] decode2fpudecode_packetdreg;
	  output [1 : 0] decode2fpudecode_packetacc;
	  output [15 : 0] decode2fpudecode_packetimm16_value;
	  output [25 : 0] decode2fpudecode_packetimm26_value;
	  output [4 : 0] decode2fpudecode_packetimm5_value;
	  output [3 : 0] decode2fpudecode_packetop_FU_type;
	  output [4 : 0] decode2fpudecode_packetalu_op;
	  output [4 : 0] decode2fpudecode_packetfpu_op;
	  output [3 : 0] decode2fpudecode_packetop_MEM_type;
	  output [1 : 0] decode2fpudecode_packetop_BR_type;
	  output [1 : 0] decode2fpudecode_packetshift_type;
	  output [2 : 0] decode2fpudecode_packetcompare_type;
	  output decode2fpudecode_packetis_imm26 ;
	  output decode2fpudecode_packetis_imm ;
	  output decode2fpudecode_packetis_imm5 ;
	  output decode2fpudecode_packetis_signed ;
	  output decode2fpudecode_packetcarry_in ;
	  output decode2fpudecode_packetcarry_out ;
	  output decode2fpudecode_packethas_sreg_t ;
	  output decode2fpudecode_packethas_sreg_s ;
	  output decode2fpudecode_packethas_dreg ;
	  output decode2fpudecode_packetis_prefetch ;
	  output decode2fpudecode_packetsprf_dest ;
	  output decode2fpudecode_packetsprf_src ;
	  output decode2fpudecode_packetis_atomic ;
	  output decode2fpudecode_packetis_ldl ;
	  output decode2fpudecode_packetis_stl ;
	  output decode2fpudecode_packetis_stc ;
	  output decode2fpudecode_packetis_valid ;
	  output decode2fpudecode_packetis_halt ;
	  output decode2fpudecode_packetis_compare ;
	  output decode2fpudecode_packetdreg_is_src ;
	  output decode2fpudecode_packetdreg_is_dest ;
	  output [31 : 0] decode2intpc;
	  output [31 : 0] decode2intpc_incr;
	  output [1 : 0] decode2intthread_num;
	  output decode2intbranch_taken ;
	  output [31 : 0] decode2intpredicted_pc;
	  output decode2intvalid ;
	  output decode2intis_oldest ;
	  output [31 : 0] decode2intsreg_t_data;
	  output [31 : 0] decode2intsreg_s_data;
	  output [31 : 0] decode2intresult;
	  output decode2intL1D_hit ;
	  output [4 : 0] decode2intdecode_packetsreg_t;
	  output [4 : 0] decode2intdecode_packetsreg_s;
	  output [4 : 0] decode2intdecode_packetdreg;
	  output [1 : 0] decode2intdecode_packetacc;
	  output [15 : 0] decode2intdecode_packetimm16_value;
	  output [25 : 0] decode2intdecode_packetimm26_value;
	  output [4 : 0] decode2intdecode_packetimm5_value;
	  output [3 : 0] decode2intdecode_packetop_FU_type;
	  output [4 : 0] decode2intdecode_packetalu_op;
	  output [4 : 0] decode2intdecode_packetfpu_op;
	  output [3 : 0] decode2intdecode_packetop_MEM_type;
	  output [1 : 0] decode2intdecode_packetop_BR_type;
	  output [1 : 0] decode2intdecode_packetshift_type;
	  output [2 : 0] decode2intdecode_packetcompare_type;
	  output decode2intdecode_packetis_imm26 ;
	  output decode2intdecode_packetis_imm ;
	  output decode2intdecode_packetis_imm5 ;
	  output decode2intdecode_packetis_signed ;
	  output decode2intdecode_packetcarry_in ;
	  output decode2intdecode_packetcarry_out ;
	  output decode2intdecode_packethas_sreg_t ;
	  output decode2intdecode_packethas_sreg_s ;
	  output decode2intdecode_packethas_dreg ;
	  output decode2intdecode_packetis_prefetch ;
	  output decode2intdecode_packetsprf_dest ;
	  output decode2intdecode_packetsprf_src ;
	  output decode2intdecode_packetis_atomic ;
	  output decode2intdecode_packetis_ldl ;
	  output decode2intdecode_packetis_stl ;
	  output decode2intdecode_packetis_stc ;
	  output decode2intdecode_packetis_valid ;
	  output decode2intdecode_packetis_halt ;
	  output decode2intdecode_packetis_compare ;
	  output decode2intdecode_packetdreg_is_src ;
	  output decode2intdecode_packetdreg_is_dest ;
	  output decode_stall ;
	  reg [31 : 0] decode2mempc;
	  reg [7:0] branvar[0:1000];
	  reg [31 : 0] decode2mempc_incr;
	  reg [1 : 0] decode2memthread_num;
	  reg decode2membranch_taken;
	  reg [31 : 0] decode2mempredicted_pc;
	  reg decode2memvalid;
	  reg decode2memis_oldest;
	  reg [31 : 0] decode2memsreg_t_data;
	  reg [31 : 0] decode2memsreg_s_data;
	  reg [31 : 0] decode2memresult;
	  reg decode2memL1D_hit;
	  reg [4 : 0] decode2memdecode_packetsreg_t;
	  reg [4 : 0] decode2memdecode_packetsreg_s;
	  reg [4 : 0] decode2memdecode_packetdreg;
	  reg [1 : 0] decode2memdecode_packetacc;
	  reg [15 : 0] decode2memdecode_packetimm16_value;
	  reg [25 : 0] decode2memdecode_packetimm26_value;
	  reg [4 : 0] decode2memdecode_packetimm5_value;
	  reg [3 : 0] decode2memdecode_packetop_FU_type;
	  reg [4 : 0] decode2memdecode_packetalu_op;
	  reg [4 : 0] decode2memdecode_packetfpu_op;
	  reg [3 : 0] decode2memdecode_packetop_MEM_type;
	  reg [1 : 0] decode2memdecode_packetop_BR_type;
	  reg [1 : 0] decode2memdecode_packetshift_type;
	  reg [2 : 0] decode2memdecode_packetcompare_type;
	  reg decode2memdecode_packetis_imm26;
	  reg decode2memdecode_packetis_imm;
	  reg decode2memdecode_packetis_imm5;
	  reg decode2memdecode_packetis_signed;
	  reg decode2memdecode_packetcarry_in;
	  reg decode2memdecode_packetcarry_out;
	  reg decode2memdecode_packethas_sreg_t;
	  reg decode2memdecode_packethas_sreg_s;
	  reg decode2memdecode_packethas_dreg;
	  reg decode2memdecode_packetis_prefetch;
	  reg decode2memdecode_packetsprf_dest;
	  reg decode2memdecode_packetsprf_src;
	  reg decode2memdecode_packetis_atomic;
	  reg decode2memdecode_packetis_ldl;
	  reg decode2memdecode_packetis_stl;
	  reg decode2memdecode_packetis_stc;
	  reg decode2memdecode_packetis_valid;
	  reg decode2memdecode_packetis_halt;
	  reg decode2memdecode_packetis_compare;
	  reg decode2memdecode_packetdreg_is_src;
	  reg decode2memdecode_packetdreg_is_dest;
	  reg [31 : 0] decode2fpupc;
	  reg [31 : 0] decode2fpupc_incr;
	  reg [1 : 0] decode2fputhread_num;
	  reg decode2fpubranch_taken;
	  reg [31 : 0] decode2fpupredicted_pc;
	  reg decode2fpuvalid;
	  reg decode2fpuis_oldest;
	  reg [31 : 0] decode2fpusreg_t_data;
	  reg [31 : 0] decode2fpusreg_s_data;
	  reg [31 : 0] decode2fpuresult;
	  reg decode2fpuL1D_hit;
	  reg [4 : 0] decode2fpudecode_packetsreg_t;
	  reg [4 : 0] decode2fpudecode_packetsreg_s;
	  reg [4 : 0] decode2fpudecode_packetdreg;
	  reg [1 : 0] decode2fpudecode_packetacc;
	  reg [15 : 0] decode2fpudecode_packetimm16_value;
	  reg [25 : 0] decode2fpudecode_packetimm26_value;
	  reg [4 : 0] decode2fpudecode_packetimm5_value;
	  reg [3 : 0] decode2fpudecode_packetop_FU_type;
	  reg [4 : 0] decode2fpudecode_packetalu_op;
	  reg [4 : 0] decode2fpudecode_packetfpu_op;
	  reg [3 : 0] decode2fpudecode_packetop_MEM_type;
	  reg [1 : 0] decode2fpudecode_packetop_BR_type;
	  reg [1 : 0] decode2fpudecode_packetshift_type;
	  reg [2 : 0] decode2fpudecode_packetcompare_type;
	  reg decode2fpudecode_packetis_imm26;
	  reg decode2fpudecode_packetis_imm;
	  reg decode2fpudecode_packetis_imm5;
	  reg decode2fpudecode_packetis_signed;
	  reg decode2fpudecode_packetcarry_in;
	  reg decode2fpudecode_packetcarry_out;
	  reg decode2fpudecode_packethas_sreg_t;
	  reg decode2fpudecode_packethas_sreg_s;
	  reg decode2fpudecode_packethas_dreg;
	  reg decode2fpudecode_packetis_prefetch;
	  reg decode2fpudecode_packetsprf_dest;
	  reg decode2fpudecode_packetsprf_src;
	  reg decode2fpudecode_packetis_atomic;
	  reg decode2fpudecode_packetis_ldl;
	  reg decode2fpudecode_packetis_stl;
	  reg decode2fpudecode_packetis_stc;
	  reg decode2fpudecode_packetis_valid;
	  reg decode2fpudecode_packetis_halt;
	  reg decode2fpudecode_packetis_compare;
	  reg decode2fpudecode_packetdreg_is_src;
	  reg decode2fpudecode_packetdreg_is_dest;
	  reg [31 : 0] decode2intpc;
	  reg [31 : 0] decode2intpc_incr;
	  reg [1 : 0] decode2intthread_num;
	  reg decode2intbranch_taken;
	  reg [31 : 0] decode2intpredicted_pc;
	  reg decode2intvalid;
	  reg decode2intis_oldest;
	  reg [31 : 0] decode2intsreg_t_data;
	  reg [31 : 0] decode2intsreg_s_data;
	  reg [31 : 0] decode2intresult;
	  reg decode2intL1D_hit;
	  reg [4 : 0] decode_packet0sreg_t;
	  reg [4 : 0] decode_packet0sreg_s;
	  reg [4 : 0] decode_packet0dreg;
	  reg [1 : 0] decode_packet0acc;
	  reg [15 : 0] decode_packet0imm16_value;
	  reg [25 : 0] decode_packet0imm26_value;
	  reg [4 : 0] decode_packet0imm5_value;
	  reg [3 : 0] decode_packet0op_FU_type;
	  reg [4 : 0] decode_packet0alu_op;
	  reg [4 : 0] decode_packet0fpu_op;
	  reg [3 : 0] decode_packet0op_MEM_type;
	  reg [1 : 0] decode_packet0op_BR_type;
	  reg [1 : 0] decode_packet0shift_type;
	  reg [2 : 0] decode_packet0compare_type;
	  reg decode_packet0is_imm26;
	  reg decode_packet0is_imm;
	  reg decode_packet0is_imm5;
	  reg decode_packet0is_signed;
	  reg decode_packet0carry_in;
	  reg decode_packet0carry_out;
	  reg decode_packet0has_sreg_t;
	  reg decode_packet0has_sreg_s;
	  reg decode_packet0has_dreg;
	  reg decode_packet0is_prefetch;
	  reg decode_packet0sprf_dest;
	  reg decode_packet0sprf_src;
	  reg decode_packet0is_atomic;
	  reg decode_packet0is_ldl;
	  reg decode_packet0is_stl;
	  reg decode_packet0is_stc;
	  reg decode_packet0is_valid;
	  reg decode_packet0is_halt;
	  reg decode_packet0is_compare;
	  reg decode_packet0dreg_is_src;
	  reg decode_packet0dreg_is_dest;
	  reg [4 : 0] decode_packet1sreg_t;
	  reg [4 : 0] decode_packet1sreg_s;
	  reg [4 : 0] decode_packet1dreg;
	  reg [1 : 0] decode_packet1acc;
	  reg [15 : 0] decode_packet1imm16_value;
	  reg [25 : 0] decode_packet1imm26_value;
	  reg [4 : 0] decode_packet1imm5_value;
	  reg [3 : 0] decode_packet1op_FU_type;
	  reg [4 : 0] decode_packet1alu_op;
	  reg [4 : 0] decode_packet1fpu_op;
	  reg [3 : 0] decode_packet1op_MEM_type;
	  reg [1 : 0] decode_packet1op_BR_type;
	  reg [1 : 0] decode_packet1shift_type;
	  reg [2 : 0] decode_packet1compare_type;
	  reg decode_packet1is_imm26;
	  reg decode_packet1is_imm;
	  reg decode_packet1is_imm5;
	  reg decode_packet1is_signed;
	  reg decode_packet1carry_in;
	  reg decode_packet1carry_out;
	  reg decode_packet1has_sreg_t;
	  reg decode_packet1has_sreg_s;
	  reg decode_packet1has_dreg;
	  reg decode_packet1is_prefetch;
	  reg decode_packet1sprf_dest;
	  reg decode_packet1sprf_src;
	  reg decode_packet1is_atomic;
	  reg decode_packet1is_ldl;
	  reg decode_packet1is_stl;
	  reg decode_packet1is_stc;
	  reg decode_packet1is_valid;
	  reg decode_packet1is_halt;
	  reg decode_packet1is_compare;
	  reg decode_packet1dreg_is_src;
	  reg decode_packet1dreg_is_dest;
	  reg [31 : 0] sreg_t_data0;
	  reg [31 : 0] sreg_t_data1;
	  reg [31 : 0] sreg_s_data0;
	  reg [31 : 0] sreg_s_data1;
	  reg [31 : 0] sprf_sreg_data;
	  reg [3 : 0] sprf_sreg_addr;
	  reg sprf_src;
	  reg sprf_dest;
	  reg [31 : 0] sreg_t_0;
	  reg [31 : 0] sreg_s_0;
	  reg [31 : 0] sreg_t_1;
	  reg [31 : 0] sreg_s_1;
	  reg instr0_issued;
	  reg schedule0;
	  reg schedule1;
	  reg issue0;
	  reg issue1;
	  reg valid_int;
	  reg valid_fpu;
	  reg valid_mem;
	  reg choice_int;
	  reg choice_fpu;
	  reg choice_mem;
	  wire issue_halt;
	  wire sreg_t_0_is_bp;
	  wire sreg_t_1_is_bp;
	  wire sreg_s_0_is_bp;
	  wire sreg_s_1_is_bp;
	  wire sreg_s_0_rdy;
	  wire sreg_t_0_rdy;
	  wire sreg_s_1_rdy;
	  wire sreg_t_1_rdy;
	  wire dreg_0_rdy;
	  wire dreg_1_rdy;
	  wire follows_vld_branch;
	  wire exec_stall;
	 assign 
	  decode_stall = ((!  branch_mispredict) && (((!  issue1) &&  fetch2decodevalid) &&  fetch2decodeinstr1_valid));
	 assign 
	  sprf_src = (  decode_packet0sprf_src |  decode_packet1sprf_src);
	 assign 
	  sprf_dest = (  decode_packet0sprf_dest |  decode_packet1sprf_dest);
	 assign 
	  sprf_sreg_addr = (  choice_int ?  decode_packet1sreg_t :  decode_packet0sreg_t);
	 assign 
	  issue_halt = (( fetch2decodevalid && (!  exec_stall)) && ( ( decode_packet1is_halt && ( ( instr0_issued &&  schedule1) || (!  fetch2decodeinstr0_valid))) || ( decode_packet0is_halt &&  schedule0)));
	 assign 
	  follows_vld_branch = (((  decode_packet0op_FU_type == 4'b1010) &&  fetch2decodeinstr0_valid) &&  branch_mispredict);
	 assign 
	  exec_stall = ( (  stall_in_0 ||  stall_in_2) ||  stall_in_1);
	  decoder0 DECODER0 (
	 .fetch2decodeinstr1_out (fetch2decodeinstr0_out[31:0]),
	 .decode_packet1sreg_t (decode_packet0sreg_t[4:0]),
	 .decode_packet1sreg_s (decode_packet0sreg_s[4:0]),
	 .decode_packet1dreg (decode_packet0dreg[4:0]),
	 .decode_packet1acc (decode_packet0acc[1:0]),
	 .decode_packet1imm16_value (decode_packet0imm16_value[15:0]),
	 .decode_packet1imm26_value (decode_packet0imm26_value[25:0]),
	 .decode_packet1imm5_value (decode_packet0imm5_value[4:0]),
	 .decode_packet1op_FU_type (decode_packet0op_FU_type[3:0]),
	 .decode_packet1alu_op (decode_packet0alu_op[4:0]),
	 .decode_packet1fpu_op (decode_packet0fpu_op[4:0]),
	 .decode_packet1op_MEM_type (decode_packet0op_MEM_type[3:0]),
	 .decode_packet1op_BR_type (decode_packet0op_BR_type[1:0]),
	 .decode_packet1shift_type (decode_packet0shift_type[1:0]),
	 .decode_packet1compare_type (decode_packet0compare_type[2:0]),
	 .decode_packet1is_imm26 ( decode_packet0is_imm26),
	 .decode_packet1is_imm ( decode_packet0is_imm),
	 .decode_packet1is_imm5 ( decode_packet0is_imm5),
	 .decode_packet1is_signed ( decode_packet0is_signed),
	 .decode_packet1carry_in ( decode_packet0carry_in),
	 .decode_packet1carry_out ( decode_packet0carry_out),
	 .decode_packet1has_sreg_t ( decode_packet0has_sreg_t),
	 .decode_packet1has_sreg_s ( decode_packet0has_sreg_s),
	 .decode_packet1has_dreg ( decode_packet0has_dreg),
	 .decode_packet1is_prefetch ( decode_packet0is_prefetch),
	 .decode_packet1sprf_dest ( decode_packet0sprf_dest),
	 .decode_packet1sprf_src ( decode_packet0sprf_src),
	 .decode_packet1is_atomic ( decode_packet0is_atomic),
	 .decode_packet1is_ldl ( decode_packet0is_ldl),
	 .decode_packet1is_stl ( decode_packet0is_stl),
	 .decode_packet1is_stc ( decode_packet0is_stc),
	 .decode_packet1is_valid ( decode_packet0is_valid),
	 .decode_packet1is_halt ( decode_packet0is_halt),
	 .decode_packet1is_compare ( decode_packet0is_compare),
	 .decode_packet1dreg_is_src ( decode_packet0dreg_is_src),
	 .decode_packet1dreg_is_dest ( decode_packet0dreg_is_dest));
	  decoder1 DECODER1 (
	 .fetch2decodeinstr1_out (fetch2decodeinstr1_out[31:0]),
	 .decode_packet1sreg_t (decode_packet1sreg_t[4:0]),
	 .decode_packet1sreg_s (decode_packet1sreg_s[4:0]),
	 .decode_packet1dreg (decode_packet1dreg[4:0]),
	 .decode_packet1acc (decode_packet1acc[1:0]),
	 .decode_packet1imm16_value (decode_packet1imm16_value[15:0]),
	 .decode_packet1imm26_value (decode_packet1imm26_value[25:0]),
	 .decode_packet1imm5_value (decode_packet1imm5_value[4:0]),
	 .decode_packet1op_FU_type (decode_packet1op_FU_type[3:0]),
	 .decode_packet1alu_op (decode_packet1alu_op[4:0]),
	 .decode_packet1fpu_op (decode_packet1fpu_op[4:0]),
	 .decode_packet1op_MEM_type (decode_packet1op_MEM_type[3:0]),
	 .decode_packet1op_BR_type (decode_packet1op_BR_type[1:0]),
	 .decode_packet1shift_type (decode_packet1shift_type[1:0]),
	 .decode_packet1compare_type (decode_packet1compare_type[2:0]),
	 .decode_packet1is_imm26 ( decode_packet1is_imm26),
	 .decode_packet1is_imm ( decode_packet1is_imm),
	 .decode_packet1is_imm5 ( decode_packet1is_imm5),
	 .decode_packet1is_signed ( decode_packet1is_signed),
	 .decode_packet1carry_in ( decode_packet1carry_in),
	 .decode_packet1carry_out ( decode_packet1carry_out),
	 .decode_packet1has_sreg_t ( decode_packet1has_sreg_t),
	 .decode_packet1has_sreg_s ( decode_packet1has_sreg_s),
	 .decode_packet1has_dreg ( decode_packet1has_dreg),
	 .decode_packet1is_prefetch ( decode_packet1is_prefetch),
	 .decode_packet1sprf_dest ( decode_packet1sprf_dest),
	 .decode_packet1sprf_src ( decode_packet1sprf_src),
	 .decode_packet1is_atomic ( decode_packet1is_atomic),
	 .decode_packet1is_ldl ( decode_packet1is_ldl),
	 .decode_packet1is_stl ( decode_packet1is_stl),
	 .decode_packet1is_stc ( decode_packet1is_stc),
	 .decode_packet1is_valid ( decode_packet1is_valid),
	 .decode_packet1is_halt ( decode_packet1is_halt),
	 .decode_packet1is_compare ( decode_packet1is_compare),
	 .decode_packet1dreg_is_src ( decode_packet1dreg_is_src),
	 .decode_packet1dreg_is_dest ( decode_packet1dreg_is_dest));
	  reg dst0_vld;
	  reg dst1_vld;
	 assign 
	  dst0_vld = (((( decode_packet0has_dreg &&  issue0) &&  schedule0) && (!  branch_mispredict)) &&  fetch2decodeinstr0_valid);
	 assign 
	  dst1_vld = ((((( decode_packet1has_dreg &&  issue1) &&  schedule1) && (!  branch_mispredict)) &&  fetch2decodeinstr1_valid) && (!  follows_vld_branch));
	  scoreboard SB (
	 .clk ( clk),
	 .reset ( reset),
	 .src0_addr ( decode_packet0sreg_s),
	 .src1_addr ( decode_packet0sreg_t),
	 .src2_addr ( decode_packet1sreg_s),
	 .src3_addr ( decode_packet1sreg_t),
	 .dst0_addr ( decode_packet0dreg),
	 .dst0_vld ( dst0_vld),
	 .dst1_addr ( decode_packet1dreg),
	 .dst1_vld ( dst1_vld),
	 .wb0_addr ( write_addr_0),
	 .wb0_vld ( write_enable_0),
	 .wb1_addr ( write_addr_1),
	 .wb1_vld ( write_enable_1),
	 .src0_rdy ( sreg_s_0_rdy),
	 .src1_rdy ( sreg_t_0_rdy),
	 .src2_rdy ( sreg_s_1_rdy),
	 .src3_rdy ( sreg_t_1_rdy),
	 .dst0_rdy ( dreg_0_rdy),
	 .dst1_rdy ( dreg_1_rdy));
	  rf_4r2w RF (
	 .clk ( clk),
	 .reset ( reset),
	 .enable (1'b1),
	 .write_addr_0 ( write_addr_0),
	 .write_addr_1 ( write_addr_1),
	 .write_data_0 ( write_data_0),
	 .write_data_1 ( write_data_1),
	 .write_enable_0 ( write_enable_0),
	 .write_enable_1 ( write_enable_1),
	 .read_addr_0 ( decode_packet0sreg_t),
	 .read_addr_1 ( decode_packet0sreg_s),
	 .read_addr_2 ( decode_packet1sreg_t),
	 .read_addr_3 ( decode_packet1sreg_s),
	 .read_enable_0 ( decode_packet0has_sreg_t),
	 .read_enable_1 ( decode_packet0has_sreg_s),
	 .read_enable_2 ( decode_packet1has_sreg_t),
	 .read_enable_3 ( decode_packet1has_sreg_s),
	 .read_data_0 ( sreg_t_data0),
	 .read_data_1 ( sreg_s_data0),
	 .read_data_2 ( sreg_t_data1),
	 .read_data_3 ( sreg_s_data1));
	  sprf SPRF (
	 .clk ( clk),
	 .reset ( reset),
	 .read_addr ( sprf_sreg_addr),
	 .read_enable ( sprf_src),
	 .write_enable ( sprf_dest),
	 .read_data ( sprf_sreg_data));
	  bypass_select0 BP_SELECT_S_0 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_s_data0),
	 .address_in_rf ( decode_packet0sreg_s),
	 .data_out ( sreg_s_0),
	 .src_is_bp ( sreg_s_0_is_bp));
	  bypass_select1 BP_SELECT_S_1 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_s_data1),
	 .address_in_rf ( decode_packet1sreg_s),
	 .data_out ( sreg_s_1),
	 .src_is_bp ( sreg_s_1_is_bp));
	  bypass_select2 BP_SELECT_T_0 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_t_data0),
	 .address_in_rf ( decode_packet0sreg_t),
	 .data_out ( sreg_t_0),
	 .src_is_bp ( sreg_t_0_is_bp));
	  bypass_select3 BP_SELECT_T_1 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_t_data1),
	 .address_in_rf ( decode_packet1sreg_t),
	 .data_out ( sreg_t_1),
	 .src_is_bp ( sreg_t_1_is_bp));
	 always @ (( fetch2decodeinstr0_valid) or ( fetch2decodevalid) or ( exec_stall) or ( sreg_t_0_is_bp) or ( sreg_t_0_rdy) or ( decode_packet0has_sreg_t) or ( sreg_s_0_is_bp) or ( sreg_s_0_rdy) or ( decode_packet0has_sreg_s) or ( dreg_0_rdy) or ( decode_packet0has_dreg) or ( instr0_issued))
	 begin
	 if((((((( fetch2decodeinstr0_valid &&  fetch2decodevalid) && (!  exec_stall)) && ( (  sreg_t_0_is_bp ||  sreg_t_0_rdy) || (!  decode_packet0has_sreg_t))) && ( (  sreg_s_0_is_bp ||  sreg_s_0_rdy) || (!  decode_packet0has_sreg_s))) && (  dreg_0_rdy || (!  decode_packet0has_dreg))) && (!  instr0_issued))) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	  schedule0 = 1'b1;
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	  schedule0 = 1'b0;
	  end
	 end
	 always @ (( exec_stall) or ( sreg_t_1_is_bp) or ( sreg_t_1_rdy) or ( decode_packet1has_sreg_t) or ( sreg_s_1_is_bp) or ( sreg_s_1_rdy) or ( decode_packet1has_sreg_s) or ( dreg_1_rdy) or ( decode_packet1has_dreg) or ( decode_packet0op_FU_type) or ( decode_packet1op_FU_type) or ( decode_packet0dreg) or ( decode_packet1dreg) or ( decode_packet0has_dreg) or ( decode_packet1sreg_t) or ( decode_packet1sreg_s) or ( fetch2decodeinstr0_valid) or ( fetch2decodeinstr1_valid) or ( issue0) or ( decode_packet1is_halt) or ( instr0_issued) or ( decode_packet1is_valid))
	 if((((((!  exec_stall) && ( (  sreg_t_1_is_bp ||  sreg_t_1_rdy) || (!  decode_packet1has_sreg_t))) && ( (  sreg_s_1_is_bp ||  sreg_s_1_rdy) || (!  decode_packet1has_sreg_s))) && (  dreg_1_rdy || (!  decode_packet1has_dreg))) && ( ( ( (((((((((  decode_packet0op_FU_type !=  decode_packet1op_FU_type) && (  decode_packet0op_FU_type != 4'b1010)) && ( ( (  decode_packet0dreg !=  decode_packet1dreg) || (!  decode_packet0has_dreg)) || (!  decode_packet1has_dreg))) && ( ( (  decode_packet0dreg !=  decode_packet1sreg_t) || (!  decode_packet0has_dreg)) || (!  decode_packet1has_sreg_t))) && ( ( (  decode_packet0dreg !=  decode_packet1sreg_s) || (!  decode_packet0has_dreg)) || (!  decode_packet1has_sreg_s))) &&  fetch2decodeinstr0_valid) &&  fetch2decodeinstr1_valid) &&  issue0) && (!  decode_packet1is_halt)) ||  instr0_issued) || ( issue0 && (!  decode_packet1is_valid))) || (!  fetch2decodeinstr0_valid)))) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	  schedule1 = 1'b1;
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	  schedule1 = 1'b0;
	  end
	 always @ (( schedule0) or ( decode_packet0op_FU_type) or ( decode_packet0is_valid) or ( stall_in_0) or ( decode_packet1is_valid))
	 if((( schedule0 && ( ( (  decode_packet0op_FU_type == 4'b0001) || (  decode_packet0op_FU_type == 4'b0110)) || (  decode_packet0op_FU_type == 4'b1010))) &&  decode_packet0is_valid)) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  valid_int = (!  stall_in_0);
	  choice_int = 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 begin
	  choice_int = 1'b1;
	  valid_int = ((( schedule1 && ( ( (  decode_packet1op_FU_type == 4'b0001) || (  decode_packet1op_FU_type == 4'b0110)) || (  decode_packet1op_FU_type == 4'b1010))) &&  decode_packet1is_valid) && (!  stall_in_0));
	 end
	  end
	 always @ (( schedule0) or ( decode_packet0op_FU_type) or ( decode_packet0is_valid) or ( stall_in_2) or ( schedule1) or ( decode_packet1op_FU_type) or ( decode_packet1is_valid))
	 if((( schedule0 && (  decode_packet0op_FU_type == 4'b0011)) &&  decode_packet0is_valid)) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  valid_fpu = (!  stall_in_2);
	  choice_fpu = 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 begin
	  valid_fpu = ((( schedule1 && (  decode_packet1op_FU_type == 4'b0011)) &&  decode_packet1is_valid) && (!  stall_in_2));
	  choice_fpu = 1'b1;
	 end
	  end
	 always @ (( schedule0) or ( decode_packet0op_FU_type) or ( decode_packet0is_valid) or ( stall_in_1) or ( schedule1) or ( decode_packet1op_FU_type) or ( decode_packet1is_valid))
	 if((( schedule0 && (  decode_packet0op_FU_type == 4'b1000)) &&  decode_packet0is_valid)) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	 begin
	  valid_mem = (!  stall_in_1);
	  choice_mem = 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	 begin
	  valid_mem = ((( schedule1 && (  decode_packet1op_FU_type == 4'b1000)) &&  decode_packet1is_valid) && (!  stall_in_1));
	  choice_mem = 1'b1;
	 end
	  end
	 assign 
	  issue0 = ( fetch2decodeinstr0_valid && ( ( ( (!  decode_packet0is_valid) || ( valid_mem && (!  choice_mem))) || ( valid_fpu && (!  choice_fpu))) || ( valid_int && (!  choice_int))));
	 assign 
	  issue1 = ( fetch2decodeinstr1_valid && ( ( ( (( (  issue0 ||  instr0_issued) || (!  fetch2decodeinstr0_valid)) && (!  decode_packet1is_valid)) || ( valid_mem &&  choice_mem)) || ( valid_fpu &&  choice_fpu)) || ( valid_int &&  choice_int)));
	 always @ (posedge  clk or posedge  reset)
	 if( reset) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  decode2intdecode_packetis_halt <= 1'b0;
	  decode2fpudecode_packetis_halt <= 1'b0;
	  decode2memdecode_packetis_halt <= 1'b0;
	  instr0_issued <= 1'b0;
	  decode2intvalid <= 1'b0;
	  decode2fpuvalid <= 1'b0;
	  decode2memvalid <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 begin
	 if( exec_stall) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  decode2intpc <=  decode2intpc;
	  decode2intpc_incr <=  decode2intpc_incr;
	  decode2intthread_num <=  decode2intthread_num;
	  decode2intbranch_taken <=  decode2intbranch_taken;
	  decode2intpredicted_pc <=  decode2intpredicted_pc;
	  decode2intvalid <=  decode2intvalid;
	  decode2intis_oldest <=  decode2intis_oldest;
	  decode2intsreg_t_data <=  decode2intsreg_t_data;
	  decode2intsreg_s_data <=  decode2intsreg_s_data;
	  decode2intresult <=  decode2intresult;
	  decode2intL1D_hit <=  decode2intL1D_hit;
	  decode2intdecode_packetsreg_t <=  decode2intdecode_packetsreg_t;
	  decode2intdecode_packetsreg_s <=  decode2intdecode_packetsreg_s;
	  decode2intdecode_packetdreg <=  decode2intdecode_packetdreg;
	  decode2intdecode_packetacc <=  decode2intdecode_packetacc;
	  decode2intdecode_packetimm16_value <=  decode2intdecode_packetimm16_value;
	  decode2intdecode_packetimm26_value <=  decode2intdecode_packetimm26_value;
	  decode2intdecode_packetimm5_value <=  decode2intdecode_packetimm5_value;
	  decode2intdecode_packetop_FU_type <=  decode2intdecode_packetop_FU_type;
	  decode2intdecode_packetalu_op <=  decode2intdecode_packetalu_op;
	  decode2intdecode_packetfpu_op <=  decode2intdecode_packetfpu_op;
	  decode2intdecode_packetop_MEM_type <=  decode2intdecode_packetop_MEM_type;
	  decode2intdecode_packetop_BR_type <=  decode2intdecode_packetop_BR_type;
	  decode2intdecode_packetshift_type <=  decode2intdecode_packetshift_type;
	  decode2intdecode_packetcompare_type <=  decode2intdecode_packetcompare_type;
	  decode2intdecode_packetis_imm26 <=  decode2intdecode_packetis_imm26;
	  decode2intdecode_packetis_imm <=  decode2intdecode_packetis_imm;
	  decode2intdecode_packetis_imm5 <=  decode2intdecode_packetis_imm5;
	  decode2intdecode_packetis_signed <=  decode2intdecode_packetis_signed;
	  decode2intdecode_packetcarry_in <=  decode2intdecode_packetcarry_in;
	  decode2intdecode_packetcarry_out <=  decode2intdecode_packetcarry_out;
	  decode2intdecode_packethas_sreg_t <=  decode2intdecode_packethas_sreg_t;
	  decode2intdecode_packethas_sreg_s <=  decode2intdecode_packethas_sreg_s;
	  decode2intdecode_packethas_dreg <=  decode2intdecode_packethas_dreg;
	  decode2intdecode_packetis_prefetch <=  decode2intdecode_packetis_prefetch;
	  decode2intdecode_packetsprf_dest <=  decode2intdecode_packetsprf_dest;
	  decode2intdecode_packetsprf_src <=  decode2intdecode_packetsprf_src;
	  decode2intdecode_packetis_atomic <=  decode2intdecode_packetis_atomic;
	  decode2intdecode_packetis_ldl <=  decode2intdecode_packetis_ldl;
	  decode2intdecode_packetis_stl <=  decode2intdecode_packetis_stl;
	  decode2intdecode_packetis_stc <=  decode2intdecode_packetis_stc;
	  decode2intdecode_packetis_valid <=  decode2intdecode_packetis_valid;
	  decode2intdecode_packetis_halt <=  decode2intdecode_packetis_halt;
	  decode2intdecode_packetis_compare <=  decode2intdecode_packetis_compare;
	  decode2intdecode_packetdreg_is_src <=  decode2intdecode_packetdreg_is_src;
	  decode2intdecode_packetdreg_is_dest <=  decode2intdecode_packetdreg_is_dest;
	  decode2fpupc <=  decode2fpupc;
	  decode2fpupc_incr <=  decode2fpupc_incr;
	  decode2fputhread_num <=  decode2fputhread_num;
	  decode2fpubranch_taken <=  decode2fpubranch_taken;
	  decode2fpupredicted_pc <=  decode2fpupredicted_pc;
	  decode2fpuvalid <=  decode2fpuvalid;
	  decode2fpuis_oldest <=  decode2fpuis_oldest;
	  decode2fpusreg_t_data <=  decode2fpusreg_t_data;
	  decode2fpusreg_s_data <=  decode2fpusreg_s_data;
	  decode2fpuresult <=  decode2fpuresult;
	  decode2fpuL1D_hit <=  decode2fpuL1D_hit;
	  decode2fpudecode_packetsreg_t <=  decode2fpudecode_packetsreg_t;
	  decode2fpudecode_packetsreg_s <=  decode2fpudecode_packetsreg_s;
	  decode2fpudecode_packetdreg <=  decode2fpudecode_packetdreg;
	  decode2fpudecode_packetacc <=  decode2fpudecode_packetacc;
	  decode2fpudecode_packetimm16_value <=  decode2fpudecode_packetimm16_value;
	  decode2fpudecode_packetimm26_value <=  decode2fpudecode_packetimm26_value;
	  decode2fpudecode_packetimm5_value <=  decode2fpudecode_packetimm5_value;
	  decode2fpudecode_packetop_FU_type <=  decode2fpudecode_packetop_FU_type;
	  decode2fpudecode_packetalu_op <=  decode2fpudecode_packetalu_op;
	  decode2fpudecode_packetfpu_op <=  decode2fpudecode_packetfpu_op;
	  decode2fpudecode_packetop_MEM_type <=  decode2fpudecode_packetop_MEM_type;
	  decode2fpudecode_packetop_BR_type <=  decode2fpudecode_packetop_BR_type;
	  decode2fpudecode_packetshift_type <=  decode2fpudecode_packetshift_type;
	  decode2fpudecode_packetcompare_type <=  decode2fpudecode_packetcompare_type;
	  decode2fpudecode_packetis_imm26 <=  decode2fpudecode_packetis_imm26;
	  decode2fpudecode_packetis_imm <=  decode2fpudecode_packetis_imm;
	  decode2fpudecode_packetis_imm5 <=  decode2fpudecode_packetis_imm5;
	  decode2fpudecode_packetis_signed <=  decode2fpudecode_packetis_signed;
	  decode2fpudecode_packetcarry_in <=  decode2fpudecode_packetcarry_in;
	  decode2fpudecode_packetcarry_out <=  decode2fpudecode_packetcarry_out;
	  decode2fpudecode_packethas_sreg_t <=  decode2fpudecode_packethas_sreg_t;
	  decode2fpudecode_packethas_sreg_s <=  decode2fpudecode_packethas_sreg_s;
	  decode2fpudecode_packethas_dreg <=  decode2fpudecode_packethas_dreg;
	  decode2fpudecode_packetis_prefetch <=  decode2fpudecode_packetis_prefetch;
	  decode2fpudecode_packetsprf_dest <=  decode2fpudecode_packetsprf_dest;
	  decode2fpudecode_packetsprf_src <=  decode2fpudecode_packetsprf_src;
	  decode2fpudecode_packetis_atomic <=  decode2fpudecode_packetis_atomic;
	  decode2fpudecode_packetis_ldl <=  decode2fpudecode_packetis_ldl;
	  decode2fpudecode_packetis_stl <=  decode2fpudecode_packetis_stl;
	  decode2fpudecode_packetis_stc <=  decode2fpudecode_packetis_stc;
	  decode2fpudecode_packetis_valid <=  decode2fpudecode_packetis_valid;
	  decode2fpudecode_packetis_halt <=  decode2fpudecode_packetis_halt;
	  decode2fpudecode_packetis_compare <=  decode2fpudecode_packetis_compare;
	  decode2fpudecode_packetdreg_is_src <=  decode2fpudecode_packetdreg_is_src;
	  decode2fpudecode_packetdreg_is_dest <=  decode2fpudecode_packetdreg_is_dest;
	  decode2mempc <=  decode2mempc;
	  decode2mempc_incr <=  decode2mempc_incr;
	  decode2memthread_num <=  decode2memthread_num;
	  decode2membranch_taken <=  decode2membranch_taken;
	  decode2mempredicted_pc <=  decode2mempredicted_pc;
	  decode2memvalid <=  decode2memvalid;
	  decode2memis_oldest <=  decode2memis_oldest;
	  decode2memsreg_t_data <=  decode2memsreg_t_data;
	  decode2memsreg_s_data <=  decode2memsreg_s_data;
	  decode2memresult <=  decode2memresult;
	  decode2memL1D_hit <=  decode2memL1D_hit;
	  decode2memdecode_packetsreg_t <=  decode2memdecode_packetsreg_t;
	  decode2memdecode_packetsreg_s <=  decode2memdecode_packetsreg_s;
	  decode2memdecode_packetdreg <=  decode2memdecode_packetdreg;
	  decode2memdecode_packetacc <=  decode2memdecode_packetacc;
	  decode2memdecode_packetimm16_value <=  decode2memdecode_packetimm16_value;
	  decode2memdecode_packetimm26_value <=  decode2memdecode_packetimm26_value;
	  decode2memdecode_packetimm5_value <=  decode2memdecode_packetimm5_value;
	  decode2memdecode_packetop_FU_type <=  decode2memdecode_packetop_FU_type;
	  decode2memdecode_packetalu_op <=  decode2memdecode_packetalu_op;
	  decode2memdecode_packetfpu_op <=  decode2memdecode_packetfpu_op;
	  decode2memdecode_packetop_MEM_type <=  decode2memdecode_packetop_MEM_type;
	  decode2memdecode_packetop_BR_type <=  decode2memdecode_packetop_BR_type;
	  decode2memdecode_packetshift_type <=  decode2memdecode_packetshift_type;
	  decode2memdecode_packetcompare_type <=  decode2memdecode_packetcompare_type;
	  decode2memdecode_packetis_imm26 <=  decode2memdecode_packetis_imm26;
	  decode2memdecode_packetis_imm <=  decode2memdecode_packetis_imm;
	  decode2memdecode_packetis_imm5 <=  decode2memdecode_packetis_imm5;
	  decode2memdecode_packetis_signed <=  decode2memdecode_packetis_signed;
	  decode2memdecode_packetcarry_in <=  decode2memdecode_packetcarry_in;
	  decode2memdecode_packetcarry_out <=  decode2memdecode_packetcarry_out;
	  decode2memdecode_packethas_sreg_t <=  decode2memdecode_packethas_sreg_t;
	  decode2memdecode_packethas_sreg_s <=  decode2memdecode_packethas_sreg_s;
	  decode2memdecode_packethas_dreg <=  decode2memdecode_packethas_dreg;
	  decode2memdecode_packetis_prefetch <=  decode2memdecode_packetis_prefetch;
	  decode2memdecode_packetsprf_dest <=  decode2memdecode_packetsprf_dest;
	  decode2memdecode_packetsprf_src <=  decode2memdecode_packetsprf_src;
	  decode2memdecode_packetis_atomic <=  decode2memdecode_packetis_atomic;
	  decode2memdecode_packetis_ldl <=  decode2memdecode_packetis_ldl;
	  decode2memdecode_packetis_stl <=  decode2memdecode_packetis_stl;
	  decode2memdecode_packetis_stc <=  decode2memdecode_packetis_stc;
	  decode2memdecode_packetis_valid <=  decode2memdecode_packetis_valid;
	  decode2memdecode_packetis_halt <=  decode2memdecode_packetis_halt;
	  decode2memdecode_packetis_compare <=  decode2memdecode_packetis_compare;
	  decode2memdecode_packetdreg_is_src <=  decode2memdecode_packetdreg_is_src;
	  decode2memdecode_packetdreg_is_dest <=  decode2memdecode_packetdreg_is_dest;
	  instr0_issued <=  instr0_issued;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 begin
	  decode2intvalid <= ((( valid_int && (!  issue_halt)) && (!  branch_mispredict)) &&  fetch2decodevalid);
	  decode2fpuvalid <= (((( valid_fpu && (!  issue_halt)) && (!  branch_mispredict)) &&  fetch2decodevalid) && (!  follows_vld_branch));
	  decode2memvalid <= (((( valid_mem && (!  issue_halt)) && (!  branch_mispredict)) &&  fetch2decodevalid) && (!  follows_vld_branch));
	  decode2intbranch_taken <=  fetch2decodebranch_taken;
	  decode2intpredicted_pc <=  fetch2decodepredicted_pc;
	 if( choice_int) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  decode2intdecode_packetsreg_t <=  decode_packet1sreg_t;
	  decode2intdecode_packetsreg_s <=  decode_packet1sreg_s;
	  decode2intdecode_packetdreg <=  decode_packet1dreg;
	  decode2intdecode_packetacc <=  decode_packet1acc;
	  decode2intdecode_packetimm16_value <=  decode_packet1imm16_value;
	  decode2intdecode_packetimm26_value <=  decode_packet1imm26_value;
	  decode2intdecode_packetimm5_value <=  decode_packet1imm5_value;
	  decode2intdecode_packetop_FU_type <=  decode_packet1op_FU_type;
	  decode2intdecode_packetalu_op <=  decode_packet1alu_op;
	  decode2intdecode_packetfpu_op <=  decode_packet1fpu_op;
	  decode2intdecode_packetop_MEM_type <=  decode_packet1op_MEM_type;
	  decode2intdecode_packetop_BR_type <=  decode_packet1op_BR_type;
	  decode2intdecode_packetshift_type <=  decode_packet1shift_type;
	  decode2intdecode_packetcompare_type <=  decode_packet1compare_type;
	  decode2intdecode_packetis_imm26 <=  decode_packet1is_imm26;
	  decode2intdecode_packetis_imm <=  decode_packet1is_imm;
	  decode2intdecode_packetis_imm5 <=  decode_packet1is_imm5;
	  decode2intdecode_packetis_signed <=  decode_packet1is_signed;
	  decode2intdecode_packetcarry_in <=  decode_packet1carry_in;
	  decode2intdecode_packetcarry_out <=  decode_packet1carry_out;
	  decode2intdecode_packethas_sreg_t <=  decode_packet1has_sreg_t;
	  decode2intdecode_packethas_sreg_s <=  decode_packet1has_sreg_s;
	  decode2intdecode_packethas_dreg <=  decode_packet1has_dreg;
	  decode2intdecode_packetis_prefetch <=  decode_packet1is_prefetch;
	  decode2intdecode_packetsprf_dest <=  decode_packet1sprf_dest;
	  decode2intdecode_packetsprf_src <=  decode_packet1sprf_src;
	  decode2intdecode_packetis_atomic <=  decode_packet1is_atomic;
	  decode2intdecode_packetis_ldl <=  decode_packet1is_ldl;
	  decode2intdecode_packetis_stl <=  decode_packet1is_stl;
	  decode2intdecode_packetis_stc <=  decode_packet1is_stc;
	  decode2intdecode_packetis_valid <=  decode_packet1is_valid;
	  decode2intdecode_packetis_halt <=  decode_packet1is_halt;
	  decode2intdecode_packetis_compare <=  decode_packet1is_compare;
	  decode2intdecode_packetdreg_is_src <=  decode_packet1dreg_is_src;
	  decode2intdecode_packetdreg_is_dest <=  decode_packet1dreg_is_dest;
	 if( sprf_src) 
	  begin 
	 branvar[15] <= branvar[15] + 1; 
	 begin
	  decode2intsreg_t_data <=  sprf_sreg_data;
	 end
	  end
	 else 
	   begin 
	   branvar[16] <= branvar[16] + 1;   
	 begin
	  decode2intsreg_t_data <=  sreg_t_1;
	 end
	  end
	  decode2intsreg_s_data <=  sreg_s_1;
	  decode2intpc <=  fetch2decodepc1;
	  decode2intpc_incr <=  fetch2decodepc1_incr;
	  decode2intis_oldest <= (!  issue0);
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 begin
	  decode2intdecode_packetsreg_t <=  decode_packet0sreg_t;
	  decode2intdecode_packetsreg_s <=  decode_packet0sreg_s;
	  decode2intdecode_packetdreg <=  decode_packet0dreg;
	  decode2intdecode_packetacc <=  decode_packet0acc;
	  decode2intdecode_packetimm16_value <=  decode_packet0imm16_value;
	  decode2intdecode_packetimm26_value <=  decode_packet0imm26_value;
	  decode2intdecode_packetimm5_value <=  decode_packet0imm5_value;
	  decode2intdecode_packetop_FU_type <=  decode_packet0op_FU_type;
	  decode2intdecode_packetalu_op <=  decode_packet0alu_op;
	  decode2intdecode_packetfpu_op <=  decode_packet0fpu_op;
	  decode2intdecode_packetop_MEM_type <=  decode_packet0op_MEM_type;
	  decode2intdecode_packetop_BR_type <=  decode_packet0op_BR_type;
	  decode2intdecode_packetshift_type <=  decode_packet0shift_type;
	  decode2intdecode_packetcompare_type <=  decode_packet0compare_type;
	  decode2intdecode_packetis_imm26 <=  decode_packet0is_imm26;
	  decode2intdecode_packetis_imm <=  decode_packet0is_imm;
	  decode2intdecode_packetis_imm5 <=  decode_packet0is_imm5;
	  decode2intdecode_packetis_signed <=  decode_packet0is_signed;
	  decode2intdecode_packetcarry_in <=  decode_packet0carry_in;
	  decode2intdecode_packetcarry_out <=  decode_packet0carry_out;
	  decode2intdecode_packethas_sreg_t <=  decode_packet0has_sreg_t;
	  decode2intdecode_packethas_sreg_s <=  decode_packet0has_sreg_s;
	  decode2intdecode_packethas_dreg <=  decode_packet0has_dreg;
	  decode2intdecode_packetis_prefetch <=  decode_packet0is_prefetch;
	  decode2intdecode_packetsprf_dest <=  decode_packet0sprf_dest;
	  decode2intdecode_packetsprf_src <=  decode_packet0sprf_src;
	  decode2intdecode_packetis_atomic <=  decode_packet0is_atomic;
	  decode2intdecode_packetis_ldl <=  decode_packet0is_ldl;
	  decode2intdecode_packetis_stl <=  decode_packet0is_stl;
	  decode2intdecode_packetis_stc <=  decode_packet0is_stc;
	  decode2intdecode_packetis_valid <=  decode_packet0is_valid;
	  decode2intdecode_packetis_halt <=  decode_packet0is_halt;
	  decode2intdecode_packetis_compare <=  decode_packet0is_compare;
	  decode2intdecode_packetdreg_is_src <=  decode_packet0dreg_is_src;
	  decode2intdecode_packetdreg_is_dest <=  decode_packet0dreg_is_dest;
	 if( sprf_src) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  decode2intsreg_t_data <=  sprf_sreg_data;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 begin
	  decode2intsreg_t_data <=  sreg_t_0;
	 end
	  end
	  decode2intsreg_s_data <=  sreg_s_0;
	  decode2intpc <=  fetch2decodepc0;
	  decode2intpc_incr <=  fetch2decodepc0_incr;
	  decode2intis_oldest <= 1'b1;
	 end
	  end
	 if( choice_fpu) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  decode2fpudecode_packetsreg_t <=  decode_packet1sreg_t;
	  decode2fpudecode_packetsreg_s <=  decode_packet1sreg_s;
	  decode2fpudecode_packetdreg <=  decode_packet1dreg;
	  decode2fpudecode_packetacc <=  decode_packet1acc;
	  decode2fpudecode_packetimm16_value <=  decode_packet1imm16_value;
	  decode2fpudecode_packetimm26_value <=  decode_packet1imm26_value;
	  decode2fpudecode_packetimm5_value <=  decode_packet1imm5_value;
	  decode2fpudecode_packetop_FU_type <=  decode_packet1op_FU_type;
	  decode2fpudecode_packetalu_op <=  decode_packet1alu_op;
	  decode2fpudecode_packetfpu_op <=  decode_packet1fpu_op;
	  decode2fpudecode_packetop_MEM_type <=  decode_packet1op_MEM_type;
	  decode2fpudecode_packetop_BR_type <=  decode_packet1op_BR_type;
	  decode2fpudecode_packetshift_type <=  decode_packet1shift_type;
	  decode2fpudecode_packetcompare_type <=  decode_packet1compare_type;
	  decode2fpudecode_packetis_imm26 <=  decode_packet1is_imm26;
	  decode2fpudecode_packetis_imm <=  decode_packet1is_imm;
	  decode2fpudecode_packetis_imm5 <=  decode_packet1is_imm5;
	  decode2fpudecode_packetis_signed <=  decode_packet1is_signed;
	  decode2fpudecode_packetcarry_in <=  decode_packet1carry_in;
	  decode2fpudecode_packetcarry_out <=  decode_packet1carry_out;
	  decode2fpudecode_packethas_sreg_t <=  decode_packet1has_sreg_t;
	  decode2fpudecode_packethas_sreg_s <=  decode_packet1has_sreg_s;
	  decode2fpudecode_packethas_dreg <=  decode_packet1has_dreg;
	  decode2fpudecode_packetis_prefetch <=  decode_packet1is_prefetch;
	  decode2fpudecode_packetsprf_dest <=  decode_packet1sprf_dest;
	  decode2fpudecode_packetsprf_src <=  decode_packet1sprf_src;
	  decode2fpudecode_packetis_atomic <=  decode_packet1is_atomic;
	  decode2fpudecode_packetis_ldl <=  decode_packet1is_ldl;
	  decode2fpudecode_packetis_stl <=  decode_packet1is_stl;
	  decode2fpudecode_packetis_stc <=  decode_packet1is_stc;
	  decode2fpudecode_packetis_valid <=  decode_packet1is_valid;
	  decode2fpudecode_packetis_halt <=  decode_packet1is_halt;
	  decode2fpudecode_packetis_compare <=  decode_packet1is_compare;
	  decode2fpudecode_packetdreg_is_src <=  decode_packet1dreg_is_src;
	  decode2fpudecode_packetdreg_is_dest <=  decode_packet1dreg_is_dest;
	  decode2fpusreg_t_data <=  sreg_t_1;
	  decode2fpusreg_s_data <=  sreg_s_1;
	  decode2fpupc <=  fetch2decodepc1;
	  decode2fpupc_incr <=  fetch2decodepc1_incr;
	  decode2fpuis_oldest <= (!  issue0);
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	 begin
	  decode2fpudecode_packetsreg_t <=  decode_packet0sreg_t;
	  decode2fpudecode_packetsreg_s <=  decode_packet0sreg_s;
	  decode2fpudecode_packetdreg <=  decode_packet0dreg;
	  decode2fpudecode_packetacc <=  decode_packet0acc;
	  decode2fpudecode_packetimm16_value <=  decode_packet0imm16_value;
	  decode2fpudecode_packetimm26_value <=  decode_packet0imm26_value;
	  decode2fpudecode_packetimm5_value <=  decode_packet0imm5_value;
	  decode2fpudecode_packetop_FU_type <=  decode_packet0op_FU_type;
	  decode2fpudecode_packetalu_op <=  decode_packet0alu_op;
	  decode2fpudecode_packetfpu_op <=  decode_packet0fpu_op;
	  decode2fpudecode_packetop_MEM_type <=  decode_packet0op_MEM_type;
	  decode2fpudecode_packetop_BR_type <=  decode_packet0op_BR_type;
	  decode2fpudecode_packetshift_type <=  decode_packet0shift_type;
	  decode2fpudecode_packetcompare_type <=  decode_packet0compare_type;
	  decode2fpudecode_packetis_imm26 <=  decode_packet0is_imm26;
	  decode2fpudecode_packetis_imm <=  decode_packet0is_imm;
	  decode2fpudecode_packetis_imm5 <=  decode_packet0is_imm5;
	  decode2fpudecode_packetis_signed <=  decode_packet0is_signed;
	  decode2fpudecode_packetcarry_in <=  decode_packet0carry_in;
	  decode2fpudecode_packetcarry_out <=  decode_packet0carry_out;
	  decode2fpudecode_packethas_sreg_t <=  decode_packet0has_sreg_t;
	  decode2fpudecode_packethas_sreg_s <=  decode_packet0has_sreg_s;
	  decode2fpudecode_packethas_dreg <=  decode_packet0has_dreg;
	  decode2fpudecode_packetis_prefetch <=  decode_packet0is_prefetch;
	  decode2fpudecode_packetsprf_dest <=  decode_packet0sprf_dest;
	  decode2fpudecode_packetsprf_src <=  decode_packet0sprf_src;
	  decode2fpudecode_packetis_atomic <=  decode_packet0is_atomic;
	  decode2fpudecode_packetis_ldl <=  decode_packet0is_ldl;
	  decode2fpudecode_packetis_stl <=  decode_packet0is_stl;
	  decode2fpudecode_packetis_stc <=  decode_packet0is_stc;
	  decode2fpudecode_packetis_valid <=  decode_packet0is_valid;
	  decode2fpudecode_packetis_halt <=  decode_packet0is_halt;
	  decode2fpudecode_packetis_compare <=  decode_packet0is_compare;
	  decode2fpudecode_packetdreg_is_src <=  decode_packet0dreg_is_src;
	  decode2fpudecode_packetdreg_is_dest <=  decode_packet0dreg_is_dest;
	  decode2fpusreg_t_data <=  sreg_t_0;
	  decode2fpusreg_s_data <=  sreg_s_0;
	  decode2fpupc <=  fetch2decodepc0;
	  decode2fpupc_incr <=  fetch2decodepc0_incr;
	  decode2fpuis_oldest <= 1'b1;
	 end
	  end
	 if( choice_mem) 
	  begin 
	 branvar[22] <= branvar[22] + 1; 
	 begin
	  decode2memdecode_packetsreg_t <=  decode_packet1sreg_t;
	  decode2memdecode_packetsreg_s <=  decode_packet1sreg_s;
	  decode2memdecode_packetdreg <=  decode_packet1dreg;
	  decode2memdecode_packetacc <=  decode_packet1acc;
	  decode2memdecode_packetimm16_value <=  decode_packet1imm16_value;
	  decode2memdecode_packetimm26_value <=  decode_packet1imm26_value;
	  decode2memdecode_packetimm5_value <=  decode_packet1imm5_value;
	  decode2memdecode_packetop_FU_type <=  decode_packet1op_FU_type;
	  decode2memdecode_packetalu_op <=  decode_packet1alu_op;
	  decode2memdecode_packetfpu_op <=  decode_packet1fpu_op;
	  decode2memdecode_packetop_MEM_type <=  decode_packet1op_MEM_type;
	  decode2memdecode_packetop_BR_type <=  decode_packet1op_BR_type;
	  decode2memdecode_packetshift_type <=  decode_packet1shift_type;
	  decode2memdecode_packetcompare_type <=  decode_packet1compare_type;
	  decode2memdecode_packetis_imm26 <=  decode_packet1is_imm26;
	  decode2memdecode_packetis_imm <=  decode_packet1is_imm;
	  decode2memdecode_packetis_imm5 <=  decode_packet1is_imm5;
	  decode2memdecode_packetis_signed <=  decode_packet1is_signed;
	  decode2memdecode_packetcarry_in <=  decode_packet1carry_in;
	  decode2memdecode_packetcarry_out <=  decode_packet1carry_out;
	  decode2memdecode_packethas_sreg_t <=  decode_packet1has_sreg_t;
	  decode2memdecode_packethas_sreg_s <=  decode_packet1has_sreg_s;
	  decode2memdecode_packethas_dreg <=  decode_packet1has_dreg;
	  decode2memdecode_packetis_prefetch <=  decode_packet1is_prefetch;
	  decode2memdecode_packetsprf_dest <=  decode_packet1sprf_dest;
	  decode2memdecode_packetsprf_src <=  decode_packet1sprf_src;
	  decode2memdecode_packetis_atomic <=  decode_packet1is_atomic;
	  decode2memdecode_packetis_ldl <=  decode_packet1is_ldl;
	  decode2memdecode_packetis_stl <=  decode_packet1is_stl;
	  decode2memdecode_packetis_stc <=  decode_packet1is_stc;
	  decode2memdecode_packetis_valid <=  decode_packet1is_valid;
	  decode2memdecode_packetis_halt <=  decode_packet1is_halt;
	  decode2memdecode_packetis_compare <=  decode_packet1is_compare;
	  decode2memdecode_packetdreg_is_src <=  decode_packet1dreg_is_src;
	  decode2memdecode_packetdreg_is_dest <=  decode_packet1dreg_is_dest;
	  decode2memsreg_t_data <=  sreg_t_1;
	  decode2memsreg_s_data <=  sreg_s_1;
	  decode2mempc <=  fetch2decodepc1;
	  decode2mempc_incr <=  fetch2decodepc1_incr;
	  decode2memis_oldest <= (!  issue0);
	 end
	  end
	 else 
	   begin 
	   branvar[23] <= branvar[23] + 1;   
	 begin
	  decode2memdecode_packetsreg_t <=  decode_packet0sreg_t;
	  decode2memdecode_packetsreg_s <=  decode_packet0sreg_s;
	  decode2memdecode_packetdreg <=  decode_packet0dreg;
	  decode2memdecode_packetacc <=  decode_packet0acc;
	  decode2memdecode_packetimm16_value <=  decode_packet0imm16_value;
	  decode2memdecode_packetimm26_value <=  decode_packet0imm26_value;
	  decode2memdecode_packetimm5_value <=  decode_packet0imm5_value;
	  decode2memdecode_packetop_FU_type <=  decode_packet0op_FU_type;
	  decode2memdecode_packetalu_op <=  decode_packet0alu_op;
	  decode2memdecode_packetfpu_op <=  decode_packet0fpu_op;
	  decode2memdecode_packetop_MEM_type <=  decode_packet0op_MEM_type;
	  decode2memdecode_packetop_BR_type <=  decode_packet0op_BR_type;
	  decode2memdecode_packetshift_type <=  decode_packet0shift_type;
	  decode2memdecode_packetcompare_type <=  decode_packet0compare_type;
	  decode2memdecode_packetis_imm26 <=  decode_packet0is_imm26;
	  decode2memdecode_packetis_imm <=  decode_packet0is_imm;
	  decode2memdecode_packetis_imm5 <=  decode_packet0is_imm5;
	  decode2memdecode_packetis_signed <=  decode_packet0is_signed;
	  decode2memdecode_packetcarry_in <=  decode_packet0carry_in;
	  decode2memdecode_packetcarry_out <=  decode_packet0carry_out;
	  decode2memdecode_packethas_sreg_t <=  decode_packet0has_sreg_t;
	  decode2memdecode_packethas_sreg_s <=  decode_packet0has_sreg_s;
	  decode2memdecode_packethas_dreg <=  decode_packet0has_dreg;
	  decode2memdecode_packetis_prefetch <=  decode_packet0is_prefetch;
	  decode2memdecode_packetsprf_dest <=  decode_packet0sprf_dest;
	  decode2memdecode_packetsprf_src <=  decode_packet0sprf_src;
	  decode2memdecode_packetis_atomic <=  decode_packet0is_atomic;
	  decode2memdecode_packetis_ldl <=  decode_packet0is_ldl;
	  decode2memdecode_packetis_stl <=  decode_packet0is_stl;
	  decode2memdecode_packetis_stc <=  decode_packet0is_stc;
	  decode2memdecode_packetis_valid <=  decode_packet0is_valid;
	  decode2memdecode_packetis_halt <=  decode_packet0is_halt;
	  decode2memdecode_packetis_compare <=  decode_packet0is_compare;
	  decode2memdecode_packetdreg_is_src <=  decode_packet0dreg_is_src;
	  decode2memdecode_packetdreg_is_dest <=  decode_packet0dreg_is_dest;
	  decode2memsreg_t_data <=  sreg_t_0;
	  decode2memsreg_s_data <=  sreg_s_0;
	  decode2mempc <=  fetch2decodepc0;
	  decode2mempc_incr <=  fetch2decodepc0_incr;
	  decode2memis_oldest <= 1'b1;
	 end
	  end
	  decode2intdecode_packetis_halt <= ( issue_halt && (!  branch_mispredict));
	  decode2memdecode_packetis_halt <= ( issue_halt && (!  branch_mispredict));
	  decode2fpudecode_packetis_halt <= ( issue_halt && (!  branch_mispredict));
	 if((!  issue1)) 
	  begin 
	 branvar[24] <= branvar[24] + 1; 
	 begin
	  instr0_issued <= ((!  branch_mispredict) && (  instr0_issued ||  issue0));
	 end
	  end
	 else 
	   begin 
	   branvar[25] <= branvar[25] + 1;   
	 begin
	  instr0_issued <= 1'b0;
	 end
	  end
	 end
	  end
	 end
	  end
	  endmodule 

module decoder0 (fetch2decodeinstr1_out , decode_packet1sreg_t , decode_packet1sreg_s , decode_packet1dreg , decode_packet1acc , decode_packet1imm16_value , decode_packet1imm26_value , decode_packet1imm5_value , decode_packet1op_FU_type , decode_packet1alu_op , decode_packet1fpu_op , decode_packet1op_MEM_type , decode_packet1op_BR_type , decode_packet1shift_type , decode_packet1compare_type , decode_packet1is_imm26 , decode_packet1is_imm , decode_packet1is_imm5 , decode_packet1is_signed , decode_packet1carry_in , decode_packet1carry_out , decode_packet1has_sreg_t , decode_packet1has_sreg_s , decode_packet1has_dreg , decode_packet1is_prefetch , decode_packet1sprf_dest , decode_packet1sprf_src , decode_packet1is_atomic , decode_packet1is_ldl , decode_packet1is_stl , decode_packet1is_stc , decode_packet1is_valid , decode_packet1is_halt , decode_packet1is_compare , decode_packet1dreg_is_src , decode_packet1dreg_is_dest );

	  input [31 : 0] fetch2decodeinstr1_out;
	  output [4 : 0] decode_packet1sreg_t;
	  output [4 : 0] decode_packet1sreg_s;
	  output [4 : 0] decode_packet1dreg;
	  output [1 : 0] decode_packet1acc;
	  output [15 : 0] decode_packet1imm16_value;
	  output [25 : 0] decode_packet1imm26_value;
	  output [4 : 0] decode_packet1imm5_value;
	  output [3 : 0] decode_packet1op_FU_type;
	  output [4 : 0] decode_packet1alu_op;
	  output [4 : 0] decode_packet1fpu_op;
	  output [3 : 0] decode_packet1op_MEM_type;
	  output [1 : 0] decode_packet1op_BR_type;
	  output [1 : 0] decode_packet1shift_type;
	  output [2 : 0] decode_packet1compare_type;
	  output decode_packet1is_imm26 ;
	  output decode_packet1is_imm ;
	  output decode_packet1is_imm5 ;
	  output decode_packet1is_signed ;
	  output decode_packet1carry_in ;
	  output decode_packet1carry_out ;
	  output decode_packet1has_sreg_t ;
	  output decode_packet1has_sreg_s ;
	  output decode_packet1has_dreg ;
	  output decode_packet1is_prefetch ;
	  output decode_packet1sprf_dest ;
	  output decode_packet1sprf_src ;
	  output decode_packet1is_atomic ;
	  output decode_packet1is_ldl ;
	  output decode_packet1is_stl ;
	  output decode_packet1is_stc ;
	  output decode_packet1is_valid ;
	  output decode_packet1is_halt ;
	  output decode_packet1is_compare ;
	  output decode_packet1dreg_is_src ;
	  output decode_packet1dreg_is_dest ;
	  reg [4 : 0] decode_packet1sreg_t;
	  reg [7:0] branvar[0:1000];
	  reg [4 : 0] decode_packet1sreg_s;
	  reg [4 : 0] decode_packet1dreg;
	  reg [1 : 0] decode_packet1acc;
	  reg [15 : 0] decode_packet1imm16_value;
	  reg [25 : 0] decode_packet1imm26_value;
	  reg [4 : 0] decode_packet1imm5_value;
	  reg [3 : 0] decode_packet1op_FU_type;
	  reg [4 : 0] decode_packet1alu_op;
	  reg [4 : 0] decode_packet1fpu_op;
	  reg [3 : 0] decode_packet1op_MEM_type;
	  reg [1 : 0] decode_packet1op_BR_type;
	  reg [1 : 0] decode_packet1shift_type;
	  reg [2 : 0] decode_packet1compare_type;
	  reg decode_packet1is_imm26;
	  reg decode_packet1is_imm;
	  reg decode_packet1is_imm5;
	  reg decode_packet1is_signed;
	  reg decode_packet1carry_in;
	  reg decode_packet1carry_out;
	  reg decode_packet1has_sreg_t;
	  reg decode_packet1has_sreg_s;
	  reg decode_packet1has_dreg;
	  reg decode_packet1is_prefetch;
	  reg decode_packet1sprf_dest;
	  reg decode_packet1sprf_src;
	  reg decode_packet1is_atomic;
	  reg decode_packet1is_ldl;
	  reg decode_packet1is_stl;
	  reg decode_packet1is_stc;
	  reg decode_packet1is_valid;
	  reg decode_packet1is_halt;
	  reg decode_packet1is_compare;
	  reg decode_packet1dreg_is_src;
	  reg decode_packet1dreg_is_dest;
	  wire [3 : 0] decode_optype;
	  wire [1 : 0] decode_opc;
	  wire [1 : 0] decode_opc26;
	  wire [10 : 0] decode_opcode;
	  reg has_sreg_s;
	  reg has_dreg;
	 assign 
	  decode_optype = fetch2decodeinstr1_out[31:28];
	 assign 
	  decode_packet1dreg = fetch2decodeinstr1_out[27:23];
	 assign 
	  decode_packet1sreg_t = fetch2decodeinstr1_out[22:18];
	 assign 
	  decode_opcode = fetch2decodeinstr1_out[8:0];
	 assign 
	  decode_packet1imm5_value = fetch2decodeinstr1_out[17:13];
	 assign 
	  decode_opc = fetch2decodeinstr1_out[17:16];
	 assign 
	  decode_opc26 = fetch2decodeinstr1_out[27:26];
	 assign 
	  decode_packet1imm16_value = fetch2decodeinstr1_out[15:0];
	 assign 
	  decode_packet1imm26_value = fetch2decodeinstr1_out[25:0];
	 assign 
	  decode_packet1acc = fetch2decodeinstr1_out[12:11];
	 assign 
	  decode_packet1sreg_s = (  decode_packet1dreg_is_src