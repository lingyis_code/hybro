
module test (clock , reset , in1 , in2 , out1 , out2 );

	  input  clock;
	  input  reset;
	  input [7 : 0] in1;
	  input [7 : 0] in2;
	  output [7 : 0] out1;
	  output [7 : 0] out2;
	  reg [7 : 0] out1;
	  reg [7:0] branvar_0;
	  reg [7:0] branvar_1;
	  reg [7:0] branvar_2;
	  reg [7:0] branvar_3;
	  reg [7:0] branvar_4;
	  reg [7:0] branvar_5;
	  reg [7:0] branvar_6;
	  reg [7:0] branvar_7;
	  reg [7:0] branvar_8;
	  reg [7:0] branvar_9;
	  reg [7:0] branvar_10;
	  reg [7:0] branvar_11;
	  reg [7:0] branvar_12;
	  reg [7:0] branvar_13;
	  reg [7:0] branvar_14;
	  reg [7:0] branvar_15;
	  reg [7:0] branvar_16;
	  reg [7:0] branvar_17;
	  reg [7:0] branvar_18;
	  reg [7:0] branvar_19;
	  reg [7:0] branvar_20;
	  reg [7:0] branvar_21;
	  reg [7:0] branvar_22;
	  reg [7:0] branvar_23;
	  reg [7:0] branvar_24;
	  reg [7:0] branvar_25;
	  reg [7:0] branvar_26;
	  reg [7:0] branvar_27;
	  reg [7:0] branvar_28;
	  reg [7:0] branvar_29;
	  reg [7:0] branvar_30;
	  reg [7:0] branvar_31;
	  reg [7:0] branvar_32;
	  reg [7:0] branvar_33;
	  reg [7:0] branvar_34;
	  reg [7:0] branvar_35;
	  reg [7:0] branvar_36;
	  reg [7:0] branvar_37;
	  reg [7:0] branvar_38;
	  reg [7:0] branvar_39;
	  reg [7:0] branvar_40;
	  reg [7:0] branvar_41;
	  reg [7:0] branvar_42;
	  reg [7:0] branvar_43;
	  reg [7:0] branvar_44;
	  reg [7:0] branvar_45;
	  reg [7:0] branvar_46;
	  reg [7:0] branvar_47;
	  reg [7:0] branvar_48;
	  reg [7:0] branvar_49;
	  reg [7:0] branvar_50;
	  reg [7:0] branvar_51;
	  reg [7:0] branvar_52;
	  reg [7:0] branvar_53;
	  reg [7:0] branvar_54;
	  reg [7:0] branvar_55;
	  reg [7:0] branvar_56;
	  reg [7:0] branvar_57;
	  reg [7:0] branvar_58;
	  reg [7:0] branvar_59;
	  reg [7:0] branvar_60;
	  reg [7:0] branvar_61;
	  reg [7:0] branvar_62;
	  reg [7:0] branvar_63;
	  reg [7:0] branvar_64;
	  reg [7:0] branvar_65;
	  reg [7:0] branvar_66;
	  reg [7:0] branvar_67;
	  reg [7:0] branvar_68;
	  reg [7:0] branvar_69;
	  reg [7:0] branvar_70;
	  reg [7:0] branvar_71;
	  reg [7:0] branvar_72;
	  reg [7:0] branvar_73;
	  reg [7:0] branvar_74;
	  reg [7:0] branvar_75;
	  reg [7:0] branvar_76;
	  reg [7:0] branvar_77;
	  reg [7:0] branvar_78;
	  reg [7:0] branvar_79;
	  reg [7:0] branvar_80;
	  reg [7:0] branvar_81;
	  reg [7:0] branvar_82;
	  reg [7:0] branvar_83;
	  reg [7:0] branvar_84;
	  reg [7:0] branvar_85;
	  reg [7:0] branvar_86;
	  reg [7:0] branvar_87;
	  reg [7:0] branvar_88;
	  reg [7:0] branvar_89;
	  reg [7:0] branvar_90;
	  reg [7:0] branvar_91;
	  reg [7:0] branvar_92;
	  reg [7:0] branvar_93;
	  reg [7:0] branvar_94;
	  reg [7:0] branvar_95;
	  reg [7:0] branvar_96;
	  reg [7:0] branvar_97;
	  reg [7:0] branvar_98;
	  reg [7:0] branvar_99;
	  reg [7:0] branvar_100;
	 assign 
	  out2[6:0] = ((out1[6:0] && (in1[6:0]);
	 assign 
	  out2[7] = ((width 1) (binary 1));
	 always @ (posedge  clock) 
	 if( reset) 
	  begin 
	 branvar_0 <= branvar_0 + 1; 
	  out1 <= ((width 8) (hex 0));
	  end
	 else 
	   begin 
	   branvar_1 <= branvar_1 + 1;   
	 if((  in1 >  in2)) 
	  begin 
	 branvar_2 <= branvar_2 + 1; 
	  out1 <= ( in1 &&  in2);
	  end
	 else 
	   begin 
	   branvar_3 <= branvar_3 + 1;   
	  out1 <= (  in1 ||  in2);
	  end
	  end
	  endmodule 
