
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 10 "vlgParser.y"

#include "vlg2sExpr.h"

  void yyerror(char *str);
  void YYTRACE(char *str);

  extern char brep[];
  extern int bexp0, bexp1;

  char current_range[MAXSTRLEN];
  struct wire_width_tuple wire_width_table[MAXNUMVARS];
  int num_width_table_entries;
  //struct 
  _cfg_node * cfg_temp;
  _cfg_node * cfg_top;
  _cfg_node * cfg_table[MAXNUMCFGS];
  int cfg_stack_pointer = 0;
  FILE * fp_out;
  int bran_var_idx=0;
  bool bran_flag=true;
  bool parameter_start=false;
  _para_table current_para_table[MAXPARAS]; 
  int para_table_idx=0;


/* Line 189 of yacc.c  */
#line 99 "vlgParser.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     YYLID = 258,
     YYINUMBER = 259,
     YYINUMBER_BIT = 260,
     YYRNUMBER = 261,
     YYSTRING = 262,
     YYALLPATH = 263,
     YYALWAYS = 264,
     YYAND = 265,
     YYASSIGN = 266,
     YYBEGIN = 267,
     YYBUF = 268,
     YYBUFIF0 = 269,
     YYBUFIF1 = 270,
     YYCASE = 271,
     YYCASEX = 272,
     YYCASEZ = 273,
     YYCMOS = 274,
     YYCONDITIONAL = 275,
     YYDEASSIGN = 276,
     YYDEFAULT = 277,
     YYDEFPARAM = 278,
     YYDISABLE = 279,
     YYEDGE = 280,
     YYEND = 281,
     YYENDCASE = 282,
     YYENDMODULE = 283,
     YYENDFUNCTION = 284,
     YYENDPRIMITIVE = 285,
     YYENDSPECIFY = 286,
     YYENDTABLE = 287,
     YYENDTASK = 288,
     YYENUM = 289,
     YYEVENT = 290,
     YYFOR = 291,
     YYFOREVER = 292,
     YYFORK = 293,
     YYFUNCTION = 294,
     YYGEQ = 295,
     YYHIGHZ0 = 296,
     YYHIGHZ1 = 297,
     YYIF = 298,
     YYELSE = 299,
     YYLOWERTHANELSE = 300,
     YYINITIAL = 301,
     YYINOUT = 302,
     YYINPUT = 303,
     YYINTEGER = 304,
     YYJOIN = 305,
     YYLARGE = 306,
     YYLEADTO = 307,
     YYLEQ = 308,
     YYLOGAND = 309,
     YYCASEEQUALITY = 310,
     YYCASEINEQUALITY = 311,
     YYLOGNAND = 312,
     YYLOGNOR = 313,
     YYLOGOR = 314,
     YYLOGXNOR = 315,
     YYLOGEQUALITY = 316,
     YYLOGINEQUALITY = 317,
     YYLSHIFT = 318,
     YYMACROMODULE = 319,
     YYMEDIUM = 320,
     YYMODULE = 321,
     YYMREG = 322,
     YYNAND = 323,
     YYNBASSIGN = 324,
     YYNEGEDGE = 325,
     YYNMOS = 326,
     YYNOR = 327,
     YYNOT = 328,
     YYNOTIF0 = 329,
     YYNOTIF1 = 330,
     YYOR = 331,
     YYOUTPUT = 332,
     YYPARAMETER = 333,
     YYPMOS = 334,
     YYPOSEDGE = 335,
     YYPRIMITIVE = 336,
     YYPULL0 = 337,
     YYPULL1 = 338,
     YYPULLUP = 339,
     YYPULLDOWN = 340,
     YYRCMOS = 341,
     YYREAL = 342,
     YYREG = 343,
     YYREPEAT = 344,
     YYRIGHTARROW = 345,
     YYRNMOS = 346,
     YYRPMOS = 347,
     YYRSHIFT = 348,
     YYRTRAN = 349,
     YYRTRANIF0 = 350,
     YYRTRANIF1 = 351,
     YYSCALARED = 352,
     YYSMALL = 353,
     YYSPECIFY = 354,
     YYSPECPARAM = 355,
     YYSTRONG0 = 356,
     YYSTRONG1 = 357,
     YYSUPPLY0 = 358,
     YYSUPPLY1 = 359,
     YYSWIRE = 360,
     YYTABLE = 361,
     YYTASK = 362,
     YYTESLATIMER = 363,
     YYTIME = 364,
     YYTRAN = 365,
     YYTRANIF0 = 366,
     YYTRANIF1 = 367,
     YYTRI = 368,
     YYTRI0 = 369,
     YYTRI1 = 370,
     YYTRIAND = 371,
     YYTRIOR = 372,
     YYTRIREG = 373,
     YYuTYPE = 374,
     YYTYPEDEF = 375,
     YYVECTORED = 376,
     YYWAIT = 377,
     YYWAND = 378,
     YYWEAK0 = 379,
     YYWEAK1 = 380,
     YYWHILE = 381,
     YYWIRE = 382,
     YYWOR = 383,
     YYXNOR = 384,
     YYXOR = 385,
     YYsysSETUP = 386,
     YYsysID = 387,
     YYsysND = 388,
     YYUNARYOPERATOR = 389
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 36 "vlgParser.y"

  char id[MAXSTRLEN];
  _cfg_node * NODE;



/* Line 214 of yacc.c  */
#line 276 "vlgParser.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 288 "vlgParser.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  2
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   3984

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  175
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  189
/* YYNRULES -- Number of rules.  */
#define YYNRULES  481
/* YYNRULES -- Number of states.  */
#define YYNSTATES  957

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   389

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   147,     2,   173,     2,   145,   138,     2,
     150,   151,   143,   141,   152,   142,   153,   144,   158,   159,
       2,     2,     2,     2,     2,     2,     2,     2,   135,   149,
     139,   172,   140,   134,   174,     2,   163,     2,     2,     2,
     167,     2,     2,     2,     2,     2,     2,     2,   171,     2,
     169,     2,   165,     2,     2,     2,     2,     2,   161,     2,
       2,   156,     2,   157,   137,     2,     2,     2,   162,     2,
       2,     2,   166,     2,     2,     2,     2,     2,     2,     2,
     170,     2,   168,     2,   164,     2,     2,     2,     2,     2,
     160,     2,     2,   154,   136,   155,   146,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   148
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     4,     7,     9,    11,    13,    18,    19,
      20,    29,    36,    37,    41,    43,    47,    49,    50,    57,
      58,    60,    62,    66,    68,    72,    74,    79,    86,    87,
      90,    92,    94,    96,    98,   100,   102,   104,   106,   108,
     110,   112,   114,   116,   118,   120,   122,   124,   126,   128,
     129,   140,   142,   145,   147,   149,   151,   155,   157,   159,
     161,   164,   169,   171,   174,   181,   183,   185,   188,   190,
     192,   197,   199,   201,   203,   205,   207,   209,   211,   213,
     215,   217,   219,   221,   223,   225,   227,   229,   231,   233,
     235,   237,   239,   241,   243,   245,   246,   254,   255,   264,
     265,   267,   269,   271,   273,   274,   277,   279,   282,   284,
     286,   288,   290,   292,   294,   296,   298,   300,   302,   304,
     305,   310,   311,   317,   319,   323,   324,   326,   330,   332,
     333,   338,   340,   341,   342,   347,   352,   357,   362,   370,
     378,   386,   388,   390,   392,   394,   396,   398,   400,   402,
     404,   406,   408,   409,   411,   413,   416,   419,   425,   431,
     435,   439,   443,   447,   448,   455,   459,   461,   465,   467,
     471,   473,   480,   482,   484,   488,   490,   491,   493,   497,
     501,   505,   506,   508,   514,   520,   522,   524,   526,   528,
     530,   532,   534,   536,   538,   540,   541,   543,   549,   551,
     555,   560,   561,   564,   566,   568,   570,   572,   574,   576,
     578,   580,   582,   584,   586,   588,   590,   592,   594,   596,
     598,   600,   602,   604,   606,   608,   610,   612,   614,   616,
     618,   620,   622,   624,   628,   632,   637,   639,   641,   645,
     647,   648,   649,   656,   658,   659,   662,   664,   666,   669,
     672,   675,   678,   683,   685,   689,   693,   694,   700,   702,
     704,   706,   710,   712,   713,   718,   719,   721,   722,   729,
     730,   734,   735,   739,   740,   742,   743,   746,   748,   751,
     753,   754,   755,   761,   762,   763,   772,   779,   786,   789,
     795,   801,   811,   814,   815,   819,   820,   828,   829,   837,
     838,   846,   847,   855,   861,   865,   867,   869,   871,   873,
     874,   879,   883,   887,   892,   897,   898,   899,   907,   908,
     910,   913,   914,   915,   921,   922,   923,   929,   932,   933,
     938,   945,   949,   956,   958,   959,   962,   964,   966,   968,
     970,   972,   974,   975,   979,   980,   987,   990,   996,   998,
    1002,  1003,  1006,  1008,  1010,  1012,  1014,  1016,  1020,  1025,
    1031,  1037,  1039,  1043,  1045,  1050,  1057,  1059,  1068,  1070,
    1076,  1084,  1098,  1100,  1105,  1115,  1132,  1139,  1150,  1151,
    1153,  1155,  1157,  1171,  1186,  1203,  1223,  1242,  1263,  1265,
    1267,  1269,  1274,  1281,  1283,  1285,  1289,  1291,  1295,  1301,
    1303,  1307,  1309,  1312,  1315,  1318,  1321,  1324,  1327,  1330,
    1333,  1336,  1339,  1343,  1347,  1351,  1355,  1359,  1363,  1367,
    1371,  1375,  1379,  1383,  1387,  1391,  1395,  1399,  1403,  1407,
    1411,  1415,  1419,  1423,  1427,  1433,  1435,  1437,  1439,  1441,
    1443,  1448,  1455,  1457,  1459,  1461,  1465,  1470,  1475,  1477,
    1479,  1483,  1487,  1491,  1498,  1503,  1505,  1507,  1511,  1512,
    1514,  1517,  1520,  1523,  1526,  1531,  1538,  1547,  1550,  1553,
    1556,  1559,  1564,  1567,  1572,  1577,  1582,  1584,  1587,  1590,
    1593,  1597
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     176,     0,    -1,    -1,   176,   177,    -1,   179,    -1,   192,
      -1,   178,    -1,   120,   221,   220,   149,    -1,    -1,    -1,
      66,     3,   180,   182,   149,   181,   190,    28,    -1,    64,
       3,   182,   149,   190,    28,    -1,    -1,   150,   183,   151,
      -1,   184,    -1,   183,   152,   184,    -1,   186,    -1,    -1,
     153,     3,   185,   150,   186,   151,    -1,    -1,   187,    -1,
     189,    -1,   154,   188,   155,    -1,   189,    -1,   188,   152,
     189,    -1,     3,    -1,     3,   156,   350,   157,    -1,     3,
     156,   350,   135,   350,   157,    -1,    -1,   190,   191,    -1,
     231,    -1,   233,    -1,   234,    -1,   235,    -1,   236,    -1,
     240,    -1,   241,    -1,   242,    -1,   243,    -1,   244,    -1,
     263,    -1,   272,    -1,   247,    -1,   245,    -1,   328,    -1,
     289,    -1,   291,    -1,   211,    -1,   213,    -1,    -1,    81,
       3,   193,   150,   183,   151,   149,   194,   196,    30,    -1,
     195,    -1,   194,   195,    -1,   234,    -1,   240,    -1,   233,
      -1,   106,   197,    32,    -1,   198,    -1,   200,    -1,   199,
      -1,   198,   199,    -1,   202,   135,   208,   149,    -1,   201,
      -1,   200,   201,    -1,   202,   135,   206,   135,   207,   149,
      -1,   203,    -1,   204,    -1,   203,   204,    -1,   209,    -1,
     205,    -1,   150,   209,   209,   151,    -1,   210,    -1,   209,
      -1,   208,    -1,   142,    -1,   158,    -1,   159,    -1,   160,
      -1,   161,    -1,   158,    -1,   159,    -1,   160,    -1,   161,
      -1,   134,    -1,   162,    -1,   163,    -1,   164,    -1,   165,
      -1,   166,    -1,   167,    -1,   168,    -1,   169,    -1,   170,
      -1,   171,    -1,   143,    -1,    -1,   107,     3,   212,   149,
     217,   293,    33,    -1,    -1,    39,   215,     3,   214,   149,
     218,   293,    29,    -1,    -1,   216,    -1,   261,    -1,    49,
      -1,    87,    -1,    -1,   217,   219,    -1,   219,    -1,   218,
     219,    -1,   231,    -1,   233,    -1,   234,    -1,   235,    -1,
     240,    -1,   241,    -1,   242,    -1,   243,    -1,   244,    -1,
       3,    -1,   222,    -1,    -1,    34,   223,   225,   226,    -1,
      -1,    34,   224,   154,   227,   155,    -1,     3,    -1,   154,
     227,   155,    -1,    -1,   228,    -1,   227,   152,   228,    -1,
       3,    -1,    -1,     3,   229,   172,   350,    -1,   119,    -1,
      -1,    -1,    78,   232,   262,   149,    -1,    48,   260,   248,
     149,    -1,    77,   260,   248,   149,    -1,    47,   260,   248,
     149,    -1,   230,   237,   256,   238,   358,   262,   149,    -1,
     230,   237,   256,   238,   358,   248,   149,    -1,   230,   118,
     254,   238,   358,   248,   149,    -1,   105,    -1,   127,    -1,
     113,    -1,   115,    -1,   103,    -1,   123,    -1,   116,    -1,
     114,    -1,   104,    -1,   128,    -1,   117,    -1,    -1,   239,
      -1,   261,    -1,    97,   261,    -1,   121,   261,    -1,   230,
      88,   260,   249,   149,    -1,   230,    67,   260,   249,   149,
      -1,   109,   249,   149,    -1,    49,   249,   149,    -1,    87,
     248,   149,    -1,    35,   252,   149,    -1,    -1,    11,   246,
     256,   358,   262,   149,    -1,    23,   262,   149,    -1,   357,
      -1,   248,   152,   357,    -1,   250,    -1,   249,   152,   250,
      -1,   251,    -1,   251,   156,   350,   135,   350,   157,    -1,
       3,    -1,   253,    -1,   252,   152,   253,    -1,     3,    -1,
      -1,   255,    -1,   150,    98,   151,    -1,   150,    65,   151,
      -1,   150,    51,   151,    -1,    -1,   257,    -1,   150,   258,
     152,   259,   151,    -1,   150,   259,   152,   258,   151,    -1,
     103,    -1,   101,    -1,    82,    -1,   124,    -1,    41,    -1,
     104,    -1,   102,    -1,    83,    -1,   125,    -1,    42,    -1,
      -1,   261,    -1,   156,   350,   135,   350,   157,    -1,   306,
      -1,   262,   152,   306,    -1,   266,   264,   267,   149,    -1,
      -1,   264,   265,    -1,   257,    -1,   359,    -1,    10,    -1,
      68,    -1,    76,    -1,    72,    -1,   130,    -1,   129,    -1,
      13,    -1,    14,    -1,    15,    -1,    73,    -1,    74,    -1,
      75,    -1,    85,    -1,    84,    -1,    71,    -1,    79,    -1,
      91,    -1,    92,    -1,    19,    -1,    86,    -1,   110,    -1,
      94,    -1,   111,    -1,    95,    -1,   112,    -1,    96,    -1,
     108,    -1,   268,    -1,   267,   152,   268,    -1,   150,   270,
     151,    -1,   269,   150,   270,   151,    -1,     3,    -1,   271,
      -1,   270,   152,   271,    -1,   350,    -1,    -1,    -1,   275,
     273,   276,   279,   149,   274,    -1,     3,    -1,    -1,   276,
     277,    -1,   257,    -1,   278,    -1,   173,     4,    -1,   173,
       5,    -1,   173,     6,    -1,   173,   357,    -1,   173,   150,
     347,   151,    -1,   280,    -1,   279,   152,   280,    -1,   150,
     282,   151,    -1,    -1,   357,   281,   150,   282,   151,    -1,
     283,    -1,   284,    -1,   286,    -1,   283,   152,   286,    -1,
     287,    -1,    -1,   284,   152,   285,   287,    -1,    -1,   350,
      -1,    -1,   153,   357,   150,   350,   151,   288,    -1,    -1,
      46,   290,   295,    -1,    -1,     9,   292,   295,    -1,    -1,
     295,    -1,    -1,   294,   295,    -1,   149,    -1,   306,   149,
      -1,   307,    -1,    -1,    -1,   307,    44,   296,   295,   297,
      -1,    -1,    -1,    16,   150,   350,   151,   298,   311,    27,
     299,    -1,    18,   150,   350,   151,   311,    27,    -1,    17,
     150,   350,   151,   311,    27,    -1,    37,   295,    -1,    89,
     150,   350,   151,   295,    -1,   126,   150,   350,   151,   295,
      -1,    36,   150,   306,   149,   350,   149,   306,   151,   295,
      -1,   360,   295,    -1,    -1,   361,   300,   295,    -1,    -1,
     346,   310,   172,   360,   350,   149,   301,    -1,    -1,   346,
     310,   172,   361,   350,   149,   302,    -1,    -1,   346,   310,
      69,   360,   350,   149,   303,    -1,    -1,   346,   310,    69,
     361,   350,   149,   304,    -1,   122,   150,   350,   151,   295,
      -1,    90,   253,   149,    -1,   317,    -1,   319,    -1,   323,
      -1,   326,    -1,    -1,    24,     3,   305,   149,    -1,    11,
     306,   149,    -1,    21,   346,   149,    -1,   346,   310,   172,
     350,    -1,   346,   310,    69,   350,    -1,    -1,    -1,    43,
     150,   350,   308,   151,   295,   309,    -1,    -1,   312,    -1,
     311,   312,    -1,    -1,    -1,   349,   135,   313,   295,   314,
      -1,    -1,    -1,    22,   135,   315,   295,   316,    -1,    22,
     295,    -1,    -1,    12,   318,   294,    26,    -1,    12,   135,
     320,   321,   294,    26,    -1,    38,   294,    50,    -1,    38,
     135,   320,   321,   294,    50,    -1,     3,    -1,    -1,   321,
     322,    -1,   231,    -1,   240,    -1,   242,    -1,   243,    -1,
     241,    -1,   244,    -1,    -1,     3,   324,   149,    -1,    -1,
       3,   325,   150,   349,   151,   149,    -1,   327,   149,    -1,
     327,   150,   349,   151,   149,    -1,   356,    -1,    99,   329,
      31,    -1,    -1,   329,   330,    -1,   331,    -1,   332,    -1,
     340,    -1,   344,    -1,   339,    -1,   100,   262,   149,    -1,
     333,   172,   337,   149,    -1,   150,   335,    52,   335,   151,
      -1,   150,   334,     8,   336,   151,    -1,   335,    -1,   334,
     152,   335,    -1,     3,    -1,     3,   156,   350,   157,    -1,
       3,   156,   350,   149,   350,   157,    -1,   334,    -1,   150,
     334,   152,   335,   343,    20,   350,   151,    -1,   338,    -1,
     150,   338,   152,   338,   151,    -1,   150,   338,   152,   338,
     152,   338,   151,    -1,   150,   338,   152,   338,   152,   338,
     152,   338,   152,   338,   152,   338,   151,    -1,   350,    -1,
     131,   150,   151,   149,    -1,    43,   150,   350,   151,   150,
     335,   342,    52,   341,    -1,    43,   150,   350,   151,   150,
     334,   152,   335,   342,     8,   334,   151,   334,   172,   337,
     149,    -1,   335,   151,   334,   172,   337,   149,    -1,   150,
     335,   343,    20,   350,   151,   151,   172,   337,   149,    -1,
      -1,   343,    -1,   141,    -1,   142,    -1,   150,   335,    52,
     150,   335,   343,    20,   350,   151,   151,   172,   337,   149,
      -1,   150,   345,   335,    52,   150,   335,   343,    20,   350,
     151,   151,   172,   337,   149,    -1,   150,   345,   335,     8,
     150,   334,   152,   335,   343,    20,   350,   151,   151,   172,
     337,   149,    -1,    43,   150,   350,   151,   150,   335,     8,
     150,   334,   152,   335,   343,    20,   350,   151,   151,   172,
     337,   149,    -1,    43,   150,   350,   151,   150,   345,   335,
      52,   150,   335,   343,    20,   350,   151,   151,   172,   337,
     149,    -1,    43,   150,   350,   151,   150,   345,   335,     8,
     150,   334,   152,   335,   343,    20,   350,   151,   151,   172,
     337,   149,    -1,    80,    -1,    70,    -1,   357,    -1,   357,
     156,   350,   157,    -1,   357,   156,   350,   135,   350,   157,
      -1,   353,    -1,   348,    -1,   347,   152,   348,    -1,   350,
      -1,   350,   135,   350,    -1,   350,   135,   350,   135,   350,
      -1,   350,    -1,   349,   152,   350,    -1,   351,    -1,   141,
     351,    -1,   142,   351,    -1,   147,   351,    -1,   146,   351,
      -1,   138,   351,    -1,   136,   351,    -1,   137,   351,    -1,
      57,   351,    -1,    58,   351,    -1,    60,   351,    -1,   350,
     141,   350,    -1,   350,   142,   350,    -1,   350,   143,   350,
      -1,   350,   144,   350,    -1,   350,   145,   350,    -1,   350,
      61,   350,    -1,   350,    62,   350,    -1,   350,    55,   350,
      -1,   350,    56,   350,    -1,   350,    54,   350,    -1,   350,
      59,   350,    -1,   350,   139,   350,    -1,   350,   140,   350,
      -1,   350,   138,   350,    -1,   350,   136,   350,    -1,   350,
     137,   350,    -1,   350,    53,   350,    -1,   350,    69,   350,
      -1,   350,    40,   350,    -1,   350,    63,   350,    -1,   350,
      93,   350,    -1,   350,    60,   350,    -1,   350,   134,   350,
     135,   350,    -1,     7,    -1,     4,    -1,     5,    -1,     6,
      -1,   357,    -1,   357,   156,   350,   157,    -1,   357,   156,
     350,   135,   350,   157,    -1,   353,    -1,   354,    -1,   355,
      -1,   150,   348,   151,    -1,   132,   150,   352,   151,    -1,
     133,   150,   352,   151,    -1,   361,    -1,   350,    -1,   352,
     152,   361,    -1,   352,   152,   350,    -1,   154,   349,   155,
      -1,   154,   350,   154,   349,   155,   155,    -1,   357,   150,
     349,   151,    -1,   132,    -1,     3,    -1,   357,   153,     3,
      -1,    -1,   359,    -1,   173,     4,    -1,   173,     5,    -1,
     173,     6,    -1,   173,   357,    -1,   173,   150,   348,   151,
      -1,   173,   150,   348,   152,   348,   151,    -1,   173,   150,
     348,   152,   348,   152,   348,   151,    -1,   173,     4,    -1,
     173,     5,    -1,   173,     6,    -1,   173,   357,    -1,   173,
     150,   347,   151,    -1,   174,   357,    -1,   174,   150,   143,
     151,    -1,   174,   150,   362,   151,    -1,   174,   150,   363,
     151,    -1,   350,    -1,    80,   350,    -1,    70,   350,    -1,
      25,   350,    -1,   362,    76,   362,    -1,   363,    76,   362,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   260,   260,   264,   271,   275,   279,   286,   294,   303,
     293,   317,   327,   331,   339,   344,   353,   359,   358,   370,
     374,   382,   387,   395,   400,   435,   441,   446,   456,   459,
     466,   470,   474,   478,   482,   486,   497,   501,   505,   509,
     513,   517,   521,   525,   529,   533,   537,   541,   545,   553,
     552,   565,   569,   576,   580,   584,   591,   598,   602,   609,
     613,   620,   627,   631,   638,   645,   652,   656,   663,   667,
     674,   678,   685,   692,   696,   703,   707,   711,   715,   722,
     726,   730,   734,   738,   742,   746,   753,   757,   761,   765,
     769,   773,   777,   781,   785,   793,   792,   804,   803,   815,
     818,   825,   829,   833,   841,   844,   851,   855,   862,   866,
     870,   874,   878,   885,   889,   893,   897,   904,   911,   918,
     918,   922,   922,   929,   936,   941,   947,   951,   958,   962,
     962,   971,   976,   982,   982,   990,  1001,  1012,  1023,  1028,
    1037,  1046,  1050,  1054,  1058,  1062,  1066,  1070,  1074,  1078,
    1082,  1086,  1095,  1100,  1108,  1113,  1118,  1126,  1134,  1141,
    1148,  1155,  1162,  1169,  1169,  1181,  1189,  1195,  1204,  1209,
    1217,  1223,  1233,  1241,  1245,  1252,  1260,  1263,  1270,  1274,
    1278,  1286,  1289,  1296,  1300,  1307,  1311,  1315,  1319,  1323,
    1330,  1334,  1338,  1342,  1346,  1354,  1359,  1368,  1377,  1382,
    1391,  1400,  1403,  1410,  1414,  1421,  1426,  1431,  1436,  1441,
    1446,  1451,  1456,  1461,  1466,  1471,  1476,  1481,  1486,  1491,
    1496,  1501,  1506,  1511,  1516,  1521,  1526,  1531,  1536,  1541,
    1546,  1551,  1559,  1564,  1572,  1577,  1585,  1593,  1598,  1606,
    1615,  1616,  1615,  1627,  1636,  1640,  1648,  1653,  1661,  1666,
    1671,  1676,  1681,  1689,  1694,  1703,  1709,  1709,  1718,  1723,
    1732,  1737,  1745,  1751,  1751,  1765,  1769,  1777,  1777,  1792,
    1792,  1800,  1800,  1810,  1814,  1823,  1829,  1848,  1856,  1865,
    1882,  1882,  1882,  1904,  1904,  1904,  1920,  1930,  1940,  1947,
    1954,  1961,  1967,  1973,  1973,  1979,  1979,  1987,  1987,  1995,
    1995,  2004,  2004,  2013,  2018,  2023,  2029,  2034,  2039,  2045,
    2044,  2052,  2059,  2067,  2085,  2093,  2093,  2093,  2104,  2109,
    2114,  2128,  2128,  2128,  2140,  2140,  2140,  2148,  2158,  2158,
    2168,  2177,  2181,  2188,  2197,  2200,  2207,  2211,  2215,  2219,
    2223,  2227,  2236,  2235,  2243,  2242,  2252,  2256,  2263,  2272,
    2278,  2280,  2284,  2285,  2286,  2287,  2288,  2292,  2296,  2302,
    2303,  2307,  2308,  2312,  2313,  2314,  2318,  2319,  2325,  2326,
    2327,  2329,  2335,  2341,  2345,  2348,  2355,  2356,  2360,  2362,
    2366,  2367,  2371,  2374,  2377,  2381,  2386,  2390,  2398,  2399,
    2405,  2410,  2415,  2420,  2428,  2433,  2441,  2446,  2451,  2459,
    2464,  2472,  2477,  2482,  2487,  2492,  2497,  2502,  2507,  2512,
    2517,  2522,  2527,  2532,  2537,  2542,  2547,  2552,  2557,  2562,
    2567,  2572,  2577,  2582,  2587,  2592,  2597,  2602,  2607,  2612,
    2617,  2622,  2627,  2632,  2637,  2642,  2650,  2655,  2660,  2665,
    2685,  2690,  2695,  2700,  2705,  2710,  2716,  2721,  2728,  2732,
    2736,  2740,  2749,  2757,  2765,  2773,  2779,  2784,  2793,  2796,
    2803,  2807,  2811,  2815,  2819,  2823,  2827,  2835,  2839,  2843,
    2847,  2851,  2858,  2863,  2868,  2873,  2882,  2887,  2892,  2897,
    2905,  2911
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "YYLID", "YYINUMBER", "YYINUMBER_BIT",
  "YYRNUMBER", "YYSTRING", "YYALLPATH", "YYALWAYS", "YYAND", "YYASSIGN",
  "YYBEGIN", "YYBUF", "YYBUFIF0", "YYBUFIF1", "YYCASE", "YYCASEX",
  "YYCASEZ", "YYCMOS", "YYCONDITIONAL", "YYDEASSIGN", "YYDEFAULT",
  "YYDEFPARAM", "YYDISABLE", "YYEDGE", "YYEND", "YYENDCASE", "YYENDMODULE",
  "YYENDFUNCTION", "YYENDPRIMITIVE", "YYENDSPECIFY", "YYENDTABLE",
  "YYENDTASK", "YYENUM", "YYEVENT", "YYFOR", "YYFOREVER", "YYFORK",
  "YYFUNCTION", "YYGEQ", "YYHIGHZ0", "YYHIGHZ1", "YYIF", "YYELSE",
  "YYLOWERTHANELSE", "YYINITIAL", "YYINOUT", "YYINPUT", "YYINTEGER",
  "YYJOIN", "YYLARGE", "YYLEADTO", "YYLEQ", "YYLOGAND", "YYCASEEQUALITY",
  "YYCASEINEQUALITY", "YYLOGNAND", "YYLOGNOR", "YYLOGOR", "YYLOGXNOR",
  "YYLOGEQUALITY", "YYLOGINEQUALITY", "YYLSHIFT", "YYMACROMODULE",
  "YYMEDIUM", "YYMODULE", "YYMREG", "YYNAND", "YYNBASSIGN", "YYNEGEDGE",
  "YYNMOS", "YYNOR", "YYNOT", "YYNOTIF0", "YYNOTIF1", "YYOR", "YYOUTPUT",
  "YYPARAMETER", "YYPMOS", "YYPOSEDGE", "YYPRIMITIVE", "YYPULL0",
  "YYPULL1", "YYPULLUP", "YYPULLDOWN", "YYRCMOS", "YYREAL", "YYREG",
  "YYREPEAT", "YYRIGHTARROW", "YYRNMOS", "YYRPMOS", "YYRSHIFT", "YYRTRAN",
  "YYRTRANIF0", "YYRTRANIF1", "YYSCALARED", "YYSMALL", "YYSPECIFY",
  "YYSPECPARAM", "YYSTRONG0", "YYSTRONG1", "YYSUPPLY0", "YYSUPPLY1",
  "YYSWIRE", "YYTABLE", "YYTASK", "YYTESLATIMER", "YYTIME", "YYTRAN",
  "YYTRANIF0", "YYTRANIF1", "YYTRI", "YYTRI0", "YYTRI1", "YYTRIAND",
  "YYTRIOR", "YYTRIREG", "YYuTYPE", "YYTYPEDEF", "YYVECTORED", "YYWAIT",
  "YYWAND", "YYWEAK0", "YYWEAK1", "YYWHILE", "YYWIRE", "YYWOR", "YYXNOR",
  "YYXOR", "YYsysSETUP", "YYsysID", "YYsysND", "'?'", "':'", "'|'", "'^'",
  "'&'", "'<'", "'>'", "'+'", "'-'", "'*'", "'/'", "'%'", "'~'", "'!'",
  "YYUNARYOPERATOR", "';'", "'('", "')'", "','", "'.'", "'{'", "'}'",
  "'['", "']'", "'0'", "'1'", "'x'", "'X'", "'b'", "'B'", "'r'", "'R'",
  "'f'", "'F'", "'p'", "'P'", "'n'", "'N'", "'='", "'#'", "'@'", "$accept",
  "source_text", "description", "type_declaration", "module", "$@1", "$@2",
  "port_list_opt", "port_list", "port", "$@3", "port_expression_opt",
  "port_expression", "port_ref_list", "port_reference", "module_item_clr",
  "module_item", "primitive", "$@4", "primitive_declaration_eclr",
  "primitive_declaration", "table_definition", "table_entries",
  "combinational_entry_eclr", "combinational_entry",
  "sequential_entry_eclr", "sequential_entry", "input_list",
  "level_symbol_or_edge_eclr", "level_symbol_or_edge", "edge", "state",
  "next_state", "output_symbol", "level_symbol", "edge_symbol", "task",
  "$@5", "function", "$@6", "range_or_type_opt", "range_or_type",
  "tf_declaration_clr", "tf_declaration_eclr", "tf_declaration",
  "type_name", "type_specifier", "enum_specifier", "$@7", "$@8",
  "enum_name", "enum_lst_opt", "enumerator_list", "enumerator", "$@9",
  "type_decorator_opt", "parameter_declaration", "$@10",
  "input_declaration", "output_declaration", "inout_declaration",
  "net_declaration", "nettype", "expandrange_opt", "expandrange",
  "reg_declaration", "time_declaration", "integer_declaration",
  "real_declaration", "event_declaration", "continuous_assign", "$@11",
  "parameter_override", "variable_list", "register_variable_list",
  "register_variable", "name_of_register", "name_of_event_list",
  "name_of_event", "charge_strength_opt", "charge_strength",
  "drive_strength_opt", "drive_strength", "strength0", "strength1",
  "range_opt", "range", "assignment_list", "gate_instantiation",
  "drive_delay_clr", "drive_delay", "gatetype", "gate_instance_list",
  "gate_instance", "name_of_gate_instance", "terminal_list", "terminal",
  "module_or_primitive_instantiation", "$@12", "$@13",
  "name_of_module_or_primitive", "module_or_primitive_option_clr",
  "module_or_primitive_option", "delay_or_parameter_value_assignment",
  "module_or_primitive_instance_list", "module_or_primitive_instance",
  "$@14", "module_connection_list", "module_port_connection_list",
  "named_port_connection_list", "$@15", "module_port_connection",
  "named_port_connection", "$@16", "initial_statement", "$@17",
  "always_statement", "$@18", "statement_opt", "statement_clr",
  "statement", "$@19", "$@20", "$@21", "$@22", "$@23", "$@24", "$@25",
  "$@26", "$@27", "$@28", "assignment", "if_branch", "$@29", "$@30",
  "type_action", "case_item_eclr", "case_item", "$@31", "$@32", "$@33",
  "$@34", "seq_block", "$@35", "par_block", "name_of_block",
  "block_declaration_clr", "block_declaration", "task_enable", "$@36",
  "$@37", "system_task_enable", "name_of_system_task", "specify_block",
  "specify_item_clr", "specify_item", "specparam_declaration",
  "path_declaration", "path_description", "path_list",
  "specify_terminal_descriptor", "path_list_or_edge_sensitive_path_list",
  "path_delay_value", "path_delay_expression", "system_timing_check",
  "level_sensitive_path_declaration",
  "spec_terminal_desptr_or_edge_sensitive_spec_terminal_desptr",
  "polarity_operator_opt", "polarity_operator",
  "edge_sensitive_path_declaration", "edge_identifier", "lvalue",
  "mintypmax_expression_list", "mintypmax_expression", "expression_list",
  "expression", "primary", "nondeterminism_list", "concatenation",
  "multiple_concatenation", "function_call", "system_identifier",
  "identifier", "delay_opt", "delay", "delay_control", "event_control",
  "event_expression", "ored_event_expression", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,    63,    58,   124,    94,    38,    60,
      62,    43,    45,    42,    47,    37,   126,    33,   389,    59,
      40,    41,    44,    46,   123,   125,    91,    93,    48,    49,
     120,    88,    98,    66,   114,    82,   102,    70,   112,    80,
     110,    78,    61,    35,    64
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint16 yyr1[] =
{
       0,   175,   176,   176,   177,   177,   177,   178,   180,   181,
     179,   179,   182,   182,   183,   183,   184,   185,   184,   186,
     186,   187,   187,   188,   188,   189,   189,   189,   190,   190,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   191,
     191,   191,   191,   191,   191,   191,   191,   191,   191,   193,
     192,   194,   194,   195,   195,   195,   196,   197,   197,   198,
     198,   199,   200,   200,   201,   202,   203,   203,   204,   204,
     205,   205,   206,   207,   207,   208,   208,   208,   208,   209,
     209,   209,   209,   209,   209,   209,   210,   210,   210,   210,
     210,   210,   210,   210,   210,   212,   211,   214,   213,   215,
     215,   216,   216,   216,   217,   217,   218,   218,   219,   219,
     219,   219,   219,   219,   219,   219,   219,   220,   221,   223,
     222,   224,   222,   225,   226,   226,   227,   227,   228,   229,
     228,   230,   230,   232,   231,   233,   234,   235,   236,   236,
     236,   237,   237,   237,   237,   237,   237,   237,   237,   237,
     237,   237,   238,   238,   239,   239,   239,   240,   240,   241,
     242,   243,   244,   246,   245,   247,   248,   248,   249,   249,
     250,   250,   251,   252,   252,   253,   254,   254,   255,   255,
     255,   256,   256,   257,   257,   258,   258,   258,   258,   258,
     259,   259,   259,   259,   259,   260,   260,   261,   262,   262,
     263,   264,   264,   265,   265,   266,   266,   266,   266,   266,
     266,   266,   266,   266,   266,   266,   266,   266,   266,   266,
     266,   266,   266,   266,   266,   266,   266,   266,   266,   266,
     266,   266,   267,   267,   268,   268,   269,   270,   270,   271,
     273,   274,   272,   275,   276,   276,   277,   277,   278,   278,
     278,   278,   278,   279,   279,   280,   281,   280,   282,   282,
     283,   283,   284,   285,   284,   286,   286,   288,   287,   290,
     289,   292,   291,   293,   293,   294,   294,   295,   295,   295,
     296,   297,   295,   298,   299,   295,   295,   295,   295,   295,
     295,   295,   295,   300,   295,   301,   295,   302,   295,   303,
     295,   304,   295,   295,   295,   295,   295,   295,   295,   305,
     295,   295,   295,   306,   306,   308,   309,   307,   310,   311,
     311,   313,   314,   312,   315,   316,   312,   312,   318,   317,
     317,   319,   319,   320,   321,   321,   322,   322,   322,   322,
     322,   322,   324,   323,   325,   323,   326,   326,   327,   328,
     329,   329,   330,   330,   330,   330,   330,   331,   332,   333,
     333,   334,   334,   335,   335,   335,   336,   336,   337,   337,
     337,   337,   338,   339,   340,   340,   341,   341,   342,   342,
     343,   343,   344,   344,   344,   344,   344,   344,   345,   345,
     346,   346,   346,   346,   347,   347,   348,   348,   348,   349,
     349,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   351,   351,   351,   351,
     351,   351,   351,   351,   351,   351,   351,   351,   352,   352,
     352,   352,   353,   354,   355,   356,   357,   357,   358,   358,
     359,   359,   359,   359,   359,   359,   359,   360,   360,   360,
     360,   360,   361,   361,   361,   361,   362,   362,   362,   362,
     363,   363
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     0,     2,     1,     1,     1,     4,     0,     0,
       8,     6,     0,     3,     1,     3,     1,     0,     6,     0,
       1,     1,     3,     1,     3,     1,     4,     6,     0,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
      10,     1,     2,     1,     1,     1,     3,     1,     1,     1,
       2,     4,     1,     2,     6,     1,     1,     2,     1,     1,
       4,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     7,     0,     8,     0,
       1,     1,     1,     1,     0,     2,     1,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     0,
       4,     0,     5,     1,     3,     0,     1,     3,     1,     0,
       4,     1,     0,     0,     4,     4,     4,     4,     7,     7,
       7,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     0,     1,     1,     2,     2,     5,     5,     3,
       3,     3,     3,     0,     6,     3,     1,     3,     1,     3,
       1,     6,     1,     1,     3,     1,     0,     1,     3,     3,
       3,     0,     1,     5,     5,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     1,     5,     1,     3,
       4,     0,     2,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     3,     3,     4,     1,     1,     3,     1,
       0,     0,     6,     1,     0,     2,     1,     1,     2,     2,
       2,     2,     4,     1,     3,     3,     0,     5,     1,     1,
       1,     3,     1,     0,     4,     0,     1,     0,     6,     0,
       3,     0,     3,     0,     1,     0,     2,     1,     2,     1,
       0,     0,     5,     0,     0,     8,     6,     6,     2,     5,
       5,     9,     2,     0,     3,     0,     7,     0,     7,     0,
       7,     0,     7,     5,     3,     1,     1,     1,     1,     0,
       4,     3,     3,     4,     4,     0,     0,     7,     0,     1,
       2,     0,     0,     5,     0,     0,     5,     2,     0,     4,
       6,     3,     6,     1,     0,     2,     1,     1,     1,     1,
       1,     1,     0,     3,     0,     6,     2,     5,     1,     3,
       0,     2,     1,     1,     1,     1,     1,     3,     4,     5,
       5,     1,     3,     1,     4,     6,     1,     8,     1,     5,
       7,    13,     1,     4,     9,    16,     6,    10,     0,     1,
       1,     1,    13,    14,    16,    19,    18,    20,     1,     1,
       1,     4,     6,     1,     1,     3,     1,     3,     5,     1,
       3,     1,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     5,     1,     1,     1,     1,     1,
       4,     6,     1,     1,     1,     3,     4,     4,     1,     1,
       3,     3,     3,     6,     4,     1,     1,     3,     0,     1,
       2,     2,     2,     2,     4,     6,     8,     2,     2,     2,
       2,     4,     2,     4,     4,     4,     1,     2,     2,     2,
       3,     3
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint16 yydefact[] =
{
       2,     0,     1,     0,     0,     0,     0,     3,     6,     4,
       5,    12,     8,    49,   119,     0,   118,    19,     0,    12,
       0,     0,     0,   117,     0,    25,     0,     0,     0,    14,
      16,    20,    21,    28,     0,    19,   123,   125,     0,     7,
       0,    17,     0,    23,    13,    19,   132,     9,     0,     0,
     120,   128,     0,   126,   456,   436,   437,   438,   435,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   401,   442,   443,   444,   439,     0,
       0,    22,    15,   243,   271,   205,   163,   211,   212,   213,
     223,     0,    11,     0,    99,   269,   195,   195,     0,   206,
     219,   208,   214,   215,   216,   207,   195,   133,   220,   218,
     217,   224,     0,   221,   222,   226,   228,   230,   350,     0,
     231,     0,   225,   227,   229,   131,   210,   209,    29,    47,
      48,     0,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    43,    42,    40,   201,    41,   240,    45,    46,
      44,    28,     0,     0,     0,     0,   122,   409,   410,   411,
       0,     0,   407,   408,   406,   402,   403,   405,   404,     0,
     396,     0,   399,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    26,     0,     0,
       0,    19,    24,     0,   181,     0,     0,   198,   318,   393,
     390,   175,     0,   173,   102,   103,     0,     0,   100,   101,
       0,     0,   196,     0,   172,     0,   168,   170,     0,     0,
       0,   166,     0,    95,     0,   195,   195,   145,   149,   141,
     143,   148,   144,   147,   151,   176,   146,   142,   150,   181,
       0,   244,   132,   132,   124,     0,   127,     0,   449,     0,
     448,     0,   445,     0,     0,   452,     0,   430,   428,   421,
     419,   420,   422,   433,   417,   418,   431,   429,   432,     0,
       0,   426,   427,   425,   423,   424,   412,   413,   414,   415,
     416,     0,   399,   457,     0,     0,   456,     0,   328,     0,
       0,     0,     0,     0,     0,     0,   275,     0,     0,     0,
       0,     0,   455,   277,     0,   272,     0,   279,   305,   306,
     307,   308,     0,   318,   348,     0,   293,     0,   458,   182,
     165,     0,     0,     0,   162,     0,     0,    97,   270,     0,
       0,   160,     0,     0,     0,     0,   161,     0,   349,     0,
       0,     0,     0,   351,   352,   353,     0,   356,   354,   355,
       0,   159,     0,     0,     0,   152,   177,   152,   236,     0,
       0,   203,   202,     0,   232,     0,   204,     0,    10,   132,
      51,     0,    55,    53,    54,   130,     0,   472,   446,     0,
     447,   397,   400,     0,     0,    27,   454,     0,   440,    18,
       0,     0,     0,     0,   275,     0,     0,     0,     0,   309,
       0,   288,     0,     0,     0,     0,     0,     0,     0,   467,
     468,   469,     0,   470,   278,   280,   346,     0,     0,   292,
       0,   189,   194,   187,   192,   186,   191,   185,   190,   188,
     193,     0,     0,     0,   459,   199,     0,     0,     0,   174,
       0,     0,   137,   135,   169,     0,   136,   134,   167,     0,
       0,     0,   363,   389,   388,     0,   361,     0,     0,   104,
       0,     0,     0,     0,     0,     0,     0,   458,   153,   154,
     458,     0,   237,   239,   460,   461,   462,     0,   463,   200,
       0,     0,   265,     0,   246,   245,   247,     0,   253,   256,
       0,    52,     0,     0,     0,     0,     0,   476,     0,     0,
     451,   450,     0,     0,   434,     0,   343,     0,   311,   333,
     334,     0,     0,     0,     0,   312,     0,     0,   334,   331,
     276,   315,     0,   304,     0,     0,     0,   394,     0,     0,
       0,     0,   294,     0,     0,     0,   314,   313,     0,   391,
       0,   132,     0,     0,   357,     0,     0,     0,     0,     0,
       0,     0,     0,   368,   372,   132,   158,   157,   180,   179,
     178,   155,   156,     0,     0,   234,     0,     0,     0,   233,
       0,     0,     0,   258,   259,   260,   262,   266,   248,   249,
     250,     0,   251,   241,     0,     0,    83,    94,     0,    79,
      80,    81,    82,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,     0,    57,    59,    58,    62,     0,    65,
      66,    69,    68,    71,    50,   479,   478,   477,   473,     0,
     474,     0,   475,   398,   453,   441,     0,   275,   329,   283,
       0,     0,   310,     0,   275,     0,     0,     0,     0,   471,
       0,   281,     0,     0,     0,     0,     0,     0,     0,   164,
       0,   197,   132,   106,   108,   109,   110,   111,   112,   113,
     114,   115,   116,     0,     0,   373,     0,     0,   366,   361,
       0,   362,     0,     0,     0,     0,     0,   372,   358,   105,
       0,   274,     0,     0,     0,   166,   238,   464,     0,   235,
       0,   255,   265,   263,     0,   242,   265,   254,   265,     0,
      56,    60,     0,    63,     0,     0,    67,   480,   481,     0,
     336,   337,   340,   338,   339,   341,     0,   335,     0,     0,
       0,   319,     0,     0,     0,     0,     0,   289,   303,   290,
     395,   282,   347,     0,     0,     0,     0,   183,   184,   392,
     107,     0,   171,     0,     0,   364,     0,   360,     0,   359,
       0,     0,     0,    96,   140,   139,   138,     0,     0,   261,
       0,   252,     0,     0,     0,     0,    75,    76,    77,    78,
       0,     0,    72,   345,   330,     0,   324,   327,   287,   320,
     321,   286,     0,   332,   316,   299,   301,   295,   297,    98,
       0,   361,     0,     0,     0,   380,   381,     0,     0,     0,
       0,   465,     0,     0,   264,   257,    70,    75,    76,    77,
      78,     0,    61,   284,     0,     0,     0,   317,   300,   302,
     296,   298,     0,     0,     0,   379,     0,   365,   362,     0,
       0,     0,   369,     0,     0,   267,    74,     0,    73,   285,
     325,   322,     0,   362,     0,     0,     0,     0,     0,     0,
     362,     0,     0,   466,   268,    64,   326,   323,   291,     0,
       0,     0,     0,   374,     0,     0,     0,     0,     0,     0,
     370,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   362,     0,     0,     0,     0,   367,
       0,     0,     0,     0,     0,     0,     0,     0,   362,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     382,     0,     0,     0,     0,     0,     0,   376,     0,     0,
       0,   383,     0,     0,     0,     0,     0,     0,     0,   371,
     375,     0,     0,     0,     0,   384,     0,     0,     0,     0,
       0,   377,     0,   386,   385,     0,   387
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     1,     7,     8,     9,    19,   151,    18,    28,    29,
      79,    30,    31,    42,    32,    46,   128,    10,    20,   379,
     380,   502,   613,   614,   615,   616,   617,   618,   619,   620,
     621,   780,   847,   781,   622,   623,   129,   360,   130,   451,
     217,   218,   565,   662,   663,    24,    15,    16,    21,    22,
      37,    50,    52,    53,   154,   381,   664,   229,   665,   666,
     667,   136,   249,   477,   478,   668,   669,   670,   671,   672,
     142,   204,   143,   230,   225,   226,   227,   212,   213,   365,
     366,   328,   329,   441,   442,   221,   222,   206,   144,   250,
     372,   145,   373,   374,   375,   481,   482,   146,   251,   705,
     147,   377,   495,   496,   497,   498,   595,   582,   583,   584,
     770,   585,   586,   864,   148,   220,   149,   203,   690,   413,
     530,   538,   741,   728,   849,   430,   830,   831,   828,   829,
     526,   316,   317,   645,   827,   332,   730,   731,   825,   867,
     824,   866,   318,   404,   319,   520,   637,   727,   320,   400,
     401,   321,   322,   150,   232,   353,   354,   355,   356,   465,
     679,   680,   562,   563,   357,   358,   873,   834,   835,   359,
     467,   323,   536,   169,   732,   564,    74,   259,    75,    76,
      77,   324,    78,   443,   444,   325,   326,   508,   509
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -655
static const yytype_int16 yypact[] =
{
    -655,   233,  -655,    39,    74,    82,    60,  -655,  -655,  -655,
    -655,   -51,  -655,  -655,    49,   106,  -655,    35,    73,   -51,
     116,   115,   127,  -655,   121,   132,   291,   297,    18,  -655,
    -655,  -655,  -655,  -655,   155,    35,  -655,   157,   306,  -655,
     122,  -655,   -63,  -655,  -655,    35,  1959,  -655,   105,   306,
    -655,   162,   110,  -655,  -655,  -655,  -655,  -655,  -655,    97,
      97,    97,   193,   195,    97,    97,    97,    97,    97,    97,
      97,   122,   122,  1772,  -655,  -655,  -655,  -655,   -72,   200,
     297,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,     9,  -655,   349,    25,  -655,   171,   171,   367,  -655,
    -655,  -655,  -655,  -655,  -655,  -655,   171,  -655,  -655,  -655,
    -655,  -655,   369,  -655,  -655,  -655,  -655,  -655,  -655,   377,
    -655,   367,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,   697,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,  -655,   239,   119,   214,   306,  -655,  -655,  -655,  -655,
    1255,  1255,  -655,  -655,  -655,  -655,  -655,  -655,  -655,   241,
    3452,   130,  2505,   122,   122,   122,   122,   122,   122,   122,
     122,   122,   122,   122,   122,   122,   122,   122,   122,   122,
     122,   122,   122,   122,   122,   122,   122,  -655,   122,   394,
     122,    24,  -655,  1182,   251,   122,   138,  -655,  -655,  -655,
     145,  -655,   156,  -655,  -655,  -655,   122,   400,  -655,  -655,
    1182,   369,  -655,   369,  -655,   183,  -655,   252,   369,     9,
     212,   263,   290,  -655,   273,   171,   171,  -655,  -655,  -655,
    -655,  -655,  -655,  -655,  -655,   299,  -655,  -655,  -655,   251,
      12,  -655,  2071,    13,  -655,   122,  -655,    21,  3690,   208,
    -655,   282,  -655,   122,   122,  -655,   122,   174,   174,  3729,
     442,   442,  3701,  3839,   442,   442,   416,   174,   416,  3545,
    2234,  3822,  3839,  1271,   174,   174,   428,   428,  -655,  -655,
    -655,   292,  3690,  -655,  2068,   305,   321,     9,   327,   316,
     330,   337,     9,   496,   364,  1182,   387,   378,   382,   349,
     413,   415,  -655,  -655,    41,  -655,   388,   506,  -655,  -655,
    -655,  -655,   427,  -655,  -655,  1182,  -655,   596,   401,  -655,
    -655,     9,    -6,   122,  -655,   349,  3562,  -655,  -655,   274,
     293,  -655,   367,   122,   301,   308,  -655,   369,  -655,   419,
       9,   430,   334,  -655,  -655,  -655,   406,  -655,  -655,  -655,
     439,  -655,   367,   367,    15,   192,  -655,   192,  -655,  1446,
      45,  -655,  -655,   309,  -655,   440,  -655,    14,  -655,    -8,
    -655,     3,  -655,  -655,  -655,  3690,  1486,   263,  -655,  1255,
    -655,  3580,  3690,   324,   122,  -655,  -655,   122,  -655,  -655,
     444,   447,   459,   631,  -655,   122,   122,   122,   497,  -655,
       9,  -655,   631,   899,   122,   122,   501,   122,   122,  -655,
    -655,  -655,   122,   263,  -655,  -655,  -655,   122,    -4,  -655,
    1182,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,   490,   503,     9,  -655,  -655,   122,   122,  2174,  -655,
     122,   524,  -655,  -655,  -655,  3673,  -655,  -655,   263,   122,
     329,   535,   531,  -655,  -655,    44,   605,   686,  1817,  -655,
     357,   361,   540,   541,   543,   171,   171,   401,  -655,  -655,
     401,   443,  -655,  3690,  -655,  -655,  -655,   122,   263,  -655,
      22,   122,  1377,    50,  -655,  -655,  -655,   366,  -655,   263,
    1304,  -655,   665,   122,   122,   122,   550,  3690,   -46,    10,
    3690,  -655,   122,   547,  3690,  2327,  -655,   122,  -655,  -655,
    -655,   943,  2629,  2647,  2665,  -655,   554,   555,  -655,  -655,
    -655,  3690,  2771,  -655,  2789,  2807,   455,  -655,  1182,   460,
    1225,  1225,  -655,   237,   372,   375,  3690,  3690,   122,  -655,
    2352,   420,   122,  2825,  -655,   556,   122,    23,   686,    36,
      54,   122,   559,  -655,  3690,   745,  -655,  -655,  -655,  -655,
    -655,  -655,  -655,   369,     9,  -655,   122,   466,   122,  -655,
     469,   369,   558,   560,   561,  -655,  -655,  3690,  -655,  -655,
    -655,   122,   263,  -655,    55,   565,  -655,  -655,   522,  -655,
    -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,  -655,  -655,   679,  1304,  -655,  1304,  -655,   588,  1304,
    -655,  -655,  -655,  -655,  -655,  3690,  3690,  3690,  -655,  1555,
    -655,  1555,  -655,  3690,  -655,  -655,   489,   587,  -655,  -655,
    1751,  1751,  -655,   122,   587,   573,  1182,  1182,  1182,  -655,
     122,  -655,   577,   122,   122,   122,   122,   576,   578,  -655,
    2387,  -655,   844,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,  -655,  -655,  2445,   580,  -655,  2199,   686,   581,  -655,
     584,  -655,   686,   585,   582,   589,   590,  2611,  -655,  -655,
     704,  -655,   381,   395,   396,   223,  -655,  -655,   122,  -655,
     399,  -655,   122,  -655,   493,  -655,  1798,  -655,  1798,   522,
    -655,  -655,   608,  -655,   609,   591,  -655,  -655,  -655,   598,
    -655,  -655,  -655,  -655,  -655,  -655,   998,  -655,  1751,  1043,
    1599,  -655,   -64,  1646,  3269,  1124,  1182,  -655,  -655,  -655,
    -655,  -655,  -655,  3286,  3311,  3417,  3434,  -655,  -655,  -655,
    -655,   726,  -655,   334,   122,  -655,   606,  -655,   507,  -655,
     686,   686,   122,  -655,  -655,  -655,  -655,   508,   122,  -655,
     607,  -655,   614,   616,   278,   522,   624,   633,   635,   636,
     639,   626,  -655,  -655,  -655,  1707,  -655,  -655,  -655,  -655,
    -655,  -655,     9,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
     625,    52,   686,  2480,   686,  -655,  -655,   756,   627,   507,
     512,  -655,   122,  2931,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,   270,  -655,  -655,  1182,  1182,   638,  -655,  -655,  -655,
    -655,  -655,   686,   634,   734,  -655,    56,  -655,   507,   122,
     686,   767,  -655,   122,   640,  -655,  -655,   641,  -655,  -655,
    -655,  -655,  1182,   111,   686,    64,   645,   646,   778,  2949,
     507,   122,   518,  -655,  -655,  -655,  -655,  -655,  -655,   795,
     652,   686,   656,  -655,   686,   686,   122,   657,   789,  2967,
    -655,   122,   686,   686,   507,   686,   664,   507,  2985,   647,
     122,   666,   674,   520,   507,   798,   -77,   686,   807,  -655,
    1817,  3091,   659,   122,   686,   808,   122,  1817,   507,   122,
     688,   687,  1817,   689,   -65,   122,  3109,   690,   820,  3127,
    -655,   670,   694,   122,  1817,  3145,   693,  -655,   122,   695,
    1817,  -655,   698,   696,   700,   676,  3251,   680,   709,  -655,
    -655,   681,  1817,   708,  1817,  -655,  1817,   714,   702,   721,
     723,  -655,  1817,  -655,  -655,   727,  -655
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -655,  -655,  -655,  -655,  -655,  -655,  -655,   859,   848,   839,
    -655,   684,  -655,  -655,     2,   735,  -655,  -655,  -655,  -655,
     510,  -655,  -655,  -655,   276,  -655,   279,  -492,  -655,   277,
    -655,  -655,  -655,    76,  -592,  -655,  -655,  -655,  -655,  -655,
    -655,  -655,  -655,  -655,  -552,  -655,  -655,  -655,  -655,  -655,
    -655,  -655,   849,   746,  -655,   -36,   -45,  -655,   -41,   -32,
     -24,  -655,  -655,   533,  -655,   -35,   -39,   -13,   -12,   -11,
    -655,  -655,  -655,  -205,  -117,   562,  -655,  -655,  -252,  -655,
    -655,   654,  -247,   362,   365,   -38,   -92,  -193,  -655,  -655,
    -655,  -655,  -655,   417,  -655,   414,   333,  -655,  -655,  -655,
    -655,  -655,  -655,  -655,  -655,   318,  -655,   205,  -655,  -655,
    -655,   222,   144,  -655,  -655,  -655,  -655,  -655,   264,  -395,
    -106,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,
    -655,   -54,  -655,  -655,  -655,   602,  -613,  -654,  -655,  -655,
    -655,  -655,  -655,  -655,  -655,   515,   402,  -655,  -655,  -655,
    -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -655,  -351,
    -236,  -655,   177,  -518,  -655,  -655,  -655,    75,  -467,  -655,
     176,   -70,   341,  -414,   -31,   -40,   424,   777,     5,  -655,
    -655,  -655,   -19,    87,   691,   136,  -141,  -439,  -655
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -397
static const yytype_int16 yytable[] =
{
      73,   132,   219,   371,   234,   133,   709,   138,   537,   521,
     131,   137,    54,   689,   134,   368,   339,    54,   340,   260,
     260,   208,   135,   344,    54,   368,   462,    25,   733,    43,
     629,   170,   172,   139,   140,   141,   345,   207,    25,   462,
      97,   171,    11,   686,    54,   419,   420,   421,    54,   484,
     485,   486,   557,    54,   588,   589,   590,   416,    54,   223,
     833,    97,   684,   446,   856,   540,   472,   462,   228,   106,
     235,   790,   210,   577,   214,   558,   789,    12,   198,   789,
     473,   199,   202,   449,   200,    13,   631,   558,   264,    80,
     106,   236,    81,   231,    14,   907,   209,   315,   500,    17,
      54,    55,    56,    57,  -378,   630,   685,   924,   857,    23,
     750,   125,   215,   474,   338,   785,   466,   773,    36,  -378,
     258,   258,   712,   782,   714,    54,    55,    56,    57,    58,
     494,   789,   125,   267,   268,   269,   270,   271,   272,   273,
     274,   275,   276,   277,   278,   279,   280,   281,   282,   283,
     284,   285,   286,   287,   288,   289,   290,   460,   292,   208,
     294,   632,   369,   205,   492,   292,   447,   291,   541,    44,
      45,   386,   578,   677,   171,   207,   336,   537,    27,    59,
      60,   216,    61,   782,   210,   370,   682,   493,    26,    27,
     717,   422,   718,   805,   806,   487,   558,   362,   363,   411,
     591,   210,   231,  -121,   231,   706,   678,   132,   209,   231,
     210,   133,   382,   138,   871,   385,   131,   137,   384,   429,
     134,   383,    33,   391,   392,   209,   292,   208,   135,    62,
      63,   560,   408,     2,   209,   393,   740,   182,   387,   139,
     140,   141,   726,   402,   810,   470,   471,    71,   511,   735,
     545,    72,   805,   806,    62,    63,   152,    45,    64,    65,
      66,   208,   155,    67,    68,   156,    35,   184,    69,    70,
      39,   155,    71,   479,   254,   479,    72,   445,   210,   432,
     208,    38,   264,   210,   767,   265,   210,   330,    40,   475,
     331,   807,  -390,   448,    41,   423,   207,     3,   199,     4,
      25,   333,   209,   455,    47,   334,   210,   209,   335,    51,
     209,    49,   210,   476,     5,   192,   193,   194,   195,   196,
     434,   348,   681,   683,   542,   862,   756,   216,   458,   483,
     209,   210,   341,   349,  -129,   342,   209,   462,   382,   436,
     208,   438,   841,   160,   384,   161,   507,   383,   216,   510,
     201,   488,   211,     6,   514,   209,   527,   515,   499,   388,
     389,   346,   440,   892,   347,   522,   523,   524,   692,   693,
     224,   858,    54,   208,   531,   532,   199,   534,   535,   333,
     233,   694,   170,   571,   572,   913,   255,   292,   253,   207,
     350,   210,   262,   878,   210,  -390,   539,   293,   844,   654,
     656,   327,   800,   337,   463,   932,   546,   547,   343,   808,
     550,   210,   846,   431,   464,   209,   199,   895,   209,   553,
     898,   351,   361,   452,   210,   342,   347,   905,   817,   818,
     819,   820,   651,   390,   389,   209,   817,   818,   819,   820,
     352,   918,   453,   396,   264,   347,   758,   170,   209,   364,
     456,   483,   587,   347,   433,    93,   399,   457,   489,   691,
     331,   490,   403,   625,   626,   627,   405,    96,    97,    98,
    -342,  -344,   633,   435,   592,   437,   264,   292,   554,   513,
     406,   331,   173,   157,   158,   159,   636,   407,   162,   163,
     164,   165,   166,   167,   168,   174,   439,   106,   107,   409,
     546,   547,   210,   870,   208,   182,   566,   112,   660,   342,
     567,   183,   673,   342,   410,   593,   676,   801,   594,   210,
     207,   687,   412,   886,   659,   809,   209,   331,   414,   121,
     764,   893,   415,   347,   896,   184,   483,   424,   483,   125,
     737,   738,   739,   209,   765,   766,   210,   347,   331,   768,
     425,   170,   199,   914,   231,   695,   691,   192,   193,   194,
     195,   196,   700,   417,   573,   418,   836,   574,   838,   459,
     209,   194,   195,   196,   370,   499,   426,   427,   468,   209,
     461,   190,   191,   192,   193,   194,   195,   196,   469,   507,
     491,   507,   720,   516,   575,   576,   853,   517,   722,   720,
     292,   292,   721,   734,   860,   722,   649,   650,   518,   721,
     170,   652,   264,   743,   744,   745,   746,   697,   698,   872,
     699,   576,    93,   787,   723,   724,   725,   210,   210,   210,
     794,   723,   724,   725,   519,   884,    98,   431,   432,   887,
     719,   264,   543,   210,   771,   650,   525,   894,   805,   806,
     533,   209,   209,   209,  -132,   544,   596,   559,   170,   811,
     812,   908,   587,   842,   843,   107,   587,   209,   587,   880,
     881,   904,   558,   551,   112,  -132,   653,   655,   433,   434,
     599,   600,   601,   602,   603,   604,   555,   556,   292,   462,
     292,   568,   569,   292,   570,   624,   121,   435,   436,   437,
     438,   628,   634,   642,   643,   675,   125,   210,   688,   701,
     210,   710,   702,   703,   803,   708,   210,   210,   850,   851,
     439,   440,   208,   715,   736,   596,   742,   747,   813,   748,
     753,   209,   760,   558,   209,   757,   759,   763,   826,   761,
     209,   209,   762,   774,   775,   292,   868,   783,   296,   776,
     777,   778,   779,   603,   604,   799,   297,   298,   804,   -79,
     581,   299,   300,   301,   235,   815,   302,   816,   -80,   303,
     -81,   -82,   170,   210,   821,   822,   839,   832,  -273,   840,
      93,   304,   305,   306,   854,   236,   855,   861,   307,   852,
     865,   863,    96,    97,    98,   874,   875,   209,   876,   859,
     237,   238,   239,   882,   883,   210,   210,   885,   889,   890,
     240,   241,   242,   243,   244,   245,   897,   902,   906,   900,
     246,   879,   106,   107,   247,   248,   903,   909,   915,   209,
     209,   912,   112,   210,   308,   309,   888,   920,   921,   927,
     928,   923,   930,   931,   935,   940,   937,   296,   942,   939,
     901,   941,   944,   946,   121,   297,   298,   209,   945,   948,
     299,   300,   301,   951,   125,   302,   916,   310,   303,   919,
     953,   311,   954,  -273,   952,   925,   956,   312,    34,    93,
     304,   305,   306,    48,    82,   295,   252,   307,   936,   501,
     711,    96,    97,    98,   313,   713,   716,   848,   153,   205,
     480,   256,   296,   367,   454,   580,   658,   579,   657,   696,
     297,   298,   707,   772,   814,   299,   300,   301,   314,   257,
     302,   106,   107,   303,   769,   428,   751,   528,   869,   802,
     644,   112,   704,   308,   309,   304,   305,   306,   261,     0,
       0,   376,   307,     0,     0,     0,   296,     0,     0,   529,
       0,     0,     0,   121,   297,   298,     0,     0,     0,   299,
     300,   301,     0,   125,   302,     0,   310,   303,     0,   638,
     311,     0,     0,     0,     0,     0,   312,     0,     0,   304,
     305,   306,     0,     0,     0,     0,   307,     0,   308,   309,
       0,     0,     0,   313,     0,     0,     0,     0,   205,     0,
       0,   296,     0,     0,     0,     0,     0,     0,     0,   297,
     298,     0,     0,     0,   299,   300,   301,   314,   257,   302,
       0,   310,   303,     0,   784,   311,     0,     0,     0,     0,
       0,   312,   308,   309,   304,   305,   306,     0,     0,     0,
       0,   307,     0,     0,     0,     0,   296,     0,   313,     0,
       0,     0,     0,   205,   297,   298,     0,     0,     0,   299,
     300,   301,     0,     0,   302,   310,     0,   303,     0,   311,
       0,     0,   314,   257,     0,   312,     0,   910,     0,   304,
     305,   306,     0,     0,   917,     0,   307,   308,   309,   922,
       0,     0,   313,     0,     0,     0,     0,   205,     0,     0,
       0,   933,     0,     0,     0,     0,     0,   938,     0,     0,
       0,     0,     0,     0,     0,     0,   314,   257,     0,   947,
     310,   949,     0,   950,   311,     0,     0,   296,     0,   955,
     312,     0,   308,   309,     0,   297,   298,     0,     0,     0,
     299,   300,   301,     0,     0,   302,     0,   313,   303,     0,
       0,     0,   205,     0,     0,     0,     0,     0,     0,     0,
     304,   305,   306,     0,     0,   310,     0,   307,     0,   311,
       0,   314,   257,     0,   793,   312,     0,     0,   786,     0,
       0,     0,     0,     0,     0,   296,     0,     0,     0,     0,
       0,     0,   313,   297,   298,     0,     0,   205,   299,   300,
     301,     0,     0,   302,     0,     0,   303,     0,     0,     0,
       0,     0,     0,   308,   309,     0,   314,   257,   304,   305,
     306,     0,     0,     0,     0,   307,     0,     0,    54,    55,
      56,    57,    58,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   310,     0,     0,     0,
     311,     0,     0,     0,     0,     0,   312,     0,    54,    55,
      56,    57,    58,     0,     0,     0,     0,     0,     0,     0,
       0,   308,   309,   313,     0,     0,     0,     0,   205,     0,
       0,     0,    59,    60,     0,    61,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   314,   257,     0,
       0,     0,     0,     0,   310,     0,     0,     0,   311,     0,
       0,   173,    59,    60,   312,    61,     0,     0,     0,     0,
       0,     0,     0,     0,   174,     0,   176,   177,     0,     0,
       0,   313,   180,   181,   182,     0,   205,     0,     0,     0,
     183,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   314,   257,    62,    63,     0,
       0,    64,    65,    66,   184,     0,    67,    68,     0,     0,
       0,    69,    70,     0,     0,    71,     0,     0,     0,    72,
      54,    55,    56,    57,    58,     0,     0,    62,    63,     0,
       0,    64,    65,    66,     0,     0,    67,    68,   314,   257,
       0,    69,    70,     0,     0,    71,     0,     0,     0,    72,
     190,   191,   192,   193,   194,   195,   196,     0,   431,   432,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   257,
       0,     0,     0,     0,    59,    60,     0,    61,   596,     0,
       0,     0,     0,     0,     0,     0,     0,   597,     0,    54,
      55,    56,    57,    58,   598,     0,     0,     0,     0,   433,
     434,     0,   599,   600,   601,   602,   603,   604,   605,   606,
     607,   608,   609,   610,   611,   612,     0,     0,   435,   436,
     437,   438,     0,     0,     0,     0,     0,   431,   432,    54,
      55,    56,    57,    58,     0,     0,     0,     0,     0,     0,
       0,   439,   440,    59,    60,     0,    61,     0,     0,    62,
      63,   503,     0,    64,    65,    66,     0,     0,    67,    68,
       0,     0,     0,    69,    70,     0,     0,    71,   433,   434,
     581,    72,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    59,    60,     0,    61,   435,   436,   437,
     438,     0,     0,     0,     0,     0,   504,     0,    54,    55,
      56,    57,    58,     0,     0,     0,   505,     0,     0,     0,
     439,   440,     0,     0,     0,     0,     0,     0,    62,    63,
     503,     0,    64,    65,    66,     0,     0,    67,    68,     0,
       0,     0,    69,    70,     0,     0,    71,     0,     0,     0,
      72,     0,    54,    55,    56,    57,    58,     0,     0,     0,
       0,     0,    59,    60,     0,    61,     0,     0,    62,    63,
       0,   729,    64,    65,    66,   504,   788,    67,    68,   506,
       0,     0,    69,    70,     0,   505,    71,     0,     0,     0,
      72,     0,     0,     0,     0,     0,     0,     0,     0,    54,
      55,    56,    57,    58,     0,     0,    59,    60,     0,    61,
       0,     0,     0,     0,     0,     0,     0,     0,   729,     0,
       0,     0,     0,   791,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    62,    63,     0,
       0,    64,    65,    66,     0,     0,    67,    68,     0,     0,
       0,    69,    70,    59,    60,    71,    61,     0,     0,    72,
      54,    55,    56,    57,    58,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   729,
       0,    62,    63,     0,   823,    64,    65,    66,     0,     0,
      67,    68,     0,     0,     0,    69,    70,     0,     0,    71,
       0,     0,     0,    72,    54,    55,    56,    57,    58,     0,
       0,     0,     0,     0,    59,    60,     0,    61,     0,     0,
       0,     0,     0,   729,     0,     0,     0,     0,    62,    63,
       0,     0,    64,    65,    66,     0,     0,    67,    68,     0,
       0,     0,    69,    70,     0,     0,    71,     0,     0,     0,
      72,    54,    55,    56,    57,    58,     0,     0,    59,    60,
       0,    61,   173,     0,     0,     0,     0,     0,     0,     0,
      54,    55,    56,    57,    58,   174,   175,   176,   177,     0,
       0,   178,   179,   180,   181,   182,     0,     0,     0,    62,
      63,   183,     0,    64,    65,    66,     0,     0,    67,    68,
       0,     0,     0,    69,    70,    59,    60,    71,    61,     0,
       0,    72,     0,     0,     0,   184,     0,     0,     0,     0,
       0,     0,     0,     0,    59,    60,     0,    61,     0,     0,
       0,     0,     0,    62,    63,     0,     0,    64,    65,    66,
       0,     0,    67,    68,     0,     0,     0,    69,    70,     0,
       0,    71,     0,     0,     0,    72,   185,   186,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   197,
      62,    63,     0,     0,    64,    65,    66,     0,     0,    67,
      68,     0,     0,     0,    69,    70,     0,     0,    71,    62,
      63,   581,    72,    64,    65,    66,     0,     0,    67,    68,
       0,     0,    83,    69,    70,     0,     0,   561,    84,    85,
      86,    72,    87,    88,    89,     0,     0,     0,    90,     0,
       0,     0,    91,     0,     0,     0,     0,    92,     0,     0,
       0,     0,     0,     0,    93,     0,     0,     0,    94,     0,
       0,     0,     0,     0,     0,    95,    96,    97,    98,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    99,     0,     0,
     100,   101,   102,   103,   104,   105,   106,   107,   108,     0,
       0,     0,     0,   109,   110,   111,   112,     0,     0,     0,
     113,   114,     0,   115,   116,   117,     0,     0,   118,     0,
       0,     0,     0,     0,     0,     0,   119,   120,   121,   122,
     123,   124,     0,     0,    83,     0,     0,     0,   125,     0,
      84,    85,    86,     0,    87,    88,    89,     0,   126,   127,
      90,     0,     0,     0,    91,     0,     0,     0,     0,   378,
       0,     0,     0,     0,     0,     0,    93,     0,   173,     0,
      94,     0,     0,     0,     0,     0,     0,    95,    96,    97,
      98,   174,   175,   176,   177,     0,     0,   178,   179,   180,
     181,   182,     0,     0,     0,     0,     0,   183,     0,    99,
       0,     0,   100,   101,   102,   103,   104,   105,   106,   107,
     108,     0,     0,     0,     0,   109,   110,   111,   112,     0,
       0,   184,   113,   114,     0,   115,   116,   117,     0,     0,
     118,     0,     0,     0,     0,     0,     0,     0,   119,   120,
     121,   122,   123,   124,     0,     0,     0,     0,     0,     0,
     125,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     126,   127,   185,   397,   187,   188,   189,   190,   191,   192,
     193,   194,   195,   196,   173,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   398,     0,   174,   175,   176,
     177,     0,     0,   178,   179,   180,   181,   182,     0,   173,
       0,     0,     0,   183,     0,     0,     0,     0,     0,     0,
       0,     0,   174,   175,   176,   177,     0,     0,   178,   179,
     180,   181,   182,     0,     0,     0,     0,   184,   183,     0,
       0,     0,     0,     0,   173,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   174,   175,   176,
     177,     0,   184,   178,   179,   180,   181,   182,     0,     0,
       0,     0,     0,   183,     0,     0,     0,     0,   185,   548,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
       0,     0,     0,     0,     0,     0,     0,   184,     0,     0,
       0,   549,     0,   185,     0,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,     0,     0,     0,   754,     0,
       0,     0,     0,     0,     0,     0,   755,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   173,   185,     0,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
     174,   175,   176,   177,     0,     0,   178,   179,   180,   181,
     182,   395,   173,     0,     0,     0,   183,     0,     0,     0,
       0,     0,     0,     0,     0,   174,   175,   176,   177,     0,
       0,   178,   179,   180,   181,   182,     0,     0,     0,     0,
     184,   183,     0,     0,     0,     0,     0,   173,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     174,   175,   176,   177,     0,   184,   178,   179,   180,   181,
     182,     0,     0,     0,     0,     0,   183,     0,     0,     0,
       0,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,     0,     0,     0,     0,     0,     0,     0,
     184,     0,     0,     0,   635,   173,   185,     0,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,   174,   175,
     176,   177,     0,     0,   178,   179,   180,   181,   182,   661,
       0,     0,     0,     0,   183,     0,     0,     0,     0,     0,
     173,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   174,   175,   176,   177,     0,   184,   178,
     179,   180,   181,   182,   749,   173,     0,     0,     0,   183,
       0,     0,     0,     0,     0,     0,     0,     0,   174,   175,
     176,   177,     0,     0,   178,   179,   180,   181,   182,     0,
       0,     0,     0,   184,   183,     0,     0,     0,     0,   185,
       0,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,     0,     0,     0,     0,     0,     0,     0,   184,     0,
       0,     0,   752,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   185,     0,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   837,     0,   185,
       0,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   173,     0,     0,     0,     0,     0,     0,     0,   266,
       0,     0,     0,     0,   174,   175,   176,   177,     0,   173,
     178,   179,   180,   181,   182,     0,     0,     0,     0,     0,
     183,     0,   174,   175,   176,   177,     0,   173,   178,   179,
     180,   181,   182,     0,     0,     0,     0,     0,   183,     0,
     174,   175,   176,   177,   184,   173,   178,   179,   180,   181,
     182,     0,     0,     0,     0,     0,   183,     0,   174,   175,
     176,   177,   184,     0,   178,   179,   180,   181,   182,     0,
       0,     0,     0,     0,   183,     0,     0,     0,     0,     0,
     184,     0,     0,     0,     0,   185,   263,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,     0,   184,     0,
       0,     0,  -396,   185,     0,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,     0,     0,     0,     0,     0,
     639,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,     0,     0,     0,     0,     0,   640,   185,
       0,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   173,     0,     0,     0,     0,   641,     0,     0,     0,
       0,     0,     0,     0,   174,   175,   176,   177,     0,   173,
     178,   179,   180,   181,   182,     0,     0,     0,     0,     0,
     183,     0,   174,   175,   176,   177,     0,   173,   178,   179,
     180,   181,   182,     0,     0,     0,     0,     0,   183,     0,
     174,   175,   176,   177,   184,   173,   178,   179,   180,   181,
     182,     0,     0,     0,     0,     0,   183,     0,   174,   175,
     176,   177,   184,     0,   178,   179,   180,   181,   182,     0,
       0,     0,     0,     0,   183,     0,     0,     0,     0,     0,
     184,     0,     0,     0,     0,   185,     0,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,     0,   184,     0,
       0,     0,   646,   185,     0,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,     0,     0,     0,     0,     0,
     647,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,     0,     0,     0,     0,     0,   648,   185,
       0,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   173,     0,     0,     0,     0,   674,     0,     0,     0,
       0,     0,     0,     0,   174,   175,   176,   177,     0,   173,
     178,   179,   180,   181,   182,     0,     0,     0,     0,     0,
     183,     0,   174,   175,   176,   177,     0,   173,   178,   179,
     180,   181,   182,     0,     0,     0,     0,     0,   183,     0,
     174,   175,   176,   177,   184,   173,   178,   179,   180,   181,
     182,     0,     0,     0,     0,     0,   183,     0,   174,   175,
     176,   177,   184,     0,   178,   179,   180,   181,   182,     0,
       0,     0,     0,     0,   183,     0,     0,     0,     0,     0,
     184,     0,     0,     0,     0,   185,     0,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,     0,   184,     0,
       0,     0,   845,   185,     0,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,     0,     0,     0,     0,     0,
     877,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,     0,     0,     0,     0,     0,   891,   185,
       0,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   173,     0,     0,     0,     0,   899,     0,     0,     0,
       0,     0,     0,     0,   174,   175,   176,   177,     0,   173,
     178,   179,   180,   181,   182,     0,     0,     0,     0,     0,
     183,     0,   174,   175,   176,   177,     0,   173,   178,   179,
     180,   181,   182,     0,     0,     0,     0,     0,   183,     0,
     174,   175,   176,   177,   184,   173,   178,   179,   180,   181,
     182,     0,     0,     0,     0,     0,   183,     0,   174,   175,
     176,   177,   184,     0,   178,   179,   180,   181,   182,     0,
       0,     0,     0,     0,   183,     0,     0,     0,     0,     0,
     184,     0,     0,     0,     0,   185,     0,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,     0,   184,     0,
       0,     0,   911,   185,     0,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,     0,     0,     0,     0,     0,
     926,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,     0,     0,     0,     0,     0,   929,   185,
       0,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,   173,     0,     0,     0,     0,   934,     0,     0,     0,
       0,     0,     0,     0,   174,   175,   176,   177,     0,   173,
     178,   179,   180,   181,   182,     0,     0,     0,     0,     0,
     183,     0,   174,   175,   176,   177,   173,     0,   178,   179,
     180,   181,   182,     0,     0,     0,     0,     0,   183,   174,
     175,   176,   177,     0,   184,   178,   179,   180,   181,   182,
       0,   173,     0,     0,     0,   183,     0,     0,     0,     0,
       0,     0,   184,     0,   174,   175,   176,   177,     0,     0,
     178,   179,   180,   181,   182,     0,     0,     0,     0,   184,
     183,     0,     0,     0,     0,   185,     0,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,     0,     0,     0,
       0,     0,   943,   185,   184,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,     0,     0,     0,   792,     0,
     185,     0,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,     0,     0,     0,   795,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   185,     0,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   173,     0,     0,
     796,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     174,   175,   176,   177,   173,     0,   178,   179,   180,   181,
     182,     0,     0,     0,     0,     0,   183,   174,   175,   176,
     177,     0,   173,   178,   179,   180,   181,   182,     0,     0,
       0,     0,     0,   183,     0,   174,   175,   176,   177,     0,
     184,   178,   179,   180,   181,   182,     0,     0,     0,     0,
       0,   183,     0,     0,     0,     0,     0,   184,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   184,     0,     0,     0,     0,
       0,   185,     0,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,     0,     0,     0,   797,     0,   185,     0,
     187,   188,   189,   190,   191,   192,   193,   194,   195,   196,
       0,     0,     0,   798,     0,   173,   185,   263,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,   174,   175,
     176,   177,   173,     0,   178,   179,   180,   181,   182,     0,
       0,     0,     0,     0,   183,   174,   175,   176,   177,     0,
     173,   178,   179,   180,   181,   182,     0,     0,     0,     0,
       0,   183,     0,   174,   175,   176,   177,     0,   184,   178,
     179,   180,   181,   182,     0,     0,     0,     0,     0,   183,
       0,     0,     0,     0,     0,   184,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   184,     0,     0,     0,     0,     0,   185,
     394,   187,   188,   189,   190,   191,   192,   193,   194,   195,
     196,     0,     0,     0,     0,     0,   185,   450,   187,   188,
     189,   190,   191,   192,   193,   194,   195,   196,     0,     0,
       0,     0,     0,   173,   185,   512,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,   174,   175,   176,   177,
     173,     0,   178,   179,   180,   181,   182,     0,     0,     0,
       0,   173,   183,   174,   175,   176,   177,     0,     0,   178,
     179,   180,   181,   182,   174,   175,   176,   177,     0,   183,
       0,   179,   180,   181,   182,     0,   184,     0,     0,   173,
     183,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   174,   184,   176,   177,     0,     0,     0,   179,
     180,   181,   182,     0,   184,     0,     0,     0,   183,     0,
       0,     0,     0,     0,     0,     0,     0,   185,   552,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,     0,
       0,     0,   184,     0,   185,     0,   187,   188,   189,   190,
     191,   192,   193,   194,   195,   196,     0,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   173,     0,     0,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   174,     0,   176,   177,   173,
       0,     0,   179,   180,   181,   182,     0,     0,     0,     0,
       0,   183,   174,     0,   176,   177,     0,     0,     0,     0,
     180,   181,   182,     0,     0,     0,     0,     0,   183,     0,
       0,     0,     0,     0,     0,   184,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   184,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   188,
     189,   190,   191,   192,   193,   194,   195,   196,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   189,   190,   191,
     192,   193,   194,   195,   196
};

static const yytype_int16 yycheck[] =
{
      40,    46,    94,   250,   121,    46,   598,    46,   422,   404,
      46,    46,     3,   565,    46,     3,   221,     3,   223,   160,
     161,    91,    46,   228,     3,     3,     3,     3,   641,    27,
      76,    71,    72,    46,    46,    46,   229,    91,     3,     3,
      48,    72,     3,   561,     3,     4,     5,     6,     3,     4,
       5,     6,     8,     3,     4,     5,     6,   309,     3,    97,
       8,    48,     8,    69,     8,    69,    51,     3,   106,    77,
      67,   135,    91,   487,    49,   152,   730,     3,   150,   733,
      65,   153,    80,   335,   156,     3,    76,   152,   152,   152,
      77,    88,   155,   112,    34,   172,    91,   203,   106,   150,
       3,     4,     5,     6,    52,   151,    52,   172,    52,     3,
     662,   119,    87,    98,   220,   728,   352,   709,     3,     8,
     160,   161,   614,   715,   616,     3,     4,     5,     6,     7,
     377,   785,   119,   173,   174,   175,   176,   177,   178,   179,
     180,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,   193,   194,   195,   196,   350,   198,   229,
     200,   151,   150,   154,   150,   205,   172,   198,   172,   151,
     152,   150,   150,   150,   205,   229,   216,   591,   154,    57,
      58,   156,    60,   775,   203,   173,   150,   173,   153,   154,
     629,   150,   631,   141,   142,   150,   152,   235,   236,   305,
     150,   220,   221,   154,   223,   150,   557,   252,   203,   228,
     229,   252,   253,   252,   150,   255,   252,   252,   253,   325,
     252,   253,   149,   263,   264,   220,   266,   297,   252,   132,
     133,   467,   302,     0,   229,   266,   650,    63,   257,   252,
     252,   252,   637,   297,   762,   362,   363,   150,   389,   644,
     443,   154,   141,   142,   132,   133,   151,   152,   136,   137,
     138,   331,   152,   141,   142,   155,   150,    93,   146,   147,
     149,   152,   150,   365,   155,   367,   154,   331,   297,    42,
     350,   154,   152,   302,   698,   155,   305,   149,   156,    97,
     152,   758,    69,   333,     3,   314,   350,    64,   153,    66,
       3,   156,   297,   343,   149,   149,   325,   302,   152,     3,
     305,   154,   331,   121,    81,   141,   142,   143,   144,   145,
      83,    31,   558,   559,   430,   843,   677,   156,   347,   369,
     325,   350,   149,    43,   172,   152,   331,     3,   379,   102,
     410,   104,   809,   150,   379,   150,   386,   379,   156,   389,
     150,   370,     3,   120,   394,   350,   410,   397,   377,   151,
     152,   149,   125,   881,   152,   405,   406,   407,   573,   574,
       3,   838,     3,   443,   414,   415,   153,   417,   418,   156,
       3,   574,   422,   475,   476,   903,   172,   427,   149,   443,
     100,   410,   151,   860,   413,   172,   427,     3,   812,   540,
     541,   150,   753,     3,    70,   923,   446,   447,   156,   760,
     450,   430,   142,    41,    80,   410,   153,   884,   413,   459,
     887,   131,   149,   149,   443,   152,   152,   894,   158,   159,
     160,   161,   538,   151,   152,   430,   158,   159,   160,   161,
     150,   908,   149,   151,   152,   152,   682,   487,   443,   150,
     149,   491,   492,   152,    82,    35,   151,   149,   149,   565,
     152,   152,   135,   503,   504,   505,   150,    47,    48,    49,
     149,   150,   512,   101,   493,   103,   152,   517,   149,   155,
     150,   152,    40,    59,    60,    61,   517,   150,    64,    65,
      66,    67,    68,    69,    70,    53,   124,    77,    78,     3,
     540,   541,   521,   854,   574,    63,   149,    87,   548,   152,
     149,    69,   552,   152,   150,   149,   556,   753,   152,   538,
     574,   561,   135,   874,   149,   761,   521,   152,   150,   109,
     149,   882,   150,   152,   885,    93,   576,   149,   578,   119,
     646,   647,   648,   538,   149,   149,   565,   152,   152,   150,
      44,   591,   153,   904,   573,   574,   662,   141,   142,   143,
     144,   145,   581,   150,   477,   150,   802,   480,   804,   150,
     565,   143,   144,   145,   173,   594,   149,   150,   172,   574,
     150,   139,   140,   141,   142,   143,   144,   145,   149,   629,
     150,   631,   637,   149,   151,   152,   832,   150,   637,   644,
     640,   641,   637,   643,   840,   644,   151,   152,   149,   644,
     650,   151,   152,   653,   654,   655,   656,   151,   152,   855,
     151,   152,    35,   729,   637,   637,   637,   646,   647,   648,
     736,   644,   644,   644,     3,   871,    49,    41,    42,   875,
     151,   152,   152,   662,   151,   152,   149,   883,   141,   142,
     149,   646,   647,   648,    67,   152,   134,    52,   698,   151,
     152,   897,   702,   151,   152,    78,   706,   662,   708,   151,
     152,   151,   152,   149,    87,    88,   540,   541,    82,    83,
     158,   159,   160,   161,   162,   163,   151,   156,   728,     3,
     730,   151,   151,   733,   151,    30,   109,   101,   102,   103,
     104,   151,   155,   149,   149,   149,   119,   726,   149,   151,
     729,    32,   152,   152,   754,   150,   735,   736,   824,   825,
     124,   125,   792,   135,   151,   134,   149,   151,   768,   151,
     150,   726,   150,   152,   729,   151,   151,    33,   792,   150,
     735,   736,   152,   135,   135,   785,   852,   149,     3,   158,
     159,   160,   161,   162,   163,    29,    11,    12,   152,   135,
     153,    16,    17,    18,    67,   151,    21,   151,   135,    24,
     135,   135,   812,   792,   135,   149,    20,   152,    33,   152,
      35,    36,    37,    38,   150,    88,    52,    20,    43,   151,
     149,   151,    47,    48,    49,   150,   150,   792,    20,   839,
     103,   104,   105,     8,   152,   824,   825,   151,   151,    20,
     113,   114,   115,   116,   117,   118,   152,   151,    20,   172,
     123,   861,    77,    78,   127,   128,   152,    20,    20,   824,
     825,   172,    87,   852,    89,    90,   876,   149,   151,   149,
      20,   152,   172,   149,   151,   149,   151,     3,   172,   151,
     890,   151,   172,   172,   109,    11,    12,   852,   149,   151,
      16,    17,    18,   149,   119,    21,   906,   122,    24,   909,
     149,   126,   149,    29,   172,   915,   149,   132,    19,    35,
      36,    37,    38,    35,    45,   201,   151,    43,   928,   379,
     614,    47,    48,    49,   149,   616,   619,   821,    49,   154,
     367,   155,     3,   249,   342,   491,   544,   490,   543,   576,
      11,    12,   594,   708,   770,    16,    17,    18,   173,   174,
      21,    77,    78,    24,   702,   323,   662,   412,   853,   753,
     528,    87,   591,    89,    90,    36,    37,    38,   161,    -1,
      -1,   250,    43,    -1,    -1,    -1,     3,    -1,    -1,    50,
      -1,    -1,    -1,   109,    11,    12,    -1,    -1,    -1,    16,
      17,    18,    -1,   119,    21,    -1,   122,    24,    -1,    26,
     126,    -1,    -1,    -1,    -1,    -1,   132,    -1,    -1,    36,
      37,    38,    -1,    -1,    -1,    -1,    43,    -1,    89,    90,
      -1,    -1,    -1,   149,    -1,    -1,    -1,    -1,   154,    -1,
      -1,     3,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    11,
      12,    -1,    -1,    -1,    16,    17,    18,   173,   174,    21,
      -1,   122,    24,    -1,    26,   126,    -1,    -1,    -1,    -1,
      -1,   132,    89,    90,    36,    37,    38,    -1,    -1,    -1,
      -1,    43,    -1,    -1,    -1,    -1,     3,    -1,   149,    -1,
      -1,    -1,    -1,   154,    11,    12,    -1,    -1,    -1,    16,
      17,    18,    -1,    -1,    21,   122,    -1,    24,    -1,   126,
      -1,    -1,   173,   174,    -1,   132,    -1,   900,    -1,    36,
      37,    38,    -1,    -1,   907,    -1,    43,    89,    90,   912,
      -1,    -1,   149,    -1,    -1,    -1,    -1,   154,    -1,    -1,
      -1,   924,    -1,    -1,    -1,    -1,    -1,   930,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   173,   174,    -1,   942,
     122,   944,    -1,   946,   126,    -1,    -1,     3,    -1,   952,
     132,    -1,    89,    90,    -1,    11,    12,    -1,    -1,    -1,
      16,    17,    18,    -1,    -1,    21,    -1,   149,    24,    -1,
      -1,    -1,   154,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      36,    37,    38,    -1,    -1,   122,    -1,    43,    -1,   126,
      -1,   173,   174,    -1,    50,   132,    -1,    -1,   135,    -1,
      -1,    -1,    -1,    -1,    -1,     3,    -1,    -1,    -1,    -1,
      -1,    -1,   149,    11,    12,    -1,    -1,   154,    16,    17,
      18,    -1,    -1,    21,    -1,    -1,    24,    -1,    -1,    -1,
      -1,    -1,    -1,    89,    90,    -1,   173,   174,    36,    37,
      38,    -1,    -1,    -1,    -1,    43,    -1,    -1,     3,     4,
       5,     6,     7,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   122,    -1,    -1,    -1,
     126,    -1,    -1,    -1,    -1,    -1,   132,    -1,     3,     4,
       5,     6,     7,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    89,    90,   149,    -1,    -1,    -1,    -1,   154,    -1,
      -1,    -1,    57,    58,    -1,    60,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   173,   174,    -1,
      -1,    -1,    -1,    -1,   122,    -1,    -1,    -1,   126,    -1,
      -1,    40,    57,    58,   132,    60,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    53,    -1,    55,    56,    -1,    -1,
      -1,   149,    61,    62,    63,    -1,   154,    -1,    -1,    -1,
      69,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   173,   174,   132,   133,    -1,
      -1,   136,   137,   138,    93,    -1,   141,   142,    -1,    -1,
      -1,   146,   147,    -1,    -1,   150,    -1,    -1,    -1,   154,
       3,     4,     5,     6,     7,    -1,    -1,   132,   133,    -1,
      -1,   136,   137,   138,    -1,    -1,   141,   142,   173,   174,
      -1,   146,   147,    -1,    -1,   150,    -1,    -1,    -1,   154,
     139,   140,   141,   142,   143,   144,   145,    -1,    41,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,
      -1,    -1,    -1,    -1,    57,    58,    -1,    60,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   143,    -1,     3,
       4,     5,     6,     7,   150,    -1,    -1,    -1,    -1,    82,
      83,    -1,   158,   159,   160,   161,   162,   163,   164,   165,
     166,   167,   168,   169,   170,   171,    -1,    -1,   101,   102,
     103,   104,    -1,    -1,    -1,    -1,    -1,    41,    42,     3,
       4,     5,     6,     7,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   124,   125,    57,    58,    -1,    60,    -1,    -1,   132,
     133,    25,    -1,   136,   137,   138,    -1,    -1,   141,   142,
      -1,    -1,    -1,   146,   147,    -1,    -1,   150,    82,    83,
     153,   154,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    57,    58,    -1,    60,   101,   102,   103,
     104,    -1,    -1,    -1,    -1,    -1,    70,    -1,     3,     4,
       5,     6,     7,    -1,    -1,    -1,    80,    -1,    -1,    -1,
     124,   125,    -1,    -1,    -1,    -1,    -1,    -1,   132,   133,
      25,    -1,   136,   137,   138,    -1,    -1,   141,   142,    -1,
      -1,    -1,   146,   147,    -1,    -1,   150,    -1,    -1,    -1,
     154,    -1,     3,     4,     5,     6,     7,    -1,    -1,    -1,
      -1,    -1,    57,    58,    -1,    60,    -1,    -1,   132,   133,
      -1,    22,   136,   137,   138,    70,    27,   141,   142,   143,
      -1,    -1,   146,   147,    -1,    80,   150,    -1,    -1,    -1,
     154,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,
       4,     5,     6,     7,    -1,    -1,    57,    58,    -1,    60,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,    -1,
      -1,    -1,    -1,    27,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   132,   133,    -1,
      -1,   136,   137,   138,    -1,    -1,   141,   142,    -1,    -1,
      -1,   146,   147,    57,    58,   150,    60,    -1,    -1,   154,
       3,     4,     5,     6,     7,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    22,
      -1,   132,   133,    -1,    27,   136,   137,   138,    -1,    -1,
     141,   142,    -1,    -1,    -1,   146,   147,    -1,    -1,   150,
      -1,    -1,    -1,   154,     3,     4,     5,     6,     7,    -1,
      -1,    -1,    -1,    -1,    57,    58,    -1,    60,    -1,    -1,
      -1,    -1,    -1,    22,    -1,    -1,    -1,    -1,   132,   133,
      -1,    -1,   136,   137,   138,    -1,    -1,   141,   142,    -1,
      -1,    -1,   146,   147,    -1,    -1,   150,    -1,    -1,    -1,
     154,     3,     4,     5,     6,     7,    -1,    -1,    57,    58,
      -1,    60,    40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,     4,     5,     6,     7,    53,    54,    55,    56,    -1,
      -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,   132,
     133,    69,    -1,   136,   137,   138,    -1,    -1,   141,   142,
      -1,    -1,    -1,   146,   147,    57,    58,   150,    60,    -1,
      -1,   154,    -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    57,    58,    -1,    60,    -1,    -1,
      -1,    -1,    -1,   132,   133,    -1,    -1,   136,   137,   138,
      -1,    -1,   141,   142,    -1,    -1,    -1,   146,   147,    -1,
      -1,   150,    -1,    -1,    -1,   154,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   157,
     132,   133,    -1,    -1,   136,   137,   138,    -1,    -1,   141,
     142,    -1,    -1,    -1,   146,   147,    -1,    -1,   150,   132,
     133,   153,   154,   136,   137,   138,    -1,    -1,   141,   142,
      -1,    -1,     3,   146,   147,    -1,    -1,   150,     9,    10,
      11,   154,    13,    14,    15,    -1,    -1,    -1,    19,    -1,
      -1,    -1,    23,    -1,    -1,    -1,    -1,    28,    -1,    -1,
      -1,    -1,    -1,    -1,    35,    -1,    -1,    -1,    39,    -1,
      -1,    -1,    -1,    -1,    -1,    46,    47,    48,    49,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    -1,
      -1,    -1,    -1,    84,    85,    86,    87,    -1,    -1,    -1,
      91,    92,    -1,    94,    95,    96,    -1,    -1,    99,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   107,   108,   109,   110,
     111,   112,    -1,    -1,     3,    -1,    -1,    -1,   119,    -1,
       9,    10,    11,    -1,    13,    14,    15,    -1,   129,   130,
      19,    -1,    -1,    -1,    23,    -1,    -1,    -1,    -1,    28,
      -1,    -1,    -1,    -1,    -1,    -1,    35,    -1,    40,    -1,
      39,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    48,
      49,    53,    54,    55,    56,    -1,    -1,    59,    60,    61,
      62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    68,
      -1,    -1,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    -1,    -1,    -1,    -1,    84,    85,    86,    87,    -1,
      -1,    93,    91,    92,    -1,    94,    95,    96,    -1,    -1,
      99,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   107,   108,
     109,   110,   111,   112,    -1,    -1,    -1,    -1,    -1,    -1,
     119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     129,   130,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,    40,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   157,    -1,    53,    54,    55,
      56,    -1,    -1,    59,    60,    61,    62,    63,    -1,    40,
      -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    53,    54,    55,    56,    -1,    -1,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    93,    69,    -1,
      -1,    -1,    -1,    -1,    40,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    53,    54,    55,
      56,    -1,    93,    59,    60,    61,    62,    63,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,   134,   135,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,
      -1,   157,    -1,   134,    -1,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,   149,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   157,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    40,   134,    -1,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
      53,    54,    55,    56,    -1,    -1,    59,    60,    61,    62,
      63,   157,    40,    -1,    -1,    -1,    69,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    53,    54,    55,    56,    -1,
      -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,
      93,    69,    -1,    -1,    -1,    -1,    -1,    40,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      53,    54,    55,    56,    -1,    93,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,
      -1,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,   157,    40,   134,    -1,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    53,    54,
      55,    56,    -1,    -1,    59,    60,    61,    62,    63,   157,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      40,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    53,    54,    55,    56,    -1,    93,    59,
      60,    61,    62,    63,   157,    40,    -1,    -1,    -1,    69,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    53,    54,
      55,    56,    -1,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    93,    69,    -1,    -1,    -1,    -1,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,
      -1,    -1,   157,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   157,    -1,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    40,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   154,
      -1,    -1,    -1,    -1,    53,    54,    55,    56,    -1,    40,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    53,    54,    55,    56,    -1,    40,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      53,    54,    55,    56,    93,    40,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    53,    54,
      55,    56,    93,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,    -1,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    93,    -1,
      -1,    -1,   151,   134,    -1,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,
     151,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,   151,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    40,    -1,    -1,    -1,    -1,   151,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    53,    54,    55,    56,    -1,    40,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    53,    54,    55,    56,    -1,    40,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      53,    54,    55,    56,    93,    40,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    53,    54,
      55,    56,    93,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    93,    -1,
      -1,    -1,   151,   134,    -1,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,
     151,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,   151,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    40,    -1,    -1,    -1,    -1,   151,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    53,    54,    55,    56,    -1,    40,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    53,    54,    55,    56,    -1,    40,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      53,    54,    55,    56,    93,    40,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    53,    54,
      55,    56,    93,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    93,    -1,
      -1,    -1,   151,   134,    -1,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,
     151,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,   151,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    40,    -1,    -1,    -1,    -1,   151,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    53,    54,    55,    56,    -1,    40,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    53,    54,    55,    56,    -1,    40,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      53,    54,    55,    56,    93,    40,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    -1,    53,    54,
      55,    56,    93,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    93,    -1,
      -1,    -1,   151,   134,    -1,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,    -1,    -1,
     151,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,    -1,    -1,   151,   134,
      -1,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    40,    -1,    -1,    -1,    -1,   151,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    53,    54,    55,    56,    -1,    40,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,
      69,    -1,    53,    54,    55,    56,    40,    -1,    59,    60,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    53,
      54,    55,    56,    -1,    93,    59,    60,    61,    62,    63,
      -1,    40,    -1,    -1,    -1,    69,    -1,    -1,    -1,    -1,
      -1,    -1,    93,    -1,    53,    54,    55,    56,    -1,    -1,
      59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,    93,
      69,    -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    -1,    -1,
      -1,    -1,   151,   134,    93,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    -1,    -1,    -1,   149,    -1,
     134,    -1,   136,   137,   138,   139,   140,   141,   142,   143,
     144,   145,    -1,    -1,    -1,   149,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   134,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    40,    -1,    -1,
     149,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      53,    54,    55,    56,    40,    -1,    59,    60,    61,    62,
      63,    -1,    -1,    -1,    -1,    -1,    69,    53,    54,    55,
      56,    -1,    40,    59,    60,    61,    62,    63,    -1,    -1,
      -1,    -1,    -1,    69,    -1,    53,    54,    55,    56,    -1,
      93,    59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,
      -1,   134,    -1,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,    -1,    -1,    -1,   149,    -1,   134,    -1,
     136,   137,   138,   139,   140,   141,   142,   143,   144,   145,
      -1,    -1,    -1,   149,    -1,    40,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    53,    54,
      55,    56,    40,    -1,    59,    60,    61,    62,    63,    -1,
      -1,    -1,    -1,    -1,    69,    53,    54,    55,    56,    -1,
      40,    59,    60,    61,    62,    63,    -1,    -1,    -1,    -1,
      -1,    69,    -1,    53,    54,    55,    56,    -1,    93,    59,
      60,    61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,
      -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,    -1,   134,
     135,   136,   137,   138,   139,   140,   141,   142,   143,   144,
     145,    -1,    -1,    -1,    -1,    -1,   134,   135,   136,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    -1,    -1,
      -1,    -1,    -1,    40,   134,   135,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,    53,    54,    55,    56,
      40,    -1,    59,    60,    61,    62,    63,    -1,    -1,    -1,
      -1,    40,    69,    53,    54,    55,    56,    -1,    -1,    59,
      60,    61,    62,    63,    53,    54,    55,    56,    -1,    69,
      -1,    60,    61,    62,    63,    -1,    93,    -1,    -1,    40,
      69,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    53,    93,    55,    56,    -1,    -1,    -1,    60,
      61,    62,    63,    -1,    93,    -1,    -1,    -1,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   134,   135,   136,
     137,   138,   139,   140,   141,   142,   143,   144,   145,    -1,
      -1,    -1,    93,    -1,   134,    -1,   136,   137,   138,   139,
     140,   141,   142,   143,   144,   145,    -1,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    40,    -1,    -1,   136,   137,   138,   139,   140,
     141,   142,   143,   144,   145,    53,    -1,    55,    56,    40,
      -1,    -1,    60,    61,    62,    63,    -1,    -1,    -1,    -1,
      -1,    69,    53,    -1,    55,    56,    -1,    -1,    -1,    -1,
      61,    62,    63,    -1,    -1,    -1,    -1,    -1,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    93,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   137,
     138,   139,   140,   141,   142,   143,   144,   145,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   138,   139,   140,
     141,   142,   143,   144,   145
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint16 yystos[] =
{
       0,   176,     0,    64,    66,    81,   120,   177,   178,   179,
     192,     3,     3,     3,    34,   221,   222,   150,   182,   180,
     193,   223,   224,     3,   220,     3,   153,   154,   183,   184,
     186,   187,   189,   149,   182,   150,     3,   225,   154,   149,
     156,     3,   188,   189,   151,   152,   190,   149,   183,   154,
     226,     3,   227,   228,     3,     4,     5,     6,     7,    57,
      58,    60,   132,   133,   136,   137,   138,   141,   142,   146,
     147,   150,   154,   350,   351,   353,   354,   355,   357,   185,
     152,   155,   184,     3,     9,    10,    11,    13,    14,    15,
      19,    23,    28,    35,    39,    46,    47,    48,    49,    68,
      71,    72,    73,    74,    75,    76,    77,    78,    79,    84,
      85,    86,    87,    91,    92,    94,    95,    96,    99,   107,
     108,   109,   110,   111,   112,   119,   129,   130,   191,   211,
     213,   230,   231,   233,   234,   235,   236,   240,   241,   242,
     243,   244,   245,   247,   263,   266,   272,   275,   289,   291,
     328,   181,   151,   227,   229,   152,   155,   351,   351,   351,
     150,   150,   351,   351,   351,   351,   351,   351,   351,   348,
     350,   349,   350,    40,    53,    54,    55,    56,    59,    60,
      61,    62,    63,    69,    93,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   157,   150,   153,
     156,   150,   189,   292,   246,   154,   262,   306,   346,   353,
     357,     3,   252,   253,    49,    87,   156,   215,   216,   261,
     290,   260,   261,   260,     3,   249,   250,   251,   260,   232,
     248,   357,   329,     3,   249,    67,    88,   103,   104,   105,
     113,   114,   115,   116,   117,   118,   123,   127,   128,   237,
     264,   273,   190,   149,   155,   172,   228,   174,   350,   352,
     361,   352,   151,   135,   152,   155,   154,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   350,   350,   350,   350,   350,   350,   350,   350,   350,
     350,   349,   350,     3,   350,   186,     3,    11,    12,    16,
      17,    18,    21,    24,    36,    37,    38,    43,    89,    90,
     122,   126,   132,   149,   173,   295,   306,   307,   317,   319,
     323,   326,   327,   346,   356,   360,   361,   150,   256,   257,
     149,   152,   310,   156,   149,   152,   350,     3,   295,   248,
     248,   149,   152,   156,   248,   262,   149,   152,    31,    43,
     100,   131,   150,   330,   331,   332,   333,   339,   340,   344,
     212,   149,   260,   260,   150,   254,   255,   256,     3,   150,
     173,   257,   265,   267,   268,   269,   359,   276,    28,   194,
     195,   230,   233,   234,   240,   350,   150,   357,   151,   152,
     151,   350,   350,   349,   135,   157,   151,   135,   157,   151,
     324,   325,   306,   135,   318,   150,   150,   150,   346,     3,
     150,   295,   135,   294,   150,   150,   253,   150,   150,     4,
       5,     6,   150,   357,   149,    44,   149,   150,   310,   295,
     300,    41,    42,    82,    83,   101,   102,   103,   104,   124,
     125,   258,   259,   358,   359,   306,    69,   172,   350,   253,
     135,   214,   149,   149,   250,   350,   149,   149,   357,   150,
     262,   150,     3,    70,    80,   334,   335,   345,   172,   149,
     249,   249,    51,    65,    98,    97,   121,   238,   239,   261,
     238,   270,   271,   350,     4,     5,     6,   150,   357,   149,
     152,   150,   150,   173,   257,   277,   278,   279,   280,   357,
     106,   195,   196,    25,    70,    80,   143,   350,   362,   363,
     350,   361,   135,   155,   350,   350,   149,   150,   149,     3,
     320,   294,   350,   350,   350,   149,   305,   306,   320,    50,
     295,   350,   350,   149,   350,   350,   347,   348,   296,   349,
      69,   172,   295,   152,   152,   262,   350,   350,   135,   157,
     350,   149,   135,   350,   149,   151,   156,     8,   152,    52,
     335,   150,   337,   338,   350,   217,   149,   149,   151,   151,
     151,   261,   261,   358,   358,   151,   152,   348,   150,   268,
     270,   153,   282,   283,   284,   286,   287,   350,     4,     5,
       6,   150,   357,   149,   152,   281,   134,   143,   150,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,   171,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   209,   210,    30,   350,   350,   350,   151,    76,
     151,    76,   151,   350,   155,   157,   349,   321,    26,   151,
     151,   151,   149,   149,   321,   308,   151,   151,   151,   151,
     152,   295,   151,   360,   361,   360,   361,   259,   258,   149,
     350,   157,   218,   219,   231,   233,   234,   235,   240,   241,
     242,   243,   244,   350,   151,   149,   350,   150,   334,   335,
     336,   335,   150,   335,     8,    52,   338,   350,   149,   219,
     293,   295,   248,   248,   262,   357,   271,   151,   152,   151,
     357,   151,   152,   152,   347,   274,   150,   280,   150,   209,
      32,   199,   202,   201,   202,   135,   204,   362,   362,   151,
     231,   240,   241,   242,   243,   244,   294,   322,   298,    22,
     311,   312,   349,   311,   350,   294,   151,   295,   295,   295,
     348,   297,   149,   350,   350,   350,   350,   151,   151,   157,
     219,   293,   157,   150,   149,   157,   334,   151,   335,   151,
     150,   150,   152,    33,   149,   149,   149,   348,   150,   286,
     285,   151,   282,   209,   135,   135,   158,   159,   160,   161,
     206,   208,   209,   149,    26,   311,   135,   295,    27,   312,
     135,    27,   149,    50,   295,   149,   149,   149,   149,    29,
     334,   335,   345,   350,   152,   141,   142,   343,   334,   335,
     338,   151,   152,   350,   287,   151,   151,   158,   159,   160,
     161,   135,   149,    27,   315,   313,   306,   309,   303,   304,
     301,   302,   152,     8,   342,   343,   335,   157,   335,    20,
     152,   343,   151,   152,   348,   151,   142,   207,   208,   299,
     295,   295,   151,   335,   150,    52,     8,    52,   343,   350,
     335,    20,   338,   151,   288,   149,   316,   314,   295,   342,
     334,   150,   335,   341,   150,   150,    20,   151,   343,   350,
     151,   152,     8,   152,   335,   151,   334,   335,   350,   151,
      20,   151,   338,   334,   335,   343,   334,   152,   343,   151,
     172,   350,   151,   152,   151,   343,    20,   172,   335,    20,
     337,   151,   172,   338,   334,    20,   350,   337,   343,   350,
     149,   151,   337,   152,   172,   350,   151,   149,    20,   151,
     172,   149,   338,   337,   151,   151,   350,   151,   337,   151,
     149,   151,   172,   151,   172,   149,   172,   337,   151,   337,
     337,   149,   172,   149,   149,   337,   149
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 260 "vlgParser.y"
    {
	    modstats = NULL;
	    YYTRACE("source_text:");
          ;}
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 265 "vlgParser.y"
    {
              YYTRACE("source_text: source_text description");
          ;}
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 272 "vlgParser.y"
    {
              YYTRACE("description: module");
          ;}
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 276 "vlgParser.y"
    {
              YYTRACE("description: primitive");
          ;}
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 280 "vlgParser.y"
    {
	      YYTRACE("module_item: type_declaration");
          ;}
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 287 "vlgParser.y"
    {
	      YYTRACE("type_declaration: YYTYPEDEF typesepcifier type_name ';'");
	  ;}
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 294 "vlgParser.y"
    {
	    thismodule = make_module ((yyvsp[(2) - (2)].id));
            fprintf (stdout, "\n=============================start to instrument================================");
	    fprintf (fp_out, "\nmodule %s ",(yyvsp[(2) - (2)].id));
            bran_flag=true;
	    //fprintf (fp_out, "\n'((|%s|\n\t   (type . module)\n",$2);
	    num_width_table_entries = 0;
          ;}
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 303 "vlgParser.y"
    {
	    fprintf (fp_out,"(%s );\n",(yyvsp[(4) - (5)].id));
            //fprintf (fp_out,"\t   (portorder %s)\n",$4);
          ;}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 309 "vlgParser.y"
    {
	    //	    print_modstats ();
	    //print_wwt ();
	    fprintf (fp_out, "\n\t  endmodule \n");
            fprintf( stdout, "\n===============================end instrument=================================== \n");
            bran_var_idx = 0;
	    YYTRACE("module: YYMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          ;}
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 320 "vlgParser.y"
    {
	    YYTRACE("module: YYMACROMODULE YYLID port_list_opt ';' module_item_clr YYENDMODULE");
          ;}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 327 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("port_list_opt:");
          ;}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 332 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(2) - (3)].id));
	    YYTRACE("port_list_opt: '(' port_list ')'");
          ;}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 340 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port_list: port");
          ;}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 345 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s , %s",(yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    //sprintf ($$,"%s %s",$1, $3);
	    YYTRACE("port_list: port_list ',' port");
          ;}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 354 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port: port_expression_opt");
          ;}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 359 "vlgParser.y"
    {
	  ;}
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 362 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(namedport %s %s)",(yyvsp[(2) - (6)].id),(yyvsp[(5) - (6)].id));
	    YYTRACE("port: ',' YYLID '(' port_expression_opt ')'");
          ;}
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 370 "vlgParser.y"
    {
	     strcpy ((yyval.id),"");
	     YYTRACE("port_expression_opt:");
           ;}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 375 "vlgParser.y"
    {
	     sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	     YYTRACE("port_expression_opt: port_expression");
           ;}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 383 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port_expression: port_reference");
          ;}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 388 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(2) - (3)].id));
	    YYTRACE("port_expression: '{' port_ref_list '}'");
          ;}
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 396 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("port_ref_list: port_reference");
          ;}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 401 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("port_ref_list: port_ref_list ',' port_reference");
          ;}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 436 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    //sprintf ($$,"%s|",$1);
	    YYTRACE("port_reference_arg:");
          ;}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 442 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s |%s|)",(yyvsp[(3) - (4)].id),(yyvsp[(1) - (4)].id));
	    YYTRACE("port_reference_arg: '[' expression ']'");
          ;}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 447 "vlgParser.y"
    {
	    sprintf ((yyval.id),"((%s %s) |%s|)",(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id),(yyvsp[(1) - (6)].id));
	    YYTRACE("port_reference_arg: '[' expression ':' expression ']'");
          ;}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 456 "vlgParser.y"
    {
	    YYTRACE("module_item_clr:");
          ;}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 460 "vlgParser.y"
    {
	    YYTRACE("module_item_clr: module_item_clr module_item");
          ;}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 467 "vlgParser.y"
    {
              YYTRACE("module_item: parameter_declaration");
          ;}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 471 "vlgParser.y"
    {
	    YYTRACE("module_item: input_declaration");
          ;}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 475 "vlgParser.y"
    {
	    YYTRACE("module_item: output_declaration");
          ;}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 479 "vlgParser.y"
    {
              YYTRACE("module_item: inout_declaration");
          ;}
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 483 "vlgParser.y"
    {              //
	    YYTRACE("modu//le_item: net_declaration");
          ;}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 487 "vlgParser.y"
    {
              int bvar=0;
              if(bran_flag) 
              {
                //for(bvar=0;bvar<=100;bvar++)
	        fprintf (fp_out,"\n\t  reg [7:0] branvar[0:1000];");
                bran_flag = false;
              }
	    YYTRACE("module_item: reg_declaration");
          ;}
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 498 "vlgParser.y"
    {
              YYTRACE("module_item: time_declaration");
          ;}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 502 "vlgParser.y"
    {
              YYTRACE("module_item: integer_declaration");
          ;}
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 506 "vlgParser.y"
    {
              YYTRACE("module_item: real_declaration");
          ;}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 510 "vlgParser.y"
    {
              YYTRACE("module_item: event_declaration");
          ;}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 514 "vlgParser.y"
    {
              YYTRACE("module_item: gate_instantiation");
          ;}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 518 "vlgParser.y"
    {
              YYTRACE("module_item: module_or_primitive_instantiation");
          ;}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 522 "vlgParser.y"
    {
              YYTRACE("module_item: parameter_override");
          ;}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 526 "vlgParser.y"
    {
              YYTRACE("module_item: continous_assign");
          ;}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 530 "vlgParser.y"
    {
              YYTRACE("module_item: specify_block");
          ;}
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 534 "vlgParser.y"
    {
              YYTRACE("module_item: initial_statement");
          ;}
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 538 "vlgParser.y"
    {
              YYTRACE("module_item: always_statement");
          ;}
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 542 "vlgParser.y"
    {
              YYTRACE("module_item: task");
          ;}
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 546 "vlgParser.y"
    {
              YYTRACE("module_item: function");
          ;}
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 553 "vlgParser.y"
    {
          ;}
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 559 "vlgParser.y"
    {
              YYTRACE("primitive: YYPRMITIVE YYLID '(' variable_list ')' ';' primitive_declaration_eclr table_definition YYENDPRIMITIVE");
          ;}
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 566 "vlgParser.y"
    {
              YYTRACE("primitive_declaration_eclr: primitive_declaration");
          ;}
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 570 "vlgParser.y"
    {
              YYTRACE("primitive_declaration_eclr: primitive_declaration_eclr primitive_declaration");
          ;}
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 577 "vlgParser.y"
    {
	    YYTRACE("primitive_declaration: output_declaration");
          ;}
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 581 "vlgParser.y"
    {
              YYTRACE("primitive_decalration: reg_declaration");
          ;}
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 585 "vlgParser.y"
    {
              YYTRACE("primitive_decalration: input_declaration");
          ;}
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 592 "vlgParser.y"
    {
              YYTRACE("table_definition: YYTABLE table_entries YYENDTABLE");
          ;}
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 599 "vlgParser.y"
    {
              YYTRACE("table_definition: combinational_entry_eclr");
          ;}
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 603 "vlgParser.y"
    {
              YYTRACE("table_definition: sequential_entry_eclr");
          ;}
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 610 "vlgParser.y"
    {
              YYTRACE("combinational_entry_eclr: combinational_entry");
          ;}
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 614 "vlgParser.y"
    {
              YYTRACE("combinational_entry_eclr: combinational_entry_eclr combinational_entry");
          ;}
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 621 "vlgParser.y"
    {
              YYTRACE("combinational_entry: input_list ':' output_symbol ';'");
          ;}
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 628 "vlgParser.y"
    {
              YYTRACE("sequential_entry_eclr: sequential_entry");
          ;}
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 632 "vlgParser.y"
    {
              YYTRACE("sequential_entry_eclr: sequential_entry_eclr sequential_entry");
          ;}
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 639 "vlgParser.y"
    {
              YYTRACE("sequential_entry: input_list ':' state ':' next_state ';'");
          ;}
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 646 "vlgParser.y"
    {
              YYTRACE("input_list: level_symbol_or_edge_eclr");
          ;}
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 653 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge");
          ;}
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 657 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge_eclr: level_symbol_or_edge_eclr level_symbol_or_edge");
          ;}
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 664 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge: level_symbol");
          ;}
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 668 "vlgParser.y"
    {
              YYTRACE("level_symbol_or_edge: edge");
          ;}
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 675 "vlgParser.y"
    {
              YYTRACE("edge: '(' level_symbol level_symbol ')'");
          ;}
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 679 "vlgParser.y"
    {
              YYTRACE("edge: edge_symbol");
          ;}
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 686 "vlgParser.y"
    {
              YYTRACE("state: level_symbol");
          ;}
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 693 "vlgParser.y"
    {
              YYTRACE("next_state: output_symbol");
          ;}
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 697 "vlgParser.y"
    {
              YYTRACE("next_state: '_'");
          ;}
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 704 "vlgParser.y"
    {
              YYTRACE("output_symbol: '0'");
          ;}
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 708 "vlgParser.y"
    {
              YYTRACE("output_symbol: '1'");
          ;}
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 712 "vlgParser.y"
    {
              YYTRACE("output_symbol: 'x'");
          ;}
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 716 "vlgParser.y"
    {
              YYTRACE("output_symbol: 'X'");
          ;}
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 723 "vlgParser.y"
    {
              YYTRACE("level_symbol: '0'");
          ;}
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 727 "vlgParser.y"
    {
              YYTRACE("level_symbol: '1'");
          ;}
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 731 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'x'");
          ;}
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 735 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'X'");
          ;}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 739 "vlgParser.y"
    {
              YYTRACE("level_symbol: '?'");
          ;}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 743 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'b'");
          ;}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 747 "vlgParser.y"
    {
              YYTRACE("level_symbol: 'B'");
          ;}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 754 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'r'");
          ;}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 758 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'R'");
          ;}
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 762 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'f'");
          ;}
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 766 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'F'");
          ;}
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 770 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'p'");
          ;}
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 774 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'P'");
          ;}
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 778 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'n'");
          ;}
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 782 "vlgParser.y"
    {
              YYTRACE("edge_symbol: 'N'");
          ;}
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 786 "vlgParser.y"
    {
              YYTRACE("edge_symbol: '*'");
          ;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 793 "vlgParser.y"
    {
	  ;}
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 797 "vlgParser.y"
    {
              YYTRACE("YYTASK YYLID ';' tf_declaration_clr statement_opt YYENDTASK");
          ;}
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 804 "vlgParser.y"
    {
	  ;}
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 808 "vlgParser.y"
    {
              YYTRACE("YYFUNCTION range_or_type_opt YYLID ';' tf_declaration_eclr statement_opt YYENDFUNCTION");
          ;}
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 815 "vlgParser.y"
    {
              YYTRACE("range_or_type_opt:");
          ;}
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 819 "vlgParser.y"
    {
              YYTRACE("range_or_type_opt: range_or_type");
          ;}
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 826 "vlgParser.y"
    {
              YYTRACE("range_or_type: range");
          ;}
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 830 "vlgParser.y"
    {
              YYTRACE("range_or_type: YYINTEGER");
          ;}
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 834 "vlgParser.y"
    {
              YYTRACE("range_or_type: YYREAL");
          ;}
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 841 "vlgParser.y"
    {
              YYTRACE("tf_declaration_clr:");
          ;}
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 845 "vlgParser.y"
    {
              YYTRACE("tf_declaration_clr: tf_declaration_clr tf_declaration");
          ;}
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 852 "vlgParser.y"
    {
              YYTRACE("tf_declaration_eclr: tf_declaration");
          ;}
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 856 "vlgParser.y"
    {
              YYTRACE("tf_declaration_eclr: tf_decalration_eclr tf_declaration");
          ;}
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 863 "vlgParser.y"
    {
              YYTRACE("tf_declaration: parameter_decalration");
          ;}
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 867 "vlgParser.y"
    {
              YYTRACE("tf_declaration: input_declaration");
          ;}
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 871 "vlgParser.y"
    {
              YYTRACE("tf_declaration: output_declaration");
          ;}
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 875 "vlgParser.y"
    {
              YYTRACE("tf_declaration: inout_declaration");
          ;}
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 879 "vlgParser.y"
    {
              //int bvar=0;
              //for(bvar=0;bvar<=100;bvar++)
	      //  fprintf (fp_out,"\n\t  reg [7:0] branvar_%d;",bvar);
              YYTRACE("tf_declaration: reg_declaration");
          ;}
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 886 "vlgParser.y"
    {
              YYTRACE("tf_declaration: time_declaration");
          ;}
    break;

  case 114:

/* Line 1455 of yacc.c  */
#line 890 "vlgParser.y"
    {
              YYTRACE("tf_declaration: integer_declaration");
          ;}
    break;

  case 115:

/* Line 1455 of yacc.c  */
#line 894 "vlgParser.y"
    {
              YYTRACE("tf_declaration: real_declaration");
          ;}
    break;

  case 116:

/* Line 1455 of yacc.c  */
#line 898 "vlgParser.y"
    {
              YYTRACE("tf_declaration: event_declaration");
          ;}
    break;

  case 117:

/* Line 1455 of yacc.c  */
#line 905 "vlgParser.y"
    {
	      YYTRACE("type_name: YYLID");
	  ;}
    break;

  case 118:

/* Line 1455 of yacc.c  */
#line 912 "vlgParser.y"
    {
	      YYTRACE("type_specifier: enum_specifier ';'");
	  ;}
    break;

  case 119:

/* Line 1455 of yacc.c  */
#line 918 "vlgParser.y"
    {;}
    break;

  case 120:

/* Line 1455 of yacc.c  */
#line 919 "vlgParser.y"
    {
	      YYTRACE("enum_specifier: YYENUM enum_name enum_lst_opt");
	  ;}
    break;

  case 121:

/* Line 1455 of yacc.c  */
#line 922 "vlgParser.y"
    {;}
    break;

  case 122:

/* Line 1455 of yacc.c  */
#line 923 "vlgParser.y"
    {
	      YYTRACE("enum_specifier: YYENUM '{' enumerator_list '}'");
	  ;}
    break;

  case 123:

/* Line 1455 of yacc.c  */
#line 930 "vlgParser.y"
    {
	      YYTRACE("enum_name: YYLID");
	  ;}
    break;

  case 124:

/* Line 1455 of yacc.c  */
#line 937 "vlgParser.y"
    {
	      YYTRACE("enum_lst_opt: '{' enumerator_list '}'");
	  ;}
    break;

  case 125:

/* Line 1455 of yacc.c  */
#line 941 "vlgParser.y"
    {
	      YYTRACE("enum_lst_opt: ");
	  ;}
    break;

  case 126:

/* Line 1455 of yacc.c  */
#line 948 "vlgParser.y"
    {
	      YYTRACE("enumerator_list: enumerator");
	  ;}
    break;

  case 127:

/* Line 1455 of yacc.c  */
#line 952 "vlgParser.y"
    {
	      YYTRACE("enumerator_list: enumerator_list ',' enumerator");
	  ;}
    break;

  case 128:

/* Line 1455 of yacc.c  */
#line 959 "vlgParser.y"
    {
	      YYTRACE("enumerator: YYLID");
	  ;}
    break;

  case 129:

/* Line 1455 of yacc.c  */
#line 962 "vlgParser.y"
    {;}
    break;

  case 130:

/* Line 1455 of yacc.c  */
#line 963 "vlgParser.y"
    {
	      YYTRACE("enumerator: YYLID '=' expression");
	  ;}
    break;

  case 131:

/* Line 1455 of yacc.c  */
#line 972 "vlgParser.y"
    {
	      YYTRACE("type_decorator_opt: YYuTYPE");
	  ;}
    break;

  case 132:

/* Line 1455 of yacc.c  */
#line 976 "vlgParser.y"
    {
	      YYTRACE("type_decorator_opt: ");
	  ;}
    break;

  case 133:

/* Line 1455 of yacc.c  */
#line 982 "vlgParser.y"
    { parameter_start = true; ;}
    break;

  case 134:

/* Line 1455 of yacc.c  */
#line 983 "vlgParser.y"
    {
              parameter_start = false;
              YYTRACE("parameter_declaration: YYPARAMETER assignment_list ';'");
          ;}
    break;

  case 135:

/* Line 1455 of yacc.c  */
#line 991 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (fp_out,"\n\t  input  %s;",(yyvsp[(3) - (4)].id));
	    else 
	      fprintf (fp_out,"\n\t  input %s %s;",(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("input_declaration: YYINPUT range_opt variable_list ';'");
          ;}
    break;

  case 136:

/* Line 1455 of yacc.c  */
#line 1002 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (fp_out,"\n\t  output %s ;",(yyvsp[(3) - (4)].id));
	    else
	      fprintf (fp_out,"\n\t  output %s %s;",(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("output_declaration: YYOUTPUT range_opt variable_list ';'");
          ;}
    break;

  case 137:

/* Line 1455 of yacc.c  */
#line 1013 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(2) - (4)].id),"")==0)
	      fprintf (fp_out,"\t   (inouts %s)\n",(yyvsp[(3) - (4)].id));
	    else
	      fprintf (fp_out,"\t   (inouts %s %s)\n",(yyvsp[(2) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("inout_declaration: YYINOUT range_opt variable_list ';'");
          ;}
    break;

  case 138:

/* Line 1455 of yacc.c  */
#line 1025 "vlgParser.y"
    {
              YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt assignment_list ';'");
          ;}
    break;

  case 139:

/* Line 1455 of yacc.c  */
#line 1030 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(4) - (7)].id),"")==0)
	      fprintf (fp_out,"\n\t  wire %s;",(yyvsp[(6) - (7)].id));
	    else
	      fprintf (fp_out,"\n\t  wire %s %s;",(yyvsp[(4) - (7)].id),(yyvsp[(6) - (7)].id));
	    YYTRACE("net_declaration: type_decorator_opt nettype drive_strength_opt expandrange_opt delay_opt variable_list ';'");
          ;}
    break;

  case 140:

/* Line 1455 of yacc.c  */
#line 1039 "vlgParser.y"
    {
              YYTRACE("net_declaration: type_decorator_opt YYTRIREG charge_strength_opt expandrange_opt delay_opt variable_list ';'");
          ;}
    break;

  case 141:

/* Line 1455 of yacc.c  */
#line 1047 "vlgParser.y"
    {
              YYTRACE("nettype: YYSWIRE");
          ;}
    break;

  case 142:

/* Line 1455 of yacc.c  */
#line 1051 "vlgParser.y"
    {
              YYTRACE("nettype: YYWIRE");
          ;}
    break;

  case 143:

/* Line 1455 of yacc.c  */
#line 1055 "vlgParser.y"
    {
              YYTRACE("nettype: YYTRI");
          ;}
    break;

  case 144:

/* Line 1455 of yacc.c  */
#line 1059 "vlgParser.y"
    {
              YYTRACE("nettype: YYTRI1");
          ;}
    break;

  case 145:

/* Line 1455 of yacc.c  */
#line 1063 "vlgParser.y"
    {
              YYTRACE("nettype: YYSUPPLY0");
          ;}
    break;

  case 146:

/* Line 1455 of yacc.c  */
#line 1067 "vlgParser.y"
    {
              YYTRACE("nettype: YYWAND");
	  ;}
    break;

  case 147:

/* Line 1455 of yacc.c  */
#line 1071 "vlgParser.y"
    {
	      YYTRACE("nettype: YYTRIAND");
	  ;}
    break;

  case 148:

/* Line 1455 of yacc.c  */
#line 1075 "vlgParser.y"
    {
	      YYTRACE("nettype: YYTRI0");
	  ;}
    break;

  case 149:

/* Line 1455 of yacc.c  */
#line 1079 "vlgParser.y"
    {
	      YYTRACE("nettype: YYSUPPLY1");
	  ;}
    break;

  case 150:

/* Line 1455 of yacc.c  */
#line 1083 "vlgParser.y"
    {
	      YYTRACE("nettype: YYWOR");
          ;}
    break;

  case 151:

/* Line 1455 of yacc.c  */
#line 1087 "vlgParser.y"
    {
              YYTRACE("nettype: YYTRIOR");
          ;}
    break;

  case 152:

/* Line 1455 of yacc.c  */
#line 1095 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("expandrange_opt:");
          ;}
    break;

  case 153:

/* Line 1455 of yacc.c  */
#line 1101 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expandrange_opt: expandrange");
          ;}
    break;

  case 154:

/* Line 1455 of yacc.c  */
#line 1109 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expandrange: range");
          ;}
    break;

  case 155:

/* Line 1455 of yacc.c  */
#line 1114 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("expandrange: YYSCALARED range");
          ;}
    break;

  case 156:

/* Line 1455 of yacc.c  */
#line 1119 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("expandrange: YYVECTORED range");
          ;}
    break;

  case 157:

/* Line 1455 of yacc.c  */
#line 1127 "vlgParser.y"
    {
	    if (strcmp((yyvsp[(3) - (5)].id),"")==0)
	      fprintf (fp_out,"\n\t  reg %s;",(yyvsp[(4) - (5)].id));
	    else
	      fprintf (fp_out,"\n\t  reg %s %s;",(yyvsp[(3) - (5)].id),(yyvsp[(4) - (5)].id));
	    YYTRACE("reg_declaration: type_decorator_opt YYREG range_opt register_variable_list ';'");
          ;}
    break;

  case 158:

/* Line 1455 of yacc.c  */
#line 1135 "vlgParser.y"
    {
              YYTRACE("reg_declaration: type_decorator_opt YYMREG range_opt register_variable_list ';'");
          ;}
    break;

  case 159:

/* Line 1455 of yacc.c  */
#line 1142 "vlgParser.y"
    {
              YYTRACE("time_declaration: YYTIME register_variable_list ';'");
          ;}
    break;

  case 160:

/* Line 1455 of yacc.c  */
#line 1149 "vlgParser.y"
    {
              YYTRACE("integer_declaration: YYINTEGER register_variable_list ';'");
          ;}
    break;

  case 161:

/* Line 1455 of yacc.c  */
#line 1156 "vlgParser.y"
    {
              YYTRACE("real_declaration: YYREAL variable_list ';'");
          ;}
    break;

  case 162:

/* Line 1455 of yacc.c  */
#line 1163 "vlgParser.y"
    {
              YYTRACE("event_declaration: YYEVENT name_of_event_list ';'");
          ;}
    break;

  case 163:

/* Line 1455 of yacc.c  */
#line 1169 "vlgParser.y"
    {fprintf(fp_out,"\n\t assign ");;}
    break;

  case 164:

/* Line 1455 of yacc.c  */
#line 1171 "vlgParser.y"
    {
            cfg_temp = build_normal_node((yyvsp[(5) - (6)].id),NULL,NULL,NULL,NULL);
            cfg_temp->node_type = BLK_AS;
            push_cfg_table(cfg_temp);
	    //fprintf (fp_out,"\t   (assign %s)\n",$5);
	    YYTRACE("continuous_assign: YYASSIGN drive_strength_opt delay_opt assignment_list ';'");
          ;}
    break;

  case 165:

/* Line 1455 of yacc.c  */
#line 1182 "vlgParser.y"
    {
	    fprintf (fp_out,"\t   (defparam %s)\n",(yyvsp[(2) - (3)].id));
	    YYTRACE("parameter_override: YYDEFPARAM assign_list ';'");
          ;}
    break;

  case 166:

/* Line 1455 of yacc.c  */
#line 1190 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    put_wwt_entry ((yyvsp[(1) - (1)].id), current_range);
	    YYTRACE("variable_list: identifier");
          ;}
    break;

  case 167:

/* Line 1455 of yacc.c  */
#line 1196 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s, %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    put_wwt_entry ((yyvsp[(3) - (3)].id), current_range);
	    YYTRACE("variable_list: variable_list ',' identifier");
          ;}
    break;

  case 168:

/* Line 1455 of yacc.c  */
#line 1205 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("register_variable_list: register_variable");
          ;}
    break;

  case 169:

/* Line 1455 of yacc.c  */
#line 1210 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s, %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("register_variable_list: register_variable_list ',' register_variable");
          ;}
    break;

  case 170:

/* Line 1455 of yacc.c  */
#line 1218 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    put_wwt_entry ((yyvsp[(1) - (1)].id), current_range);
	    YYTRACE("register_variable: name_of_register");
          ;}
    break;

  case 171:

/* Line 1455 of yacc.c  */
#line 1224 "vlgParser.y"
    {
	    sprintf ((yyval.id),"((%s %s) %s)",(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id),(yyvsp[(1) - (6)].id));
	    {char s[MAXSTRLEN]; sprintf (s,"(%s %s)",(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id));
	    put_wwt_entry ((yyvsp[(1) - (6)].id), s);}
	    YYTRACE("register_variable: name_of_register '[' expression ':' expression ']'");
          ;}
    break;

  case 172:

/* Line 1455 of yacc.c  */
#line 1234 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_register: YYLID");
          ;}
    break;

  case 173:

/* Line 1455 of yacc.c  */
#line 1242 "vlgParser.y"
    {
              YYTRACE("name_of_event_list: name_of_event");
          ;}
    break;

  case 174:

/* Line 1455 of yacc.c  */
#line 1246 "vlgParser.y"
    {
              YYTRACE("name_of_event_list: name_of_event_list ',' name_of_event");
          ;}
    break;

  case 175:

/* Line 1455 of yacc.c  */
#line 1253 "vlgParser.y"
    {
              YYTRACE("name_of_event: YYLID");
          ;}
    break;

  case 176:

/* Line 1455 of yacc.c  */
#line 1260 "vlgParser.y"
    {
              YYTRACE("charge_strength_opt:");
          ;}
    break;

  case 177:

/* Line 1455 of yacc.c  */
#line 1264 "vlgParser.y"
    {
              YYTRACE("charge_strength_opt: charge_strength");
          ;}
    break;

  case 178:

/* Line 1455 of yacc.c  */
#line 1271 "vlgParser.y"
    {
              YYTRACE("charge_strength: '(' YYSMALL ')'");
          ;}
    break;

  case 179:

/* Line 1455 of yacc.c  */
#line 1275 "vlgParser.y"
    {
              YYTRACE("charge_strength: '(' YYMEDIUM ')'");
          ;}
    break;

  case 180:

/* Line 1455 of yacc.c  */
#line 1279 "vlgParser.y"
    {
              YYTRACE("charge_strength: '(' YYLARGE ')'");
          ;}
    break;

  case 181:

/* Line 1455 of yacc.c  */
#line 1286 "vlgParser.y"
    {
              YYTRACE("drive_strength_opt:");
          ;}
    break;

  case 182:

/* Line 1455 of yacc.c  */
#line 1290 "vlgParser.y"
    {
              YYTRACE("drive_strength_opt: drive_strength");
          ;}
    break;

  case 183:

/* Line 1455 of yacc.c  */
#line 1297 "vlgParser.y"
    {
              YYTRACE("drive_strength: '(' strength0 ',' strength1 ')'");
          ;}
    break;

  case 184:

/* Line 1455 of yacc.c  */
#line 1301 "vlgParser.y"
    {
              YYTRACE("drive_strength: '(' strength1 ',' strength0 ')'");
          ;}
    break;

  case 185:

/* Line 1455 of yacc.c  */
#line 1308 "vlgParser.y"
    {
              YYTRACE("strength0: YYSUPPLY0");
          ;}
    break;

  case 186:

/* Line 1455 of yacc.c  */
#line 1312 "vlgParser.y"
    {
              YYTRACE("strength0: YYSTRONG0");
          ;}
    break;

  case 187:

/* Line 1455 of yacc.c  */
#line 1316 "vlgParser.y"
    {
              YYTRACE("strength0: YYPULL0");
          ;}
    break;

  case 188:

/* Line 1455 of yacc.c  */
#line 1320 "vlgParser.y"
    {
              YYTRACE("strength0: YYWEAK0");
          ;}
    break;

  case 189:

/* Line 1455 of yacc.c  */
#line 1324 "vlgParser.y"
    {
              YYTRACE("strength0: YYHIGHZ0");
          ;}
    break;

  case 190:

/* Line 1455 of yacc.c  */
#line 1331 "vlgParser.y"
    {
              YYTRACE("strength1: YYSUPPLY1");
          ;}
    break;

  case 191:

/* Line 1455 of yacc.c  */
#line 1335 "vlgParser.y"
    {
              YYTRACE("strength1: YYSTRONG1");
          ;}
    break;

  case 192:

/* Line 1455 of yacc.c  */
#line 1339 "vlgParser.y"
    {
              YYTRACE("strength1: YYPULL1");
          ;}
    break;

  case 193:

/* Line 1455 of yacc.c  */
#line 1343 "vlgParser.y"
    {
              YYTRACE("strength1: YYWEAK1");
          ;}
    break;

  case 194:

/* Line 1455 of yacc.c  */
#line 1347 "vlgParser.y"
    {
              YYTRACE("strength1: YYHIGHZ1");
          ;}
    break;

  case 195:

/* Line 1455 of yacc.c  */
#line 1354 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    sprintf (current_range,"(0 0)");
	    YYTRACE("range_opt:");
          ;}
    break;

  case 196:

/* Line 1455 of yacc.c  */
#line 1360 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    sprintf (current_range, (yyvsp[(1) - (1)].id));
	    YYTRACE("range_opt: range");
          ;}
    break;

  case 197:

/* Line 1455 of yacc.c  */
#line 1369 "vlgParser.y"
    {
	    sprintf ((yyval.id),"[%s : %s]",(yyvsp[(2) - (5)].id),(yyvsp[(4) - (5)].id));
	    sprintf (current_range,"(%s %s)",(yyvsp[(2) - (5)].id),(yyvsp[(4) - (5)].id));
              YYTRACE("range: '[' expression ':' expression ']'");
          ;}
    break;

  case 198:

/* Line 1455 of yacc.c  */
#line 1378 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("assignment_list: assignment");
          ;}
    break;

  case 199:

/* Line 1455 of yacc.c  */
#line 1383 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("assignment_list: assignment_list ',' assignment");
          ;}
    break;

  case 200:

/* Line 1455 of yacc.c  */
#line 1392 "vlgParser.y"
    {
	    fprintf (fp_out,"\t   (occs %s %s)\n",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("gate_instantiation: gatetype drive_delay_clr gate_instance_list ';'");
          ;}
    break;

  case 201:

/* Line 1455 of yacc.c  */
#line 1400 "vlgParser.y"
    {
              YYTRACE("drive_delay_clr:");
          ;}
    break;

  case 202:

/* Line 1455 of yacc.c  */
#line 1404 "vlgParser.y"
    {
              YYTRACE("drive_delay_clr: drive_delay_clr drive_delay");
          ;}
    break;

  case 203:

/* Line 1455 of yacc.c  */
#line 1411 "vlgParser.y"
    {
              YYTRACE("drive_delay: drive_strength");
          ;}
    break;

  case 204:

/* Line 1455 of yacc.c  */
#line 1415 "vlgParser.y"
    {
              YYTRACE("drive_delay: delay");
          ;}
    break;

  case 205:

/* Line 1455 of yacc.c  */
#line 1422 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYAND");
	  ;}
    break;

  case 206:

/* Line 1455 of yacc.c  */
#line 1427 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNAND");
	  ;}
    break;

  case 207:

/* Line 1455 of yacc.c  */
#line 1432 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYOR");
	  ;}
    break;

  case 208:

/* Line 1455 of yacc.c  */
#line 1437 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOR");
	  ;}
    break;

  case 209:

/* Line 1455 of yacc.c  */
#line 1442 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYXOR");
	  ;}
    break;

  case 210:

/* Line 1455 of yacc.c  */
#line 1447 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYXNOR");
	  ;}
    break;

  case 211:

/* Line 1455 of yacc.c  */
#line 1452 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYBUF");
	  ;}
    break;

  case 212:

/* Line 1455 of yacc.c  */
#line 1457 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYBIFIF0");
	  ;}
    break;

  case 213:

/* Line 1455 of yacc.c  */
#line 1462 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYBIFIF1");
	  ;}
    break;

  case 214:

/* Line 1455 of yacc.c  */
#line 1467 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOT");
	  ;}
    break;

  case 215:

/* Line 1455 of yacc.c  */
#line 1472 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOTIF0");
	  ;}
    break;

  case 216:

/* Line 1455 of yacc.c  */
#line 1477 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNOTIF1");
	  ;}
    break;

  case 217:

/* Line 1455 of yacc.c  */
#line 1482 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYPULLDOWN");
	  ;}
    break;

  case 218:

/* Line 1455 of yacc.c  */
#line 1487 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYPULLUP");
	  ;}
    break;

  case 219:

/* Line 1455 of yacc.c  */
#line 1492 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYNMOS");
	  ;}
    break;

  case 220:

/* Line 1455 of yacc.c  */
#line 1497 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYPMOS");
	  ;}
    break;

  case 221:

/* Line 1455 of yacc.c  */
#line 1502 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRNMOS");
	  ;}
    break;

  case 222:

/* Line 1455 of yacc.c  */
#line 1507 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRPMOS");
	  ;}
    break;

  case 223:

/* Line 1455 of yacc.c  */
#line 1512 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYCMOS");
	  ;}
    break;

  case 224:

/* Line 1455 of yacc.c  */
#line 1517 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRCMOS");
	  ;}
    break;

  case 225:

/* Line 1455 of yacc.c  */
#line 1522 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTRAN");
	  ;}
    break;

  case 226:

/* Line 1455 of yacc.c  */
#line 1527 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRTRAN");
	  ;}
    break;

  case 227:

/* Line 1455 of yacc.c  */
#line 1532 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTRANIF0");
	  ;}
    break;

  case 228:

/* Line 1455 of yacc.c  */
#line 1537 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRTRANIF0");
	  ;}
    break;

  case 229:

/* Line 1455 of yacc.c  */
#line 1542 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTRANIF1");
	  ;}
    break;

  case 230:

/* Line 1455 of yacc.c  */
#line 1547 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYRTRANIF1");
	  ;}
    break;

  case 231:

/* Line 1455 of yacc.c  */
#line 1552 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("gatetype: YYTESLATIMER");
	  ;}
    break;

  case 232:

/* Line 1455 of yacc.c  */
#line 1560 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("gate_instance_list: gate_instance");
          ;}
    break;

  case 233:

/* Line 1455 of yacc.c  */
#line 1565 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("gate_instance_list: gate_instance_list ',' gate_instance");
          ;}
    break;

  case 234:

/* Line 1455 of yacc.c  */
#line 1573 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s)",(yyvsp[(2) - (3)].id));
	    YYTRACE("gate_instance: '(' terminal_list ')'");
          ;}
    break;

  case 235:

/* Line 1455 of yacc.c  */
#line 1578 "vlgParser.y"
    {
	    sprintf ((yyval.id),"((occname %s) %s)",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("gate_instance: name_of_gate_instance '(' terminal_list ')'");
          ;}
    break;

  case 236:

/* Line 1455 of yacc.c  */
#line 1586 "vlgParser.y"
    {
	    sprintf ((yyval.id),"|%s|",(yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_gate_instance: YYLID");
          ;}
    break;

  case 237:

/* Line 1455 of yacc.c  */
#line 1594 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("terminal_list: terminal");
          ;}
    break;

  case 238:

/* Line 1455 of yacc.c  */
#line 1599 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("terminal_list: terminal_list ',' terminal");
          ;}
    break;

  case 239:

/* Line 1455 of yacc.c  */
#line 1607 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("terminal: expression");
          ;}
    break;

  case 240:

/* Line 1455 of yacc.c  */
#line 1615 "vlgParser.y"
    {fprintf(fp_out, "\n\t  %s ", (yyvsp[(1) - (1)].id));;}
    break;

  case 241:

/* Line 1455 of yacc.c  */
#line 1616 "vlgParser.y"
    {fprintf(fp_out, ";");;}
    break;

  case 242:

/* Line 1455 of yacc.c  */
#line 1617 "vlgParser.y"
    {
	    //if (strcmp($2,"")==0)
	    //  fprintf (fp_out,"\t   (%s %s)\n",$1,{$3); 
	    //else
	    //  fprintf (fp_out,"\t   (%s (options %s) %s)\n",$1,$2,$3); 
	    YYTRACE("module_or_primitive_instantiation: name_of_module_or_primitive module_or_primitive_option_clr module_or_primitive_instance_list ';'");
          ;}
    break;

  case 243:

/* Line 1455 of yacc.c  */
#line 1628 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_module_or_primitive: YYLID");
          ;}
    break;

  case 244:

/* Line 1455 of yacc.c  */
#line 1636 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("module_or_primitive_option_clr:");
          ;}
    break;

  case 245:

/* Line 1455 of yacc.c  */
#line 1641 "vlgParser.y"
    {
	    sprintf ((yyval.id), "%s %s", (yyvsp[(1) - (2)].id), (yyvsp[(2) - (2)].id));
	    YYTRACE("module_or_primitive_option_clr: module_or_primitive_option_clr module_or_primitive_option");
          ;}
    break;

  case 246:

/* Line 1455 of yacc.c  */
#line 1649 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("module_or_primitive_option:");
          ;}
    break;

  case 247:

/* Line 1455 of yacc.c  */
#line 1654 "vlgParser.y"
    {
	    strcpy((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("module_or_primitive_option: delay");
          ;}
    break;

  case 248:

/* Line 1455 of yacc.c  */
#line 1662 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER");
	  ;}
    break;

  case 249:

/* Line 1455 of yacc.c  */
#line 1667 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYINUMBER_BIT");
	  ;}
    break;

  case 250:

/* Line 1455 of yacc.c  */
#line 1672 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));	
	    YYTRACE("delay_or_parameter_value_assignment: '#' YYRNUMBER");
	  ;}
    break;

  case 251:

/* Line 1455 of yacc.c  */
#line 1677 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' identifier");
	  ;}
    break;

  case 252:

/* Line 1455 of yacc.c  */
#line 1682 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(3) - (4)].id));
	    YYTRACE("delay_or_parameter_value_assignment: '#' '(' mintypmax_expression_list ')'");
	  ;}
    break;

  case 253:

/* Line 1455 of yacc.c  */
#line 1690 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
            YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance");
          ;}
    break;

  case 254:

/* Line 1455 of yacc.c  */
#line 1695 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("module_or_primitive_instance_list: module_or_primitive_instance_list ',' module_or_primitive_instance");
          ;}
    break;

  case 255:

/* Line 1455 of yacc.c  */
#line 1704 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s)",(yyvsp[(2) - (3)].id));
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          ;}
    break;

  case 256:

/* Line 1455 of yacc.c  */
#line 1709 "vlgParser.y"
    {fprintf(fp_out,"%s (",(yyvsp[(1) - (1)].id));;}
    break;

  case 257:

/* Line 1455 of yacc.c  */
#line 1710 "vlgParser.y"
    {
            fprintf(fp_out,")");
	    sprintf ((yyval.id)," %s (%s)",(yyvsp[(1) - (5)].id),(yyvsp[(4) - (5)].id));
	    YYTRACE("module_or_primitive_instance: '(' module_connection_list ')'");
          ;}
    break;

  case 258:

/* Line 1455 of yacc.c  */
#line 1719 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("module_connection_list: module_port_connection_list");
          ;}
    break;

  case 259:

/* Line 1455 of yacc.c  */
#line 1724 "vlgParser.y"
    {
            push_cfg_table((yyvsp[(1) - (1)].NODE));
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].NODE)->exp_name);
	    YYTRACE("module_connection_list: named_port_connection_list");
          ;}
    break;

  case 260:

/* Line 1455 of yacc.c  */
#line 1733 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("module_port_connection_list: module_port_connection");
          ;}
    break;

  case 261:

/* Line 1455 of yacc.c  */
#line 1738 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("module_port_connection_list: module_port_connection_list ',' module_port_connection");
          ;}
    break;

  case 262:

/* Line 1455 of yacc.c  */
#line 1746 "vlgParser.y"
    {
	    //sprintf ($$,$1);
            (yyval.NODE)=(yyvsp[(1) - (1)].NODE);
	    YYTRACE("named_port_connection_list: named_port_connection");
          ;}
    break;

  case 263:

/* Line 1455 of yacc.c  */
#line 1751 "vlgParser.y"
    {fprintf(fp_out, ",");;}
    break;

  case 264:

/* Line 1455 of yacc.c  */
#line 1752 "vlgParser.y"
    {
	    //sprintf ($$,"%s %s",$1,$3);
            cfg_temp = (yyvsp[(1) - (4)].NODE);
            while(cfg_temp->next_node!=NULL)
              cfg_temp=cfg_temp->next_node;
            cfg_temp->next_node = (yyvsp[(4) - (4)].NODE);
            (yyval.NODE)=(yyvsp[(1) - (4)].NODE);
	    YYTRACE("named_port_connection_list: named_port_connection_list ',' name_port_connection");
          ;}
    break;

  case 265:

/* Line 1455 of yacc.c  */
#line 1765 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("module_port_connection:");
          ;}
    break;

  case 266:

/* Line 1455 of yacc.c  */
#line 1770 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("module_port_connection: expression");
          ;}
    break;

  case 267:

/* Line 1455 of yacc.c  */
#line 1777 "vlgParser.y"
    {fprintf(fp_out,"\n\t .%s (%s)",(yyvsp[(2) - (5)].id),(yyvsp[(4) - (5)].id));;}
    break;

  case 268:

/* Line 1455 of yacc.c  */
#line 1778 "vlgParser.y"
    {
            char id_temp[MAXSTRLEN];
            sprintf (id_temp,"%s = %s",(yyvsp[(2) - (6)].id),(yyvsp[(4) - (6)].id));
            cfg_temp = build_normal_node(id_temp, NULL, NULL, NULL, NULL);
            cfg_temp->node_type = PORT_CONNECT;
            (yyval.NODE)=cfg_temp;
	    //sprintf ($$,"(namedport %s %s)",$2,$4);
	    YYTRACE("named_port_connection: '.' identifier '(' expression ')'");
          ;}
    break;

  case 269:

/* Line 1455 of yacc.c  */
#line 1792 "vlgParser.y"
    {;}
    break;

  case 270:

/* Line 1455 of yacc.c  */
#line 1793 "vlgParser.y"
    {
	    fprintf (fp_out,"\t   (initial\n\t\t%s\n\t   )\n",(yyvsp[(3) - (3)].NODE)->exp_name);
	    YYTRACE("initial_statement: YYINITIAL statement");
          ;}
    break;

  case 271:

/* Line 1455 of yacc.c  */
#line 1800 "vlgParser.y"
    {;}
    break;

  case 272:

/* Line 1455 of yacc.c  */
#line 1801 "vlgParser.y"
    {
            push_cfg_table((yyvsp[(3) - (3)].NODE));
	    //fprintf (fp_out,"\n   always %s",$3->exp_name);
  	    YYTRACE("always_statement: YYALWAYS statement");
          ;}
    break;

  case 273:

/* Line 1455 of yacc.c  */
#line 1810 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("statement_opt:");
          ;}
    break;

  case 274:

/* Line 1455 of yacc.c  */
#line 1815 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].NODE)->exp_name);
	    YYTRACE("statement_opt: statement");
          ;}
    break;

  case 275:

/* Line 1455 of yacc.c  */
#line 1823 "vlgParser.y"
    {
	    //strcpy ($$,"");
            //printf("return\n");
            (yyval.NODE)=NULL;
	    YYTRACE("statement_clr:");
          ;}
    break;

  case 276:

/* Line 1455 of yacc.c  */
#line 1830 "vlgParser.y"
    {
	    //sprintf ($$, "%s  %s", $1, $2);
            if((yyvsp[(1) - (2)].NODE)!=NULL)
              set_next_node((yyvsp[(1) - (2)].NODE),(yyvsp[(2) - (2)].NODE));  
            if((yyvsp[(1) - (2)].NODE)!=NULL) 
              (yyval.NODE)=(yyvsp[(1) - (2)].NODE);
            else
              (yyval.NODE)=(yyvsp[(2) - (2)].NODE);
            //==if($1!=NULL){
            //==  printf("......%s---next is",$1->exp_name);
            //==  printf("......%s\n",$1->next_node->exp_name);
            //==}
	    YYTRACE("statement_clr: statement_clr statement");
          ;}
    break;

  case 277:

/* Line 1455 of yacc.c  */
#line 1849 "vlgParser.y"
    {
	    //strcpy ($$,"");
            //$$=(_cfg_node *)malloc(sizeof(_cfg_node));
            //strcpy($$->exp_name,"");
            (yyval.NODE)=NULL;
	    YYTRACE("statement: ';'");
          ;}
    break;

  case 278:

/* Line 1455 of yacc.c  */
#line 1857 "vlgParser.y"
    {
	    //sprintf ($$->exp_name, "%s", $1);
            cfg_top = build_normal_node((yyvsp[(1) - (2)].id),NULL,NULL,NULL,NULL);
            cfg_top->node_type = BLK_AS;
            (yyval.NODE)=cfg_top;
            //printf("deduce an statement : %s",$$->exp_name);
	    YYTRACE("statement: assignment ';'");
          ;}
    break;

  case 279:

/* Line 1455 of yacc.c  */
#line 1866 "vlgParser.y"
    {
          fprintf(fp_out, "\n\t else \n\t begin  \n\t   %s   \n\t   end ",get_next_inst_statement());
          
          ;}
    break;

  case 280:

/* Line 1455 of yacc.c  */
#line 1882 "vlgParser.y"
    {fprintf(fp_out,"\n\t else \n\t   begin \n\t   %s   ",get_next_inst_statement() );;}
    break;

  case 281:

/* Line 1455 of yacc.c  */
#line 1882 "vlgParser.y"
    {fprintf(fp_out,"\n\t  end");;}
    break;

  case 282:

/* Line 1455 of yacc.c  */
#line 1883 "vlgParser.y"
    {
            //===cfg_temp = build_blank_node();
            //===cfg_top = build_normal_node($3,$5,NULL,cfg_temp,NULL);
            //===$5->brother_node=$8;
            //===cfg_top->node_type = IFELSE_EXP;
            //===set_next_node($5,cfg_temp);
            //===$$=cfg_top;
            //cfg_temp = build_blank_node();
            //cfg_top = build_normal_node($1->exp_name,$5,NULL,cfg_temp,NULL);
            (yyvsp[(1) - (5)].NODE)->left_node->brother_node=(yyvsp[(4) - (5)].NODE);
            //cfg_top->node_type = IFELSE_EXP;
            //set_next_node($5,cfg_temp);
            (yyval.NODE)=cfg_top;

            //==printf("----%s\n",$3);
            //==printf("----%s\n",$5->exp_name);
            //==printf("----%s\n",$5->next_node->exp_name);
            //==printf("----%s\n",$7->exp_name);
	    //sprintf ($$, "(if %s %s %s)", $3, $5, $7);
	    YYTRACE("statement: YYIF '(' expression ')' statement YYELSE statement");   
	  ;}
    break;

  case 283:

/* Line 1455 of yacc.c  */
#line 1904 "vlgParser.y"
    {fprintf(fp_out,"\n\t case(%s)",(yyvsp[(3) - (4)].id));;}
    break;

  case 284:

/* Line 1455 of yacc.c  */
#line 1904 "vlgParser.y"
    {fprintf(fp_out,"\n\t endcase");;}
    break;

  case 285:

/* Line 1455 of yacc.c  */
#line 1905 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node((yyvsp[(3) - (8)].id),(yyvsp[(6) - (8)].NODE),NULL,cfg_temp,NULL);
            cfg_top->node_type = CASE_EXP;
            set_next_node((yyvsp[(6) - (8)].NODE),cfg_temp); 
            (yyval.NODE)=cfg_top;
            //cfg_temp=$5;
            //while(cfg_temp!=NULL){
            //  printf("++++%s \n",cfg_temp->exp_name);
            //  cfg_temp=cfg_temp->next_node;
            //}
	    //sprintf ($$, "(case %s %s)", $3, $5);
            //printf("");
	    YYTRACE("statement: YYCASE '(' expression ')' case_item_eclr YYENDCASE");
          ;}
    break;

  case 286:

/* Line 1455 of yacc.c  */
#line 1921 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node((yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].NODE),NULL,cfg_temp,NULL);
            cfg_top->node_type = CASE_EXP;
            set_next_node((yyvsp[(5) - (6)].NODE),cfg_temp); 
            (yyval.NODE)=cfg_top;
            //sprintf ($$, "(casez %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEZ '(' expression ')' case_item_eclr YYENDCASE"); 
          ;}
    break;

  case 287:

/* Line 1455 of yacc.c  */
#line 1931 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node((yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].NODE),NULL,cfg_temp,NULL);
            cfg_top->node_type = CASE_EXP;
            set_next_node((yyvsp[(5) - (6)].NODE),cfg_temp); 
            (yyval.NODE)=cfg_top;
	    //sprintf ($$, "(casex %s %s)", $3, $5);
	    YYTRACE("statement: YYCASEX '(' expression ')' case_item_eclr YYENDCASE");
          ;}
    break;

  case 288:

/* Line 1455 of yacc.c  */
#line 1941 "vlgParser.y"
    {
              cfg_temp = build_blank_node();
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOREVER statement");
          ;}
    break;

  case 289:

/* Line 1455 of yacc.c  */
#line 1948 "vlgParser.y"
    {
              cfg_temp = build_blank_node();
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYREPEAT '(' expression ')' statement");
          ;}
    break;

  case 290:

/* Line 1455 of yacc.c  */
#line 1955 "vlgParser.y"
    {
              cfg_temp = build_blank_node();
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYWHILE '(' expression ')' statement");
          ;}
    break;

  case 291:

/* Line 1455 of yacc.c  */
#line 1962 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
 	      //strcpy ($$,"");
              YYTRACE("statement: YYFOR '(' assignment ';' expression ';' assignment ')' statement");
          ;}
    break;

  case 292:

/* Line 1455 of yacc.c  */
#line 1968 "vlgParser.y"
    {
              (yyval.NODE)=(yyvsp[(2) - (2)].NODE);
 	      //strcpy ($$,"");
              YYTRACE("statement: delay_control statement");
          ;}
    break;

  case 293:

/* Line 1455 of yacc.c  */
#line 1973 "vlgParser.y"
    {fprintf (fp_out,"\n\t %s", (yyvsp[(1) - (1)].id));;}
    break;

  case 294:

/* Line 1455 of yacc.c  */
#line 1974 "vlgParser.y"
    {
              (yyval.NODE)=(yyvsp[(3) - (3)].NODE);
 	      //fprintf (fp_out,"\n\t %s", $1);
              YYTRACE("statement: event_control statement");
          ;}
    break;

  case 295:

/* Line 1455 of yacc.c  */
#line 1979 "vlgParser.y"
    {fprintf(fp_out,"\n\t %s = %s;",(yyvsp[(1) - (6)].id),(yyvsp[(5) - (6)].id));;}
    break;

  case 296:

/* Line 1455 of yacc.c  */
#line 1980 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = BLK_AS;
              sprintf(cfg_temp->exp_name,"%s = %s",(yyvsp[(1) - (7)].id),(yyvsp[(5) - (7)].id));
              (yyval.NODE)=cfg_temp;
              YYTRACE("statement: lvalue '=' delay_control expression ';'");
          ;}
    break;

  case 297:

/* Line 1455 of yacc.c  */
#line 1987 "vlgParser.y"
    {fprintf(fp_out,"\n\t %s = %s;",(yyvsp[(1) - (6)].id),(yyvsp[(5) - (6)].id));;}
    break;

  case 298:

/* Line 1455 of yacc.c  */
#line 1988 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = BLK_AS;
              sprintf(cfg_temp->exp_name,"%s = %s",(yyvsp[(1) - (7)].id),(yyvsp[(5) - (7)].id));
              (yyval.NODE)=cfg_temp;
              YYTRACE("statement: lvalue '=' event_control expression ';'");
          ;}
    break;

  case 299:

/* Line 1455 of yacc.c  */
#line 1995 "vlgParser.y"
    {fprintf(fp_out,"\n\t %s <= %s;",(yyvsp[(1) - (6)].id),(yyvsp[(5) - (6)].id));;}
    break;

  case 300:

/* Line 1455 of yacc.c  */
#line 1996 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = NBLK_AS;
              sprintf(cfg_temp->exp_name,"%s <= %s",(yyvsp[(1) - (7)].id),(yyvsp[(5) - (7)].id));
              (yyval.NODE)=cfg_temp;
              //printf("deduce an statement : %s",$$->exp_name);
              YYTRACE("statement: lvalue YYNBASSIGN delay_control expression ';'");
          ;}
    break;

  case 301:

/* Line 1455 of yacc.c  */
#line 2004 "vlgParser.y"
    {fprintf(fp_out,"\n\t %s <= %s;",(yyvsp[(1) - (6)].id),(yyvsp[(5) - (6)].id));;}
    break;

  case 302:

/* Line 1455 of yacc.c  */
#line 2005 "vlgParser.y"
    {
              cfg_temp = build_normal_node("",NULL,NULL,NULL,NULL);
              cfg_temp->node_type = NBLK_AS;
              sprintf(cfg_temp->exp_name,"%s <= %s",(yyvsp[(1) - (7)].id),(yyvsp[(5) - (7)].id));
              (yyval.NODE)=cfg_temp;
 	      //strcpy ($$,"");
              YYTRACE("statement: lvalue YYNBASSIGN event_control expression ';'");
          ;}
    break;

  case 303:

/* Line 1455 of yacc.c  */
#line 2014 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYWAIT '(' expression ')' statement");
          ;}
    break;

  case 304:

/* Line 1455 of yacc.c  */
#line 2019 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYRIGHTARROW name_of_event ';'");
          ;}
    break;

  case 305:

/* Line 1455 of yacc.c  */
#line 2024 "vlgParser.y"
    {
              (yyval.NODE)=(yyvsp[(1) - (1)].NODE);
              //fprintf(fp_out, "\n\t begin");
              YYTRACE("statement: seq_block");
          ;}
    break;

  case 306:

/* Line 1455 of yacc.c  */
#line 2030 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: par_block");
          ;}
    break;

  case 307:

/* Line 1455 of yacc.c  */
#line 2035 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: task_enable");
          ;}
    break;

  case 308:

/* Line 1455 of yacc.c  */
#line 2040 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: system_task_enable");
          ;}
    break;

  case 309:

/* Line 1455 of yacc.c  */
#line 2045 "vlgParser.y"
    {
	  ;}
    break;

  case 310:

/* Line 1455 of yacc.c  */
#line 2048 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYDISABLE YYLID ';'");
          ;}
    break;

  case 311:

/* Line 1455 of yacc.c  */
#line 2053 "vlgParser.y"
    {
            cfg_temp = build_normal_node((yyvsp[(2) - (3)].id),NULL,NULL,NULL,NULL);
            cfg_temp->node_type = BLK_AS;
            (yyval.NODE)=cfg_temp;
	    YYTRACE("statement: YYASSIGN assignment ';'");
          ;}
    break;

  case 312:

/* Line 1455 of yacc.c  */
#line 2060 "vlgParser.y"
    {
              (yyval.NODE)=NULL;
              YYTRACE("statement: YYDEASSIGN lvalue ';'");
          ;}
    break;

  case 313:

/* Line 1455 of yacc.c  */
#line 2068 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s %s)",get_wwt_entry((yyvsp[(1) - (4)].id)),(yyvsp[(4) - (4)].id));
            //add the parameter handle program
            if(parameter_start)
            {
              //bool parameter_start=false;
              //_para_table current_para_table[MAXPARAS]; 
              //int para_table_idx=0;
              strcpy(current_para_table[para_table_idx].para_name,(yyvsp[(1) - (4)].id));
              strcpy(current_para_table[para_table_idx].para_value,(yyvsp[(4) - (4)].id));
              printf("\n para %s = %s \n",current_para_table[para_table_idx].para_name,current_para_table[para_table_idx].para_value);
              para_table_idx++;
            }
            else  
            fprintf(fp_out,"\n\t  %s = %s;",(yyvsp[(1) - (4)].id),(yyvsp[(4) - (4)].id));
	    YYTRACE("assignment: lvalue '=' expression");
          ;}
    break;

  case 314:

/* Line 1455 of yacc.c  */
#line 2086 "vlgParser.y"
    {
            fprintf(fp_out,"\n\t  %s <= %s;",(yyvsp[(1) - (4)].id),(yyvsp[(4) - (4)].id));
	    sprintf ((yyval.id),"(nonblock %s %s)",get_wwt_entry((yyvsp[(1) - (4)].id)),(yyvsp[(4) - (4)].id));
	    YYTRACE("assignment: lvalue YYNBASSIGN expression");
          ;}
    break;

  case 315:

/* Line 1455 of yacc.c  */
#line 2093 "vlgParser.y"
    {fprintf(fp_out,"\n\t if(%s) \n\t  begin \n\t %s ", (yyvsp[(3) - (3)].id), get_next_inst_statement());;}
    break;

  case 316:

/* Line 1455 of yacc.c  */
#line 2093 "vlgParser.y"
    {fprintf(fp_out,"\n\t  end");;}
    break;

  case 317:

/* Line 1455 of yacc.c  */
#line 2094 "vlgParser.y"
    {
            cfg_temp = build_blank_node();
            cfg_top = build_normal_node((yyvsp[(3) - (7)].id),(yyvsp[(6) - (7)].NODE),NULL,cfg_temp,NULL);
            cfg_top->node_type = IFCOND_EXP;
            set_next_node((yyvsp[(6) - (7)].NODE),cfg_temp);
            (yyval.NODE)=cfg_top;
          ;}
    break;

  case 318:

/* Line 1455 of yacc.c  */
#line 2104 "vlgParser.y"
    {
          ;}
    break;

  case 319:

/* Line 1455 of yacc.c  */
#line 2110 "vlgParser.y"
    {
            (yyval.NODE)=(yyvsp[(1) - (1)].NODE);
	    YYTRACE("case_item_eclr: case_item");
          ;}
    break;

  case 320:

/* Line 1455 of yacc.c  */
#line 2115 "vlgParser.y"
    {
            cfg_temp = (yyvsp[(1) - (2)].NODE);
            while(cfg_temp->brother_node!=NULL)
             cfg_temp = cfg_temp->brother_node; 
            cfg_temp->brother_node = (yyvsp[(2) - (2)].NODE); 
            (yyval.NODE)=(yyvsp[(1) - (2)].NODE);
	    //sprintf ($$,"%s %s", $1,$2);
	    YYTRACE("case_item_eclr: case_item_eclr case_item");
          ;}
    break;

  case 321:

/* Line 1455 of yacc.c  */
#line 2128 "vlgParser.y"
    {fprintf(fp_out, "\n\t  %s: \n\t    begin \n\t   %s", (yyvsp[(1) - (2)].id),get_next_inst_statement());;}
    break;

  case 322:

/* Line 1455 of yacc.c  */
#line 2128 "vlgParser.y"
    {fprintf(fp_out,"\n\t  end");;}
    break;

  case 323:

/* Line 1455 of yacc.c  */
#line 2129 "vlgParser.y"
    {
            cfg_temp = build_normal_node((yyvsp[(1) - (5)].id),NULL,NULL,(yyvsp[(4) - (5)].NODE),NULL);
            cfg_temp->node_type = CASE_EXP;
            (yyval.NODE)=cfg_temp;
            //while(cfg_temp!=NULL){
            // printf("^^^the case next st is %s \n",cfg_temp->exp_name);
            // cfg_temp=cfg_temp->next_node;
            //}
            //printf("=====================");
	    YYTRACE("case_item: expression_list ':' statement");
          ;}
    break;

  case 324:

/* Line 1455 of yacc.c  */
#line 2140 "vlgParser.y"
    {fprintf(fp_out, "\n\t  default: \n\t    begin \n\t %s", get_next_inst_statement());;}
    break;

  case 325:

/* Line 1455 of yacc.c  */
#line 2140 "vlgParser.y"
    {fprintf(fp_out,"\n\t  end");;}
    break;

  case 326:

/* Line 1455 of yacc.c  */
#line 2142 "vlgParser.y"
    {
            cfg_temp = build_normal_node("default",NULL,NULL,(yyvsp[(4) - (5)].NODE),NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            (yyval.NODE)=cfg_temp;
            YYTRACE("case_item: YYDEFAULT ':' statement");
          ;}
    break;

  case 327:

/* Line 1455 of yacc.c  */
#line 2149 "vlgParser.y"
    {
            cfg_temp = build_normal_node("default",NULL,NULL,(yyvsp[(2) - (2)].NODE),NULL);
            cfg_temp->node_type = CASE_DEFAULT;
            (yyval.NODE)=cfg_temp;
	    YYTRACE("case_item: YYDEFAULT statement");
          ;}
    break;

  case 328:

/* Line 1455 of yacc.c  */
#line 2158 "vlgParser.y"
    {fprintf(fp_out, "\n\t begin");;}
    break;

  case 329:

/* Line 1455 of yacc.c  */
#line 2159 "vlgParser.y"
    {             
	    //sprintf ($$,"(%s)", $2);
            (yyval.NODE)=(yyvsp[(3) - (4)].NODE);
            fprintf(fp_out, "\n\t end");
            //printf("=dedece to a seq block: %s\n",$$->exp_name);
            //printf("=next is: %s\n",$$->next_node->exp_name);
            //printf("=next is: %s\n",$$->next_node->next_node->exp_name);
	    YYTRACE("seq_block: YYBEGIN statement_clr YYEND");
          ;}
    break;

  case 330:

/* Line 1455 of yacc.c  */
#line 2169 "vlgParser.y"
    {
	    //sprintf ($$,"(|%s| (%s))", $3, $5);
            (yyval.NODE)=NULL;
	    YYTRACE("seq_block: YYBEGIN ':' name_of_block block_declaration_clr statement_clr YYEND");
          ;}
    break;

  case 331:

/* Line 1455 of yacc.c  */
#line 2178 "vlgParser.y"
    {
              YYTRACE("par_block: YYFORK statement_clr YYJOIN");
          ;}
    break;

  case 332:

/* Line 1455 of yacc.c  */
#line 2182 "vlgParser.y"
    {
              YYTRACE("par_block: YYFORK ':' name_of_block block_declaration_clr statement_clr YYJOIN");
          ;}
    break;

  case 333:

/* Line 1455 of yacc.c  */
#line 2189 "vlgParser.y"
    {
	    sprintf ((yyval.id), "%s", (yyvsp[(1) - (1)].id));
	    YYTRACE("name_of_block: YYLID");
          ;}
    break;

  case 334:

/* Line 1455 of yacc.c  */
#line 2197 "vlgParser.y"
    {
              YYTRACE("block_declaration_clr:");
          ;}
    break;

  case 335:

/* Line 1455 of yacc.c  */
#line 2201 "vlgParser.y"
    {
              YYTRACE("block_declaration_clr: block_declaration_clr block_declaration");
          ;}
    break;

  case 336:

/* Line 1455 of yacc.c  */
#line 2208 "vlgParser.y"
    {
              YYTRACE("block_declaration: parameter_declaration");
          ;}
    break;

  case 337:

/* Line 1455 of yacc.c  */
#line 2212 "vlgParser.y"
    {
              YYTRACE("block_declaration: reg_declaration");
          ;}
    break;

  case 338:

/* Line 1455 of yacc.c  */
#line 2216 "vlgParser.y"
    {
              YYTRACE("block_declaration: integer_declaration");
          ;}
    break;

  case 339:

/* Line 1455 of yacc.c  */
#line 2220 "vlgParser.y"
    {
              YYTRACE("block_declaration: real_declaration");
          ;}
    break;

  case 340:

/* Line 1455 of yacc.c  */
#line 2224 "vlgParser.y"
    {
              YYTRACE("block_delcaration: time_declaration");
          ;}
    break;

  case 341:

/* Line 1455 of yacc.c  */
#line 2228 "vlgParser.y"
    {
              YYTRACE("block_declaration: event_declaration");
          ;}
    break;

  case 342:

/* Line 1455 of yacc.c  */
#line 2236 "vlgParser.y"
    {
	  ;}
    break;

  case 343:

/* Line 1455 of yacc.c  */
#line 2239 "vlgParser.y"
    {
              YYTRACE("task_enable: YYLID ';'");
          ;}
    break;

  case 344:

/* Line 1455 of yacc.c  */
#line 2243 "vlgParser.y"
    {
	  ;}
    break;

  case 345:

/* Line 1455 of yacc.c  */
#line 2246 "vlgParser.y"
    {
              YYTRACE("task_enable: YYLID '(' expression_list ')' ';'");
          ;}
    break;

  case 346:

/* Line 1455 of yacc.c  */
#line 2253 "vlgParser.y"
    {
              YYTRACE("system_task_enable: name_of_system_task ';'");
          ;}
    break;

  case 347:

/* Line 1455 of yacc.c  */
#line 2257 "vlgParser.y"
    {
              YYTRACE("system_task_enable: name_of_system_task '(' expression_list ')'");
          ;}
    break;

  case 348:

/* Line 1455 of yacc.c  */
#line 2264 "vlgParser.y"
    {
              YYTRACE("name_of_system_task: system_identifier");
          ;}
    break;

  case 349:

/* Line 1455 of yacc.c  */
#line 2273 "vlgParser.y"
    {
              YYTRACE("specify_block: YYSPECIFY specify_item_clr YYENDSPECIFY");
          ;}
    break;

  case 363:

/* Line 1455 of yacc.c  */
#line 2312 "vlgParser.y"
    {;}
    break;

  case 364:

/* Line 1455 of yacc.c  */
#line 2313 "vlgParser.y"
    {;}
    break;

  case 365:

/* Line 1455 of yacc.c  */
#line 2314 "vlgParser.y"
    {;}
    break;

  case 372:

/* Line 1455 of yacc.c  */
#line 2336 "vlgParser.y"
    {
	  ;}
    break;

  case 390:

/* Line 1455 of yacc.c  */
#line 2406 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("lvalue: YYLID");
	  ;}
    break;

  case 391:

/* Line 1455 of yacc.c  */
#line 2411 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s[%s]",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("lvalue: YYLID '[' expression ']'");
	  ;}
    break;

  case 392:

/* Line 1455 of yacc.c  */
#line 2416 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s[%s:%s]",(yyvsp[(1) - (6)].id),(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id));
	    YYTRACE("lvalue: YYLID'[' expression ':' expression ']'");
	  ;}
    break;

  case 393:

/* Line 1455 of yacc.c  */
#line 2421 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("lvalue: concatenation");
	  ;}
    break;

  case 394:

/* Line 1455 of yacc.c  */
#line 2429 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("mintypmax_expression_list: mintypmax_expression");
	  ;}
    break;

  case 395:

/* Line 1455 of yacc.c  */
#line 2434 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s %s", (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("mintypmax_expression_list: mintypmax_expression_list ',' mintypmax_expression");
	  ;}
    break;

  case 396:

/* Line 1455 of yacc.c  */
#line 2442 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("mintypmax_expression: expression");
	  ;}
    break;

  case 397:

/* Line 1455 of yacc.c  */
#line 2447 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(: %s %s)", (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("mintypmax_expression: expression ':' expression");
	  ;}
    break;

  case 398:

/* Line 1455 of yacc.c  */
#line 2452 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(: %s %s %s)", (yyvsp[(1) - (5)].id), (yyvsp[(3) - (5)].id),(yyvsp[(5) - (5)].id));
	    YYTRACE("mintypmax_expression: expression ':' expression ':' expression");
	  ;}
    break;

  case 399:

/* Line 1455 of yacc.c  */
#line 2460 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expression_list: expression");
	  ;}
    break;

  case 400:

/* Line 1455 of yacc.c  */
#line 2465 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s,%s", (yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression_list: expression_list ',' expression");
	  ;}
    break;

  case 401:

/* Line 1455 of yacc.c  */
#line 2473 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expression: primary");
	  ;}
    break;

  case 402:

/* Line 1455 of yacc.c  */
#line 2478 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(uplus %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '+' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 403:

/* Line 1455 of yacc.c  */
#line 2483 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(-%s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '-' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 404:

/* Line 1455 of yacc.c  */
#line 2488 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(! %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '!' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 405:

/* Line 1455 of yacc.c  */
#line 2493 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(~ %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '~' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 406:

/* Line 1455 of yacc.c  */
#line 2498 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(& %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '&' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 407:

/* Line 1455 of yacc.c  */
#line 2503 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(| %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '|' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 408:

/* Line 1455 of yacc.c  */
#line 2508 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(^ %s,",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: '^' primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 409:

/* Line 1455 of yacc.c  */
#line 2513 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(~& %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: YYLOGNAND primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 410:

/* Line 1455 of yacc.c  */
#line 2518 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(~| %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: YYLOGNOR primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 411:

/* Line 1455 of yacc.c  */
#line 2523 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(~^ %s)",(yyvsp[(2) - (2)].id));
	    YYTRACE("expression: YYLOGXNOR primary %prec YYUNARYOPERATOR");
	  ;}
    break;

  case 412:

/* Line 1455 of yacc.c  */
#line 2528 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s + %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '+' expression");
	  ;}
    break;

  case 413:

/* Line 1455 of yacc.c  */
#line 2533 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s - %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expressio '-' expression");
	  ;}
    break;

  case 414:

/* Line 1455 of yacc.c  */
#line 2538 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s * %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '*' expression");
	  ;}
    break;

  case 415:

/* Line 1455 of yacc.c  */
#line 2543 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s / %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '/' expression");
	  ;}
    break;

  case 416:

/* Line 1455 of yacc.c  */
#line 2548 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s %% %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '%' expression");
	  ;}
    break;

  case 417:

/* Line 1455 of yacc.c  */
#line 2553 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s == %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLOgEQUALITY expression");
	  ;}
    break;

  case 418:

/* Line 1455 of yacc.c  */
#line 2558 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s != %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLOGINEQUALITY expression");
	  ;}
    break;

  case 419:

/* Line 1455 of yacc.c  */
#line 2563 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s === %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYCASEEQUALITY expression");
	  ;}
    break;

  case 420:

/* Line 1455 of yacc.c  */
#line 2568 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s !== %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYCASEINEQUALITY expression");
	  ;}
    break;

  case 421:

/* Line 1455 of yacc.c  */
#line 2573 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s && %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLOGAND expression");
	  ;}
    break;

  case 422:

/* Line 1455 of yacc.c  */
#line 2578 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s || %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLOGOR expression");
	  ;}
    break;

  case 423:

/* Line 1455 of yacc.c  */
#line 2583 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s < %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '<' expression");
	  ;}
    break;

  case 424:

/* Line 1455 of yacc.c  */
#line 2588 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s > %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '>' expression");
	  ;}
    break;

  case 425:

/* Line 1455 of yacc.c  */
#line 2593 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s & %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '&' expression");
	  ;}
    break;

  case 426:

/* Line 1455 of yacc.c  */
#line 2598 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s | %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '|' expression");
	  ;}
    break;

  case 427:

/* Line 1455 of yacc.c  */
#line 2603 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s ^ %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression '^' expression");
	  ;}
    break;

  case 428:

/* Line 1455 of yacc.c  */
#line 2608 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s =< %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLEQ expression");
	  ;}
    break;

  case 429:

/* Line 1455 of yacc.c  */
#line 2613 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s <= %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLEQ expression");
	  ;}
    break;

  case 430:

/* Line 1455 of yacc.c  */
#line 2618 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s >= %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYGEQ expression");
	  ;}
    break;

  case 431:

/* Line 1455 of yacc.c  */
#line 2623 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s << %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLSHIFT expression");
	  ;}
    break;

  case 432:

/* Line 1455 of yacc.c  */
#line 2628 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s >> %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYRSHIFT expression");
	  ;}
    break;

  case 433:

/* Line 1455 of yacc.c  */
#line 2633 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(%s ^~ %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("expression: expression YYLOGXNOR expression");
	  ;}
    break;

  case 434:

/* Line 1455 of yacc.c  */
#line 2638 "vlgParser.y"
    {
	    sprintf ((yyval.id),"( %s ? %s : %s)",(yyvsp[(1) - (5)].id),(yyvsp[(3) - (5)].id),(yyvsp[(5) - (5)].id));
	    YYTRACE("expression: expression '?' expression ':' expression");
	  ;}
    break;

  case 435:

/* Line 1455 of yacc.c  */
#line 2643 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("expression: YYSTRING");
	  ;}
    break;

  case 436:

/* Line 1455 of yacc.c  */
#line 2651 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("primary: YYINUMBER");
	  ;}
    break;

  case 437:

/* Line 1455 of yacc.c  */
#line 2656 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	      YYTRACE("primary: YYINUMBER_BIT");
	  ;}
    break;

  case 438:

/* Line 1455 of yacc.c  */
#line 2661 "vlgParser.y"
    {
	    sprintf ((yyval.id),(yyvsp[(1) - (1)].id));
	      YYTRACE("primary: YYRNUMBER");
	  ;}
    break;

  case 439:

/* Line 1455 of yacc.c  */
#line 2666 "vlgParser.y"
    {
	    sprintf ((yyval.id),get_wwt_entry((yyvsp[(1) - (1)].id)));
            //check whether the identifier is the parameter
            int idx_temp;
            if(parameter_start==false)
            {
             // int para_table_idx=0;
             //_para_table current_para_table[MAXPARAS]; 
              for(idx_temp=0;idx_temp<para_table_idx;idx_temp++)
              {
                if(strcmp((yyvsp[(1) - (1)].id),current_para_table[idx_temp].para_name)==0)
                {
                  sprintf((yyval.id),"%s", current_para_table[idx_temp].para_value);
                  printf("\n substitute the parameter %s with value %s\n",current_para_table[idx_temp].para_name,(yyval.id));  
                 }
              }   
            }
	    YYTRACE("primary: identifier");
	  ;}
    break;

  case 440:

/* Line 1455 of yacc.c  */
#line 2686 "vlgParser.y"
    {
	    sprintf ((yyval.id)," %s[%s]",(yyvsp[(1) - (4)].id),(yyvsp[(3) - (4)].id));
	    YYTRACE("primary: identifier '[' expression ']'");
	  ;}
    break;

  case 441:

/* Line 1455 of yacc.c  */
#line 2691 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s[%s:%s]",(yyvsp[(1) - (6)].id),(yyvsp[(3) - (6)].id),(yyvsp[(5) - (6)].id));
	    YYTRACE("primary: identifier '[' expression ':' expression ']'");
	  ;}
    break;

  case 442:

/* Line 1455 of yacc.c  */
#line 2696 "vlgParser.y"
    {
	    strcpy ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("primary: concatenation");
	  ;}
    break;

  case 443:

/* Line 1455 of yacc.c  */
#line 2701 "vlgParser.y"
    {
	    strcpy ((yyval.id),(yyvsp[(1) - (1)].id));
	    YYTRACE("primary: multiple_concatenatin");
	  ;}
    break;

  case 444:

/* Line 1455 of yacc.c  */
#line 2706 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("primary: function_call");
	  ;}
    break;

  case 445:

/* Line 1455 of yacc.c  */
#line 2711 "vlgParser.y"
    {
	    strcpy ((yyval.id),(yyvsp[(2) - (3)].id));
	    YYTRACE("primary: '(' mintypmax_expression ')'");
	  ;}
    break;

  case 446:

/* Line 1455 of yacc.c  */
#line 2717 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	    YYTRACE("primary: YYsysID '(' nondeterminism_list ')'");
	  ;}
    break;

  case 447:

/* Line 1455 of yacc.c  */
#line 2722 "vlgParser.y"
    {
	    strcpy ((yyval.id),"");
	  ;}
    break;

  case 448:

/* Line 1455 of yacc.c  */
#line 2729 "vlgParser.y"
    {
	      YYTRACE("nondeterminism_list : event_control");
	  ;}
    break;

  case 449:

/* Line 1455 of yacc.c  */
#line 2733 "vlgParser.y"
    {
	      YYTRACE("nondeterminism_list : expression");
	  ;}
    break;

  case 450:

/* Line 1455 of yacc.c  */
#line 2737 "vlgParser.y"
    {
	      YYTRACE("nondeterminism_list : nondeterminism_list ',' event_control");
	  ;}
    break;

  case 451:

/* Line 1455 of yacc.c  */
#line 2741 "vlgParser.y"
    {
	      YYTRACE("nondeterminism_list : nondeterminism_list ',' expression");
	  ;}
    break;

  case 452:

/* Line 1455 of yacc.c  */
#line 2750 "vlgParser.y"
    {
	    sprintf ((yyval.id),"{%s}",(yyvsp[(2) - (3)].id));
	    YYTRACE("concatenation: '{' expression_list '}'");
	  ;}
    break;

  case 453:

/* Line 1455 of yacc.c  */
#line 2758 "vlgParser.y"
    {
	      sprintf ((yyval.id),"(concatenation %s (concatenation %s))",(yyvsp[(2) - (6)].id),(yyvsp[(4) - (6)].id));
	      YYTRACE("multiple_concatenation: '{' expression '{' expression_list '}' '}'");
	    ;}
    break;

  case 454:

/* Line 1455 of yacc.c  */
#line 2766 "vlgParser.y"
    {
	      YYTRACE("function_call: identifier '(' expression_list ')'");
	  ;}
    break;

  case 456:

/* Line 1455 of yacc.c  */
#line 2780 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s",(yyvsp[(1) - (1)].id));
	    YYTRACE("identifier: YYLID");
          ;}
    break;

  case 457:

/* Line 1455 of yacc.c  */
#line 2785 "vlgParser.y"
    {
	    sprintf ((yyval.id),"(element %s %s)",(yyvsp[(1) - (3)].id),(yyvsp[(3) - (3)].id));
	    YYTRACE("identifier: identifier '.' YYLID");
          ;}
    break;

  case 458:

/* Line 1455 of yacc.c  */
#line 2793 "vlgParser.y"
    {
	      YYTRACE("delay_opt:");
	  ;}
    break;

  case 459:

/* Line 1455 of yacc.c  */
#line 2797 "vlgParser.y"
    {
	      YYTRACE("delay_opt: delay");
	  ;}
    break;

  case 460:

/* Line 1455 of yacc.c  */
#line 2804 "vlgParser.y"
    {
	      YYTRACE("delay: '#' YYINUMBER");
	  ;}
    break;

  case 461:

/* Line 1455 of yacc.c  */
#line 2808 "vlgParser.y"
    {
	      YYTRACE("delay: '#' YYINUMBER_BIT");
	  ;}
    break;

  case 462:

/* Line 1455 of yacc.c  */
#line 2812 "vlgParser.y"
    {
	      YYTRACE("delay: '#' YYRNUMBER");
	  ;}
    break;

  case 463:

/* Line 1455 of yacc.c  */
#line 2816 "vlgParser.y"
    {
	      YYTRACE("delay: '#' identifier");
	  ;}
    break;

  case 464:

/* Line 1455 of yacc.c  */
#line 2820 "vlgParser.y"
    {
	      YYTRACE("delay: '#' '(' mintypmax_expression ')'");
	  ;}
    break;

  case 465:

/* Line 1455 of yacc.c  */
#line 2824 "vlgParser.y"
    {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ')'");
	  ;}
    break;

  case 466:

/* Line 1455 of yacc.c  */
#line 2829 "vlgParser.y"
    {
	      YYTRACE("delay: '#' '(' mintypmax_expression ',' mintypmax_expression ',' mintypmax_expression ')'");
	  ;}
    break;

  case 467:

/* Line 1455 of yacc.c  */
#line 2836 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' YYINUMBER");
	  ;}
    break;

  case 468:

/* Line 1455 of yacc.c  */
#line 2840 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' YYINUMBER_BIT");
	  ;}
    break;

  case 469:

/* Line 1455 of yacc.c  */
#line 2844 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' YYRNUMBER");
	  ;}
    break;

  case 470:

/* Line 1455 of yacc.c  */
#line 2848 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' identifier");
	  ;}
    break;

  case 471:

/* Line 1455 of yacc.c  */
#line 2852 "vlgParser.y"
    {
	      YYTRACE("delay_control: '#' '(' mintypmax_expression ')'");
	  ;}
    break;

  case 472:

/* Line 1455 of yacc.c  */
#line 2859 "vlgParser.y"
    {
	    sprintf ((yyval.id),"always @ (%s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("event_control: '@' identifier");
	  ;}
    break;

  case 473:

/* Line 1455 of yacc.c  */
#line 2864 "vlgParser.y"
    {
	    sprintf ((yyval.id),"always @ (*)");
	    YYTRACE("event_control: '@' (*)");
          ;}
    break;

  case 474:

/* Line 1455 of yacc.c  */
#line 2869 "vlgParser.y"
    {
	    sprintf ((yyval.id),"always @ (%s) ", (yyvsp[(3) - (4)].id));
	    YYTRACE("event_control: '@' '(' event_expression ')'");
	  ;}
    break;

  case 475:

/* Line 1455 of yacc.c  */
#line 2874 "vlgParser.y"
    {
	    sprintf ((yyval.id),"always @ (%s)", (yyvsp[(3) - (4)].id));
	    YYTRACE("event_control: '@' '(' ored_event_expression ')'");
	  ;}
    break;

  case 476:

/* Line 1455 of yacc.c  */
#line 2883 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(%s)", (yyvsp[(1) - (1)].id));
	    YYTRACE("event_expression: expression");
	  ;}
    break;

  case 477:

/* Line 1455 of yacc.c  */
#line 2888 "vlgParser.y"
    {
	    sprintf ((yyval.id), "posedge %s", (yyvsp[(2) - (2)].id));
	    YYTRACE("event_expression: YYPOSEDGE expression");
	  ;}
    break;

  case 478:

/* Line 1455 of yacc.c  */
#line 2893 "vlgParser.y"
    {
	    sprintf ((yyval.id), "negedge %s", (yyvsp[(2) - (2)].id));
	    YYTRACE("event_expression: YYNEGEDGE expression");
	  ;}
    break;

  case 479:

/* Line 1455 of yacc.c  */
#line 2898 "vlgParser.y"
    {
	    sprintf ((yyval.id), "(edge %s)", (yyvsp[(2) - (2)].id));
	    YYTRACE("event_expression: YYEDGE expression");
          ;}
    break;

  case 480:

/* Line 1455 of yacc.c  */
#line 2906 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s or %s", (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("ored_event_expression: event_expression YYOR event_expression");
            //printf("\nored event expression1\n");
	  ;}
    break;

  case 481:

/* Line 1455 of yacc.c  */
#line 2912 "vlgParser.y"
    {
	    sprintf ((yyval.id),"%s or %s", (yyvsp[(1) - (3)].id), (yyvsp[(3) - (3)].id));
	    YYTRACE("ored_event_expression: ored_event_expression YYOR event_expression");
            //printf("\nored event expression2\n");
	  ;}
    break;



/* Line 1455 of yacc.c  */
#line 7379 "vlgParser.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 2920 "vlgParser.y"


void yyerror(char * str)
{
    fprintf(stderr, str);
    exit (1);
}

void YYTRACE(char * str)
{
}

_modulestats * make_module (char *name) {
  _modulestats *newmodule;

  if (modstats == NULL) {
    modstats = (_modulestats *) malloc (sizeof (struct __modulestats));
    modstats->next = NULL;
    modstats->numports = 0;
    modstats->numinputs = 0;
    modstats->numoutputs = 0;
    modstats->numregs = 0;
    modstats->numwires = 0;
    return modstats;
  }
  newmodule = modstats;
  while (newmodule->next != NULL) newmodule = newmodule->next;
  newmodule->next = (_modulestats *) malloc (sizeof (struct __modulestats));
  newmodule->next->next = NULL;
  newmodule->next->numports = 0;
  newmodule->next->numinputs = 0;
  newmodule->next->numoutputs = 0;
  newmodule->next->numregs = 0;
  newmodule->next->numwires = 0;
  return newmodule->next;
}

char *get_string_from_var (_var *v) {
  char *outstr;
  int i;
  outstr = (char *) malloc (MAXSTRLEN*(v->end-v->start+1));
  if (v->end-v->start) {
    strcpy (outstr,"");
    for (i=v->start; i<=v->end; i++)
      sprintf (outstr, "%s |%s%d|", outstr, v->name, i);
  } 
  else sprintf (outstr,v->name);
  return outstr;
}

char *get_string_from_varlist (_var **v, int n) {
  char *retstr;
  int i;
  retstr = NULL;
  if (n<=0) return "";
  sprintf (retstr,get_string_from_var(v[0]));
  for (i=1; i<n; i++)
    sprintf (retstr, "%s%s", retstr, get_string_from_var(v[i]));
  return retstr;
}

void print_modstats (void) {
  fprintf (stdout, "\t   (ins  %s)\n", get_string_from_varlist (thismodule->input_list, thismodule->numinputs));
  fprintf (stdout, "\t   (sts  %s)\n", get_string_from_varlist (thismodule->reg_list, thismodule->numregs));
  fprintf (stdout, "\t   (outs  %s)\n", get_string_from_varlist (thismodule->output_list, thismodule->numoutputs));
  fprintf (stdout, "\t   (wires  %s)\n", get_string_from_varlist (thismodule->wire_list, thismodule->numwires));
  fprintf (stdout, "\t   (occs  %s)\n","");
}

void put_wwt_entry (char *name, char *range) {
  struct wire_width_tuple t;
  strcpy (t.name, name);
  strcpy (t.range, range);
  wire_width_table[num_width_table_entries++] = t;
  return;
}

char *get_wwt_entry (char *name) {
  int i;
  char *rstr = (char *) malloc (MAXSTRLEN);
  for (i=0; i<num_width_table_entries; i++) 
    if (strcmp (wire_width_table[i].name, name)==0) {
      //sprintf (rstr, "(%s %s)", wire_width_table[i].range, wire_width_table[i].name);
      sprintf (rstr, " %s", wire_width_table[i].name);
      return rstr;
    }
  return name;
}

void print_wwt (void) {
  printf ("\n");printf ("\n");
  for (int i=0; i<num_width_table_entries; i++) 
    printf ("(%s %s)\n", wire_width_table[i].name, wire_width_table[i].range);
  printf ("\n");
}

//============================================
//============for building CFG================
//===========liu187@illinois.edu==============
_cfg_node * build_blank_node( )
{ 
  _cfg_node * temp;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, "blank");
  temp->node_type = NULL_NODE;
  temp->left_node = NULL;
  temp->right_node = NULL;
  temp->brother_node = NULL;
  temp->next_node = NULL;
  return temp;
}

_cfg_node * build_normal_node(char exp[], _cfg_node * left, _cfg_node * right, _cfg_node * next, _cfg_node * brother)
{
  _cfg_node * temp;
  temp = (_cfg_node *)malloc(sizeof(_cfg_node));
  strcpy(temp->exp_name, exp);
  temp->left_node = left;
  temp->right_node = right;
  temp->brother_node = brother;
  temp->next_node = next; 
  return temp;
}

int set_next_node(_cfg_node * cfg_x, _cfg_node * cfg_nxt)
{
  _cfg_node * temp;
  temp = cfg_x;
  while(temp->next_node!=NULL)
   temp = temp->next_node;
  temp->next_node = cfg_nxt;
  return 1;
}

  //int bran_var_idx=0;
char * get_next_inst_statement()
{
   int idx;
   char * p_next_inst_statement = (char *)malloc(50*sizeof(char));
   for (idx=0;idx<50;idx++)
     *(p_next_inst_statement+idx)='\0';
   sprintf(p_next_inst_statement,"branvar[%d] <= branvar[%d] + 1;",bran_var_idx, bran_var_idx);
   bran_var_idx++;
   return p_next_inst_statement;
}

//finish a cfg building
int push_cfg_table(_cfg_node * cfg_x)
{
  cfg_table[cfg_stack_pointer]=cfg_x;
  cfg_stack_pointer++;
  return 1;
}

void print_all()
{
  int i;
  for(i=0;i<cfg_stack_pointer;i++)
   print_cfg(cfg_table[i]);
}


void print_cfg(_cfg_node * cfg_x)
{
  _cfg_node * cfg_bfs[MAXNUMCFGS];
  int rd_pointer=0;
  int wr_pointer=0;
  cfg_bfs[wr_pointer++]=cfg_x;
  //make a breadth first search
  while(rd_pointer!=wr_pointer)
  {
    cfg_x = cfg_bfs[rd_pointer];
    rd_pointer=rd_pointer+1;
    rd_pointer=rd_pointer%100;
    
    printf("Node: %s\n",cfg_x->exp_name);
    if(cfg_x->left_node!=NULL){
      cfg_bfs[wr_pointer]=cfg_x->left_node;
      wr_pointer++;
      wr_pointer=wr_pointer%100;
      continue;
    }
    if(cfg_x->brother_node!=NULL){
      cfg_bfs[wr_pointer]=cfg_x->brother_node;
      wr_pointer++;
      wr_pointer=wr_pointer%100;
    }
    if(cfg_x->next_node!=NULL){
      cfg_bfs[wr_pointer]=cfg_x->next_node;
      wr_pointer++;
      wr_pointer=wr_pointer%100;
     }
  } 
}

