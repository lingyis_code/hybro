// b02.v
// Verilog version, rewritten by Li Shen, Aug 2002
// Institute of Computing Technology, Chinese Academy of Sciences
// The original VHDL version is b02.vhd from Politecnico di Torino

module b02 (clock,reset,linea,u);
input clock;
input reset;
input linea;
output u;
/////
wire clock;
wire reset;
wire linea;
reg u;
/////
parameter A=0, B=1, C=2, D=3, E=4, F=5, G=6;
reg [2:0] stato;
/////
always @(posedge clock)
begin
  if (reset)
  begin
    stato=A;
    u<=0;
  end
  else
    case (stato)
      A: begin
           stato=B; 
           u<=0;
	 end
      B: begin
           if (!linea) stato=C;
           else	       stato=F;
           u<=0;
	 end
      C: begin
           if (!linea) stato=D;
           else	       stato=G;
           u<=0;
	 end
      D: begin
           stato=E; 
           u<=0;
	 end
      E: begin
           stato=B;
           u<=1;
	 end
      F: begin
           stato=G;
           u<=0;
	 end
      G: begin
           if (!linea) stato=E;
           else	       stato=A;
           u<=0;
	 end
    endcase
end
endmodule
