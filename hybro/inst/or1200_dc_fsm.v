module or1200_dc_fsm(
	// Clock and reset
	clk, rst,

	// Internal i/f to top level DC
	dc_en, dcdmmu_cycstb_i, dcdmmu_ci_i, dcpu_we_i, dcpu_sel_i,
	tagcomp_miss, biudata_valid, biudata_error, start_addr, saved_addr,
	dcram_we, biu_read, biu_write, first_hit_ack, first_miss_ack, first_miss_err,
	burst, tag_we, dc_addr
);

//
// I/O
//
input				clk;
input				rst;
input				dc_en;
input				dcdmmu_cycstb_i;
input				dcdmmu_ci_i;
input				dcpu_we_i;
input	[3:0]			dcpu_sel_i;
input				tagcomp_miss;
input				biudata_valid;
input				biudata_error;
input	[31:0]			start_addr;
output	[31:0]			saved_addr;
output	[3:0]			dcram_we;
output				biu_read;
output				biu_write;
output				first_hit_ack;
output				first_miss_ack;
output				first_miss_err;
output				burst;
output				tag_we;
output	[31:0]			dc_addr;

//
// Internal wires and regs
//
reg	[31:0]			saved_addr_r;
reg	[2:0]			state;
reg	[2:0]			cnt;
reg				hitmiss_eval;
reg				store;
reg				load;
reg				cache_inhibit;
wire				first_store_hit_ack;

parameter OR1200_DCFSM_IDLE	=0;
parameter OR1200_DCFSM_CLOAD	=1;
parameter OR1200_DCFSM_LREFILL3	=2;
parameter OR1200_DCFSM_CSTORE	=3;
parameter OR1200_DCFSM_SREFILL4	=4;

//
// Main DC FSM
//
always @(posedge clk or posedge rst) begin
	if (rst) begin
		state <=   OR1200_DCFSM_IDLE;
		saved_addr_r <=   32'b0;
		hitmiss_eval <=   1'b0;
		store <=   1'b0;
		load <=   1'b0;
		cnt <=   3'b000;
		cache_inhibit <=   1'b0;
	end
	else
	case (state)	// synopsys parallel_case
		OR1200_DCFSM_IDLE :
			if (dc_en & dcdmmu_cycstb_i & dcpu_we_i) begin	// store
				state <=   OR1200_DCFSM_CSTORE;
				saved_addr_r <=   start_addr;
				hitmiss_eval <=   1'b1;
				store <=   1'b1;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else if (dc_en & dcdmmu_cycstb_i) begin		// load
				state <=   OR1200_DCFSM_CLOAD;
				saved_addr_r <=   start_addr;
				hitmiss_eval <=   1'b1;
				store <=   1'b0;
				load <=   1'b1;
				cache_inhibit <=   1'b0;
			end
			else begin							// idle
				hitmiss_eval <=   1'b0;
				store <=   1'b0;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
		OR1200_DCFSM_CLOAD: begin		// load
			if (dcdmmu_cycstb_i & dcdmmu_ci_i)
				cache_inhibit <=   1'b1;
			if (hitmiss_eval)
				saved_addr_r[31:13] <=   start_addr[31:13];
			if ((hitmiss_eval & !dcdmmu_cycstb_i) ||					// load aborted (usually caused by DMMU)
			    (biudata_error) ||										// load terminated with an error
			    ((cache_inhibit | dcdmmu_ci_i) & biudata_valid)) begin	// load from cache-inhibited area
				state <=   OR1200_DCFSM_IDLE;
				hitmiss_eval <=   1'b0;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else if (tagcomp_miss & biudata_valid) begin	// load missed, finish current external load and refill
				state <=   OR1200_DCFSM_LREFILL3;
				saved_addr_r[3:2] <=   saved_addr_r[3:2] + 'd1;
				hitmiss_eval <=   1'b0;
				cnt <=   2;
				cache_inhibit <=   1'b0;
			end
			else if (!tagcomp_miss & !dcdmmu_ci_i) begin	// load hit, finish immediately
				state <=   OR1200_DCFSM_IDLE;
				hitmiss_eval <=   1'b0;
				load <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else						// load in-progress
				hitmiss_eval <=   1'b0;
		end
		OR1200_DCFSM_LREFILL3 : begin
			if (biudata_valid && (cnt[2]||cnt[1]||cnt[0])) begin		// refill ack, more loads to come
				cnt <=   cnt - 1;
				saved_addr_r[3:2] <=   saved_addr_r[3:2] + 'd1;
			end
			else if (biudata_valid) begin			// last load of line refill
				state <=   OR1200_DCFSM_IDLE;
				load <=   1'b0;
			end
		end
		OR1200_DCFSM_CSTORE: begin		// store
			if (dcdmmu_cycstb_i & dcdmmu_ci_i)
				cache_inhibit <=   1'b1;
			if (hitmiss_eval)
				saved_addr_r[31:13] <=   start_addr[31:13];
			if ((hitmiss_eval & !dcdmmu_cycstb_i) ||	// store aborted (usually caused by DMMU)
			    (biudata_error) ||						// store terminated with an error
			    ((cache_inhibit | dcdmmu_ci_i) & biudata_valid)) begin	// store to cache-inhibited area
				state <=   OR1200_DCFSM_IDLE;
				hitmiss_eval <=   1'b0;
				store <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else if (tagcomp_miss & biudata_valid) begin	// store missed, finish write-through and do load refill
				state <=   OR1200_DCFSM_SREFILL4;
				hitmiss_eval <=   1'b0;
				store <=   1'b0;
				load <=   1'b1;
				cnt <=   3;
				cache_inhibit <=   1'b0;
			end
			else if (biudata_valid) begin			// store hit, finish write-through
				state <=   OR1200_DCFSM_IDLE;
				hitmiss_eval <=   1'b0;
				store <=   1'b0;
				cache_inhibit <=   1'b0;
			end
			else						// store write-through in-progress
				hitmiss_eval <=   1'b0;
			end
		OR1200_DCFSM_SREFILL4 : begin
			if (biudata_valid && (cnt[2]||cnt[1]||cnt[0])) begin		// refill ack, more loads to come
				cnt <=   cnt - 1;
				saved_addr_r[3:2] <=   saved_addr_r[3:2] + 'd1;
			end
			else if (biudata_valid) begin			// last load of line refill
				state <=   OR1200_DCFSM_IDLE;
				load <=   1'b0;
			end
		end
	endcase
end

endmodule
