
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     YYLID = 258,
     YYINUMBER = 259,
     YYINUMBER_BIT = 260,
     YYRNUMBER = 261,
     YYSTRING = 262,
     YYALLPATH = 263,
     YYALWAYS = 264,
     YYAND = 265,
     YYASSIGN = 266,
     YYBEGIN = 267,
     YYBUF = 268,
     YYBUFIF0 = 269,
     YYBUFIF1 = 270,
     YYCASE = 271,
     YYCASEX = 272,
     YYCASEZ = 273,
     YYCMOS = 274,
     YYCONDITIONAL = 275,
     YYDEASSIGN = 276,
     YYDEFAULT = 277,
     YYDEFPARAM = 278,
     YYDISABLE = 279,
     YYEDGE = 280,
     YYEND = 281,
     YYENDCASE = 282,
     YYENDMODULE = 283,
     YYENDFUNCTION = 284,
     YYENDPRIMITIVE = 285,
     YYENDSPECIFY = 286,
     YYENDTABLE = 287,
     YYENDTASK = 288,
     YYENUM = 289,
     YYEVENT = 290,
     YYFOR = 291,
     YYFOREVER = 292,
     YYFORK = 293,
     YYFUNCTION = 294,
     YYGEQ = 295,
     YYHIGHZ0 = 296,
     YYHIGHZ1 = 297,
     YYIF = 298,
     YYELSE = 299,
     YYLOWERTHANELSE = 300,
     YYINITIAL = 301,
     YYINOUT = 302,
     YYINPUT = 303,
     YYINTEGER = 304,
     YYJOIN = 305,
     YYLARGE = 306,
     YYLEADTO = 307,
     YYLEQ = 308,
     YYLOGAND = 309,
     YYCASEEQUALITY = 310,
     YYCASEINEQUALITY = 311,
     YYLOGNAND = 312,
     YYLOGNOR = 313,
     YYLOGOR = 314,
     YYLOGXNOR = 315,
     YYLOGEQUALITY = 316,
     YYLOGINEQUALITY = 317,
     YYLSHIFT = 318,
     YYMACROMODULE = 319,
     YYMEDIUM = 320,
     YYMODULE = 321,
     YYMREG = 322,
     YYNAND = 323,
     YYNBASSIGN = 324,
     YYNEGEDGE = 325,
     YYNMOS = 326,
     YYNOR = 327,
     YYNOT = 328,
     YYNOTIF0 = 329,
     YYNOTIF1 = 330,
     YYOR = 331,
     YYOUTPUT = 332,
     YYPARAMETER = 333,
     YYPMOS = 334,
     YYPOSEDGE = 335,
     YYPRIMITIVE = 336,
     YYPULL0 = 337,
     YYPULL1 = 338,
     YYPULLUP = 339,
     YYPULLDOWN = 340,
     YYRCMOS = 341,
     YYREAL = 342,
     YYREG = 343,
     YYREPEAT = 344,
     YYRIGHTARROW = 345,
     YYRNMOS = 346,
     YYRPMOS = 347,
     YYRSHIFT = 348,
     YYRTRAN = 349,
     YYRTRANIF0 = 350,
     YYRTRANIF1 = 351,
     YYSCALARED = 352,
     YYSMALL = 353,
     YYSPECIFY = 354,
     YYSPECPARAM = 355,
     YYSTRONG0 = 356,
     YYSTRONG1 = 357,
     YYSUPPLY0 = 358,
     YYSUPPLY1 = 359,
     YYSWIRE = 360,
     YYTABLE = 361,
     YYTASK = 362,
     YYTESLATIMER = 363,
     YYTIME = 364,
     YYTRAN = 365,
     YYTRANIF0 = 366,
     YYTRANIF1 = 367,
     YYTRI = 368,
     YYTRI0 = 369,
     YYTRI1 = 370,
     YYTRIAND = 371,
     YYTRIOR = 372,
     YYTRIREG = 373,
     YYuTYPE = 374,
     YYTYPEDEF = 375,
     YYVECTORED = 376,
     YYWAIT = 377,
     YYWAND = 378,
     YYWEAK0 = 379,
     YYWEAK1 = 380,
     YYWHILE = 381,
     YYWIRE = 382,
     YYWOR = 383,
     YYXNOR = 384,
     YYXOR = 385,
     YYsysSETUP = 386,
     YYsysID = 387,
     YYsysND = 388,
     YYUNARYOPERATOR = 389
   };
#endif



#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 36 "vlgParser.y"

  char id[MAXSTRLEN];
  _cfg_node * NODE;



/* Line 1676 of yacc.c  */
#line 193 "vlgParser.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;


