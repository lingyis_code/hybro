
module b1000 (clock , reset , inst_in , instruction_valid , pc , srcA_tmp , srcB_tmp , srcC_tmp );

	  input  clock;
	  input  reset;
	  input [31 : 0] inst_in;
	  input  instruction_valid;
	  input [31 : 0] pc;
	  input [31 : 0] srcA_tmp;
	  input [31 : 0] srcB_tmp;
	  input [31 : 0] srcC_tmp;
	  reg [7:0] branvar[0:1000];
	  reg [31 : 0] pc_reg;
	  wire [31 : 0] pc;
	  wire [31 : 0] inst_in;
	  reg [31 : 0] instruction;
	  wire [31 : 0] srcA_tmp;
	  wire [31 : 0] srcB_tmp;
	  wire [31 : 0] srcC_tmp;
	  reg [31 : 0] srcA_tmp_dly;
	  reg [31 : 0] srcB_tmp_dly;
	  reg [31 : 0] srcC_tmp_dly;
	  wire [7 : 0] opcode_tmp;
	  wire [15 : 0] label_tmp_1;
	  wire [15 : 0] label_tmp_2;
	  reg [15 : 0] label_tmp;
	  wire [23 : 0] longlabel_tmp;
	  reg [32 : 0] branch_target_address;
	 assign 
	  opcode_tmp = instruction[31:24];
	 assign 
	  label_tmp_1 = instruction[15:0];
	 assign 
	  label_tmp_2 = ( ( 16'hffff -  label_tmp_1) + 1);
	 assign 
	  longlabel_tmp = instruction[23:0];
	 assign 
	  label_tmp = (  instruction[15] ? label_tmp2 : label_tmp1);

	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  pc_reg <= 32'h0;
	  branch_target_address <= 32'h0;
	  instruction <= 32'h0;
	  srcA_tmp_dly <= 32'h0;
	  srcB_tmp_dly <= 32'h0;
	  srcC_tmp_dly <= 32'h0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 if( instruction_valid) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  pc_reg <=  pc;
	  instruction <=  inst_in;
	  srcA_tmp_dly <=  srcA_tmp;
	  srcB_tmp_dly <=  srcB_tmp;
	  srcC_tmp_dly <=  srcC_tmp;
	 case( opcode_tmp)
	  8'h10: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	 if((  srcA_tmp_dly ==  srcC_tmp_dly)) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	  branch_target_address <= (  pc_reg +  label_tmp);
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	  branch_target_address <= (  pc_reg + 1);
	  end
	 end
	  end
	  8'h11: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 begin
	 if((  srcA_tmp_dly !=  srcC_tmp_dly)) 
	  begin 
	 branvar[7] <= branvar[7] + 1; 
	  branch_target_address <= (  pc_reg +  label_tmp);
	  end
	 else 
	   begin 
	   branvar[8] <= branvar[8] + 1;   
	  branch_target_address <= (  pc_reg + 1);
	  end
	 end
	  end
	  8'h12: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 begin
	 if((  srcA_tmp_dly <  srcC_tmp_dly)) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	  branch_target_address <= (  pc_reg +  label_tmp);
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	  branch_target_address <= (  pc_reg + 1);
	  end
	 end
	  end
	  8'h13: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	 begin
	 if(( srcA_tmp_dly <=  srcC_tmp_dly)) 
	  begin 
	 branvar[13] <= branvar[13] + 1; 
	  branch_target_address <= (  pc_reg +  label_tmp);
	  end
	 else 
	   begin 
	   branvar[14] <= branvar[14] + 1;   
	  branch_target_address <= (  pc_reg + 1);
	  end
	 end
	  end
	  8'h14: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	 begin
	 if((  srcA_tmp_dly >  srcC_tmp_dly)) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	  branch_target_address <= (  pc_reg +  label_tmp);
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	  branch_target_address <= (  pc_reg + 1);
	  end
	 end
	  end
	  8'h15: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	 begin
	 if(( srcC_tmp_dly <=  srcA_tmp_dly)) 
	  begin 
	 branvar[19] <= branvar[19] + 1; 
	  branch_target_address <= (  pc_reg +  label_tmp);
	  end
	 else 
	   begin 
	   branvar[20] <= branvar[20] + 1;   
	  branch_target_address <= (  pc_reg + 1);
	  end
	 end
	  end
	  8'h16: 
	    begin 
	   branvar[21] <= branvar[21] + 1;
	 begin
	  branch_target_address <=  longlabel_tmp;
	 end
	  end
	 endcase
	 end
	  end
	 else 
	   begin 
	   branvar[22] <= branvar[22] + 1;   
	 begin
	  pc_reg <=  pc_reg;
	  branch_target_address <=  branch_target_address;
	  instruction <=  instruction;
	  srcA_tmp_dly <=  srcA_tmp_dly;
	  srcB_tmp_dly <=  srcB_tmp_dly;
	  srcC_tmp_dly <=  srcC_tmp_dly;
	 end
	  end
	  end
	 end
	  endmodule 
