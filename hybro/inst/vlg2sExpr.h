#ifndef _VL2S_EXPR_H_
#define _VL2S_EXPR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#define MAXSTRLEN    8192
#define MAXBITNUM    32
#define MAXNUMVARS   8192
#define MAXNUMCFGS   128
#define MAXPARAS     200

extern int  yylineno;
extern char *yyfile;
extern char *yytext;

extern FILE *yyin;

extern int yylex ();
extern void yyerror (char *str);
extern int yyparse (void);

struct __errStruct {
  int errorNumber;
  char errString[1024];
};

struct __errStruct make_error (int, char *);
int printError (struct __errStruct);

extern char current_range[MAXSTRLEN];

struct wire_width_tuple {
  char name[MAXSTRLEN];
  char range[MAXSTRLEN];
};

extern struct wire_width_tuple wire_width_table[MAXNUMVARS];
extern int num_width_table_entries;

char *get_wwt_entry (char *name);
void put_wwt_entry (char *name, char *range);
void print_wwt (void);

typedef struct __var {
  char name[MAXSTRLEN];
  int start;
  int end;
} _var;

typedef struct __modulecall {
  char name[MAXSTRLEN];
  char calledmodulename[MAXSTRLEN];
  char *inlist[MAXSTRLEN];
  int numins;
  char *outlist[MAXSTRLEN];
  int numouts;
  struct __modulecall *next;
} _modulecall;

typedef struct __modulestats {
  char name[MAXSTRLEN];
  _var *portorder[MAXNUMVARS];
  int numports;
  _var *input_list[MAXNUMVARS];
  int numinputs;
  _var *output_list[MAXNUMVARS];
  int numoutputs;
  _var *reg_list[MAXNUMVARS];
  int numregs;
  _var *wire_list[MAXNUMVARS];
  int numwires;
  _modulecall *occ_list;
  struct __modulestats *next;
} _modulestats;

extern _modulestats *modstats, *thismodule;

char *get_string_from_var (_var *v);
char *get_string_from_varlist (_var **v, int n);
void print_modstats (void);
_modulestats * make_module (char *name);

//================================================
//==========The following code is for CFG=========
//=============liu187@illinois.edu================
enum cfg_node_type {
  NULL_NODE    = 0,
  IFCOND_EXP   = 1,
  IFELSE_EXP   = 2,
  CASE_EXP     = 3,
  CASE_DEFAULT = 4,
  BLK_AS       = 5,
  NBLK_AS      = 6,
  PORT_CONNECT = 7
};
typedef struct __cfg_node {
  char exp_name[MAXSTRLEN];
  char case_exp[MAXSTRLEN];
  int  frame_number;
  cfg_node_type node_type;
  struct __cfg_node * left_node;
  struct __cfg_node * right_node;
  struct __cfg_node * brother_node;
  struct __cfg_node * next_node; 
} _cfg_node;

extern _cfg_node * cfg_table[MAXNUMCFGS];
extern _cfg_node * cfg_temp;
extern _cfg_node * cfg_top;
extern int cfg_stack_pointer;

_cfg_node * build_blank_node();
_cfg_node * build_normal_node(char exp[], _cfg_node * left, _cfg_node * right, _cfg_node * next, _cfg_node * brother);
int set_next_node(_cfg_node * cfg_x, _cfg_node * cfg_nxt);
int push_cfg_table(_cfg_node * cfg_x);
void print_cfg(_cfg_node * cfg_x);
void print_all();
char * get_next_inst_statement();
extern FILE * fp_out;

typedef struct __para_table {
  char para_name[MAXSTRLEN];
  char para_value[MAXSTRLEN];
} _para_table;
extern _para_table current_para_table[MAXPARAS]; 


//==================================================
#endif //_VL2S_EXPR_H_
