module decode_stage(
	clk,
	reset,
	stall_in_0,
	stall_in_1,
	stall_in_2,
	branch_mispredict,
	fetch2decodeinstr0_out,
	fetch2decodeinstr1_out,
	fetch2decodepc1,
	fetch2decodepc0,
	fetch2decodepc1_incr,
	fetch2decodepc0_incr,
	fetch2decodeinstr0_valid,
	fetch2decodeinstr1_valid,
	fetch2decodebranch_taken,
	fetch2decodepredicted_pc,
	fetch2decodevalid,
	data_in_bpaddress_0,
	data_in_bpdata_0,
	data_in_bpvalid_0,
	data_in_bpaddress_1,
	data_in_bpdata_1,
	data_in_bpvalid_1,
	data_in_bpaddress_2,
	data_in_bpdata_2,
	data_in_bpvalid_2,
	data_in_bpaddress_3,
	data_in_bpdata_3,
	data_in_bpvalid_3,
	data_in_bpaddress_4,
	data_in_bpdata_4,
	data_in_bpvalid_4,
	data_in_bpaddress_5,
	data_in_bpdata_5,
	data_in_bpvalid_5,
	data_in_bpaddress_6,
	data_in_bpdata_6,
	data_in_bpvalid_6,
	data_in_bpaddress_7,
	data_in_bpdata_7,
	data_in_bpvalid_7,
	data_in_bpaddress_8,
	data_in_bpdata_8,
	data_in_bpvalid_8,
	data_in_bpaddress_9,
	data_in_bpdata_9,
	data_in_bpvalid_9,
	data_in_bpaddress_10,
	data_in_bpdata_10,
	data_in_bpvalid_10,
	write_enable_0,
	write_addr_0,
	write_data_0,
	write_enable_1,
	write_addr_1,
	write_data_1,
	decode2mempc,
	decode2mempc_incr,
	decode2memthread_num,
	decode2membranch_taken,
	decode2mempredicted_pc,
	decode2memvalid,
	decode2memis_oldest,
	decode2memsreg_t_data,
	decode2memsreg_s_data,
	decode2memresult,
	decode2memL1D_hit,
	decode2memdecode_packetsreg_t,
	decode2memdecode_packetsreg_s,
	decode2memdecode_packetdreg,
	decode2memdecode_packetacc,
	decode2memdecode_packetimm16_value,
	decode2memdecode_packetimm26_value,
	decode2memdecode_packetimm5_value,
	decode2memdecode_packetop_FU_type,
	decode2memdecode_packetalu_op,
	decode2memdecode_packetfpu_op,
	decode2memdecode_packetop_MEM_type,
	decode2memdecode_packetop_BR_type,
	decode2memdecode_packetshift_type,
	decode2memdecode_packetcompare_type,
	decode2memdecode_packetis_imm26,
	decode2memdecode_packetis_imm,
	decode2memdecode_packetis_imm5,
	decode2memdecode_packetis_signed,
	decode2memdecode_packetcarry_in,
	decode2memdecode_packetcarry_out,
	decode2memdecode_packethas_sreg_t,
	decode2memdecode_packethas_sreg_s,
	decode2memdecode_packethas_dreg,
	decode2memdecode_packetis_prefetch,
	decode2memdecode_packetsprf_dest,
	decode2memdecode_packetsprf_src,
	decode2memdecode_packetis_atomic,
	decode2memdecode_packetis_ldl,
	decode2memdecode_packetis_stl,
	decode2memdecode_packetis_stc,
	decode2memdecode_packetis_valid,
	decode2memdecode_packetis_halt,
	decode2memdecode_packetis_compare,
	decode2memdecode_packetdreg_is_src,
	decode2memdecode_packetdreg_is_dest,
	decode2fpupc,
	decode2fpupc_incr,
	decode2fputhread_num,
	decode2fpubranch_taken,
	decode2fpupredicted_pc,
	decode2fpuvalid,
	decode2fpuis_oldest,
	decode2fpusreg_t_data,
	decode2fpusreg_s_data,
	decode2fpuresult,
	decode2fpuL1D_hit,
	decode2fpudecode_packetsreg_t,
	decode2fpudecode_packetsreg_s,
	decode2fpudecode_packetdreg,
	decode2fpudecode_packetacc,
	decode2fpudecode_packetimm16_value,
	decode2fpudecode_packetimm26_value,
	decode2fpudecode_packetimm5_value,
	decode2fpudecode_packetop_FU_type,
	decode2fpudecode_packetalu_op,
	decode2fpudecode_packetfpu_op,
	decode2fpudecode_packetop_MEM_type,
	decode2fpudecode_packetop_BR_type,
	decode2fpudecode_packetshift_type,
	decode2fpudecode_packetcompare_type,
	decode2fpudecode_packetis_imm26,
	decode2fpudecode_packetis_imm,
	decode2fpudecode_packetis_imm5,
	decode2fpudecode_packetis_signed,
	decode2fpudecode_packetcarry_in,
	decode2fpudecode_packetcarry_out,
	decode2fpudecode_packethas_sreg_t,
	decode2fpudecode_packethas_sreg_s,
	decode2fpudecode_packethas_dreg,
	decode2fpudecode_packetis_prefetch,
	decode2fpudecode_packetsprf_dest,
	decode2fpudecode_packetsprf_src,
	decode2fpudecode_packetis_atomic,
	decode2fpudecode_packetis_ldl,
	decode2fpudecode_packetis_stl,
	decode2fpudecode_packetis_stc,
	decode2fpudecode_packetis_valid,
	decode2fpudecode_packetis_halt,
	decode2fpudecode_packetis_compare,
	decode2fpudecode_packetdreg_is_src,
	decode2fpudecode_packetdreg_is_dest,
	decode2intpc,
	decode2intpc_incr,
	decode2intthread_num,
	decode2intbranch_taken,
	decode2intpredicted_pc,
	decode2intvalid,
	decode2intis_oldest,
	decode2intsreg_t_data,
	decode2intsreg_s_data,
	decode2intresult,
	decode2intL1D_hit,
	decode2intdecode_packetsreg_t,
	decode2intdecode_packetsreg_s,
	decode2intdecode_packetdreg,
	decode2intdecode_packetacc,
	decode2intdecode_packetimm16_value,
	decode2intdecode_packetimm26_value,
	decode2intdecode_packetimm5_value,
	decode2intdecode_packetop_FU_type,
	decode2intdecode_packetalu_op,
	decode2intdecode_packetfpu_op,
	decode2intdecode_packetop_MEM_type,
	decode2intdecode_packetop_BR_type,
	decode2intdecode_packetshift_type,
	decode2intdecode_packetcompare_type,
	decode2intdecode_packetis_imm26,
	decode2intdecode_packetis_imm,
	decode2intdecode_packetis_imm5,
	decode2intdecode_packetis_signed,
	decode2intdecode_packetcarry_in,
	decode2intdecode_packetcarry_out,
	decode2intdecode_packethas_sreg_t,
	decode2intdecode_packethas_sreg_s,
	decode2intdecode_packethas_dreg,
	decode2intdecode_packetis_prefetch,
	decode2intdecode_packetsprf_dest,
	decode2intdecode_packetsprf_src,
	decode2intdecode_packetis_atomic,
	decode2intdecode_packetis_ldl,
	decode2intdecode_packetis_stl,
	decode2intdecode_packetis_stc,
	decode2intdecode_packetis_valid,
	decode2intdecode_packetis_halt,
	decode2intdecode_packetis_compare,
	decode2intdecode_packetdreg_is_src,
	decode2intdecode_packetdreg_is_dest,
	decode_stall
);

	input  clk;
	input  reset;
	input  stall_in_0;
	input  stall_in_1;
	input  stall_in_2;
	input  branch_mispredict;
	input [31:0] fetch2decodeinstr0_out;
	input [31:0] fetch2decodeinstr1_out;
	input [31:0] fetch2decodepc1;
	input [31:0] fetch2decodepc0;
	input [31:0] fetch2decodepc1_incr;
	input [31:0] fetch2decodepc0_incr;
	input  fetch2decodeinstr0_valid;
	input  fetch2decodeinstr1_valid;
	input  fetch2decodebranch_taken;
	input [31:0] fetch2decodepredicted_pc;
	input  fetch2decodevalid;
	input [4:0] data_in_bpaddress_0;
	input [31:0] data_in_bpdata_0;
	input  data_in_bpvalid_0;
	input [4:0] data_in_bpaddress_1;
	input [31:0] data_in_bpdata_1;
	input  data_in_bpvalid_1;
	input [4:0] data_in_bpaddress_2;
	input [31:0] data_in_bpdata_2;
	input  data_in_bpvalid_2;
	input [4:0] data_in_bpaddress_3;
	input [31:0] data_in_bpdata_3;
	input  data_in_bpvalid_3;
	input [4:0] data_in_bpaddress_4;
	input [31:0] data_in_bpdata_4;
	input  data_in_bpvalid_4;
	input [4:0] data_in_bpaddress_5;
	input [31:0] data_in_bpdata_5;
	input  data_in_bpvalid_5;
	input [4:0] data_in_bpaddress_6;
	input [31:0] data_in_bpdata_6;
	input  data_in_bpvalid_6;
	input [4:0] data_in_bpaddress_7;
	input [31:0] data_in_bpdata_7;
	input  data_in_bpvalid_7;
	input [4:0] data_in_bpaddress_8;
	input [31:0] data_in_bpdata_8;
	input  data_in_bpvalid_8;
	input [4:0] data_in_bpaddress_9;
	input [31:0] data_in_bpdata_9;
	input  data_in_bpvalid_9;
	input [4:0] data_in_bpaddress_10;
	input [31:0] data_in_bpdata_10;
	input  data_in_bpvalid_10;
	input  write_enable_0;
	input [4:0] write_addr_0;
	input [31:0] write_data_0;
	input  write_enable_1;
	input [4:0] write_addr_1;
	input [31:0] write_data_1;
	output [31:0] decode2mempc;
	output [31:0] decode2mempc_incr;
	output [1:0] decode2memthread_num;
	output  decode2membranch_taken;
	output [31:0] decode2mempredicted_pc;
	output  decode2memvalid;
	output  decode2memis_oldest;
	output [31:0] decode2memsreg_t_data;
	output [31:0] decode2memsreg_s_data;
	output [31:0] decode2memresult;
	output  decode2memL1D_hit;
	output [4:0] decode2memdecode_packetsreg_t;
	output [4:0] decode2memdecode_packetsreg_s;
	output [4:0] decode2memdecode_packetdreg;
	output [1:0] decode2memdecode_packetacc;
	output [15:0] decode2memdecode_packetimm16_value;
	output [25:0] decode2memdecode_packetimm26_value;
	output [4:0] decode2memdecode_packetimm5_value;
	output [3:0] decode2memdecode_packetop_FU_type;
	output [4:0] decode2memdecode_packetalu_op;
	output [4:0] decode2memdecode_packetfpu_op;
	output [3:0] decode2memdecode_packetop_MEM_type;
	output [1:0] decode2memdecode_packetop_BR_type;
	output [1:0] decode2memdecode_packetshift_type;
	output [2:0] decode2memdecode_packetcompare_type;
	output  decode2memdecode_packetis_imm26;
	output  decode2memdecode_packetis_imm;
	output  decode2memdecode_packetis_imm5;
	output  decode2memdecode_packetis_signed;
	output  decode2memdecode_packetcarry_in;
	output  decode2memdecode_packetcarry_out;
	output  decode2memdecode_packethas_sreg_t;
	output  decode2memdecode_packethas_sreg_s;
	output  decode2memdecode_packethas_dreg;
	output  decode2memdecode_packetis_prefetch;
	output  decode2memdecode_packetsprf_dest;
	output  decode2memdecode_packetsprf_src;
	output  decode2memdecode_packetis_atomic;
	output  decode2memdecode_packetis_ldl;
	output  decode2memdecode_packetis_stl;
	output  decode2memdecode_packetis_stc;
	output  decode2memdecode_packetis_valid;
	output  decode2memdecode_packetis_halt;
	output  decode2memdecode_packetis_compare;
	output  decode2memdecode_packetdreg_is_src;
	output  decode2memdecode_packetdreg_is_dest;
	output [31:0] decode2fpupc;
	output [31:0] decode2fpupc_incr;
	output [1:0] decode2fputhread_num;
	output  decode2fpubranch_taken;
	output [31:0] decode2fpupredicted_pc;
	output  decode2fpuvalid;
	output  decode2fpuis_oldest;
	output [31:0] decode2fpusreg_t_data;
	output [31:0] decode2fpusreg_s_data;
	output [31:0] decode2fpuresult;
	output  decode2fpuL1D_hit;
	output [4:0] decode2fpudecode_packetsreg_t;
	output [4:0] decode2fpudecode_packetsreg_s;
	output [4:0] decode2fpudecode_packetdreg;
	output [1:0] decode2fpudecode_packetacc;
	output [15:0] decode2fpudecode_packetimm16_value;
	output [25:0] decode2fpudecode_packetimm26_value;
	output [4:0] decode2fpudecode_packetimm5_value;
	output [3:0] decode2fpudecode_packetop_FU_type;
	output [4:0] decode2fpudecode_packetalu_op;
	output [4:0] decode2fpudecode_packetfpu_op;
	output [3:0] decode2fpudecode_packetop_MEM_type;
	output [1:0] decode2fpudecode_packetop_BR_type;
	output [1:0] decode2fpudecode_packetshift_type;
	output [2:0] decode2fpudecode_packetcompare_type;
	output  decode2fpudecode_packetis_imm26;
	output  decode2fpudecode_packetis_imm;
	output  decode2fpudecode_packetis_imm5;
	output  decode2fpudecode_packetis_signed;
	output  decode2fpudecode_packetcarry_in;
	output  decode2fpudecode_packetcarry_out;
	output  decode2fpudecode_packethas_sreg_t;
	output  decode2fpudecode_packethas_sreg_s;
	output  decode2fpudecode_packethas_dreg;
	output  decode2fpudecode_packetis_prefetch;
	output  decode2fpudecode_packetsprf_dest;
	output  decode2fpudecode_packetsprf_src;
	output  decode2fpudecode_packetis_atomic;
	output  decode2fpudecode_packetis_ldl;
	output  decode2fpudecode_packetis_stl;
	output  decode2fpudecode_packetis_stc;
	output  decode2fpudecode_packetis_valid;
	output  decode2fpudecode_packetis_halt;
	output  decode2fpudecode_packetis_compare;
	output  decode2fpudecode_packetdreg_is_src;
	output  decode2fpudecode_packetdreg_is_dest;
	output [31:0] decode2intpc;
	output [31:0] decode2intpc_incr;
	output [1:0] decode2intthread_num;
	output  decode2intbranch_taken;
	output [31:0] decode2intpredicted_pc;
	output  decode2intvalid;
	output  decode2intis_oldest;
	output [31:0] decode2intsreg_t_data;
	output [31:0] decode2intsreg_s_data;
	output [31:0] decode2intresult;
	output  decode2intL1D_hit;
	output [4:0] decode2intdecode_packetsreg_t;
	output [4:0] decode2intdecode_packetsreg_s;
	output [4:0] decode2intdecode_packetdreg;
	output [1:0] decode2intdecode_packetacc;
	output [15:0] decode2intdecode_packetimm16_value;
	output [25:0] decode2intdecode_packetimm26_value;
	output [4:0] decode2intdecode_packetimm5_value;
	output [3:0] decode2intdecode_packetop_FU_type;
	output [4:0] decode2intdecode_packetalu_op;
	output [4:0] decode2intdecode_packetfpu_op;
	output [3:0] decode2intdecode_packetop_MEM_type;
	output [1:0] decode2intdecode_packetop_BR_type;
	output [1:0] decode2intdecode_packetshift_type;
	output [2:0] decode2intdecode_packetcompare_type;
	output  decode2intdecode_packetis_imm26;
	output  decode2intdecode_packetis_imm;
	output  decode2intdecode_packetis_imm5;
	output  decode2intdecode_packetis_signed;
	output  decode2intdecode_packetcarry_in;
	output  decode2intdecode_packetcarry_out;
	output  decode2intdecode_packethas_sreg_t;
	output  decode2intdecode_packethas_sreg_s;
	output  decode2intdecode_packethas_dreg;
	output  decode2intdecode_packetis_prefetch;
	output  decode2intdecode_packetsprf_dest;
	output  decode2intdecode_packetsprf_src;
	output  decode2intdecode_packetis_atomic;
	output  decode2intdecode_packetis_ldl;
	output  decode2intdecode_packetis_stl;
	output  decode2intdecode_packetis_stc;
	output  decode2intdecode_packetis_valid;
	output  decode2intdecode_packetis_halt;
	output  decode2intdecode_packetis_compare;
	output  decode2intdecode_packetdreg_is_src;
	output  decode2intdecode_packetdreg_is_dest;
	output  decode_stall;
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // local wires and regs
        reg [31:0] decode2mempc;
	reg [31:0] decode2mempc_incr;
	reg [1:0] decode2memthread_num;
	reg  decode2membranch_taken;
	reg [31:0] decode2mempredicted_pc;
	reg  decode2memvalid;
	reg  decode2memis_oldest;
	reg [31:0] decode2memsreg_t_data;
	reg [31:0] decode2memsreg_s_data;
	reg [31:0] decode2memresult;
	reg  decode2memL1D_hit;
	reg [4:0] decode2memdecode_packetsreg_t;
	reg [4:0] decode2memdecode_packetsreg_s;
	reg [4:0] decode2memdecode_packetdreg;
	reg [1:0] decode2memdecode_packetacc;
	reg [15:0] decode2memdecode_packetimm16_value;
	reg [25:0] decode2memdecode_packetimm26_value;
	reg [4:0] decode2memdecode_packetimm5_value;
	reg [3:0] decode2memdecode_packetop_FU_type;
	reg [4:0] decode2memdecode_packetalu_op;
	reg [4:0] decode2memdecode_packetfpu_op;
	reg [3:0] decode2memdecode_packetop_MEM_type;
	reg [1:0] decode2memdecode_packetop_BR_type;
	reg [1:0] decode2memdecode_packetshift_type;
	reg [2:0] decode2memdecode_packetcompare_type;
	reg  decode2memdecode_packetis_imm26;
	reg  decode2memdecode_packetis_imm;
	reg  decode2memdecode_packetis_imm5;
	reg  decode2memdecode_packetis_signed;
	reg  decode2memdecode_packetcarry_in;
	reg  decode2memdecode_packetcarry_out;
	reg  decode2memdecode_packethas_sreg_t;
	reg  decode2memdecode_packethas_sreg_s;
	reg  decode2memdecode_packethas_dreg;
	reg  decode2memdecode_packetis_prefetch;
	reg  decode2memdecode_packetsprf_dest;
	reg  decode2memdecode_packetsprf_src;
	reg  decode2memdecode_packetis_atomic;
	reg  decode2memdecode_packetis_ldl;
	reg  decode2memdecode_packetis_stl;
	reg  decode2memdecode_packetis_stc;
	reg  decode2memdecode_packetis_valid;
	reg  decode2memdecode_packetis_halt;
	reg  decode2memdecode_packetis_compare;
	reg  decode2memdecode_packetdreg_is_src;
	reg  decode2memdecode_packetdreg_is_dest;
	reg [31:0] decode2fpupc;
	reg [31:0] decode2fpupc_incr;
	reg [1:0] decode2fputhread_num;
	reg  decode2fpubranch_taken;
	reg [31:0] decode2fpupredicted_pc;
	reg  decode2fpuvalid;
	reg  decode2fpuis_oldest;
	reg [31:0] decode2fpusreg_t_data;
	reg [31:0] decode2fpusreg_s_data;
	reg [31:0] decode2fpuresult;
	reg  decode2fpuL1D_hit;
	reg [4:0] decode2fpudecode_packetsreg_t;
	reg [4:0] decode2fpudecode_packetsreg_s;
	reg [4:0] decode2fpudecode_packetdreg;
	reg [1:0] decode2fpudecode_packetacc;
	reg [15:0] decode2fpudecode_packetimm16_value;
	reg [25:0] decode2fpudecode_packetimm26_value;
	reg [4:0] decode2fpudecode_packetimm5_value;
	reg [3:0] decode2fpudecode_packetop_FU_type;
	reg [4:0] decode2fpudecode_packetalu_op;
	reg [4:0] decode2fpudecode_packetfpu_op;
	reg [3:0] decode2fpudecode_packetop_MEM_type;
	reg [1:0] decode2fpudecode_packetop_BR_type;
	reg [1:0] decode2fpudecode_packetshift_type;
	reg [2:0] decode2fpudecode_packetcompare_type;
	reg  decode2fpudecode_packetis_imm26;
	reg  decode2fpudecode_packetis_imm;
	reg  decode2fpudecode_packetis_imm5;
	reg  decode2fpudecode_packetis_signed;
	reg  decode2fpudecode_packetcarry_in;
	reg  decode2fpudecode_packetcarry_out;
	reg  decode2fpudecode_packethas_sreg_t;
	reg  decode2fpudecode_packethas_sreg_s;
	reg  decode2fpudecode_packethas_dreg;
	reg  decode2fpudecode_packetis_prefetch;
	reg  decode2fpudecode_packetsprf_dest;
	reg  decode2fpudecode_packetsprf_src;
	reg  decode2fpudecode_packetis_atomic;
	reg  decode2fpudecode_packetis_ldl;
	reg  decode2fpudecode_packetis_stl;
	reg  decode2fpudecode_packetis_stc;
	reg  decode2fpudecode_packetis_valid;
	reg  decode2fpudecode_packetis_halt;
	reg  decode2fpudecode_packetis_compare;
	reg  decode2fpudecode_packetdreg_is_src;
	reg  decode2fpudecode_packetdreg_is_dest;
	reg [31:0] decode2intpc;
	reg [31:0] decode2intpc_incr;
	reg [1:0] decode2intthread_num;
	reg  decode2intbranch_taken;
	reg [31:0] decode2intpredicted_pc;
	reg  decode2intvalid;
	reg  decode2intis_oldest;
	reg [31:0] decode2intsreg_t_data;
	reg [31:0] decode2intsreg_s_data;
	reg [31:0] decode2intresult;
	reg  decode2intL1D_hit;



  //////////////////////////////////////////////////////////////////////////////
  //decode_packet_t decode_packet0,decode_packet1; // signals pass through data from decoders
  reg [4:0] decode_packet0sreg_t;
  reg [4:0] decode_packet0sreg_s;
  reg [4:0] decode_packet0dreg;
  reg [1:0] decode_packet0acc;
  reg [15:0] decode_packet0imm16_value;
  reg [25:0] decode_packet0imm26_value;
  reg [4:0] decode_packet0imm5_value;
  reg [3:0] decode_packet0op_FU_type;
  reg [4:0] decode_packet0alu_op;
  reg [4:0] decode_packet0fpu_op;
  reg [3:0] decode_packet0op_MEM_type;
  reg [1:0] decode_packet0op_BR_type;
  reg [1:0] decode_packet0shift_type;
  reg [2:0] decode_packet0compare_type;
  reg  decode_packet0is_imm26;
  reg  decode_packet0is_imm;
  reg  decode_packet0is_imm5;
  reg  decode_packet0is_signed;
  reg  decode_packet0carry_in;
  reg  decode_packet0carry_out;
  reg  decode_packet0has_sreg_t;
  reg  decode_packet0has_sreg_s;
  reg  decode_packet0has_dreg;
  reg  decode_packet0is_prefetch;
  reg  decode_packet0sprf_dest;
  reg  decode_packet0sprf_src;
  reg  decode_packet0is_atomic;
  reg  decode_packet0is_ldl;
  reg  decode_packet0is_stl;
  reg  decode_packet0is_stc;
  reg  decode_packet0is_valid;
  reg  decode_packet0is_halt;
  reg  decode_packet0is_compare;
  reg  decode_packet0dreg_is_src;
  reg  decode_packet0dreg_is_dest;
  reg [4:0] decode_packet1sreg_t;
  reg [4:0] decode_packet1sreg_s;
  reg [4:0] decode_packet1dreg;
  reg [1:0] decode_packet1acc;
  reg [15:0] decode_packet1imm16_value;
  reg [25:0] decode_packet1imm26_value;
  reg [4:0] decode_packet1imm5_value;
  reg [3:0] decode_packet1op_FU_type;
  reg [4:0] decode_packet1alu_op;
  reg [4:0] decode_packet1fpu_op;
  reg [3:0] decode_packet1op_MEM_type;
  reg [1:0] decode_packet1op_BR_type;
  reg [1:0] decode_packet1shift_type;
  reg [2:0] decode_packet1compare_type;
  reg  decode_packet1is_imm26;
  reg  decode_packet1is_imm;
  reg  decode_packet1is_imm5;
  reg  decode_packet1is_signed;
  reg  decode_packet1carry_in;
  reg  decode_packet1carry_out;
  reg  decode_packet1has_sreg_t;
  reg  decode_packet1has_sreg_s;
  reg  decode_packet1has_dreg;
  reg  decode_packet1is_prefetch;
  reg  decode_packet1sprf_dest;
  reg  decode_packet1sprf_src;
  reg  decode_packet1is_atomic;
  reg  decode_packet1is_ldl;
  reg  decode_packet1is_stl;
  reg  decode_packet1is_stc;
  reg  decode_packet1is_valid;
  reg  decode_packet1is_halt;
  reg  decode_packet1is_compare;
  reg  decode_packet1dreg_is_src;
  reg  decode_packet1dreg_is_dest;
  reg [31:0] sreg_t_data0;         // signals hold data from regfile
  reg [31:0] sreg_t_data1;         // signals hold data from regfile
  reg [31:0] sreg_s_data0;         // signals hold data from regfile
  reg [31:0] sreg_s_data1;         // signals hold data from regfile
  reg [31:0] sprf_sreg_data;                    // signal holds data from sprf
  reg [3:0] sprf_sreg_addr;                     // signal hold address to a register sprf
  reg sprf_src;                                // sprf read enable
  reg sprf_dest;                               // sprf write enable
  reg [31:0] sreg_t_0;   // data coming out of bypass/rf selector mux
  reg [31:0] sreg_s_0;   // data coming out of bypass/rf selector mux
  reg [31:0] sreg_t_1;   // data coming out of bypass/rf selector mux
  reg [31:0] sreg_s_1;   // data coming out of bypass/rf selector mux
  reg instr0_issued;                           // indicate whether the instruction was issued
  reg schedule0;                     // the instr in this slot may be considered for issue
  reg schedule1;                     // the instr in this slot may be considered for issue
  reg issue0;                           // actually issue the slot this cycle?
  reg issue1;                           // actually issue the slot this cycle?
  reg valid_int;             // indicates whether the FU is being used
  reg valid_fpu;             // indicates whether the FU is being used
  reg valid_mem;             // indicates whether the FU is being used
  reg choice_int;          // indicates which slot issues to the pipe
  reg choice_fpu;          // indicates which slot issues to the pipe
  reg choice_mem;          // indicates which slot issues to the pipe
  wire issue_halt;                               // Propagate the HALT signal
  wire sreg_t_0_is_bp;           // Indicate that data was obtained
  wire sreg_t_1_is_bp;           // Indicate that data was obtained
  wire sreg_s_0_is_bp;           // from the bypass network
  wire sreg_s_1_is_bp;           // from the bypass network
  wire sreg_s_0_rdy;
  wire sreg_t_0_rdy;
  wire sreg_s_1_rdy;
  wire sreg_t_1_rdy;
  wire dreg_0_rdy;
  wire dreg_1_rdy;
  wire follows_vld_branch;                       // Indicate instr in slot 1 follows a valid branch from slot 0
  wire exec_stall;
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // Decoder Modules - 1 Decoder per Fetch/Issue Slot
  //////////////////////////////////////////////////////////////////////////////
  // stall signal indicates that not all instructions were issued
  assign decode_stall = !branch_mispredict && (/*(decode_packet0is_halt || decode_packet1is_halt) ||*/ 
        (!issue1 && fetch2decodevalid && fetch2decodeinstr1_valid));
  // set the read enable signal for sprf
  assign sprf_src = decode_packet0sprf_src | decode_packet1sprf_src;
  // set the write enable signal for sprf
  assign sprf_dest = decode_packet0sprf_dest | decode_packet1sprf_dest;
  // set the proper address to the sprf  
  assign sprf_sreg_addr = ( choice_int ) ? decode_packet1sreg_t:decode_packet0sreg_t;
  // set the halt signal flag only if all stages are unstalled and instr0 is a halt and 
  // is issuing this cycle, or if instr1 is a halt and instr0 issued last cycle or if instr0 in invalid
  assign issue_halt = fetch2decodevalid && !exec_stall && 
      ((decode_packet1is_halt && ((instr0_issued && schedule1) || !fetch2decodeinstr0_valid)) || (decode_packet0is_halt && schedule0));
  assign follows_vld_branch = (decode_packet0op_FU_type == 4'b1010 && fetch2decodeinstr0_valid && branch_mispredict);
  assign exec_stall = stall_in_0 || stall_in_2 || stall_in_1;
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // Decoder Modules - 1 Decoder per Fetch/Issue Slot
  //////////////////////////////////////////////////////////////////////////////
  decoder0 DECODER0(
    .fetch2decodeinstr1_out(fetch2decodeinstr0_out[31:0]),
    .decode_packet1sreg_t(decode_packet0sreg_t[4:0]),
    .decode_packet1sreg_s(decode_packet0sreg_s[4:0]),
    .decode_packet1dreg(decode_packet0dreg[4:0]),
    .decode_packet1acc(decode_packet0acc[1:0]),
    .decode_packet1imm16_value(decode_packet0imm16_value[15:0]),
    .decode_packet1imm26_value(decode_packet0imm26_value[25:0]),
    .decode_packet1imm5_value(decode_packet0imm5_value[4:0]),
    .decode_packet1op_FU_type(decode_packet0op_FU_type[3:0]),
    .decode_packet1alu_op(decode_packet0alu_op[4:0]),
    .decode_packet1fpu_op(decode_packet0fpu_op[4:0]),
    .decode_packet1op_MEM_type(decode_packet0op_MEM_type[3:0]),
    .decode_packet1op_BR_type(decode_packet0op_BR_type[1:0]),
    .decode_packet1shift_type(decode_packet0shift_type[1:0]),
    .decode_packet1compare_type(decode_packet0compare_type[2:0]),
    .decode_packet1is_imm26(decode_packet0is_imm26),
    .decode_packet1is_imm(decode_packet0is_imm),
    .decode_packet1is_imm5(decode_packet0is_imm5),
    .decode_packet1is_signed(decode_packet0is_signed),
    .decode_packet1carry_in(decode_packet0carry_in),
    .decode_packet1carry_out(decode_packet0carry_out),
    .decode_packet1has_sreg_t(decode_packet0has_sreg_t),
    .decode_packet1has_sreg_s(decode_packet0has_sreg_s),
    .decode_packet1has_dreg(decode_packet0has_dreg),
    .decode_packet1is_prefetch(decode_packet0is_prefetch),
    .decode_packet1sprf_dest(decode_packet0sprf_dest),
    .decode_packet1sprf_src(decode_packet0sprf_src),
    .decode_packet1is_atomic(decode_packet0is_atomic),
    .decode_packet1is_ldl(decode_packet0is_ldl),
    .decode_packet1is_stl(decode_packet0is_stl),
    .decode_packet1is_stc(decode_packet0is_stc),
    .decode_packet1is_valid(decode_packet0is_valid),
    .decode_packet1is_halt(decode_packet0is_halt),
    .decode_packet1is_compare(decode_packet0is_compare),
    .decode_packet1dreg_is_src(decode_packet0dreg_is_src),
    .decode_packet1dreg_is_dest(decode_packet0dreg_is_dest)    
  );
  
  decoder1 DECODER1(
    // Input from fetch
    .fetch2decodeinstr1_out(fetch2decodeinstr1_out[31:0]),
    // Decoded instruction packet
    //decode_packet1
    .decode_packet1sreg_t(decode_packet1sreg_t[4:0]),
    .decode_packet1sreg_s(decode_packet1sreg_s[4:0]),
    .decode_packet1dreg(decode_packet1dreg[4:0]),
    .decode_packet1acc(decode_packet1acc[1:0]),
    .decode_packet1imm16_value(decode_packet1imm16_value[15:0]),
    .decode_packet1imm26_value(decode_packet1imm26_value[25:0]),
    .decode_packet1imm5_value(decode_packet1imm5_value[4:0]),
    .decode_packet1op_FU_type(decode_packet1op_FU_type[3:0]),
    .decode_packet1alu_op(decode_packet1alu_op[4:0]),
    .decode_packet1fpu_op(decode_packet1fpu_op[4:0]),
    .decode_packet1op_MEM_type(decode_packet1op_MEM_type[3:0]),
    .decode_packet1op_BR_type(decode_packet1op_BR_type[1:0]),
    .decode_packet1shift_type(decode_packet1shift_type[1:0]),
    .decode_packet1compare_type(decode_packet1compare_type[2:0]),
    .decode_packet1is_imm26(decode_packet1is_imm26),
    .decode_packet1is_imm(decode_packet1is_imm),
    .decode_packet1is_imm5(decode_packet1is_imm5),
    .decode_packet1is_signed(decode_packet1is_signed),
    .decode_packet1carry_in(decode_packet1carry_in),
    .decode_packet1carry_out(decode_packet1carry_out),
    .decode_packet1has_sreg_t(decode_packet1has_sreg_t),
    .decode_packet1has_sreg_s(decode_packet1has_sreg_s),
    .decode_packet1has_dreg(decode_packet1has_dreg),
    .decode_packet1is_prefetch(decode_packet1is_prefetch),
    .decode_packet1sprf_dest(decode_packet1sprf_dest),
    .decode_packet1sprf_src(decode_packet1sprf_src),
    .decode_packet1is_atomic(decode_packet1is_atomic),
    .decode_packet1is_ldl(decode_packet1is_ldl),
    .decode_packet1is_stl(decode_packet1is_stl),
    .decode_packet1is_stc(decode_packet1is_stc),
    .decode_packet1is_valid(decode_packet1is_valid),
    .decode_packet1is_halt(decode_packet1is_halt),
    .decode_packet1is_compare(decode_packet1is_compare),
    .decode_packet1dreg_is_src(decode_packet1dreg_is_src),
    .decode_packet1dreg_is_dest(decode_packet1dreg_is_dest)
  );
  ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  // *** local wires and regs ***
  ///////////////////////////////////////////////////////////////////////////////
  reg dst0_vld; // status indicates that the dreg is valid for locking
  reg dst1_vld;
  assign dst0_vld = decode_packet0has_dreg && issue0 && schedule0 && !branch_mispredict && fetch2decodeinstr0_valid;
  assign dst1_vld = decode_packet1has_dreg && issue1 && schedule1 && !branch_mispredict && fetch2decodeinstr1_valid &&
                    !follows_vld_branch;
  ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  // Scoreboard
  ///////////////////////////////////////////////////////////////////////////////
  scoreboard SB(
    // control signals
    .clk(clk),
    .reset(reset),
    // Source registers - inputs to read ports
    .src0_addr(decode_packet0sreg_s),
    .src1_addr(decode_packet0sreg_t),
    .src2_addr(decode_packet1sreg_s),
    .src3_addr(decode_packet1sreg_t),
    // Destination registers - inputs to read/write ports
    .dst0_addr(decode_packet0dreg),
    .dst0_vld(dst0_vld),
    .dst1_addr(decode_packet1dreg),
    .dst1_vld(dst1_vld),
    // Writeback access - inputs to write ports
    .wb0_addr(write_addr_0),
    .wb0_vld(write_enable_0),
    .wb1_addr(write_addr_1),
    .wb1_vld(write_enable_1),
    // Outputs from the scoreboard
    .src0_rdy(sreg_s_0_rdy),
    .src1_rdy(sreg_t_0_rdy),
    .src2_rdy(sreg_s_1_rdy),
    .src3_rdy(sreg_t_1_rdy),
    .dst0_rdy(dreg_0_rdy),
    .dst1_rdy(dreg_1_rdy)
  );
  ///////////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////
  // Register File - 4 Read Ports, 2 Write Ports
  ///////////////////////////////////////////////////////////////////////////////
  rf_4r2w RF(
    // control signals
    .clk(clk),
    .reset(reset),
    .enable(1'b1),                 // Reg File Enable Signal - used for clock gating
    // write port input signals
    .write_addr_0(write_addr_0),
    .write_addr_1(write_addr_1),
    .write_data_0(write_data_0),
    .write_data_1(write_data_1),
    .write_enable_0(write_enable_0),
    .write_enable_1(write_enable_1),
    // read port input signals
    .read_addr_0(decode_packet0sreg_t),
    .read_addr_1(decode_packet0sreg_s),
    .read_addr_2(decode_packet1sreg_t),
    .read_addr_3(decode_packet1sreg_s),
    .read_enable_0(decode_packet0has_sreg_t),
    .read_enable_1(decode_packet0has_sreg_s),
    .read_enable_2(decode_packet1has_sreg_t),
    .read_enable_3(decode_packet1has_sreg_s),
    // read port data outputs
    .read_data_0(sreg_t_data0),
    .read_data_1(sreg_s_data0),
    .read_data_2(sreg_t_data1),
    .read_data_3(sreg_s_data1)
  );
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  // Special Purpose Register File - aka SPRF
  //////////////////////////////////////////////////////////////////////////////
  sprf SPRF(
    // control signals
    .clk(clk),
    .reset(reset),
    // read/write input signals
    .read_addr(sprf_sreg_addr),
    .read_enable(sprf_src),       // read from sprf
    .write_enable(sprf_dest),     // write to sprf
    // data output from sprf
    .read_data(sprf_sreg_data)
  );
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // RF / Bypass Selectors
  //////////////////////////////////////////////////////////////////////////////
  // For each register in each issue slot the modules select whether the data
  // comes from the bypass network or the register file
  //////////////////////////////////////////////////////////////////////////////
  bypass_select0 BP_SELECT_S_0(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_s_data0),
    .address_in_rf(decode_packet0sreg_s),
    // Output from the source selector
    .data_out(sreg_s_0),         // Data
    .src_is_bp(sreg_s_0_is_bp)  // Indicator that data obtained from bp network
  );
  bypass_select1 BP_SELECT_S_1(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_s_data1),
    .address_in_rf(decode_packet1sreg_s),
    // Output from thie source selector
    .data_out(sreg_s_1),
    .src_is_bp(sreg_s_1_is_bp)  // Indicator that data obtained from bp network
  );
  bypass_select2 BP_SELECT_T_0(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_t_data0),
    .address_in_rf(decode_packet0sreg_t),
    // Output from the source selector
    .data_out(sreg_t_0),
    .src_is_bp(sreg_t_0_is_bp)  // Indicator that data obtained from bp network
  );
  bypass_select3 BP_SELECT_T_1(
    // Input from the bypass network 
    //data_in_bp(data_in_bp),
    .data_in_bpaddress_0(data_in_bpaddress_0),
    .data_in_bpdata_0(data_in_bpdata_0),
    .data_in_bpvalid_0(data_in_bpvalid_0),
    .data_in_bpaddress_1(data_in_bpaddress_1),
    .data_in_bpdata_1(data_in_bpdata_1),
    .data_in_bpvalid_1(data_in_bpvalid_1),
    .data_in_bpaddress_2(data_in_bpaddress_2),
    .data_in_bpdata_2(data_in_bpdata_2),
    .data_in_bpvalid_2(data_in_bpvalid_2),
    .data_in_bpaddress_3(data_in_bpaddress_3),
    .data_in_bpdata_3(data_in_bpdata_3),
    .data_in_bpvalid_3(data_in_bpvalid_3),
    .data_in_bpaddress_4(data_in_bpaddress_4),
    .data_in_bpdata_4(data_in_bpdata_4),
    .data_in_bpvalid_4(data_in_bpvalid_4),
    .data_in_bpaddress_5(data_in_bpaddress_5),
    .data_in_bpdata_5(data_in_bpdata_5),
    .data_in_bpvalid_5(data_in_bpvalid_5),
    .data_in_bpaddress_6(data_in_bpaddress_6),
    .data_in_bpdata_6(data_in_bpdata_6),
    .data_in_bpvalid_6(data_in_bpvalid_6),
    .data_in_bpaddress_7(data_in_bpaddress_7),
    .data_in_bpdata_7(data_in_bpdata_7),
    .data_in_bpvalid_7(data_in_bpvalid_7),
    .data_in_bpaddress_8(data_in_bpaddress_8),
    .data_in_bpdata_8(data_in_bpdata_8),
    .data_in_bpvalid_8(data_in_bpvalid_8),
    .data_in_bpaddress_9(data_in_bpaddress_9),
    .data_in_bpdata_9(data_in_bpdata_9),
    .data_in_bpvalid_9(data_in_bpvalid_9),
    .data_in_bpaddress_10(data_in_bpaddress_10),
    .data_in_bpdata_10(data_in_bpdata_10),
    .data_in_bpvalid_10(data_in_bpvalid_10),
    // Inputs from the register file
    .data_in_rf(sreg_t_data1),
    .address_in_rf(decode_packet1sreg_t),
    // Output from the source selector
    .data_out(sreg_t_1),
    .src_is_bp(sreg_t_1_is_bp)  // Indicator that data obtained from bp network
  );
 
  always @ (fetch2decodeinstr0_valid or fetch2decodevalid or exec_stall or sreg_t_0_is_bp or sreg_t_0_rdy or decode_packet0has_sreg_t or sreg_s_0_is_bp or sreg_s_0_rdy or decode_packet0has_sreg_s or dreg_0_rdy or decode_packet0has_dreg or instr0_issued) begin
    if( fetch2decodeinstr0_valid && fetch2decodevalid && !exec_stall &&
        ( sreg_t_0_is_bp || sreg_t_0_rdy || !decode_packet0has_sreg_t) &&
        ( sreg_s_0_is_bp || sreg_s_0_rdy || !decode_packet0has_sreg_s) && 
        ( dreg_0_rdy || !decode_packet0has_dreg) &&
        !instr0_issued 
      )
      schedule0 = 1'b1;
    else
      schedule0 = 1'b0;
  end
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // ISSUE_1
  //////////////////////////////////////////////////////////////////////////////
  // Determine if instruction from slot 1 may be issued this cycle  It may only
  // be issued if all the following conditions have been met:
  //  - There are no stalls from the scoreboard
  //  - Instruction from slot 0 has issued last cycle or this cycle
  //  - If instruction from slot 0 is issuing this cycle, issue only if:
  //    - Instr1 does not need to go down the same pipe
  //    - instr1 and instr0 do not write to the same register
  //    - instr0 does not write to source registers used by instr1
  //    - The instruction in slot 1 is a valid instruction
  //    - instr1 is not a halt
  //    - Instr1 is a nop
  //  - If instr1 is a halt, issue only if instr0 has been issued previous cycle
  //////////////////////////////////////////////////////////////////////////////
  always @ (exec_stall or sreg_t_1_is_bp or sreg_t_1_rdy or decode_packet1has_sreg_t or sreg_s_1_is_bp or sreg_s_1_rdy or decode_packet1has_sreg_s or dreg_1_rdy or decode_packet1has_dreg or decode_packet0op_FU_type or decode_packet1op_FU_type or decode_packet0dreg or decode_packet1dreg or decode_packet0has_dreg or decode_packet1sreg_t or decode_packet1sreg_s or fetch2decodeinstr0_valid or fetch2decodeinstr1_valid or issue0 or decode_packet1is_halt or instr0_issued or decode_packet1is_valid) begin: ISSUE_1 
    if( !exec_stall && ( sreg_t_1_is_bp || sreg_t_1_rdy || !decode_packet1has_sreg_t) && 
        ( sreg_s_1_is_bp || sreg_s_1_rdy || !decode_packet1has_sreg_s) && 
        ( dreg_1_rdy || !decode_packet1has_dreg) &&
        (((decode_packet0op_FU_type != decode_packet1op_FU_type) &&
          (decode_packet0op_FU_type != 4'b1010) &&
          ((decode_packet0dreg != decode_packet1dreg) || !decode_packet0has_dreg || !decode_packet1has_dreg) &&
          ((decode_packet0dreg != decode_packet1sreg_t) || !decode_packet0has_dreg || !decode_packet1has_sreg_t) && 
          ((decode_packet0dreg != decode_packet1sreg_s) || !decode_packet0has_dreg || !decode_packet1has_sreg_s) &&
          fetch2decodeinstr0_valid && fetch2decodeinstr1_valid && issue0 && !decode_packet1is_halt
         ) || instr0_issued  || (issue0 && !decode_packet1is_valid) || !fetch2decodeinstr0_valid 
        )
      )
      // NOTE TO ONESELF FROM MY PAST ONESELF - If issues arise when sb is being
      // used it's probably due to this if statement  
      schedule1 = 1'b1;
    else
      schedule1 = 1'b0;
  end
  //////////////////////////////////////////////////////////////////////////////

  always @ (schedule0 or decode_packet0op_FU_type or decode_packet0is_valid or stall_in_0 or decode_packet1is_valid) begin: INT_ISSUE
    if ( schedule0  && ( 
         decode_packet0op_FU_type == 4'b0001 || 
         decode_packet0op_FU_type == 4'b0110 ||
         decode_packet0op_FU_type == 4'b1010 ) &&
         decode_packet0is_valid
       ) begin
      valid_int = !stall_in_0;
      choice_int = 1'b0;
    end else begin
      choice_int = 1'b1;
      valid_int = ( schedule1 && ( 
                    decode_packet1op_FU_type == 4'b0001 || 
                    decode_packet1op_FU_type == 4'b0110 ||
                    decode_packet1op_FU_type == 4'b1010 ) &&
                    decode_packet1is_valid && !stall_in_0
                  );
    end
  end
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // FPU_ISSUE
  //////////////////////////////////////////////////////////////////////////////
  // Control whether an instruction from either slot should be issued to the FPU
  // pipe  Instructions from slot 0 always take priority
  // * At this time we only issue instructions with the FU_FPU functional unit
  // type to this pipe
  //////////////////////////////////////////////////////////////////////////////
  always @ (schedule0 or decode_packet0op_FU_type or decode_packet0is_valid or stall_in_2 or schedule1 or decode_packet1op_FU_type or decode_packet1is_valid) begin: FPU_ISSUE
    if ( schedule0 && ( decode_packet0op_FU_type == 4'b0011) &&
         decode_packet0is_valid
       ) begin
      valid_fpu = !stall_in_2;
      choice_fpu = 1'b0;
    end
    else begin
      valid_fpu = ( schedule1 && ( decode_packet1op_FU_type == 4'b0011) &&
                    decode_packet1is_valid && !stall_in_2
                  );
      choice_fpu = 1'b1;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
 
  //////////////////////////////////////////////////////////////////////////////
  // MEM_ISSUE
  //////////////////////////////////////////////////////////////////////////////
  // Control whether an instruction from either slot should be issued to the MEM
  // pipe  Instructions from slot 0 always take priority
  // * At this time we only issue instructions with the FU_MEM functional unit
  // type to this pipe
  //////////////////////////////////////////////////////////////////////////////
  always @ (schedule0 or decode_packet0op_FU_type or decode_packet0is_valid or stall_in_1 or schedule1 or decode_packet1op_FU_type or decode_packet1is_valid) begin: MEM_ISSUE
     if ( schedule0 && ( decode_packet0op_FU_type == 4'b1000) &&
          decode_packet0is_valid
        ) begin
      valid_mem = !stall_in_1;
      choice_mem = 1'b0;
    end
    else begin
      valid_mem = ( schedule1 && ( decode_packet1op_FU_type == 4'b1000) && 
                    decode_packet1is_valid && !stall_in_1
                  );
      choice_mem = 1'b1;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
  assign issue0 = fetch2decodeinstr0_valid && (!decode_packet0is_valid || 
        (valid_mem &&!choice_mem) || (valid_fpu && !choice_fpu) || (valid_int && !choice_int));
  assign issue1 = fetch2decodeinstr1_valid && (((issue0 || instr0_issued || !fetch2decodeinstr0_valid) && !decode_packet1is_valid) ||
        (valid_mem && choice_mem) || (valid_fpu && choice_fpu) || (valid_int && choice_int));

  //////////////////////////////////////////////////////////////////////////////
  // DECODE_OUT
  //////////////////////////////////////////////////////////////////////////////
  // Latch outputs for the DECODE pipeline stage
  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk or posedge reset) begin: DECODE_OUT
    // Reset the signals
    if(reset) begin
      decode2intdecode_packetis_halt <= 1'b0;
      decode2fpudecode_packetis_halt <= 1'b0;
      decode2memdecode_packetis_halt <= 1'b0;
      instr0_issued <= 1'b0;
      decode2intvalid <= 1'b0;
      decode2fpuvalid <= 1'b0;
      decode2memvalid <= 1'b0;
    end
    else begin 
      if(exec_stall) begin
        //decode2int <= decode2int;
        decode2intpc <= decode2intpc;
        decode2intpc_incr <= decode2intpc_incr;
        decode2intthread_num <= decode2intthread_num;
        decode2intbranch_taken <= decode2intbranch_taken;
        decode2intpredicted_pc <= decode2intpredicted_pc;
        decode2intvalid <= decode2intvalid;
        decode2intis_oldest <= decode2intis_oldest;
        decode2intsreg_t_data <= decode2intsreg_t_data;
        decode2intsreg_s_data <= decode2intsreg_s_data;
        decode2intresult <= decode2intresult;
        decode2intL1D_hit <= decode2intL1D_hit;
        decode2intdecode_packetsreg_t <= decode2intdecode_packetsreg_t;
        decode2intdecode_packetsreg_s <= decode2intdecode_packetsreg_s;
        decode2intdecode_packetdreg <= decode2intdecode_packetdreg;
        decode2intdecode_packetacc <= decode2intdecode_packetacc;
        decode2intdecode_packetimm16_value <= decode2intdecode_packetimm16_value;
        decode2intdecode_packetimm26_value <= decode2intdecode_packetimm26_value;
        decode2intdecode_packetimm5_value <= decode2intdecode_packetimm5_value;
        decode2intdecode_packetop_FU_type <= decode2intdecode_packetop_FU_type;
        decode2intdecode_packetalu_op <= decode2intdecode_packetalu_op;
        decode2intdecode_packetfpu_op <= decode2intdecode_packetfpu_op;
        decode2intdecode_packetop_MEM_type <= decode2intdecode_packetop_MEM_type;
        decode2intdecode_packetop_BR_type <= decode2intdecode_packetop_BR_type;
        decode2intdecode_packetshift_type <= decode2intdecode_packetshift_type;
        decode2intdecode_packetcompare_type <= decode2intdecode_packetcompare_type;
        decode2intdecode_packetis_imm26 <= decode2intdecode_packetis_imm26;
        decode2intdecode_packetis_imm <= decode2intdecode_packetis_imm;
        decode2intdecode_packetis_imm5 <= decode2intdecode_packetis_imm5;
        decode2intdecode_packetis_signed <= decode2intdecode_packetis_signed;
        decode2intdecode_packetcarry_in <= decode2intdecode_packetcarry_in;
        decode2intdecode_packetcarry_out <= decode2intdecode_packetcarry_out;
        decode2intdecode_packethas_sreg_t <= decode2intdecode_packethas_sreg_t;
        decode2intdecode_packethas_sreg_s <= decode2intdecode_packethas_sreg_s;
        decode2intdecode_packethas_dreg <= decode2intdecode_packethas_dreg;
        decode2intdecode_packetis_prefetch <= decode2intdecode_packetis_prefetch;
        decode2intdecode_packetsprf_dest <= decode2intdecode_packetsprf_dest;
        decode2intdecode_packetsprf_src <= decode2intdecode_packetsprf_src;
        decode2intdecode_packetis_atomic <= decode2intdecode_packetis_atomic;
        decode2intdecode_packetis_ldl <= decode2intdecode_packetis_ldl;
        decode2intdecode_packetis_stl <= decode2intdecode_packetis_stl;
        decode2intdecode_packetis_stc <= decode2intdecode_packetis_stc;
        decode2intdecode_packetis_valid <= decode2intdecode_packetis_valid;
        decode2intdecode_packetis_halt <= decode2intdecode_packetis_halt;
        decode2intdecode_packetis_compare <= decode2intdecode_packetis_compare;
        decode2intdecode_packetdreg_is_src <= decode2intdecode_packetdreg_is_src;
        decode2intdecode_packetdreg_is_dest <= decode2intdecode_packetdreg_is_dest;
		//decode2fpu <= decode2fpu;
        decode2fpupc <= decode2fpupc;
        decode2fpupc_incr <= decode2fpupc_incr;
        decode2fputhread_num <= decode2fputhread_num;
        decode2fpubranch_taken <= decode2fpubranch_taken;
        decode2fpupredicted_pc <= decode2fpupredicted_pc;
        decode2fpuvalid <= decode2fpuvalid;
        decode2fpuis_oldest <= decode2fpuis_oldest;
        decode2fpusreg_t_data <= decode2fpusreg_t_data;
        decode2fpusreg_s_data <= decode2fpusreg_s_data;
        decode2fpuresult <= decode2fpuresult;
        decode2fpuL1D_hit <= decode2fpuL1D_hit;
        decode2fpudecode_packetsreg_t <= decode2fpudecode_packetsreg_t;
        decode2fpudecode_packetsreg_s <= decode2fpudecode_packetsreg_s;
        decode2fpudecode_packetdreg <= decode2fpudecode_packetdreg;
        decode2fpudecode_packetacc <= decode2fpudecode_packetacc;
        decode2fpudecode_packetimm16_value <= decode2fpudecode_packetimm16_value;
        decode2fpudecode_packetimm26_value <= decode2fpudecode_packetimm26_value;
        decode2fpudecode_packetimm5_value <= decode2fpudecode_packetimm5_value;
        decode2fpudecode_packetop_FU_type <= decode2fpudecode_packetop_FU_type;
        decode2fpudecode_packetalu_op <= decode2fpudecode_packetalu_op;
        decode2fpudecode_packetfpu_op <= decode2fpudecode_packetfpu_op;
        decode2fpudecode_packetop_MEM_type <= decode2fpudecode_packetop_MEM_type;
        decode2fpudecode_packetop_BR_type <= decode2fpudecode_packetop_BR_type;
        decode2fpudecode_packetshift_type <= decode2fpudecode_packetshift_type;
        decode2fpudecode_packetcompare_type <= decode2fpudecode_packetcompare_type;
        decode2fpudecode_packetis_imm26 <= decode2fpudecode_packetis_imm26;
        decode2fpudecode_packetis_imm <= decode2fpudecode_packetis_imm;
        decode2fpudecode_packetis_imm5 <= decode2fpudecode_packetis_imm5;
        decode2fpudecode_packetis_signed <= decode2fpudecode_packetis_signed;
        decode2fpudecode_packetcarry_in <= decode2fpudecode_packetcarry_in;
        decode2fpudecode_packetcarry_out <= decode2fpudecode_packetcarry_out;
        decode2fpudecode_packethas_sreg_t <= decode2fpudecode_packethas_sreg_t;
        decode2fpudecode_packethas_sreg_s <= decode2fpudecode_packethas_sreg_s;
        decode2fpudecode_packethas_dreg <= decode2fpudecode_packethas_dreg;
        decode2fpudecode_packetis_prefetch <= decode2fpudecode_packetis_prefetch;
        decode2fpudecode_packetsprf_dest <= decode2fpudecode_packetsprf_dest;
        decode2fpudecode_packetsprf_src <= decode2fpudecode_packetsprf_src;
        decode2fpudecode_packetis_atomic <= decode2fpudecode_packetis_atomic;
        decode2fpudecode_packetis_ldl <= decode2fpudecode_packetis_ldl;
        decode2fpudecode_packetis_stl <= decode2fpudecode_packetis_stl;
        decode2fpudecode_packetis_stc <= decode2fpudecode_packetis_stc;
        decode2fpudecode_packetis_valid <= decode2fpudecode_packetis_valid;
        decode2fpudecode_packetis_halt <= decode2fpudecode_packetis_halt;
        decode2fpudecode_packetis_compare <= decode2fpudecode_packetis_compare;
        decode2fpudecode_packetdreg_is_src <= decode2fpudecode_packetdreg_is_src;
        decode2fpudecode_packetdreg_is_dest <= decode2fpudecode_packetdreg_is_dest;
		//decode2mem <= decode2mem;
        decode2mempc <= decode2mempc;
        decode2mempc_incr <= decode2mempc_incr;
        decode2memthread_num <= decode2memthread_num;
        decode2membranch_taken <= decode2membranch_taken;
        decode2mempredicted_pc <= decode2mempredicted_pc;
        decode2memvalid <= decode2memvalid;
        decode2memis_oldest <= decode2memis_oldest;
        decode2memsreg_t_data <= decode2memsreg_t_data;
        decode2memsreg_s_data <= decode2memsreg_s_data;
        decode2memresult <= decode2memresult;
        decode2memL1D_hit <= decode2memL1D_hit;
        decode2memdecode_packetsreg_t <= decode2memdecode_packetsreg_t;
        decode2memdecode_packetsreg_s <= decode2memdecode_packetsreg_s;
        decode2memdecode_packetdreg <= decode2memdecode_packetdreg;
        decode2memdecode_packetacc <= decode2memdecode_packetacc;
        decode2memdecode_packetimm16_value <= decode2memdecode_packetimm16_value;
        decode2memdecode_packetimm26_value <= decode2memdecode_packetimm26_value;
        decode2memdecode_packetimm5_value <= decode2memdecode_packetimm5_value;
        decode2memdecode_packetop_FU_type <= decode2memdecode_packetop_FU_type;
        decode2memdecode_packetalu_op <= decode2memdecode_packetalu_op;
        decode2memdecode_packetfpu_op <= decode2memdecode_packetfpu_op;
        decode2memdecode_packetop_MEM_type <= decode2memdecode_packetop_MEM_type;
        decode2memdecode_packetop_BR_type <= decode2memdecode_packetop_BR_type;
        decode2memdecode_packetshift_type <= decode2memdecode_packetshift_type;
        decode2memdecode_packetcompare_type <= decode2memdecode_packetcompare_type;
        decode2memdecode_packetis_imm26 <= decode2memdecode_packetis_imm26;
        decode2memdecode_packetis_imm <= decode2memdecode_packetis_imm;
        decode2memdecode_packetis_imm5 <= decode2memdecode_packetis_imm5;
        decode2memdecode_packetis_signed <= decode2memdecode_packetis_signed;
        decode2memdecode_packetcarry_in <= decode2memdecode_packetcarry_in;
        decode2memdecode_packetcarry_out <= decode2memdecode_packetcarry_out;
        decode2memdecode_packethas_sreg_t <= decode2memdecode_packethas_sreg_t;
        decode2memdecode_packethas_sreg_s <= decode2memdecode_packethas_sreg_s;
        decode2memdecode_packethas_dreg <= decode2memdecode_packethas_dreg;
        decode2memdecode_packetis_prefetch <= decode2memdecode_packetis_prefetch;
        decode2memdecode_packetsprf_dest <= decode2memdecode_packetsprf_dest;
        decode2memdecode_packetsprf_src <= decode2memdecode_packetsprf_src;
        decode2memdecode_packetis_atomic <= decode2memdecode_packetis_atomic;
        decode2memdecode_packetis_ldl <= decode2memdecode_packetis_ldl;
        decode2memdecode_packetis_stl <= decode2memdecode_packetis_stl;
        decode2memdecode_packetis_stc <= decode2memdecode_packetis_stc;
        decode2memdecode_packetis_valid <= decode2memdecode_packetis_valid;
        decode2memdecode_packetis_halt <= decode2memdecode_packetis_halt;
        decode2memdecode_packetis_compare <= decode2memdecode_packetis_compare;
        decode2memdecode_packetdreg_is_src <= decode2memdecode_packetdreg_is_src;
        decode2memdecode_packetdreg_is_dest <= decode2memdecode_packetdreg_is_dest;
        instr0_issued <= instr0_issued;
      end else begin
        decode2intvalid <= valid_int && !issue_halt && !branch_mispredict && fetch2decodevalid;
        decode2fpuvalid <= valid_fpu && !issue_halt && !branch_mispredict && fetch2decodevalid && (!follows_vld_branch);
        decode2memvalid <= valid_mem && !issue_halt && !branch_mispredict && fetch2decodevalid && (!follows_vld_branch);
      
        // Branches are handled inside the INT pipe
        decode2intbranch_taken <= fetch2decodebranch_taken;
        decode2intpredicted_pc <= fetch2decodepredicted_pc;

      //////////////////////////////////////////////////////////////////////////
      // INT Pipeline Dispatch
      //////////////////////////////////////////////////////////////////////////
      // Select which slot gets issued to the INT pipe  Slot 0 takes priority
      //////////////////////////////////////////////////////////////////////////
      if( choice_int) begin                           // Issue from slot 1
          //decode2intdecode_packet <= decode_packet1;
          decode2intdecode_packetsreg_t <= decode_packet1sreg_t;
          decode2intdecode_packetsreg_s <= decode_packet1sreg_s;
          decode2intdecode_packetdreg <= decode_packet1dreg;
          decode2intdecode_packetacc <= decode_packet1acc;
          decode2intdecode_packetimm16_value <= decode_packet1imm16_value;
          decode2intdecode_packetimm26_value <= decode_packet1imm26_value;
          decode2intdecode_packetimm5_value <= decode_packet1imm5_value;
          decode2intdecode_packetop_FU_type <= decode_packet1op_FU_type;
          decode2intdecode_packetalu_op <= decode_packet1alu_op;
          decode2intdecode_packetfpu_op <= decode_packet1fpu_op;
          decode2intdecode_packetop_MEM_type <= decode_packet1op_MEM_type;
          decode2intdecode_packetop_BR_type <= decode_packet1op_BR_type;
          decode2intdecode_packetshift_type <= decode_packet1shift_type;
          decode2intdecode_packetcompare_type <= decode_packet1compare_type;
          decode2intdecode_packetis_imm26 <= decode_packet1is_imm26;
          decode2intdecode_packetis_imm <= decode_packet1is_imm;
          decode2intdecode_packetis_imm5 <= decode_packet1is_imm5;
          decode2intdecode_packetis_signed <= decode_packet1is_signed;
          decode2intdecode_packetcarry_in <= decode_packet1carry_in;
          decode2intdecode_packetcarry_out <= decode_packet1carry_out;
          decode2intdecode_packethas_sreg_t <= decode_packet1has_sreg_t;
          decode2intdecode_packethas_sreg_s <= decode_packet1has_sreg_s;
          decode2intdecode_packethas_dreg <= decode_packet1has_dreg;
          decode2intdecode_packetis_prefetch <= decode_packet1is_prefetch;
          decode2intdecode_packetsprf_dest <= decode_packet1sprf_dest;
          decode2intdecode_packetsprf_src <= decode_packet1sprf_src;
          decode2intdecode_packetis_atomic <= decode_packet1is_atomic;
          decode2intdecode_packetis_ldl <= decode_packet1is_ldl;
          decode2intdecode_packetis_stl <= decode_packet1is_stl;
          decode2intdecode_packetis_stc <= decode_packet1is_stc;
          decode2intdecode_packetis_valid <= decode_packet1is_valid;
          decode2intdecode_packetis_halt <= decode_packet1is_halt;
          decode2intdecode_packetis_compare <= decode_packet1is_compare;
          decode2intdecode_packetdreg_is_src <= decode_packet1dreg_is_src;
          decode2intdecode_packetdreg_is_dest <= decode_packet1dreg_is_dest;
          if (sprf_src) begin
            decode2intsreg_t_data <= sprf_sreg_data;
          end else begin
            decode2intsreg_t_data <= sreg_t_1;
          end
          decode2intsreg_s_data <= sreg_s_1;
          decode2intpc          <= fetch2decodepc1;
          decode2intpc_incr     <= fetch2decodepc1_incr;
          decode2intis_oldest   <= !issue0;
      end else begin                                 // Issue from slot 0
          // decode2intdecode_packet <= decode_packet0;
          decode2intdecode_packetsreg_t <= decode_packet0sreg_t;
          decode2intdecode_packetsreg_s <= decode_packet0sreg_s;
          decode2intdecode_packetdreg <= decode_packet0dreg;
          decode2intdecode_packetacc <= decode_packet0acc;
          decode2intdecode_packetimm16_value <= decode_packet0imm16_value;
          decode2intdecode_packetimm26_value <= decode_packet0imm26_value;
          decode2intdecode_packetimm5_value <= decode_packet0imm5_value;
          decode2intdecode_packetop_FU_type <= decode_packet0op_FU_type;
          decode2intdecode_packetalu_op <= decode_packet0alu_op;
          decode2intdecode_packetfpu_op <= decode_packet0fpu_op;
          decode2intdecode_packetop_MEM_type <= decode_packet0op_MEM_type;
          decode2intdecode_packetop_BR_type <= decode_packet0op_BR_type;
          decode2intdecode_packetshift_type <= decode_packet0shift_type;
          decode2intdecode_packetcompare_type <= decode_packet0compare_type;
          decode2intdecode_packetis_imm26 <= decode_packet0is_imm26;
          decode2intdecode_packetis_imm <= decode_packet0is_imm;
          decode2intdecode_packetis_imm5 <= decode_packet0is_imm5;
          decode2intdecode_packetis_signed <= decode_packet0is_signed;
          decode2intdecode_packetcarry_in <= decode_packet0carry_in;
          decode2intdecode_packetcarry_out <= decode_packet0carry_out;
          decode2intdecode_packethas_sreg_t <= decode_packet0has_sreg_t;
          decode2intdecode_packethas_sreg_s <= decode_packet0has_sreg_s;
          decode2intdecode_packethas_dreg <= decode_packet0has_dreg;
          decode2intdecode_packetis_prefetch <= decode_packet0is_prefetch;
          decode2intdecode_packetsprf_dest <= decode_packet0sprf_dest;
          decode2intdecode_packetsprf_src <= decode_packet0sprf_src;
          decode2intdecode_packetis_atomic <= decode_packet0is_atomic;
          decode2intdecode_packetis_ldl <= decode_packet0is_ldl;
          decode2intdecode_packetis_stl <= decode_packet0is_stl;
          decode2intdecode_packetis_stc <= decode_packet0is_stc;
          decode2intdecode_packetis_valid <= decode_packet0is_valid;
          decode2intdecode_packetis_halt <= decode_packet0is_halt;
          decode2intdecode_packetis_compare <= decode_packet0is_compare;
          decode2intdecode_packetdreg_is_src <= decode_packet0dreg_is_src;
          decode2intdecode_packetdreg_is_dest <= decode_packet0dreg_is_dest;
          if (sprf_src) begin
            decode2intsreg_t_data <= sprf_sreg_data;
          end else begin
            decode2intsreg_t_data <= sreg_t_0;
          end
          decode2intsreg_s_data <= sreg_s_0;
          decode2intpc          <= fetch2decodepc0;
          decode2intpc_incr     <= fetch2decodepc0_incr;
          decode2intis_oldest   <= 1'b1;
      end
      //////////////////////////////////////////////////////////////////////////
     
      //////////////////////////////////////////////////////////////////////////
      // FPU Pipeline Dispatch
      //////////////////////////////////////////////////////////////////////////
      // Select which slot gets issued to the FPU pipe  Slot 0 takes priority
      //////////////////////////////////////////////////////////////////////////
      if( choice_fpu) begin                         // Issue from slot 1
          //decode2fpudecode_packet <= decode_packet1;
          decode2fpudecode_packetsreg_t <= decode_packet1sreg_t;
          decode2fpudecode_packetsreg_s <= decode_packet1sreg_s;
          decode2fpudecode_packetdreg <= decode_packet1dreg;
          decode2fpudecode_packetacc <= decode_packet1acc;
          decode2fpudecode_packetimm16_value <= decode_packet1imm16_value;
          decode2fpudecode_packetimm26_value <= decode_packet1imm26_value;
          decode2fpudecode_packetimm5_value <= decode_packet1imm5_value;
          decode2fpudecode_packetop_FU_type <= decode_packet1op_FU_type;
          decode2fpudecode_packetalu_op <= decode_packet1alu_op;
          decode2fpudecode_packetfpu_op <= decode_packet1fpu_op;
          decode2fpudecode_packetop_MEM_type <= decode_packet1op_MEM_type;
          decode2fpudecode_packetop_BR_type <= decode_packet1op_BR_type;
          decode2fpudecode_packetshift_type <= decode_packet1shift_type;
          decode2fpudecode_packetcompare_type <= decode_packet1compare_type;
          decode2fpudecode_packetis_imm26 <= decode_packet1is_imm26;
          decode2fpudecode_packetis_imm <= decode_packet1is_imm;
          decode2fpudecode_packetis_imm5 <= decode_packet1is_imm5;
          decode2fpudecode_packetis_signed <= decode_packet1is_signed;
          decode2fpudecode_packetcarry_in <= decode_packet1carry_in;
          decode2fpudecode_packetcarry_out <= decode_packet1carry_out;
          decode2fpudecode_packethas_sreg_t <= decode_packet1has_sreg_t;
          decode2fpudecode_packethas_sreg_s <= decode_packet1has_sreg_s;
          decode2fpudecode_packethas_dreg <= decode_packet1has_dreg;
          decode2fpudecode_packetis_prefetch <= decode_packet1is_prefetch;
          decode2fpudecode_packetsprf_dest <= decode_packet1sprf_dest;
          decode2fpudecode_packetsprf_src <= decode_packet1sprf_src;
          decode2fpudecode_packetis_atomic <= decode_packet1is_atomic;
          decode2fpudecode_packetis_ldl <= decode_packet1is_ldl;
          decode2fpudecode_packetis_stl <= decode_packet1is_stl;
          decode2fpudecode_packetis_stc <= decode_packet1is_stc;
          decode2fpudecode_packetis_valid <= decode_packet1is_valid;
          decode2fpudecode_packetis_halt <= decode_packet1is_halt;
          decode2fpudecode_packetis_compare <= decode_packet1is_compare;
          decode2fpudecode_packetdreg_is_src <= decode_packet1dreg_is_src;
          decode2fpudecode_packetdreg_is_dest <= decode_packet1dreg_is_dest;
          decode2fpusreg_t_data <= sreg_t_1;
          decode2fpusreg_s_data <= sreg_s_1;
          decode2fpupc          <= fetch2decodepc1;
          decode2fpupc_incr     <= fetch2decodepc1_incr;
          decode2fpuis_oldest   <= !issue0;
      end else begin                                // Issue from slot 0
          //decode2fpudecode_packet <= decode_packet0;
          decode2fpudecode_packetsreg_t <= decode_packet0sreg_t;
          decode2fpudecode_packetsreg_s <= decode_packet0sreg_s;
          decode2fpudecode_packetdreg <= decode_packet0dreg;
          decode2fpudecode_packetacc <= decode_packet0acc;
          decode2fpudecode_packetimm16_value <= decode_packet0imm16_value;
          decode2fpudecode_packetimm26_value <= decode_packet0imm26_value;
          decode2fpudecode_packetimm5_value <= decode_packet0imm5_value;
          decode2fpudecode_packetop_FU_type <= decode_packet0op_FU_type;
          decode2fpudecode_packetalu_op <= decode_packet0alu_op;
          decode2fpudecode_packetfpu_op <= decode_packet0fpu_op;
          decode2fpudecode_packetop_MEM_type <= decode_packet0op_MEM_type;
          decode2fpudecode_packetop_BR_type <= decode_packet0op_BR_type;
          decode2fpudecode_packetshift_type <= decode_packet0shift_type;
          decode2fpudecode_packetcompare_type <= decode_packet0compare_type;
          decode2fpudecode_packetis_imm26 <= decode_packet0is_imm26;
          decode2fpudecode_packetis_imm <= decode_packet0is_imm;
          decode2fpudecode_packetis_imm5 <= decode_packet0is_imm5;
          decode2fpudecode_packetis_signed <= decode_packet0is_signed;
          decode2fpudecode_packetcarry_in <= decode_packet0carry_in;
          decode2fpudecode_packetcarry_out <= decode_packet0carry_out;
          decode2fpudecode_packethas_sreg_t <= decode_packet0has_sreg_t;
          decode2fpudecode_packethas_sreg_s <= decode_packet0has_sreg_s;
          decode2fpudecode_packethas_dreg <= decode_packet0has_dreg;
          decode2fpudecode_packetis_prefetch <= decode_packet0is_prefetch;
          decode2fpudecode_packetsprf_dest <= decode_packet0sprf_dest;
          decode2fpudecode_packetsprf_src <= decode_packet0sprf_src;
          decode2fpudecode_packetis_atomic <= decode_packet0is_atomic;
          decode2fpudecode_packetis_ldl <= decode_packet0is_ldl;
          decode2fpudecode_packetis_stl <= decode_packet0is_stl;
          decode2fpudecode_packetis_stc <= decode_packet0is_stc;
          decode2fpudecode_packetis_valid <= decode_packet0is_valid;
          decode2fpudecode_packetis_halt <= decode_packet0is_halt;
          decode2fpudecode_packetis_compare <= decode_packet0is_compare;
          decode2fpudecode_packetdreg_is_src <= decode_packet0dreg_is_src;
          decode2fpudecode_packetdreg_is_dest <= decode_packet0dreg_is_dest;
          decode2fpusreg_t_data <= sreg_t_0;
          decode2fpusreg_s_data <= sreg_s_0;
          decode2fpupc          <= fetch2decodepc0;
          decode2fpupc_incr     <= fetch2decodepc0_incr;
          decode2fpuis_oldest   <= 1'b1;
      end
      //////////////////////////////////////////////////////////////////////////
 
      //////////////////////////////////////////////////////////////////////////
      // MEM Pipeline Dispatch
      //////////////////////////////////////////////////////////////////////////
      // Select which slot gets issued to the MEM pipe  Slot 0 takes priority
      //////////////////////////////////////////////////////////////////////////
      if( choice_mem) begin                         // Issue from slot 1
          //decode2memdecode_packet <= decode_packet1;
          decode2memdecode_packetsreg_t <= decode_packet1sreg_t;
          decode2memdecode_packetsreg_s <= decode_packet1sreg_s;
          decode2memdecode_packetdreg <= decode_packet1dreg;
          decode2memdecode_packetacc <= decode_packet1acc;
          decode2memdecode_packetimm16_value <= decode_packet1imm16_value;
          decode2memdecode_packetimm26_value <= decode_packet1imm26_value;
          decode2memdecode_packetimm5_value <= decode_packet1imm5_value;
          decode2memdecode_packetop_FU_type <= decode_packet1op_FU_type;
          decode2memdecode_packetalu_op <= decode_packet1alu_op;
          decode2memdecode_packetfpu_op <= decode_packet1fpu_op;
          decode2memdecode_packetop_MEM_type <= decode_packet1op_MEM_type;
          decode2memdecode_packetop_BR_type <= decode_packet1op_BR_type;
          decode2memdecode_packetshift_type <= decode_packet1shift_type;
          decode2memdecode_packetcompare_type <= decode_packet1compare_type;
          decode2memdecode_packetis_imm26 <= decode_packet1is_imm26;
          decode2memdecode_packetis_imm <= decode_packet1is_imm;
          decode2memdecode_packetis_imm5 <= decode_packet1is_imm5;
          decode2memdecode_packetis_signed <= decode_packet1is_signed;
          decode2memdecode_packetcarry_in <= decode_packet1carry_in;
          decode2memdecode_packetcarry_out <= decode_packet1carry_out;
          decode2memdecode_packethas_sreg_t <= decode_packet1has_sreg_t;
          decode2memdecode_packethas_sreg_s <= decode_packet1has_sreg_s;
          decode2memdecode_packethas_dreg <= decode_packet1has_dreg;
          decode2memdecode_packetis_prefetch <= decode_packet1is_prefetch;
          decode2memdecode_packetsprf_dest <= decode_packet1sprf_dest;
          decode2memdecode_packetsprf_src <= decode_packet1sprf_src;
          decode2memdecode_packetis_atomic <= decode_packet1is_atomic;
          decode2memdecode_packetis_ldl <= decode_packet1is_ldl;
          decode2memdecode_packetis_stl <= decode_packet1is_stl;
          decode2memdecode_packetis_stc <= decode_packet1is_stc;
          decode2memdecode_packetis_valid <= decode_packet1is_valid;
          decode2memdecode_packetis_halt <= decode_packet1is_halt;
          decode2memdecode_packetis_compare <= decode_packet1is_compare;
          decode2memdecode_packetdreg_is_src <= decode_packet1dreg_is_src;
          decode2memdecode_packetdreg_is_dest <= decode_packet1dreg_is_dest;
          decode2memsreg_t_data <= sreg_t_1;
          decode2memsreg_s_data <= sreg_s_1;
          decode2mempc          <= fetch2decodepc1;
          decode2mempc_incr     <= fetch2decodepc1_incr;
          decode2memis_oldest   <= !issue0;
      end else begin                                // Issue from slot 0
          //decode2memdecode_packet <= decode_packet0;
          decode2memdecode_packetsreg_t <= decode_packet0sreg_t;
          decode2memdecode_packetsreg_s <= decode_packet0sreg_s;
          decode2memdecode_packetdreg <= decode_packet0dreg;
          decode2memdecode_packetacc <= decode_packet0acc;
          decode2memdecode_packetimm16_value <= decode_packet0imm16_value;
          decode2memdecode_packetimm26_value <= decode_packet0imm26_value;
          decode2memdecode_packetimm5_value <= decode_packet0imm5_value;
          decode2memdecode_packetop_FU_type <= decode_packet0op_FU_type;
          decode2memdecode_packetalu_op <= decode_packet0alu_op;
          decode2memdecode_packetfpu_op <= decode_packet0fpu_op;
          decode2memdecode_packetop_MEM_type <= decode_packet0op_MEM_type;
          decode2memdecode_packetop_BR_type <= decode_packet0op_BR_type;
          decode2memdecode_packetshift_type <= decode_packet0shift_type;
          decode2memdecode_packetcompare_type <= decode_packet0compare_type;
          decode2memdecode_packetis_imm26 <= decode_packet0is_imm26;
          decode2memdecode_packetis_imm <= decode_packet0is_imm;
          decode2memdecode_packetis_imm5 <= decode_packet0is_imm5;
          decode2memdecode_packetis_signed <= decode_packet0is_signed;
          decode2memdecode_packetcarry_in <= decode_packet0carry_in;
          decode2memdecode_packetcarry_out <= decode_packet0carry_out;
          decode2memdecode_packethas_sreg_t <= decode_packet0has_sreg_t;
          decode2memdecode_packethas_sreg_s <= decode_packet0has_sreg_s;
          decode2memdecode_packethas_dreg <= decode_packet0has_dreg;
          decode2memdecode_packetis_prefetch <= decode_packet0is_prefetch;
          decode2memdecode_packetsprf_dest <= decode_packet0sprf_dest;
          decode2memdecode_packetsprf_src <= decode_packet0sprf_src;
          decode2memdecode_packetis_atomic <= decode_packet0is_atomic;
          decode2memdecode_packetis_ldl <= decode_packet0is_ldl;
          decode2memdecode_packetis_stl <= decode_packet0is_stl;
          decode2memdecode_packetis_stc <= decode_packet0is_stc;
          decode2memdecode_packetis_valid <= decode_packet0is_valid;
          decode2memdecode_packetis_halt <= decode_packet0is_halt;
          decode2memdecode_packetis_compare <= decode_packet0is_compare;
          decode2memdecode_packetdreg_is_src <= decode_packet0dreg_is_src;
          decode2memdecode_packetdreg_is_dest <= decode_packet0dreg_is_dest;
          decode2memsreg_t_data <= sreg_t_0;
          decode2memsreg_s_data <= sreg_s_0;
          decode2mempc          <= fetch2decodepc0;
          decode2mempc_incr     <= fetch2decodepc0_incr;
          decode2memis_oldest   <= 1'b1;
      end
      //////////////////////////////////////////////////////////////////////////

      // Propagate the HALT signal if needed
      decode2intdecode_packetis_halt <= issue_halt && !branch_mispredict;
      decode2memdecode_packetis_halt <= issue_halt && !branch_mispredict;
      decode2fpudecode_packetis_halt <= issue_halt && !branch_mispredict;

      if( !issue1 ) begin
        instr0_issued <= !branch_mispredict && (instr0_issued || issue0);
      end
      else begin
        instr0_issued <= 1'b0;
      end
    end
    end
  end
endmodule

/////////////////////////////////////////////////////////////////////////////
// NOTE: This Decoder is AUTO-GENERATED! DO NOT EDIT BY HAND! 
/////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
// A decoder unit for the Rigel architecture
////////////////////////////////////////////////////////////////////
module decoder0(
  fetch2decodeinstr1_out,
  //output decode_packet_t instr_packet
  decode_packet1sreg_t,
  decode_packet1sreg_s,
  decode_packet1dreg,
  decode_packet1acc,
  decode_packet1imm16_value,
  decode_packet1imm26_value,
  decode_packet1imm5_value,
  decode_packet1op_FU_type,
  decode_packet1alu_op,
  decode_packet1fpu_op,
  decode_packet1op_MEM_type,
  decode_packet1op_BR_type,
  decode_packet1shift_type,
  decode_packet1compare_type,
  decode_packet1is_imm26,
  decode_packet1is_imm,
  decode_packet1is_imm5,
  decode_packet1is_signed,
  decode_packet1carry_in,
  decode_packet1carry_out,
  decode_packet1has_sreg_t,
  decode_packet1has_sreg_s,
  decode_packet1has_dreg,
  decode_packet1is_prefetch,
  decode_packet1sprf_dest,
  decode_packet1sprf_src,
  decode_packet1is_atomic,
  decode_packet1is_ldl,
  decode_packet1is_stl,
  decode_packet1is_stc,
  decode_packet1is_valid,
  decode_packet1is_halt,
  decode_packet1is_compare,
  decode_packet1dreg_is_src,
  decode_packet1dreg_is_dest
);

  input [31:0] fetch2decodeinstr1_out;
  //output instr_packet
  output [4:0] decode_packet1sreg_t;
  output [4:0] decode_packet1sreg_s;
  output [4:0] decode_packet1dreg;
  output [1:0] decode_packet1acc;
  output [15:0] decode_packet1imm16_value;
  output [25:0] decode_packet1imm26_value;
  output [4:0] decode_packet1imm5_value;
  output [3:0] decode_packet1op_FU_type;
  output [4:0] decode_packet1alu_op;
  output [4:0] decode_packet1fpu_op;
  output [3:0] decode_packet1op_MEM_type;
  output [1:0] decode_packet1op_BR_type;
  output [1:0] decode_packet1shift_type;
  output [2:0] decode_packet1compare_type;
  output  decode_packet1is_imm26;
  output  decode_packet1is_imm;
  output  decode_packet1is_imm5;
  output  decode_packet1is_signed;
  output  decode_packet1carry_in;
  output  decode_packet1carry_out;
  output  decode_packet1has_sreg_t;
  output  decode_packet1has_sreg_s;
  output  decode_packet1has_dreg;
  output  decode_packet1is_prefetch;
  output  decode_packet1sprf_dest;
  output  decode_packet1sprf_src;
  output  decode_packet1is_atomic;
  output  decode_packet1is_ldl;
  output  decode_packet1is_stl;
  output  decode_packet1is_stc;
  output  decode_packet1is_valid;
  output  decode_packet1is_halt;
  output  decode_packet1is_compare;
  output  decode_packet1dreg_is_src;
  output  decode_packet1dreg_is_dest;

//====================================
  reg [4:0] decode_packet1sreg_t;
  reg [4:0] decode_packet1sreg_s;
  reg [4:0] decode_packet1dreg;
  reg [1:0] decode_packet1acc;
  reg [15:0] decode_packet1imm16_value;
  reg [25:0] decode_packet1imm26_value;
  reg [4:0] decode_packet1imm5_value;
  reg [3:0] decode_packet1op_FU_type;
  reg [4:0] decode_packet1alu_op;
  reg [4:0] decode_packet1fpu_op;
  reg [3:0] decode_packet1op_MEM_type;
  reg [1:0] decode_packet1op_BR_type;
  reg [1:0] decode_packet1shift_type;
  reg [2:0] decode_packet1compare_type;
  reg  decode_packet1is_imm26;
  reg  decode_packet1is_imm;
  reg  decode_packet1is_imm5;
  reg  decode_packet1is_signed;
  reg  decode_packet1carry_in;
  reg  decode_packet1carry_out;
  reg  decode_packet1has_sreg_t;
  reg  decode_packet1has_sreg_s;
  reg  decode_packet1has_dreg;
  reg  decode_packet1is_prefetch;
  reg  decode_packet1sprf_dest;
  reg  decode_packet1sprf_src;
  reg  decode_packet1is_atomic;
  reg  decode_packet1is_ldl;
  reg  decode_packet1is_stl;
  reg  decode_packet1is_stc;
  reg  decode_packet1is_valid;
  reg  decode_packet1is_halt;
  reg  decode_packet1is_compare;
  reg  decode_packet1dreg_is_src;
  reg  decode_packet1dreg_is_dest;


//====================================
  wire [3:0] decode_optype;
  wire [1:0] decode_opc;
  wire [1:0] decode_opc26;
  wire [10:0] decode_opcode;
  reg has_sreg_s;
  reg has_dreg;

// set instruction-agnostic outputs with continuous assignments 
  assign decode_optype = fetch2decodeinstr1_out[31:28]; 
  assign decode_packet1dreg   = fetch2decodeinstr1_out[27:23]; 
  assign decode_packet1sreg_t = fetch2decodeinstr1_out[22:18]; 
  assign decode_opcode = fetch2decodeinstr1_out[8:0]; 
  assign decode_packet1imm5_value   = fetch2decodeinstr1_out[17:13]; 
  assign decode_opc    = fetch2decodeinstr1_out[17:16]; 
  assign decode_opc26    = fetch2decodeinstr1_out[27:26]; 
  assign decode_packet1imm16_value  = fetch2decodeinstr1_out[15:0]; 
  assign decode_packet1imm26_value  = fetch2decodeinstr1_out[25:0]; 
  assign decode_packet1acc    = fetch2decodeinstr1_out[12:11]; 
 //FIXME: This mux checks if dreg is actually a source register
  assign decode_packet1sreg_s = (decode_packet1dreg_is_src)? fetch2decodeinstr1_out[27:23]:fetch2decodeinstr1_out[17:13];
  assign decode_packet1has_sreg_s = has_sreg_s || decode_packet1dreg_is_src;
  assign decode_packet1has_dreg = has_dreg && (!decode_packet1dreg_is_src || decode_packet1dreg_is_dest);
/////////////////////////////////////////////////////////////////////////////
// Decode on OpType  
/////////////////////////////////////////////////////////////////////////////
	always @ (decode_optype or decode_opcode or fetch2decodeinstr1_out) // trigger decode on instruction change 
	begin                        
		case( decode_optype )
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 0 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | sreg_s/imm5  | d| t| s| i|         opcode           |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h0:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = fetch2decodeinstr1_out[12];
			decode_packet1has_sreg_t = fetch2decodeinstr1_out[11];
			has_sreg_s = fetch2decodeinstr1_out[10];
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = fetch2decodeinstr1_out[9];

			// op-specific decode logic
			case ( decode_opcode  )
				//////////////////////////
				// add
				// Signed Addition
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sub
				// Signed Subtraction
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addu
				// Unsigned Addition
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subu
				// Unsigned Subtraction
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addc
				// Signed Addition (Carry In $r1)
				//////////////////////////
				9'h04:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b1;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addg
				// Signed Addition (Carry Out $r1)
				//////////////////////////
				9'h05:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b1;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addcu
				// Unsigned Addition (Carry In $r1)
				//////////////////////////
				9'h06:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b1;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addgu
				// Unsigned Addition (Carry Out $r1)
				//////////////////////////
				9'h07:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b1;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// and
				// Bitwise AND
				//////////////////////////
				9'h08:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// or
				// Bitwise OR
				//////////////////////////
				9'h09:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0101; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// xor
				// Bitwise XOR
				//////////////////////////
				9'h0a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0111; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// nor
				// Bitwise NOR
				//////////////////////////
				9'h0b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0110; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sll
				// Shift Left Logical
				//////////////////////////
				9'h0c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b01; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srl
				// Shift Right Logical
				//////////////////////////
				9'h0d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sra
				// Shift Right Arithmetic
				//////////////////////////
				9'h0e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// slli
				// Shift Left Logical Immediate
				//////////////////////////
				9'h0f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b01; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srli
				// Shift Right Logical Immediate
				//////////////////////////
				9'h10:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srai
				// Shift Right Arithmetic Immediate
				//////////////////////////
				9'h11:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ceq
				// Compare Equal (Signed)
				//////////////////////////
				9'h12:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// clt
				// Compare Less Than (Signed)
				//////////////////////////
				9'h13:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cle
				// Compare Less Than or Equal (Signed)
				//////////////////////////
				9'h14:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltu
				// Compare Less Than (Unsigned)
				//////////////////////////
				9'h15:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cleu
				// Compare Less Than or Equal (Unsigned)
				//////////////////////////
				9'h16:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ceqf
				// Compare Equal (Float)
				//////////////////////////
				9'h17:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltf
				// Compare Less Than (Float)
				//////////////////////////
				9'h18:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltef
				// Compare Less Than or Equal (Float)
				//////////////////////////
				9'h19:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cmoveq
				// Conditional Move if Equal
				//////////////////////////
				9'h1a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cmovneq
				// Conditional Move if Not Equal
				//////////////////////////
				9'h1b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mfsr
				// Move from Special-Purpose Register
				//////////////////////////
				9'h1c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b1;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mtsr
				// Move to Special-Purpose Register
				//////////////////////////
				9'h1d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jmpr
				// Unconditional Indirect Jump
				//////////////////////////
				9'h1e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b01; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jalr
				// Jump and Link Register
				//////////////////////////
				9'h1f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b01; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ldl
				// Load Linked (Local)
				//////////////////////////
				9'h20:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1001; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b1;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// stc
				// Store Conditional (Local)
				//////////////////////////
				9'h21:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1010; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b1;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomcas
				// Atomic Compare and Swap
				//////////////////////////
				9'h22:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomaddu
				// Atomic Unsigned Addition
				//////////////////////////
				9'h23:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// nop
				// No Operation
				//////////////////////////
				9'h24:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// brk
				// Software Break Point
				//////////////////////////
				9'h25:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// hlt
				// Halt Core
				//////////////////////////
				9'h26:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b1;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sync
				// Force all pending operations to complete before executing next the next instruction
				//////////////////////////
				9'h27:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// undef
				// Undefined Opcode
				//////////////////////////
				9'h28:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccwb
				// Writeback Cluster Cache
				//////////////////////////
				9'h29:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccinv
				// Invalidate Cluster Cache
				//////////////////////////
				9'h2a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccflush
				// Writeback and Invalidate Cluster Cache
				//////////////////////////
				9'h2b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// icinv
				// Invalidate Instruction Cache
				//////////////////////////
				9'h2c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mb
				// Full memory barrier
				//////////////////////////
				9'h2d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// rfe
				// Return from exception
				//////////////////////////
				9'h2e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// abort
				// RigelSim-Only: Abort the simulation
				//////////////////////////
				9'h2f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// syscall
				// RigelSim-Only: Call a UNIX system call
				//////////////////////////
				9'h30:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// printreg
				// RigelSim-Only: Print out the contents of a register
				//////////////////////////
				9'h31:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// timerstart
				// RigelSim-Only: Start a timer without a syscall
				//////////////////////////
				9'h32:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// timerstop
				// RigelSim-Only: Stop a timer without a syscall
				//////////////////////////
				9'h33:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// linewb
				// Writeback Cache Line
				//////////////////////////
				9'h34:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1100; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// lineinv
				// Invalidate Cache Line
				//////////////////////////
				9'h35:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1101; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// lineflush
				// Writeback and Invalidate Cache Line
				//////////////////////////
				9'h36:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1011; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqenq
				// Enqueue a Task in the HW Task Queue
				//////////////////////////
				9'h37:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqdeq
				// Dequeue a Task from the HW Task Queue
				//////////////////////////
				9'h38:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqloop
				// Enqueue a Range of Tasks
				//////////////////////////
				9'h39:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqinit
				// Initialize the HW Task Queue
				//////////////////////////
				9'h3a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqend
				// Initialize the HW Task Queue
				//////////////////////////
				9'h3b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fadd
				// Floating-Point Addition
				//////////////////////////
				9'h3c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b0001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fsub
				// Floating-Point Subtraction
				//////////////////////////
				9'h3d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b0010; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmul
				// Floating-Point Multiply
				//////////////////////////
				9'h3e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b1001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// frcp
				// Floating-Point Reciprocal
				//////////////////////////
				9'h3f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10010; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// frsq
				// Floating-Point Reciprocal Square Root
				//////////////////////////
				9'h40:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10011; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fabs
				// Floating-Point Absolute Value (Rounding IDK?!)
				//////////////////////////
				9'h41:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b1110; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmrs
				// REMOVE ME!!  Floating-Point Register-to-Register Move
				//////////////////////////
				9'h42:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10100; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmov
				// Floating-Point Register-to-Register Move
				//////////////////////////
				9'h43:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10100; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fr2a
				// Floating-Point Register-to-Accumulator Move
				//////////////////////////
				9'h44:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fa2r
				// Floating-Point Accumulator-to-Register Move
				//////////////////////////
				9'h45:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// f2i
				// Floating-Point to Signed Integer Conversion
				//////////////////////////
				9'h46:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// i2f
				// Signed Integer to Floating-Point Conversion
				//////////////////////////
				9'h47:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul
				// REMOVE ME!! Multiply (No Carry)
				//////////////////////////
				9'h48:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul32
				// 32-bit Multiply (No Carry)
				//////////////////////////
				9'h49:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16
				// 16-bit Multiply (No Carry)
				//////////////////////////
				9'h4a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16c
				// 16-bit Multiply (Carry In)
				//////////////////////////
				9'h4b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16g
				// 16-bit Multiply (Generate Carry)
				//////////////////////////
				9'h4c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// clz
				// Count Leading Zeros
				//////////////////////////
				9'h4d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// zext8
				// Zero Extend Byte
				//////////////////////////
				9'h4e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sext8
				// Sign Extend Byte
				//////////////////////////
				9'h4f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// zext16
				// Zero Extend 2-Byte
				//////////////////////////
				9'h50:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1011; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sext16
				// Sign Extend 2-Byte
				//////////////////////////
				9'h51:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1011; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vadd
				// Vector Signed Addition
				//////////////////////////
				9'h52:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vsub
				// Vector Signed Subtraction
				//////////////////////////
				9'h53:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfadd
				// Vector Floating-Point Addition
				//////////////////////////
				9'h54:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfsub
				// Vector Floating-Point Subtraction
				//////////////////////////
				9'h55:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfmul
				// Vector Floating-Point Multiply
				//////////////////////////
				9'h56:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 1 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h1:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// addi
				// Signed Addition with Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subi
				// Signed Subtraction with Immediate
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addiu
				// Unsigned Addition with Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subiu
				// Unsigned Subtraction with Immediate
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 2 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h2:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// andi
				// Bitwise AND Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ori
				// Bitwise OR Immediate
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0101; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// xori
				// Bitwise XOR Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0111; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// beq
				// Branch If Equal
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 3 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h3:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// mvui
				// Move to Upper Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jal
				// Jump and Link
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// prefl
				// Line Prefetch
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// prefb
				// Block Prefetch (4 KiB)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 4 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h4:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// bne
				// Branch If Not Equal
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b101; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// pldw
				// Paused Load (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bcastu
				// Broadcast update all cluster caches with new value
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ldw
				// Load Word (Local)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0001; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 5 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |//////////////|    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h5:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// be
				// Branch If Equal to Zero
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bnz
				// Branch If Not Equal to Zero
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b101; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// blt
				// Branch If Less than Zero
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bgt
				// Branch If Greater than Zero
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b001; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 6 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |//////////////|    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h6:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// ble
				// Branch If Less than or Equal to Zero
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bge
				// Branch If Greater than or Equal to Zero
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b011; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jmp
				// Unconditional Jump
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 7 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  | opc |                                    imm26                                    |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h7:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b1;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc26 )
				//////////////////////////
				// lj
				// Unconditional Long Jump
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ljl
				// Long Jump and Link
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 8 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h8:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// prefnga
				// Line Prefetch (No GCache Allocate)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bcasti
				// Broadcast invalidate to all cluster caches
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// strtof
				// RigelSim-Only: Do string to floating-point conversion
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// event
				// RigelSim-Only: event tracking with immediate operand
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 9 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h9:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// stw
				// Store Word (Local)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0011; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// gldw
				// Load Word (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0010; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// gstw
				// Store Word (Global)
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0100; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomdec
				// Atomic Fetch and Decrement (Global)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0111; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE a 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'ha:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// atominc
				// Atomic Fetch and Increment (Global)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0110; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomxchg
				// Atomic Exchange (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0101; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b1;
				end
				//////////////////////////
				// vaddi
				// Vector Signed Addition with Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vsubi
				// Vector Signed Subtraction with Immediate
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE b 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    |    sreg_s    | acc |         opcode                 |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hb:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b1;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opcode  )
				//////////////////////////
				// fmadd
				// Floating-Point Multiply Accumulate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmsub
				// Floating-Point Multiply Subtraction
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE c 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hc:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// vldw
				// Vector Load Word (Local)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vstw
				// Vector Store Word (Local)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE d 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hd:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// prio
				// Set priority of traffic flow with immediate operand Zero is lowest priority
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
		default:
		begin
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
			decode_packet1is_valid       = 1'b0;
			decode_packet1is_signed      = 1'b0;
			decode_packet1carry_in       = 1'b0;
			decode_packet1carry_out      = 1'b0;
			decode_packet1is_prefetch    = 1'b0;
			decode_packet1sprf_dest      = 1'b0;
			decode_packet1sprf_src       = 1'b0;
			decode_packet1is_atomic      = 1'b0;
			decode_packet1is_ldl         = 1'b0;
			decode_packet1is_stl         = 1'b0;
			decode_packet1is_stc         = 1'b0;
			decode_packet1is_halt        = 1'b0;
			decode_packet1is_compare     = 1'b0;
			decode_packet1dreg_is_src    = 1'b0;
			decode_packet1dreg_is_dest   = 1'b0;
		end
		endcase
	end
endmodule
module decoder1(
  fetch2decodeinstr1_out,
  //output decode_packet_t instr_packet
  decode_packet1sreg_t,
  decode_packet1sreg_s,
  decode_packet1dreg,
  decode_packet1acc,
  decode_packet1imm16_value,
  decode_packet1imm26_value,
  decode_packet1imm5_value,
  decode_packet1op_FU_type,
  decode_packet1alu_op,
  decode_packet1fpu_op,
  decode_packet1op_MEM_type,
  decode_packet1op_BR_type,
  decode_packet1shift_type,
  decode_packet1compare_type,
  decode_packet1is_imm26,
  decode_packet1is_imm,
  decode_packet1is_imm5,
  decode_packet1is_signed,
  decode_packet1carry_in,
  decode_packet1carry_out,
  decode_packet1has_sreg_t,
  decode_packet1has_sreg_s,
  decode_packet1has_dreg,
  decode_packet1is_prefetch,
  decode_packet1sprf_dest,
  decode_packet1sprf_src,
  decode_packet1is_atomic,
  decode_packet1is_ldl,
  decode_packet1is_stl,
  decode_packet1is_stc,
  decode_packet1is_valid,
  decode_packet1is_halt,
  decode_packet1is_compare,
  decode_packet1dreg_is_src,
  decode_packet1dreg_is_dest
);

  input [31:0] fetch2decodeinstr1_out;
  //output instr_packet
  output [4:0] decode_packet1sreg_t;
  output [4:0] decode_packet1sreg_s;
  output [4:0] decode_packet1dreg;
  output [1:0] decode_packet1acc;
  output [15:0] decode_packet1imm16_value;
  output [25:0] decode_packet1imm26_value;
  output [4:0] decode_packet1imm5_value;
  output [3:0] decode_packet1op_FU_type;
  output [4:0] decode_packet1alu_op;
  output [4:0] decode_packet1fpu_op;
  output [3:0] decode_packet1op_MEM_type;
  output [1:0] decode_packet1op_BR_type;
  output [1:0] decode_packet1shift_type;
  output [2:0] decode_packet1compare_type;
  output  decode_packet1is_imm26;
  output  decode_packet1is_imm;
  output  decode_packet1is_imm5;
  output  decode_packet1is_signed;
  output  decode_packet1carry_in;
  output  decode_packet1carry_out;
  output  decode_packet1has_sreg_t;
  output  decode_packet1has_sreg_s;
  output  decode_packet1has_dreg;
  output  decode_packet1is_prefetch;
  output  decode_packet1sprf_dest;
  output  decode_packet1sprf_src;
  output  decode_packet1is_atomic;
  output  decode_packet1is_ldl;
  output  decode_packet1is_stl;
  output  decode_packet1is_stc;
  output  decode_packet1is_valid;
  output  decode_packet1is_halt;
  output  decode_packet1is_compare;
  output  decode_packet1dreg_is_src;
  output  decode_packet1dreg_is_dest;
//==========================================
  reg [4:0] decode_packet1sreg_t;
  reg [4:0] decode_packet1sreg_s;
  reg [4:0] decode_packet1dreg;
  reg [1:0] decode_packet1acc;
  reg [15:0] decode_packet1imm16_value;
  reg [25:0] decode_packet1imm26_value;
  reg [4:0] decode_packet1imm5_value;
  reg [3:0] decode_packet1op_FU_type;
  reg [4:0] decode_packet1alu_op;
  reg [4:0] decode_packet1fpu_op;
  reg [3:0] decode_packet1op_MEM_type;
  reg [1:0] decode_packet1op_BR_type;
  reg [1:0] decode_packet1shift_type;
  reg [2:0] decode_packet1compare_type;
  reg  decode_packet1is_imm26;
  reg  decode_packet1is_imm;
  reg  decode_packet1is_imm5;
  reg  decode_packet1is_signed;
  reg  decode_packet1carry_in;
  reg  decode_packet1carry_out;
  reg  decode_packet1has_sreg_t;
  reg  decode_packet1has_sreg_s;
  reg  decode_packet1has_dreg;
  reg  decode_packet1is_prefetch;
  reg  decode_packet1sprf_dest;
  reg  decode_packet1sprf_src;
  reg  decode_packet1is_atomic;
  reg  decode_packet1is_ldl;
  reg  decode_packet1is_stl;
  reg  decode_packet1is_stc;
  reg  decode_packet1is_valid;
  reg  decode_packet1is_halt;
  reg  decode_packet1is_compare;
  reg  decode_packet1dreg_is_src;
  reg  decode_packet1dreg_is_dest;

//==========================================


  wire [3:0] decode_optype;
  wire [1:0] decode_opc;
  wire [1:0] decode_opc26;
  wire [10:0] decode_opcode;
  reg has_sreg_s;
  reg has_dreg;

// set instruction-agnostic outputs with continuous assignments 
  assign decode_optype = fetch2decodeinstr1_out[31:28]; 
  assign decode_packet1dreg   = fetch2decodeinstr1_out[27:23]; 
  assign decode_packet1sreg_t = fetch2decodeinstr1_out[22:18]; 
  assign decode_opcode = fetch2decodeinstr1_out[8:0]; 
  assign decode_packet1imm5_value   = fetch2decodeinstr1_out[17:13]; 
  assign decode_opc    = fetch2decodeinstr1_out[17:16]; 
  assign decode_opc26    = fetch2decodeinstr1_out[27:26]; 
  assign decode_packet1imm16_value  = fetch2decodeinstr1_out[15:0]; 
  assign decode_packet1imm26_value  = fetch2decodeinstr1_out[25:0]; 
  assign decode_packet1acc    = fetch2decodeinstr1_out[12:11]; 
 //FIXME: This mux checks if dreg is actually a source register
  assign decode_packet1sreg_s = (decode_packet1dreg_is_src)? fetch2decodeinstr1_out[27:23]:fetch2decodeinstr1_out[17:13];
  assign decode_packet1has_sreg_s = has_sreg_s || decode_packet1dreg_is_src;
  assign decode_packet1has_dreg = has_dreg && (!decode_packet1dreg_is_src || decode_packet1dreg_is_dest);
/////////////////////////////////////////////////////////////////////////////
// Decode on OpType  
/////////////////////////////////////////////////////////////////////////////
	always @ (decode_optype or fetch2decodeinstr1_out) // trigger decode on instruction change 
	begin                        
		case( decode_optype )
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 0 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | sreg_s/imm5  | d| t| s| i|         opcode           |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h0:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = fetch2decodeinstr1_out[12];
			decode_packet1has_sreg_t = fetch2decodeinstr1_out[11];
			has_sreg_s = fetch2decodeinstr1_out[10];
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = fetch2decodeinstr1_out[9];

			// op-specific decode logic
			case ( decode_opcode  )
				//////////////////////////
				// add
				// Signed Addition
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sub
				// Signed Subtraction
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addu
				// Unsigned Addition
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subu
				// Unsigned Subtraction
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addc
				// Signed Addition (Carry In $r1)
				//////////////////////////
				9'h04:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b1;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addg
				// Signed Addition (Carry Out $r1)
				//////////////////////////
				9'h05:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b1;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addcu
				// Unsigned Addition (Carry In $r1)
				//////////////////////////
				9'h06:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b1;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addgu
				// Unsigned Addition (Carry Out $r1)
				//////////////////////////
				9'h07:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b1;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// and
				// Bitwise AND
				//////////////////////////
				9'h08:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// or
				// Bitwise OR
				//////////////////////////
				9'h09:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0101; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// xor
				// Bitwise XOR
				//////////////////////////
				9'h0a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0111; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// nor
				// Bitwise NOR
				//////////////////////////
				9'h0b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0110; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sll
				// Shift Left Logical
				//////////////////////////
				9'h0c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b01; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srl
				// Shift Right Logical
				//////////////////////////
				9'h0d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sra
				// Shift Right Arithmetic
				//////////////////////////
				9'h0e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// slli
				// Shift Left Logical Immediate
				//////////////////////////
				9'h0f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b01; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srli
				// Shift Right Logical Immediate
				//////////////////////////
				9'h10:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// srai
				// Shift Right Arithmetic Immediate
				//////////////////////////
				9'h11:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0110; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b10; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ceq
				// Compare Equal (Signed)
				//////////////////////////
				9'h12:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// clt
				// Compare Less Than (Signed)
				//////////////////////////
				9'h13:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cle
				// Compare Less Than or Equal (Signed)
				//////////////////////////
				9'h14:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltu
				// Compare Less Than (Unsigned)
				//////////////////////////
				9'h15:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cleu
				// Compare Less Than or Equal (Unsigned)
				//////////////////////////
				9'h16:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ceqf
				// Compare Equal (Float)
				//////////////////////////
				9'h17:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltf
				// Compare Less Than (Float)
				//////////////////////////
				9'h18:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cltef
				// Compare Less Than or Equal (Float)
				//////////////////////////
				9'h19:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b1;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cmoveq
				// Conditional Move if Equal
				//////////////////////////
				9'h1a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// cmovneq
				// Conditional Move if Not Equal
				//////////////////////////
				9'h1b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mfsr
				// Move from Special-Purpose Register
				//////////////////////////
				9'h1c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b1;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mtsr
				// Move to Special-Purpose Register
				//////////////////////////
				9'h1d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jmpr
				// Unconditional Indirect Jump
				//////////////////////////
				9'h1e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b01; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jalr
				// Jump and Link Register
				//////////////////////////
				9'h1f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b01; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ldl
				// Load Linked (Local)
				//////////////////////////
				9'h20:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1001; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b1;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// stc
				// Store Conditional (Local)
				//////////////////////////
				9'h21:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1010; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b1;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomcas
				// Atomic Compare and Swap
				//////////////////////////
				9'h22:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomaddu
				// Atomic Unsigned Addition
				//////////////////////////
				9'h23:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// nop
				// No Operation
				//////////////////////////
				9'h24:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// brk
				// Software Break Point
				//////////////////////////
				9'h25:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// hlt
				// Halt Core
				//////////////////////////
				9'h26:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b1;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sync
				// Force all pending operations to complete before executing next the next instruction
				//////////////////////////
				9'h27:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// undef
				// Undefined Opcode
				//////////////////////////
				9'h28:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccwb
				// Writeback Cluster Cache
				//////////////////////////
				9'h29:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccinv
				// Invalidate Cluster Cache
				//////////////////////////
				9'h2a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ccflush
				// Writeback and Invalidate Cluster Cache
				//////////////////////////
				9'h2b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// icinv
				// Invalidate Instruction Cache
				//////////////////////////
				9'h2c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mb
				// Full memory barrier
				//////////////////////////
				9'h2d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// rfe
				// Return from exception
				//////////////////////////
				9'h2e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// abort
				// RigelSim-Only: Abort the simulation
				//////////////////////////
				9'h2f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// syscall
				// RigelSim-Only: Call a UNIX system call
				//////////////////////////
				9'h30:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// printreg
				// RigelSim-Only: Print out the contents of a register
				//////////////////////////
				9'h31:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// timerstart
				// RigelSim-Only: Start a timer without a syscall
				//////////////////////////
				9'h32:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// timerstop
				// RigelSim-Only: Stop a timer without a syscall
				//////////////////////////
				9'h33:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// linewb
				// Writeback Cache Line
				//////////////////////////
				9'h34:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1100; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// lineinv
				// Invalidate Cache Line
				//////////////////////////
				9'h35:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1101; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// lineflush
				// Writeback and Invalidate Cache Line
				//////////////////////////
				9'h36:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b1011; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqenq
				// Enqueue a Task in the HW Task Queue
				//////////////////////////
				9'h37:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqdeq
				// Dequeue a Task from the HW Task Queue
				//////////////////////////
				9'h38:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqloop
				// Enqueue a Range of Tasks
				//////////////////////////
				9'h39:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqinit
				// Initialize the HW Task Queue
				//////////////////////////
				9'h3a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// tqend
				// Initialize the HW Task Queue
				//////////////////////////
				9'h3b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fadd
				// Floating-Point Addition
				//////////////////////////
				9'h3c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b0001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fsub
				// Floating-Point Subtraction
				//////////////////////////
				9'h3d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b0010; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmul
				// Floating-Point Multiply
				//////////////////////////
				9'h3e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b1001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// frcp
				// Floating-Point Reciprocal
				//////////////////////////
				9'h3f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10010; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// frsq
				// Floating-Point Reciprocal Square Root
				//////////////////////////
				9'h40:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10011; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fabs
				// Floating-Point Absolute Value (Rounding IDK?!)
				//////////////////////////
				9'h41:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b1110; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmrs
				// REMOVE ME!!  Floating-Point Register-to-Register Move
				//////////////////////////
				9'h42:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10100; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmov
				// Floating-Point Register-to-Register Move
				//////////////////////////
				9'h43:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10100; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fr2a
				// Floating-Point Register-to-Accumulator Move
				//////////////////////////
				9'h44:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fa2r
				// Floating-Point Accumulator-to-Register Move
				//////////////////////////
				9'h45:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// f2i
				// Floating-Point to Signed Integer Conversion
				//////////////////////////
				9'h46:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10001; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// i2f
				// Signed Integer to Floating-Point Conversion
				//////////////////////////
				9'h47:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b10000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul
				// REMOVE ME!! Multiply (No Carry)
				//////////////////////////
				9'h48:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul32
				// 32-bit Multiply (No Carry)
				//////////////////////////
				9'h49:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16
				// 16-bit Multiply (No Carry)
				//////////////////////////
				9'h4a:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16c
				// 16-bit Multiply (Carry In)
				//////////////////////////
				9'h4b:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// mul16g
				// 16-bit Multiply (Generate Carry)
				//////////////////////////
				9'h4c:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// clz
				// Count Leading Zeros
				//////////////////////////
				9'h4d:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// zext8
				// Zero Extend Byte
				//////////////////////////
				9'h4e:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sext8
				// Sign Extend Byte
				//////////////////////////
				9'h4f:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// zext16
				// Zero Extend 2-Byte
				//////////////////////////
				9'h50:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1011; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// sext16
				// Sign Extend 2-Byte
				//////////////////////////
				9'h51:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1011; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vadd
				// Vector Signed Addition
				//////////////////////////
				9'h52:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vsub
				// Vector Signed Subtraction
				//////////////////////////
				9'h53:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfadd
				// Vector Floating-Point Addition
				//////////////////////////
				9'h54:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfsub
				// Vector Floating-Point Subtraction
				//////////////////////////
				9'h55:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vfmul
				// Vector Floating-Point Multiply
				//////////////////////////
				9'h56:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0101; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 1 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h1:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// addi
				// Signed Addition with Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subi
				// Signed Subtraction with Immediate
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// addiu
				// Unsigned Addition with Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0001; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// subiu
				// Unsigned Subtraction with Immediate
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0010; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 2 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h2:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// andi
				// Bitwise AND Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ori
				// Bitwise OR Immediate
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0101; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// xori
				// Bitwise XOR Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b0111; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// beq
				// Branch If Equal
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 3 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h3:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// mvui
				// Move to Upper Immediate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b1100; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jal
				// Jump and Link
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// prefl
				// Line Prefetch
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// prefb
				// Block Prefetch (4 KiB)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 4 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h4:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// bne
				// Branch If Not Equal
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b101; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// pldw
				// Paused Load (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bcastu
				// Broadcast update all cluster caches with new value
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ldw
				// Load Word (Local)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0001; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 5 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |//////////////|    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h5:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// be
				// Branch If Equal to Zero
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b010; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bnz
				// Branch If Not Equal to Zero
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b101; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// blt
				// Branch If Less than Zero
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b100; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bgt
				// Branch If Greater than Zero
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b001; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 6 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |//////////////|    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h6:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// ble
				// Branch If Less than or Equal to Zero
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b110; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bge
				// Branch If Greater than or Equal to Zero
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b011; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// jmp
				// Unconditional Jump
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 7 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  | opc |                                    imm26                                    |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h7:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b1;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc26 )
				//////////////////////////
				// lj
				// Unconditional Long Jump
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// ljl
				// Long Jump and Link
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b10; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b111; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b1;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 8 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h8:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// prefnga
				// Line Prefetch (No GCache Allocate)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// bcasti
				// Broadcast invalidate to all cluster caches
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// strtof
				// RigelSim-Only: Do string to floating-point conversion
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// event
				// RigelSim-Only: event tracking with immediate operand
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE 9 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'h9:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// stw
				// Store Word (Local)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0011; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b1;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// gldw
				// Load Word (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0010; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// gstw
				// Store Word (Global)
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0100; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomdec
				// Atomic Fetch and Decrement (Global)
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0111; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE a 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'ha:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// atominc
				// Atomic Fetch and Increment (Global)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0110; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// atomxchg
				// Atomic Exchange (Global)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0101; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b1;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b1;
					decode_packet1dreg_is_dest   = 1'b1;
				end
				//////////////////////////
				// vaddi
				// Vector Signed Addition with Immediate
				//////////////////////////
				9'h02:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vsubi
				// Vector Signed Subtraction with Immediate
				//////////////////////////
				9'h03:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0010; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE b 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    |    sreg_s    | acc |         opcode                 |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hb:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b1;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opcode  )
				//////////////////////////
				// fmadd
				// Floating-Point Multiply Accumulate
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// fmsub
				// Floating-Point Multiply Subtraction
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0011; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE c 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |    sreg_t    | opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hc:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b1;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// vldw
				// Vector Load Word (Local)
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				//////////////////////////
				// vstw
				// Vector Store Word (Local)
				//////////////////////////
				9'h01:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b1000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
/////////////////////////////////////////////////////////////////////////////
/* INSTR_TYPE d 
|-----------------------------------------------------------------------------------------------|
|31|30|29|28|27|26|25|24|23|22|21|20|19|18|17|16|15|14|13|12|11|10|09|08|07|06|05|04|03|02|01|00|
|   OPTYPE  |    dreg      |//////////////| opc |                   imm16                       |
|-----------------------------------------------------------------------------------------------|
*//////////////////////////////////////////////////////////////////////////////
		4'hd:    
		begin
			// Set Properties that can be Inferred from OpType
			has_dreg   = 1'b1;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b1;
			decode_packet1is_imm5    = 1'b0;

			// op-specific decode logic
			case ( decode_opc )
				//////////////////////////
				// prio
				// Set priority of traffic flow with immediate operand Zero is lowest priority
				//////////////////////////
				9'h00:
				begin
					// decode logic 
					decode_packet1op_FU_type     = 4'b0001; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
				default:
				begin
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
					decode_packet1is_valid       = 1'b0;
					decode_packet1is_signed      = 1'b0;
					decode_packet1carry_in       = 1'b0;
					decode_packet1carry_out      = 1'b0;
					decode_packet1is_prefetch    = 1'b0;
					decode_packet1sprf_dest      = 1'b0;
					decode_packet1sprf_src       = 1'b0;
					decode_packet1is_atomic      = 1'b0;
					decode_packet1is_ldl         = 1'b0;
					decode_packet1is_stl         = 1'b0;
					decode_packet1is_stc         = 1'b0;
					decode_packet1is_halt        = 1'b0;
					decode_packet1is_compare     = 1'b0;
					decode_packet1dreg_is_src    = 1'b0;
					decode_packet1dreg_is_dest   = 1'b0;
				end
			endcase
		end
		default:
		begin
			has_dreg   = 1'b0;
			decode_packet1has_sreg_t = 1'b0;
			has_sreg_s = 1'b0;
			decode_packet1is_imm26   = 1'b0;
			decode_packet1is_imm     = 1'b0;
			decode_packet1is_imm5    = 1'b0;
					decode_packet1op_FU_type     = 4'b0000; 
					decode_packet1alu_op         = 5'b00000; 
					decode_packet1fpu_op         = 5'b00000; 
					decode_packet1op_BR_type     = 2'b00; 
					decode_packet1shift_type     = 2'b00; 
					decode_packet1op_MEM_type    = 4'b0000; 
					decode_packet1compare_type   = 3'b000; 
			decode_packet1is_valid       = 1'b0;
			decode_packet1is_signed      = 1'b0;
			decode_packet1carry_in       = 1'b0;
			decode_packet1carry_out      = 1'b0;
			decode_packet1is_prefetch    = 1'b0;
			decode_packet1sprf_dest      = 1'b0;
			decode_packet1sprf_src       = 1'b0;
			decode_packet1is_atomic      = 1'b0;
			decode_packet1is_ldl         = 1'b0;
			decode_packet1is_stl         = 1'b0;
			decode_packet1is_stc         = 1'b0;
			decode_packet1is_halt        = 1'b0;
			decode_packet1is_compare     = 1'b0;
			decode_packet1dreg_is_src    = 1'b0;
			decode_packet1dreg_is_dest   = 1'b0;
		end
		endcase
	end
endmodule
module scoreboard(
  // control
  clk, 
  reset,
  // source registers (read sb)
  src0_addr,
  src1_addr,
  src2_addr,
  src3_addr,
  // destination registers (read sb)
  dst0_addr,
  dst0_vld,
  dst1_addr,
  dst1_vld,
  // inputs from wb
  wb0_addr,
  wb0_vld,
  wb1_addr,
  wb1_vld,
  // ready outputs (true if ready)
  src0_rdy,
  src1_rdy,
  src2_rdy,
  src3_rdy,
  dst0_rdy,
  dst1_rdy
);

  // control
  input clk; 
  input reset;
  // source registers (read sb)
  input [4:0] src0_addr;
  input [4:0] src1_addr;
  input [4:0] src2_addr;
  input [4:0] src3_addr;
  // destination registers (read sb)
  input [4:0] dst0_addr;
  input dst0_vld;
  input [4:0] dst1_addr;
  input dst1_vld;
  // inputs from wb
  input [4:0] wb0_addr;
  input wb0_vld;
  input [4:0] wb1_addr;
  input wb1_vld;
  // ready outputs (true if ready)
  output src0_rdy;
  output src1_rdy;
  output src2_rdy;
  output src3_rdy;
  output dst0_rdy;
  output dst1_rdy;
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////
  reg [31:0] sb;      // scoreboard bits
  reg [3:0] next_sb; // next scoreboard bits
  reg [4:0] next_sb_addr_0; // next scoreboard bits
  reg [4:0] next_sb_addr_1; // next scoreboard bits
  reg [4:0] next_sb_addr_2; // next scoreboard bits
  reg [4:0] next_sb_addr_3; // next scoreboard bits
  reg [3:0] next_sb_vld; // next scoreboard bits

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** combinational logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_READ
  //////////////////////////////////////////////////////////////////////////////
  // 
  //////////////////////////////////////////////////////////////////////////////

  //assign dst0_rdy = sb[dst0_addr]!=1'b0;
  always@(dst0_addr or sb)
    begin
      case(dst0_addr)
        5'h0: dst0_rdy = (sb[0]!=1'b0);
        5'h1: dst0_rdy = (sb[1]!=1'b0);
        5'h2: dst0_rdy = (sb[2]!=1'b0);
        5'h3: dst0_rdy = (sb[3]!=1'b0);
        5'h4: dst0_rdy = (sb[4]!=1'b0);
        5'h5: dst0_rdy = (sb[5]!=1'b0);
        5'h6: dst0_rdy = (sb[6]!=1'b0);
        5'h7: dst0_rdy = (sb[7]!=1'b0);
        5'h8: dst0_rdy = (sb[8]!=1'b0);
        5'h9: dst0_rdy = (sb[9]!=1'b0);
        5'ha: dst0_rdy = (sb[10]!=1'b0);
        5'hb: dst0_rdy = (sb[11]!=1'b0);
        5'hc: dst0_rdy = (sb[12]!=1'b0);
        5'hd: dst0_rdy = (sb[13]!=1'b0);
        5'he: dst0_rdy = (sb[14]!=1'b0);
        5'hf: dst0_rdy = (sb[15]!=1'b0);
        5'h10: dst0_rdy = (sb[16]!=1'b0);
        5'h11: dst0_rdy = (sb[17]!=1'b0);
        5'h12: dst0_rdy = (sb[18]!=1'b0);
        5'h13: dst0_rdy = (sb[19]!=1'b0);
        5'h14: dst0_rdy = (sb[20]!=1'b0);
        5'h15: dst0_rdy = (sb[21]!=1'b0);
        5'h16: dst0_rdy = (sb[22]!=1'b0);
        5'h17: dst0_rdy = (sb[23]!=1'b0);
        5'h18: dst0_rdy = (sb[24]!=1'b0);
        5'h19: dst0_rdy = (sb[25]!=1'b0);
        5'h1a: dst0_rdy = (sb[26]!=1'b0);
        5'h1b: dst0_rdy = (sb[27]!=1'b0);
        5'h1c: dst0_rdy = (sb[28]!=1'b0);
        5'h1d: dst0_rdy = (sb[29]!=1'b0);
        5'h1e: dst0_rdy = (sb[30]!=1'b0);
        5'h1f: dst0_rdy = (sb[31]!=1'b0);
      endcase
    end


always@(dst1_addr or sb)
    begin
      case(dst1_addr)
        5'h0: dst1_rdy = (sb[0]!=1'b0);
        5'h1: dst1_rdy = (sb[1]!=1'b0);
        5'h2: dst1_rdy = (sb[2]!=1'b0);
        5'h3: dst1_rdy = (sb[3]!=1'b0);
        5'h4: dst1_rdy = (sb[4]!=1'b0);
        5'h5: dst1_rdy = (sb[5]!=1'b0);
        5'h6: dst1_rdy = (sb[6]!=1'b0);
        5'h7: dst1_rdy = (sb[7]!=1'b0);
        5'h8: dst1_rdy = (sb[8]!=1'b0);
        5'h9: dst1_rdy = (sb[9]!=1'b0);
        5'ha: dst1_rdy = (sb[10]!=1'b0);
        5'hb: dst1_rdy = (sb[11]!=1'b0);
        5'hc: dst1_rdy = (sb[12]!=1'b0);
        5'hd: dst1_rdy = (sb[13]!=1'b0);
        5'he: dst1_rdy = (sb[14]!=1'b0);
        5'hf: dst1_rdy = (sb[15]!=1'b0);
        5'h10: dst1_rdy = (sb[16]!=1'b0);
        5'h11: dst1_rdy = (sb[17]!=1'b0);
        5'h12: dst1_rdy = (sb[18]!=1'b0);
        5'h13: dst1_rdy = (sb[19]!=1'b0);
        5'h14: dst1_rdy = (sb[20]!=1'b0);
        5'h15: dst1_rdy = (sb[21]!=1'b0);
        5'h16: dst1_rdy = (sb[22]!=1'b0);
        5'h17: dst1_rdy = (sb[23]!=1'b0);
        5'h18: dst1_rdy = (sb[24]!=1'b0);
        5'h19: dst1_rdy = (sb[25]!=1'b0);
        5'h1a: dst1_rdy = (sb[26]!=1'b0);
        5'h1b: dst1_rdy = (sb[27]!=1'b0);
        5'h1c: dst1_rdy = (sb[28]!=1'b0);
        5'h1d: dst1_rdy = (sb[29]!=1'b0);
        5'h1e: dst1_rdy = (sb[30]!=1'b0);
        5'h1f: dst1_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(src0_addr or sb)
    begin
      case(src0_addr)
        5'h0: src0_rdy = 1;
        5'h1: src0_rdy = (sb[1]!=1'b0);
        5'h2: src0_rdy = (sb[2]!=1'b0);
        5'h3: src0_rdy = (sb[3]!=1'b0);
        5'h4: src0_rdy = (sb[4]!=1'b0);
        5'h5: src0_rdy = (sb[5]!=1'b0);
        5'h6: src0_rdy = (sb[6]!=1'b0);
        5'h7: src0_rdy = (sb[7]!=1'b0);
        5'h8: src0_rdy = (sb[8]!=1'b0);
        5'h9: src0_rdy = (sb[9]!=1'b0);
        5'ha: src0_rdy = (sb[10]!=1'b0);
        5'hb: src0_rdy = (sb[11]!=1'b0);
        5'hc: src0_rdy = (sb[12]!=1'b0);
        5'hd: src0_rdy = (sb[13]!=1'b0);
        5'he: src0_rdy = (sb[14]!=1'b0);
        5'hf: src0_rdy = (sb[15]!=1'b0);
        5'h10: src0_rdy = (sb[16]!=1'b0);
        5'h11: src0_rdy = (sb[17]!=1'b0);
        5'h12: src0_rdy = (sb[18]!=1'b0);
        5'h13: src0_rdy = (sb[19]!=1'b0);
        5'h14: src0_rdy = (sb[20]!=1'b0);
        5'h15: src0_rdy = (sb[21]!=1'b0);
        5'h16: src0_rdy = (sb[22]!=1'b0);
        5'h17: src0_rdy = (sb[23]!=1'b0);
        5'h18: src0_rdy = (sb[24]!=1'b0);
        5'h19: src0_rdy = (sb[25]!=1'b0);
        5'h1a: src0_rdy = (sb[26]!=1'b0);
        5'h1b: src0_rdy = (sb[27]!=1'b0);
        5'h1c: src0_rdy = (sb[28]!=1'b0);
        5'h1d: src0_rdy = (sb[29]!=1'b0);
        5'h1e: src0_rdy = (sb[30]!=1'b0);
        5'h1f: src0_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(src1_addr or sb)
    begin
      case(src1_addr)
        5'h0: src1_rdy = 1;
        5'h1: src1_rdy = (sb[1]!=1'b0);
        5'h2: src1_rdy = (sb[2]!=1'b0);
        5'h3: src1_rdy = (sb[3]!=1'b0);
        5'h4: src1_rdy = (sb[4]!=1'b0);
        5'h5: src1_rdy = (sb[5]!=1'b0);
        5'h6: src1_rdy = (sb[6]!=1'b0);
        5'h7: src1_rdy = (sb[7]!=1'b0);
        5'h8: src1_rdy = (sb[8]!=1'b0);
        5'h9: src1_rdy = (sb[9]!=1'b0);
        5'ha: src1_rdy = (sb[10]!=1'b0);
        5'hb: src1_rdy = (sb[11]!=1'b0);
        5'hc: src1_rdy = (sb[12]!=1'b0);
        5'hd: src1_rdy = (sb[13]!=1'b0);
        5'he: src1_rdy = (sb[14]!=1'b0);
        5'hf: src1_rdy = (sb[15]!=1'b0);
        5'h10: src1_rdy = (sb[16]!=1'b0);
        5'h11: src1_rdy = (sb[17]!=1'b0);
        5'h12: src1_rdy = (sb[18]!=1'b0);
        5'h13: src1_rdy = (sb[19]!=1'b0);
        5'h14: src1_rdy = (sb[20]!=1'b0);
        5'h15: src1_rdy = (sb[21]!=1'b0);
        5'h16: src1_rdy = (sb[22]!=1'b0);
        5'h17: src1_rdy = (sb[23]!=1'b0);
        5'h18: src1_rdy = (sb[24]!=1'b0);
        5'h19: src1_rdy = (sb[25]!=1'b0);
        5'h1a: src1_rdy = (sb[26]!=1'b0);
        5'h1b: src1_rdy = (sb[27]!=1'b0);
        5'h1c: src1_rdy = (sb[28]!=1'b0);
        5'h1d: src1_rdy = (sb[29]!=1'b0);
        5'h1e: src1_rdy = (sb[30]!=1'b0);
        5'h1f: src1_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(src2_addr or sb)
    begin
      case(src2_addr)
        5'h0: src2_rdy = 1;
        5'h1: src2_rdy = (sb[1]!=1'b0);
        5'h2: src2_rdy = (sb[2]!=1'b0);
        5'h3: src2_rdy = (sb[3]!=1'b0);
        5'h4: src2_rdy = (sb[4]!=1'b0);
        5'h5: src2_rdy = (sb[5]!=1'b0);
        5'h6: src2_rdy = (sb[6]!=1'b0);
        5'h7: src2_rdy = (sb[7]!=1'b0);
        5'h8: src2_rdy = (sb[8]!=1'b0);
        5'h9: src2_rdy = (sb[9]!=1'b0);
        5'ha: src2_rdy = (sb[10]!=1'b0);
        5'hb: src2_rdy = (sb[11]!=1'b0);
        5'hc: src2_rdy = (sb[12]!=1'b0);
        5'hd: src2_rdy = (sb[13]!=1'b0);
        5'he: src2_rdy = (sb[14]!=1'b0);
        5'hf: src2_rdy = (sb[15]!=1'b0);
        5'h10: src2_rdy = (sb[16]!=1'b0);
        5'h11: src2_rdy = (sb[17]!=1'b0);
        5'h12: src2_rdy = (sb[18]!=1'b0);
        5'h13: src2_rdy = (sb[19]!=1'b0);
        5'h14: src2_rdy = (sb[20]!=1'b0);
        5'h15: src2_rdy = (sb[21]!=1'b0);
        5'h16: src2_rdy = (sb[22]!=1'b0);
        5'h17: src2_rdy = (sb[23]!=1'b0);
        5'h18: src2_rdy = (sb[24]!=1'b0);
        5'h19: src2_rdy = (sb[25]!=1'b0);
        5'h1a: src2_rdy = (sb[26]!=1'b0);
        5'h1b: src2_rdy = (sb[27]!=1'b0);
        5'h1c: src2_rdy = (sb[28]!=1'b0);
        5'h1d: src2_rdy = (sb[29]!=1'b0);
        5'h1e: src2_rdy = (sb[30]!=1'b0);
        5'h1f: src2_rdy = (sb[31]!=1'b0);
      endcase
    end

always@(src3_addr or sb)
    begin
      case(src3_addr)
        5'h0: src3_rdy = 1;
        5'h1: src3_rdy = (sb[1]!=1'b0);
        5'h2: src3_rdy = (sb[2]!=1'b0);
        5'h3: src3_rdy = (sb[3]!=1'b0);
        5'h4: src3_rdy = (sb[4]!=1'b0);
        5'h5: src3_rdy = (sb[5]!=1'b0);
        5'h6: src3_rdy = (sb[6]!=1'b0);
        5'h7: src3_rdy = (sb[7]!=1'b0);
        5'h8: src3_rdy = (sb[8]!=1'b0);
        5'h9: src3_rdy = (sb[9]!=1'b0);
        5'ha: src3_rdy = (sb[10]!=1'b0);
        5'hb: src3_rdy = (sb[11]!=1'b0);
        5'hc: src3_rdy = (sb[12]!=1'b0);
        5'hd: src3_rdy = (sb[13]!=1'b0);
        5'he: src3_rdy = (sb[14]!=1'b0);
        5'hf: src3_rdy = (sb[15]!=1'b0);
        5'h10: src3_rdy = (sb[16]!=1'b0);
        5'h11: src3_rdy = (sb[17]!=1'b0);
        5'h12: src3_rdy = (sb[18]!=1'b0);
        5'h13: src3_rdy = (sb[19]!=1'b0);
        5'h14: src3_rdy = (sb[20]!=1'b0);
        5'h15: src3_rdy = (sb[21]!=1'b0);
        5'h16: src3_rdy = (sb[22]!=1'b0);
        5'h17: src3_rdy = (sb[23]!=1'b0);
        5'h18: src3_rdy = (sb[24]!=1'b0);
        5'h19: src3_rdy = (sb[25]!=1'b0);
        5'h1a: src3_rdy = (sb[26]!=1'b0);
        5'h1b: src3_rdy = (sb[27]!=1'b0);
        5'h1c: src3_rdy = (sb[28]!=1'b0);
        5'h1d: src3_rdy = (sb[29]!=1'b0);
        5'h1e: src3_rdy = (sb[30]!=1'b0);
        5'h1f: src3_rdy = (sb[31]!=1'b0);
      endcase
    end

  assign next_sb_addr_0 = dst0_addr;
  assign next_sb_addr_1 = dst1_addr;
  assign next_sb_addr_2 = wb0_addr;
  assign next_sb_addr_3 = wb1_addr;
 
  
  //////////////////////////////////////////////////////////////////////////////
  // SB_UNLOCK
  //////////////////////////////////////////////////////////////////////////////
  always @ (dst0_vld or
			dst0_addr or
			sb or
			dst1_vld or
			dst1_addr or
			wb0_vld or
			wb1_vld) 
begin
    // decode/issue ports (lock) 
    //next_sb[0] = 1'b0;
    if(dst0_vld && dst0_rdy==1'b1) begin
      next_sb_vld[0] = 1'b1;
    end else begin
      next_sb_vld[0] = 1'b0;
    end
    //next_sb[1] = 1'b0;
    if(dst1_vld && dst1_rdy==1'b1) begin
      next_sb_vld[1] = 1'b1;
    end else begin
      next_sb_vld[1] = 1'b0;
    end
    // writeback ports (unlock)
    //next_sb[2] = 1'b1;
    if(wb0_vld) begin
      next_sb_vld[2] = 1'b1;
    end else begin
      next_sb_vld[2] = 1'b0;
    end
    //next_sb[3] = 1'b1;
    if(wb1_vld) begin
      next_sb_vld[3] = 1'b1;
    end else begin
      next_sb_vld[3] = 1'b0;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
 

////////////////////////////////////////////////////////////////////////////////
// *** sequential logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_UPDATE
  //////////////////////////////////////////////////////////////////////////////
  // update the scoreboard
  // for reset
  //////////////////////////////////////////////////////////////////////////////
 always @ (posedge clk) begin: SB_UPDATE
    if( reset == 1 ) begin
      sb[31:0] <= 32'hffffffff;
    end
    else begin
      if(next_sb_vld[0])
        begin
          case(next_sb_addr_0)
            5'h0: sb[0] <= 0;
            5'h1: sb[1] <= 0;
            5'h2: sb[2] <= 0;
            5'h3: sb[3] <= 0;
            5'h4: sb[4] <= 0;
            5'h5: sb[5] <= 0;
            5'h6: sb[6] <= 0;
            5'h7: sb[7] <= 0;
            5'h8: sb[8] <= 0;
            5'h9: sb[9] <= 0;
            5'ha: sb[10] <= 0;
            5'hb: sb[11] <= 0;
            5'hc: sb[12] <= 0;
            5'hd: sb[13] <= 0;
            5'he: sb[14] <= 0;
            5'hf: sb[15] <= 0;
            5'h10: sb[16] <= 0;
            5'h11: sb[17] <= 0;
            5'h12: sb[18] <= 0;
            5'h13: sb[19] <= 0;
            5'h14: sb[20] <= 0;
            5'h15: sb[21] <= 0;
            5'h16: sb[22] <= 0;
            5'h17: sb[23] <= 0;
            5'h18: sb[24] <= 0;
            5'h19: sb[25] <= 0;
            5'h1a: sb[26] <= 0;
            5'h1b: sb[27] <= 0;
            5'h1c: sb[28] <= 0;
            5'h1d: sb[29] <= 0;
            5'h1e: sb[30] <= 0;
            5'h1f: sb[31] <= 0;
          endcase
        end
      if(next_sb_vld[1])
        begin
          case(next_sb_addr_1)
            5'h0: sb[0] <= 0;
            5'h1: sb[1] <= 0;
            5'h2: sb[2] <= 0;
            5'h3: sb[3] <= 0;
            5'h4: sb[4] <= 0;
            5'h5: sb[5] <= 0;
            5'h6: sb[6] <= 0;
            5'h7: sb[7] <= 0;
            5'h8: sb[8] <= 0;
            5'h9: sb[9] <= 0;
            5'ha: sb[10] <= 0;
            5'hb: sb[11] <= 0;
            5'hc: sb[12] <= 0;
            5'hd: sb[13] <= 0;
            5'he: sb[14] <= 0;
            5'hf: sb[15] <= 0;
            5'h10: sb[16] <= 0;
            5'h11: sb[17] <= 0;
            5'h12: sb[18] <= 0;
            5'h13: sb[19] <= 0;
            5'h14: sb[20] <= 0;
            5'h15: sb[21] <= 0;
            5'h16: sb[22] <= 0;
            5'h17: sb[23] <= 0;
            5'h18: sb[24] <= 0;
            5'h19: sb[25] <= 0;
            5'h1a: sb[26] <= 0;
            5'h1b: sb[27] <= 0;
            5'h1c: sb[28] <= 0;
            5'h1d: sb[29] <= 0;
            5'h1e: sb[30] <= 0;
            5'h1f: sb[31] <= 0;
          endcase
        end
      if(next_sb_vld[2])
        begin
          case(next_sb_addr_2)
            5'h0: sb[0] <= 1;
            5'h1: sb[1] <= 1;
            5'h2: sb[2] <= 1;
            5'h3: sb[3] <= 1;
            5'h4: sb[4] <= 1;
            5'h5: sb[5] <= 1;
            5'h6: sb[6] <= 1;
            5'h7: sb[7] <= 1;
            5'h8: sb[8] <= 1;
            5'h9: sb[9] <= 1;
            5'ha: sb[10] <= 1;
            5'hb: sb[11] <= 1;
            5'hc: sb[12] <= 1;
            5'hd: sb[13] <= 1;
            5'he: sb[14] <= 1;
            5'hf: sb[15] <= 1;
            5'h10: sb[16] <= 1;
            5'h11: sb[17] <= 1;
            5'h12: sb[18] <= 1;
            5'h13: sb[19] <= 1;
            5'h14: sb[20] <= 1;
            5'h15: sb[21] <= 1;
            5'h16: sb[22] <= 1;
            5'h17: sb[23] <= 1;
            5'h18: sb[24] <= 1;
            5'h19: sb[25] <= 1;
            5'h1a: sb[26] <= 1;
            5'h1b: sb[27] <= 1;
            5'h1c: sb[28] <= 1;
            5'h1d: sb[29] <= 1;
            5'h1e: sb[30] <= 1;
            5'h1f: sb[31] <= 1;
          endcase
        end

      if(next_sb_vld[3])
        begin
          case(next_sb_addr_3)
            5'h0: sb[0] <= 1;
            5'h1: sb[1] <= 1;
            5'h2: sb[2] <= 1;
            5'h3: sb[3] <= 1;
            5'h4: sb[4] <= 1;
            5'h5: sb[5] <= 1;
            5'h6: sb[6] <= 1;
            5'h7: sb[7] <= 1;
            5'h8: sb[8] <= 1;
            5'h9: sb[9] <= 1;
            5'ha: sb[10] <= 1;
            5'hb: sb[11] <= 1;
            5'hc: sb[12] <= 1;
            5'hd: sb[13] <= 1;
            5'he: sb[14] <= 1;
            5'hf: sb[15] <= 1;
            5'h10: sb[16] <= 1;
            5'h11: sb[17] <= 1;
            5'h12: sb[18] <= 1;
            5'h13: sb[19] <= 1;
            5'h14: sb[20] <= 1;
            5'h15: sb[21] <= 1;
            5'h16: sb[22] <= 1;
            5'h17: sb[23] <= 1;
            5'h18: sb[24] <= 1;
            5'h19: sb[25] <= 1;
            5'h1a: sb[26] <= 1;
            5'h1b: sb[27] <= 1;
            5'h1c: sb[28] <= 1;
            5'h1d: sb[29] <= 1;
            5'h1e: sb[30] <= 1;
            5'h1f: sb[31] <= 1;
          endcase
        end
   end
 end
endmodule

module rf_4r2w(
  // control signals
  clk,
  reset,
  enable,
  // read port signals
  read_addr_0,
  read_data_0,
  read_enable_0,
  read_addr_1,
  read_data_1,
  read_enable_1,
  read_addr_2,
  read_data_2,
  read_enable_2,
  read_addr_3,
  read_data_3,
  read_enable_3,
  // write port signals
  write_addr_0,
  write_data_0,
  write_enable_0,
  write_addr_1,
  write_data_1,
  write_enable_1
);

  // control signals
  input  clk;
  input  reset;
  input  enable;
  // read port signals
  input [4:0]   read_addr_0;
  output [31:0] read_data_0;
  input         read_enable_0;
  input [4:0]   read_addr_1;
  output [31:0] read_data_1;
  input         read_enable_1;
  input [4:0]   read_addr_2;
  output [31:0] read_data_2;
  input         read_enable_2;
  input [4:0]   read_addr_3;
  output [31:0] read_data_3;
  input         read_enable_3;
  // write port signals
  input [4:0]   write_addr_0;
  input [31:0]  write_data_0;
  input         write_enable_0;
  input [4:0]   write_addr_1;
  input [31:0]  write_data_1;
  input         write_enable_1;
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // local wires and regs
  //////////////////////////////////////////////////////////////////////////////
  reg[31:0] rf_0; 
  reg[31:0] rf_1; 
  reg[31:0] rf_2; 
  reg[31:0] rf_3; 
  reg[31:0] rf_4; 
  reg[31:0] rf_5; 
  reg[31:0] rf_6; 
  reg[31:0] rf_7; 
  reg[31:0] rf_8; 
  reg[31:0] rf_9; 
  reg[31:0] rf_10; 
  reg[31:0] rf_11; 
  reg[31:0] rf_12; 
  reg[31:0] rf_13; 
  reg[31:0] rf_14; 
  reg[31:0] rf_15; 
  reg[31:0] rf_16; 
  reg[31:0] rf_17; 
  reg[31:0] rf_18; 
  reg[31:0] rf_19; 
  reg[31:0] rf_20; 
  reg[31:0] rf_21; 
  reg[31:0] rf_22; 
  reg[31:0] rf_23; 
  reg[31:0] rf_24; 
  reg[31:0] rf_25; 
  reg[31:0] rf_26; 
  reg[31:0] rf_27; 
  reg[31:0] rf_28; 
  reg[31:0] rf_29; 
  reg[31:0] rf_30; 
  reg[31:0] rf_31; 

  wire we_0;
  wire we_1;
  //////////////////////////////////////////////////////////////////////////////
  // Continuous assignments
  //////////////////////////////////////////////////////////////////////////////
  //assign read_data_0 = (read_enable_0 && (read_addr_0 != 5'b0))? rf[read_addr_0] : 32'b0;
  always@(read_enable_0 or read_addr_0 or rf_0 or rf_1 or rf_2 or rf_3 or rf_4 or rf_5 or rf_6 or rf_7 or rf_8 or rf_9 or rf_10 or rf_11 or rf_12 or rf_13 or rf_14 or rf_15 or rf_16 or rf_17 or rf_18 or rf_19 or rf_20 or rf_21 or rf_22 or rf_23 or rf_24 or rf_25 or rf_26 or rf_27 or rf_28 or rf_29 or rf_30 or rf_31)
  begin
    if((read_enable_0 && (read_addr_0 != 5'b0))==0)
      read_data_0 = 32'h0;
    else
     case(read_addr_0)
       5'h0: read_data_0 = rf_0;
       5'h1: read_data_0 = rf_1;
       5'h2: read_data_0 = rf_2;
       5'h3: read_data_0 = rf_3;
       5'h4: read_data_0 = rf_4;
       5'h5: read_data_0 = rf_5;
       5'h6: read_data_0 = rf_6;
       5'h7: read_data_0 = rf_7;
       5'h8: read_data_0 = rf_8;
       5'h9: read_data_0 = rf_9;
       5'h10: read_data_0 = rf_10;
       5'h11: read_data_0 = rf_11;
       5'h12: read_data_0 = rf_12;
       5'h13: read_data_0 = rf_13;
       5'h14: read_data_0 = rf_14;
       5'h15: read_data_0 = rf_15;
       5'h16: read_data_0 = rf_16;
       5'h17: read_data_0 = rf_17;
       5'h18: read_data_0 = rf_18;
       5'h19: read_data_0 = rf_19;
       5'h20: read_data_0 = rf_20;
       5'h21: read_data_0 = rf_21;
       5'h22: read_data_0 = rf_22;
       5'h23: read_data_0 = rf_23;
       5'h24: read_data_0 = rf_24;
       5'h25: read_data_0 = rf_25;
       5'h26: read_data_0 = rf_26;
       5'h27: read_data_0 = rf_27;
       5'h28: read_data_0 = rf_28;
       5'h29: read_data_0 = rf_29;
       5'h30: read_data_0 = rf_30;
       5'h31: read_data_0 = rf_31;
     endcase
  end
 // assign read_data_1 = (read_enable_1 && (read_addr_1 != 5'b0))? rf[read_addr_1] : 32'b0;
always@(read_enable_1 or read_addr_1 or rf_0 or rf_1 or rf_2 or rf_3 or rf_4 or rf_5 or rf_6 or rf_7 or rf_8 or rf_9 or rf_10 or rf_11 or rf_12 or rf_13 or rf_14 or rf_15 or rf_16 or rf_17 or rf_18 or rf_19 or rf_20 or rf_21 or rf_22 or rf_23 or rf_24 or rf_25 or rf_26 or rf_27 or rf_28 or rf_29 or rf_30 or rf_31)
  begin
    if((read_enable_1 && (read_addr_1 != 5'b0))==0)
      read_data_1 = 32'h0;
    else
     case(read_addr_1)
       5'h0: read_data_1 = rf_0;
       5'h1: read_data_1 = rf_1;
       5'h2: read_data_1 = rf_2;
       5'h3: read_data_1 = rf_3;
       5'h4: read_data_1 = rf_4;
       5'h5: read_data_1 = rf_5;
       5'h6: read_data_1 = rf_6;
       5'h7: read_data_1 = rf_7;
       5'h8: read_data_1 = rf_8;
       5'h9: read_data_1 = rf_9;
       5'h10: read_data_1 = rf_10;
       5'h11: read_data_1 = rf_11;
       5'h12: read_data_1 = rf_12;
       5'h13: read_data_1 = rf_13;
       5'h14: read_data_1 = rf_14;
       5'h15: read_data_1 = rf_15;
       5'h16: read_data_1 = rf_16;
       5'h17: read_data_1 = rf_17;
       5'h18: read_data_1 = rf_18;
       5'h19: read_data_1 = rf_19;
       5'h20: read_data_1 = rf_20;
       5'h21: read_data_1 = rf_21;
       5'h22: read_data_1 = rf_22;
       5'h23: read_data_1 = rf_23;
       5'h24: read_data_1 = rf_24;
       5'h25: read_data_1 = rf_25;
       5'h26: read_data_1 = rf_26;
       5'h27: read_data_1 = rf_27;
       5'h28: read_data_1 = rf_28;
       5'h29: read_data_1 = rf_29;
       5'h30: read_data_1 = rf_30;
       5'h31: read_data_1 = rf_31;
     endcase
  end

  //assign read_data_2 = (read_enable_2 && (read_addr_2 != 5'b0))? rf[read_addr_2] : 32'b0;
always@(read_enable_2 or read_addr_2 or rf_0 or rf_1 or rf_2 or rf_3 or rf_4 or rf_5 or rf_6 or rf_7 or rf_8 or rf_9 or rf_10 or rf_11 or rf_12 or rf_13 or rf_14 or rf_15 or rf_16 or rf_17 or rf_18 or rf_19 or rf_20 or rf_21 or rf_22 or rf_23 or rf_24 or rf_25 or rf_26 or rf_27 or rf_28 or rf_29 or rf_30 or rf_31)
  begin
    if((read_enable_2 && (read_addr_2 != 5'b0))==0)
      read_data_2 = 32'h0;
    else
     case(read_addr_2)
       5'h0: read_data_2 = rf_0;
       5'h1: read_data_2 = rf_1;
       5'h2: read_data_2 = rf_2;
       5'h3: read_data_2 = rf_3;
       5'h4: read_data_2 = rf_4;
       5'h5: read_data_2 = rf_5;
       5'h6: read_data_2 = rf_6;
       5'h7: read_data_2 = rf_7;
       5'h8: read_data_2 = rf_8;
       5'h9: read_data_2 = rf_9;
       5'h10: read_data_2 = rf_10;
       5'h11: read_data_2 = rf_11;
       5'h12: read_data_2 = rf_12;
       5'h13: read_data_2 = rf_13;
       5'h14: read_data_2 = rf_14;
       5'h15: read_data_2 = rf_15;
       5'h16: read_data_2 = rf_16;
       5'h17: read_data_2 = rf_17;
       5'h18: read_data_2 = rf_18;
       5'h19: read_data_2 = rf_19;
       5'h20: read_data_2 = rf_20;
       5'h21: read_data_2 = rf_21;
       5'h22: read_data_2 = rf_22;
       5'h23: read_data_2 = rf_23;
       5'h24: read_data_2 = rf_24;
       5'h25: read_data_2 = rf_25;
       5'h26: read_data_2 = rf_26;
       5'h27: read_data_2 = rf_27;
       5'h28: read_data_2 = rf_28;
       5'h29: read_data_2 = rf_29;
       5'h30: read_data_2 = rf_30;
       5'h31: read_data_2 = rf_31;
     endcase
  end

  //assign read_data_3 = (read_enable_3 && (read_addr_3 != 5'b0))? rf[read_addr_3] : 32'b0;
always@(read_enable_3 or read_addr_3 or rf_0 or rf_1 or rf_2 or rf_3 or rf_4 or rf_5 or rf_6 or rf_7 or rf_8 or rf_9 or rf_10 or rf_11 or rf_12 or rf_13 or rf_14 or rf_15 or rf_16 or rf_17 or rf_18 or rf_19 or rf_20 or rf_21 or rf_22 or rf_23 or rf_24 or rf_25 or rf_26 or rf_27 or rf_28 or rf_29 or rf_30 or rf_31)
  begin
    if((read_enable_3 && (read_addr_3 != 5'b0))==0)
      read_data_3 = 32'h0;
    else
     case(read_addr_3)
       5'h0: read_data_3 = rf_0;
       5'h1: read_data_3 = rf_1;
       5'h2: read_data_3 = rf_2;
       5'h3: read_data_3 = rf_3;
       5'h4: read_data_3 = rf_4;
       5'h5: read_data_3 = rf_5;
       5'h6: read_data_3 = rf_6;
       5'h7: read_data_3 = rf_7;
       5'h8: read_data_3 = rf_8;
       5'h9: read_data_3 = rf_9;
       5'h10: read_data_3 = rf_10;
       5'h11: read_data_3 = rf_11;
       5'h12: read_data_3 = rf_12;
       5'h13: read_data_3 = rf_13;
       5'h14: read_data_3 = rf_14;
       5'h15: read_data_3 = rf_15;
       5'h16: read_data_3 = rf_16;
       5'h17: read_data_3 = rf_17;
       5'h18: read_data_3 = rf_18;
       5'h19: read_data_3 = rf_19;
       5'h20: read_data_3 = rf_20;
       5'h21: read_data_3 = rf_21;
       5'h22: read_data_3 = rf_22;
       5'h23: read_data_3 = rf_23;
       5'h24: read_data_3 = rf_24;
       5'h25: read_data_3 = rf_25;
       5'h26: read_data_3 = rf_26;
       5'h27: read_data_3 = rf_27;
       5'h28: read_data_3 = rf_28;
       5'h29: read_data_3 = rf_29;
       5'h30: read_data_3 = rf_30;
       5'h31: read_data_3 = rf_31;
     endcase
  end

  assign we_0 = write_enable_0 && !( write_addr_0 == 5'b0);
  assign we_1 = write_enable_1 && !( write_addr_1 == 5'b0);
  always @ (posedge clk or posedge reset)
  begin
    if( reset == 1 ) begin
      //for(i=0;i<32;i=i+1) begin
      //  rf[i] <= 32'b0;    
      //end
      rf_0 <= 32'h0;
      rf_1 <= 32'h0;
      rf_2 <= 32'h0;
      rf_3 <= 32'h0;
      rf_4 <= 32'h0;
      rf_5 <= 32'h0;
      rf_6 <= 32'h0;
      rf_7 <= 32'h0;
      rf_8 <= 32'h0;
      rf_9 <= 32'h0;
      rf_10 <= 32'h0;
      rf_11 <= 32'h0;
      rf_12 <= 32'h0;
      rf_13 <= 32'h0;
      rf_14 <= 32'h0;
      rf_15 <= 32'h0;
      rf_16 <= 32'h0;
      rf_17 <= 32'h0;
      rf_18 <= 32'h0;
      rf_19 <= 32'h0;
      rf_20 <= 32'h0;
      rf_21 <= 32'h0;
      rf_22 <= 32'h0;
      rf_23 <= 32'h0;
      rf_24 <= 32'h0;
      rf_25 <= 32'h0;
      rf_26 <= 32'h0;
      rf_27 <= 32'h0;
      rf_28 <= 32'h0;
      rf_29 <= 32'h0;
      rf_30 <= 32'h0;
      rf_31 <= 32'h0;

    end

    // regular clocked operation
    else begin

      // write port 0
      if( we_0 )
        begin
        //rf[write_addr_0] <= write_data_0;
        case(write_addr_0)
          5'h0: rf_0 <= write_data_0;
          5'h1: rf_1 <= write_data_0;
          5'h2: rf_2 <= write_data_0;
          5'h3: rf_3 <= write_data_0;
          5'h4: rf_4 <= write_data_0;
          5'h5: rf_5 <= write_data_0;
          5'h6: rf_6 <= write_data_0;
          5'h7: rf_7 <= write_data_0;
          5'h8: rf_8 <= write_data_0;
          5'h9: rf_9 <= write_data_0;
          5'h10: rf_10 <= write_data_0;
          5'h11: rf_11 <= write_data_0;
          5'h12: rf_12 <= write_data_0;
          5'h13: rf_13 <= write_data_0;
          5'h14: rf_14 <= write_data_0;
          5'h15: rf_15 <= write_data_0;
          5'h16: rf_16 <= write_data_0;
          5'h17: rf_17 <= write_data_0;
          5'h18: rf_18 <= write_data_0;
          5'h19: rf_19 <= write_data_0;
          5'h20: rf_20 <= write_data_0;
          5'h21: rf_21 <= write_data_0;
          5'h22: rf_22 <= write_data_0;
          5'h23: rf_23 <= write_data_0;
          5'h24: rf_24 <= write_data_0;
          5'h25: rf_25 <= write_data_0;
          5'h26: rf_26 <= write_data_0;
          5'h27: rf_27 <= write_data_0;
          5'h28: rf_28 <= write_data_0;
          5'h29: rf_29 <= write_data_0;
          5'h30: rf_30 <= write_data_0;
          5'h31: rf_31 <= write_data_0;
         endcase 
        end
      // write port 1
      if( we_1 )
        //rf[write_addr_1] <= write_data_1;
        begin
         case(write_addr_1)
          5'h0: rf_0 <= write_data_1;
          5'h1: rf_1 <= write_data_1;
          5'h2: rf_2 <= write_data_1;
          5'h3: rf_3 <= write_data_1;
          5'h4: rf_4 <= write_data_1;
          5'h5: rf_5 <= write_data_1;
          5'h6: rf_6 <= write_data_1;
          5'h7: rf_7 <= write_data_1;
          5'h8: rf_8 <= write_data_1;
          5'h9: rf_9 <= write_data_1;
          5'h10: rf_10 <= write_data_1;
          5'h11: rf_11 <= write_data_1;
          5'h12: rf_12 <= write_data_1;
          5'h13: rf_13 <= write_data_1;
          5'h14: rf_14 <= write_data_1;
          5'h15: rf_15 <= write_data_1;
          5'h16: rf_16 <= write_data_1;
          5'h17: rf_17 <= write_data_1;
          5'h18: rf_18 <= write_data_1;
          5'h19: rf_19 <= write_data_1;
          5'h20: rf_20 <= write_data_1;
          5'h21: rf_21 <= write_data_1;
          5'h22: rf_22 <= write_data_1;
          5'h23: rf_23 <= write_data_1;
          5'h24: rf_24 <= write_data_1;
          5'h25: rf_25 <= write_data_1;
          5'h26: rf_26 <= write_data_1;
          5'h27: rf_27 <= write_data_1;
          5'h28: rf_28 <= write_data_1;
          5'h29: rf_29 <= write_data_1;
          5'h30: rf_30 <= write_data_1;
          5'h31: rf_31 <= write_data_1;
         endcase
        end
    end
  end
endmodule

module sprf(
  // control signals
  clk,
  reset,
  read_addr, // register address
  read_data,
  read_enable,
  write_enable
);

  // control signals
  input clk;
  input reset;
  input [3:0] read_addr; // register address
  output [31:0] read_data;
  input read_enable;
  input write_enable;
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // local wires and regs
  //////////////////////////////////////////////////////////////////////////////
  //reg [31:0] rf[0:16-1];
  reg [31:0] rf_0;
  reg [31:0] rf_1;
  reg [31:0] rf_2;
  reg [31:0] rf_3;
  reg [31:0] rf_4;
  reg [31:0] rf_5;
  reg [31:0] rf_6;
  reg [31:0] rf_7;
  reg [31:0] rf_8;
  reg [31:0] rf_9;
  reg [31:0] rf_10;
  reg [31:0] rf_11;
  reg [31:0] rf_12;
  reg [31:0] rf_13;
  reg [31:0] rf_14;
  reg [31:0] rf_15;
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // Continuous assignments
  //////////////////////////////////////////////////////////////////////////////
  //assign read_data = rf[read_addr];
  always @(read_addr or rf_0 or rf_1 or rf_2 or rf_3 or rf_4 or rf_5 or rf_6 or rf_7 or rf_8 or rf_9 or rf_10 or rf_11 or rf_12 or rf_13 or rf_14 or rf_15)
   case(read_addr)
     5'h0: read_data  = rf_0;
     5'h1: read_data  = rf_1;
     5'h2: read_data  = rf_2;
     5'h3: read_data  = rf_3;
     5'h4: read_data  = rf_4;
     5'h5: read_data  = rf_5;
     5'h6: read_data  = rf_6;
     5'h7: read_data  = rf_7;
     5'h8: read_data  = rf_8;
     5'h9: read_data  = rf_9;
     5'h10: read_data = rf_10;
     5'h11: read_data = rf_11;
     5'h12: read_data = rf_12;
     5'h13: read_data = rf_13;
     5'h14: read_data = rf_14;
     5'h15: read_data = rf_15;
   endcase
  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk or posedge reset) begin: SPRF_CLK

    ////////////////////////////////////////////////////////////////////////////
    // reset
    if( reset == 1'b1 ) begin
      //reset sprf state
      rf_0 <= 32'h00000000; 
      rf_1 <= 32'h00000100;
      rf_2 <= 32'h00000008;
      rf_3 <= 32'h00000010;
      //SPRF_UNDEF_3 = 3,
      rf_4 <= 32'h00000000;
      rf_5 <= 32'h00000100;
      rf_6 <= 32'h00000000;
      rf_7 <= 32'h00001000;
      rf_8 <= 32'h00000004;
      rf_9 <= 32'h000000ff;
      rf_10 <= 32'h00000200;
    end

    ////////////////////////////////////////////////////////////////////////////
  
  end  
  //////////////////////////////////////////////////////////////////////////////

endmodule

module bypass_select0(
  // operand sourced from two places
  address_in_rf,                 // Address of the desired data
  data_in_rf,                   // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  data_in_bpaddress_0,
  data_in_bpdata_0,
  data_in_bpvalid_0,
  data_in_bpaddress_1,
  data_in_bpdata_1,
  data_in_bpvalid_1,
  data_in_bpaddress_2,
  data_in_bpdata_2,
  data_in_bpvalid_2,
  data_in_bpaddress_3,
  data_in_bpdata_3,
  data_in_bpvalid_3,
  data_in_bpaddress_4,
  data_in_bpdata_4,
  data_in_bpvalid_4,
  data_in_bpaddress_5,
  data_in_bpdata_5,
  data_in_bpvalid_5,
  data_in_bpaddress_6,
  data_in_bpdata_6,
  data_in_bpvalid_6,
  data_in_bpaddress_7,
  data_in_bpdata_7,
  data_in_bpvalid_7,
  data_in_bpaddress_8,
  data_in_bpdata_8,
  data_in_bpvalid_8,
  data_in_bpaddress_9,
  data_in_bpdata_9,
  data_in_bpvalid_9,
  data_in_bpaddress_10,
  data_in_bpdata_10,
  data_in_bpvalid_10,
  // output the operand
  data_out,   // Desired data output
  src_is_bp         // Indicate that value was obtained from bypass
);

  // operand sourced from two places
  input [4:0] address_in_rf;                 // Address of the desired data
  input [31:0] data_in_rf;                  // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  input [4:0] data_in_bpaddress_0;
  input [31:0] data_in_bpdata_0;
  input  data_in_bpvalid_0;
  input [4:0] data_in_bpaddress_1;
  input [31:0] data_in_bpdata_1;
  input  data_in_bpvalid_1;
  input [4:0] data_in_bpaddress_2;
  input [31:0] data_in_bpdata_2;
  input  data_in_bpvalid_2;
  input [4:0] data_in_bpaddress_3;
  input [31:0] data_in_bpdata_3;
  input  data_in_bpvalid_3;
  input [4:0] data_in_bpaddress_4;
  input [31:0] data_in_bpdata_4;
  input  data_in_bpvalid_4;
  input [4:0] data_in_bpaddress_5;
  input [31:0] data_in_bpdata_5;
  input  data_in_bpvalid_5;
  input [4:0] data_in_bpaddress_6;
  input [31:0] data_in_bpdata_6;
  input  data_in_bpvalid_6;
  input [4:0] data_in_bpaddress_7;
  input [31:0] data_in_bpdata_7;
  input  data_in_bpvalid_7;
  input [4:0] data_in_bpaddress_8;
  input [31:0] data_in_bpdata_8;
  input  data_in_bpvalid_8;
  input [4:0] data_in_bpaddress_9;
  input [31:0] data_in_bpdata_9;
  input  data_in_bpvalid_9;
  input [4:0] data_in_bpaddress_10;
  input [31:0] data_in_bpdata_10;
  input  data_in_bpvalid_10;
  // output the operand
  output [31:0] data_out;   // Desired data output
  output src_is_bp;         // Indicate that value was obtained from bypass

//  genvar i;
  always @ (data_in_bpvalid_0 or data_in_bpaddress_0 or address_in_rf or data_in_bpaddress_0 or data_in_bpdata_0 or 
            data_in_bpvalid_1 or data_in_bpaddress_1 or address_in_rf or data_in_bpaddress_1 or data_in_bpdata_1 or
            data_in_bpvalid_2 or data_in_bpaddress_2 or address_in_rf or data_in_bpaddress_2 or data_in_bpdata_2 or
            data_in_bpvalid_3 or data_in_bpaddress_3 or address_in_rf or data_in_bpaddress_3 or data_in_bpdata_3 or
            data_in_bpvalid_4 or data_in_bpaddress_4 or address_in_rf or data_in_bpaddress_4 or data_in_bpdata_4 or
            data_in_bpvalid_5 or data_in_bpaddress_5 or address_in_rf or data_in_bpaddress_5 or data_in_bpdata_5 or
            data_in_bpvalid_6 or data_in_bpaddress_6 or address_in_rf or data_in_bpaddress_6 or data_in_bpdata_6 or
            data_in_bpvalid_7 or data_in_bpaddress_7 or address_in_rf or data_in_bpaddress_7 or data_in_bpdata_7 or
            data_in_bpvalid_8 or data_in_bpaddress_8 or address_in_rf or data_in_bpaddress_8 or data_in_bpdata_8 or
            data_in_bpvalid_9 or data_in_bpaddress_9 or address_in_rf or data_in_bpaddress_9 or data_in_bpdata_9 or
            data_in_bpvalid_10 or data_in_bpaddress_10 or address_in_rf or data_in_bpaddress_10 or data_in_bpdata_10
           ) begin
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == address_in_rf) && data_in_bpaddress_0 != 5'b0) begin
      data_out = data_in_bpdata_0; // Exec0 - INT
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == address_in_rf) && data_in_bpaddress_1 != 5'b0) begin
      data_out = data_in_bpdata_1; // Exec0 - MEM
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == address_in_rf) && data_in_bpaddress_2 != 5'b0) begin
      data_out = data_in_bpdata_2; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == address_in_rf) && data_in_bpaddress_3 != 5'b0) begin
      data_out = data_in_bpdata_3; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == address_in_rf) && data_in_bpaddress_4 != 5'b0) begin
      data_out = data_in_bpdata_4; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == address_in_rf) && data_in_bpaddress_5 != 5'b0) begin
      data_out = data_in_bpdata_5; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == address_in_rf) && data_in_bpaddress_6 != 5'b0) begin
      data_out = data_in_bpdata_6; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == address_in_rf) && data_in_bpaddress_7 != 5'b0) begin
      data_out = data_in_bpdata_7; // WB - slot1
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == address_in_rf) && data_in_bpaddress_8 != 5'b0) begin
      data_out = data_in_bpdata_8; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == address_in_rf) && data_in_bpaddress_9 != 5'b0) begin
      data_out = data_in_bpdata_9; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == address_in_rf) && data_in_bpaddress_10 != 5'b0) begin
      data_out = data_in_bpdata_10; // WB - slot1
      src_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      data_out = data_in_rf;
      src_is_bp = 1'b0;
    end
  end
endmodule

module bypass_select1(
  // operand sourced from two places
  address_in_rf,                 // Address of the desired data
  data_in_rf,                   // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  data_in_bpaddress_0,
  data_in_bpdata_0,
  data_in_bpvalid_0,
  data_in_bpaddress_1,
  data_in_bpdata_1,
  data_in_bpvalid_1,
  data_in_bpaddress_2,
  data_in_bpdata_2,
  data_in_bpvalid_2,
  data_in_bpaddress_3,
  data_in_bpdata_3,
  data_in_bpvalid_3,
  data_in_bpaddress_4,
  data_in_bpdata_4,
  data_in_bpvalid_4,
  data_in_bpaddress_5,
  data_in_bpdata_5,
  data_in_bpvalid_5,
  data_in_bpaddress_6,
  data_in_bpdata_6,
  data_in_bpvalid_6,
  data_in_bpaddress_7,
  data_in_bpdata_7,
  data_in_bpvalid_7,
  data_in_bpaddress_8,
  data_in_bpdata_8,
  data_in_bpvalid_8,
  data_in_bpaddress_9,
  data_in_bpdata_9,
  data_in_bpvalid_9,
  data_in_bpaddress_10,
  data_in_bpdata_10,
  data_in_bpvalid_10,
  // output the operand
  data_out,   // Desired data output
  src_is_bp         // Indicate that value was obtained from bypass
);

  // operand sourced from two places
  input [4:0] address_in_rf;                 // Address of the desired data
  input [31:0] data_in_rf;                  // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  input [4:0] data_in_bpaddress_0;
  input [31:0] data_in_bpdata_0;
  input  data_in_bpvalid_0;
  input [4:0] data_in_bpaddress_1;
  input [31:0] data_in_bpdata_1;
  input  data_in_bpvalid_1;
  input [4:0] data_in_bpaddress_2;
  input [31:0] data_in_bpdata_2;
  input  data_in_bpvalid_2;
  input [4:0] data_in_bpaddress_3;
  input [31:0] data_in_bpdata_3;
  input  data_in_bpvalid_3;
  input [4:0] data_in_bpaddress_4;
  input [31:0] data_in_bpdata_4;
  input  data_in_bpvalid_4;
  input [4:0] data_in_bpaddress_5;
  input [31:0] data_in_bpdata_5;
  input  data_in_bpvalid_5;
  input [4:0] data_in_bpaddress_6;
  input [31:0] data_in_bpdata_6;
  input  data_in_bpvalid_6;
  input [4:0] data_in_bpaddress_7;
  input [31:0] data_in_bpdata_7;
  input  data_in_bpvalid_7;
  input [4:0] data_in_bpaddress_8;
  input [31:0] data_in_bpdata_8;
  input  data_in_bpvalid_8;
  input [4:0] data_in_bpaddress_9;
  input [31:0] data_in_bpdata_9;
  input  data_in_bpvalid_9;
  input [4:0] data_in_bpaddress_10;
  input [31:0] data_in_bpdata_10;
  input  data_in_bpvalid_10;
  // output the operand
  output [31:0] data_out;   // Desired data output
  output src_is_bp;         // Indicate that value was obtained from bypass

//  genvar i;
  always @ (data_in_bpvalid_0 or data_in_bpaddress_0 or address_in_rf or data_in_bpaddress_0 or data_in_bpdata_0 or 
            data_in_bpvalid_1 or data_in_bpaddress_1 or address_in_rf or data_in_bpaddress_1 or data_in_bpdata_1 or
            data_in_bpvalid_2 or data_in_bpaddress_2 or address_in_rf or data_in_bpaddress_2 or data_in_bpdata_2 or
            data_in_bpvalid_3 or data_in_bpaddress_3 or address_in_rf or data_in_bpaddress_3 or data_in_bpdata_3 or
            data_in_bpvalid_4 or data_in_bpaddress_4 or address_in_rf or data_in_bpaddress_4 or data_in_bpdata_4 or
            data_in_bpvalid_5 or data_in_bpaddress_5 or address_in_rf or data_in_bpaddress_5 or data_in_bpdata_5 or
            data_in_bpvalid_6 or data_in_bpaddress_6 or address_in_rf or data_in_bpaddress_6 or data_in_bpdata_6 or
            data_in_bpvalid_7 or data_in_bpaddress_7 or address_in_rf or data_in_bpaddress_7 or data_in_bpdata_7 or
            data_in_bpvalid_8 or data_in_bpaddress_8 or address_in_rf or data_in_bpaddress_8 or data_in_bpdata_8 or
            data_in_bpvalid_9 or data_in_bpaddress_9 or address_in_rf or data_in_bpaddress_9 or data_in_bpdata_9 or
            data_in_bpvalid_10 or data_in_bpaddress_10 or address_in_rf or data_in_bpaddress_10 or data_in_bpdata_10
           ) begin
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == address_in_rf) && data_in_bpaddress_0 != 5'b0) begin
      data_out = data_in_bpdata_0; // Exec0 - INT
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == address_in_rf) && data_in_bpaddress_1 != 5'b0) begin
      data_out = data_in_bpdata_1; // Exec0 - MEM
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == address_in_rf) && data_in_bpaddress_2 != 5'b0) begin
      data_out = data_in_bpdata_2; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == address_in_rf) && data_in_bpaddress_3 != 5'b0) begin
      data_out = data_in_bpdata_3; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == address_in_rf) && data_in_bpaddress_4 != 5'b0) begin
      data_out = data_in_bpdata_4; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == address_in_rf) && data_in_bpaddress_5 != 5'b0) begin
      data_out = data_in_bpdata_5; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == address_in_rf) && data_in_bpaddress_6 != 5'b0) begin
      data_out = data_in_bpdata_6; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == address_in_rf) && data_in_bpaddress_7 != 5'b0) begin
      data_out = data_in_bpdata_7; // WB - slot1
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == address_in_rf) && data_in_bpaddress_8 != 5'b0) begin
      data_out = data_in_bpdata_8; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == address_in_rf) && data_in_bpaddress_9 != 5'b0) begin
      data_out = data_in_bpdata_9; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == address_in_rf) && data_in_bpaddress_10 != 5'b0) begin
      data_out = data_in_bpdata_10; // WB - slot1
      src_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      data_out = data_in_rf;
      src_is_bp = 1'b0;
    end
  end
endmodule
module bypass_select2(
  // operand sourced from two places
  address_in_rf,                 // Address of the desired data
  data_in_rf,                   // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  data_in_bpaddress_0,
  data_in_bpdata_0,
  data_in_bpvalid_0,
  data_in_bpaddress_1,
  data_in_bpdata_1,
  data_in_bpvalid_1,
  data_in_bpaddress_2,
  data_in_bpdata_2,
  data_in_bpvalid_2,
  data_in_bpaddress_3,
  data_in_bpdata_3,
  data_in_bpvalid_3,
  data_in_bpaddress_4,
  data_in_bpdata_4,
  data_in_bpvalid_4,
  data_in_bpaddress_5,
  data_in_bpdata_5,
  data_in_bpvalid_5,
  data_in_bpaddress_6,
  data_in_bpdata_6,
  data_in_bpvalid_6,
  data_in_bpaddress_7,
  data_in_bpdata_7,
  data_in_bpvalid_7,
  data_in_bpaddress_8,
  data_in_bpdata_8,
  data_in_bpvalid_8,
  data_in_bpaddress_9,
  data_in_bpdata_9,
  data_in_bpvalid_9,
  data_in_bpaddress_10,
  data_in_bpdata_10,
  data_in_bpvalid_10,
  // output the operand
  data_out,   // Desired data output
  src_is_bp         // Indicate that value was obtained from bypass
);

  // operand sourced from two places
  input [4:0] address_in_rf;                 // Address of the desired data
  input [31:0] data_in_rf;                  // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  input [4:0] data_in_bpaddress_0;
  input [31:0] data_in_bpdata_0;
  input  data_in_bpvalid_0;
  input [4:0] data_in_bpaddress_1;
  input [31:0] data_in_bpdata_1;
  input  data_in_bpvalid_1;
  input [4:0] data_in_bpaddress_2;
  input [31:0] data_in_bpdata_2;
  input  data_in_bpvalid_2;
  input [4:0] data_in_bpaddress_3;
  input [31:0] data_in_bpdata_3;
  input  data_in_bpvalid_3;
  input [4:0] data_in_bpaddress_4;
  input [31:0] data_in_bpdata_4;
  input  data_in_bpvalid_4;
  input [4:0] data_in_bpaddress_5;
  input [31:0] data_in_bpdata_5;
  input  data_in_bpvalid_5;
  input [4:0] data_in_bpaddress_6;
  input [31:0] data_in_bpdata_6;
  input  data_in_bpvalid_6;
  input [4:0] data_in_bpaddress_7;
  input [31:0] data_in_bpdata_7;
  input  data_in_bpvalid_7;
  input [4:0] data_in_bpaddress_8;
  input [31:0] data_in_bpdata_8;
  input  data_in_bpvalid_8;
  input [4:0] data_in_bpaddress_9;
  input [31:0] data_in_bpdata_9;
  input  data_in_bpvalid_9;
  input [4:0] data_in_bpaddress_10;
  input [31:0] data_in_bpdata_10;
  input  data_in_bpvalid_10;
  // output the operand
  output [31:0] data_out;   // Desired data output
  output src_is_bp;         // Indicate that value was obtained from bypass

//  genvar i;
  always @ (data_in_bpvalid_0 or data_in_bpaddress_0 or address_in_rf or data_in_bpaddress_0 or data_in_bpdata_0 or 
            data_in_bpvalid_1 or data_in_bpaddress_1 or address_in_rf or data_in_bpaddress_1 or data_in_bpdata_1 or
            data_in_bpvalid_2 or data_in_bpaddress_2 or address_in_rf or data_in_bpaddress_2 or data_in_bpdata_2 or
            data_in_bpvalid_3 or data_in_bpaddress_3 or address_in_rf or data_in_bpaddress_3 or data_in_bpdata_3 or
            data_in_bpvalid_4 or data_in_bpaddress_4 or address_in_rf or data_in_bpaddress_4 or data_in_bpdata_4 or
            data_in_bpvalid_5 or data_in_bpaddress_5 or address_in_rf or data_in_bpaddress_5 or data_in_bpdata_5 or
            data_in_bpvalid_6 or data_in_bpaddress_6 or address_in_rf or data_in_bpaddress_6 or data_in_bpdata_6 or
            data_in_bpvalid_7 or data_in_bpaddress_7 or address_in_rf or data_in_bpaddress_7 or data_in_bpdata_7 or
            data_in_bpvalid_8 or data_in_bpaddress_8 or address_in_rf or data_in_bpaddress_8 or data_in_bpdata_8 or
            data_in_bpvalid_9 or data_in_bpaddress_9 or address_in_rf or data_in_bpaddress_9 or data_in_bpdata_9 or
            data_in_bpvalid_10 or data_in_bpaddress_10 or address_in_rf or data_in_bpaddress_10 or data_in_bpdata_10
           ) begin
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == address_in_rf) && data_in_bpaddress_0 != 5'b0) begin
      data_out = data_in_bpdata_0; // Exec0 - INT
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == address_in_rf) && data_in_bpaddress_1 != 5'b0) begin
      data_out = data_in_bpdata_1; // Exec0 - MEM
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == address_in_rf) && data_in_bpaddress_2 != 5'b0) begin
      data_out = data_in_bpdata_2; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == address_in_rf) && data_in_bpaddress_3 != 5'b0) begin
      data_out = data_in_bpdata_3; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == address_in_rf) && data_in_bpaddress_4 != 5'b0) begin
      data_out = data_in_bpdata_4; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == address_in_rf) && data_in_bpaddress_5 != 5'b0) begin
      data_out = data_in_bpdata_5; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == address_in_rf) && data_in_bpaddress_6 != 5'b0) begin
      data_out = data_in_bpdata_6; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == address_in_rf) && data_in_bpaddress_7 != 5'b0) begin
      data_out = data_in_bpdata_7; // WB - slot1
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == address_in_rf) && data_in_bpaddress_8 != 5'b0) begin
      data_out = data_in_bpdata_8; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == address_in_rf) && data_in_bpaddress_9 != 5'b0) begin
      data_out = data_in_bpdata_9; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == address_in_rf) && data_in_bpaddress_10 != 5'b0) begin
      data_out = data_in_bpdata_10; // WB - slot1
      src_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      data_out = data_in_rf;
      src_is_bp = 1'b0;
    end
  end
endmodule

module bypass_select3(
  // operand sourced from two places
  address_in_rf,                 // Address of the desired data
  data_in_rf,                   // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  data_in_bpaddress_0,
  data_in_bpdata_0,
  data_in_bpvalid_0,
  data_in_bpaddress_1,
  data_in_bpdata_1,
  data_in_bpvalid_1,
  data_in_bpaddress_2,
  data_in_bpdata_2,
  data_in_bpvalid_2,
  data_in_bpaddress_3,
  data_in_bpdata_3,
  data_in_bpvalid_3,
  data_in_bpaddress_4,
  data_in_bpdata_4,
  data_in_bpvalid_4,
  data_in_bpaddress_5,
  data_in_bpdata_5,
  data_in_bpvalid_5,
  data_in_bpaddress_6,
  data_in_bpdata_6,
  data_in_bpvalid_6,
  data_in_bpaddress_7,
  data_in_bpdata_7,
  data_in_bpvalid_7,
  data_in_bpaddress_8,
  data_in_bpdata_8,
  data_in_bpvalid_8,
  data_in_bpaddress_9,
  data_in_bpdata_9,
  data_in_bpvalid_9,
  data_in_bpaddress_10,
  data_in_bpdata_10,
  data_in_bpvalid_10,
  // output the operand
  data_out,   // Desired data output
  src_is_bp         // Indicate that value was obtained from bypass
);

  // operand sourced from two places
  input [4:0] address_in_rf;                 // Address of the desired data
  input [31:0] data_in_rf;                  // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  input [4:0] data_in_bpaddress_0;
  input [31:0] data_in_bpdata_0;
  input  data_in_bpvalid_0;
  input [4:0] data_in_bpaddress_1;
  input [31:0] data_in_bpdata_1;
  input  data_in_bpvalid_1;
  input [4:0] data_in_bpaddress_2;
  input [31:0] data_in_bpdata_2;
  input  data_in_bpvalid_2;
  input [4:0] data_in_bpaddress_3;
  input [31:0] data_in_bpdata_3;
  input  data_in_bpvalid_3;
  input [4:0] data_in_bpaddress_4;
  input [31:0] data_in_bpdata_4;
  input  data_in_bpvalid_4;
  input [4:0] data_in_bpaddress_5;
  input [31:0] data_in_bpdata_5;
  input  data_in_bpvalid_5;
  input [4:0] data_in_bpaddress_6;
  input [31:0] data_in_bpdata_6;
  input  data_in_bpvalid_6;
  input [4:0] data_in_bpaddress_7;
  input [31:0] data_in_bpdata_7;
  input  data_in_bpvalid_7;
  input [4:0] data_in_bpaddress_8;
  input [31:0] data_in_bpdata_8;
  input  data_in_bpvalid_8;
  input [4:0] data_in_bpaddress_9;
  input [31:0] data_in_bpdata_9;
  input  data_in_bpvalid_9;
  input [4:0] data_in_bpaddress_10;
  input [31:0] data_in_bpdata_10;
  input  data_in_bpvalid_10;
  // output the operand
  output [31:0] data_out;   // Desired data output
  output src_is_bp;         // Indicate that value was obtained from bypass

//  genvar i;
  always @ (data_in_bpvalid_0 or data_in_bpaddress_0 or address_in_rf or data_in_bpaddress_0 or data_in_bpdata_0 or 
            data_in_bpvalid_1 or data_in_bpaddress_1 or address_in_rf or data_in_bpaddress_1 or data_in_bpdata_1 or
            data_in_bpvalid_2 or data_in_bpaddress_2 or address_in_rf or data_in_bpaddress_2 or data_in_bpdata_2 or
            data_in_bpvalid_3 or data_in_bpaddress_3 or address_in_rf or data_in_bpaddress_3 or data_in_bpdata_3 or
            data_in_bpvalid_4 or data_in_bpaddress_4 or address_in_rf or data_in_bpaddress_4 or data_in_bpdata_4 or
            data_in_bpvalid_5 or data_in_bpaddress_5 or address_in_rf or data_in_bpaddress_5 or data_in_bpdata_5 or
            data_in_bpvalid_6 or data_in_bpaddress_6 or address_in_rf or data_in_bpaddress_6 or data_in_bpdata_6 or
            data_in_bpvalid_7 or data_in_bpaddress_7 or address_in_rf or data_in_bpaddress_7 or data_in_bpdata_7 or
            data_in_bpvalid_8 or data_in_bpaddress_8 or address_in_rf or data_in_bpaddress_8 or data_in_bpdata_8 or
            data_in_bpvalid_9 or data_in_bpaddress_9 or address_in_rf or data_in_bpaddress_9 or data_in_bpdata_9 or
            data_in_bpvalid_10 or data_in_bpaddress_10 or address_in_rf or data_in_bpaddress_10 or data_in_bpdata_10
           ) begin
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == address_in_rf) && data_in_bpaddress_0 != 5'b0) begin
      data_out = data_in_bpdata_0; // Exec0 - INT
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == address_in_rf) && data_in_bpaddress_1 != 5'b0) begin
      data_out = data_in_bpdata_1; // Exec0 - MEM
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == address_in_rf) && data_in_bpaddress_2 != 5'b0) begin
      data_out = data_in_bpdata_2; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == address_in_rf) && data_in_bpaddress_3 != 5'b0) begin
      data_out = data_in_bpdata_3; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == address_in_rf) && data_in_bpaddress_4 != 5'b0) begin
      data_out = data_in_bpdata_4; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == address_in_rf) && data_in_bpaddress_5 != 5'b0) begin
      data_out = data_in_bpdata_5; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == address_in_rf) && data_in_bpaddress_6 != 5'b0) begin
      data_out = data_in_bpdata_6; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == address_in_rf) && data_in_bpaddress_7 != 5'b0) begin
      data_out = data_in_bpdata_7; // WB - slot1
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == address_in_rf) && data_in_bpaddress_8 != 5'b0) begin
      data_out = data_in_bpdata_8; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == address_in_rf) && data_in_bpaddress_9 != 5'b0) begin
      data_out = data_in_bpdata_9; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == address_in_rf) && data_in_bpaddress_10 != 5'b0) begin
      data_out = data_in_bpdata_10; // WB - slot1
      src_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      data_out = data_in_rf;
      src_is_bp = 1'b0;
    end
  end
endmodule

