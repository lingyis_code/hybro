
module b02 (clock , reset , linea , u );

	  input  clock;
	  input  reset;
	  input  linea;
	  output u ;
	  wire clock;
	  wire reset;
	  wire linea;
	  reg u;
	  reg [7:0] branvar[0:100];
	  reg [2 : 0] stato;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  stato = 0;
	  u <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( stato)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  stato = 1;
	  u <= 0;
	 end
	  end
	  1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	 if((!  linea)) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	  stato = 2;
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	  stato = 5;
	  end
	  u <= 0;
	 end
	  end
	  2: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 begin
	 if((!  linea)) 
	  begin 
	 branvar[7] <= branvar[7] + 1; 
	  stato = 3;
	  end
	 else 
	   begin 
	   branvar[8] <= branvar[8] + 1;   
	  stato = 6;
	  end
	  u <= 0;
	 end
	  end
	  3: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 begin
	  stato = 4;
	  u <= 0;
	 end
	  end
	  4: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	 begin
	  stato = 1;
	  u <= 1;
	 end
	  end
	  5: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	 begin
	  stato = 6;
	  u <= 0;
	 end
	  end
	  6: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	 begin
	 if((!  linea)) 
	  begin 
	 branvar[13] <= branvar[13] + 1; 
	  stato = 4;
	  end
	 else 
	   begin 
	   branvar[14] <= branvar[14] + 1;   
	  stato = 0;
	  end
	  u <= 0;
	 end
	  end
	 endcase
	  end
	 end
	  endmodule 
