
#include "svdpi.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _VC_TYPES_
#define _VC_TYPES_
/* common definitions shared with DirectC.h */

typedef unsigned int U;
typedef unsigned char UB;
typedef unsigned char scalar;
typedef struct { U c; U d;} vec32;

#define scalar_0 0
#define scalar_1 1
#define scalar_z 2
#define scalar_x 3

extern long long int ConvUP2LLI(U* a);
extern void ConvLLI2UP(long long int a1, U* a2);
extern long long int GetLLIresult();
extern void StoreLLIresult(const unsigned int* data);
typedef struct VeriC_Descriptor *vc_handle;

#ifndef SV_3_COMPATIBILITY
#define SV_STRING const char*
#else
#define SV_STRING char*
#endif

#endif /* _VC_TYPES_ */


 extern void start_build_cfg(const char* vlg_file);

 extern void record_branch(int bran_num, int frame_num);

 extern int generate_nxt_pattern(int unroll_frame_num, int pat_num);

 extern int get_var_assignment(const char* var_name, int frame_num, int bit_width);

 extern void result_stat(int bran_num_max, int frame_num, int pattern_num);

#ifdef __cplusplus
}
#endif

