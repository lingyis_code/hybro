#ifndef _VL2S_EXPR_H_
#define _VL2S_EXPR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#define DEBUG1_MODE
#define DEBUG_MODE  
#define DEBUG0_MODE //trace mode 
#define LEARN_MODE_2  //local conflict detection
#define LEARN_MODE_1  //dynamic slicing
#define LEARN_MODE_3
//#define BRAN_COV
//#define PATTERN_OUT
//#define RANDOM_GEN
//#define PAT_NUM 10000

//#define YICES_DEBUG 

#define MAXBRANNUM  512

#define MAXSTRLEN    8192
#define MAXBITNUM    64
#define MAXNUMVARS   1024
#define MAXNUMCFGS   128
#define MAXNUMBRAN   1024
#define MAXFRAMES    200
#define MAXCNST      165535
#define MAXVARNAME   50

#define MAX_VAR_IDX  65535
#define MAX_CNST_NUM  65535

#define MAX_VAR_NUMBER 10000
#define MAX_CTX_BUF_LENGTH 1024

#define MAX_NUM_INT 10

extern int  yylineno;
extern char *yyfile;
extern char *yytext;

extern FILE *yyin;

extern int yylex ();
extern void yyerror (char *str);
extern int yyparse (void);
extern int stp_caller();
struct __errStruct {
  int errorNumber;
  char errString[1024];
};

struct __errStruct make_error (int, char *);
int printError (struct __errStruct);

extern char current_range[MAXSTRLEN];

struct wire_width_tuple {
  char name[MAXSTRLEN];
  char range[MAXSTRLEN];
};

extern struct wire_width_tuple wire_width_table[MAXNUMVARS];
extern int num_width_table_entries;

char *get_wwt_entry (char *name);
void put_wwt_entry (char *name, char *range);
void print_wwt (void);

typedef struct __var {
  char name[MAXSTRLEN];
  int start;
  int end;
} _var;

typedef struct __modulecall {
  char name[MAXSTRLEN];
  char calledmodulename[MAXSTRLEN];
  char *inlist[MAXSTRLEN];
  int numins;
  char *outlist[MAXSTRLEN];
  int numouts;
  struct __modulecall *next;
} _modulecall;

typedef struct __modulestats {
  char name[MAXSTRLEN];
  _var *portorder[MAXNUMVARS];
  int numports;
  _var *input_list[MAXNUMVARS];
  int numinputs;
  _var *output_list[MAXNUMVARS];
  int numoutputs;
  _var *reg_list[MAXNUMVARS];
  int numregs;
  _var *wire_list[MAXNUMVARS];
  int numwires;
  _modulecall *occ_list;
  struct __modulestats *next;
} _modulestats;

extern _modulestats *modstats, *thismodule;

char *get_string_from_var (_var *v);
char *get_string_from_varlist (_var **v, int n);
void print_modstats (void);
_modulestats * make_module (char *name);

//================================================
//==========The following code is for CFG=========
//=============liu187@illinois.edu================
//=========================================================================
//build the used chain and defined chain
enum cfg_node_type {
  NULL_NODE    = 0,
  IFCOND_EXP   = 1,
  IFELSE_EXP   = 2,
  CASECOND_EXP = 3,
  CASE_EXP     = 4,
  CASE_DEFAULT = 5,
  BLK_AS       = 6,
  NBLK_AS      = 7,
  PORT_CONNECT = 8,
  RESET_IF     = 9
};

struct __cfg_node;
struct __var_list; 
typedef struct __var_node {
  int var_id;
  int width_start;
  int width_end;
  bool Is_input_port;
  struct __var_list * p_use2def;
  struct __var_list * p_def2use;
  struct __cfg_node * self_cfg;
  
  struct __var_node * next_node;
} _var_node;

extern _var_node * current_lvalue_node;


//===========================================================================
//build the variable table
typedef struct __var_table {
  int var_id;
  char var_name[MAXSTRLEN];
  int width_start;
  int width_end;
  bool Is_input_port;
} _var_table;

extern  int var_table_id;
 
int add_to_var_table(char * var_name, int width_start, int width_end);
int get_range_start(char * var_range);
int get_range_end(char * var_range);
int get_number_width(char * number_str);
char * get_number_string(char * number_str);
int get_var_entry(char * var_name);
void create_varlist_table();
void print_var_table(_var_table * current_var_table, int var_table_id);

//===========================================================================
//build the express tree
enum tree_node_type {
  GEQ     = 0,  // a>=b
  LEQ     = 1,  // a<=b
  LOGAND  = 2,  // a&&b
  LOGOR   = 3,  // a||b
  LOGEQ   = 4,  // a==b 
  LOGINEQ = 5,  // a!=b
  LSFT    = 6,  // a<<b  
  RSFT    = 7,  // a>>b
  ULOGNOT  = 8,  // !a 
  ULOGNEG  = 9,  // ~a 
  ULOGAND  = 10, //&a
  ULOGOR   = 11, //|a
  ULOGXOR  = 12, //^a
  ULOGNAND = 13, //~&a
  ULOGNOR  = 14, //~|a
  ULOGXNOR = 15, //~^a
  BADD     = 16, //a+b
  BSUB     = 17, //a-b
  BMUL     = 18, //a*b
  BDIV     = 19, //a/b
  BMOD     = 20, //a%b
  GAR      = 21, //a>b
  LES      = 22, //a<b
  SAND     = 23, //a&b
  SOR      = 24, //a|b
  SXOR     = 25, //a^b
  LOGXNOR  = 26, //a^b
  ASS_BLK  = 27, //a=b
  ASS_NBLK = 28, //a<=b
  NAME_PORT= 29, //.a(b)
  POSEGE   = 30, //@(posedge a)
  NEGEGE   = 31, //@(negedge a)
  EDGE     = 32, //@(edge a)
  ITE_E    = 33,  // a ? b : c
  PARSEL   = 34, // a[b:c]
  INDEX    = 35, // a[b]
  CNST     = 36, // 1,2...
  VAR      = 37, // a
  CATCOPY  = 38, // 4{a[1]}
  CONCAT   = 39  // {a[1],b[2]}
};

typedef struct __exptree_node {
  tree_node_type tree_node_t;
  struct __exptree_node * left;
  struct __exptree_node * middle;
  struct __exptree_node * right;
  int width_start;
  int width_end;
  int var_table_idx;
  char * number_string;
  bool Is_traversed;
  bool Is_top_node;
} _exptree_node;

_exptree_node * new_exptree_node(tree_node_type tree_node_t, _exptree_node * left, _exptree_node * middle, _exptree_node * right, int var_table_idx, char * number_string); 

extern void print_exptree_node(_exptree_node * p_exp_treenode);

extern void add_to_cfg_defined_chain(int var_idx,int width_start,int width_end);
extern void compute_used_var_node(_exptree_node * p_exp_treenode);
extern void compute_defined_var_node(_exptree_node * p_exp_treenode);


typedef struct __var_list {
  _var_node * p_var_node;
  bool Is_defined;
  struct __var_list * next;
} _var_list;

extern _var_list * used_chain_cur_frame;



//==========================================================================

struct __instance_node;
struct __dep_list;
struct __common_cond_list;


typedef struct __cfg_node {
  char exp_name[MAXSTRLEN];
  char case_exp[MAXSTRLEN];
  int  frame_number;
  int  branch_number;
  bool Is_branch_node;
  bool Is_exit_branch;
  int  frame_num_exhaust;
  bool exhaust_bran[MAXFRAMES];
  bool Is_traversed;
  cfg_node_type node_type;
  bool branch_taken[MAXFRAMES];
  bool guard_negated[MAXFRAMES];
  struct __cfg_node * left_node;
  struct __cfg_node * right_node;
  struct __cfg_node * brother_node;
  struct __cfg_node * next_node;
  struct __cfg_node * bran_converge_node;
  struct __dep_list * p_dep_list;
 
  int bran_num;
  bool temp_flag;
  long int Is_covered[MAXFRAMES]; 
  _var_node * used_var_chain;
  _var_node * defined_var_chain;

  //@@@
  struct __common_cond_list * p_common_list;
  bool is_visited_before1;
  int is_visited_before2;
  //@@@
  //@@@
  int cnst_stack_idx[MAXFRAMES]; 
  //@@@

  //for record the taken branch
  int int_index;
  int bit_index;
  int bit_width; 

  _exptree_node * p_exptree_node;
  struct __instance_node * instance_pointer;
} _cfg_node;

//@@@
typedef struct __common_cond_list
{
  _cfg_node * p_same;
  struct __common_cond_list * next; 
} _common_cond_list;
//@@@

typedef struct __dep_list
{
  _cfg_node * p_branch_cfg_node;
  struct __dep_list * p_dep_nxt;
} _dep_list;



typedef struct __cfg_block {
  _exptree_node * event_trigger_exp;
  _cfg_node * p_cfg_node;
  struct __cfg_block * cfg_block_next;
} _cfg_block;
void add_current_cfgblk_to_list(_cfg_block * p_cfgblk);

typedef struct __instance_node {
  char inst_name[MAXSTRLEN];
  _cfg_block * p_cfg_block; 
  _var_table * p_var_table; 
  struct __instance_node * instance_node_next; 
} _instance_node;

void add_current_inst_to_list(_instance_node * p_instance);

extern int var_index;

extern _instance_node * top_instance_list;
extern _instance_node * current_instance_node;
//extern _cfg_block * current_cfg_block;
extern _exptree_node * current_exptree_event; //each for every block

extern _cfg_node * current_cfg_node;
extern char * new_intermediate_variable();

extern void add_to_cfg_used_chain(int var_idx,int width_start,int width_end);


extern _cfg_node * cfg_table[MAXNUMCFGS];
extern _cfg_node * cfg_temp;
extern _cfg_node * cfg_top;
extern int cfg_stack_pointer;
extern _cfg_node * branch_table[MAXNUMBRAN];
extern FILE * dbg_out;
extern FILE * cfg_out;
_cfg_node * build_blank_node();
_cfg_node * build_normal_node(char exp[], _cfg_node * left, _cfg_node * right, _cfg_node * next, _cfg_node * brother);
int set_next_node(_cfg_node * cfg_x, _cfg_node * cfg_nxt);
int push_cfg_table(_cfg_node * cfg_x);
void print_cfg(_cfg_node * cfg_x);
void print_all();
void print_branch();
//get branch number
int get_branch_number(_cfg_node * cfg_x);

typedef struct __constraint_s {
  _cfg_node * p_cfg_node;
  _cfg_node * p_cfg_node_branch;
  _var_node * p_var_node_reg;
  int frame_num;
  bool Is_inverted;
  bool Is_expr;
  bool case_or_if;
  //bool defined_before; 
} _constraint_s;


typedef struct __symsate_cycle{
  int bran_map[MAX_NUM_INT];
  struct __symstate_cycle * p_sym;
} _symstate_cycle;


typedef struct __symstate_set{
  _symstate_cycle * onesymstate;
  int lasting_cycle;
  struct __symstate_set * next;
} _symstate_set;


extern _var_table * current_var_table;

extern void print_constraints(_exptree_node * p_exptree_node,_exptree_node * p_exptree_node_branch,_var_node * p_var_node_reg,int frame_num,bool Is_if_branch, bool t_or_f);
extern void yices_constraint_gen(_exptree_node * p_exptree_node,_exptree_node * p_exptree_node_branch,_var_node * p_var_node_reg, int frame_num,bool Is_if_branch, bool t_or_f);

extern void send_constraints(_cfg_node * p_cfg_node, _cfg_node * p_cfg_node_branch, _var_node * p_var_node_reg,int frame_num);
extern void merge_design_constraints(int frame_num);
//extern void push_constraint_stack(int stack_no, _constraint_s * p_constraint);
//extern _constraint_s * pop_constraint_stack(int stack_no);
extern int get_frame_up_bound(int frame_num);
extern int get_frame_down_bound(int frame_num);
extern void push_to_constraint_stack(int up_bound, int down_bound);
extern void clear_constraint_temp_stack(int start_idx, int stop_idx);
//==================================================

extern int yices_initial (int idx_max,int frame_num_max);
extern int yices_destroy (int idx_max,int frame_num_max);
extern int yices_solver ();
extern int constraint_number;

extern bool detect_local_conflict(_constraint_s * p_top, int kind);

extern bool subspace_prune(_constraint_s *);

extern _symstate_set * explored_state[MAXFRAMES];
extern int int_index;
extern int bit_index;

extern void store_symstate(int frame_num);
extern bool chk_explored_symstate(int frame_num);
extern void remove_constraints(int frame_num);
extern bool compare_bit_map(_symstate_cycle * p1, _symstate_cycle * p2);

extern void add_to_cur_used_chain_state (_var_node * p_used_chain, bool Is_var_node);
extern void free_constraint_state();
extern void free_cur_used_chain_state();
extern bool Is_in_pre_used_chain_state(_var_node * p_defined_node);
extern void print_explored_state(int frame_num);
extern void print_symstate(_symstate_cycle * p_got_state);

#endif //_VL2S_EXPR_H_
