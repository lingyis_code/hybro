#include <stdio.h>
#include "yices_c.h"
#include "vlg2sExpr.h"

int constraint_number=0;
yices_expr cnst_exprs[MAX_CNST_NUM];
//FILE * pattern_file;
//------------------------------------------------------------
//------------------------------------------------------------

yices_context ctx;
yices_expr var_expr[MAXFRAMES][MAX_VAR_IDX];
yices_var_decl var_decl[MAXFRAMES][MAX_VAR_IDX];
yices_model mod;
bool yices_start=false;
unsigned int shift_oprand;
int yices_initial(int idx_max,int frame_num_max)
{
  yices_set_verbosity(0);
  ctx = yices_mk_context();
  char * p_var_name = (char *)malloc(MAXVARNAME*sizeof(char));
  constraint_number = 0;
  if((idx_max>=MAX_VAR_IDX) || (frame_num_max>=MAXFRAMES))
  {
     printf("\n\n------------Fatal Error: MAX_VAR_IDX or MAXFRAMES overflow------------\n\n");
     exit(-1);
  }
  //yices_type 	yices_mk_bitvector_type (yices_context ctx, unsigned size)
  yices_type bv_type;
  yices_var_decl xdecl; 
  //= yices_mk_var_decl(ctx, "x", ty); 
  for(int x_idx=0; x_idx<=(frame_num_max+1); x_idx++)
    for(int y_idx=0; y_idx<=idx_max-1; y_idx++)
    {
      //printf("\n%s_%d\n",current_var_table[y_idx].var_name,x_idx); 
      sprintf(p_var_name,"%sM%d",current_var_table[y_idx].var_name,x_idx); 
      //if((current_var_table[y_idx].width_start==0)&&(current_var_table[y_idx].width_end==0))
      #ifdef DEBUG_MODE
      fprintf(cfg_out,"\n--the variable name is %s: the end width is %d===the start width is %d--\n",p_var_name,current_var_table[y_idx].width_end,current_var_table[y_idx].width_start);
      #endif
      if((current_var_table[y_idx].width_end)>=(current_var_table[y_idx].width_start))
      {
        bv_type = yices_mk_bitvector_type(ctx, ((1+current_var_table[y_idx].width_end)-(current_var_table[y_idx].width_start)));
        //bv_type = yices_mk_bitvector_type(ctx, 8);
        var_decl[x_idx][y_idx] = yices_mk_var_decl(ctx, p_var_name, bv_type);
        var_expr[x_idx][y_idx] = yices_mk_var_from_decl(ctx, var_decl[x_idx][y_idx]);
        //yices_pp_expr(var_expr[x_idx][y_idx]); 

      }
      else if((current_var_table[y_idx].width_end)<(current_var_table[y_idx].width_start))
      {
        bv_type = yices_mk_bitvector_type(ctx, ((1+current_var_table[y_idx].width_start)-(current_var_table[y_idx].width_end)));
        //bv_type = yices_mk_bitvector_type(ctx, 8);
        var_decl[x_idx][y_idx] = yices_mk_var_decl(ctx, p_var_name, bv_type);
        var_expr[x_idx][y_idx] = yices_mk_var_from_decl(ctx, var_decl[x_idx][y_idx]);
        //yices_pp_expr(var_expr[x_idx][y_idx]); 
      }
      //printf("\n");
    }
  //for(int i=0;i<result_buf_idx;i++)
  //  free(result_buf[i]);
  free(p_var_name);
  yices_start = true;
  #ifdef YICES_DEBUG
  printf("\nYICES INITIAL DONE\n");
  #endif
  //result_buf_idx = 0;
  return 1;
}
int yices_destroy(int idx_max, int frame_num_max)
{
  yices_reset(ctx);
  yices_del_context(ctx);
  yices_start = false;
  return 1;
}

int yices_solver()
{
  int idx;
  #ifdef DEBUG1_MODE
  printf("\n constraint_number is equal = %d\n",constraint_number);
  #endif
  yices_expr texpr;
  //for(idx=0; idx < constraint_number; idx++)
  for(idx=0; idx < constraint_number; idx++)
  {
    #ifdef YICES_DEBUG
    printf("idx %d:",idx);
    #endif
    //texpr = yices_mk_eq(ctx, cnst_exprs[idx], cexpr);
    texpr = cnst_exprs[idx];
    #ifdef YICES_DEBUG
    yices_pp_expr(texpr); 
    printf("\n\n");
    #endif
    yices_assert(ctx, texpr);
  }
  //yices_expr final = yices_mk_and(ctx, cnst_exprs, constraint_number); 
  //yices_pp_expr(final); 
  //yices_assert(ctx, final);
 
  lbool result = yices_check(ctx);
  if(result == l_false)  
  {
    #ifdef YICES_DEBUG
    printf("\nunsatisfied\n"); 
    #endif
    //exit(-1);
    return 1;
  } 
  else if(result == l_true )  
  {
    //yices_dump_context(ctx);
    #ifdef YICES_DEBUG
    printf("\nsatisfied\n"); 
    #endif

    //exit(-1);
    mod = yices_get_model(ctx);
    
    #ifdef YICES_DEBUG
    yices_display_model(mod);
    #endif 
    //exit(-1);
    return 0;
  }
  else
  {
    yices_dump_context(ctx);
    printf("\nYices return an unknown value: %d\n",result);
    exit(-1);
  }
}


extern "C" int get_var_assignment(char * cur_var_name, int cur_frame_num, int bit_width)
{
  int bv_value[64];
  for(int i=0; i<64; i++) 
    bv_value[i]=0;
  char * var_temp = (char *)malloc(MAXNUMVARS*sizeof(char)); 
  sprintf(var_temp,"%sM%d",cur_var_name,cur_frame_num);
  if(yices_start == false)
  {
    free(var_temp);
    return -1;
  }
  yices_var_decl xdecl = yices_get_var_decl_from_name(ctx, var_temp);
  int ret =  yices_get_bitvector_value (mod, xdecl, bit_width, bv_value);
    #ifdef YICES_DEBUG
    printf("\n********the normally returned value is %s",var_temp);
    #endif
  free(var_temp);
  if(ret == 0)
  {
    return -1;
  }
  else
  {
    int ret_value=0;
    for(int j=bit_width-1; j>=0; j--)
    {
      if(bv_value[j]==1)
        ret_value = ret_value*2 + 1;
      else
        ret_value = ret_value*2;
    } 
    #ifdef YICES_DEBUG
    printf("= %d********",ret_value);
    #endif
    return ret_value;
  }
}
int temp_copy;
void yices_constraint_gen(_exptree_node * p_exptree_node,_exptree_node * p_exptree_node_branch,_var_node * p_var_node_reg,int frame_num, bool Is_if_branch,bool t_or_f)
{
  //#ifdef DEBUG_MODE
  //printf("\nYices-------------------calling the print constraints function:--------------------");
  //#endif   
  yices_expr temp_expr1;
  yices_expr temp_expr2;
  yices_expr temp_expr3;
  yices_expr temp_top;
  yices_expr temp_left;
  yices_expr temp_middle;
  yices_expr temp_right;
  yices_expr final_exp;
  yices_expr temp_expr4;

  if(p_var_node_reg!=NULL)
  {
    //#ifdef DEBUG_MODE
    //fprintf(cfg_out,"\n Yices---at frame : x[i+1] <= x[i] \n");
    //#endif
    temp_expr1 = var_expr[frame_num+1][p_var_node_reg->var_id];
    temp_expr2 = var_expr[frame_num][p_var_node_reg->var_id];
    temp_expr1 = yices_mk_bv_extract(ctx, p_var_node_reg->width_start, p_var_node_reg->width_end, temp_expr1); 
    temp_expr2 = yices_mk_bv_extract(ctx, p_var_node_reg->width_start, p_var_node_reg->width_end, temp_expr2); 
       
    temp_top = yices_mk_eq(ctx, temp_expr1, temp_expr2);
    cnst_exprs[constraint_number++]=temp_top;
    //yices_assert(ctx, temp_top);
    return;
  }

  if((p_exptree_node!=NULL))
  {
    if((p_exptree_node->number_string!=NULL)&&(Is_if_branch==false))
    {
      #ifdef DEBUG_MODE
      fprintf(cfg_out,"\n Yices---at frame %d : %s = %s \n",frame_num,p_exptree_node->number_string,p_exptree_node_branch->number_string);
      #endif
      if(p_exptree_node->tree_node_t!=CNST)
      {
        temp_expr2=var_expr[frame_num][p_exptree_node->var_table_idx];
        temp_left = yices_mk_bv_extract(ctx, p_exptree_node->width_start, p_exptree_node->width_end, temp_expr2); 
      }

      if(p_exptree_node_branch->tree_node_t!=CNST)
      {
        temp_expr2=var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        temp_right = yices_mk_bv_extract(ctx, p_exptree_node->width_start, p_exptree_node->width_end, temp_expr2); 
      }
      else       
      {
        temp_right = yices_mk_bv_constant(ctx,(p_exptree_node->width_start-p_exptree_node->width_end)+1,atoi(p_exptree_node_branch->number_string));
      }

      temp_expr1 = yices_mk_eq(ctx, temp_left, temp_right);
      //yices_assert(ctx, temp_expr1);
      cnst_exprs[constraint_number++]=temp_expr1;

      if((p_exptree_node->tree_node_t!=PARSEL)&&(p_exptree_node->tree_node_t!=INDEX)&&(p_exptree_node->tree_node_t!=CNST)&&(p_exptree_node->tree_node_t!=VAR))
        yices_constraint_gen(NULL,p_exptree_node,NULL,frame_num,false,false);
      if((p_exptree_node_branch->tree_node_t!=PARSEL)&&(p_exptree_node_branch->tree_node_t!=INDEX)&&(p_exptree_node_branch->tree_node_t!=CNST)&&(p_exptree_node_branch->tree_node_t!=VAR))
        yices_constraint_gen(NULL,p_exptree_node_branch,NULL,frame_num,false,false);
    }
    else 
    { 
      yices_constraint_gen(NULL,p_exptree_node,NULL,frame_num,true,t_or_f);
    }
  }
  else if(p_exptree_node_branch!=NULL)
  {
    //LEFT NODE
    #ifdef DEBUG_MODE
    fprintf(cfg_out,"\n Yices---Left Node \n");
    #endif
    
    if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t!=CNST))
    {
      if(p_exptree_node_branch->tree_node_t == ASS_NBLK)
        temp_expr1 = var_expr[frame_num+1][p_exptree_node_branch->left->var_table_idx];
      else
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->left->var_table_idx];
           
      temp_left = yices_mk_bv_extract(ctx, p_exptree_node_branch->left->width_start, p_exptree_node_branch->left->width_end, temp_expr1); 
    }
    else if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t==CNST)&&((p_exptree_node_branch->tree_node_t==CONCAT)||(p_exptree_node_branch->tree_node_t==CATCOPY)))
    {
      //!!!!!!!!!!!!!!!!!!!!!!!!!!
      int right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
      int left_oprand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
      temp_left =  yices_mk_bv_constant(ctx,left_oprand_width,atoi(p_exptree_node_branch->left->number_string));
    }
    else if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t==CNST))
    {
      int right_oprand_width;
      if(p_exptree_node_branch->right!=NULL)
        right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
      else
        right_oprand_width = 1000000;
      int left_oprand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
      if((p_exptree_node_branch->right!=NULL)&&(right_oprand_width<left_oprand_width))
        temp_left =  yices_mk_bv_constant(ctx,right_oprand_width,atoi(p_exptree_node_branch->left->number_string));
      else 
        temp_left =  yices_mk_bv_constant(ctx,left_oprand_width,atoi(p_exptree_node_branch->left->number_string));
    }
   
    //MIDDLE NODE
    if((p_exptree_node_branch->middle!=NULL)&&(p_exptree_node_branch->middle->tree_node_t!=CNST))
    {
      temp_expr2 = var_expr[frame_num][p_exptree_node_branch->middle->var_table_idx];
      temp_middle = yices_mk_bv_extract(ctx, p_exptree_node_branch->middle->width_start, p_exptree_node_branch->middle->width_end, temp_expr2); 
    }
    else if((p_exptree_node_branch->middle!=NULL)&&(p_exptree_node_branch->middle->tree_node_t==CNST))
    {
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      temp_middle = yices_mk_bv_constant(ctx,(p_exptree_node_branch->middle->width_start+1),atoi(p_exptree_node_branch->middle->number_string));
    }

    //RIGHT NODE 
    if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t!=CNST))
    {
      temp_expr3 = var_expr[frame_num][p_exptree_node_branch->right->var_table_idx];
      temp_right = yices_mk_bv_extract(ctx, p_exptree_node_branch->right->width_start, p_exptree_node_branch->right->width_end, temp_expr3); 
    }
    else if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t==CNST)&&(p_exptree_node_branch->tree_node_t==CONCAT))
    {
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      int right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
      int left_oprand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
      temp_right = yices_mk_bv_constant(ctx,right_oprand_width,atoi(p_exptree_node_branch->right->number_string));
    }
    else if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t==CNST))
    {
      int right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
      int left_oprand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
      temp_right = yices_mk_bv_constant(ctx,left_oprand_width,atoi(p_exptree_node_branch->right->number_string));
    }
    //yices_pp_expr (temp_left);
    #ifdef DEBUG_MODE
    fprintf(cfg_out,"\n Yices---End Left Right Node: type %d \n",p_exptree_node_branch->tree_node_t);
    #endif
    switch(p_exptree_node_branch->tree_node_t)
    {
      case GEQ : 
        temp_top = yices_mk_bv_ge(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@
        //@@@temp_expr1 = yices_mk_fresh_bool_var(ctx);
        //@@@var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
        //@@@
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //^^^temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //^^^var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr2 = yices_mk_bv_constant(ctx,1,0);// 
          temp_expr3 = yices_mk_bv_gt(ctx,temp_expr1,temp_expr2);//
          final_exp =  yices_mk_eq(ctx, temp_expr3, temp_top);
          //^^^final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        break;
      case LEQ :
        temp_top = yices_mk_bv_le(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@
        //@@@temp_expr1 = yices_mk_fresh_bool_var(ctx);
        //@@@var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
        //@@@
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //^^^temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //^^^var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr2 = yices_mk_bv_constant(ctx,1,0);// 
          temp_expr3 = yices_mk_bv_gt(ctx,temp_expr1,temp_expr2);//
          final_exp =  yices_mk_eq(ctx, temp_expr3, temp_top);
          //^^^final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        break;
      case LOGAND:      
        temp_top = yices_mk_bv_and(ctx, temp_left, temp_right);
        //@@@temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        break;
      case LOGOR:
        temp_top = yices_mk_bv_or(ctx, temp_left, temp_right);
        //@@@temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        break;
      case LOGEQ:
        temp_top = yices_mk_eq(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@@final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //^^^temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //^^^var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr2 = yices_mk_bv_constant(ctx,1,0);// 
          temp_expr3 = yices_mk_bv_gt(ctx,temp_expr1,temp_expr2);//
          final_exp =  yices_mk_eq(ctx, temp_expr3, temp_top);
          //^^^final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        break;
      case LOGINEQ:
        temp_top = yices_mk_diseq(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //^^^temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //^^^var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr2 = yices_mk_bv_constant(ctx,1,0);// 
          temp_expr3 = yices_mk_bv_gt(ctx,temp_expr1,temp_expr2);//
          final_exp =  yices_mk_eq(ctx, temp_expr3, temp_top);
          //^^^final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        break;
      case LSFT:
        //unsigned int shift_oprand;
        if((p_exptree_node_branch->right->tree_node_t!=CNST)) 
        {
          fprintf(cfg_out,"\nFatal error: the a<<b operation while the b is not constant\n");
          exit(-1);
        }
        else 
          shift_oprand = atoi(p_exptree_node_branch->right->number_string);    
        temp_top = yices_mk_bv_shift_left0(ctx, temp_left, shift_oprand);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);       
        break;
      case RSFT:
        if((p_exptree_node_branch->right->tree_node_t!=CNST)) 
        {
          fprintf(cfg_out,"\nFatal error: the a<<b operation while the b is not constant\n");
          exit(-1);
        }
        else 
          shift_oprand = atoi(p_exptree_node_branch->right->number_string);    
        temp_top = yices_mk_bv_shift_right0(ctx, temp_left, shift_oprand);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);       
        break;
      case ULOGNOT:
        temp_top = yices_mk_bv_not(ctx, temp_left);
        //@@@temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@final_exp = yices_mk_eq(ctx, temp_expr1, temp_top);  
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 

        break;
      case ULOGNEG :
        temp_top = yices_mk_bv_not(ctx, temp_left);
        //@@@temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@final_exp = yices_mk_eq(ctx, temp_expr1, temp_top);  
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 

        break;
      case ULOGAND  :  //&a
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case ULOGOR   :  //|a
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break; 
      case ULOGXOR  :  //^a
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break; 
      case ULOGNAND : //~&a
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case ULOGNOR  : //~|a
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case ULOGXNOR : //~^a
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case BADD : 
        temp_top = yices_mk_bv_add(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_eq(ctx, temp_expr1, temp_top);  
        break;
      case BSUB :
        temp_top = yices_mk_bv_sub(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_eq(ctx, temp_expr1, temp_top);  
        break;
      case BMUL :
        temp_top = yices_mk_bv_mul(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_eq(ctx, temp_expr1, temp_top);  
        break;
      case BDIV :
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case GAR :
        temp_top = yices_mk_bv_gt(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@
        //@@@temp_expr1 = yices_mk_fresh_bool_var(ctx);
        //@@@var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
        //@@@
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //^^^temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //^^^var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr2 = yices_mk_bv_constant(ctx,1,0);// 
          temp_expr3 = yices_mk_bv_gt(ctx,temp_expr1,temp_expr2);//
          final_exp =  yices_mk_eq(ctx, temp_expr3, temp_top);
          //^^^final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        //final_exp = temp_top;
        //final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        break;
      case LES :
        temp_top = yices_mk_bv_lt(ctx, temp_left, temp_right);
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //@@@
        //@@@temp_expr1 = yices_mk_fresh_bool_var(ctx);
        //@@@var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
        //@@@
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          //^^^temp_expr1 = yices_mk_fresh_bool_var(ctx);
          //^^^var_expr[frame_num][p_exptree_node_branch->var_table_idx] = temp_expr1;
          temp_expr2 = yices_mk_bv_constant(ctx,1,0);// 
          temp_expr3 = yices_mk_bv_gt(ctx,temp_expr1,temp_expr2);//
          final_exp =  yices_mk_eq(ctx, temp_expr3, temp_top);
          //^^^final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        //final_exp = temp_top;
        //@@@final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        break;
      case SAND :
        temp_top = yices_mk_bv_and(ctx, temp_left, temp_right);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        //temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        break;
      case SOR :
        temp_top = yices_mk_bv_or(ctx, temp_left, temp_right);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 

        //temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        break;
      case SXOR :
        temp_top = yices_mk_bv_xor(ctx, temp_left, temp_right);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        //temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        break;
      case LOGXNOR :
        temp_expr2 = yices_mk_bv_xor(ctx, temp_left, temp_right);
        temp_top = yices_mk_bv_not(ctx,temp_expr2);
        if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
          final_exp = temp_top;
        else
        {
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        } 
        //temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        //final_exp =  yices_mk_eq(ctx, temp_expr1, temp_top);
        break;
      case ASS_BLK :
        final_exp =  yices_mk_eq(ctx, temp_left, temp_right);
        break;
      case ASS_NBLK :
        final_exp =  yices_mk_eq(ctx, temp_left, temp_right);
        break;
      case NAME_PORT :
        final_exp =  yices_mk_eq(ctx, temp_left, temp_right);
        break;
      case POSEGE : //
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case NEGEGE : //
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case EDGE   : //
        fprintf(cfg_out,"\nFatal error: Unsupported operator\n");
        exit(-1);
        break;
      case ITE_E  : // a? b : c
        temp_expr4 = yices_mk_bv_constant(ctx, 1, 1);
        temp_top = yices_mk_eq(ctx, temp_expr4, temp_left); 

        temp_expr1 =  yices_mk_ite(ctx, temp_top, temp_middle,temp_right);
        temp_top =  var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_eq(ctx,temp_expr1,temp_top); 
        break;
      case PARSEL :
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_bv_extract(ctx, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end, temp_expr1);  
        break;
      case INDEX :
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_bv_extract(ctx, p_exptree_node_branch->width_start, p_exptree_node_branch->width_start, temp_expr1);  
        break;
      case VAR:
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_bv_extract(ctx, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end, temp_expr1);  
        break;
      case CONCAT:
        if(p_exptree_node_branch->right !=NULL)
          temp_top = yices_mk_bv_concat(ctx, temp_left, temp_right);
        else
          temp_top = temp_left;
        temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp = yices_mk_eq(ctx,temp_expr1,temp_top); 
        break;
      case CATCOPY:
        temp_copy = atoi(p_exptree_node_branch->left->number_string) - 1;
        int temp_idx;
        //temp_expr1 = yices_mk_bv_extract(ctx, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end, temp_right);
        temp_expr1 = yices_mk_bv_extract(ctx, p_exptree_node_branch->right->width_start, p_exptree_node_branch->right->width_end, temp_right);
        temp_top = temp_expr1;
        for(temp_idx=1;temp_idx<=temp_copy;temp_idx++)
          temp_top = yices_mk_bv_concat(ctx,temp_expr1,temp_top);
        temp_expr2 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        final_exp =  yices_mk_eq(ctx,temp_expr2,temp_top);
        break;
      default  :
        break;
    }


    //@@@@@@@@@@@@2

    if((p_exptree_node_branch->tree_node_t!=VAR)&&(p_exptree_node_branch->tree_node_t!=PARSEL)&&(p_exptree_node_branch->tree_node_t!=INDEX)&&(p_exptree_node_branch->tree_node_t!=CNST)&&(!p_exptree_node_branch->Is_top_node))
       //yices_assert(ctx, final_exp);
       cnst_exprs[constraint_number++]=final_exp;
    
    
    if(((p_exptree_node_branch->tree_node_t == LES)||(p_exptree_node_branch->tree_node_t == LOGEQ)||(p_exptree_node_branch->tree_node_t == LOGINEQ)||(p_exptree_node_branch->tree_node_t == GAR) || (p_exptree_node_branch->tree_node_t == GEQ) ||(p_exptree_node_branch->tree_node_t == LEQ))&&((p_exptree_node_branch->Is_top_node)&&(Is_if_branch)))
    {
      if(t_or_f == false)
        cnst_exprs[constraint_number++]=temp_top;
      else
      {
        final_exp = yices_mk_not(ctx,temp_top); 
        //yices_assert(ctx, final_exp);
        cnst_exprs[constraint_number++]=final_exp;
      } 
    }
    //else if (((p_exptree_node_branch->tree_node_t == LOGAND)||(p_exptree_node_branch->tree_node_t == LOGOR))&&((p_exptree_node_branch->Is_top_node)&&(Is_if_branch)))
    //{
    //  if(t_or_f == false)
    //  {
    //    yices_expr ctexpr = yices_mk_bv_constant(ctx, 1, 1);
    //    final_exp =  yices_mk_eq(temp_top,ctexpr);
    //    cnst_exprs[constraint_number++]=final_top;
    //    
    //  }
    //  else
    //  {
    //    final_exp = yices_mk_not(ctx,temp_top); 
    //    //yices_assert(ctx, final_exp);
    //    cnst_exprs[constraint_number++]=final_exp;
    //  } 
    //} 
    else if((p_exptree_node_branch->tree_node_t==VAR)&&(p_exptree_node_branch->tree_node_t==PARSEL)&&(p_exptree_node_branch->tree_node_t==INDEX)&&(p_exptree_node_branch->Is_top_node)&&Is_if_branch)
    {
       if(p_exptree_node_branch->width_start == p_exptree_node_branch->width_end)
       {
          yices_expr ctexpr = yices_mk_bv_constant(ctx, 1, 1);
          temp_top = yices_mk_eq(ctx, temp_expr2, ctexpr); 
       }
       else {printf("fatal error"); exit(-1);}
       if(t_or_f == false)
       { 
        //yices_assert(ctx, temp_top);
        cnst_exprs[constraint_number++]=temp_top;
        //yices_expr ctexpr = yices_mk_bv_constant(ctx, 1, 1);
           
       }
       else
       {
        final_exp = yices_mk_not(ctx,temp_top); 
        //yices_assert(ctx, final_exp);
        cnst_exprs[constraint_number++]=final_exp;
       } 

        

    } 
    else if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
    {
      //#ifdef DEBUG_MODE
      //printf("\n--a Top node--\n");
      //#endif
      if((p_exptree_node_branch->tree_node_t!=VAR)&&(p_exptree_node_branch->tree_node_t!=PARSEL)&&(p_exptree_node_branch->tree_node_t!=INDEX)&&(p_exptree_node_branch->tree_node_t!=CNST))
         temp_expr2 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
      else
         temp_expr2 = final_exp;
      
      temp_top = yices_mk_eq(ctx,temp_expr2,final_exp);
       cnst_exprs[constraint_number++]=temp_top;
   
      //printf("\nthe tree node type is %d,--constraint_number is %d, name is %s\n",p_exptree_node_branch->tree_node_t,constraint_number,p_exptree_node_branch->number_string);
      if(p_exptree_node_branch->width_start == p_exptree_node_branch->width_end)
      {
        #ifdef DEBUG_MODE
        printf("\nthe tree node type is %d,--constraint_number is %d, name is %s\n",p_exptree_node_branch->tree_node_t,constraint_number,p_exptree_node_branch->number_string);
        #endif
        yices_expr ctexpr = yices_mk_bv_constant(ctx, 1, 1);
        temp_top = yices_mk_eq(ctx, temp_expr2, ctexpr); 
        //temp_top = yices_mk_not(ctx,temp_top); 
      }
      else
      {
         printf("fatal error");
         exit(-1);
      }
        //temp_top = yices_mk_bv_extract(ctx, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end, temp_expr2); 
      if(t_or_f == false)
      { 
        //yices_assert(ctx, temp_top);
        cnst_exprs[constraint_number++]=temp_top;
        //yices_expr ctexpr = yices_mk_bv_constant(ctx, 1, 1);
           
      }
      else
      {
        final_exp = yices_mk_not(ctx,temp_top); 
        //yices_assert(ctx, final_exp);
        cnst_exprs[constraint_number++]=final_exp;
      } 
    }
    if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t!=VAR)&&(p_exptree_node_branch->left->tree_node_t!=PARSEL)&&(p_exptree_node_branch->left->tree_node_t!=INDEX)&&(p_exptree_node_branch->left->tree_node_t!=CNST))
      yices_constraint_gen(NULL,p_exptree_node_branch->left,NULL,frame_num,false,false);
    if((p_exptree_node_branch->middle!=NULL)&&(p_exptree_node_branch->middle->tree_node_t!=VAR)&&(p_exptree_node_branch->middle->tree_node_t!=PARSEL)&&(p_exptree_node_branch->middle->tree_node_t!=INDEX)&&(p_exptree_node_branch->middle->tree_node_t!=CNST))
      yices_constraint_gen(NULL,p_exptree_node_branch->middle,NULL,frame_num,false,false);
    if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t!=VAR)&&(p_exptree_node_branch->right->tree_node_t!=PARSEL)&&(p_exptree_node_branch->right->tree_node_t!=INDEX)&&(p_exptree_node_branch->right->tree_node_t!=CNST))
      yices_constraint_gen(NULL,p_exptree_node_branch->right,NULL,frame_num,false,false);
  } 
}

