////////////////////////////////////////////////////////////////////////////////
// bypass logic box
// Converted from revision 3283
////////////////////////////////////////////////////////////////////////////////
// simple logic to direct the source operand from RF or the bypass network
////////////////////////////////////////////////////////////////////////////////
module bypass_select(
  // operand sourced from two places
  address_in_rf,                 // Address of the desired data
  data_in_rf,                   // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  data_in_bpaddress_0,
  data_in_bpdata_0,
  data_in_bpvalid_0,
  data_in_bpaddress_1,
  data_in_bpdata_1,
  data_in_bpvalid_1,
  data_in_bpaddress_2,
  data_in_bpdata_2,
  data_in_bpvalid_2,
  data_in_bpaddress_3,
  data_in_bpdata_3,
  data_in_bpvalid_3,
  data_in_bpaddress_4,
  data_in_bpdata_4,
  data_in_bpvalid_4,
  data_in_bpaddress_5,
  data_in_bpdata_5,
  data_in_bpvalid_5,
  data_in_bpaddress_6,
  data_in_bpdata_6,
  data_in_bpvalid_6,
  data_in_bpaddress_7,
  data_in_bpdata_7,
  data_in_bpvalid_7,
  data_in_bpaddress_8,
  data_in_bpdata_8,
  data_in_bpvalid_8,
  data_in_bpaddress_9,
  data_in_bpdata_9,
  data_in_bpvalid_9,
  data_in_bpaddress_10,
  data_in_bpdata_10,
  data_in_bpvalid_10,
  // output the operand
  data_out,   // Desired data output
  src_is_bp         // Indicate that value was obtained from bypass
);

  // operand sourced from two places
  input [4:0] address_in_rf;                 // Address of the desired data
  input [31:0] data_in_rf;                  // Data from regfile
  //input bp_packet_t data_in_bp[10:0], // Data from the bypass network
  input [4:0] data_in_bpaddress_0;
  input [31:0] data_in_bpdata_0;
  input  data_in_bpvalid_0;
  input [4:0] data_in_bpaddress_1;
  input [31:0] data_in_bpdata_1;
  input  data_in_bpvalid_1;
  input [4:0] data_in_bpaddress_2;
  input [31:0] data_in_bpdata_2;
  input  data_in_bpvalid_2;
  input [4:0] data_in_bpaddress_3;
  input [31:0] data_in_bpdata_3;
  input  data_in_bpvalid_3;
  input [4:0] data_in_bpaddress_4;
  input [31:0] data_in_bpdata_4;
  input  data_in_bpvalid_4;
  input [4:0] data_in_bpaddress_5;
  input [31:0] data_in_bpdata_5;
  input  data_in_bpvalid_5;
  input [4:0] data_in_bpaddress_6;
  input [31:0] data_in_bpdata_6;
  input  data_in_bpvalid_6;
  input [4:0] data_in_bpaddress_7;
  input [31:0] data_in_bpdata_7;
  input  data_in_bpvalid_7;
  input [4:0] data_in_bpaddress_8;
  input [31:0] data_in_bpdata_8;
  input  data_in_bpvalid_8;
  input [4:0] data_in_bpaddress_9;
  input [31:0] data_in_bpdata_9;
  input  data_in_bpvalid_9;
  input [4:0] data_in_bpaddress_10;
  input [31:0] data_in_bpdata_10;
  input  data_in_bpvalid_10;
  // output the operand
  output [31:0] data_out;   // Desired data output
  output src_is_bp;         // Indicate that value was obtained from bypass

//  genvar i;
  always @ (
  address_in_rf or
  data_in_rf or
  data_in_bpaddress_0 or
  data_in_bpdata_0 or
  data_in_bpvalid_0 or
  data_in_bpaddress_1 or
  data_in_bpdata_1 or
  data_in_bpvalid_1 or
  data_in_bpaddress_2 or
  data_in_bpdata_2 or
  data_in_bpvalid_2 or
  data_in_bpaddress_3 or
  data_in_bpdata_3 or
  data_in_bpvalid_3 or
  data_in_bpaddress_4 or
  data_in_bpdata_4 or
  data_in_bpvalid_4 or
  data_in_bpaddress_5 or
  data_in_bpdata_5 or
  data_in_bpvalid_5 or
  data_in_bpaddress_6 or
  data_in_bpdata_6 or
  data_in_bpvalid_6 or
  data_in_bpaddress_7 or
  data_in_bpdata_7 or
  data_in_bpvalid_7 or
  data_in_bpaddress_8 or
  data_in_bpdata_8 or
  data_in_bpvalid_8 or
  data_in_bpaddress_9 or
  data_in_bpdata_9 or
  data_in_bpvalid_9 or
  data_in_bpaddress_10 or
  data_in_bpdata_10 or
  data_in_bpvalid_10
  ) begin
    // check each bypass source to see if we can obtain the data from there
    // Note: Never bypass register 0.
/*    if( data_in_bp[0].valid && (data_in_bp[0].address == address_in_rf) && 
        data_in_bp[0].address != 5'b0) begin
      data_out = data_in_bp[i].data; 
      src_is_bp = 1'b1;
    end
    generate
      for(i=0;i<BYPASS_SIZE;i++) begin
        // check each bypass source to see if we can obtain the data from there
        // Note: Never bypass register 0.
        else if( data_in_bp[i].valid && (data_in_bp[i].address == address_in_rf) && 
            data_in_bp[i].address != 5'b0) begin
          data_out = data_in_bp[i].data; 
          src_is_bp = 1'b1;
        end
      end
    endgenerate
    else begin
      data_out = data_in_rf;      // By default read the regfile
      src_is_bp = 1'b0;
    end*/
    // check each bypass source to determine where data is coming from
    if( data_in_bpvalid_0 && (data_in_bpaddress_0 == address_in_rf) && data_in_bpaddress_0 != 5'b0) begin
      data_out = data_in_bpdata_0; // Exec0 - INT
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_1 && (data_in_bpaddress_1 == address_in_rf) && data_in_bpaddress_1 != 5'b0) begin
      data_out = data_in_bpdata_1; // Exec0 - MEM
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_2 && (data_in_bpaddress_2 == address_in_rf) && data_in_bpaddress_2 != 5'b0) begin
      data_out = data_in_bpdata_2; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_3 && (data_in_bpaddress_3 == address_in_rf) && data_in_bpaddress_3 != 5'b0) begin
      data_out = data_in_bpdata_3; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_4 && (data_in_bpaddress_4 == address_in_rf) && data_in_bpaddress_4 != 5'b0) begin
      data_out = data_in_bpdata_4; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_5 && (data_in_bpaddress_5 == address_in_rf) && data_in_bpaddress_5 != 5'b0) begin
      data_out = data_in_bpdata_5; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_6 && (data_in_bpaddress_6 == address_in_rf) && data_in_bpaddress_6 != 5'b0) begin
      data_out = data_in_bpdata_6; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_7 && (data_in_bpaddress_7 == address_in_rf) && data_in_bpaddress_7 != 5'b0) begin
      data_out = data_in_bpdata_7; // WB - slot1
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_8 && (data_in_bpaddress_8 == address_in_rf) && data_in_bpaddress_8 != 5'b0) begin
      data_out = data_in_bpdata_8; // Exec0 - FPU
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_9 && (data_in_bpaddress_9 == address_in_rf) && data_in_bpaddress_9 != 5'b0) begin
      data_out = data_in_bpdata_9; // WB - slot0
      src_is_bp = 1'b1;
    end else if( data_in_bpvalid_10 && (data_in_bpaddress_10 == address_in_rf) && data_in_bpaddress_10 != 5'b0) begin
      data_out = data_in_bpdata_10; // WB - slot1
      src_is_bp = 1'b1;
   end else begin // data does not exist on bypass network, so get it from the rf
      data_out = data_in_rf;
      src_is_bp = 1'b0;
    end
  end
endmodule
