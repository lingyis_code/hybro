`timescale 1ns/10ps

module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(string instance_name, int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
//import "DPI-C" function int  read_next_pattern(int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int pat_num);
//import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg reset;

reg [7:0] decode_stage_branvar_last[0:1000];
reg [7:0] decode0_branvar_last[0:1000];
reg [7:0] decode1_branvar_last[0:1000];
reg [7:0] scoreboard_branvar_last[0:1000];
reg [7:0] rf_4r2w_branvar_last[0:1000];
reg [7:0] sprf_branvar_last[0:1000];
reg [7:0] bypass_select0_branvar_last[0:1000];
reg [7:0] bypass_select1_branvar_last[0:1000];
reg [7:0] bypass_select2_branvar_last[0:1000];
reg [7:0] bypass_select3_branvar_last[0:1000];


integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;

reg  stall_in_0;
reg  stall_in_1;
reg  stall_in_2;
reg  branch_mispredict;
reg [31:0] fetch2decodeinstr0_out;
reg [31:0] fetch2decodeinstr1_out;
reg [31:0] fetch2decodepc1;
reg [31:0] fetch2decodepc0;
reg [31:0] fetch2decodepc1_incr;
reg [31:0] fetch2decodepc0_incr;
reg  fetch2decodeinstr0_valid;
reg  fetch2decodeinstr1_valid;
reg  fetch2decodebranch_taken;
reg [31:0] fetch2decodepredicted_pc;
reg  fetch2decodevalid;
reg [4:0] data_in_bpaddress_0;
reg [31:0] data_in_bpdata_0;
reg  data_in_bpvalid_0;
reg [4:0] data_in_bpaddress_1;
reg [31:0] data_in_bpdata_1;
reg  data_in_bpvalid_1;
reg [4:0] data_in_bpaddress_2;
reg [31:0] data_in_bpdata_2;
reg  data_in_bpvalid_2;
reg [4:0] data_in_bpaddress_3;
reg [31:0] data_in_bpdata_3;
reg  data_in_bpvalid_3;
reg [4:0] data_in_bpaddress_4;
reg [31:0] data_in_bpdata_4;
reg  data_in_bpvalid_4;
reg [4:0] data_in_bpaddress_5;
reg [31:0] data_in_bpdata_5;
reg  data_in_bpvalid_5;
reg [4:0] data_in_bpaddress_6;
reg [31:0] data_in_bpdata_6;
reg  data_in_bpvalid_6;
reg [4:0] data_in_bpaddress_7;
reg [31:0] data_in_bpdata_7;
reg  data_in_bpvalid_7;
reg [4:0] data_in_bpaddress_8;
reg [31:0] data_in_bpdata_8;
reg  data_in_bpvalid_8;
reg [4:0] data_in_bpaddress_9;
reg [31:0] data_in_bpdata_9;
reg  data_in_bpvalid_9;
reg [4:0] data_in_bpaddress_10;
reg [31:0] data_in_bpdata_10;
reg  data_in_bpvalid_10;
reg  write_enable_0;
reg [4:0] write_addr_0;
reg [31:0] write_data_0;
reg  write_enable_1;
reg [4:0] write_addr_1;
reg [31:0] write_data_1;
reg [31:0] decode2mempc;
reg [31:0] decode2mempc_incr;
reg [1:0] decode2memthread_num;
reg  decode2membranch_taken;
reg [31:0] decode2mempredicted_pc;
reg  decode2memvalid;
reg  decode2memis_oldest;
reg [31:0] decode2memsreg_t_data;
reg [31:0] decode2memsreg_s_data;
reg [31:0] decode2memresult;
reg  decode2memL1D_hit;
reg [4:0] decode2memdecode_packetsreg_t;
reg [4:0] decode2memdecode_packetsreg_s;
reg [4:0] decode2memdecode_packetdreg;
reg [1:0] decode2memdecode_packetacc;
reg [15:0] decode2memdecode_packetimm16_value;
reg [25:0] decode2memdecode_packetimm26_value;
reg [4:0] decode2memdecode_packetimm5_value;
reg [3:0] decode2memdecode_packetop_FU_type;
reg [4:0] decode2memdecode_packetalu_op;
reg [4:0] decode2memdecode_packetfpu_op;
reg [3:0] decode2memdecode_packetop_MEM_type;
reg [1:0] decode2memdecode_packetop_BR_type;
reg [1:0] decode2memdecode_packetshift_type;
reg [2:0] decode2memdecode_packetcompare_type;
reg  decode2memdecode_packetis_imm26;
reg  decode2memdecode_packetis_imm;
reg  decode2memdecode_packetis_imm5;
reg  decode2memdecode_packetis_signed;
reg  decode2memdecode_packetcarry_in;
reg  decode2memdecode_packetcarry_out;
reg  decode2memdecode_packethas_sreg_t;
reg  decode2memdecode_packethas_sreg_s;
reg  decode2memdecode_packethas_dreg;
reg  decode2memdecode_packetis_prefetch;
reg  decode2memdecode_packetsprf_dest;
reg  decode2memdecode_packetsprf_src;
reg  decode2memdecode_packetis_atomic;
reg  decode2memdecode_packetis_ldl;
reg  decode2memdecode_packetis_stl;
reg  decode2memdecode_packetis_stc;
reg  decode2memdecode_packetis_valid;
reg  decode2memdecode_packetis_halt;
reg  decode2memdecode_packetis_compare;
reg  decode2memdecode_packetdreg_is_src;
reg  decode2memdecode_packetdreg_is_dest;
reg [31:0] decode2fpupc;
reg [31:0] decode2fpupc_incr;
reg [1:0] decode2fputhread_num;
reg  decode2fpubranch_taken;
reg [31:0] decode2fpupredicted_pc;
reg  decode2fpuvalid;
reg  decode2fpuis_oldest;
reg [31:0] decode2fpusreg_t_data;
reg [31:0] decode2fpusreg_s_data;
reg [31:0] decode2fpuresult;
reg  decode2fpuL1D_hit;
reg [4:0] decode2fpudecode_packetsreg_t;
reg [4:0] decode2fpudecode_packetsreg_s;
reg [4:0] decode2fpudecode_packetdreg;
reg [1:0] decode2fpudecode_packetacc;
reg [15:0] decode2fpudecode_packetimm16_value;
reg [25:0] decode2fpudecode_packetimm26_value;
reg [4:0] decode2fpudecode_packetimm5_value;
reg [3:0] decode2fpudecode_packetop_FU_type;
reg [4:0] decode2fpudecode_packetalu_op;
reg [4:0] decode2fpudecode_packetfpu_op;
reg [3:0] decode2fpudecode_packetop_MEM_type;
reg [1:0] decode2fpudecode_packetop_BR_type;
reg [1:0] decode2fpudecode_packetshift_type;
reg [2:0] decode2fpudecode_packetcompare_type;
reg  decode2fpudecode_packetis_imm26;
reg  decode2fpudecode_packetis_imm;
reg  decode2fpudecode_packetis_imm5;
reg  decode2fpudecode_packetis_signed;
reg  decode2fpudecode_packetcarry_in;
reg  decode2fpudecode_packetcarry_out;
reg  decode2fpudecode_packethas_sreg_t;
reg  decode2fpudecode_packethas_sreg_s;
reg  decode2fpudecode_packethas_dreg;
reg  decode2fpudecode_packetis_prefetch;
reg  decode2fpudecode_packetsprf_dest;
reg  decode2fpudecode_packetsprf_src;
reg  decode2fpudecode_packetis_atomic;
reg  decode2fpudecode_packetis_ldl;
reg  decode2fpudecode_packetis_stl;
reg  decode2fpudecode_packetis_stc;
reg  decode2fpudecode_packetis_valid;
reg  decode2fpudecode_packetis_halt;
reg  decode2fpudecode_packetis_compare;
reg  decode2fpudecode_packetdreg_is_src;
reg  decode2fpudecode_packetdreg_is_dest;
reg [31:0] decode2intpc;
reg [31:0] decode2intpc_incr;
reg [1:0] decode2intthread_num;
reg  decode2intbranch_taken;
reg [31:0] decode2intpredicted_pc;
reg  decode2intvalid;
reg  decode2intis_oldest;
reg [31:0] decode2intsreg_t_data;
reg [31:0] decode2intsreg_s_data;
reg [31:0] decode2intresult;
reg  decode2intL1D_hit;
reg [4:0] decode2intdecode_packetsreg_t;
reg [4:0] decode2intdecode_packetsreg_s;
reg [4:0] decode2intdecode_packetdreg;
reg [1:0] decode2intdecode_packetacc;
reg [15:0] decode2intdecode_packetimm16_value;
reg [25:0] decode2intdecode_packetimm26_value;
reg [4:0] decode2intdecode_packetimm5_value;
reg [3:0] decode2intdecode_packetop_FU_type;
reg [4:0] decode2intdecode_packetalu_op;
reg [4:0] decode2intdecode_packetfpu_op;
reg [3:0] decode2intdecode_packetop_MEM_type;
reg [1:0] decode2intdecode_packetop_BR_type;
reg [1:0] decode2intdecode_packetshift_type;
reg [2:0] decode2intdecode_packetcompare_type;
reg  decode2intdecode_packetis_imm26;
reg  decode2intdecode_packetis_imm;
reg  decode2intdecode_packetis_imm5;
reg  decode2intdecode_packetis_signed;
reg  decode2intdecode_packetcarry_in;
reg  decode2intdecode_packetcarry_out;
reg  decode2intdecode_packethas_sreg_t;
reg  decode2intdecode_packethas_sreg_s;
reg  decode2intdecode_packethas_dreg;
reg  decode2intdecode_packetis_prefetch;
reg  decode2intdecode_packetsprf_dest;
reg  decode2intdecode_packetsprf_src;
reg  decode2intdecode_packetis_atomic;
reg  decode2intdecode_packetis_ldl;
reg  decode2intdecode_packetis_stl;
reg  decode2intdecode_packetis_stc;
reg  decode2intdecode_packetis_valid;
reg  decode2intdecode_packetis_halt;
reg  decode2intdecode_packetis_compare;
reg  decode2intdecode_packetdreg_is_src;
reg  decode2intdecode_packetdreg_is_dest;
reg  decode_stall;

integer rand_f;
integer rand_cnt;

decode_stage inst(
   .clk(clock),
   .reset(reset),
   .stall_in_0(stall_in_0),
   .stall_in_1(stall_in_1),
   .stall_in_2(stall_in_2),
   .branch_mispredict(branch_mispredict),
   .fetch2decodeinstr0_out(fetch2decodeinstr0_out[31:0]),
   .fetch2decodeinstr1_out(fetch2decodeinstr1_out[31:0]),
   .fetch2decodepc1(fetch2decodepc1[31:0]),
   .fetch2decodepc0(fetch2decodepc0[31:0]),
   .fetch2decodepc1_incr(fetch2decodepc1_incr[31:0]),
   .fetch2decodepc0_incr(fetch2decodepc0_incr[31:0]),
   .fetch2decodeinstr0_valid(fetch2decodeinstr0_valid),
   .fetch2decodeinstr1_valid(fetch2decodeinstr1_valid),
   .fetch2decodebranch_taken(fetch2decodebranch_taken),
   .fetch2decodepredicted_pc(fetch2decodepredicted_pc[31:0]),
   .fetch2decodevalid(fetch2decodevalid),
   .data_in_bpaddress_0(data_in_bpaddress_0[4:0]),
   .data_in_bpdata_0(data_in_bpdata_0[31:0]),
   .data_in_bpvalid_0(data_in_bpvalid_0),
   .data_in_bpaddress_1(data_in_bpaddress_1[4:0]),
   .data_in_bpdata_1(data_in_bpdata_1[31:0]),
   .data_in_bpvalid_1(data_in_bpvalid_1),
   .data_in_bpaddress_2(data_in_bpaddress_2[4:0]),
   .data_in_bpdata_2(data_in_bpdata_2[31:0]),
   .data_in_bpvalid_2(data_in_bpvalid_2),
   .data_in_bpaddress_3(data_in_bpaddress_3[4:0]),
   .data_in_bpdata_3(data_in_bpdata_3[31:0]),
   .data_in_bpvalid_3(data_in_bpvalid_3),
   .data_in_bpaddress_4(data_in_bpaddress_4[4:0]),
   .data_in_bpdata_4(data_in_bpdata_4[31:0]),
   .data_in_bpvalid_4(data_in_bpvalid_4),
   .data_in_bpaddress_5(data_in_bpaddress_5[4:0]),
   .data_in_bpdata_5(data_in_bpdata_5[31:0]),
   .data_in_bpvalid_5(data_in_bpvalid_5),
   .data_in_bpaddress_6(data_in_bpaddress_6[4:0]),
   .data_in_bpdata_6(data_in_bpdata_6[31:0]),
   .data_in_bpvalid_6(data_in_bpvalid_6),
   .data_in_bpaddress_7(data_in_bpaddress_7[4:0]),
   .data_in_bpdata_7(data_in_bpdata_7[31:0]),
   .data_in_bpvalid_7(data_in_bpvalid_7),
   .data_in_bpaddress_8(data_in_bpaddress_8[4:0]),
   .data_in_bpdata_8(data_in_bpdata_8[31:0]),
   .data_in_bpvalid_8(data_in_bpvalid_8),
   .data_in_bpaddress_9(data_in_bpaddress_9[4:0]),
   .data_in_bpdata_9(data_in_bpdata_9[31:0]),
   .data_in_bpvalid_9(data_in_bpvalid_9),
   .data_in_bpaddress_10(data_in_bpaddress_10[4:0]),
   .data_in_bpdata_10(data_in_bpdata_10[31:0]),
   .data_in_bpvalid_10(data_in_bpvalid_10),
   .write_enable_0(write_enable_0),
   .write_addr_0(write_addr_0[4:0]),
   .write_data_0(write_data_0[31:0]),
   .write_enable_1(write_enable_1),
   .write_addr_1(write_addr_1[4:0]),
   .write_data_1(write_data_1[31:0]),
   .decode2mempc(decode2mempc[31:0]),
   .decode2mempc_incr(decode2mempc_incr[31:0]),
   .decode2memthread_num(decode2memthread_num[1:0]),
   .decode2membranch_taken(decode2membranch_taken),
   .decode2mempredicted_pc(decode2mempredicted_pc[31:0]),
   .decode2memvalid(decode2memvalid),
   .decode2memis_oldest(decode2memis_oldest),
   .decode2memsreg_t_data(decode2memsreg_t_data[31:0]),
   .decode2memsreg_s_data(decode2memsreg_s_data[31:0]),
   .decode2memresult(decode2memresult[31:0]),
   .decode2memL1D_hit(decode2memL1D_hit),
   .decode2memdecode_packetsreg_t(decode2memdecode_packetsreg_t[4:0]),
   .decode2memdecode_packetsreg_s(decode2memdecode_packetsreg_s[4:0]),
   .decode2memdecode_packetdreg(decode2memdecode_packetdreg[4:0]),
   .decode2memdecode_packetacc(decode2memdecode_packetacc[1:0]),
   .decode2memdecode_packetimm16_value(decode2memdecode_packetimm16_value[15:0]), 
   .decode2memdecode_packetimm26_value(decode2memdecode_packetimm26_value[25:0]),
   .decode2memdecode_packetimm5_value(decode2memdecode_packetimm5_value[4:0]),
   .decode2memdecode_packetop_FU_type(decode2memdecode_packetop_FU_type[3:0]),
   .decode2memdecode_packetalu_op(decode2memdecode_packetalu_op[4:0]),
   .decode2memdecode_packetfpu_op(decode2memdecode_packetfpu_op[4:0]),
   .decode2memdecode_packetop_MEM_type(decode2memdecode_packetop_MEM_type[3:0]),
   .decode2memdecode_packetop_BR_type(decode2memdecode_packetop_BR_type[1:0]),
   .decode2memdecode_packetshift_type(decode2memdecode_packetshift_type[1:0]),
   .decode2memdecode_packetcompare_type(decode2memdecode_packetcompare_type[2:0]),
   .decode2memdecode_packetis_imm26(decode2memdecode_packetis_imm26),
   .decode2memdecode_packetis_imm(decode2memdecode_packetis_imm),
   .decode2memdecode_packetis_imm5(decode2memdecode_packetis_imm5),
   .decode2memdecode_packetis_signed(decode2memdecode_packetis_signed),
   .decode2memdecode_packetcarry_in(decode2memdecode_packetcarry_in),
   .decode2memdecode_packetcarry_out(decode2memdecode_packetcarry_out),
   .decode2memdecode_packethas_sreg_t(decode2memdecode_packethas_sreg_t),
   .decode2memdecode_packethas_sreg_s(decode2memdecode_packethas_sreg_s),
   .decode2memdecode_packethas_dreg(decode2memdecode_packethas_dreg),
   .decode2memdecode_packetis_prefetch(decode2memdecode_packetis_prefetch),
   .decode2memdecode_packetsprf_dest(decode2memdecode_packetsprf_dest),
   .decode2memdecode_packetsprf_src(decode2memdecode_packetsprf_src),
   .decode2memdecode_packetis_atomic(decode2memdecode_packetis_atomic),
   .decode2memdecode_packetis_ldl(decode2memdecode_packetis_ldl),
   .decode2memdecode_packetis_stl(decode2memdecode_packetis_stl),
   .decode2memdecode_packetis_stc(decode2memdecode_packetis_stc),
   .decode2memdecode_packetis_valid(decode2memdecode_packetis_valid),
   .decode2memdecode_packetis_halt(decode2memdecode_packetis_halt),
   .decode2memdecode_packetis_compare(decode2memdecode_packetis_compare),
   .decode2memdecode_packetdreg_is_src(decode2memdecode_packetdreg_is_src),
   .decode2memdecode_packetdreg_is_dest(decode2memdecode_packetdreg_is_dest),
   .decode2fpupc(decode2fpupc[31:0]),
   .decode2fpupc_incr(decode2fpupc_incr[31:0]),
   .decode2fputhread_num(decode2fputhread_num[1:0]),
   .decode2fpubranch_taken(decode2fpubranch_taken),
   .decode2fpupredicted_pc(decode2fpupredicted_pc[31:0]),
   .decode2fpuvalid(decode2fpuvalid),
   .decode2fpuis_oldest(decode2fpuis_oldest),
   .decode2fpusreg_t_data(decode2fpusreg_t_data[31:0]),
   .decode2fpusreg_s_data(decode2fpusreg_s_data[31:0]),
   .decode2fpuresult(decode2fpuresult[31:0]),
   .decode2fpuL1D_hit(decode2fpuL1D_hit),
   .decode2fpudecode_packetsreg_t(decode2fpudecode_packetsreg_t[4:0]),
   .decode2fpudecode_packetsreg_s(decode2fpudecode_packetsreg_s[4:0]),
   .decode2fpudecode_packetdreg(decode2fpudecode_packetdreg[4:0]),
   .decode2fpudecode_packetacc(decode2fpudecode_packetacc[1:0]),
   .decode2fpudecode_packetimm16_value(decode2fpudecode_packetimm16_value[15:0]),
   .decode2fpudecode_packetimm26_value(decode2fpudecode_packetimm26_value[25:0]),
   .decode2fpudecode_packetimm5_value(decode2fpudecode_packetimm5_value[4:0]),
   .decode2fpudecode_packetop_FU_type(decode2fpudecode_packetop_FU_type[3:0]),
   .decode2fpudecode_packetalu_op(decode2fpudecode_packetalu_op[4:0]),
   .decode2fpudecode_packetfpu_op(decode2fpudecode_packetfpu_op[4:0]),
   .decode2fpudecode_packetop_MEM_type(decode2fpudecode_packetop_MEM_type[3:0]),
   .decode2fpudecode_packetop_BR_type(decode2fpudecode_packetop_BR_type[1:0]),
   .decode2fpudecode_packetshift_type(decode2fpudecode_packetshift_type[1:0]),
   .decode2fpudecode_packetcompare_type(decode2fpudecode_packetcompare_type[2:0]),
   .decode2fpudecode_packetis_imm26(decode2fpudecode_packetis_imm26),
   .decode2fpudecode_packetis_imm(decode2fpudecode_packetis_im),
   .decode2fpudecode_packetis_imm5(decode2fpudecode_packetis_imm5),
   .decode2fpudecode_packetis_signed(decode2fpudecode_packetis_signed),
   .decode2fpudecode_packetcarry_in(decode2fpudecode_packetcarry_in),
   .decode2fpudecode_packetcarry_out(decode2fpudecode_packetcarry_out),
   .decode2fpudecode_packethas_sreg_t(decode2fpudecode_packethas_sreg_t),
   .decode2fpudecode_packethas_sreg_s(decode2fpudecode_packethas_sreg_s),
   .decode2fpudecode_packethas_dreg(decode2fpudecode_packethas_dreg),
   .decode2fpudecode_packetis_prefetch(decode2fpudecode_packetis_prefetch),
   .decode2fpudecode_packetsprf_dest(decode2fpudecode_packetsprf_dest),
   .decode2fpudecode_packetsprf_src(decode2fpudecode_packetsprf_src),
   .decode2fpudecode_packetis_atomic(decode2fpudecode_packetis_atomic),
   .decode2fpudecode_packetis_ldl(decode2fpudecode_packetis_ldl),
   .decode2fpudecode_packetis_stl(decode2fpudecode_packetis_stl),
   .decode2fpudecode_packetis_stc(decode2fpudecode_packetis_stc),
   .decode2fpudecode_packetis_valid(decode2fpudecode_packetis_valid),
   .decode2fpudecode_packetis_halt(decode2fpudecode_packetis_halt),
   .decode2fpudecode_packetis_compare(decode2fpudecode_packetis_compare),
   .decode2fpudecode_packetdreg_is_src(decode2fpudecode_packetdreg_is_src),
   .decode2fpudecode_packetdreg_is_dest(decode2fpudecode_packetdreg_is_dest),
   .decode2intpc(decode2intpc[31:0]),
   .decode2intpc_incr(decode2intpc_incr[31:0]),
   .decode2intthread_num(decode2intthread_num[1:0]),
   .decode2intbranch_taken(decode2intbranch_taken),
   .decode2intpredicted_pc(decode2intpredicted_pc[31:0]),
   .decode2intvalid(decode2intvalid),
   .decode2intis_oldest(decode2intis_oldest),
   .decode2intsreg_t_data(decode2intsreg_t_data[31:0]),
   .decode2intsreg_s_data(decode2intsreg_s_data[31:0]),
   .decode2intresult(decode2intresult[31:0]),
   .decode2intL1D_hit(decode2intL1D_hit),
   .decode2intdecode_packetsreg_t(decode2intdecode_packetsreg_t[4:0]),
   .decode2intdecode_packetsreg_s(decode2intdecode_packetsreg_s[4:0]),
   .decode2intdecode_packetdreg(decode2intdecode_packetdreg[4:0]),
   .decode2intdecode_packetacc(decode2intdecode_packetacc[1:0]),
   .decode2intdecode_packetimm16_value(decode2intdecode_packetimm16_value[15:0]),
   .decode2intdecode_packetimm26_value(decode2intdecode_packetimm26_value[25:0]),
   .decode2intdecode_packetimm5_value(decode2intdecode_packetimm5_value[4:0]),
   .decode2intdecode_packetop_FU_type(decode2intdecode_packetop_FU_type[3:0]),
   .decode2intdecode_packetalu_op(decode2intdecode_packetalu_op[4:0]),
   .decode2intdecode_packetfpu_op(decode2intdecode_packetfpu_op[4:0]),
   .decode2intdecode_packetop_MEM_type(decode2intdecode_packetop_MEM_type[3:0]),
   .decode2intdecode_packetop_BR_type(decode2intdecode_packetop_BR_type[1:0]),
   .decode2intdecode_packetshift_type(decode2intdecode_packetshift_type[1:0]),
   .decode2intdecode_packetcompare_type(decode2intdecode_packetcompare_type[2:0]),
   .decode2intdecode_packetis_imm26(decode2intdecode_packetis_imm26),
   .decode2intdecode_packetis_imm(decode2intdecode_packetis_imm),
   .decode2intdecode_packetis_imm5(decode2intdecode_packetis_imm5),
   .decode2intdecode_packetis_signed(decode2intdecode_packetis_signed),
   .decode2intdecode_packetcarry_in(decode2intdecode_packetcarry_in),
   .decode2intdecode_packetcarry_out(decode2intdecode_packetcarry_out),
   .decode2intdecode_packethas_sreg_t(decode2intdecode_packethas_sreg_t),
   .decode2intdecode_packethas_sreg_s(decode2intdecode_packethas_sreg_s),
   .decode2intdecode_packethas_dreg(decode2intdecode_packethas_dreg),
   .decode2intdecode_packetis_prefetch(decode2intdecode_packetis_prefetch),
   .decode2intdecode_packetsprf_dest(decode2intdecode_packetsprf_dest),
   .decode2intdecode_packetsprf_src(decode2intdecode_packetsprf_src),
   .decode2intdecode_packetis_atomic(decode2intdecode_packetis_atomic),
   .decode2intdecode_packetis_ldl(decode2intdecode_packetis_ldl),
   .decode2intdecode_packetis_stl(decode2intdecode_packetis_stl),
   .decode2intdecode_packetis_stc(decode2intdecode_packetis_stc),
   .decode2intdecode_packetis_valid(decode2intdecode_packetis_valid),
   .decode2intdecode_packetis_halt(decode2intdecode_packetis_halt),
   .decode2intdecode_packetis_compare(decode2intdecode_packetis_compare),
   .decode2intdecode_packetdreg_is_src(decode2intdecode_packetdreg_is_src),
   .decode2intdecode_packetdreg_is_dest(decode2intdecode_packetdreg_is_dest),
   .decode_stall(decode_stall)
);

initial begin
#1
  rand_f = 0;
  rand_cnt = 0;
  $display("\nStart to build CFG for RTL Design\n");
  start_build_cfg("source_rtl/decode_stage/decode_stage_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num = 0;
end

//initial begin
// $dumpfile("vectorfetch.vcd");
// $dumpvars(1,inst);
//end


always 
begin
  #5 clock = ~clock;
end

always
begin
 #6;
 for(j=0;j<=1000;j=j+1)
 begin
   $display("decode_stage:branvar[%d] value is %d",j,inst.branvar[j]);
   $display("DECODER0: branvar[%d] value is %d",j,inst.DECODER0.branvar[j]);
 end
 #4;
end

always 
begin
  #3;
  begin
    for(j=0;j<=1000;j=j+1)
    begin
      //each module
      //branvar_last[j]=inst.branvar[j];
      decode_stage_branvar_last[j] = inst.branvar[j];
      decode0_branvar_last[j] = inst.DECODER0.branvar[j];
      decode1_branvar_last[j] = inst.DECODER1.branvar[j];
      scoreboard_branvar_last[j] = inst.SB.branvar[j];
      rf_4r2w_branvar_last[j] = inst.RF.branvar[j];
      sprf_branvar_last[j] = inst.SPRF.branvar[j];
      bypass_select0_branvar_last[j] = inst.BP_SELECT_S_0.branvar[j];
      bypass_select1_branvar_last[j] = inst.BP_SELECT_S_1.branvar[j];
      bypass_select2_branvar_last[j] = inst.BP_SELECT_T_0.branvar[j];
      bypass_select3_branvar_last[j] = inst.BP_SELECT_T_1.branvar[j];
    end
  end
  #7; 
end

always
begin
  #6;
    begin
      for(k=0;k<=1000;k=k+1)
       begin
         if(decode_stage_branvar_last[k]!=inst.branvar[k])
         begin 
           record_branch("decode_stage",k,frame_num); 
         end
         if(decode0_branvar_last[k]!=inst.DECODER0.branvar[k])
         begin 
           record_branch("decode0",k,frame_num); 
         end
         if(decode1_branvar_last[k]!=inst.DECODER1.branvar[k])
         begin 
           record_branch("decode1",k,frame_num); 
         end
         if(scoreboard_branvar_last[k]!=inst.SB.branvar[k])
         begin
           record_branch("scoreboard",k,frame_num); 
         end  
         if(rf_4r2w_branvar_last[k]!=inst.RF.branvar[k])
         begin
           record_branch("rf_4r2w",k,frame_num); 
         end
         if(sprf_branvar_last[k]!=inst.SPRF.branvar[k])
         begin
           record_branch("sprf",k,frame_num); 
         end
         if(bypass_select0_branvar_last[j]!=inst.BP_SELECT_S_0.branvar[k])
         begin
           record_branch("bypass_select0",k,frame_num); 
         end
         if(bypass_select1_branvar_last[j]!=inst.BP_SELECT_S_0.branvar[k])
         begin
           record_branch("bypass_select1",k,frame_num); 
         end
         if(bypass_select2_branvar_last[j]!=inst.BP_SELECT_T_0.branvar[k])
         begin
           record_branch("bypass_select2",k,frame_num); 
         end
         if(bypass_select3_branvar_last[j]!=inst.BP_SELECT_T_1.branvar[k])
         begin
           record_branch("bypass_select3",k,frame_num); 
         end
       end
    end
  #4;
end

 
always
begin
  #4;
  //if(frame_num == -1)
  //  begin
  //    inst.btb_valid  = 1'b0;
  //    inst.btb_pc     = 32'h0;
  //    inst.btb_target = 32'h0;
  //    inst.pc = 32'hFFFFFFF8;
  //    inst.icache_read_req_vld_o = 1'b0;
  //    inst.fetch2decodevalid        = 1'b0;
  //    inst.fetch2decodeinstr0_valid = 1'b0;
  //    inst.fetch2decodeinstr1_valid = 1'b0;
  //  end

  if(frame_num<0)
     reset = 1;
  else 
     reset = 0;

  var_value = get_var_assignment("stall_in_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
     stall_in_0 = $random;
  else 
     stall_in_0 = var_value;

  var_value = get_var_assignment("stall_in_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
     stall_in_1 = $random;
  else 
     stall_in_1 = var_value;

  var_value = get_var_assignment("stall_in_2",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
     stall_in_2 = $random;
  else 
     stall_in_2 = var_value;

  var_value = get_var_assignment("branch_mispredict",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
     branch_mispredict = $random;
  else 
     branch_mispredict = var_value;
  
  var_value = get_var_assignment("fetch2decodeinstr0_out",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
     fetch2decodeinstr0_out = $random;
  else 
     fetch2decodeinstr0_out = var_value;

  var_value = get_var_assignment("fetch2decodeinstr1_out",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
     fetch2decodeinstr1_out = $random;
  else 
     fetch2decodeinstr1_out = var_value;

  var_value = get_var_assignment("fetch2decodepc1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodepc1  = $random;
  else 
    fetch2decodepc1  = var_value;

  var_value = get_var_assignment("fetch2decodepc0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodepc0  = $random;
  else 
    fetch2decodepc0  = var_value;

  var_value = get_var_assignment("fetch2decodepc1_incr",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodepc1_incr  = $random;
  else 
    fetch2decodepc1_incr  = var_value;

  var_value = get_var_assignment("fetch2decodepc0_incr",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodepc0_incr  = $random;
  else 
    fetch2decodepc0_incr  = var_value;

  var_value = get_var_assignment("fetch2decodeinstr0_valid",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodeinstr0_valid  = $random;
  else 
    fetch2decodeinstr0_valid  = var_value;

  var_value = get_var_assignment("fetch2decodeinstr1_valid",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodeinstr1_valid  = $random;
  else 
    fetch2decodeinstr1_valid  = var_value;

  var_value = get_var_assignment("fetch2decodebranch_taken",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodebranch_taken  = $random;
  else 
    fetch2decodebranch_taken  = var_value;

  var_value = get_var_assignment("fetch2decodepredicted_pc",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodepredicted_pc  = $random;
  else 
    fetch2decodepredicted_pc  = var_value;

  var_value = get_var_assignment("fetch2decodevalid",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    fetch2decodevalid  = $random;
  else 
    fetch2decodevalid  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_0  = $random;
  else 
    data_in_bpaddress_0  = var_value;

  var_value = get_var_assignment("data_in_bpdata_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_0  = $random;
  else 
    data_in_bpdata_0  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_0  = $random;
  else 
    data_in_bpvalid_0  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_1  = $random;
  else 
    data_in_bpaddress_1  = var_value;

  var_value = get_var_assignment("data_in_bpdata_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_1  = $random;
  else 
    data_in_bpdata_1  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_1  = $random;
  else 
    data_in_bpvalid_1  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_2",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_2  = $random;
  else 
    data_in_bpaddress_2  = var_value;

  var_value = get_var_assignment("data_in_bpdata_2",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_2  = $random;
  else 
    data_in_bpdata_2  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_2",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_2  = $random;
  else 
    data_in_bpvalid_2  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_3",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_3  = $random;
  else 
    data_in_bpaddress_3  = var_value;

  var_value = get_var_assignment("data_in_bpdata_3",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_3  = $random;
  else 
    data_in_bpdata_3  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_3",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_3  = $random;
  else 
    data_in_bpvalid_3  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_4",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_4  = $random;
  else 
    data_in_bpaddress_4  = var_value;

  var_value = get_var_assignment("data_in_bpdata_4",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_4  = $random;
  else 
    data_in_bpdata_4  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_4",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_4  = $random;
  else 
    data_in_bpvalid_4  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_5",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_5  = $random;
  else 
    data_in_bpaddress_5  = var_value;

  var_value = get_var_assignment("data_in_bpdata_5",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_5  = $random;
  else 
    data_in_bpdata_5  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_5",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_5  = $random;
  else 
    data_in_bpvalid_5  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_6",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_6  = $random;
  else 
    data_in_bpaddress_6  = var_value;

  var_value = get_var_assignment("data_in_bpdata_6",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_6  = $random;
  else 
    data_in_bpdata_6  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_6",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_6  = $random;
  else 
    data_in_bpvalid_6  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_7",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_7  = $random;
  else 
    data_in_bpaddress_7  = var_value;

  var_value = get_var_assignment("data_in_bpdata_7",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_7  = $random;
  else 
    data_in_bpdata_7  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_7",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_7  = $random;
  else 
    data_in_bpvalid_7  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_8",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_8  = $random;
  else 
    data_in_bpaddress_8  = var_value;

  var_value = get_var_assignment("data_in_bpdata_8",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_8  = $random;
  else 
    data_in_bpdata_8  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_8",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_8  = $random;
  else 
    data_in_bpvalid_8  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_9",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_9  = $random;
  else 
    data_in_bpaddress_9  = var_value;

  var_value = get_var_assignment("data_in_bpdata_9",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_9  = $random;
  else 
    data_in_bpdata_9  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_9",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_9  = $random;
  else 
    data_in_bpvalid_9  = var_value;

  var_value = get_var_assignment("data_in_bpaddress_10",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpaddress_10  = $random;
  else 
    data_in_bpaddress_10  = var_value;

  var_value = get_var_assignment("data_in_bpdata_10",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpdata_10  = $random;
  else 
    data_in_bpdata_10  = var_value;

  var_value = get_var_assignment("data_in_bpvalid_10",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    data_in_bpvalid_10  = $random;
  else 
    data_in_bpvalid_10  = var_value;

  var_value = get_var_assignment("write_enable_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    write_enable_0  = $random;
  else 
    write_enable_0  = var_value;

  var_value = get_var_assignment("write_addr_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    write_addr_0  = $random;
  else 
    write_addr_0  = var_value;

  var_value = get_var_assignment("write_data_0",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    write_data_0  = $random;
  else 
    write_data_0  = var_value;

  var_value = get_var_assignment("write_enable_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    write_enable_1  = $random;
  else 
    write_enable_1  = var_value;

  var_value = get_var_assignment("write_addr_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    write_addr_1  = $random;
  else 
    write_addr_1  = var_value;

  var_value = get_var_assignment("write_data_1",frame_num+1,pat_num);
  if(var_value == -2)
    $finish;
  if(var_value == -1)
    write_data_1  = $random;
  else 
    write_data_1  = var_value;

  frame_num = frame_num + 1;
  
  #5
  if((frame_num==1))
   begin
     $display("finished one pattern enlarge: %d\n",pat_num);
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     $finish;
     pat_num = pat_num + 1;
     frame_num=-1;
   end 
  #1;
end



initial begin
  for(i=0;i<=1000;i=i+1)
  begin
    inst.branvar[i] = 8'h0;
    inst.DECODER0.branvar[i] = 8'h0;;
    inst.DECODER1.branvar[i] = 8'h0;;
    inst.SB.branvar[i] = 8'h0;;
    inst.RF.branvar[i] = 8'h0;;
    inst.SPRF.branvar[i] = 8'h0;;
    inst.BP_SELECT_S_0.branvar[i] = 8'h0;;
    inst.BP_SELECT_S_1.branvar[i] = 8'h0;;
    inst.BP_SELECT_T_0.branvar[i] = 8'h0;;
    inst.BP_SELECT_T_1.branvar[i] = 8'h0;;
  end
end

endmodule
