////////////////////////////////////////////////////////////////////////////////
// wb_stage.sv
////////////////////////////////////////////////////////////////////////////////

// writeback pipeline stage

////////////////////////////////////////////////////////////////////////////////

module main(
  //inputs
  clk,
  reset,
  stall_in,
  wb_int_inpc,
  wb_int_inpc_incr,
  wb_int_inbranch_taken,
  wb_int_inpredicted_pc,
  wb_int_invalid,
  wb_int_insreg_t_data,
  wb_int_insreg_s_data,
  wb_int_inresult,
  wb_int_inL1D_hit,
  wb_int_indecode_packetsreg_t,
  wb_int_indecode_packetsreg_s,
  wb_int_indecode_packetdreg,
  wb_int_indecode_packetacc,
  wb_int_indecode_packetimm16_value,
  wb_int_indecode_packetimm5_value,
  wb_int_indecode_packetop_FU_type,
  wb_int_indecode_packetalu_op,
  wb_int_indecode_packetfpu_op,
  wb_int_indecode_packetop_MEM_type,
  wb_int_indecode_packetop_BR_type,
  wb_int_indecode_packetshift_type,
  wb_int_indecode_packetcompare_type,
  wb_int_indecode_packetis_imm,
  wb_int_indecode_packetis_imm5,
  wb_int_indecode_packetis_signed,
  wb_int_indecode_packetcarry_in,
  wb_int_indecode_packetcarry_out,
  wb_int_indecode_packethas_sreg_t,
  wb_int_indecode_packethas_sreg_s,
  wb_int_indecode_packethas_dreg,
  wb_int_indecode_packetis_prefetch,
  wb_int_indecode_packetsprf_dest,
  wb_int_indecode_packetsprf_src,
  wb_int_indecode_packetis_atomic,
  wb_int_indecode_packetis_ldl,
  wb_int_indecode_packetis_stl,
  wb_int_indecode_packetis_stc,
  wb_int_indecode_packetis_valid,
  wb_int_indecode_packetis_halt,
  wb_int_indecode_packetis_compare,
  wb_mem_inpc,
  wb_mem_inpc_incr,
  wb_mem_inbranch_taken,
  wb_mem_inpredicted_pc,
  wb_mem_invalid,
  wb_mem_insreg_t_data,
  wb_mem_insreg_s_data,
  wb_mem_inresult,
  wb_mem_inL1D_hit,
  wb_mem_indecode_packetsreg_t,
  wb_mem_indecode_packetsreg_s,
  wb_mem_indecode_packetdreg,
  wb_mem_indecode_packetacc,
  wb_mem_indecode_packetimm16_value,
  wb_mem_indecode_packetimm5_value,
  wb_mem_indecode_packetop_FU_type,
  wb_mem_indecode_packetalu_op,
  wb_mem_indecode_packetfpu_op,
  wb_mem_indecode_packetop_MEM_type,
  wb_mem_indecode_packetop_BR_type,
  wb_mem_indecode_packetshift_type,
  wb_mem_indecode_packetcompare_type,
  wb_mem_indecode_packetis_imm,
  wb_mem_indecode_packetis_imm5,
  wb_mem_indecode_packetis_signed,
  wb_mem_indecode_packetcarry_in,
  wb_mem_indecode_packetcarry_out,
  wb_mem_indecode_packethas_sreg_t,
  wb_mem_indecode_packethas_sreg_s,
  wb_mem_indecode_packethas_dreg,
  wb_mem_indecode_packetis_prefetch,
  wb_mem_indecode_packetsprf_dest,
  wb_mem_indecode_packetsprf_src,
  wb_mem_indecode_packetis_atomic,
  wb_mem_indecode_packetis_ldl,
  wb_mem_indecode_packetis_stl,
  wb_mem_indecode_packetis_stc,
  wb_mem_indecode_packetis_valid,
  wb_mem_indecode_packetis_halt,
  wb_mem_indecode_packetis_compare,
  wb_fpu_inpc,
  wb_fpu_inpc_incr,
  wb_fpu_inbranch_taken,
  wb_fpu_inpredicted_pc,
  wb_fpu_invalid,
  wb_fpu_insreg_t_data,
  wb_fpu_insreg_s_data,
  wb_fpu_inresult,
  wb_fpu_inL1D_hit,
  wb_fpu_indecode_packetsreg_t,
  wb_fpu_indecode_packetsreg_s,
  wb_fpu_indecode_packetdreg,
  wb_fpu_indecode_packetacc,
  wb_fpu_indecode_packetimm16_value,
  wb_fpu_indecode_packetimm5_value,
  wb_fpu_indecode_packetop_FU_type,
  wb_fpu_indecode_packetalu_op,
  wb_fpu_indecode_packetfpu_op,
  wb_fpu_indecode_packetop_MEM_type,
  wb_fpu_indecode_packetop_BR_type,
  wb_fpu_indecode_packetshift_type,
  wb_fpu_indecode_packetcompare_type,
  wb_fpu_indecode_packetis_imm,
  wb_fpu_indecode_packetis_imm5,
  wb_fpu_indecode_packetis_signed,
  wb_fpu_indecode_packetcarry_in,
  wb_fpu_indecode_packetcarry_out,
  wb_fpu_indecode_packethas_sreg_t,
  wb_fpu_indecode_packethas_sreg_s,
  wb_fpu_indecode_packethas_dreg,
  wb_fpu_indecode_packetis_prefetch,
  wb_fpu_indecode_packetsprf_dest,
  wb_fpu_indecode_packetsprf_src,
  wb_fpu_indecode_packetis_atomic,
  wb_fpu_indecode_packetis_ldl,
  wb_fpu_indecode_packetis_stl,
  wb_fpu_indecode_packetis_stc,
  wb_fpu_indecode_packetis_valid,
  wb_fpu_indecode_packetis_halt,
  wb_fpu_indecode_packetis_compare,
  // outputs
  wb_valid0,
  wb_addr0,
  wb_data0,
  wb_valid1,
  wb_addr1,
  wb_data1,
  halt
);

  input clk;
  input reset;
  input stall_in;
  input [31:0] wb_int_inpc;
  input [31:0] wb_int_inpc_incr;
  input wb_int_inbranch_taken;
  input [31:0] wb_int_inpredicted_pc;
  input wb_int_invalid;
  input [31:0] wb_int_insreg_t_data;
  input [31:0] wb_int_insreg_s_data;
  input [31:0] wb_int_inresult;
  input wb_int_inL1D_hit;
  input [4:0] wb_int_indecode_packetsreg_t;
  input [4:0] wb_int_indecode_packetsreg_s;
  input [4:0] wb_int_indecode_packetdreg;
  input [1:0] wb_int_indecode_packetacc;
  input [15:0] wb_int_indecode_packetimm16_value;
  input [4:0] wb_int_indecode_packetimm5_value;
  input [3:0] wb_int_indecode_packetop_FU_type;
  input [3:0] wb_int_indecode_packetalu_op;
  input [3:0] wb_int_indecode_packetfpu_op;
  input [2:0] wb_int_indecode_packetop_MEM_type;
  input [1:0] wb_int_indecode_packetop_BR_type;
  input [1:0] wb_int_indecode_packetshift_type;
  input [2:0] wb_int_indecode_packetcompare_type;
  input wb_int_indecode_packetis_imm;
  input wb_int_indecode_packetis_imm5;
  input wb_int_indecode_packetis_signed;
  input wb_int_indecode_packetcarry_in;
  input wb_int_indecode_packetcarry_out;
  input wb_int_indecode_packethas_sreg_t;
  input wb_int_indecode_packethas_sreg_s;
  input wb_int_indecode_packethas_dreg;
  input wb_int_indecode_packetis_prefetch;
  input wb_int_indecode_packetsprf_dest;
  input wb_int_indecode_packetsprf_src;
  input wb_int_indecode_packetis_atomic;
  input wb_int_indecode_packetis_ldl;
  input wb_int_indecode_packetis_stl;
  input wb_int_indecode_packetis_stc;
  input wb_int_indecode_packetis_valid;
  input wb_int_indecode_packetis_halt;
  input wb_int_indecode_packetis_compare;
  input [31:0] wb_mem_inpc;
  input [31:0] wb_mem_inpc_incr;
  input wb_mem_inbranch_taken;
  input [31:0] wb_mem_inpredicted_pc;
  input wb_mem_invalid;
  input [31:0] wb_mem_insreg_t_data;
  input [31:0] wb_mem_insreg_s_data;
  input [31:0] wb_mem_inresult;
  input wb_mem_inL1D_hit;
  input [4:0] wb_mem_indecode_packetsreg_t;
  input [4:0] wb_mem_indecode_packetsreg_s;
  input [4:0] wb_mem_indecode_packetdreg;
  input [1:0] wb_mem_indecode_packetacc;
  input [15:0] wb_mem_indecode_packetimm16_value;
  input [4:0] wb_mem_indecode_packetimm5_value;
  input [3:0] wb_mem_indecode_packetop_FU_type;
  input [3:0] wb_mem_indecode_packetalu_op;
  input [3:0] wb_mem_indecode_packetfpu_op;
  input [2:0] wb_mem_indecode_packetop_MEM_type;
  input [1:0] wb_mem_indecode_packetop_BR_type;
  input [1:0] wb_mem_indecode_packetshift_type;
  input [2:0] wb_mem_indecode_packetcompare_type;
  input wb_mem_indecode_packetis_imm;
  input wb_mem_indecode_packetis_imm5;
  input wb_mem_indecode_packetis_signed;
  input wb_mem_indecode_packetcarry_in;
  input wb_mem_indecode_packetcarry_out;
  input wb_mem_indecode_packethas_sreg_t;
  input wb_mem_indecode_packethas_sreg_s;
  input wb_mem_indecode_packethas_dreg;
  input wb_mem_indecode_packetis_prefetch;
  input wb_mem_indecode_packetsprf_dest;
  input wb_mem_indecode_packetsprf_src;
  input wb_mem_indecode_packetis_atomic;
  input wb_mem_indecode_packetis_ldl;
  input wb_mem_indecode_packetis_stl;
  input wb_mem_indecode_packetis_stc;
  input wb_mem_indecode_packetis_valid;
  input wb_mem_indecode_packetis_halt;
  input wb_mem_indecode_packetis_compare;
  input [31:0] wb_fpu_inpc;
  input [31:0] wb_fpu_inpc_incr;
  input wb_fpu_inbranch_taken;
  input [31:0] wb_fpu_inpredicted_pc;
  input wb_fpu_invalid;
  input [31:0] wb_fpu_insreg_t_data;
  input [31:0] wb_fpu_insreg_s_data;
  input [31:0] wb_fpu_inresult;
  input wb_fpu_inL1D_hit;
  input [4:0] wb_fpu_indecode_packetsreg_t;
  input [4:0] wb_fpu_indecode_packetsreg_s;
  input [4:0] wb_fpu_indecode_packetdreg;
  input [1:0] wb_fpu_indecode_packetacc;
  input [15:0] wb_fpu_indecode_packetimm16_value;
  input [4:0] wb_fpu_indecode_packetimm5_value;
  input [3:0] wb_fpu_indecode_packetop_FU_type;
  input [3:0] wb_fpu_indecode_packetalu_op;
  input [3:0] wb_fpu_indecode_packetfpu_op;
  input [2:0] wb_fpu_indecode_packetop_MEM_type;
  input [1:0] wb_fpu_indecode_packetop_BR_type;
  input [1:0] wb_fpu_indecode_packetshift_type;
  input [2:0] wb_fpu_indecode_packetcompare_type;
  input wb_fpu_indecode_packetis_imm;
  input wb_fpu_indecode_packetis_imm5;
  input wb_fpu_indecode_packetis_signed;
  input wb_fpu_indecode_packetcarry_in;
  input wb_fpu_indecode_packetcarry_out;
  input wb_fpu_indecode_packethas_sreg_t;
  input wb_fpu_indecode_packethas_sreg_s;
  input wb_fpu_indecode_packethas_dreg;
  input wb_fpu_indecode_packetis_prefetch;
  input wb_fpu_indecode_packetsprf_dest;
  input wb_fpu_indecode_packetsprf_src;
  input wb_fpu_indecode_packetis_atomic;
  input wb_fpu_indecode_packetis_ldl;
  input wb_fpu_indecode_packetis_stl;
  input wb_fpu_indecode_packetis_stc;
  input wb_fpu_indecode_packetis_valid;
  input wb_fpu_indecode_packetis_halt;
  input wb_fpu_indecode_packetis_compare;
  output wb_valid0;
  output [4:0]  wb_addr0;
  output [31:0] wb_data0;
  output wb_valid1;
  output [4:0]  wb_addr1;
  output [31:0] wb_data1;
  output halt;
/*
  reg wb_valid0;
  reg [4:0]  wb_addr0;
  reg [31:0] wb_data0;
  reg wb_valid1;
  reg [4:0]  wb_addr1;
  reg [31:0] wb_data1;
  reg halt;
*/
////////////////////////////////////////////////////////////////////////////////

  // COMMENT ME
  //assign bypass_output[0].address = wb_addr0;
  //assign bypass_output[0].data = wb_data0;
  //assign bypass_output[0].valid = wb_valid0;
  //assign bypass_output[1].address = wb_addr1;
  //assign bypass_output[1].data = wb_data1;
  //assign bypass_output[1].valid = wb_valid1;
  reg [31:0] write_port0;
  reg [31:0] write_port1;

  // COMMENT ME
  assign   halt = 
           (wb_int_indecode_packetis_halt) || // && wb_int_invalid) || 
           (wb_fpu_indecode_packetis_halt) || // && wb_fpu_invalid) || 
           (wb_mem_indecode_packetis_halt);   // && wb_mem_invalid);
  //////////////////////////////////////////////////////////////////////////////
  // write port 0
  // INT and MEM use this port
  always @ (wb_int_invalid or wb_int_indecode_packethas_dreg or wb_mem_invalid or wb_mem_indecode_packethas_dreg or wb_int_inresult or wb_mem_inresult) begin: WB_PORT0

    write_port0 = 32'b0;
/*    halt = (wb_int_indecode_packetis_halt && wb_int_invalid) || 
           (wb_fpu_indecode_packetis_halt && wb_fpu_invalid) || 
           (wb_mem_indecode_packetis_halt && wb_mem_invalid); */
    // INT
    if(wb_int_invalid && wb_int_indecode_packethas_dreg) begin
      wb_data0    = wb_int_inresult;
      wb_addr0    = wb_int_indecode_packetdreg;
      wb_valid0   = 1'b1;
      write_port0 = 32'b1;
    // MEM
    end else if(wb_mem_invalid && wb_mem_indecode_packethas_dreg) begin
      wb_data0    = wb_mem_inresult;
      wb_addr0    = wb_mem_indecode_packetdreg;
      wb_valid0   = 1'b1;
      write_port0 = 32'b10;
    end else begin
      wb_valid0 = 1'b0;
    end
      
  end
  //////////////////////////////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////////////////////////////
  // write port 1
  // FPU and MEM use this port
  // make sure MEM not using port0 before assigning it to this port
  always @(wb_fpu_invalid or wb_fpu_indecode_packethas_dreg or write_port0 or wb_mem_invalid or wb_mem_indecode_packethas_dreg or wb_fpu_inresult or wb_mem_inresult) begin: WB_PORT1
  
    write_port1 = 32'b0;

    // FPU
    if(wb_fpu_invalid && wb_fpu_indecode_packethas_dreg) begin
      wb_data1    = wb_fpu_inresult; 
      wb_addr1    = wb_fpu_indecode_packetdreg;
      wb_valid1   = 1'b1;
      write_port1 = 32'b11;
    // MEM
    end else if(write_port0!=32'b10 && wb_mem_invalid && 
                wb_mem_indecode_packethas_dreg) 
    begin
      wb_data1  = wb_mem_inresult;
      wb_addr1  = wb_mem_indecode_packetdreg;
      wb_valid1 = 1'b1;
    end else begin
      wb_valid1 = 1'b0;
    end

  end
  //////////////////////////////////////////////////////////////////////////////

//`include "wb_prop1set1.assert"
/*
always @ (posedge clk)
begin
if (wb_int_invalid==1 && wb_int_indecode_packethas_dreg==1 && wb_mem_indecode_packethas_dreg==0 && wb_mem_invalid==0)
	assert rule5: (write_port0 == 32'b10);
end
// Confidence  = 1000%
// Support     = 31197%
// NumExamples = 370
*/
/*
always @ (posedge clk)
begin
if (wb_int_indecode_packethas_dreg==1 && wb_int_invalid==1 && wb_mem_invalid==0 && wb_mem_indecode_packethas_dreg==0)
	assert rule4: (write_port0[31:0] == 1);
end
// Confidence  = 1000%
// Support     = 55649%
// NumExamples = 660
*/
/*  
  always @ (posedge clk) 
  begin 
  if (wb_int_invalid==1 && wb_int_indecode_packethas_dreg==1) 
    assert rule4: (wb_valid0 == 1);
    assert rule2: (write_port0 == 1);
    assert rule3: (wb_data0 == wb_int_inresult);
    assert rule1: (wb_addr0 == wb_int_indecode_packetdreg);
  end
  // Confidence  = 100%
  // Support     = 55.649%
  // NumExamples = 660
*/
/*
  always @ (clk) 
  begin 
  if (wb_int_indecode_packethas_dreg==1 && wb_int_invalid==1) 
    assert rule2: (write_port0 == 1); 
  end
  // Confidence  = 100%
  // Support     = 55.649%
  // NumExamples = 660

  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk)
  begin
  end
  //////////////////////////////////////////////////////////////////////////////
*/
endmodule
////////////////////////////////////////////////////////////////////////////////
