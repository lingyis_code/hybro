`timescale 1ns/10ps
module b100 (clk , reset , ic_en , icqmem_cycstb_i , icqmem_ci_i , tagcomp_miss , biudata_valid , biudata_error , start_addr , saved_addr , icram_we , biu_read , first_hit_ack , first_miss_ack , first_miss_err , burst , tag_we );

	  input  clk;
	  input  reset;
	  input  ic_en;
	  input  icqmem_cycstb_i;
	  input  icqmem_ci_i;
	  input  tagcomp_miss;
	  input  biudata_valid;
	  input  biudata_error;
	  input [31 : 0] start_addr;
	  output [31 : 0] saved_addr;
	  output [3 : 0] icram_we;
	  output biu_read ;
	  output first_hit_ack ;
	  output first_miss_ack ;
	  output first_miss_err ;
	  output burst ;
	  output tag_we ;
	  reg [31 : 0] saved_addr_r;
	  reg [7:0] branvar[0:100];
	  reg [1 : 0] state;
	  reg [2 : 0] cnt;
	  reg hitmiss_eval;
	  reg load;
	  reg cache_inhibit;
	 assign 
	  icram_we = {4 {(biu_read) &&  (biudata_valid) && (!  cache_inhibit)}};
	 assign 
	  tag_we = ( (  biu_read &&  biudata_valid) && (!  cache_inhibit));
	 assign 
	  biu_read = ( (  hitmiss_eval &&  tagcomp_miss) || ( (!  hitmiss_eval) &&  load));
	 assign 
	  saved_addr =  saved_addr_r;
	 assign 
	  first_hit_ack = ( ( ( ( (  state == 2'd1) &  hitmiss_eval) & (!  tagcomp_miss)) & (!  cache_inhibit)) & (!  icqmem_ci_i));
	 assign 
	  first_miss_ack = ( (  state == 2'd1) &  biudata_valid);
	 assign 
	  first_miss_err = ( (  state == 2'd1) &  biudata_error);
	 assign 
	  burst = ( ( ( (  state == 2'd1) &  tagcomp_miss) & (!  cache_inhibit)) | (  state == 2'd2));
	 always @ (posedge  clk )
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  state <= 2'd0;
	  saved_addr_r <= 32'h0;
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	  cnt <= 3'b000;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( state)
	  2'd0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 if((  ic_en &  icqmem_cycstb_i)) 
	  begin 
	 branvar[3] <= branvar[3] + 1; 
	 begin
	  state <= 2'd1;
	  saved_addr_r <=  start_addr;
	  hitmiss_eval <= 1'b1;
	  load <= 1'b1;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[4] <= branvar[4] + 1;   
	 begin
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	  end
	  2'd1: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 begin
	 if((  icqmem_cycstb_i &  icqmem_ci_i)) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	  cache_inhibit <= 1'b1;
	  end
	 else 
	 begin  
	   branvar[7] <= branvar[7] + 1;   
	   end 
	 if( hitmiss_eval) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	  saved_addr_r[31:13] <= start_addr[31:13];
	  end
	 else 
	 begin  
	   branvar[9] <= branvar[9] + 1;   
	   end 
	 if(( ( ( (!  ic_en) || (  hitmiss_eval & (!  icqmem_cycstb_i))) ||  biudata_error) || (  cache_inhibit &  biudata_valid))) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  state <= 2'd0;
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 if((  tagcomp_miss &  biudata_valid)) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  state <= 2'd2;
	  saved_addr_r[3:2] <= (saved_addr_r[3:2] + 2'd1);
	  hitmiss_eval <= 1'b0;
	  cnt <= 2;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 if(( (!  tagcomp_miss) & (!  icqmem_ci_i))) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  saved_addr_r <=  start_addr;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if((!  icqmem_cycstb_i)) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  state <= 2'd0;
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	  cache_inhibit <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	  hitmiss_eval <= 1'b0;
	  end
	  end
	  end
	  end
	 end
	  end
	  2'd2: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	 begin
	 if(( biudata_valid && (  cnt[0] || cnt[2] || cnt[1]))) 
	  begin 
	 branvar[19] <= branvar[19] + 1; 
	 begin
	  cnt <= (  cnt - 3'd1);
	  saved_addr_r[3:2] <= (saved_addr_r[3:2] + 2'd1);
	 end
	  end
	 else 
	   begin 
	   branvar[20] <= branvar[20] + 1;   
	 if( biudata_valid) 
	  begin 
	 branvar[21] <= branvar[21] + 1; 
	 begin
	  state <= 2'd0;
	  saved_addr_r <=  start_addr;
	  hitmiss_eval <= 1'b0;
	  load <= 1'b0;
	 end
	  end
	 else 
	 begin  
	   branvar[22] <= branvar[22] + 1;   
	   end 
	  end
	 end
	  end
	 endcase
	  end
	 end
   `include "../rtl/b100.true"
	  endmodule 
