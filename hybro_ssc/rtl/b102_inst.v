////                                                              ////
////  OR1200's WISHBONE BIU                                       ////
`timescale 1ns/10ps
module b102(

	// WISHBONE interface
	clk, rst, wb_ack_i, wb_err_i, wb_rty_i, wb_dat_i,
	wb_cyc_o, wb_adr_o, wb_stb_o, wb_we_o, wb_sel_o, wb_dat_o,
	wb_cab_o,
	wb_cti_o, wb_bte_o,

	// Internal RISC bus
	biu_dat_i, biu_adr_i, biu_cyc_i, biu_stb_i, biu_we_i, biu_sel_i, biu_cab_i,
	biu_dat_o, biu_ack_o, biu_err_o
);


//
// RISC clock, reset and clock control
//

//
// WISHBONE interface
//
input			clk;	// clock input
input			rst;	// reset input
input			wb_ack_i;	// normal termination
input			wb_err_i;	// termination w/ error
input			wb_rty_i;	// termination w/ retry
input	[31:0]	wb_dat_i;	// input data bus
output			wb_cyc_o;	// cycle valid output
output	[31:0]	wb_adr_o;	// address bus outputs
output			wb_stb_o;	// strobe output
output			wb_we_o;	// indicates write transfer
output	[3:0]		wb_sel_o;	// byte select outputs
output	[31:0]	wb_dat_o;	// output data bus
output			wb_cab_o;	// consecutive address burst
output	[2:0]		wb_cti_o;	// cycle type identifier
output	[1:0]		wb_bte_o;	// burst type extension

//
// Internal RISC interface
//
input	[31:0]	biu_dat_i;	// input data bus
input	[31:0]	biu_adr_i;	// address bus
input			biu_cyc_i;	// WB cycle
input			biu_stb_i;	// WB strobe
input			biu_we_i;	// WB write enable
input			biu_cab_i;	// CAB input
input	[3:0]		biu_sel_i;	// byte selects
output	[31:0]		biu_dat_o;	// output data bus
output			biu_ack_o;	// ack output
output			biu_err_o;	// err output

//
// Registers
//
reg	[31:0]	wb_adr_o;	// address bus outputs
reg			wb_cyc_o;	// cycle output
reg			wb_stb_o;	// strobe output
reg			wb_we_o;	// indicates write transfer
reg	[3:0]		wb_sel_o;	// byte select outputs
reg			wb_cab_o;	// CAB output

reg	[1:0]		burst_len;	// burst counter
reg	[2:0]		wb_cti_o;	// cycle type identifier
reg	[31:0]	wb_dat_o;	// output data bus

reg			long_ack_o;	// normal termination
reg			long_err_o;	// error termination
reg	[31:0]	biu_dat_o;	// output data bus
wire			aborted;	// Graceful abort
reg			aborted_r;	// Graceful abort
wire			retry;		// Retry
reg	[6:0] retry_cntr;	// Retry counter
reg [7:0] branvar[0:100];



assign biu_ack_o = long_ack_o;
assign biu_err_o = long_err_o;
assign wb_bte_o = 2'b01;	
assign retry = wb_rty_i || (retry_cntr[6] ||retry_cntr[5] ||retry_cntr[4] ||retry_cntr[3] ||retry_cntr[2] ||retry_cntr[1] ||retry_cntr[0]);
assign aborted = wb_stb_o & ~(biu_cyc_i & biu_stb_i) & ~(wb_ack_i | wb_err_i);

always @(posedge clk  ) 
	if (rst)
      begin
          branvar[0] <= branvar[0] + 1; 
		wb_adr_o <=   32'h0;
      end
	else 
      begin 
          branvar[1] <= branvar[1] + 1; 
        if ((biu_cyc_i & biu_stb_i) & ~wb_ack_i & ~aborted & ~(wb_stb_o & ~wb_ack_i))
          begin
              branvar[2] <= branvar[2] + 1; 
         	wb_adr_o <=   biu_adr_i;
          end
         else
          begin
              branvar[3] <= branvar[3] + 1; 
          end
      end

always @(posedge clk  )
	if (rst)
     begin
          branvar[4] <= branvar[4] + 1; 
		biu_dat_o <=   32'h0000_0000;
     end
	else
     begin   
            branvar[5] <= branvar[5] + 1; 
       if (wb_ack_i)
       begin
            branvar[6] <= branvar[6] + 1; 
	  	biu_dat_o <=   wb_dat_i;
       end
       else
       begin
            branvar[7] <= branvar[7] + 1; 
       end
     end

always @(posedge clk  )
	if (rst)
     begin
          branvar[8] <= branvar[8] + 1; 
		wb_dat_o <=   32'h0;
     end
	else
     begin
          branvar[9] <= branvar[9] + 1; 
       if ((biu_cyc_i & biu_stb_i) & ~wb_ack_i & ~aborted)
       begin
            branvar[10] <= branvar[10] + 1; 
	  	wb_dat_o <=   biu_dat_i;
       end
       else
       begin
            branvar[11] <= branvar[11] + 1; 
       end
     end

always @(posedge clk  )
	if (rst)
     begin
          branvar[12] <= branvar[12] + 1; 
		retry_cntr <=   7'h0;
     end
	else 
     begin 
        branvar[13] <= branvar[13] + 1; 
        if (wb_rty_i)
        begin
            branvar[14] <= branvar[14] + 1; 
	       retry_cntr <=  7'h3f;
        end
	   else
        begin 
          branvar[15] <= branvar[15] + 1; 
          if (retry_cntr!=0)
          begin
              branvar[16] <= branvar[16] + 1; 
	         retry_cntr <=   retry_cntr - 7'h1;
          end
          else
          begin
              branvar[17] <= branvar[17] + 1; 
          end
        end
     end


always @(posedge clk  )
	if (rst)
     begin
          branvar[18] <= branvar[18] + 1; 
		aborted_r <=   1'b0;
     end
	else
     begin 
       branvar[19] <= branvar[19] + 1; 
       if (wb_ack_i | wb_err_i)
       begin
            branvar[20] <= branvar[20] + 1; 
	  	aborted_r <=   1'b0;
       end
	  else 
       begin
          branvar[21] <= branvar[21] + 1; 
          if (aborted)
          begin
               branvar[22] <= branvar[22] + 1; 
	     	aborted_r <=   1'b1;
          end
          else
           begin
               branvar[23] <= branvar[23] + 1; 
           end
       end
      end

always @(posedge clk  )
	if (rst)
     begin
          branvar[24] <= branvar[24] + 1; 
		wb_cyc_o <=   1'b0;
		wb_stb_o <=   1'b0;
		wb_we_o <=   1'b0;
		wb_sel_o <=   4'b0000;
		wb_cab_o <=   1'b0;
		long_ack_o <=   1'b0;
		long_err_o <=   1'b0;
     end     
	else
     begin
          branvar[25] <= branvar[25] + 1; 
		wb_cyc_o <=   biu_cyc_i & ~wb_ack_i & ~retry | aborted & ~wb_ack_i;
		wb_stb_o <=   (biu_cyc_i & biu_stb_i) & ~wb_ack_i & ~retry | aborted & ~wb_ack_i;
		wb_we_o <=   biu_cyc_i & biu_stb_i & biu_we_i | aborted & wb_we_o;
		wb_sel_o <=   biu_sel_i;
		wb_cab_o <=   biu_cab_i;
		long_ack_o <=   wb_ack_i & ~aborted;
		long_err_o <=   wb_err_i & ~aborted;
          
     end


always @(posedge clk  )
	if (rst)
     begin
          branvar[26] <= branvar[26] + 1; 
		burst_len <=   2'b00;
     end
	else
     begin 
        branvar[27] <= branvar[27] + 1; 
        if (biu_cab_i && (burst_len[1]) && wb_ack_i)
        begin
           branvar[28] <= branvar[28] + 1; 
	   	burst_len <=   burst_len - 1'b1;
        end
	   else
        begin 
           branvar[29] <= branvar[29] + 1; 
          if (~biu_cab_i)
          begin
               branvar[30] <= branvar[30] + 1; 
	     	burst_len <=   2'b11;
          end
          else
          begin
               branvar[31] <= branvar[31] + 1; 
          end
        end
      end

always @(posedge clk  )
	if (rst)
     begin
          branvar[32] <= branvar[32] + 1; 
		wb_cti_o <=   3'b000;	
     end
	else 
     begin
          branvar[33] <= branvar[33] + 1; 
       if (biu_cab_i && burst_len[1])
       begin
            branvar[34] <= branvar[34] + 1; 
	  	wb_cti_o <=   3'b010;	
       end
	  else
       begin 
            branvar[35] <= branvar[35] + 1; 
         if (biu_cab_i && wb_ack_i)
         begin
              branvar[36] <= branvar[36] + 1; 
	    	wb_cti_o <=   3'b111;	
         end
         else
         begin
              branvar[37] <= branvar[37] + 1; 
         end
        end
     end
   
endmodule
