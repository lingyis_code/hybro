module decode_stage (clk , reset , stall_in_0 , stall_in_1 , stall_in_2 , branch_mispredict , fetch2decodeinstr0_out , fetch2decodeinstr1_out , fetch2decodepc1 , fetch2decodepc0 , fetch2decodepc1_incr , fetch2decodepc0_incr , fetch2decodeinstr0_valid , fetch2decodeinstr1_valid , fetch2decodebranch_taken , fetch2decodepredicted_pc , fetch2decodevalid , data_in_bpaddress_0 , data_in_bpdata_0 , data_in_bpvalid_0 , data_in_bpaddress_1 , data_in_bpdata_1 , data_in_bpvalid_1 , data_in_bpaddress_2 , data_in_bpdata_2 , data_in_bpvalid_2 , data_in_bpaddress_3 , data_in_bpdata_3 , data_in_bpvalid_3 , data_in_bpaddress_4 , data_in_bpdata_4 , data_in_bpvalid_4 , data_in_bpaddress_5 , data_in_bpdata_5 , data_in_bpvalid_5 , data_in_bpaddress_6 , data_in_bpdata_6 , data_in_bpvalid_6 , data_in_bpaddress_7 , data_in_bpdata_7 , data_in_bpvalid_7 , data_in_bpaddress_8 , data_in_bpdata_8 , data_in_bpvalid_8 , data_in_bpaddress_9 , data_in_bpdata_9 , data_in_bpvalid_9 , data_in_bpaddress_10 , data_in_bpdata_10 , data_in_bpvalid_10 , write_enable_0 , write_addr_0 , write_data_0 , write_enable_1 , write_addr_1 , write_data_1 , decode2mempc , decode2mempc_incr , decode2memthread_num , decode2membranch_taken , decode2mempredicted_pc , decode2memvalid , decode2memis_oldest , decode2memsreg_t_data , decode2memsreg_s_data , decode2memresult , decode2memL1D_hit , decode2memdecode_packetsreg_t , decode2memdecode_packetsreg_s , decode2memdecode_packetdreg , decode2memdecode_packetacc , decode2memdecode_packetimm16_value , decode2memdecode_packetimm26_value , decode2memdecode_packetimm5_value , decode2memdecode_packetop_FU_type , decode2memdecode_packetalu_op , decode2memdecode_packetfpu_op , decode2memdecode_packetop_MEM_type , decode2memdecode_packetop_BR_type , decode2memdecode_packetshift_type , decode2memdecode_packetcompare_type , decode2memdecode_packetis_imm26 , decode2memdecode_packetis_imm , decode2memdecode_packetis_imm5 , decode2memdecode_packetis_signed , decode2memdecode_packetcarry_in , decode2memdecode_packetcarry_out , decode2memdecode_packethas_sreg_t , decode2memdecode_packethas_sreg_s , decode2memdecode_packethas_dreg , decode2memdecode_packetis_prefetch , decode2memdecode_packetsprf_dest , decode2memdecode_packetsprf_src , decode2memdecode_packetis_atomic , decode2memdecode_packetis_ldl , decode2memdecode_packetis_stl , decode2memdecode_packetis_stc , decode2memdecode_packetis_valid , decode2memdecode_packetis_halt , decode2memdecode_packetis_compare , decode2memdecode_packetdreg_is_src , decode2memdecode_packetdreg_is_dest , decode2fpupc , decode2fpupc_incr , decode2fputhread_num , decode2fpubranch_taken , decode2fpupredicted_pc , decode2fpuvalid , decode2fpuis_oldest , decode2fpusreg_t_data , decode2fpusreg_s_data , decode2fpuresult , decode2fpuL1D_hit , decode2fpudecode_packetsreg_t , decode2fpudecode_packetsreg_s , decode2fpudecode_packetdreg , decode2fpudecode_packetacc , decode2fpudecode_packetimm16_value , decode2fpudecode_packetimm26_value , decode2fpudecode_packetimm5_value , decode2fpudecode_packetop_FU_type , decode2fpudecode_packetalu_op , decode2fpudecode_packetfpu_op , decode2fpudecode_packetop_MEM_type , decode2fpudecode_packetop_BR_type , decode2fpudecode_packetshift_type , decode2fpudecode_packetcompare_type , decode2fpudecode_packetis_imm26 , decode2fpudecode_packetis_imm , decode2fpudecode_packetis_imm5 , decode2fpudecode_packetis_signed , decode2fpudecode_packetcarry_in , decode2fpudecode_packetcarry_out , decode2fpudecode_packethas_sreg_t , decode2fpudecode_packethas_sreg_s , decode2fpudecode_packethas_dreg , decode2fpudecode_packetis_prefetch , decode2fpudecode_packetsprf_dest , decode2fpudecode_packetsprf_src , decode2fpudecode_packetis_atomic , decode2fpudecode_packetis_ldl , decode2fpudecode_packetis_stl , decode2fpudecode_packetis_stc , decode2fpudecode_packetis_valid , decode2fpudecode_packetis_halt , decode2fpudecode_packetis_compare , decode2fpudecode_packetdreg_is_src , decode2fpudecode_packetdreg_is_dest , decode2intpc , decode2intpc_incr , decode2intthread_num , decode2intbranch_taken , decode2intpredicted_pc , decode2intvalid , decode2intis_oldest , decode2intsreg_t_data , decode2intsreg_s_data , decode2intresult , decode2intL1D_hit , decode2intdecode_packetsreg_t , decode2intdecode_packetsreg_s , decode2intdecode_packetdreg , decode2intdecode_packetacc , decode2intdecode_packetimm16_value , decode2intdecode_packetimm26_value , decode2intdecode_packetimm5_value , decode2intdecode_packetop_FU_type , decode2intdecode_packetalu_op , decode2intdecode_packetfpu_op , decode2intdecode_packetop_MEM_type , decode2intdecode_packetop_BR_type , decode2intdecode_packetshift_type , decode2intdecode_packetcompare_type , decode2intdecode_packetis_imm26 , decode2intdecode_packetis_imm , decode2intdecode_packetis_imm5 , decode2intdecode_packetis_signed , decode2intdecode_packetcarry_in , decode2intdecode_packetcarry_out , decode2intdecode_packethas_sreg_t , decode2intdecode_packethas_sreg_s , decode2intdecode_packethas_dreg , decode2intdecode_packetis_prefetch , decode2intdecode_packetsprf_dest , decode2intdecode_packetsprf_src , decode2intdecode_packetis_atomic , decode2intdecode_packetis_ldl , decode2intdecode_packetis_stl , decode2intdecode_packetis_stc , decode2intdecode_packetis_valid , decode2intdecode_packetis_halt , decode2intdecode_packetis_compare , decode2intdecode_packetdreg_is_src , decode2intdecode_packetdreg_is_dest , decode_stall );
	  input  clk;
	  input  reset;
	  input  stall_in_0;
	  input  stall_in_1;
	  input  stall_in_2;
	  input  branch_mispredict;
	  input [31 : 0] fetch2decodeinstr0_out;
	  input [31 : 0] fetch2decodeinstr1_out;
	  input [31 : 0] fetch2decodepc1;
	  input [31 : 0] fetch2decodepc0;
	  input [31 : 0] fetch2decodepc1_incr;
	  input [31 : 0] fetch2decodepc0_incr;
	  input  fetch2decodeinstr0_valid;
	  input  fetch2decodeinstr1_valid;
	  input  fetch2decodebranch_taken;
	  input [31 : 0] fetch2decodepredicted_pc;
	  input  fetch2decodevalid;
	  input [4 : 0] data_in_bpaddress_0;
	  input [31 : 0] data_in_bpdata_0;
	  input  data_in_bpvalid_0;
	  input [4 : 0] data_in_bpaddress_1;
	  input [31 : 0] data_in_bpdata_1;
	  input  data_in_bpvalid_1;
	  input [4 : 0] data_in_bpaddress_2;
	  input [31 : 0] data_in_bpdata_2;
	  input  data_in_bpvalid_2;
	  input [4 : 0] data_in_bpaddress_3;
	  input [31 : 0] data_in_bpdata_3;
	  input  data_in_bpvalid_3;
	  input [4 : 0] data_in_bpaddress_4;
	  input [31 : 0] data_in_bpdata_4;
	  input  data_in_bpvalid_4;
	  input [4 : 0] data_in_bpaddress_5;
	  input [31 : 0] data_in_bpdata_5;
	  input  data_in_bpvalid_5;
	  input [4 : 0] data_in_bpaddress_6;
	  input [31 : 0] data_in_bpdata_6;
	  input  data_in_bpvalid_6;
	  input [4 : 0] data_in_bpaddress_7;
	  input [31 : 0] data_in_bpdata_7;
	  input  data_in_bpvalid_7;
	  input [4 : 0] data_in_bpaddress_8;
	  input [31 : 0] data_in_bpdata_8;
	  input  data_in_bpvalid_8;
	  input [4 : 0] data_in_bpaddress_9;
	  input [31 : 0] data_in_bpdata_9;
	  input  data_in_bpvalid_9;
	  input [4 : 0] data_in_bpaddress_10;
	  input [31 : 0] data_in_bpdata_10;
	  input  data_in_bpvalid_10;
	  input  write_enable_0;
	  input [4 : 0] write_addr_0;
	  input [31 : 0] write_data_0;
	  input  write_enable_1;
	  input [4 : 0] write_addr_1;
	  input [31 : 0] write_data_1;
	  output [31 : 0] decode2mempc;
	  output [31 : 0] decode2mempc_incr;
	  output [1 : 0] decode2memthread_num;
	  output decode2membranch_taken ;
	  output [31 : 0] decode2mempredicted_pc;
	  output decode2memvalid ;
	  output decode2memis_oldest ;
	  output [31 : 0] decode2memsreg_t_data;
	  output [31 : 0] decode2memsreg_s_data;
	  output [31 : 0] decode2memresult;
	  output decode2memL1D_hit ;
	  output [4 : 0] decode2memdecode_packetsreg_t;
	  output [4 : 0] decode2memdecode_packetsreg_s;
	  output [4 : 0] decode2memdecode_packetdreg;
	  output [1 : 0] decode2memdecode_packetacc;
	  output [15 : 0] decode2memdecode_packetimm16_value;
	  output [25 : 0] decode2memdecode_packetimm26_value;
	  output [4 : 0] decode2memdecode_packetimm5_value;
	  output [3 : 0] decode2memdecode_packetop_FU_type;
	  output [4 : 0] decode2memdecode_packetalu_op;
	  output [4 : 0] decode2memdecode_packetfpu_op;
	  output [3 : 0] decode2memdecode_packetop_MEM_type;
	  output [1 : 0] decode2memdecode_packetop_BR_type;
	  output [1 : 0] decode2memdecode_packetshift_type;
	  output [2 : 0] decode2memdecode_packetcompare_type;
	  output decode2memdecode_packetis_imm26 ;
	  output decode2memdecode_packetis_imm ;
	  output decode2memdecode_packetis_imm5 ;
	  output decode2memdecode_packetis_signed ;
	  output decode2memdecode_packetcarry_in ;
	  output decode2memdecode_packetcarry_out ;
	  output decode2memdecode_packethas_sreg_t ;
	  output decode2memdecode_packethas_sreg_s ;
	  output decode2memdecode_packethas_dreg ;
	  output decode2memdecode_packetis_prefetch ;
	  output decode2memdecode_packetsprf_dest ;
	  output decode2memdecode_packetsprf_src ;
	  output decode2memdecode_packetis_atomic ;
	  output decode2memdecode_packetis_ldl ;
	  output decode2memdecode_packetis_stl ;
	  output decode2memdecode_packetis_stc ;
	  output decode2memdecode_packetis_valid ;
	  output decode2memdecode_packetis_halt ;
	  output decode2memdecode_packetis_compare ;
	  output decode2memdecode_packetdreg_is_src ;
	  output decode2memdecode_packetdreg_is_dest ;
	  output [31 : 0] decode2fpupc;
	  output [31 : 0] decode2fpupc_incr;
	  output [1 : 0] decode2fputhread_num;
	  output decode2fpubranch_taken ;
	  output [31 : 0] decode2fpupredicted_pc;
	  output decode2fpuvalid ;
	  output decode2fpuis_oldest ;
	  output [31 : 0] decode2fpusreg_t_data;
	  output [31 : 0] decode2fpusreg_s_data;
	  output [31 : 0] decode2fpuresult;
	  output decode2fpuL1D_hit ;
	  output [4 : 0] decode2fpudecode_packetsreg_t;
	  output [4 : 0] decode2fpudecode_packetsreg_s;
	  output [4 : 0] decode2fpudecode_packetdreg;
	  output [1 : 0] decode2fpudecode_packetacc;
	  output [15 : 0] decode2fpudecode_packetimm16_value;
	  output [25 : 0] decode2fpudecode_packetimm26_value;
	  output [4 : 0] decode2fpudecode_packetimm5_value;
	  output [3 : 0] decode2fpudecode_packetop_FU_type;
	  output [4 : 0] decode2fpudecode_packetalu_op;
	  output [4 : 0] decode2fpudecode_packetfpu_op;
	  output [3 : 0] decode2fpudecode_packetop_MEM_type;
	  output [1 : 0] decode2fpudecode_packetop_BR_type;
	  output [1 : 0] decode2fpudecode_packetshift_type;
	  output [2 : 0] decode2fpudecode_packetcompare_type;
	  output decode2fpudecode_packetis_imm26 ;
	  output decode2fpudecode_packetis_imm ;
	  output decode2fpudecode_packetis_imm5 ;
	  output decode2fpudecode_packetis_signed ;
	  output decode2fpudecode_packetcarry_in ;
	  output decode2fpudecode_packetcarry_out ;
	  output decode2fpudecode_packethas_sreg_t ;
	  output decode2fpudecode_packethas_sreg_s ;
	  output decode2fpudecode_packethas_dreg ;
	  output decode2fpudecode_packetis_prefetch ;
	  output decode2fpudecode_packetsprf_dest ;
	  output decode2fpudecode_packetsprf_src ;
	  output decode2fpudecode_packetis_atomic ;
	  output decode2fpudecode_packetis_ldl ;
	  output decode2fpudecode_packetis_stl ;
	  output decode2fpudecode_packetis_stc ;
	  output decode2fpudecode_packetis_valid ;
	  output decode2fpudecode_packetis_halt ;
	  output decode2fpudecode_packetis_compare ;
	  output decode2fpudecode_packetdreg_is_src ;
	  output decode2fpudecode_packetdreg_is_dest ;
	  output [31 : 0] decode2intpc;
	  output [31 : 0] decode2intpc_incr;
	  output [1 : 0] decode2intthread_num;
	  output decode2intbranch_taken ;
	  output [31 : 0] decode2intpredicted_pc;
	  output decode2intvalid ;
	  output decode2intis_oldest ;
	  output [31 : 0] decode2intsreg_t_data;
	  output [31 : 0] decode2intsreg_s_data;
	  output [31 : 0] decode2intresult;
	  output decode2intL1D_hit ;
	  output [4 : 0] decode2intdecode_packetsreg_t;
	  output [4 : 0] decode2intdecode_packetsreg_s;
	  output [4 : 0] decode2intdecode_packetdreg;
	  output [1 : 0] decode2intdecode_packetacc;
	  output [15 : 0] decode2intdecode_packetimm16_value;
	  output [25 : 0] decode2intdecode_packetimm26_value;
	  output [4 : 0] decode2intdecode_packetimm5_value;
	  output [3 : 0] decode2intdecode_packetop_FU_type;
	  output [4 : 0] decode2intdecode_packetalu_op;
	  output [4 : 0] decode2intdecode_packetfpu_op;
	  output [3 : 0] decode2intdecode_packetop_MEM_type;
	  output [1 : 0] decode2intdecode_packetop_BR_type;
	  output [1 : 0] decode2intdecode_packetshift_type;
	  output [2 : 0] decode2intdecode_packetcompare_type;
	  output decode2intdecode_packetis_imm26 ;
	  output decode2intdecode_packetis_imm ;
	  output decode2intdecode_packetis_imm5 ;
	  output decode2intdecode_packetis_signed ;
	  output decode2intdecode_packetcarry_in ;
	  output decode2intdecode_packetcarry_out ;
	  output decode2intdecode_packethas_sreg_t ;
	  output decode2intdecode_packethas_sreg_s ;
	  output decode2intdecode_packethas_dreg ;
	  output decode2intdecode_packetis_prefetch ;
	  output decode2intdecode_packetsprf_dest ;
	  output decode2intdecode_packetsprf_src ;
	  output decode2intdecode_packetis_atomic ;
	  output decode2intdecode_packetis_ldl ;
	  output decode2intdecode_packetis_stl ;
	  output decode2intdecode_packetis_stc ;
	  output decode2intdecode_packetis_valid ;
	  output decode2intdecode_packetis_halt ;
	  output decode2intdecode_packetis_compare ;
	  output decode2intdecode_packetdreg_is_src ;
	  output decode2intdecode_packetdreg_is_dest ;
	  output decode_stall ;
	  reg [4 : 0] decode_packet0sreg_t;
	  reg [7:0] branvar[0:1000];
	  reg [4 : 0] decode_packet0sreg_s;
	  reg [4 : 0] decode_packet0dreg;
	  reg [1 : 0] decode_packet0acc;
	  reg [15 : 0] decode_packet0imm16_value;
	  reg [25 : 0] decode_packet0imm26_value;
	  reg [4 : 0] decode_packet0imm5_value;
	  reg [3 : 0] decode_packet0op_FU_type;
	  reg [4 : 0] decode_packet0alu_op;
	  reg [4 : 0] decode_packet0fpu_op;
	  reg [3 : 0] decode_packet0op_MEM_type;
	  reg [1 : 0] decode_packet0op_BR_type;
	  reg [1 : 0] decode_packet0shift_type;
	  reg [2 : 0] decode_packet0compare_type;
	  reg decode_packet0is_imm26;
	  reg decode_packet0is_imm;
	  reg decode_packet0is_imm5;
	  reg decode_packet0is_signed;
	  reg decode_packet0carry_in;
	  reg decode_packet0carry_out;
	  reg decode_packet0has_sreg_t;
	  reg decode_packet0has_sreg_s;
	  reg decode_packet0has_dreg;
	  reg decode_packet0is_prefetch;
	  reg decode_packet0sprf_dest;
	  reg decode_packet0sprf_src;
	  reg decode_packet0is_atomic;
	  reg decode_packet0is_ldl;
	  reg decode_packet0is_stl;
	  reg decode_packet0is_stc;
	  reg decode_packet0is_valid;
	  reg decode_packet0is_halt;
	  reg decode_packet0is_compare;
	  reg decode_packet0dreg_is_src;
	  reg decode_packet0dreg_is_dest;
	  reg [4 : 0] decode_packet1sreg_t;
	  reg [4 : 0] decode_packet1sreg_s;
	  reg [4 : 0] decode_packet1dreg;
	  reg [1 : 0] decode_packet1acc;
	  reg [15 : 0] decode_packet1imm16_value;
	  reg [25 : 0] decode_packet1imm26_value;
	  reg [4 : 0] decode_packet1imm5_value;
	  reg [3 : 0] decode_packet1op_FU_type;
	  reg [4 : 0] decode_packet1alu_op;
	  reg [4 : 0] decode_packet1fpu_op;
	  reg [3 : 0] decode_packet1op_MEM_type;
	  reg [1 : 0] decode_packet1op_BR_type;
	  reg [1 : 0] decode_packet1shift_type;
	  reg [2 : 0] decode_packet1compare_type;
	  reg decode_packet1is_imm26;
	  reg decode_packet1is_imm;
	  reg decode_packet1is_imm5;
	  reg decode_packet1is_signed;
	  reg decode_packet1carry_in;
	  reg decode_packet1carry_out;
	  reg decode_packet1has_sreg_t;
	  reg decode_packet1has_sreg_s;
	  reg decode_packet1has_dreg;
	  reg decode_packet1is_prefetch;
	  reg decode_packet1sprf_dest;
	  reg decode_packet1sprf_src;
	  reg decode_packet1is_atomic;
	  reg decode_packet1is_ldl;
	  reg decode_packet1is_stl;
	  reg decode_packet1is_stc;
	  reg decode_packet1is_valid;
	  reg decode_packet1is_halt;
	  reg decode_packet1is_compare;
	  reg decode_packet1dreg_is_src;
	  reg decode_packet1dreg_is_dest;
	  reg [31 : 0] sreg_t_data0;
	  reg [31 : 0] sreg_t_data1;
	  reg [31 : 0] sreg_s_data0;
	  reg [31 : 0] sreg_s_data1;
	  reg [31 : 0] sprf_sreg_data;
	  reg [3 : 0] sprf_sreg_addr;
	  reg sprf_src;
	  reg sprf_dest;
	  reg [31 : 0] sreg_t_0;
	  reg [31 : 0] sreg_s_0;
	  reg [31 : 0] sreg_t_1;
	  reg [31 : 0] sreg_s_1;
	  reg instr0_issued;
	  reg schedule0;
	  reg schedule1;
	  reg issue0;
	  reg issue1;
	  reg valid_int;
	  reg valid_fpu;
	  reg valid_mem;
	  reg choice_int;
	  reg choice_fpu;
	  reg choice_mem;
	  wire issue_halt;
	  wire sreg_t_0_is_bp;
	  wire sreg_t_1_is_bp;
	  wire sreg_s_0_is_bp;
	  wire sreg_s_1_is_bp;
	  wire sreg_s_0_rdy;
	  wire sreg_t_0_rdy;
	  wire sreg_s_1_rdy;
	  wire sreg_t_1_rdy;
	  wire dreg_0_rdy;
	  wire dreg_1_rdy;
	  wire follows_vld_branch;
	  wire exec_stall;
	 assign 
	  decode_stall = ((!  branch_mispredict) && (((!  issue1) &&  fetch2decodevalid) &&  fetch2decodeinstr1_valid));
	 assign 
	  sprf_src = (  decode_packet0sprf_src |  decode_packet1sprf_src);
	 assign 
	  sprf_dest = (  decode_packet0sprf_dest |  decode_packet1sprf_dest);
	 assign 
	  sprf_sreg_addr = (  choice_int ?  decode_packet1sreg_t :  decode_packet0sreg_t);
	 assign 
	  issue_halt = (( fetch2decodevalid && (!  exec_stall)) && ( ( decode_packet1is_halt && ( ( instr0_issued &&  schedule1) || (!  fetch2decodeinstr0_valid))) || ( decode_packet0is_halt &&  schedule0)));
	 assign 
	  follows_vld_branch = (((  decode_packet0op_FU_type == 4'b1010) &&  fetch2decodeinstr0_valid) &&  branch_mispredict);
	 assign 
	  exec_stall = ( (  stall_in_0 ||  stall_in_2) ||  stall_in_1);
	  decoder0 DECODER0 (
	 .fetch2decodeinstr1_out (fetch2decodeinstr0_out[31:0]),
	 .decode_packet1sreg_t (decode_packet0sreg_t[4:0]),
	 .decode_packet1sreg_s (decode_packet0sreg_s[4:0]),
	 .decode_packet1dreg (decode_packet0dreg[4:0]),
	 .decode_packet1acc (decode_packet0acc[1:0]),
	 .decode_packet1imm16_value (decode_packet0imm16_value[15:0]),
	 .decode_packet1imm26_value (decode_packet0imm26_value[25:0]),
	 .decode_packet1imm5_value (decode_packet0imm5_value[4:0]),
	 .decode_packet1op_FU_type (decode_packet0op_FU_type[3:0]),
	 .decode_packet1alu_op (decode_packet0alu_op[4:0]),
	 .decode_packet1fpu_op (decode_packet0fpu_op[4:0]),
	 .decode_packet1op_MEM_type (decode_packet0op_MEM_type[3:0]),
	 .decode_packet1op_BR_type (decode_packet0op_BR_type[1:0]),
	 .decode_packet1shift_type (decode_packet0shift_type[1:0]),
	 .decode_packet1compare_type (decode_packet0compare_type[2:0]),
	 .decode_packet1is_imm26 ( decode_packet0is_imm26),
	 .decode_packet1is_imm ( decode_packet0is_imm),
	 .decode_packet1is_imm5 ( decode_packet0is_imm5),
	 .decode_packet1is_signed ( decode_packet0is_signed),
	 .decode_packet1carry_in ( decode_packet0carry_in),
	 .decode_packet1carry_out ( decode_packet0carry_out),
	 .decode_packet1has_sreg_t ( decode_packet0has_sreg_t),
	 .decode_packet1has_sreg_s ( decode_packet0has_sreg_s),
	 .decode_packet1has_dreg ( decode_packet0has_dreg),
	 .decode_packet1is_prefetch ( decode_packet0is_prefetch),
	 .decode_packet1sprf_dest ( decode_packet0sprf_dest),
	 .decode_packet1sprf_src ( decode_packet0sprf_src),
	 .decode_packet1is_atomic ( decode_packet0is_atomic),
	 .decode_packet1is_ldl ( decode_packet0is_ldl),
	 .decode_packet1is_stl ( decode_packet0is_stl),
	 .decode_packet1is_stc ( decode_packet0is_stc),
	 .decode_packet1is_valid ( decode_packet0is_valid),
	 .decode_packet1is_halt ( decode_packet0is_halt),
	 .decode_packet1is_compare ( decode_packet0is_compare),
	 .decode_packet1dreg_is_src ( decode_packet0dreg_is_src),
	 .decode_packet1dreg_is_dest ( decode_packet0dreg_is_dest));
	  decoder1 DECODER1 (
	 .fetch2decodeinstr1_out (fetch2decodeinstr1_out[31:0]),
	 .decode_packet1sreg_t (decode_packet1sreg_t[4:0]),
	 .decode_packet1sreg_s (decode_packet1sreg_s[4:0]),
	 .decode_packet1dreg (decode_packet1dreg[4:0]),
	 .decode_packet1acc (decode_packet1acc[1:0]),
	 .decode_packet1imm16_value (decode_packet1imm16_value[15:0]),
	 .decode_packet1imm26_value (decode_packet1imm26_value[25:0]),
	 .decode_packet1imm5_value (decode_packet1imm5_value[4:0]),
	 .decode_packet1op_FU_type (decode_packet1op_FU_type[3:0]),
	 .decode_packet1alu_op (decode_packet1alu_op[4:0]),
	 .decode_packet1fpu_op (decode_packet1fpu_op[4:0]),
	 .decode_packet1op_MEM_type (decode_packet1op_MEM_type[3:0]),
	 .decode_packet1op_BR_type (decode_packet1op_BR_type[1:0]),
	 .decode_packet1shift_type (decode_packet1shift_type[1:0]),
	 .decode_packet1compare_type (decode_packet1compare_type[2:0]),
	 .decode_packet1is_imm26 ( decode_packet1is_imm26),
	 .decode_packet1is_imm ( decode_packet1is_imm),
	 .decode_packet1is_imm5 ( decode_packet1is_imm5),
	 .decode_packet1is_signed ( decode_packet1is_signed),
	 .decode_packet1carry_in ( decode_packet1carry_in),
	 .decode_packet1carry_out ( decode_packet1carry_out),
	 .decode_packet1has_sreg_t ( decode_packet1has_sreg_t),
	 .decode_packet1has_sreg_s ( decode_packet1has_sreg_s),
	 .decode_packet1has_dreg ( decode_packet1has_dreg),
	 .decode_packet1is_prefetch ( decode_packet1is_prefetch),
	 .decode_packet1sprf_dest ( decode_packet1sprf_dest),
	 .decode_packet1sprf_src ( decode_packet1sprf_src),
	 .decode_packet1is_atomic ( decode_packet1is_atomic),
	 .decode_packet1is_ldl ( decode_packet1is_ldl),
	 .decode_packet1is_stl ( decode_packet1is_stl),
	 .decode_packet1is_stc ( decode_packet1is_stc),
	 .decode_packet1is_valid ( decode_packet1is_valid),
	 .decode_packet1is_halt ( decode_packet1is_halt),
	 .decode_packet1is_compare ( decode_packet1is_compare),
	 .decode_packet1dreg_is_src ( decode_packet1dreg_is_src),
	 .decode_packet1dreg_is_dest ( decode_packet1dreg_is_dest));
	  reg dst0_vld;
	  reg dst1_vld;
	 assign 
	  dst0_vld = (((( decode_packet0has_dreg &&  issue0) &&  schedule0) && (!  branch_mispredict)) &&  fetch2decodeinstr0_valid);
	 assign 
	  dst1_vld = ((((( decode_packet1has_dreg &&  issue1) &&  schedule1) && (!  branch_mispredict)) &&  fetch2decodeinstr1_valid) && (!  follows_vld_branch));
	  scoreboard SB (
	 .clk ( clk),
	 .reset ( reset),
	 .src0_addr ( decode_packet0sreg_s),
	 .src1_addr ( decode_packet0sreg_t),
	 .src2_addr ( decode_packet1sreg_s),
	 .src3_addr ( decode_packet1sreg_t),
	 .dst0_addr ( decode_packet0dreg),
	 .dst0_vld ( dst0_vld),
	 .dst1_addr ( decode_packet1dreg),
	 .dst1_vld ( dst1_vld),
	 .wb0_addr ( write_addr_0),
	 .wb0_vld ( write_enable_0),
	 .wb1_addr ( write_addr_1),
	 .wb1_vld ( write_enable_1),
	 .src0_rdy ( sreg_s_0_rdy),
	 .src1_rdy ( sreg_t_0_rdy),
	 .src2_rdy ( sreg_s_1_rdy),
	 .src3_rdy ( sreg_t_1_rdy),
	 .dst0_rdy ( dreg_0_rdy),
	 .dst1_rdy ( dreg_1_rdy));
	  rf_4r2w RF (
	 .clk ( clk),
	 .reset ( reset),
	 .enable (1'b1),
	 .write_addr_0 ( write_addr_0),
	 .write_addr_1 ( write_addr_1),
	 .write_data_0 ( write_data_0),
	 .write_data_1 ( write_data_1),
	 .write_enable_0 ( write_enable_0),
	 .write_enable_1 ( write_enable_1),
	 .read_addr_0 ( decode_packet0sreg_t),
	 .read_addr_1 ( decode_packet0sreg_s),
	 .read_addr_2 ( decode_packet1sreg_t),
	 .read_addr_3 ( decode_packet1sreg_s),
	 .read_enable_0 ( decode_packet0has_sreg_t),
	 .read_enable_1 ( decode_packet0has_sreg_s),
	 .read_enable_2 ( decode_packet1has_sreg_t),
	 .read_enable_3 ( decode_packet1has_sreg_s),
	 .read_data_0 ( sreg_t_data0),
	 .read_data_1 ( sreg_s_data0),
	 .read_data_2 ( sreg_t_data1),
	 .read_data_3 ( sreg_s_data1));
	  sprf SPRF (
	 .clk ( clk),
	 .reset ( reset),
	 .read_addr ( sprf_sreg_addr),
	 .read_enable ( sprf_src),
	 .write_enable ( sprf_dest),
	 .read_data ( sprf_sreg_data));
	  bypass_select0 BP_SELECT_S_0 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_s_data0),
	 .address_in_rf ( decode_packet0sreg_s),
	 .data_out ( sreg_s_0),
	 .src_is_bp ( sreg_s_0_is_bp));
	  bypass_select1 BP_SELECT_S_1 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_s_data1),
	 .address_in_rf ( decode_packet1sreg_s),
	 .data_out ( sreg_s_1),
	 .src_is_bp ( sreg_s_1_is_bp));
	  bypass_select2 BP_SELECT_T_0 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_t_data0),
	 .address_in_rf ( decode_packet0sreg_t),
	 .data_out ( sreg_t_0),
	 .src_is_bp ( sreg_t_0_is_bp));
	  bypass_select3 BP_SELECT_T_1 (
	 .data_in_bpaddress_0 ( data_in_bpaddress_0),
	 .data_in_bpdata_0 ( data_in_bpdata_0),
	 .data_in_bpvalid_0 ( data_in_bpvalid_0),
	 .data_in_bpaddress_1 ( data_in_bpaddress_1),
	 .data_in_bpdata_1 ( data_in_bpdata_1),
	 .data_in_bpvalid_1 ( data_in_bpvalid_1),
	 .data_in_bpaddress_2 ( data_in_bpaddress_2),
	 .data_in_bpdata_2 ( data_in_bpdata_2),
	 .data_in_bpvalid_2 ( data_in_bpvalid_2),
	 .data_in_bpaddress_3 ( data_in_bpaddress_3),
	 .data_in_bpdata_3 ( data_in_bpdata_3),
	 .data_in_bpvalid_3 ( data_in_bpvalid_3),
	 .data_in_bpaddress_4 ( data_in_bpaddress_4),
	 .data_in_bpdata_4 ( data_in_bpdata_4),
	 .data_in_bpvalid_4 ( data_in_bpvalid_4),
	 .data_in_bpaddress_5 ( data_in_bpaddress_5),
	 .data_in_bpdata_5 ( data_in_bpdata_5),
	 .data_in_bpvalid_5 ( data_in_bpvalid_5),
	 .data_in_bpaddress_6 ( data_in_bpaddress_6),
	 .data_in_bpdata_6 ( data_in_bpdata_6),
	 .data_in_bpvalid_6 ( data_in_bpvalid_6),
	 .data_in_bpaddress_7 ( data_in_bpaddress_7),
	 .data_in_bpdata_7 ( data_in_bpdata_7),
	 .data_in_bpvalid_7 ( data_in_bpvalid_7),
	 .data_in_bpaddress_8 ( data_in_bpaddress_8),
	 .data_in_bpdata_8 ( data_in_bpdata_8),
	 .data_in_bpvalid_8 ( data_in_bpvalid_8),
	 .data_in_bpaddress_9 ( data_in_bpaddress_9),
	 .data_in_bpdata_9 ( data_in_bpdata_9),
	 .data_in_bpvalid_9 ( data_in_bpvalid_9),
	 .data_in_bpaddress_10 ( data_in_bpaddress_10),
	 .data_in_bpdata_10 ( data_in_bpdata_10),
	 .data_in_bpvalid_10 ( data_in_bpvalid_10),
	 .data_in_rf ( sreg_t_data1),
	 .address_in_rf ( decode_packet1sreg_t),
	 .data_out ( sreg_t_1),
	 .src_is_bp ( sreg_t_1_is_bp));
	 always @ ( fetch2decodeinstr0_valid) or ( fetch2decodevalid) or ( exec_stall) or ( sreg_t_0_is_bp) or ( sreg_t_0_rdy) or ( decode_packet0has_sreg_t) or ( sreg_s_0_is_bp) or ( sreg_s_0_rdy) or ( decode_packet0has_sreg_s) or ( dreg_0_rdy) or ( decode_packet0has_dreg) or ( instr0_issued)
	 begin
	 if((((((( fetch2decodeinstr0_valid &&  fetch2decodevalid) && (!  exec_stall)) && ( (  sreg_t_0_is_bp ||  sreg_t_0_rdy) || (!  decode_packet0has_sreg_t))) && ( (  sreg_s_0_is_bp ||  sreg_s_0_rdy) || (!  decode_packet0has_sreg_s))) && (  dreg_0_rdy || (!  decode_packet0has_dreg))) && (!  instr0_issued))) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	  schedule0 = 1'b1;
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	  schedule0 = 1'b0;
	  end
	 end
	 always @ ( exec_stall) or ( sreg_t_1_is_bp) or ( sreg_t_1_rdy) or ( decode_packet1has_sreg_t) or ( sreg_s_1_is_bp) or ( sreg_s_1_rdy) or ( decode_packet1has_sreg_s) or ( dreg_1_rdy) or ( decode_packet1has_dreg) or ( decode_packet0op_FU_type) or ( decode_packet1op_FU_type) or ( decode_packet0dreg) or ( decode_packet1dreg) or ( decode_packet0has_dreg) or ( decode_packet1sreg_t) or ( decode_packet1sreg_s) or ( fetch2decodeinstr0_valid) or ( fetch2decodeinstr1_valid) or ( issue0) or ( decode_packet1is_halt) or ( instr0_issued) or ( decode_packet1is_valid)
	 if((((((!  exec_stall) && ( (  sreg_t_1_is_bp ||  sreg_t_1_rdy) || (!  decode_packet1has_sreg_t))) && ( (  sreg_s_1_is_bp ||  sreg_s_1_rdy) || (!  decode_packet1has_sreg_s))) && (  dreg_1_rdy || (!  decode_packet1has_dreg))) && ( ( ( (((((((((  decode_packet0op_FU_type !=  decode_packet1op_FU_type) && (  decode_packet0op_FU_type != 4'b1010)) && ( ( (  decode_packet0dreg !=  decode_packet1dreg) || (!  decode_packet0has_dreg)) || (!  decode_packet1has_dreg))) && ( ( (  decode_packet0dreg !=  decode_packet1sreg_t) || (!  decode_packet0has_dreg)) || (!  decode_packet1has_sreg_t))) && ( ( (  decode_packet0dreg !=  decode_packet1sreg_s) || (!  decode_packet0has_dreg)) || (!  decode_packet1has_sreg_s))) &&  fetch2decodeinstr0_valid) &&  fetch2decodeinstr1_valid) &&  issue0) && (!  decode_packet1is_halt)) ||  instr0_issued) || ( issue0 && (!  decode_packet1is_valid))) || (!  fetch2decodeinstr0_valid)))) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	  schedule1 = 1'b1;
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	  schedule1 = 1'b0;
	  end
	 always @ ( schedule0) or ( decode_packet0op_FU_type) or ( decode_packet0is_valid) or ( stall_in_0) or ( decode_packet1is_valid)
	 if((( schedule0 && ( ( (  decode_packet0op_FU_type == 4'b0001) || (  decode_packet0op_FU_type == 4'b0110)) || (  decode_packet0op_FU_type == 4'b1010))) &&  decode_packet0is_valid)) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  valid_int = (!  stall_in_0);
	  choice_int = 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 begin
	  choice_int = 1'b1;
	  valid_int = ((( schedule1 && ( ( (  decode_packet1op_FU_type == 4'b0001) || (  decode_packet1op_FU_type == 4'b0110)) || (  decode_packet1op_FU_type == 4'b1010))) &&  decode_packet1is_valid) && (!  stall_in_0));
	 end
	  end
	 always @ ( schedule0) or ( decode_packet0op_FU_type) or ( decode_packet0is_valid) or ( stall_in_2) or ( schedule1) or ( decode_packet1op_FU_type) or ( decode_packet1is_valid)
	 if((( schedule0 && (  decode_packet0op_FU_type == 4'b0011)) &&  decode_packet0is_valid)) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  valid_fpu = (!  stall_in_2);
	  choice_fpu = 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 begin
	  valid_fpu = ((( schedule1 && (  decode_packet1op_FU_type == 4'b0011)) &&  decode_packet1is_valid) && (!  stall_in_2));
	  choice_fpu = 1'b1;
	 end
	  end
	 always @ ( schedule0) or ( decode_packet0op_FU_type) or ( decode_packet0is_valid) or ( stall_in_1) or ( schedule1) or ( decode_packet1op_FU_type) or ( decode_packet1is_valid)
	 if((( schedule0 && (  decode_packet0op_FU_type == 4'b1000)) &&  decode_packet0is_valid)) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	 begin
	  valid_mem = (!  stall_in_1);
	  choice_mem = 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	 begin
	  valid_mem = ((( schedule1 && (  decode_packet1op_FU_type == 4'b1000)) &&  decode_packet1is_valid) && (!  stall_in_1));
	  choice_mem = 1'b1;
	 end
	  end
	 assign 
	  issue0 = ( fetch2decodeinstr0_valid && ( ( ( (!  decode_packet0is_valid) || ( valid_mem && (!  choice_mem))) || ( valid_fpu && (!  choice_fpu))) || ( valid_int && (!  choice_int))));
	 assign 
	  issue1 = ( fetch2decodeinstr1_valid && ( ( ( (( (  issue0 ||  instr0_issued) || (!  fetch2decodeinstr0_valid)) && (!  decode_packet1is_valid)) || ( valid_mem &&  choice_mem)) || ( valid_fpu &&  choice_fpu)) || ( valid_int &&  choice_int)));
	 always @ (posedge  clk) or (posedge  reset)
	 if( reset) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  decode2intdecode_packetis_halt <= 1'b0;
	  decode2fpudecode_packetis_halt <= 1'b0;
	  decode2memdecode_packetis_halt <= 1'b0;
	  instr0_issued <= 1'b0;
	  decode2intvalid <= 1'b0;
	  decode2fpuvalid <= 1'b0;
	  decode2memvalid <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 begin
	 if( exec_stall) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  decode2intpc <=  decode2intpc;
	  decode2intpc_incr <=  decode2intpc_incr;
	  decode2intthread_num <=  decode2intthread_num;
	  decode2intbranch_taken <=  decode2intbranch_taken;
	  decode2intpredicted_pc <=  decode2intpredicted_pc;
	  decode2intvalid <=  decode2intvalid;
	  decode2intis_oldest <=  decode2intis_oldest;
	  decode2intsreg_t_data <=  decode2intsreg_t_data;
	  decode2intsreg_s_data <=  decode2intsreg_s_data;
	  decode2intresult <=  decode2intresult;
	  decode2intL1D_hit <=  decode2intL1D_hit;
	  decode2intdecode_packetsreg_t <=  decode2intdecode_packetsreg_t;
	  decode2intdecode_packetsreg_s <=  decode2intdecode_packetsreg_s;
	  decode2intdecode_packetdreg <=  decode2intdecode_packetdreg;
	  decode2intdecode_packetacc <=  decode2intdecode_packetacc;
	  decode2intdecode_packetimm16_value <=  decode2intdecode_packetimm16_value;
	  decode2intdecode_packetimm26_value <=  decode2intdecode_packetimm26_value;
	  decode2intdecode_packetimm5_value <=  decode2intdecode_packetimm5_value;
	  decode2intdecode_packetop_FU_type <=  decode2intdecode_packetop_FU_type;
	  decode2intdecode_packetalu_op <=  decode2intdecode_packetalu_op;
	  decode2intdecode_packetfpu_op <=  decode2intdecode_packetfpu_op;
	  decode2intdecode_packetop_MEM_type <=  decode2intdecode_packetop_MEM_type;
	  decode2intdecode_packetop_BR_type <=  decode2intdecode_packetop_BR_type;
	  decode2intdecode_packetshift_type <=  decode2intdecode_packetshift_type;
	  decode2intdecode_packetcompare_type <=  decode2intdecode_packetcompare_type;
	  decode2intdecode_packetis_imm26 <=  decode2intdecode_packetis_imm26;
	  decode2intdecode_packetis_imm <=  decode2intdecode_packetis_imm;
	  decode2intdecode_packetis_imm5 <=  decode2intdecode_packetis_imm5;
	  decode2intdecode_packetis_signed <=  decode2intdecode_packetis_signed;
	  decode2intdecode_packetcarry_in <=  decode2intdecode_packetcarry_in;
	  decode2intdecode_packetcarry_out <=  decode2intdecode_packetcarry_out;
	  decode2intdecode_packethas_sreg_t <=  decode2intdecode_packethas_sreg_t;
	  decode2intdecode_packethas_sreg_s <=  decode2intdecode_packethas_sreg_s;
	  decode2intdecode_packethas_dreg <=  decode2intdecode_packethas_dreg;
	  decode2intdecode_packetis_prefetch <=  decode2intdecode_packetis_prefetch;
	  decode2intdecode_packetsprf_dest <=  decode2intdecode_packetsprf_dest;
	  decode2intdecode_packetsprf_src <=  decode2intdecode_packetsprf_src;
	  decode2intdecode_packetis_atomic <=  decode2intdecode_packetis_atomic;
	  decode2intdecode_packetis_ldl <=  decode2intdecode_packetis_ldl;
	  decode2intdecode_packetis_stl <=  decode2intdecode_packetis_stl;
	  decode2intdecode_packetis_stc <=  decode2intdecode_packetis_stc;
	  decode2intdecode_packetis_valid <=  decode2intdecode_packetis_valid;
	  decode2intdecode_packetis_halt <=  decode2intdecode_packetis_halt;
	  decode2intdecode_packetis_compare <=  decode2intdecode_packetis_compare;
	  decode2intdecode_packetdreg_is_src <=  decode2intdecode_packetdreg_is_src;
	  decode2intdecode_packetdreg_is_dest <=  decode2intdecode_packetdreg_is_dest;
	  decode2fpupc <=  decode2fpupc;
	  decode2fpupc_incr <=  decode2fpupc_incr;
	  decode2fputhread_num <=  decode2fputhread_num;
	  decode2fpubranch_taken <=  decode2fpubranch_taken;
	  decode2fpupredicted_pc <=  decode2fpupredicted_pc;
	  decode2fpuvalid <=  decode2fpuvalid;
	  decode2fpuis_oldest <=  decode2fpuis_oldest;
	  decode2fpusreg_t_data <=  decode2fpusreg_t_data;
	  decode2fpusreg_s_data <=  decode2fpusreg_s_data;
	  decode2fpuresult <=  decode2fpuresult;
	  decode2fpuL1D_hit <=  decode2fpuL1D_hit;
	  decode2fpudecode_packetsreg_t <=  decode2fpudecode_packetsreg_t;
	  decode2fpudecode_packetsreg_s <=  decode2fpudecode_packetsreg_s;
	  decode2fpudecode_packetdreg <=  decode2fpudecode_packetdreg;
	  decode2fpudecode_packetacc <=  decode2fpudecode_packetacc;
	  decode2fpudecode_packetimm16_value <=  decode2fpudecode_packetimm16_value;
	  decode2fpudecode_packetimm26_value <=  decode2fpudecode_packetimm26_value;
	  decode2fpudecode_packetimm5_value <=  decode2fpudecode_packetimm5_value;
	  decode2fpudecode_packetop_FU_type <=  decode2fpudecode_packetop_FU_type;
	  decode2fpudecode_packetalu_op <=  decode2fpudecode_packetalu_op;
	  decode2fpudecode_packetfpu_op <=  decode2fpudecode_packetfpu_op;
	  decode2fpudecode_packetop_MEM_type <=  decode2fpudecode_packetop_MEM_type;
	  decode2fpudecode_packetop_BR_type <=  decode2fpudecode_packetop_BR_type;
	  decode2fpudecode_packetshift_type <=  decode2fpudecode_packetshift_type;
	  decode2fpudecode_packetcompare_type <=  decode2fpudecode_packetcompare_type;
	  decode2fpudecode_packetis_imm26 <=  decode2fpudecode_packetis_imm26;
	  decode2fpudecode_packetis_imm <=  decode2fpudecode_packetis_imm;
	  decode2fpudecode_packetis_imm5 <=  decode2fpudecode_packetis_imm5;
	  decode2fpudecode_packetis_signed <=  decode2fpudecode_packetis_signed;
	  decode2fpudecode_packetcarry_in <=  decode2fpudecode_packetcarry_in;
	  decode2fpudecode_packetcarry_out <=  decode2fpudecode_packetcarry_out;
	  decode2fpudecode_packethas_sreg_t <=  decode2fpudecode_packethas_sreg_t;
	  decode2fpudecode_packethas_sreg_s <=  decode2fpudecode_packethas_sreg_s;
	  decode2fpudecode_packethas_dreg <=  decode2fpudecode_packethas_dreg;
	  decode2fpudecode_packetis_prefetch <=  decode2fpudecode_packetis_prefetch;
	  decode2fpudecode_packetsprf_dest <=  decode2fpudecode_packetsprf_dest;
	  decode2fpudecode_packetsprf_src <=  decode2fpudecode_packetsprf_src;
	  decode2fpudecode_packetis_atomic <=  decode2fpudecode_packetis_atomic;
	  decode2fpudecode_packetis_ldl <=  decode2fpudecode_packetis_ldl;
	  decode2fpudecode_packetis_stl <=  decode2fpudecode_packetis_stl;
	  decode2fpudecode_packetis_stc <=  decode2fpudecode_packetis_stc;
	  decode2fpudecode_packetis_valid <=  decode2fpudecode_packetis_valid;
	  decode2fpudecode_packetis_halt <=  decode2fpudecode_packetis_halt;
	  decode2fpudecode_packetis_compare <=  decode2fpudecode_packetis_compare;
	  decode2fpudecode_packetdreg_is_src <=  decode2fpudecode_packetdreg_is_src;
	  decode2fpudecode_packetdreg_is_dest <=  decode2fpudecode_packetdreg_is_dest;
	  decode2mempc <=  decode2mempc;
	  decode2mempc_incr <=  decode2mempc_incr;
	  decode2memthread_num <=  decode2memthread_num;
	  decode2membranch_taken <=  decode2membranch_taken;
	  decode2mempredicted_pc <=  decode2mempredicted_pc;
	  decode2memvalid <=  decode2memvalid;
	  decode2memis_oldest <=  decode2memis_oldest;
	  decode2memsreg_t_data <=  decode2memsreg_t_data;
	  decode2memsreg_s_data <=  decode2memsreg_s_data;
	  decode2memresult <=  decode2memresult;
	  decode2memL1D_hit <=  decode2memL1D_hit;
	  decode2memdecode_packetsreg_t <=  decode2memdecode_packetsreg_t;
	  decode2memdecode_packetsreg_s <=  decode2memdecode_packetsreg_s;
	  decode2memdecode_packetdreg <=  decode2memdecode_packetdreg;
	  decode2memdecode_packetacc <=  decode2memdecode_packetacc;
	  decode2memdecode_packetimm16_value <=  decode2memdecode_packetimm16_value;
	  decode2memdecode_packetimm26_value <=  decode2memdecode_packetimm26_value;
	  decode2memdecode_packetimm5_value <=  decode2memdecode_packetimm5_value;
	  decode2memdecode_packetop_FU_type <=  decode2memdecode_packetop_FU_type;
	  decode2memdecode_packetalu_op <=  decode2memdecode_packetalu_op;
	  decode2memdecode_packetfpu_op <=  decode2memdecode_packetfpu_op;
	  decode2memdecode_packetop_MEM_type <=  decode2memdecode_packetop_MEM_type;
	  decode2memdecode_packetop_BR_type <=  decode2memdecode_packetop_BR_type;
	  decode2memdecode_packetshift_type <=  decode2memdecode_packetshift_type;
	  decode2memdecode_packetcompare_type <=  decode2memdecode_packetcompare_type;
	  decode2memdecode_packetis_imm26 <=  decode2memdecode_packetis_imm26;
	  decode2memdecode_packetis_imm <=  decode2memdecode_packetis_imm;
	  decode2memdecode_packetis_imm5 <=  decode2memdecode_packetis_imm5;
	  decode2memdecode_packetis_signed <=  decode2memdecode_packetis_signed;
	  decode2memdecode_packetcarry_in <=  decode2memdecode_packetcarry_in;
	  decode2memdecode_packetcarry_out <=  decode2memdecode_packetcarry_out;
	  decode2memdecode_packethas_sreg_t <=  decode2memdecode_packethas_sreg_t;
	  decode2memdecode_packethas_sreg_s <=  decode2memdecode_packethas_sreg_s;
	  decode2memdecode_packethas_dreg <=  decode2memdecode_packethas_dreg;
	  decode2memdecode_packetis_prefetch <=  decode2memdecode_packetis_prefetch;
	  decode2memdecode_packetsprf_dest <=  decode2memdecode_packetsprf_dest;
	  decode2memdecode_packetsprf_src <=  decode2memdecode_packetsprf_src;
	  decode2memdecode_packetis_atomic <=  decode2memdecode_packetis_atomic;
	  decode2memdecode_packetis_ldl <=  decode2memdecode_packetis_ldl;
	  decode2memdecode_packetis_stl <=  decode2memdecode_packetis_stl;
	  decode2memdecode_packetis_stc <=  decode2memdecode_packetis_stc;
	  decode2memdecode_packetis_valid <=  decode2memdecode_packetis_valid;
	  decode2memdecode_packetis_halt <=  decode2memdecode_packetis_halt;
	  decode2memdecode_packetis_compare <=  decode2memdecode_packetis_compare;
	  decode2memdecode_packetdreg_is_src <=  decode2memdecode_packetdreg_is_src;
	  decode2memdecode_packetdreg_is_dest <=  decode2memdecode_packetdreg_is_dest;
	  instr0_issued <=  instr0_issued;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 begin
	  decode2intvalid <= ((( valid_int && (!  issue_halt)) && (!  branch_mispredict)) &&  fetch2decodevalid);
	  decode2fpuvalid <= (((( valid_fpu && (!  issue_halt)) && (!  branch_mispredict)) &&  fetch2decodevalid) && (!  follows_vld_branch));
	  decode2memvalid <= (((( valid_mem && (!  issue_halt)) && (!  branch_mispredict)) &&  fetch2decodevalid) && (!  follows_vld_branch));
	  decode2intbranch_taken <=  fetch2decodebranch_taken;
	  decode2intpredicted_pc <=  fetch2decodepredicted_pc;
	 if( choice_int) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  decode2intdecode_packetsreg_t <=  decode_packet1sreg_t;
	  decode2intdecode_packetsreg_s <=  decode_packet1sreg_s;
	  decode2intdecode_packetdreg <=  decode_packet1dreg;
	  decode2intdecode_packetacc <=  decode_packet1acc;
	  decode2intdecode_packetimm16_value <=  decode_packet1imm16_value;
	  decode2intdecode_packetimm26_value <=  decode_packet1imm26_value;
	  decode2intdecode_packetimm5_value <=  decode_packet1imm5_value;
	  decode2intdecode_packetop_FU_type <=  decode_packet1op_FU_type;
	  decode2intdecode_packetalu_op <=  decode_packet1alu_op;
	  decode2intdecode_packetfpu_op <=  decode_packet1fpu_op;
	  decode2intdecode_packetop_MEM_type <=  decode_packet1op_MEM_type;
	  decode2intdecode_packetop_BR_type <=  decode_packet1op_BR_type;
	  decode2intdecode_packetshift_type <=  decode_packet1shift_type;
	  decode2intdecode_packetcompare_type <=  decode_packet1compare_type;
	  decode2intdecode_packetis_imm26 <=  decode_packet1is_imm26;
	  decode2intdecode_packetis_imm <=  decode_packet1is_imm;
	  decode2intdecode_packetis_imm5 <=  decode_packet1is_imm5;
	  decode2intdecode_packetis_signed <=  decode_packet1is_signed;
	  decode2intdecode_packetcarry_in <=  decode_packet1carry_in;
	  decode2intdecode_packetcarry_out <=  decode_packet1carry_out;
	  decode2intdecode_packethas_sreg_t <=  decode_packet1has_sreg_t;
	  decode2intdecode_packethas_sreg_s <=  decode_packet1has_sreg_s;
	  decode2intdecode_packethas_dreg <=  decode_packet1has_dreg;
	  decode2intdecode_packetis_prefetch <=  decode_packet1is_prefetch;
	  decode2intdecode_packetsprf_dest <=  decode_packet1sprf_dest;
	  decode2intdecode_packetsprf_src <=  decode_packet1sprf_src;
	  decode2intdecode_packetis_atomic <=  decode_packet1is_atomic;
	  decode2intdecode_packetis_ldl <=  decode_packet1is_ldl;
	  decode2intdecode_packetis_stl <=  decode_packet1is_stl;
	  decode2intdecode_packetis_stc <=  decode_packet1is_stc;
	  decode2intdecode_packetis_valid <=  decode_packet1is_valid;
	  decode2intdecode_packetis_halt <=  decode_packet1is_halt;
	  decode2intdecode_packetis_compare <=  decode_packet1is_compare;
	  decode2intdecode_packetdreg_is_src <=  decode_packet1dreg_is_src;
	  decode2intdecode_packetdreg_is_dest <=  decode_packet1dreg_is_dest;
	 if( sprf_src) 
	  begin 
	 branvar[15] <= branvar[15] + 1; 
	 begin
	  decode2intsreg_t_data <=  sprf_sreg_data;
	 end
	  end
	 else 
	   begin 
	   branvar[16] <= branvar[16] + 1;   
	 begin
	  decode2intsreg_t_data <=  sreg_t_1;
	 end
	  end
	  decode2intsreg_s_data <=  sreg_s_1;
	  decode2intpc <=  fetch2decodepc1;
	  decode2intpc_incr <=  fetch2decodepc1_incr;
	  decode2intis_oldest <= (!  issue0);
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 begin
	  decode2intdecode_packetsreg_t <=  decode_packet0sreg_t;
	  decode2intdecode_packetsreg_s <=  decode_packet0sreg_s;
	  decode2intdecode_packetdreg <=  decode_packet0dreg;
	  decode2intdecode_packetacc <=  decode_packet0acc;
	  decode2intdecode_packetimm16_value <=  decode_packet0imm16_value;
	  decode2intdecode_packetimm26_value <=  decode_packet0imm26_value;
	  decode2intdecode_packetimm5_value <=  decode_packet0imm5_value;
	  decode2intdecode_packetop_FU_type <=  decode_packet0op_FU_type;
	  decode2intdecode_packetalu_op <=  decode_packet0alu_op;
	  decode2intdecode_packetfpu_op <=  decode_packet0fpu_op;
	  decode2intdecode_packetop_MEM_type <=  decode_packet0op_MEM_type;
	  decode2intdecode_packetop_BR_type <=  decode_packet0op_BR_type;
	  decode2intdecode_packetshift_type <=  decode_packet0shift_type;
	  decode2intdecode_packetcompare_type <=  decode_packet0compare_type;
	  decode2intdecode_packetis_imm26 <=  decode_packet0is_imm26;
	  decode2intdecode_packetis_imm <=  decode_packet0is_imm;
	  decode2intdecode_packetis_imm5 <=  decode_packet0is_imm5;
	  decode2intdecode_packetis_signed <=  decode_packet0is_signed;
	  decode2intdecode_packetcarry_in <=  decode_packet0carry_in;
	  decode2intdecode_packetcarry_out <=  decode_packet0carry_out;
	  decode2intdecode_packethas_sreg_t <=  decode_packet0has_sreg_t;
	  decode2intdecode_packethas_sreg_s <=  decode_packet0has_sreg_s;
	  decode2intdecode_packethas_dreg <=  decode_packet0has_dreg;
	  decode2intdecode_packetis_prefetch <=  decode_packet0is_prefetch;
	  decode2intdecode_packetsprf_dest <=  decode_packet0sprf_dest;
	  decode2intdecode_packetsprf_src <=  decode_packet0sprf_src;
	  decode2intdecode_packetis_atomic <=  decode_packet0is_atomic;
	  decode2intdecode_packetis_ldl <=  decode_packet0is_ldl;
	  decode2intdecode_packetis_stl <=  decode_packet0is_stl;
	  decode2intdecode_packetis_stc <=  decode_packet0is_stc;
	  decode2intdecode_packetis_valid <=  decode_packet0is_valid;
	  decode2intdecode_packetis_halt <=  decode_packet0is_halt;
	  decode2intdecode_packetis_compare <=  decode_packet0is_compare;
	  decode2intdecode_packetdreg_is_src <=  decode_packet0dreg_is_src;
	  decode2intdecode_packetdreg_is_dest <=  decode_packet0dreg_is_dest;
	 if( sprf_src) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  decode2intsreg_t_data <=  sprf_sreg_data;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 begin
	  decode2intsreg_t_data <=  sreg_t_0;
	 end
	  end
	  decode2intsreg_s_data <=  sreg_s_0;
	  decode2intpc <=  fetch2decodepc0;
	  decode2intpc_incr <=  fetch2decodepc0_incr;
	  decode2intis_oldest <= 1'b1;
	 end
	  end
	 if( choice_fpu) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  decode2fpudecode_packetsreg_t <=  decode_packet1sreg_t;
	  decode2fpudecode_packetsreg_s <=  decode_packet1sreg_s;
	  decode2fpudecode_packetdreg <=  decode_packet1dreg;
	  decode2fpudecode_packetacc <=  decode_packet1acc;
	  decode2fpudecode_packetimm16_value <=  decode_packet1imm16_value;
	  decode2fpudecode_packetimm26_value <=  decode_packet1imm26_value;
	  decode2fpudecode_packetimm5_value <=  decode_packet1imm5_value;
	  decode2fpudecode_packetop_FU_type <=  decode_packet1op_FU_type;
	  decode2fpudecode_packetalu_op <=  decode_packet1alu_op;
	  decode2fpudecode_packetfpu_op <=  decode_packet1fpu_op;
	  decode2fpudecode_packetop_MEM_type <=  decode_packet1op_MEM_type;
	  decode2fpudecode_packetop_BR_type <=  decode_packet1op_BR_type;
	  decode2fpudecode_packetshift_type <=  decode_packet1shift_type;
	  decode2fpudecode_packetcompare_type <=  decode_packet1compare_type;
	  decode2fpudecode_packetis_imm26 <=  decode_packet1is_imm26;
	  decode2fpudecode_packetis_imm <=  decode_packet1is_imm;
	  decode2fpudecode_packetis_imm5 <=  decode_packet1is_imm5;
	  decode2fpudecode_packetis_signed <=  decode_packet1is_signed;
	  decode2fpudecode_packetcarry_in <=  decode_packet1carry_in;
	  decode2fpudecode_packetcarry_out <=  decode_packet1carry_out;
	  decode2fpudecode_packethas_sreg_t <=  decode_packet1has_sreg_t;
	  decode2fpudecode_packethas_sreg_s <=  decode_packet1has_sreg_s;
	  decode2fpudecode_packethas_dreg <=  decode_packet1has_dreg;
	  decode2fpudecode_packetis_prefetch <=  decode_packet1is_prefetch;
	  decode2fpudecode_packetsprf_dest <=  decode_packet1sprf_dest;
	  decode2fpudecode_packetsprf_src <=  decode_packet1sprf_src;
	  decode2fpudecode_packetis_atomic <=  decode_packet1is_atomic;
	  decode2fpudecode_packetis_ldl <=  decode_packet1is_ldl;
	  decode2fpudecode_packetis_stl <=  decode_packet1is_stl;
	  decode2fpudecode_packetis_stc <=  decode_packet1is_stc;
	  decode2fpudecode_packetis_valid <=  decode_packet1is_valid;
	  decode2fpudecode_packetis_halt <=  decode_packet1is_halt;
	  decode2fpudecode_packetis_compare <=  decode_packet1is_compare;
	  decode2fpudecode_packetdreg_is_src <=  decode_packet1dreg_is_src;
	  decode2fpudecode_packetdreg_is_dest <=  decode_packet1dreg_is_dest;
	  decode2fpusreg_t_data <=  sreg_t_1;
	  decode2fpusreg_s_data <=  sreg_s_1;
	  decode2fpupc <=  fetch2decodepc1;
	  decode2fpupc_incr <=  fetch2decodepc1_incr;
	  decode2fpuis_oldest <= (!  issue0);
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	 begin
	  decode2fpudecode_packetsreg_t <=  decode_packet0sreg_t;
	  decode2fpudecode_packetsreg_s <=  decode_packet0sreg_s;
	  decode2fpudecode_packetdreg <=  decode_packet0dreg;
	  decode2fpudecode_packetacc <=  decode_packet0acc;
	  decode2fpudecode_packetimm16_value <=  decode_packet0imm16_value;
	  decode2fpudecode_packetimm26_value <=  decode_packet0imm26_value;
	  decode2fpudecode_packetimm5_value <=  decode_packet0imm5_value;
	  decode2fpudecode_packetop_FU_type <=  decode_packet0op_FU_type;
	  decode2fpudecode_packetalu_op <=  decode_packet0alu_op;
	  decode2fpudecode_packetfpu_op <=  decode_packet0fpu_op;
	  decode2fpudecode_packetop_MEM_type <=  decode_packet0op_MEM_type;
	  decode2fpudecode_packetop_BR_type <=  decode_packet0op_BR_type;
	  decode2fpudecode_packetshift_type <=  decode_packet0shift_type;
	  decode2fpudecode_packetcompare_type <=  decode_packet0compare_type;
	  decode2fpudecode_packetis_imm26 <=  decode_packet0is_imm26;
	  decode2fpudecode_packetis_imm <=  decode_packet0is_imm;
	  decode2fpudecode_packetis_imm5 <=  decode_packet0is_imm5;
	  decode2fpudecode_packetis_signed <=  decode_packet0is_signed;
	  decode2fpudecode_packetcarry_in <=  decode_packet0carry_in;
	  decode2fpudecode_packetcarry_out <=  decode_packet0carry_out;
	  decode2fpudecode_packethas_sreg_t <=  decode_packet0has_sreg_t;
	  decode2fpudecode_packethas_sreg_s <=  decode_packet0has_sreg_s;
	  decode2fpudecode_packethas_dreg <=  decode_packet0has_dreg;
	  decode2fpudecode_packetis_prefetch <=  decode_packet0is_prefetch;
	  decode2fpudecode_packetsprf_dest <=  decode_packet0sprf_dest;
	  decode2fpudecode_packetsprf_src <=  decode_packet0sprf_src;
	  decode2fpudecode_packetis_atomic <=  decode_packet0is_atomic;
	  decode2fpudecode_packetis_ldl <=  decode_packet0is_ldl;
	  decode2fpudecode_packetis_stl <=  decode_packet0is_stl;
	  decode2fpudecode_packetis_stc <=  decode_packet0is_stc;
	  decode2fpudecode_packetis_valid <=  decode_packet0is_valid;
	  decode2fpudecode_packetis_halt <=  decode_packet0is_halt;
	  decode2fpudecode_packetis_compare <=  decode_packet0is_compare;
	  decode2fpudecode_packetdreg_is_src <=  decode_packet0dreg_is_src;
	  decode2fpudecode_packetdreg_is_dest <=  decode_packet0dreg_is_dest;
	  decode2fpusreg_t_data <=  sreg_t_0;
	  decode2fpusreg_s_data <=  sreg_s_0;
	  decode2fpupc <=  fetch2decodepc0;
	  decode2fpupc_incr <=  fetch2decodepc0_incr;
	  decode2fpuis_oldest <= 1'b1;
	 end
	  end
	 if( choice_mem) 
	  begin 
	 branvar[22] <= branvar[22] + 1; 
	 begin
	  decode2memdecode_packetsreg_t <=  decode_packet1sreg_t;
	  decode2memdecode_packetsreg_s <=  decode_packet1sreg_s;
	  decode2memdecode_packetdreg <=  decode_packet1dreg;
	  decode2memdecode_packetacc <=  decode_packet1acc;
	  decode2memdecode_packetimm16_value <=  decode_packet1imm16_value;
	  decode2memdecode_packetimm26_value <=  decode_packet1imm26_value;
	  decode2memdecode_packetimm5_value <=  decode_packet1imm5_value;
	  decode2memdecode_packetop_FU_type <=  decode_packet1op_FU_type;
	  decode2memdecode_packetalu_op <=  decode_packet1alu_op;
	  decode2memdecode_packetfpu_op <=  decode_packet1fpu_op;
	  decode2memdecode_packetop_MEM_type <=  decode_packet1op_MEM_type;
	  decode2memdecode_packetop_BR_type <=  decode_packet1op_BR_type;
	  decode2memdecode_packetshift_type <=  decode_packet1shift_type;
	  decode2memdecode_packetcompare_type <=  decode_packet1compare_type;
	  decode2memdecode_packetis_imm26 <=  decode_packet1is_imm26;
	  decode2memdecode_packetis_imm <=  decode_packet1is_imm;
	  decode2memdecode_packetis_imm5 <=  decode_packet1is_imm5;
	  decode2memdecode_packetis_signed <=  decode_packet1is_signed;
	  decode2memdecode_packetcarry_in <=  decode_packet1carry_in;
	  decode2memdecode_packetcarry_out <=  decode_packet1carry_out;
	  decode2memdecode_packethas_sreg_t <=  decode_packet1has_sreg_t;
	  decode2memdecode_packethas_sreg_s <=  decode_packet1has_sreg_s;
	  decode2memdecode_packethas_dreg <=  decode_packet1has_dreg;
	  decode2memdecode_packetis_prefetch <=  decode_packet1is_prefetch;
	  decode2memdecode_packetsprf_dest <=  decode_packet1sprf_dest;
	  decode2memdecode_packetsprf_src <=  decode_packet1sprf_src;
	  decode2memdecode_packetis_atomic <=  decode_packet1is_atomic;
	  decode2memdecode_packetis_ldl <=  decode_packet1is_ldl;
	  decode2memdecode_packetis_stl <=  decode_packet1is_stl;
	  decode2memdecode_packetis_stc <=  decode_packet1is_stc;
	  decode2memdecode_packetis_valid <=  decode_packet1is_valid;
	  decode2memdecode_packetis_halt <=  decode_packet1is_halt;
	  decode2memdecode_packetis_compare <=  decode_packet1is_compare;
	  decode2memdecode_packetdreg_is_src <=  decode_packet1dreg_is_src;
	  decode2memdecode_packetdreg_is_dest <=  decode_packet1dreg_is_dest;
	  decode2memsreg_t_data <=  sreg_t_1;
	  decode2memsreg_s_data <=  sreg_s_1;
	  decode2mempc <=  fetch2decodepc1;
	  decode2mempc_incr <=  fetch2decodepc1_incr;
	  decode2memis_oldest <= (!  issue0);
	 end
	  end
	 else 
	   begin 
	   branvar[23] <= branvar[23] + 1;   
	 begin
	  decode2memdecode_packetsreg_t <=  decode_packet0sreg_t;
	  decode2memdecode_packetsreg_s <=  decode_packet0sreg_s;
	  decode2memdecode_packetdreg <=  decode_packet0dreg;
	  decode2memdecode_packetacc <=  decode_packet0acc;
	  decode2memdecode_packetimm16_value <=  decode_packet0imm16_value;
	  decode2memdecode_packetimm26_value <=  decode_packet0imm26_value;
	  decode2memdecode_packetimm5_value <=  decode_packet0imm5_value;
	  decode2memdecode_packetop_FU_type <=  decode_packet0op_FU_type;
	  decode2memdecode_packetalu_op <=  decode_packet0alu_op;
	  decode2memdecode_packetfpu_op <=  decode_packet0fpu_op;
	  decode2memdecode_packetop_MEM_type <=  decode_packet0op_MEM_type;
	  decode2memdecode_packetop_BR_type <=  decode_packet0op_BR_type;
	  decode2memdecode_packetshift_type <=  decode_packet0shift_type;
	  decode2memdecode_packetcompare_type <=  decode_packet0compare_type;
	  decode2memdecode_packetis_imm26 <=  decode_packet0is_imm26;
	  decode2memdecode_packetis_imm <=  decode_packet0is_imm;
	  decode2memdecode_packetis_imm5 <=  decode_packet0is_imm5;
	  decode2memdecode_packetis_signed <=  decode_packet0is_signed;
	  decode2memdecode_packetcarry_in <=  decode_packet0carry_in;
	  decode2memdecode_packetcarry_out <=  decode_packet0carry_out;
	  decode2memdecode_packethas_sreg_t <=  decode_packet0has_sreg_t;
	  decode2memdecode_packethas_sreg_s <=  decode_packet0has_sreg_s;
	  decode2memdecode_packethas_dreg <=  decode_packet0has_dreg;
	  decode2memdecode_packetis_prefetch <=  decode_packet0is_prefetch;
	  decode2memdecode_packetsprf_dest <=  decode_packet0sprf_dest;
	  decode2memdecode_packetsprf_src <=  decode_packet0sprf_src;
	  decode2memdecode_packetis_atomic <=  decode_packet0is_atomic;
	  decode2memdecode_packetis_ldl <=  decode_packet0is_ldl;
	  decode2memdecode_packetis_stl <=  decode_packet0is_stl;
	  decode2memdecode_packetis_stc <=  decode_packet0is_stc;
	  decode2memdecode_packetis_valid <=  decode_packet0is_valid;
	  decode2memdecode_packetis_halt <=  decode_packet0is_halt;
	  decode2memdecode_packetis_compare <=  decode_packet0is_compare;
	  decode2memdecode_packetdreg_is_src <=  decode_packet0dreg_is_src;
	  decode2memdecode_packetdreg_is_dest <=  decode_packet0dreg_is_dest;
	  decode2memsreg_t_data <=  sreg_t_0;
	  decode2memsreg_s_data <=  sreg_s_0;
	  decode2mempc <=  fetch2decodepc0;
	  decode2mempc_incr <=  fetch2decodepc0_incr;
	  decode2memis_oldest <= 1'b1;
	 end
	  end
	  decode2intdecode_packetis_halt <= ( issue_halt && (!  branch_mispredict));
	  decode2memdecode_packetis_halt <= ( issue_halt && (!  branch_mispredict));
	  decode2fpudecode_packetis_halt <= ( issue_halt && (!  branch_mispredict));
	 if((!  issue1)) 
	  begin 
	 branvar[24] <= branvar[24] + 1; 
	 begin
	  instr0_issued <= ((!  branch_mispredict) && (  instr0_issued ||  issue0));
	 end
	  end
	 else 
	   begin 
	   branvar[25] <= branvar[25] + 1;   
	 begin
	  instr0_issued <= 1'b0;
	 end
	  end
	 end
	  end
	 end
	  end
	  endmodule 

module decoder0 (fetch2decodeinstr1_out , decode_packet1sreg_t , decode_packet1sreg_s , decode_packet1dreg , decode_packet1acc , decode_packet1imm16_value , decode_packet1imm26_value , decode_packet1imm5_value , decode_packet1op_FU_type , decode_packet1alu_op , decode_packet1fpu_op , decode_packet1op_MEM_type , decode_packet1op_BR_type , decode_packet1shift_type , decode_packet1compare_type , decode_packet1is_imm26 , decode_packet1is_imm , decode_packet1is_imm5 , decode_packet1is_signed , decode_packet1carry_in , decode_packet1carry_out , decode_packet1has_sreg_t , decode_packet1has_sreg_s , decode_packet1has_dreg , decode_packet1is_prefetch , decode_packet1sprf_dest , decode_packet1sprf_src , decode_packet1is_atomic , decode_packet1is_ldl , decode_packet1is_stl , decode_packet1is_stc , decode_packet1is_valid , decode_packet1is_halt , decode_packet1is_compare , decode_packet1dreg_is_src , decode_packet1dreg_is_dest );

	  input [31 : 0] fetch2decodeinstr1_out;
	  output [4 : 0] decode_packet1sreg_t;
	  output [4 : 0] decode_packet1sreg_s;
	  output [4 : 0] decode_packet1dreg;
	  output [1 : 0] decode_packet1acc;
	  output [15 : 0] decode_packet1imm16_value;
	  output [25 : 0] decode_packet1imm26_value;
	  output [4 : 0] decode_packet1imm5_value;
	  output [3 : 0] decode_packet1op_FU_type;
	  output [4 : 0] decode_packet1alu_op;
	  output [4 : 0] decode_packet1fpu_op;
	  output [3 : 0] decode_packet1op_MEM_type;
	  output [1 : 0] decode_packet1op_BR_type;
	  output [1 : 0] decode_packet1shift_type;
	  output [2 : 0] decode_packet1compare_type;
	  output decode_packet1is_imm26 ;
	  output decode_packet1is_imm ;
	  output decode_packet1is_imm5 ;
	  output decode_packet1is_signed ;
	  output decode_packet1carry_in ;
	  output decode_packet1carry_out ;
	  output decode_packet1has_sreg_t ;
	  output decode_packet1has_sreg_s ;
	  output decode_packet1has_dreg ;
	  output decode_packet1is_prefetch ;
	  output decode_packet1sprf_dest ;
	  output decode_packet1sprf_src ;
	  output decode_packet1is_atomic ;
	  output decode_packet1is_ldl ;
	  output decode_packet1is_stl ;
	  output decode_packet1is_stc ;
	  output decode_packet1is_valid ;
	  output decode_packet1is_halt ;
	  output decode_packet1is_compare ;
	  output decode_packet1dreg_is_src ;
	  output decode_packet1dreg_is_dest ;
	  wire [3 : 0] decode_optype;
	  wire [1 : 0] decode_opc;
	  wire [1 : 0] decode_opc26;
	  wire [10 : 0] decode_opcode;
	  reg has_sreg_s;
	  reg [7:0] branvar[0:1000];
	  reg has_dreg;
	 assign 
	  decode_optype = fetch2decodeinstr1_out[31:28];
	 assign 
	  decode_packet1dreg = fetch2decodeinstr1_out[27:23];
	 assign 
	  decode_packet1sreg_t = fetch2decodeinstr1_out[22:18];
	 assign 
	  decode_opcode = fetch2decodeinstr1_out[8:0];
	 assign 
	  decode_packet1imm5_value = fetch2decodeinstr1_out[17:13];
	 assign 
	  decode_opc = fetch2decodeinstr1_out[17:16];
	 assign 
	  decode_opc26 = fetch2decodeinstr1_out[27:26];
	 assign 
	  decode_packet1imm16_value = fetch2decodeinstr1_out[15:0];
	 assign 
	  decode_packet1imm26_value = fetch2decodeinstr1_out[25:0];
	 assign 
	  decode_packet1acc = fetch2decodeinstr1_out[12:11];
	 assign 
	  decode_packet1sreg_s = (  decode_packet1dreg_is_src ? fetch2decodeinstr1_out[27:23] : fetch2decodeinstr1_out[17:13]);
	 assign 
	  decode_packet1has_sreg_s = (  has_sreg_s ||  decode_packet1dreg_is_src);
	 assign 
	  decode_packet1has_dreg = ( has_dreg && ( (!  decode_packet1dreg_is_src) ||  decode_packet1dreg_is_dest));
	 always @ ( decode_optype) or ( decode_opcode) or ( fetch2decodeinstr1_out)
	 begin
	 case( decode_optype)
	  4'h0: 
	    begin 
	   branvar[0] <= branvar[0] + 1;
	 begin
	  has_dreg =  fetch2decodeinstr1_out[12];
	  decode_packet1has_sreg_t =  fetch2decodeinstr1_out[11];
	  has_sreg_s =  fetch2decodeinstr1_out[10];
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 =  fetch2decodeinstr1_out[9];
	 case( decode_opcode)
	  9'h0: 
	    begin 
	   branvar[1] <= branvar[1] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b1;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h5: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b1;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h6: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b1;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h7: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b1;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h8: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00100;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h9: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00101;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'ha: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00111;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hb: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00110;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hc: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b01;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hd: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'he: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hf: 
	    begin 
	   branvar[16] <= branvar[16] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b01;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h10: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h11: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h12: 
	    begin 
	   branvar[19] <= branvar[19] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h13: 
	    begin 
	   branvar[20] <= branvar[20] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h14: 
	    begin 
	   branvar[21] <= branvar[21] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h15: 
	    begin 
	   branvar[22] <= branvar[22] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h16: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h17: 
	    begin 
	   branvar[24] <= branvar[24] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h18: 
	    begin 
	   branvar[25] <= branvar[25] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h19: 
	    begin 
	   branvar[26] <= branvar[26] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1a: 
	    begin 
	   branvar[27] <= branvar[27] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1b: 
	    begin 
	   branvar[28] <= branvar[28] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1c: 
	    begin 
	   branvar[29] <= branvar[29] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b1;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1d: 
	    begin 
	   branvar[30] <= branvar[30] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1e: 
	    begin 
	   branvar[31] <= branvar[31] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b01;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1f: 
	    begin 
	   branvar[32] <= branvar[32] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b01;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b1;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h20: 
	    begin 
	   branvar[33] <= branvar[33] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1001;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b1;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h21: 
	    begin 
	   branvar[34] <= branvar[34] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1010;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b1;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h22: 
	    begin 
	   branvar[35] <= branvar[35] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h23: 
	    begin 
	   branvar[36] <= branvar[36] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h24: 
	    begin 
	   branvar[37] <= branvar[37] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h25: 
	    begin 
	   branvar[38] <= branvar[38] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h26: 
	    begin 
	   branvar[39] <= branvar[39] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b1;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h27: 
	    begin 
	   branvar[40] <= branvar[40] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h28: 
	    begin 
	   branvar[41] <= branvar[41] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h29: 
	    begin 
	   branvar[42] <= branvar[42] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2a: 
	    begin 
	   branvar[43] <= branvar[43] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2b: 
	    begin 
	   branvar[44] <= branvar[44] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2c: 
	    begin 
	   branvar[45] <= branvar[45] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2d: 
	    begin 
	   branvar[46] <= branvar[46] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2e: 
	    begin 
	   branvar[47] <= branvar[47] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2f: 
	    begin 
	   branvar[48] <= branvar[48] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h30: 
	    begin 
	   branvar[49] <= branvar[49] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h31: 
	    begin 
	   branvar[50] <= branvar[50] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h32: 
	    begin 
	   branvar[51] <= branvar[51] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h33: 
	    begin 
	   branvar[52] <= branvar[52] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h34: 
	    begin 
	   branvar[53] <= branvar[53] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1100;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h35: 
	    begin 
	   branvar[54] <= branvar[54] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1101;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h36: 
	    begin 
	   branvar[55] <= branvar[55] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1011;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h37: 
	    begin 
	   branvar[56] <= branvar[56] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h38: 
	    begin 
	   branvar[57] <= branvar[57] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h39: 
	    begin 
	   branvar[58] <= branvar[58] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3a: 
	    begin 
	   branvar[59] <= branvar[59] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3b: 
	    begin 
	   branvar[60] <= branvar[60] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3c: 
	    begin 
	   branvar[61] <= branvar[61] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00001;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3d: 
	    begin 
	   branvar[62] <= branvar[62] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00010;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3e: 
	    begin 
	   branvar[63] <= branvar[63] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b01001;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3f: 
	    begin 
	   branvar[64] <= branvar[64] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10010;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h40: 
	    begin 
	   branvar[65] <= branvar[65] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10011;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h41: 
	    begin 
	   branvar[66] <= branvar[66] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b01110;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h42: 
	    begin 
	   branvar[67] <= branvar[67] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10100;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h43: 
	    begin 
	   branvar[68] <= branvar[68] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10100;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h44: 
	    begin 
	   branvar[69] <= branvar[69] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h45: 
	    begin 
	   branvar[70] <= branvar[70] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h46: 
	    begin 
	   branvar[71] <= branvar[71] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10001;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h47: 
	    begin 
	   branvar[72] <= branvar[72] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h48: 
	    begin 
	   branvar[73] <= branvar[73] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h49: 
	    begin 
	   branvar[74] <= branvar[74] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4a: 
	    begin 
	   branvar[75] <= branvar[75] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4b: 
	    begin 
	   branvar[76] <= branvar[76] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4c: 
	    begin 
	   branvar[77] <= branvar[77] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4d: 
	    begin 
	   branvar[78] <= branvar[78] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4e: 
	    begin 
	   branvar[79] <= branvar[79] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4f: 
	    begin 
	   branvar[80] <= branvar[80] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h50: 
	    begin 
	   branvar[81] <= branvar[81] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01011;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h51: 
	    begin 
	   branvar[82] <= branvar[82] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01011;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h52: 
	    begin 
	   branvar[83] <= branvar[83] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h53: 
	    begin 
	   branvar[84] <= branvar[84] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h54: 
	    begin 
	   branvar[85] <= branvar[85] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0101;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h55: 
	    begin 
	   branvar[86] <= branvar[86] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0101;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h56: 
	    begin 
	   branvar[87] <= branvar[87] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0101;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[88] <= branvar[88] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h1: 
	    begin 
	   branvar[89] <= branvar[89] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[90] <= branvar[90] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[91] <= branvar[91] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[92] <= branvar[92] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[93] <= branvar[93] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[94] <= branvar[94] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h2: 
	    begin 
	   branvar[95] <= branvar[95] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[96] <= branvar[96] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00100;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[97] <= branvar[97] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00101;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[98] <= branvar[98] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00111;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[99] <= branvar[99] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[100] <= branvar[100] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h3: 
	    begin 
	   branvar[101] <= branvar[101] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[102] <= branvar[102] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01100;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[103] <= branvar[103] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b1;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[104] <= branvar[104] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[105] <= branvar[105] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[106] <= branvar[106] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h4: 
	    begin 
	   branvar[107] <= branvar[107] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[108] <= branvar[108] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b101;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[109] <= branvar[109] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[110] <= branvar[110] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[111] <= branvar[111] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0001;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[112] <= branvar[112] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h5: 
	    begin 
	   branvar[113] <= branvar[113] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[114] <= branvar[114] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[115] <= branvar[115] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b101;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[116] <= branvar[116] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[117] <= branvar[117] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b001;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[118] <= branvar[118] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h6: 
	    begin 
	   branvar[119] <= branvar[119] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[120] <= branvar[120] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[121] <= branvar[121] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b011;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[122] <= branvar[122] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[123] <= branvar[123] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h7: 
	    begin 
	   branvar[124] <= branvar[124] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b1;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc26)
	  9'h0: 
	    begin 
	   branvar[125] <= branvar[125] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[126] <= branvar[126] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b1;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[127] <= branvar[127] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h8: 
	    begin 
	   branvar[128] <= branvar[128] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[129] <= branvar[129] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[130] <= branvar[130] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[131] <= branvar[131] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[132] <= branvar[132] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[133] <= branvar[133] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h9: 
	    begin 
	   branvar[134] <= branvar[134] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[135] <= branvar[135] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0011;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[136] <= branvar[136] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0010;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[137] <= branvar[137] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0100;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[138] <= branvar[138] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0111;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[139] <= branvar[139] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'ha: 
	    begin 
	   branvar[140] <= branvar[140] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[141] <= branvar[141] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0110;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[142] <= branvar[142] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0101;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b1;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[143] <= branvar[143] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[144] <= branvar[144] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[145] <= branvar[145] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'hb: 
	    begin 
	   branvar[146] <= branvar[146] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b1;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opcode)
	  9'h0: 
	    begin 
	   branvar[147] <= branvar[147] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[148] <= branvar[148] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[149] <= branvar[149] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'hc: 
	    begin 
	   branvar[150] <= branvar[150] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[151] <= branvar[151] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[152] <= branvar[152] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[153] <= branvar[153] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'hd: 
	    begin 
	   branvar[154] <= branvar[154] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[155] <= branvar[155] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[156] <= branvar[156] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  default: 
	    begin 
	 branvar[157] <= branvar[157] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 = 1'b0;
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  endmodule 

module decoder1 (fetch2decodeinstr1_out , decode_packet1sreg_t , decode_packet1sreg_s , decode_packet1dreg , decode_packet1acc , decode_packet1imm16_value , decode_packet1imm26_value , decode_packet1imm5_value , decode_packet1op_FU_type , decode_packet1alu_op , decode_packet1fpu_op , decode_packet1op_MEM_type , decode_packet1op_BR_type , decode_packet1shift_type , decode_packet1compare_type , decode_packet1is_imm26 , decode_packet1is_imm , decode_packet1is_imm5 , decode_packet1is_signed , decode_packet1carry_in , decode_packet1carry_out , decode_packet1has_sreg_t , decode_packet1has_sreg_s , decode_packet1has_dreg , decode_packet1is_prefetch , decode_packet1sprf_dest , decode_packet1sprf_src , decode_packet1is_atomic , decode_packet1is_ldl , decode_packet1is_stl , decode_packet1is_stc , decode_packet1is_valid , decode_packet1is_halt , decode_packet1is_compare , decode_packet1dreg_is_src , decode_packet1dreg_is_dest );

	  input [31 : 0] fetch2decodeinstr1_out;
	  output [4 : 0] decode_packet1sreg_t;
	  output [4 : 0] decode_packet1sreg_s;
	  output [4 : 0] decode_packet1dreg;
	  output [1 : 0] decode_packet1acc;
	  output [15 : 0] decode_packet1imm16_value;
	  output [25 : 0] decode_packet1imm26_value;
	  output [4 : 0] decode_packet1imm5_value;
	  output [3 : 0] decode_packet1op_FU_type;
	  output [4 : 0] decode_packet1alu_op;
	  output [4 : 0] decode_packet1fpu_op;
	  output [3 : 0] decode_packet1op_MEM_type;
	  output [1 : 0] decode_packet1op_BR_type;
	  output [1 : 0] decode_packet1shift_type;
	  output [2 : 0] decode_packet1compare_type;
	  output decode_packet1is_imm26 ;
	  output decode_packet1is_imm ;
	  output decode_packet1is_imm5 ;
	  output decode_packet1is_signed ;
	  output decode_packet1carry_in ;
	  output decode_packet1carry_out ;
	  output decode_packet1has_sreg_t ;
	  output decode_packet1has_sreg_s ;
	  output decode_packet1has_dreg ;
	  output decode_packet1is_prefetch ;
	  output decode_packet1sprf_dest ;
	  output decode_packet1sprf_src ;
	  output decode_packet1is_atomic ;
	  output decode_packet1is_ldl ;
	  output decode_packet1is_stl ;
	  output decode_packet1is_stc ;
	  output decode_packet1is_valid ;
	  output decode_packet1is_halt ;
	  output decode_packet1is_compare ;
	  output decode_packet1dreg_is_src ;
	  output decode_packet1dreg_is_dest ;
	  wire [3 : 0] decode_optype;
	  wire [1 : 0] decode_opc;
	  wire [1 : 0] decode_opc26;
	  wire [10 : 0] decode_opcode;
	  reg has_sreg_s;
	  reg [7:0] branvar[0:100];
	  reg has_dreg;
	 assign 
	  decode_optype = fetch2decodeinstr1_out[31:28];
	 assign 
	  decode_packet1dreg = fetch2decodeinstr1_out[27:23];
	 assign 
	  decode_packet1sreg_t = fetch2decodeinstr1_out[22:18];
	 assign 
	  decode_opcode = fetch2decodeinstr1_out[8:0];
	 assign 
	  decode_packet1imm5_value = fetch2decodeinstr1_out[17:13];
	 assign 
	  decode_opc = fetch2decodeinstr1_out[17:16];
	 assign 
	  decode_opc26 = fetch2decodeinstr1_out[27:26];
	 assign 
	  decode_packet1imm16_value = fetch2decodeinstr1_out[15:0];
	 assign 
	  decode_packet1imm26_value = fetch2decodeinstr1_out[25:0];
	 assign 
	  decode_packet1acc = fetch2decodeinstr1_out[12:11];
	 assign 
	  decode_packet1sreg_s = (  decode_packet1dreg_is_src ? fetch2decodeinstr1_out[27:23] : fetch2decodeinstr1_out[17:13]);
	 assign 
	  decode_packet1has_sreg_s = (  has_sreg_s ||  decode_packet1dreg_is_src);
	 assign 
	  decode_packet1has_dreg = ( has_dreg && ( (!  decode_packet1dreg_is_src) ||  decode_packet1dreg_is_dest));
	 always @ ( decode_optype) or ( fetch2decodeinstr1_out)
	 begin
	 case( decode_optype)
	  4'h0: 
	    begin 
	   branvar[0] <= branvar[0] + 1;
	 begin
	  has_dreg =  fetch2decodeinstr1_out[12];
	  decode_packet1has_sreg_t =  fetch2decodeinstr1_out[11];
	  has_sreg_s =  fetch2decodeinstr1_out[10];
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 =  fetch2decodeinstr1_out[9];
	 case( decode_opcode)
	  9'h0: 
	    begin 
	   branvar[1] <= branvar[1] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b1;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h5: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b1;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h6: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b1;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h7: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b1;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h8: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00100;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h9: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00101;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'ha: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00111;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hb: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00110;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hc: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b01;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hd: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'he: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'hf: 
	    begin 
	   branvar[16] <= branvar[16] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b01;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h10: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h11: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0110;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b10;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h12: 
	    begin 
	   branvar[19] <= branvar[19] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h13: 
	    begin 
	   branvar[20] <= branvar[20] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h14: 
	    begin 
	   branvar[21] <= branvar[21] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h15: 
	    begin 
	   branvar[22] <= branvar[22] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h16: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h17: 
	    begin 
	   branvar[24] <= branvar[24] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h18: 
	    begin 
	   branvar[25] <= branvar[25] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h19: 
	    begin 
	   branvar[26] <= branvar[26] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b1;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1a: 
	    begin 
	   branvar[27] <= branvar[27] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1b: 
	    begin 
	   branvar[28] <= branvar[28] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1c: 
	    begin 
	   branvar[29] <= branvar[29] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b1;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1d: 
	    begin 
	   branvar[30] <= branvar[30] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1e: 
	    begin 
	   branvar[31] <= branvar[31] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b01;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1f: 
	    begin 
	   branvar[32] <= branvar[32] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b01;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b1;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h20: 
	    begin 
	   branvar[33] <= branvar[33] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1001;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b1;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h21: 
	    begin 
	   branvar[34] <= branvar[34] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1010;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b1;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h22: 
	    begin 
	   branvar[35] <= branvar[35] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h23: 
	    begin 
	   branvar[36] <= branvar[36] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h24: 
	    begin 
	   branvar[37] <= branvar[37] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h25: 
	    begin 
	   branvar[38] <= branvar[38] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h26: 
	    begin 
	   branvar[39] <= branvar[39] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b1;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h27: 
	    begin 
	   branvar[40] <= branvar[40] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h28: 
	    begin 
	   branvar[41] <= branvar[41] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h29: 
	    begin 
	   branvar[42] <= branvar[42] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2a: 
	    begin 
	   branvar[43] <= branvar[43] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2b: 
	    begin 
	   branvar[44] <= branvar[44] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2c: 
	    begin 
	   branvar[45] <= branvar[45] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2d: 
	    begin 
	   branvar[46] <= branvar[46] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2e: 
	    begin 
	   branvar[47] <= branvar[47] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2f: 
	    begin 
	   branvar[48] <= branvar[48] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h30: 
	    begin 
	   branvar[49] <= branvar[49] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h31: 
	    begin 
	   branvar[50] <= branvar[50] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h32: 
	    begin 
	   branvar[51] <= branvar[51] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h33: 
	    begin 
	   branvar[52] <= branvar[52] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h34: 
	    begin 
	   branvar[53] <= branvar[53] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1100;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h35: 
	    begin 
	   branvar[54] <= branvar[54] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1101;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h36: 
	    begin 
	   branvar[55] <= branvar[55] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b1011;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h37: 
	    begin 
	   branvar[56] <= branvar[56] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h38: 
	    begin 
	   branvar[57] <= branvar[57] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h39: 
	    begin 
	   branvar[58] <= branvar[58] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3a: 
	    begin 
	   branvar[59] <= branvar[59] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3b: 
	    begin 
	   branvar[60] <= branvar[60] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3c: 
	    begin 
	   branvar[61] <= branvar[61] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00001;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3d: 
	    begin 
	   branvar[62] <= branvar[62] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00010;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3e: 
	    begin 
	   branvar[63] <= branvar[63] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b01001;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3f: 
	    begin 
	   branvar[64] <= branvar[64] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10010;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h40: 
	    begin 
	   branvar[65] <= branvar[65] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10011;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h41: 
	    begin 
	   branvar[66] <= branvar[66] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b01110;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h42: 
	    begin 
	   branvar[67] <= branvar[67] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10100;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h43: 
	    begin 
	   branvar[68] <= branvar[68] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10100;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h44: 
	    begin 
	   branvar[69] <= branvar[69] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h45: 
	    begin 
	   branvar[70] <= branvar[70] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h46: 
	    begin 
	   branvar[71] <= branvar[71] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10001;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h47: 
	    begin 
	   branvar[72] <= branvar[72] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b10000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h48: 
	    begin 
	   branvar[73] <= branvar[73] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h49: 
	    begin 
	   branvar[74] <= branvar[74] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4a: 
	    begin 
	   branvar[75] <= branvar[75] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4b: 
	    begin 
	   branvar[76] <= branvar[76] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4c: 
	    begin 
	   branvar[77] <= branvar[77] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4d: 
	    begin 
	   branvar[78] <= branvar[78] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4e: 
	    begin 
	   branvar[79] <= branvar[79] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h4f: 
	    begin 
	   branvar[80] <= branvar[80] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h50: 
	    begin 
	   branvar[81] <= branvar[81] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01011;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h51: 
	    begin 
	   branvar[82] <= branvar[82] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01011;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h52: 
	    begin 
	   branvar[83] <= branvar[83] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h53: 
	    begin 
	   branvar[84] <= branvar[84] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h54: 
	    begin 
	   branvar[85] <= branvar[85] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0101;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h55: 
	    begin 
	   branvar[86] <= branvar[86] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0101;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h56: 
	    begin 
	   branvar[87] <= branvar[87] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0101;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[88] <= branvar[88] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h1: 
	    begin 
	   branvar[89] <= branvar[89] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[90] <= branvar[90] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[91] <= branvar[91] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[92] <= branvar[92] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00001;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[93] <= branvar[93] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00010;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[94] <= branvar[94] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h2: 
	    begin 
	   branvar[95] <= branvar[95] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[96] <= branvar[96] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00100;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[97] <= branvar[97] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00101;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[98] <= branvar[98] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00111;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[99] <= branvar[99] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[100] <= branvar[100] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h3: 
	    begin 
	   branvar[101] <= branvar[101] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[102] <= branvar[102] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b01100;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[103] <= branvar[103] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b1;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[104] <= branvar[104] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[105] <= branvar[105] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[106] <= branvar[106] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h4: 
	    begin 
	   branvar[107] <= branvar[107] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[108] <= branvar[108] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b101;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[109] <= branvar[109] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[110] <= branvar[110] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[111] <= branvar[111] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0001;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[112] <= branvar[112] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h5: 
	    begin 
	   branvar[113] <= branvar[113] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[114] <= branvar[114] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b010;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[115] <= branvar[115] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b101;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[116] <= branvar[116] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b100;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[117] <= branvar[117] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b001;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[118] <= branvar[118] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h6: 
	    begin 
	   branvar[119] <= branvar[119] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[120] <= branvar[120] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b110;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[121] <= branvar[121] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b011;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[122] <= branvar[122] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[123] <= branvar[123] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h7: 
	    begin 
	   branvar[124] <= branvar[124] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b1;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc26)
	  9'h0: 
	    begin 
	   branvar[125] <= branvar[125] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[126] <= branvar[126] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b10;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b111;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b1;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[127] <= branvar[127] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h8: 
	    begin 
	   branvar[128] <= branvar[128] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[129] <= branvar[129] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[130] <= branvar[130] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[131] <= branvar[131] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[132] <= branvar[132] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[133] <= branvar[133] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'h9: 
	    begin 
	   branvar[134] <= branvar[134] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[135] <= branvar[135] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0011;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b1;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[136] <= branvar[136] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0010;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[137] <= branvar[137] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0100;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[138] <= branvar[138] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0111;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[139] <= branvar[139] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'ha: 
	    begin 
	   branvar[140] <= branvar[140] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[141] <= branvar[141] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0110;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[142] <= branvar[142] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0101;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b1;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b1;
	  decode_packet1dreg_is_dest = 1'b1;
	 end
	  end
	  9'h2: 
	    begin 
	   branvar[143] <= branvar[143] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h3: 
	    begin 
	   branvar[144] <= branvar[144] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0010;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[145] <= branvar[145] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'hb: 
	    begin 
	   branvar[146] <= branvar[146] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b1;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opcode)
	  9'h0: 
	    begin 
	   branvar[147] <= branvar[147] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[148] <= branvar[148] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0011;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[149] <= branvar[149] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'hc: 
	    begin 
	   branvar[150] <= branvar[150] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b1;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[151] <= branvar[151] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  9'h1: 
	    begin 
	   branvar[152] <= branvar[152] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b1000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[153] <= branvar[153] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  4'hd: 
	    begin 
	   branvar[154] <= branvar[154] + 1;
	 begin
	  has_dreg = 1'b1;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b1;
	  decode_packet1is_imm5 = 1'b0;
	 case( decode_opc)
	  9'h0: 
	    begin 
	   branvar[155] <= branvar[155] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0001;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	  default: 
	    begin 
	 branvar[156] <= branvar[156] + 1;
	 begin
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  end
	  default: 
	    begin 
	 branvar[157] <= branvar[157] + 1;
	 begin
	  has_dreg = 1'b0;
	  decode_packet1has_sreg_t = 1'b0;
	  has_sreg_s = 1'b0;
	  decode_packet1is_imm26 = 1'b0;
	  decode_packet1is_imm = 1'b0;
	  decode_packet1is_imm5 = 1'b0;
	  decode_packet1op_FU_type = 4'b0000;
	  decode_packet1alu_op = 5'b00000;
	  decode_packet1fpu_op = 5'b00000;
	  decode_packet1op_BR_type = 2'b00;
	  decode_packet1shift_type = 2'b00;
	  decode_packet1op_MEM_type = 4'b0000;
	  decode_packet1compare_type = 3'b000;
	  decode_packet1is_valid = 1'b0;
	  decode_packet1is_signed = 1'b0;
	  decode_packet1carry_in = 1'b0;
	  decode_packet1carry_out = 1'b0;
	  decode_packet1is_prefetch = 1'b0;
	  decode_packet1sprf_dest = 1'b0;
	  decode_packet1sprf_src = 1'b0;
	  decode_packet1is_atomic = 1'b0;
	  decode_packet1is_ldl = 1'b0;
	  decode_packet1is_stl = 1'b0;
	  decode_packet1is_stc = 1'b0;
	  decode_packet1is_halt = 1'b0;
	  decode_packet1is_compare = 1'b0;
	  decode_packet1dreg_is_src = 1'b0;
	  decode_packet1dreg_is_dest = 1'b0;
	 end
	  end
	 endcase
	 end
	  endmodule 

module scoreboard (clk , reset , src0_addr , src1_addr , src2_addr , src3_addr , dst0_addr , dst0_vld , dst1_addr , dst1_vld , wb0_addr , wb0_vld , wb1_addr , wb1_vld , src0_rdy , src1_rdy , src2_rdy , src3_rdy , dst0_rdy , dst1_rdy );

	  input  clk;
	  input  reset;
	  input [4 : 0] src0_addr;
	  input [4 : 0] src1_addr;
	  input [4 : 0] src2_addr;
	  input [4 : 0] src3_addr;
	  input [4 : 0] dst0_addr;
	  input  dst0_vld;
	  input [4 : 0] dst1_addr;
	  input  dst1_vld;
	  input [4 : 0] wb0_addr;
	  input  wb0_vld;
	  input [4 : 0] wb1_addr;
	  input  wb1_vld;
	  output src0_rdy ;
	  output src1_rdy ;
	  output src2_rdy ;
	  output src3_rdy ;
	  output dst0_rdy ;
	  output dst1_rdy ;
	  reg [31 : 0] sb;
	  reg [7:0] branvar[0:100];
	  reg [3 : 0] next_sb;
	  reg [4 : 0] next_sb_addr_0;
	  reg [4 : 0] next_sb_addr_1;
	  reg [4 : 0] next_sb_addr_2;
	  reg [4 : 0] next_sb_addr_3;
	  reg [3 : 0] next_sb_vld;
	 always @ ( dst0_addr) or ( sb)
	 begin
	 case( dst0_addr)
	  5'h0: 
	    begin 
	   branvar[0] <= branvar[0] + 1;
	  dst0_rdy = (  sb[0] != 1'b0);
	  end
	  5'h1: 
	    begin 
	   branvar[1] <= branvar[1] + 1;
	  dst0_rdy = (  sb[1] != 1'b0);
	  end
	  5'h2: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	  dst0_rdy = (  sb[2] != 1'b0);
	  end
	  5'h3: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	  dst0_rdy = (  sb[3] != 1'b0);
	  end
	  5'h4: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	  dst0_rdy = (  sb[4] != 1'b0);
	  end
	  5'h5: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	  dst0_rdy = (  sb[5] != 1'b0);
	  end
	  5'h6: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	  dst0_rdy = (  sb[6] != 1'b0);
	  end
	  5'h7: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	  dst0_rdy = (  sb[7] != 1'b0);
	  end
	  5'h8: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	  dst0_rdy = (  sb[8] != 1'b0);
	  end
	  5'h9: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	  dst0_rdy = (  sb[9] != 1'b0);
	  end
	  5'ha: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	  dst0_rdy = (  sb[10] != 1'b0);
	  end
	  5'hb: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	  dst0_rdy = (  sb[11] != 1'b0);
	  end
	  5'hc: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	  dst0_rdy = (  sb[12] != 1'b0);
	  end
	  5'hd: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	  dst0_rdy = (  sb[13] != 1'b0);
	  end
	  5'he: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	  dst0_rdy = (  sb[14] != 1'b0);
	  end
	  5'hf: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	  dst0_rdy = (  sb[15] != 1'b0);
	  end
	  5'h10: 
	    begin 
	   branvar[16] <= branvar[16] + 1;
	  dst0_rdy = (  sb[16] != 1'b0);
	  end
	  5'h11: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	  dst0_rdy = (  sb[17] != 1'b0);
	  end
	  5'h12: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	  dst0_rdy = (  sb[18] != 1'b0);
	  end
	  5'h13: 
	    begin 
	   branvar[19] <= branvar[19] + 1;
	  dst0_rdy = (  sb[19] != 1'b0);
	  end
	  5'h14: 
	    begin 
	   branvar[20] <= branvar[20] + 1;
	  dst0_rdy = (  sb[20] != 1'b0);
	  end
	  5'h15: 
	    begin 
	   branvar[21] <= branvar[21] + 1;
	  dst0_rdy = (  sb[21] != 1'b0);
	  end
	  5'h16: 
	    begin 
	   branvar[22] <= branvar[22] + 1;
	  dst0_rdy = (  sb[22] != 1'b0);
	  end
	  5'h17: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	  dst0_rdy = (  sb[23] != 1'b0);
	  end
	  5'h18: 
	    begin 
	   branvar[24] <= branvar[24] + 1;
	  dst0_rdy = (  sb[24] != 1'b0);
	  end
	  5'h19: 
	    begin 
	   branvar[25] <= branvar[25] + 1;
	  dst0_rdy = (  sb[25] != 1'b0);
	  end
	  5'h1a: 
	    begin 
	   branvar[26] <= branvar[26] + 1;
	  dst0_rdy = (  sb[26] != 1'b0);
	  end
	  5'h1b: 
	    begin 
	   branvar[27] <= branvar[27] + 1;
	  dst0_rdy = (  sb[27] != 1'b0);
	  end
	  5'h1c: 
	    begin 
	   branvar[28] <= branvar[28] + 1;
	  dst0_rdy = (  sb[28] != 1'b0);
	  end
	  5'h1d: 
	    begin 
	   branvar[29] <= branvar[29] + 1;
	  dst0_rdy = (  sb[29] != 1'b0);
	  end
	  5'h1e: 
	    begin 
	   branvar[30] <= branvar[30] + 1;
	  dst0_rdy = (  sb[30] != 1'b0);
	  end
	  5'h1f: 
	    begin 
	   branvar[31] <= branvar[31] + 1;
	  dst0_rdy = (  sb[31] != 1'b0);
	  end
	 endcase
	 end
	 always @ ( dst1_addr) or ( sb)
	 begin
	 case( dst1_addr)
	  5'h0: 
	    begin 
	   branvar[32] <= branvar[32] + 1;
	  dst1_rdy = (  sb[0] != 1'b0);
	  end
	  5'h1: 
	    begin 
	   branvar[33] <= branvar[33] + 1;
	  dst1_rdy = (  sb[1] != 1'b0);
	  end
	  5'h2: 
	    begin 
	   branvar[34] <= branvar[34] + 1;
	  dst1_rdy = (  sb[2] != 1'b0);
	  end
	  5'h3: 
	    begin 
	   branvar[35] <= branvar[35] + 1;
	  dst1_rdy = (  sb[3] != 1'b0);
	  end
	  5'h4: 
	    begin 
	   branvar[36] <= branvar[36] + 1;
	  dst1_rdy = (  sb[4] != 1'b0);
	  end
	  5'h5: 
	    begin 
	   branvar[37] <= branvar[37] + 1;
	  dst1_rdy = (  sb[5] != 1'b0);
	  end
	  5'h6: 
	    begin 
	   branvar[38] <= branvar[38] + 1;
	  dst1_rdy = (  sb[6] != 1'b0);
	  end
	  5'h7: 
	    begin 
	   branvar[39] <= branvar[39] + 1;
	  dst1_rdy = (  sb[7] != 1'b0);
	  end
	  5'h8: 
	    begin 
	   branvar[40] <= branvar[40] + 1;
	  dst1_rdy = (  sb[8] != 1'b0);
	  end
	  5'h9: 
	    begin 
	   branvar[41] <= branvar[41] + 1;
	  dst1_rdy = (  sb[9] != 1'b0);
	  end
	  5'ha: 
	    begin 
	   branvar[42] <= branvar[42] + 1;
	  dst1_rdy = (  sb[10] != 1'b0);
	  end
	  5'hb: 
	    begin 
	   branvar[43] <= branvar[43] + 1;
	  dst1_rdy = (  sb[11] != 1'b0);
	  end
	  5'hc: 
	    begin 
	   branvar[44] <= branvar[44] + 1;
	  dst1_rdy = (  sb[12] != 1'b0);
	  end
	  5'hd: 
	    begin 
	   branvar[45] <= branvar[45] + 1;
	  dst1_rdy = (  sb[13] != 1'b0);
	  end
	  5'he: 
	    begin 
	   branvar[46] <= branvar[46] + 1;
	  dst1_rdy = (  sb[14] != 1'b0);
	  end
	  5'hf: 
	    begin 
	   branvar[47] <= branvar[47] + 1;
	  dst1_rdy = (  sb[15] != 1'b0);
	  end
	  5'h10: 
	    begin 
	   branvar[48] <= branvar[48] + 1;
	  dst1_rdy = (  sb[16] != 1'b0);
	  end
	  5'h11: 
	    begin 
	   branvar[49] <= branvar[49] + 1;
	  dst1_rdy = (  sb[17] != 1'b0);
	  end
	  5'h12: 
	    begin 
	   branvar[50] <= branvar[50] + 1;
	  dst1_rdy = (  sb[18] != 1'b0);
	  end
	  5'h13: 
	    begin 
	   branvar[51] <= branvar[51] + 1;
	  dst1_rdy = (  sb[19] != 1'b0);
	  end
	  5'h14: 
	    begin 
	   branvar[52] <= branvar[52] + 1;
	  dst1_rdy = (  sb[20] != 1'b0);
	  end
	  5'h15: 
	    begin 
	   branvar[53] <= branvar[53] + 1;
	  dst1_rdy = (  sb[21] != 1'b0);
	  end
	  5'h16: 
	    begin 
	   branvar[54] <= branvar[54] + 1;
	  dst1_rdy = (  sb[22] != 1'b0);
	  end
	  5'h17: 
	    begin 
	   branvar[55] <= branvar[55] + 1;
	  dst1_rdy = (  sb[23] != 1'b0);
	  end
	  5'h18: 
	    begin 
	   branvar[56] <= branvar[56] + 1;
	  dst1_rdy = (  sb[24] != 1'b0);
	  end
	  5'h19: 
	    begin 
	   branvar[57] <= branvar[57] + 1;
	  dst1_rdy = (  sb[25] != 1'b0);
	  end
	  5'h1a: 
	    begin 
	   branvar[58] <= branvar[58] + 1;
	  dst1_rdy = (  sb[26] != 1'b0);
	  end
	  5'h1b: 
	    begin 
	   branvar[59] <= branvar[59] + 1;
	  dst1_rdy = (  sb[27] != 1'b0);
	  end
	  5'h1c: 
	    begin 
	   branvar[60] <= branvar[60] + 1;
	  dst1_rdy = (  sb[28] != 1'b0);
	  end
	  5'h1d: 
	    begin 
	   branvar[61] <= branvar[61] + 1;
	  dst1_rdy = (  sb[29] != 1'b0);
	  end
	  5'h1e: 
	    begin 
	   branvar[62] <= branvar[62] + 1;
	  dst1_rdy = (  sb[30] != 1'b0);
	  end
	  5'h1f: 
	    begin 
	   branvar[63] <= branvar[63] + 1;
	  dst1_rdy = (  sb[31] != 1'b0);
	  end
	 endcase
	 end
	 always @ ( src0_addr) or ( sb)
	 begin
	 case( src0_addr)
	  5'h0: 
	    begin 
	   branvar[64] <= branvar[64] + 1;
	  src0_rdy = 1;
	  end
	  5'h1: 
	    begin 
	   branvar[65] <= branvar[65] + 1;
	  src0_rdy = (  sb[1] != 1'b0);
	  end
	  5'h2: 
	    begin 
	   branvar[66] <= branvar[66] + 1;
	  src0_rdy = (  sb[2] != 1'b0);
	  end
	  5'h3: 
	    begin 
	   branvar[67] <= branvar[67] + 1;
	  src0_rdy = (  sb[3] != 1'b0);
	  end
	  5'h4: 
	    begin 
	   branvar[68] <= branvar[68] + 1;
	  src0_rdy = (  sb[4] != 1'b0);
	  end
	  5'h5: 
	    begin 
	   branvar[69] <= branvar[69] + 1;
	  src0_rdy = (  sb[5] != 1'b0);
	  end
	  5'h6: 
	    begin 
	   branvar[70] <= branvar[70] + 1;
	  src0_rdy = (  sb[6] != 1'b0);
	  end
	  5'h7: 
	    begin 
	   branvar[71] <= branvar[71] + 1;
	  src0_rdy = (  sb[7] != 1'b0);
	  end
	  5'h8: 
	    begin 
	   branvar[72] <= branvar[72] + 1;
	  src0_rdy = (  sb[8] != 1'b0);
	  end
	  5'h9: 
	    begin 
	   branvar[73] <= branvar[73] + 1;
	  src0_rdy = (  sb[9] != 1'b0);
	  end
	  5'ha: 
	    begin 
	   branvar[74] <= branvar[74] + 1;
	  src0_rdy = (  sb[10] != 1'b0);
	  end
	  5'hb: 
	    begin 
	   branvar[75] <= branvar[75] + 1;
	  src0_rdy = (  sb[11] != 1'b0);
	  end
	  5'hc: 
	    begin 
	   branvar[76] <= branvar[76] + 1;
	  src0_rdy = (  sb[12] != 1'b0);
	  end
	  5'hd: 
	    begin 
	   branvar[77] <= branvar[77] + 1;
	  src0_rdy = (  sb[13] != 1'b0);
	  end
	  5'he: 
	    begin 
	   branvar[78] <= branvar[78] + 1;
	  src0_rdy = (  sb[14] != 1'b0);
	  end
	  5'hf: 
	    begin 
	   branvar[79] <= branvar[79] + 1;
	  src0_rdy = (  sb[15] != 1'b0);
	  end
	  5'h10: 
	    begin 
	   branvar[80] <= branvar[80] + 1;
	  src0_rdy = (  sb[16] != 1'b0);
	  end
	  5'h11: 
	    begin 
	   branvar[81] <= branvar[81] + 1;
	  src0_rdy = (  sb[17] != 1'b0);
	  end
	  5'h12: 
	    begin 
	   branvar[82] <= branvar[82] + 1;
	  src0_rdy = (  sb[18] != 1'b0);
	  end
	  5'h13: 
	    begin 
	   branvar[83] <= branvar[83] + 1;
	  src0_rdy = (  sb[19] != 1'b0);
	  end
	  5'h14: 
	    begin 
	   branvar[84] <= branvar[84] + 1;
	  src0_rdy = (  sb[20] != 1'b0);
	  end
	  5'h15: 
	    begin 
	   branvar[85] <= branvar[85] + 1;
	  src0_rdy = (  sb[21] != 1'b0);
	  end
	  5'h16: 
	    begin 
	   branvar[86] <= branvar[86] + 1;
	  src0_rdy = (  sb[22] != 1'b0);
	  end
	  5'h17: 
	    begin 
	   branvar[87] <= branvar[87] + 1;
	  src0_rdy = (  sb[23] != 1'b0);
	  end
	  5'h18: 
	    begin 
	   branvar[88] <= branvar[88] + 1;
	  src0_rdy = (  sb[24] != 1'b0);
	  end
	  5'h19: 
	    begin 
	   branvar[89] <= branvar[89] + 1;
	  src0_rdy = (  sb[25] != 1'b0);
	  end
	  5'h1a: 
	    begin 
	   branvar[90] <= branvar[90] + 1;
	  src0_rdy = (  sb[26] != 1'b0);
	  end
	  5'h1b: 
	    begin 
	   branvar[91] <= branvar[91] + 1;
	  src0_rdy = (  sb[27] != 1'b0);
	  end
	  5'h1c: 
	    begin 
	   branvar[92] <= branvar[92] + 1;
	  src0_rdy = (  sb[28] != 1'b0);
	  end
	  5'h1d: 
	    begin 
	   branvar[93] <= branvar[93] + 1;
	  src0_rdy = (  sb[29] != 1'b0);
	  end
	  5'h1e: 
	    begin 
	   branvar[94] <= branvar[94] + 1;
	  src0_rdy = (  sb[30] != 1'b0);
	  end
	  5'h1f: 
	    begin 
	   branvar[95] <= branvar[95] + 1;
	  src0_rdy = (  sb[31] != 1'b0);
	  end
	 endcase
	 end
	 always @ ( src1_addr) or ( sb)
	 begin
	 case( src1_addr)
	  5'h0: 
	    begin 
	   branvar[96] <= branvar[96] + 1;
	  src1_rdy = 1;
	  end
	  5'h1: 
	    begin 
	   branvar[97] <= branvar[97] + 1;
	  src1_rdy = (  sb[1] != 1'b0);
	  end
	  5'h2: 
	    begin 
	   branvar[98] <= branvar[98] + 1;
	  src1_rdy = (  sb[2] != 1'b0);
	  end
	  5'h3: 
	    begin 
	   branvar[99] <= branvar[99] + 1;
	  src1_rdy = (  sb[3] != 1'b0);
	  end
	  5'h4: 
	    begin 
	   branvar[100] <= branvar[100] + 1;
	  src1_rdy = (  sb[4] != 1'b0);
	  end
	  5'h5: 
	    begin 
	   branvar[101] <= branvar[101] + 1;
	  src1_rdy = (  sb[5] != 1'b0);
	  end
	  5'h6: 
	    begin 
	   branvar[102] <= branvar[102] + 1;
	  src1_rdy = (  sb[6] != 1'b0);
	  end
	  5'h7: 
	    begin 
	   branvar[103] <= branvar[103] + 1;
	  src1_rdy = (  sb[7] != 1'b0);
	  end
	  5'h8: 
	    begin 
	   branvar[104] <= branvar[104] + 1;
	  src1_rdy = (  sb[8] != 1'b0);
	  end
	  5'h9: 
	    begin 
	   branvar[105] <= branvar[105] + 1;
	  src1_rdy = (  sb[9] != 1'b0);
	  end
	  5'ha: 
	    begin 
	   branvar[106] <= branvar[106] + 1;
	  src1_rdy = (  sb[10] != 1'b0);
	  end
	  5'hb: 
	    begin 
	   branvar[107] <= branvar[107] + 1;
	  src1_rdy = (  sb[11] != 1'b0);
	  end
	  5'hc: 
	    begin 
	   branvar[108] <= branvar[108] + 1;
	  src1_rdy = (  sb[12] != 1'b0);
	  end
	  5'hd: 
	    begin 
	   branvar[109] <= branvar[109] + 1;
	  src1_rdy = (  sb[13] != 1'b0);
	  end
	  5'he: 
	    begin 
	   branvar[110] <= branvar[110] + 1;
	  src1_rdy = (  sb[14] != 1'b0);
	  end
	  5'hf: 
	    begin 
	   branvar[111] <= branvar[111] + 1;
	  src1_rdy = (  sb[15] != 1'b0);
	  end
	  5'h10: 
	    begin 
	   branvar[112] <= branvar[112] + 1;
	  src1_rdy = (  sb[16] != 1'b0);
	  end
	  5'h11: 
	    begin 
	   branvar[113] <= branvar[113] + 1;
	  src1_rdy = (  sb[17] != 1'b0);
	  end
	  5'h12: 
	    begin 
	   branvar[114] <= branvar[114] + 1;
	  src1_rdy = (  sb[18] != 1'b0);
	  end
	  5'h13: 
	    begin 
	   branvar[115] <= branvar[115] + 1;
	  src1_rdy = (  sb[19] != 1'b0);
	  end
	  5'h14: 
	    begin 
	   branvar[116] <= branvar[116] + 1;
	  src1_rdy = (  sb[20] != 1'b0);
	  end
	  5'h15: 
	    begin 
	   branvar[117] <= branvar[117] + 1;
	  src1_rdy = (  sb[21] != 1'b0);
	  end
	  5'h16: 
	    begin 
	   branvar[118] <= branvar[118] + 1;
	  src1_rdy = (  sb[22] != 1'b0);
	  end
	  5'h17: 
	    begin 
	   branvar[119] <= branvar[119] + 1;
	  src1_rdy = (  sb[23] != 1'b0);
	  end
	  5'h18: 
	    begin 
	   branvar[120] <= branvar[120] + 1;
	  src1_rdy = (  sb[24] != 1'b0);
	  end
	  5'h19: 
	    begin 
	   branvar[121] <= branvar[121] + 1;
	  src1_rdy = (  sb[25] != 1'b0);
	  end
	  5'h1a: 
	    begin 
	   branvar[122] <= branvar[122] + 1;
	  src1_rdy = (  sb[26] != 1'b0);
	  end
	  5'h1b: 
	    begin 
	   branvar[123] <= branvar[123] + 1;
	  src1_rdy = (  sb[27] != 1'b0);
	  end
	  5'h1c: 
	    begin 
	   branvar[124] <= branvar[124] + 1;
	  src1_rdy = (  sb[28] != 1'b0);
	  end
	  5'h1d: 
	    begin 
	   branvar[125] <= branvar[125] + 1;
	  src1_rdy = (  sb[29] != 1'b0);
	  end
	  5'h1e: 
	    begin 
	   branvar[126] <= branvar[126] + 1;
	  src1_rdy = (  sb[30] != 1'b0);
	  end
	  5'h1f: 
	    begin 
	   branvar[127] <= branvar[127] + 1;
	  src1_rdy = (  sb[31] != 1'b0);
	  end
	 endcase
	 end
	 always @ ( src2_addr) or ( sb)
	 begin
	 case( src2_addr)
	  5'h0: 
	    begin 
	   branvar[128] <= branvar[128] + 1;
	  src2_rdy = 1;
	  end
	  5'h1: 
	    begin 
	   branvar[129] <= branvar[129] + 1;
	  src2_rdy = (  sb[1] != 1'b0);
	  end
	  5'h2: 
	    begin 
	   branvar[130] <= branvar[130] + 1;
	  src2_rdy = (  sb[2] != 1'b0);
	  end
	  5'h3: 
	    begin 
	   branvar[131] <= branvar[131] + 1;
	  src2_rdy = (  sb[3] != 1'b0);
	  end
	  5'h4: 
	    begin 
	   branvar[132] <= branvar[132] + 1;
	  src2_rdy = (  sb[4] != 1'b0);
	  end
	  5'h5: 
	    begin 
	   branvar[133] <= branvar[133] + 1;
	  src2_rdy = (  sb[5] != 1'b0);
	  end
	  5'h6: 
	    begin 
	   branvar[134] <= branvar[134] + 1;
	  src2_rdy = (  sb[6] != 1'b0);
	  end
	  5'h7: 
	    begin 
	   branvar[135] <= branvar[135] + 1;
	  src2_rdy = (  sb[7] != 1'b0);
	  end
	  5'h8: 
	    begin 
	   branvar[136] <= branvar[136] + 1;
	  src2_rdy = (  sb[8] != 1'b0);
	  end
	  5'h9: 
	    begin 
	   branvar[137] <= branvar[137] + 1;
	  src2_rdy = (  sb[9] != 1'b0);
	  end
	  5'ha: 
	    begin 
	   branvar[138] <= branvar[138] + 1;
	  src2_rdy = (  sb[10] != 1'b0);
	  end
	  5'hb: 
	    begin 
	   branvar[139] <= branvar[139] + 1;
	  src2_rdy = (  sb[11] != 1'b0);
	  end
	  5'hc: 
	    begin 
	   branvar[140] <= branvar[140] + 1;
	  src2_rdy = (  sb[12] != 1'b0);
	  end
	  5'hd: 
	    begin 
	   branvar[141] <= branvar[141] + 1;
	  src2_rdy = (  sb[13] != 1'b0);
	  end
	  5'he: 
	    begin 
	   branvar[142] <= branvar[142] + 1;
	  src2_rdy = (  sb[14] != 1'b0);
	  end
	  5'hf: 
	    begin 
	   branvar[143] <= branvar[143] + 1;
	  src2_rdy = (  sb[15] != 1'b0);
	  end
	  5'h10: 
	    begin 
	   branvar[144] <= branvar[144] + 1;
	  src2_rdy = (  sb[16] != 1'b0);
	  end
	  5'h11: 
	    begin 
	   branvar[145] <= branvar[145] + 1;
	  src2_rdy = (  sb[17] != 1'b0);
	  end
	  5'h12: 
	    begin 
	   branvar[146] <= branvar[146] + 1;
	  src2_rdy = (  sb[18] != 1'b0);
	  end
	  5'h13: 
	    begin 
	   branvar[147] <= branvar[147] + 1;
	  src2_rdy = (  sb[19] != 1'b0);
	  end
	  5'h14: 
	    begin 
	   branvar[148] <= branvar[148] + 1;
	  src2_rdy = (  sb[20] != 1'b0);
	  end
	  5'h15: 
	    begin 
	   branvar[149] <= branvar[149] + 1;
	  src2_rdy = (  sb[21] != 1'b0);
	  end
	  5'h16: 
	    begin 
	   branvar[150] <= branvar[150] + 1;
	  src2_rdy = (  sb[22] != 1'b0);
	  end
	  5'h17: 
	    begin 
	   branvar[151] <= branvar[151] + 1;
	  src2_rdy = (  sb[23] != 1'b0);
	  end
	  5'h18: 
	    begin 
	   branvar[152] <= branvar[152] + 1;
	  src2_rdy = (  sb[24] != 1'b0);
	  end
	  5'h19: 
	    begin 
	   branvar[153] <= branvar[153] + 1;
	  src2_rdy = (  sb[25] != 1'b0);
	  end
	  5'h1a: 
	    begin 
	   branvar[154] <= branvar[154] + 1;
	  src2_rdy = (  sb[26] != 1'b0);
	  end
	  5'h1b: 
	    begin 
	   branvar[155] <= branvar[155] + 1;
	  src2_rdy = (  sb[27] != 1'b0);
	  end
	  5'h1c: 
	    begin 
	   branvar[156] <= branvar[156] + 1;
	  src2_rdy = (  sb[28] != 1'b0);
	  end
	  5'h1d: 
	    begin 
	   branvar[157] <= branvar[157] + 1;
	  src2_rdy = (  sb[29] != 1'b0);
	  end
	  5'h1e: 
	    begin 
	   branvar[158] <= branvar[158] + 1;
	  src2_rdy = (  sb[30] != 1'b0);
	  end
	  5'h1f: 
	    begin 
	   branvar[159] <= branvar[159] + 1;
	  src2_rdy = (  sb[31] != 1'b0);
	  end
	 endcase
	 end
	 always @ ( src3_addr) or ( sb)
	 begin
	 case( src3_addr)
	  5'h0: 
	    begin 
	   branvar[160] <= branvar[160] + 1;
	  src3_rdy = 1;
	  end
	  5'h1: 
	    begin 
	   branvar[161] <= branvar[161] + 1;
	  src3_rdy = (  sb[1] != 1'b0);
	  end
	  5'h2: 
	    begin 
	   branvar[162] <= branvar[162] + 1;
	  src3_rdy = (  sb[2] != 1'b0);
	  end
	  5'h3: 
	    begin 
	   branvar[163] <= branvar[163] + 1;
	  src3_rdy = (  sb[3] != 1'b0);
	  end
	  5'h4: 
	    begin 
	   branvar[164] <= branvar[164] + 1;
	  src3_rdy = (  sb[4] != 1'b0);
	  end
	  5'h5: 
	    begin 
	   branvar[165] <= branvar[165] + 1;
	  src3_rdy = (  sb[5] != 1'b0);
	  end
	  5'h6: 
	    begin 
	   branvar[166] <= branvar[166] + 1;
	  src3_rdy = (  sb[6] != 1'b0);
	  end
	  5'h7: 
	    begin 
	   branvar[167] <= branvar[167] + 1;
	  src3_rdy = (  sb[7] != 1'b0);
	  end
	  5'h8: 
	    begin 
	   branvar[168] <= branvar[168] + 1;
	  src3_rdy = (  sb[8] != 1'b0);
	  end
	  5'h9: 
	    begin 
	   branvar[169] <= branvar[169] + 1;
	  src3_rdy = (  sb[9] != 1'b0);
	  end
	  5'ha: 
	    begin 
	   branvar[170] <= branvar[170] + 1;
	  src3_rdy = (  sb[10] != 1'b0);
	  end
	  5'hb: 
	    begin 
	   branvar[171] <= branvar[171] + 1;
	  src3_rdy = (  sb[11] != 1'b0);
	  end
	  5'hc: 
	    begin 
	   branvar[172] <= branvar[172] + 1;
	  src3_rdy = (  sb[12] != 1'b0);
	  end
	  5'hd: 
	    begin 
	   branvar[173] <= branvar[173] + 1;
	  src3_rdy = (  sb[13] != 1'b0);
	  end
	  5'he: 
	    begin 
	   branvar[174] <= branvar[174] + 1;
	  src3_rdy = (  sb[14] != 1'b0);
	  end
	  5'hf: 
	    begin 
	   branvar[175] <= branvar[175] + 1;
	  src3_rdy = (  sb[15] != 1'b0);
	  end
	  5'h10: 
	    begin 
	   branvar[176] <= branvar[176] + 1;
	  src3_rdy = (  sb[16] != 1'b0);
	  end
	  5'h11: 
	    begin 
	   branvar[177] <= branvar[177] + 1;
	  src3_rdy = (  sb[17] != 1'b0);
	  end
	  5'h12: 
	    begin 
	   branvar[178] <= branvar[178] + 1;
	  src3_rdy = (  sb[18] != 1'b0);
	  end
	  5'h13: 
	    begin 
	   branvar[179] <= branvar[179] + 1;
	  src3_rdy = (  sb[19] != 1'b0);
	  end
	  5'h14: 
	    begin 
	   branvar[180] <= branvar[180] + 1;
	  src3_rdy = (  sb[20] != 1'b0);
	  end
	  5'h15: 
	    begin 
	   branvar[181] <= branvar[181] + 1;
	  src3_rdy = (  sb[21] != 1'b0);
	  end
	  5'h16: 
	    begin 
	   branvar[182] <= branvar[182] + 1;
	  src3_rdy = (  sb[22] != 1'b0);
	  end
	  5'h17: 
	    begin 
	   branvar[183] <= branvar[183] + 1;
	  src3_rdy = (  sb[23] != 1'b0);
	  end
	  5'h18: 
	    begin 
	   branvar[184] <= branvar[184] + 1;
	  src3_rdy = (  sb[24] != 1'b0);
	  end
	  5'h19: 
	    begin 
	   branvar[185] <= branvar[185] + 1;
	  src3_rdy = (  sb[25] != 1'b0);
	  end
	  5'h1a: 
	    begin 
	   branvar[186] <= branvar[186] + 1;
	  src3_rdy = (  sb[26] != 1'b0);
	  end
	  5'h1b: 
	    begin 
	   branvar[187] <= branvar[187] + 1;
	  src3_rdy = (  sb[27] != 1'b0);
	  end
	  5'h1c: 
	    begin 
	   branvar[188] <= branvar[188] + 1;
	  src3_rdy = (  sb[28] != 1'b0);
	  end
	  5'h1d: 
	    begin 
	   branvar[189] <= branvar[189] + 1;
	  src3_rdy = (  sb[29] != 1'b0);
	  end
	  5'h1e: 
	    begin 
	   branvar[190] <= branvar[190] + 1;
	  src3_rdy = (  sb[30] != 1'b0);
	  end
	  5'h1f: 
	    begin 
	   branvar[191] <= branvar[191] + 1;
	  src3_rdy = (  sb[31] != 1'b0);
	  end
	 endcase
	 end
	 assign 
	  next_sb_addr_0 =  dst0_addr;
	 assign 
	  next_sb_addr_1 =  dst1_addr;
	 assign 
	  next_sb_addr_2 =  wb0_addr;
	 assign 
	  next_sb_addr_3 =  wb1_addr;
	 always @ ( dst0_vld) or ( dst0_addr) or ( sb) or ( dst1_vld) or ( dst1_addr) or ( wb0_vld) or ( wb1_vld)
	 if(( dst0_vld && (  sb[ dst0_addr] == 1'b1))) 
	  begin 
	 branvar[192] <= branvar[192] + 1; 
	 begin
	  next_sb_vld[0] = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[193] <= branvar[193] + 1;   
	 begin
	  next_sb_vld[0] = 1'b0;
	 end
	  end
	 if(( dst1_vld && (  sb[ dst1_addr] == 1'b1))) 
	  begin 
	 branvar[194] <= branvar[194] + 1; 
	 begin
	  next_sb_vld[1] = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[195] <= branvar[195] + 1;   
	 begin
	  next_sb_vld[1] = 1'b0;
	 end
	  end
	 if( wb0_vld) 
	  begin 
	 branvar[196] <= branvar[196] + 1; 
	 begin
	  next_sb_vld[2] = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[197] <= branvar[197] + 1;   
	 begin
	  next_sb_vld[2] = 1'b0;
	 end
	  end
	 if( wb1_vld) 
	  begin 
	 branvar[198] <= branvar[198] + 1; 
	 begin
	  next_sb_vld[3] = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[199] <= branvar[199] + 1;   
	 begin
	  next_sb_vld[3] = 1'b0;
	 end
	  end
	 always @ (posedge  clk) 
	 if((  reset == 1)) 
	  begin 
	 branvar[200] <= branvar[200] + 1; 
	 begin
	  sb[31:0] <= 32'h7fffffff;
	 end
	  end
	 else 
	   begin 
	   branvar[201] <= branvar[201] + 1;   
	 begin
	 if( next_sb_vld[0]) 
	  begin 
	 branvar[202] <= branvar[202] + 1; 
	 begin
	 case( next_sb_addr[0])
	  5'h0: 
	    begin 
	   branvar[203] <= branvar[203] + 1;
	  sb[0] <= 0;
	  end
	  5'h1: 
	    begin 
	   branvar[204] <= branvar[204] + 1;
	  sb[1] <= 0;
	  end
	  5'h2: 
	    begin 
	   branvar[205] <= branvar[205] + 1;
	  sb[2] <= 0;
	  end
	  5'h3: 
	    begin 
	   branvar[206] <= branvar[206] + 1;
	  sb[3] <= 0;
	  end
	  5'h4: 
	    begin 
	   branvar[207] <= branvar[207] + 1;
	  sb[4] <= 0;
	  end
	  5'h5: 
	    begin 
	   branvar[208] <= branvar[208] + 1;
	  sb[5] <= 0;
	  end
	  5'h6: 
	    begin 
	   branvar[209] <= branvar[209] + 1;
	  sb[6] <= 0;
	  end
	  5'h7: 
	    begin 
	   branvar[210] <= branvar[210] + 1;
	  sb[7] <= 0;
	  end
	  5'h8: 
	    begin 
	   branvar[211] <= branvar[211] + 1;
	  sb[8] <= 0;
	  end
	  5'h9: 
	    begin 
	   branvar[212] <= branvar[212] + 1;
	  sb[9] <= 0;
	  end
	  5'ha: 
	    begin 
	   branvar[213] <= branvar[213] + 1;
	  sb[10] <= 0;
	  end
	  5'hb: 
	    begin 
	   branvar[214] <= branvar[214] + 1;
	  sb[11] <= 0;
	  end
	  5'hc: 
	    begin 
	   branvar[215] <= branvar[215] + 1;
	  sb[12] <= 0;
	  end
	  5'hd: 
	    begin 
	   branvar[216] <= branvar[216] + 1;
	  sb[13] <= 0;
	  end
	  5'he: 
	    begin 
	   branvar[217] <= branvar[217] + 1;
	  sb[14] <= 0;
	  end
	  5'hf: 
	    begin 
	   branvar[218] <= branvar[218] + 1;
	  sb[15] <= 0;
	  end
	  5'h10: 
	    begin 
	   branvar[219] <= branvar[219] + 1;
	  sb[16] <= 0;
	  end
	  5'h11: 
	    begin 
	   branvar[220] <= branvar[220] + 1;
	  sb[17] <= 0;
	  end
	  5'h12: 
	    begin 
	   branvar[221] <= branvar[221] + 1;
	  sb[18] <= 0;
	  end
	  5'h13: 
	    begin 
	   branvar[222] <= branvar[222] + 1;
	  sb[19] <= 0;
	  end
	  5'h14: 
	    begin 
	   branvar[223] <= branvar[223] + 1;
	  sb[20] <= 0;
	  end
	  5'h15: 
	    begin 
	   branvar[224] <= branvar[224] + 1;
	  sb[21] <= 0;
	  end
	  5'h16: 
	    begin 
	   branvar[225] <= branvar[225] + 1;
	  sb[22] <= 0;
	  end
	  5'h17: 
	    begin 
	   branvar[226] <= branvar[226] + 1;
	  sb[23] <= 0;
	  end
	  5'h18: 
	    begin 
	   branvar[227] <= branvar[227] + 1;
	  sb[24] <= 0;
	  end
	  5'h19: 
	    begin 
	   branvar[228] <= branvar[228] + 1;
	  sb[25] <= 0;
	  end
	  5'h1a: 
	    begin 
	   branvar[229] <= branvar[229] + 1;
	  sb[26] <= 0;
	  end
	  5'h1b: 
	    begin 
	   branvar[230] <= branvar[230] + 1;
	  sb[27] <= 0;
	  end
	  5'h1c: 
	    begin 
	   branvar[231] <= branvar[231] + 1;
	  sb[28] <= 0;
	  end
	  5'h1d: 
	    begin 
	   branvar[232] <= branvar[232] + 1;
	  sb[29] <= 0;
	  end
	  5'h1e: 
	    begin 
	   branvar[233] <= branvar[233] + 1;
	  sb[30] <= 0;
	  end
	  5'h1f: 
	    begin 
	   branvar[234] <= branvar[234] + 1;
	  sb[31] <= 0;
	  end
	 endcase
	 end
	  end
	 else 
	 begin  
	   branvar[235] <= branvar[235] + 1;   
	   end 
	 if( next_sb_vld[1]) 
	  begin 
	 branvar[236] <= branvar[236] + 1; 
	 begin
	 case( next_sb_addr[1])
	  5'h0: 
	    begin 
	   branvar[237] <= branvar[237] + 1;
	  sb[0] <= 0;
	  end
	  5'h1: 
	    begin 
	   branvar[238] <= branvar[238] + 1;
	  sb[1] <= 0;
	  end
	  5'h2: 
	    begin 
	   branvar[239] <= branvar[239] + 1;
	  sb[2] <= 0;
	  end
	  5'h3: 
	    begin 
	   branvar[240] <= branvar[240] + 1;
	  sb[3] <= 0;
	  end
	  5'h4: 
	    begin 
	   branvar[241] <= branvar[241] + 1;
	  sb[4] <= 0;
	  end
	  5'h5: 
	    begin 
	   branvar[242] <= branvar[242] + 1;
	  sb[5] <= 0;
	  end
	  5'h6: 
	    begin 
	   branvar[243] <= branvar[243] + 1;
	  sb[6] <= 0;
	  end
	  5'h7: 
	    begin 
	   branvar[244] <= branvar[244] + 1;
	  sb[7] <= 0;
	  end
	  5'h8: 
	    begin 
	   branvar[245] <= branvar[245] + 1;
	  sb[8] <= 0;
	  end
	  5'h9: 
	    begin 
	   branvar[246] <= branvar[246] + 1;
	  sb[9] <= 0;
	  end
	  5'ha: 
	    begin 
	   branvar[247] <= branvar[247] + 1;
	  sb[10] <= 0;
	  end
	  5'hb: 
	    begin 
	   branvar[248] <= branvar[248] + 1;
	  sb[11] <= 0;
	  end
	  5'hc: 
	    begin 
	   branvar[249] <= branvar[249] + 1;
	  sb[12] <= 0;
	  end
	  5'hd: 
	    begin 
	   branvar[250] <= branvar[250] + 1;
	  sb[13] <= 0;
	  end
	  5'he: 
	    begin 
	   branvar[251] <= branvar[251] + 1;
	  sb[14] <= 0;
	  end
	  5'hf: 
	    begin 
	   branvar[252] <= branvar[252] + 1;
	  sb[15] <= 0;
	  end
	  5'h10: 
	    begin 
	   branvar[253] <= branvar[253] + 1;
	  sb[16] <= 0;
	  end
	  5'h11: 
	    begin 
	   branvar[254] <= branvar[254] + 1;
	  sb[17] <= 0;
	  end
	  5'h12: 
	    begin 
	   branvar[255] <= branvar[255] + 1;
	  sb[18] <= 0;
	  end
	  5'h13: 
	    begin 
	   branvar[256] <= branvar[256] + 1;
	  sb[19] <= 0;
	  end
	  5'h14: 
	    begin 
	   branvar[257] <= branvar[257] + 1;
	  sb[20] <= 0;
	  end
	  5'h15: 
	    begin 
	   branvar[258] <= branvar[258] + 1;
	  sb[21] <= 0;
	  end
	  5'h16: 
	    begin 
	   branvar[259] <= branvar[259] + 1;
	  sb[22] <= 0;
	  end
	  5'h17: 
	    begin 
	   branvar[260] <= branvar[260] + 1;
	  sb[23] <= 0;
	  end
	  5'h18: 
	    begin 
	   branvar[261] <= branvar[261] + 1;
	  sb[24] <= 0;
	  end
	  5'h19: 
	    begin 
	   branvar[262] <= branvar[262] + 1;
	  sb[25] <= 0;
	  end
	  5'h1a: 
	    begin 
	   branvar[263] <= branvar[263] + 1;
	  sb[26] <= 0;
	  end
	  5'h1b: 
	    begin 
	   branvar[264] <= branvar[264] + 1;
	  sb[27] <= 0;
	  end
	  5'h1c: 
	    begin 
	   branvar[265] <= branvar[265] + 1;
	  sb[28] <= 0;
	  end
	  5'h1d: 
	    begin 
	   branvar[266] <= branvar[266] + 1;
	  sb[29] <= 0;
	  end
	  5'h1e: 
	    begin 
	   branvar[267] <= branvar[267] + 1;
	  sb[30] <= 0;
	  end
	  5'h1f: 
	    begin 
	   branvar[268] <= branvar[268] + 1;
	  sb[31] <= 0;
	  end
	 endcase
	 end
	  end
	 else 
	 begin  
	   branvar[269] <= branvar[269] + 1;   
	   end 
	 if( next_sb_vld[2]) 
	  begin 
	 branvar[270] <= branvar[270] + 1; 
	 begin
	 case( next_sb_addr[2])
	  5'h0: 
	    begin 
	   branvar[271] <= branvar[271] + 1;
	  sb[0] <= 1;
	  end
	  5'h1: 
	    begin 
	   branvar[272] <= branvar[272] + 1;
	  sb[1] <= 1;
	  end
	  5'h2: 
	    begin 
	   branvar[273] <= branvar[273] + 1;
	  sb[2] <= 1;
	  end
	  5'h3: 
	    begin 
	   branvar[274] <= branvar[274] + 1;
	  sb[3] <= 1;
	  end
	  5'h4: 
	    begin 
	   branvar[275] <= branvar[275] + 1;
	  sb[4] <= 1;
	  end
	  5'h5: 
	    begin 
	   branvar[276] <= branvar[276] + 1;
	  sb[5] <= 1;
	  end
	  5'h6: 
	    begin 
	   branvar[277] <= branvar[277] + 1;
	  sb[6] <= 1;
	  end
	  5'h7: 
	    begin 
	   branvar[278] <= branvar[278] + 1;
	  sb[7] <= 1;
	  end
	  5'h8: 
	    begin 
	   branvar[279] <= branvar[279] + 1;
	  sb[8] <= 1;
	  end
	  5'h9: 
	    begin 
	   branvar[280] <= branvar[280] + 1;
	  sb[9] <= 1;
	  end
	  5'ha: 
	    begin 
	   branvar[281] <= branvar[281] + 1;
	  sb[10] <= 1;
	  end
	  5'hb: 
	    begin 
	   branvar[282] <= branvar[282] + 1;
	  sb[11] <= 1;
	  end
	  5'hc: 
	    begin 
	   branvar[283] <= branvar[283] + 1;
	  sb[12] <= 1;
	  end
	  5'hd: 
	    begin 
	   branvar[284] <= branvar[284] + 1;
	  sb[13] <= 1;
	  end
	  5'he: 
	    begin 
	   branvar[285] <= branvar[285] + 1;
	  sb[14] <= 1;
	  end
	  5'hf: 
	    begin 
	   branvar[286] <= branvar[286] + 1;
	  sb[15] <= 1;
	  end
	  5'h10: 
	    begin 
	   branvar[287] <= branvar[287] + 1;
	  sb[16] <= 1;
	  end
	  5'h11: 
	    begin 
	   branvar[288] <= branvar[288] + 1;
	  sb[17] <= 1;
	  end
	  5'h12: 
	    begin 
	   branvar[289] <= branvar[289] + 1;
	  sb[18] <= 1;
	  end
	  5'h13: 
	    begin 
	   branvar[290] <= branvar[290] + 1;
	  sb[19] <= 1;
	  end
	  5'h14: 
	    begin 
	   branvar[291] <= branvar[291] + 1;
	  sb[20] <= 1;
	  end
	  5'h15: 
	    begin 
	   branvar[292] <= branvar[292] + 1;
	  sb[21] <= 1;
	  end
	  5'h16: 
	    begin 
	   branvar[293] <= branvar[293] + 1;
	  sb[22] <= 1;
	  end
	  5'h17: 
	    begin 
	   branvar[294] <= branvar[294] + 1;
	  sb[23] <= 1;
	  end
	  5'h18: 
	    begin 
	   branvar[295] <= branvar[295] + 1;
	  sb[24] <= 1;
	  end
	  5'h19: 
	    begin 
	   branvar[296] <= branvar[296] + 1;
	  sb[25] <= 1;
	  end
	  5'h1a: 
	    begin 
	   branvar[297] <= branvar[297] + 1;
	  sb[26] <= 1;
	  end
	  5'h1b: 
	    begin 
	   branvar[298] <= branvar[298] + 1;
	  sb[27] <= 1;
	  end
	  5'h1c: 
	    begin 
	   branvar[299] <= branvar[299] + 1;
	  sb[28] <= 1;
	  end
	  5'h1d: 
	    begin 
	   branvar[300] <= branvar[300] + 1;
	  sb[29] <= 1;
	  end
	  5'h1e: 
	    begin 
	   branvar[301] <= branvar[301] + 1;
	  sb[30] <= 1;
	  end
	  5'h1f: 
	    begin 
	   branvar[302] <= branvar[302] + 1;
	  sb[31] <= 1;
	  end
	 endcase
	 end
	  end
	 else 
	 begin  
	   branvar[303] <= branvar[303] + 1;   
	   end 
	 if( next_sb_vld[3]) 
	  begin 
	 branvar[304] <= branvar[304] + 1; 
	 begin
	 case( next_sb_addr[3])
	  5'h0: 
	    begin 
	   branvar[305] <= branvar[305] + 1;
	  sb[0] <= 1;
	  end
	  5'h1: 
	    begin 
	   branvar[306] <= branvar[306] + 1;
	  sb[1] <= 1;
	  end
	  5'h2: 
	    begin 
	   branvar[307] <= branvar[307] + 1;
	  sb[2] <= 1;
	  end
	  5'h3: 
	    begin 
	   branvar[308] <= branvar[308] + 1;
	  sb[3] <= 1;
	  end
	  5'h4: 
	    begin 
	   branvar[309] <= branvar[309] + 1;
	  sb[4] <= 1;
	  end
	  5'h5: 
	    begin 
	   branvar[310] <= branvar[310] + 1;
	  sb[5] <= 1;
	  end
	  5'h6: 
	    begin 
	   branvar[311] <= branvar[311] + 1;
	  sb[6] <= 1;
	  end
	  5'h7: 
	    begin 
	   branvar[312] <= branvar[312] + 1;
	  sb[7] <= 1;
	  end
	  5'h8: 
	    begin 
	   branvar[313] <= branvar[313] + 1;
	  sb[8] <= 1;
	  end
	  5'h9: 
	    begin 
	   branvar[314] <= branvar[314] + 1;
	  sb[9] <= 1;
	  end
	  5'ha: 
	    begin 
	   branvar[315] <= branvar[315] + 1;
	  sb[10] <= 1;
	  end
	  5'hb: 
	    begin 
	   branvar[316] <= branvar[316] + 1;
	  sb[11] <= 1;
	  end
	  5'hc: 
	    begin 
	   branvar[317] <= branvar[317] + 1;
	  sb[12] <= 1;
	  end
	  5'hd: 
	    begin 
	   branvar[318] <= branvar[318] + 1;
	  sb[13] <= 1;
	  end
	  5'he: 
	    begin 
	   branvar[319] <= branvar[319] + 1;
	  sb[14] <= 1;
	  end
	  5'hf: 
	    begin 
	   branvar[320] <= branvar[320] + 1;
	  sb[15] <= 1;
	  end
	  5'h10: 
	    begin 
	   branvar[321] <= branvar[321] + 1;
	  sb[16] <= 1;
	  end
	  5'h11: 
	    begin 
	   branvar[322] <= branvar[322] + 1;
	  sb[17] <= 1;
	  end
	  5'h12: 
	    begin 
	   branvar[323] <= branvar[323] + 1;
	  sb[18] <= 1;
	  end
	  5'h13: 
	    begin 
	   branvar[324] <= branvar[324] + 1;
	  sb[19] <= 1;
	  end
	  5'h14: 
	    begin 
	   branvar[325] <= branvar[325] + 1;
	  sb[20] <= 1;
	  end
	  5'h15: 
	    begin 
	   branvar[326] <= branvar[326] + 1;
	  sb[21] <= 1;
	  end
	  5'h16: 
	    begin 
	   branvar[327] <= branvar[327] + 1;
	  sb[22] <= 1;
	  end
	  5'h17: 
	    begin 
	   branvar[328] <= branvar[328] + 1;
	  sb[23] <= 1;
	  end
	  5'h18: 
	    begin 
	   branvar[329] <= branvar[329] + 1;
	  sb[24] <= 1;
	  end
	  5'h19: 
	    begin 
	   branvar[330] <= branvar[330] + 1;
	  sb[25] <= 1;
	  end
	  5'h1a: 
	    begin 
	   branvar[331] <= branvar[331] + 1;
	  sb[26] <= 1;
	  end
	  5'h1b: 
	    begin 
	   branvar[332] <= branvar[332] + 1;
	  sb[27] <= 1;
	  end
	  5'h1c: 
	    begin 
	   branvar[333] <= branvar[333] + 1;
	  sb[28] <= 1;
	  end
	  5'h1d: 
	    begin 
	   branvar[334] <= branvar[334] + 1;
	  sb[29] <= 1;
	  end
	  5'h1e: 
	    begin 
	   branvar[335] <= branvar[335] + 1;
	  sb[30] <= 1;
	  end
	  5'h1f: 
	    begin 
	   branvar[336] <= branvar[336] + 1;
	  sb[31] <= 1;
	  end
	 endcase
	 end
	  end
	 else 
	 begin  
	   branvar[337] <= branvar[337] + 1;   
	   end 
	 end
	  end
	  endmodule 

module rf_4r2w (clk , reset , enable , read_addr_0 , read_data_0 , read_enable_0 , read_addr_1 , read_data_1 , read_enable_1 , read_addr_2 , read_data_2 , read_enable_2 , read_addr_3 , read_data_3 , read_enable_3 , write_addr_0 , write_data_0 , write_enable_0 , write_addr_1 , write_data_1 , write_enable_1 );

	  input  clk;
	  input  reset;
	  input  enable;
	  input [4 : 0] read_addr_0;
	  output [31 : 0] read_data_0;
	  input  read_enable_0;
	  input [4 : 0] read_addr_1;
	  output [31 : 0] read_data_1;
	  input  read_enable_1;
	  input [4 : 0] read_addr_2;
	  output [31 : 0] read_data_2;
	  input  read_enable_2;
	  input [4 : 0] read_addr_3;
	  output [31 : 0] read_data_3;
	  input  read_enable_3;
	  input [4 : 0] write_addr_0;
	  input [31 : 0] write_data_0;
	  input  write_enable_0;
	  input [4 : 0] write_addr_1;
	  input [31 : 0] write_data_1;
	  input  write_enable_1;
	  reg [31 : 0] rf_0;
	  reg [7:0] branvar[0:100];
	  reg [31 : 0] rf_1;
	  reg [31 : 0] rf_2;
	  reg [31 : 0] rf_3;
	  reg [31 : 0] rf_4;
	  reg [31 : 0] rf_5;
	  reg [31 : 0] rf_6;
	  reg [31 : 0] rf_7;
	  reg [31 : 0] rf_8;
	  reg [31 : 0] rf_9;
	  reg [31 : 0] rf_10;
	  reg [31 : 0] rf_11;
	  reg [31 : 0] rf_12;
	  reg [31 : 0] rf_13;
	  reg [31 : 0] rf_14;
	  reg [31 : 0] rf_15;
	  reg [31 : 0] rf_16;
	  reg [31 : 0] rf_17;
	  reg [31 : 0] rf_18;
	  reg [31 : 0] rf_19;
	  reg [31 : 0] rf_20;
	  reg [31 : 0] rf_21;
	  reg [31 : 0] rf_22;
	  reg [31 : 0] rf_23;
	  reg [31 : 0] rf_24;
	  reg [31 : 0] rf_25;
	  reg [31 : 0] rf_26;
	  reg [31 : 0] rf_27;
	  reg [31 : 0] rf_28;
	  reg [31 : 0] rf_29;
	  reg [31 : 0] rf_30;
	  reg [31 : 0] rf_31;
	  wire we_0;
	  wire we_1;
	 always @ ( read_enable_0) or ( read_addr_0) or ( rf_0) or ( rf_1) or ( rf_2) or ( rf_3) or ( rf_4) or ( rf_5) or ( rf_6) or ( rf_7) or ( rf_8) or ( rf_9) or ( rf_10) or ( rf_11) or ( rf_12) or ( rf_13) or ( rf_14) or ( rf_15) or ( rf_16) or ( rf_17) or ( rf_18) or ( rf_19) or ( rf_20) or ( rf_21) or ( rf_22) or ( rf_23) or ( rf_24) or ( rf_25) or ( rf_26) or ( rf_27) or ( rf_28) or ( rf_29) or ( rf_30) or ( rf_31)
	 begin
	 if(( ( read_enable_0 && (  read_addr_0 != 5'b00000)) == 0)) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	  read_data_0 = 32'h0;
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( read_addr_0)
	  5'h0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	  read_data_0 =  rf_0;
	  end
	  5'h1: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	  read_data_0 =  rf_1;
	  end
	  5'h2: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	  read_data_0 =  rf_2;
	  end
	  5'h3: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	  read_data_0 =  rf_3;
	  end
	  5'h4: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	  read_data_0 =  rf_4;
	  end
	  5'h5: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	  read_data_0 =  rf_5;
	  end
	  5'h6: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	  read_data_0 =  rf_6;
	  end
	  5'h7: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	  read_data_0 =  rf_7;
	  end
	  5'h8: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	  read_data_0 =  rf_8;
	  end
	  5'h9: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	  read_data_0 =  rf_9;
	  end
	  5'h10: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	  read_data_0 =  rf_10;
	  end
	  5'h11: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	  read_data_0 =  rf_11;
	  end
	  5'h12: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	  read_data_0 =  rf_12;
	  end
	  5'h13: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	  read_data_0 =  rf_13;
	  end
	  5'h14: 
	    begin 
	   branvar[16] <= branvar[16] + 1;
	  read_data_0 =  rf_14;
	  end
	  5'h15: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	  read_data_0 =  rf_15;
	  end
	  5'h16: 
	    begin 
	   branvar[18] <= branvar[18] + 1;
	  read_data_0 =  rf_16;
	  end
	  5'h17: 
	    begin 
	   branvar[19] <= branvar[19] + 1;
	  read_data_0 =  rf_17;
	  end
	  5'h18: 
	    begin 
	   branvar[20] <= branvar[20] + 1;
	  read_data_0 =  rf_18;
	  end
	  5'h19: 
	    begin 
	   branvar[21] <= branvar[21] + 1;
	  read_data_0 =  rf_19;
	  end
	  5'h0: 
	    begin 
	   branvar[22] <= branvar[22] + 1;
	  read_data_0 =  rf_20;
	  end
	  5'h1: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	  read_data_0 =  rf_21;
	  end
	  5'h2: 
	    begin 
	   branvar[24] <= branvar[24] + 1;
	  read_data_0 =  rf_22;
	  end
	  5'h3: 
	    begin 
	   branvar[25] <= branvar[25] + 1;
	  read_data_0 =  rf_23;
	  end
	  5'h4: 
	    begin 
	   branvar[26] <= branvar[26] + 1;
	  read_data_0 =  rf_24;
	  end
	  5'h5: 
	    begin 
	   branvar[27] <= branvar[27] + 1;
	  read_data_0 =  rf_25;
	  end
	  5'h6: 
	    begin 
	   branvar[28] <= branvar[28] + 1;
	  read_data_0 =  rf_26;
	  end
	  5'h7: 
	    begin 
	   branvar[29] <= branvar[29] + 1;
	  read_data_0 =  rf_27;
	  end
	  5'h8: 
	    begin 
	   branvar[30] <= branvar[30] + 1;
	  read_data_0 =  rf_28;
	  end
	  5'h9: 
	    begin 
	   branvar[31] <= branvar[31] + 1;
	  read_data_0 =  rf_29;
	  end
	  5'h10: 
	    begin 
	   branvar[32] <= branvar[32] + 1;
	  read_data_0 =  rf_30;
	  end
	  5'h11: 
	    begin 
	   branvar[33] <= branvar[33] + 1;
	  read_data_0 =  rf_31;
	  end
	 endcase
	  end
	 end
	 always @ ( read_enable_1) or ( read_addr_1) or ( rf_0) or ( rf_1) or ( rf_2) or ( rf_3) or ( rf_4) or ( rf_5) or ( rf_6) or ( rf_7) or ( rf_8) or ( rf_9) or ( rf_10) or ( rf_11) or ( rf_12) or ( rf_13) or ( rf_14) or ( rf_15) or ( rf_16) or ( rf_17) or ( rf_18) or ( rf_19) or ( rf_20) or ( rf_21) or ( rf_22) or ( rf_23) or ( rf_24) or ( rf_25) or ( rf_26) or ( rf_27) or ( rf_28) or ( rf_29) or ( rf_30) or ( rf_31)
	 begin
	 if(( ( read_enable_1 && (  read_addr_1 != 5'b00000)) == 0)) 
	  begin 
	 branvar[34] <= branvar[34] + 1; 
	  read_data_1 = 32'h0;
	  end
	 else 
	   begin 
	   branvar[35] <= branvar[35] + 1;   
	 case( read_addr_1)
	  5'h0: 
	    begin 
	   branvar[36] <= branvar[36] + 1;
	  read_data_1 =  rf_0;
	  end
	  5'h1: 
	    begin 
	   branvar[37] <= branvar[37] + 1;
	  read_data_1 =  rf_1;
	  end
	  5'h2: 
	    begin 
	   branvar[38] <= branvar[38] + 1;
	  read_data_1 =  rf_2;
	  end
	  5'h3: 
	    begin 
	   branvar[39] <= branvar[39] + 1;
	  read_data_1 =  rf_3;
	  end
	  5'h4: 
	    begin 
	   branvar[40] <= branvar[40] + 1;
	  read_data_1 =  rf_4;
	  end
	  5'h5: 
	    begin 
	   branvar[41] <= branvar[41] + 1;
	  read_data_1 =  rf_5;
	  end
	  5'h6: 
	    begin 
	   branvar[42] <= branvar[42] + 1;
	  read_data_1 =  rf_6;
	  end
	  5'h7: 
	    begin 
	   branvar[43] <= branvar[43] + 1;
	  read_data_1 =  rf_7;
	  end
	  5'h8: 
	    begin 
	   branvar[44] <= branvar[44] + 1;
	  read_data_1 =  rf_8;
	  end
	  5'h9: 
	    begin 
	   branvar[45] <= branvar[45] + 1;
	  read_data_1 =  rf_9;
	  end
	  5'h10: 
	    begin 
	   branvar[46] <= branvar[46] + 1;
	  read_data_1 =  rf_10;
	  end
	  5'h11: 
	    begin 
	   branvar[47] <= branvar[47] + 1;
	  read_data_1 =  rf_11;
	  end
	  5'h12: 
	    begin 
	   branvar[48] <= branvar[48] + 1;
	  read_data_1 =  rf_12;
	  end
	  5'h13: 
	    begin 
	   branvar[49] <= branvar[49] + 1;
	  read_data_1 =  rf_13;
	  end
	  5'h14: 
	    begin 
	   branvar[50] <= branvar[50] + 1;
	  read_data_1 =  rf_14;
	  end
	  5'h15: 
	    begin 
	   branvar[51] <= branvar[51] + 1;
	  read_data_1 =  rf_15;
	  end
	  5'h16: 
	    begin 
	   branvar[52] <= branvar[52] + 1;
	  read_data_1 =  rf_16;
	  end
	  5'h17: 
	    begin 
	   branvar[53] <= branvar[53] + 1;
	  read_data_1 =  rf_17;
	  end
	  5'h18: 
	    begin 
	   branvar[54] <= branvar[54] + 1;
	  read_data_1 =  rf_18;
	  end
	  5'h19: 
	    begin 
	   branvar[55] <= branvar[55] + 1;
	  read_data_1 =  rf_19;
	  end
	  5'h0: 
	    begin 
	   branvar[56] <= branvar[56] + 1;
	  read_data_1 =  rf_20;
	  end
	  5'h1: 
	    begin 
	   branvar[57] <= branvar[57] + 1;
	  read_data_1 =  rf_21;
	  end
	  5'h2: 
	    begin 
	   branvar[58] <= branvar[58] + 1;
	  read_data_1 =  rf_22;
	  end
	  5'h3: 
	    begin 
	   branvar[59] <= branvar[59] + 1;
	  read_data_1 =  rf_23;
	  end
	  5'h4: 
	    begin 
	   branvar[60] <= branvar[60] + 1;
	  read_data_1 =  rf_24;
	  end
	  5'h5: 
	    begin 
	   branvar[61] <= branvar[61] + 1;
	  read_data_1 =  rf_25;
	  end
	  5'h6: 
	    begin 
	   branvar[62] <= branvar[62] + 1;
	  read_data_1 =  rf_26;
	  end
	  5'h7: 
	    begin 
	   branvar[63] <= branvar[63] + 1;
	  read_data_1 =  rf_27;
	  end
	  5'h8: 
	    begin 
	   branvar[64] <= branvar[64] + 1;
	  read_data_1 =  rf_28;
	  end
	  5'h9: 
	    begin 
	   branvar[65] <= branvar[65] + 1;
	  read_data_1 =  rf_29;
	  end
	  5'h10: 
	    begin 
	   branvar[66] <= branvar[66] + 1;
	  read_data_1 =  rf_30;
	  end
	  5'h11: 
	    begin 
	   branvar[67] <= branvar[67] + 1;
	  read_data_1 =  rf_31;
	  end
	 endcase
	  end
	 end
	 always @ ( read_enable_2) or ( read_addr_2) or ( rf_0) or ( rf_1) or ( rf_2) or ( rf_3) or ( rf_4) or ( rf_5) or ( rf_6) or ( rf_7) or ( rf_8) or ( rf_9) or ( rf_10) or ( rf_11) or ( rf_12) or ( rf_13) or ( rf_14) or ( rf_15) or ( rf_16) or ( rf_17) or ( rf_18) or ( rf_19) or ( rf_20) or ( rf_21) or ( rf_22) or ( rf_23) or ( rf_24) or ( rf_25) or ( rf_26) or ( rf_27) or ( rf_28) or ( rf_29) or ( rf_30) or ( rf_31)
	 begin
	 if(( ( read_enable_2 && (  read_addr_2 != 5'b00000)) == 0)) 
	  begin 
	 branvar[68] <= branvar[68] + 1; 
	  read_data_2 = 32'h0;
	  end
	 else 
	   begin 
	   branvar[69] <= branvar[69] + 1;   
	 case( read_addr_2)
	  5'h0: 
	    begin 
	   branvar[70] <= branvar[70] + 1;
	  read_data_2 =  rf_0;
	  end
	  5'h1: 
	    begin 
	   branvar[71] <= branvar[71] + 1;
	  read_data_2 =  rf_1;
	  end
	  5'h2: 
	    begin 
	   branvar[72] <= branvar[72] + 1;
	  read_data_2 =  rf_2;
	  end
	  5'h3: 
	    begin 
	   branvar[73] <= branvar[73] + 1;
	  read_data_2 =  rf_3;
	  end
	  5'h4: 
	    begin 
	   branvar[74] <= branvar[74] + 1;
	  read_data_2 =  rf_4;
	  end
	  5'h5: 
	    begin 
	   branvar[75] <= branvar[75] + 1;
	  read_data_2 =  rf_5;
	  end
	  5'h6: 
	    begin 
	   branvar[76] <= branvar[76] + 1;
	  read_data_2 =  rf_6;
	  end
	  5'h7: 
	    begin 
	   branvar[77] <= branvar[77] + 1;
	  read_data_2 =  rf_7;
	  end
	  5'h8: 
	    begin 
	   branvar[78] <= branvar[78] + 1;
	  read_data_2 =  rf_8;
	  end
	  5'h9: 
	    begin 
	   branvar[79] <= branvar[79] + 1;
	  read_data_2 =  rf_9;
	  end
	  5'h10: 
	    begin 
	   branvar[80] <= branvar[80] + 1;
	  read_data_2 =  rf_10;
	  end
	  5'h11: 
	    begin 
	   branvar[81] <= branvar[81] + 1;
	  read_data_2 =  rf_11;
	  end
	  5'h12: 
	    begin 
	   branvar[82] <= branvar[82] + 1;
	  read_data_2 =  rf_12;
	  end
	  5'h13: 
	    begin 
	   branvar[83] <= branvar[83] + 1;
	  read_data_2 =  rf_13;
	  end
	  5'h14: 
	    begin 
	   branvar[84] <= branvar[84] + 1;
	  read_data_2 =  rf_14;
	  end
	  5'h15: 
	    begin 
	   branvar[85] <= branvar[85] + 1;
	  read_data_2 =  rf_15;
	  end
	  5'h16: 
	    begin 
	   branvar[86] <= branvar[86] + 1;
	  read_data_2 =  rf_16;
	  end
	  5'h17: 
	    begin 
	   branvar[87] <= branvar[87] + 1;
	  read_data_2 =  rf_17;
	  end
	  5'h18: 
	    begin 
	   branvar[88] <= branvar[88] + 1;
	  read_data_2 =  rf_18;
	  end
	  5'h19: 
	    begin 
	   branvar[89] <= branvar[89] + 1;
	  read_data_2 =  rf_19;
	  end
	  5'h0: 
	    begin 
	   branvar[90] <= branvar[90] + 1;
	  read_data_2 =  rf_20;
	  end
	  5'h1: 
	    begin 
	   branvar[91] <= branvar[91] + 1;
	  read_data_2 =  rf_21;
	  end
	  5'h2: 
	    begin 
	   branvar[92] <= branvar[92] + 1;
	  read_data_2 =  rf_22;
	  end
	  5'h3: 
	    begin 
	   branvar[93] <= branvar[93] + 1;
	  read_data_2 =  rf_23;
	  end
	  5'h4: 
	    begin 
	   branvar[94] <= branvar[94] + 1;
	  read_data_2 =  rf_24;
	  end
	  5'h5: 
	    begin 
	   branvar[95] <= branvar[95] + 1;
	  read_data_2 =  rf_25;
	  end
	  5'h6: 
	    begin 
	   branvar[96] <= branvar[96] + 1;
	  read_data_2 =  rf_26;
	  end
	  5'h7: 
	    begin 
	   branvar[97] <= branvar[97] + 1;
	  read_data_2 =  rf_27;
	  end
	  5'h8: 
	    begin 
	   branvar[98] <= branvar[98] + 1;
	  read_data_2 =  rf_28;
	  end
	  5'h9: 
	    begin 
	   branvar[99] <= branvar[99] + 1;
	  read_data_2 =  rf_29;
	  end
	  5'h10: 
	    begin 
	   branvar[100] <= branvar[100] + 1;
	  read_data_2 =  rf_30;
	  end
	  5'h11: 
	    begin 
	   branvar[101] <= branvar[101] + 1;
	  read_data_2 =  rf_31;
	  end
	 endcase
	  end
	 end
	 always @ ( read_enable_3) or ( read_addr_3) or ( rf_0) or ( rf_1) or ( rf_2) or ( rf_3) or ( rf_4) or ( rf_5) or ( rf_6) or ( rf_7) or ( rf_8) or ( rf_9) or ( rf_10) or ( rf_11) or ( rf_12) or ( rf_13) or ( rf_14) or ( rf_15) or ( rf_16) or ( rf_17) or ( rf_18) or ( rf_19) or ( rf_20) or ( rf_21) or ( rf_22) or ( rf_23) or ( rf_24) or ( rf_25) or ( rf_26) or ( rf_27) or ( rf_28) or ( rf_29) or ( rf_30) or ( rf_31)
	 begin
	 if(( ( read_enable_3 && (  read_addr_3 != 5'b00000)) == 0)) 
	  begin 
	 branvar[102] <= branvar[102] + 1; 
	  read_data_3 = 32'h0;
	  end
	 else 
	   begin 
	   branvar[103] <= branvar[103] + 1;   
	 case( read_addr_3)
	  5'h0: 
	    begin 
	   branvar[104] <= branvar[104] + 1;
	  read_data_3 =  rf_0;
	  end
	  5'h1: 
	    begin 
	   branvar[105] <= branvar[105] + 1;
	  read_data_3 =  rf_1;
	  end
	  5'h2: 
	    begin 
	   branvar[106] <= branvar[106] + 1;
	  read_data_3 =  rf_2;
	  end
	  5'h3: 
	    begin 
	   branvar[107] <= branvar[107] + 1;
	  read_data_3 =  rf_3;
	  end
	  5'h4: 
	    begin 
	   branvar[108] <= branvar[108] + 1;
	  read_data_3 =  rf_4;
	  end
	  5'h5: 
	    begin 
	   branvar[109] <= branvar[109] + 1;
	  read_data_3 =  rf_5;
	  end
	  5'h6: 
	    begin 
	   branvar[110] <= branvar[110] + 1;
	  read_data_3 =  rf_6;
	  end
	  5'h7: 
	    begin 
	   branvar[111] <= branvar[111] + 1;
	  read_data_3 =  rf_7;
	  end
	  5'h8: 
	    begin 
	   branvar[112] <= branvar[112] + 1;
	  read_data_3 =  rf_8;
	  end
	  5'h9: 
	    begin 
	   branvar[113] <= branvar[113] + 1;
	  read_data_3 =  rf_9;
	  end
	  5'h10: 
	    begin 
	   branvar[114] <= branvar[114] + 1;
	  read_data_3 =  rf_10;
	  end
	  5'h11: 
	    begin 
	   branvar[115] <= branvar[115] + 1;
	  read_data_3 =  rf_11;
	  end
	  5'h12: 
	    begin 
	   branvar[116] <= branvar[116] + 1;
	  read_data_3 =  rf_12;
	  end
	  5'h13: 
	    begin 
	   branvar[117] <= branvar[117] + 1;
	  read_data_3 =  rf_13;
	  end
	  5'h14: 
	    begin 
	   branvar[118] <= branvar[118] + 1;
	  read_data_3 =  rf_14;
	  end
	  5'h15: 
	    begin 
	   branvar[119] <= branvar[119] + 1;
	  read_data_3 =  rf_15;
	  end
	  5'h16: 
	    begin 
	   branvar[120] <= branvar[120] + 1;
	  read_data_3 =  rf_16;
	  end
	  5'h17: 
	    begin 
	   branvar[121] <= branvar[121] + 1;
	  read_data_3 =  rf_17;
	  end
	  5'h18: 
	    begin 
	   branvar[122] <= branvar[122] + 1;
	  read_data_3 =  rf_18;
	  end
	  5'h19: 
	    begin 
	   branvar[123] <= branvar[123] + 1;
	  read_data_3 =  rf_19;
	  end
	  5'h0: 
	    begin 
	   branvar[124] <= branvar[124] + 1;
	  read_data_3 =  rf_20;
	  end
	  5'h1: 
	    begin 
	   branvar[125] <= branvar[125] + 1;
	  read_data_3 =  rf_21;
	  end
	  5'h2: 
	    begin 
	   branvar[126] <= branvar[126] + 1;
	  read_data_3 =  rf_22;
	  end
	  5'h3: 
	    begin 
	   branvar[127] <= branvar[127] + 1;
	  read_data_3 =  rf_23;
	  end
	  5'h4: 
	    begin 
	   branvar[128] <= branvar[128] + 1;
	  read_data_3 =  rf_24;
	  end
	  5'h5: 
	    begin 
	   branvar[129] <= branvar[129] + 1;
	  read_data_3 =  rf_25;
	  end
	  5'h6: 
	    begin 
	   branvar[130] <= branvar[130] + 1;
	  read_data_3 =  rf_26;
	  end
	  5'h7: 
	    begin 
	   branvar[131] <= branvar[131] + 1;
	  read_data_3 =  rf_27;
	  end
	  5'h8: 
	    begin 
	   branvar[132] <= branvar[132] + 1;
	  read_data_3 =  rf_28;
	  end
	  5'h9: 
	    begin 
	   branvar[133] <= branvar[133] + 1;
	  read_data_3 =  rf_29;
	  end
	  5'h10: 
	    begin 
	   branvar[134] <= branvar[134] + 1;
	  read_data_3 =  rf_30;
	  end
	  5'h11: 
	    begin 
	   branvar[135] <= branvar[135] + 1;
	  read_data_3 =  rf_31;
	  end
	 endcase
	  end
	 end
	 assign 
	  we_0 = ( write_enable_0 && (! (  write_addr_0 == 5'b00000)));
	 assign 
	  we_1 = ( write_enable_1 && (! (  write_addr_1 == 5'b00000)));
	 always @ (posedge  clk) or (posedge  reset)
	 begin
	 if((  reset == 1)) 
	  begin 
	 branvar[136] <= branvar[136] + 1; 
	 begin
	  rf_0 <= 32'h0;
	  rf_1 <= 32'h0;
	  rf_2 <= 32'h0;
	  rf_3 <= 32'h0;
	  rf_4 <= 32'h0;
	  rf_5 <= 32'h0;
	  rf_6 <= 32'h0;
	  rf_7 <= 32'h0;
	  rf_8 <= 32'h0;
	  rf_9 <= 32'h0;
	  rf_10 <= 32'h0;
	  rf_11 <= 32'h0;
	  rf_12 <= 32'h0;
	  rf_13 <= 32'h0;
	  rf_14 <= 32'h0;
	  rf_15 <= 32'h0;
	  rf_16 <= 32'h0;
	  rf_17 <= 32'h0;
	  rf_18 <= 32'h0;
	  rf_19 <= 32'h0;
	  rf_20 <= 32'h0;
	  rf_21 <= 32'h0;
	  rf_22 <= 32'h0;
	  rf_23 <= 32'h0;
	  rf_24 <= 32'h0;
	  rf_25 <= 32'h0;
	  rf_26 <= 32'h0;
	  rf_27 <= 32'h0;
	  rf_28 <= 32'h0;
	  rf_29 <= 32'h0;
	  rf_30 <= 32'h0;
	  rf_31 <= 32'h0;
	 end
	  end
	 else 
	   begin 
	   branvar[137] <= branvar[137] + 1;   
	 begin
	 if( we_0) 
	  begin 
	 branvar[138] <= branvar[138] + 1; 
	 begin
	 case( write_addr_0)
	  5'h0: 
	    begin 
	   branvar[139] <= branvar[139] + 1;
	  rf_0 <=  write_data_0;
	  end
	  5'h1: 
	    begin 
	   branvar[140] <= branvar[140] + 1;
	  rf_1 <=  write_data_0;
	  end
	  5'h2: 
	    begin 
	   branvar[141] <= branvar[141] + 1;
	  rf_2 <=  write_data_0;
	  end
	  5'h3: 
	    begin 
	   branvar[142] <= branvar[142] + 1;
	  rf_3 <=  write_data_0;
	  end
	  5'h4: 
	    begin 
	   branvar[143] <= branvar[143] + 1;
	  rf_4 <=  write_data_0;
	  end
	  5'h5: 
	    begin 
	   branvar[144] <= branvar[144] + 1;
	  rf_5 <=  write_data_0;
	  end
	  5'h6: 
	    begin 
	   branvar[145] <= branvar[145] + 1;
	  rf_6 <=  write_data_0;
	  end
	  5'h7: 
	    begin 
	   branvar[146] <= branvar[146] + 1;
	  rf_7 <=  write_data_0;
	  end
	  5'h8: 
	    begin 
	   branvar[147] <= branvar[147] + 1;
	  rf_8 <=  write_data_0;
	  end
	  5'h9: 
	    begin 
	   branvar[148] <= branvar[148] + 1;
	  rf_9 <=  write_data_0;
	  end
	  5'h10: 
	    begin 
	   branvar[149] <= branvar[149] + 1;
	  rf_10 <=  write_data_0;
	  end
	  5'h11: 
	    begin 
	   branvar[150] <= branvar[150] + 1;
	  rf_11 <=  write_data_0;
	  end
	  5'h12: 
	    begin 
	   branvar[151] <= branvar[151] + 1;
	  rf_12 <=  write_data_0;
	  end
	  5'h13: 
	    begin 
	   branvar[152] <= branvar[152] + 1;
	  rf_13 <=  write_data_0;
	  end
	  5'h14: 
	    begin 
	   branvar[153] <= branvar[153] + 1;
	  rf_14 <=  write_data_0;
	  end
	  5'h15: 
	    begin 
	   branvar[154] <= branvar[154] + 1;
	  rf_15 <=  write_data_0;
	  end
	  5'h16: 
	    begin 
	   branvar[155] <= branvar[155] + 1;
	  rf_16 <=  write_data_0;
	  end
	  5'h17: 
	    begin 
	   branvar[156] <= branvar[156] + 1;
	  rf_17 <=  write_data_0;
	  end
	  5'h18: 
	    begin 
	   branvar[157] <= branvar[157] + 1;
	  rf_18 <=  write_data_0;
	  end
	  5'h19: 
	    begin 
	   branvar[158] <= branvar[158] + 1;
	  rf_19 <=  write_data_0;
	  end
	  5'h0: 
	    begin 
	   branvar[159] <= branvar[159] + 1;
	  rf_20 <=  write_data_0;
	  end
	  5'h1: 
	    begin 
	   branvar[160] <= branvar[160] + 1;
	  rf_21 <=  write_data_0;
	  end
	  5'h2: 
	    begin 
	   branvar[161] <= branvar[161] + 1;
	  rf_22 <=  write_data_0;
	  end
	  5'h3: 
	    begin 
	   branvar[162] <= branvar[162] + 1;
	  rf_23 <=  write_data_0;
	  end
	  5'h4: 
	    begin 
	   branvar[163] <= branvar[163] + 1;
	  rf_24 <=  write_data_0;
	  end
	  5'h5: 
	    begin 
	   branvar[164] <= branvar[164] + 1;
	  rf_25 <=  write_data_0;
	  end
	  5'h6: 
	    begin 
	   branvar[165] <= branvar[165] + 1;
	  rf_26 <=  write_data_0;
	  end
	  5'h7: 
	    begin 
	   branvar[166] <= branvar[166] + 1;
	  rf_27 <=  write_data_0;
	  end
	  5'h8: 
	    begin 
	   branvar[167] <= branvar[167] + 1;
	  rf_28 <=  write_data_0;
	  end
	  5'h9: 
	    begin 
	   branvar[168] <= branvar[168] + 1;
	  rf_29 <=  write_data_0;
	  end
	  5'h10: 
	    begin 
	   branvar[169] <= branvar[169] + 1;
	  rf_30 <=  write_data_0;
	  end
	  5'h11: 
	    begin 
	   branvar[170] <= branvar[170] + 1;
	  rf_31 <=  write_data_0;
	  end
	 endcase
	 end
	  end
	 else 
	 begin  
	   branvar[171] <= branvar[171] + 1;   
	   end 
	 if( we_1) 
	  begin 
	 branvar[172] <= branvar[172] + 1; 
	 begin
	 case( write_addr_1)
	  5'h0: 
	    begin 
	   branvar[173] <= branvar[173] + 1;
	  rf_0 <=  write_data_1;
	  end
	  5'h1: 
	    begin 
	   branvar[174] <= branvar[174] + 1;
	  rf_1 <=  write_data_1;
	  end
	  5'h2: 
	    begin 
	   branvar[175] <= branvar[175] + 1;
	  rf_2 <=  write_data_1;
	  end
	  5'h3: 
	    begin 
	   branvar[176] <= branvar[176] + 1;
	  rf_3 <=  write_data_1;
	  end
	  5'h4: 
	    begin 
	   branvar[177] <= branvar[177] + 1;
	  rf_4 <=  write_data_1;
	  end
	  5'h5: 
	    begin 
	   branvar[178] <= branvar[178] + 1;
	  rf_5 <=  write_data_1;
	  end
	  5'h6: 
	    begin 
	   branvar[179] <= branvar[179] + 1;
	  rf_6 <=  write_data_1;
	  end
	  5'h7: 
	    begin 
	   branvar[180] <= branvar[180] + 1;
	  rf_7 <=  write_data_1;
	  end
	  5'h8: 
	    begin 
	   branvar[181] <= branvar[181] + 1;
	  rf_8 <=  write_data_1;
	  end
	  5'h9: 
	    begin 
	   branvar[182] <= branvar[182] + 1;
	  rf_9 <=  write_data_1;
	  end
	  5'h10: 
	    begin 
	   branvar[183] <= branvar[183] + 1;
	  rf_10 <=  write_data_1;
	  end
	  5'h11: 
	    begin 
	   branvar[184] <= branvar[184] + 1;
	  rf_11 <=  write_data_1;
	  end
	  5'h12: 
	    begin 
	   branvar[185] <= branvar[185] + 1;
	  rf_12 <=  write_data_1;
	  end
	  5'h13: 
	    begin 
	   branvar[186] <= branvar[186] + 1;
	  rf_13 <=  write_data_1;
	  end
	  5'h14: 
	    begin 
	   branvar[187] <= branvar[187] + 1;
	  rf_14 <=  write_data_1;
	  end
	  5'h15: 
	    begin 
	   branvar[188] <= branvar[188] + 1;
	  rf_15 <=  write_data_1;
	  end
	  5'h16: 
	    begin 
	   branvar[189] <= branvar[189] + 1;
	  rf_16 <=  write_data_1;
	  end
	  5'h17: 
	    begin 
	   branvar[190] <= branvar[190] + 1;
	  rf_17 <=  write_data_1;
	  end
	  5'h18: 
	    begin 
	   branvar[191] <= branvar[191] + 1;
	  rf_18 <=  write_data_1;
	  end
	  5'h19: 
	    begin 
	   branvar[192] <= branvar[192] + 1;
	  rf_19 <=  write_data_1;
	  end
	  5'h0: 
	    begin 
	   branvar[193] <= branvar[193] + 1;
	  rf_20 <=  write_data_1;
	  end
	  5'h1: 
	    begin 
	   branvar[194] <= branvar[194] + 1;
	  rf_21 <=  write_data_1;
	  end
	  5'h2: 
	    begin 
	   branvar[195] <= branvar[195] + 1;
	  rf_22 <=  write_data_1;
	  end
	  5'h3: 
	    begin 
	   branvar[196] <= branvar[196] + 1;
	  rf_23 <=  write_data_1;
	  end
	  5'h4: 
	    begin 
	   branvar[197] <= branvar[197] + 1;
	  rf_24 <=  write_data_1;
	  end
	  5'h5: 
	    begin 
	   branvar[198] <= branvar[198] + 1;
	  rf_25 <=  write_data_1;
	  end
	  5'h6: 
	    begin 
	   branvar[199] <= branvar[199] + 1;
	  rf_26 <=  write_data_1;
	  end
	  5'h7: 
	    begin 
	   branvar[200] <= branvar[200] + 1;
	  rf_27 <=  write_data_1;
	  end
	  5'h8: 
	    begin 
	   branvar[201] <= branvar[201] + 1;
	  rf_28 <=  write_data_1;
	  end
	  5'h9: 
	    begin 
	   branvar[202] <= branvar[202] + 1;
	  rf_29 <=  write_data_1;
	  end
	  5'h10: 
	    begin 
	   branvar[203] <= branvar[203] + 1;
	  rf_30 <=  write_data_1;
	  end
	  5'h11: 
	    begin 
	   branvar[204] <= branvar[204] + 1;
	  rf_31 <=  write_data_1;
	  end
	 endcase
	 end
	  end
	 else 
	 begin  
	   branvar[205] <= branvar[205] + 1;   
	   end 
	 end
	  end
	 end
	  endmodule 

module sprf (clk , reset , read_addr , read_data , read_enable , write_enable );

	  input  clk;
	  input  reset;
	  input [3 : 0] read_addr;
	  output [31 : 0] read_data;
	  input  read_enable;
	  input  write_enable;
	  reg [31 : 0] rf_0;
	  reg [7:0] branvar[0:100];
	  reg [31 : 0] rf_1;
	  reg [31 : 0] rf_2;
	  reg [31 : 0] rf_3;
	  reg [31 : 0] rf_4;
	  reg [31 : 0] rf_5;
	  reg [31 : 0] rf_6;
	  reg [31 : 0] rf_7;
	  reg [31 : 0] rf_8;
	  reg [31 : 0] rf_9;
	  reg [31 : 0] rf_10;
	  reg [31 : 0] rf_11;
	  reg [31 : 0] rf_12;
	  reg [31 : 0] rf_13;
	  reg [31 : 0] rf_14;
	  reg [31 : 0] rf_15;
	 always @ ( read_addr) or ( rf_0) or ( rf_1) or ( rf_2) or ( rf_3) or ( rf_4) or ( rf_5) or ( rf_6) or ( rf_7) or ( rf_8) or ( rf_9) or ( rf_10) or ( rf_11) or ( rf_12) or ( rf_13) or ( rf_14) or ( rf_15)
	 case( read_addr)
	  5'h0: 
	    begin 
	   branvar[0] <= branvar[0] + 1;
	  read_data =  rf_0;
	  end
	  5'h1: 
	    begin 
	   branvar[1] <= branvar[1] + 1;
	  read_data =  rf_1;
	  end
	  5'h2: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	  read_data =  rf_2;
	  end
	  5'h3: 
	    begin 
	   branvar[3] <= branvar[3] + 1;
	  read_data =  rf_3;
	  end
	  5'h4: 
	    begin 
	   branvar[4] <= branvar[4] + 1;
	  read_data =  rf_4;
	  end
	  5'h5: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	  read_data =  rf_5;
	  end
	  5'h6: 
	    begin 
	   branvar[6] <= branvar[6] + 1;
	  read_data =  rf_6;
	  end
	  5'h7: 
	    begin 
	   branvar[7] <= branvar[7] + 1;
	  read_data =  rf_7;
	  end
	  5'h8: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	  read_data =  rf_8;
	  end
	  5'h9: 
	    begin 
	   branvar[9] <= branvar[9] + 1;
	  read_data =  rf_9;
	  end
	  5'h10: 
	    begin 
	   branvar[10] <= branvar[10] + 1;
	  read_data =  rf_10;
	  end
	  5'h11: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	  read_data =  rf_11;
	  end
	  5'h12: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	  read_data =  rf_12;
	  end
	  5'h13: 
	    begin 
	   branvar[13] <= branvar[13] + 1;
	  read_data =  rf_13;
	  end
	  5'h14: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	  read_data =  rf_14;
	  end
	  5'h15: 
	    begin 
	   branvar[15] <= branvar[15] + 1;
	  read_data =  rf_15;
	  end
	 endcase
	 always @ (posedge  clk) or (posedge  reset)
	 if((  reset == 1'b1)) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  rf_0 <= 32'h0;
	  rf_1 <= 32'h100;
	  rf_2 <= 32'h8;
	  rf_3 <= 32'h10;
	  rf_4 <= 32'h0;
	  rf_5 <= 32'h100;
	  rf_6 <= 32'h0;
	  rf_7 <= 32'h1000;
	  rf_8 <= 32'h4;
	  rf_9 <= 32'hff;
	  rf_10 <= 32'h200;
	 end
	  end
	 else 
	 begin  
	   branvar[17] <= branvar[17] + 1;   
	   end 
	  endmodule 

module bypass_select0 (address_in_rf , data_in_rf , data_in_bpaddress_0 , data_in_bpdata_0 , data_in_bpvalid_0 , data_in_bpaddress_1 , data_in_bpdata_1 , data_in_bpvalid_1 , data_in_bpaddress_2 , data_in_bpdata_2 , data_in_bpvalid_2 , data_in_bpaddress_3 , data_in_bpdata_3 , data_in_bpvalid_3 , data_in_bpaddress_4 , data_in_bpdata_4 , data_in_bpvalid_4 , data_in_bpaddress_5 , data_in_bpdata_5 , data_in_bpvalid_5 , data_in_bpaddress_6 , data_in_bpdata_6 , data_in_bpvalid_6 , data_in_bpaddress_7 , data_in_bpdata_7 , data_in_bpvalid_7 , data_in_bpaddress_8 , data_in_bpdata_8 , data_in_bpvalid_8 , data_in_bpaddress_9 , data_in_bpdata_9 , data_in_bpvalid_9 , data_in_bpaddress_10 , data_in_bpdata_10 , data_in_bpvalid_10 , data_out , src_is_bp );

	  input [4 : 0] address_in_rf;
	  input [31 : 0] data_in_rf;
	  input [4 : 0] data_in_bpaddress_0;
	  input [31 : 0] data_in_bpdata_0;
	  input  data_in_bpvalid_0;
	  input [4 : 0] data_in_bpaddress_1;
	  input [31 : 0] data_in_bpdata_1;
	  input  data_in_bpvalid_1;
	  input [4 : 0] data_in_bpaddress_2;
	  input [31 : 0] data_in_bpdata_2;
	  input  data_in_bpvalid_2;
	  input [4 : 0] data_in_bpaddress_3;
	  input [31 : 0] data_in_bpdata_3;
	  input  data_in_bpvalid_3;
	  input [4 : 0] data_in_bpaddress_4;
	  input [31 : 0] data_in_bpdata_4;
	  input  data_in_bpvalid_4;
	  input [4 : 0] data_in_bpaddress_5;
	  input [31 : 0] data_in_bpdata_5;
	  input  data_in_bpvalid_5;
	  input [4 : 0] data_in_bpaddress_6;
	  input [31 : 0] data_in_bpdata_6;
	  input  data_in_bpvalid_6;
	  input [4 : 0] data_in_bpaddress_7;
	  input [31 : 0] data_in_bpdata_7;
	  input  data_in_bpvalid_7;
	  input [4 : 0] data_in_bpaddress_8;
	  input [31 : 0] data_in_bpdata_8;
	  input  data_in_bpvalid_8;
	  input [4 : 0] data_in_bpaddress_9;
	  input [31 : 0] data_in_bpdata_9;
	  input  data_in_bpvalid_9;
	  input [4 : 0] data_in_bpaddress_10;
	  input [31 : 0] data_in_bpdata_10;
	  input  data_in_bpvalid_10;
	  output [31 : 0] data_out;
	  output src_is_bp ;
	 always @ ( data_in_bpvalid_0) or ( data_in_bpaddress_0) or ( address_in_rf) or ( data_in_bpaddress_0) or ( data_in_bpdata_0) or ( data_in_bpvalid_1) or ( data_in_bpaddress_1) or ( address_in_rf) or ( data_in_bpaddress_1) or ( data_in_bpdata_1) or ( data_in_bpvalid_2) or ( data_in_bpaddress_2) or ( address_in_rf) or ( data_in_bpaddress_2) or ( data_in_bpdata_2) or ( data_in_bpvalid_3) or ( data_in_bpaddress_3) or ( address_in_rf) or ( data_in_bpaddress_3) or ( data_in_bpdata_3) or ( data_in_bpvalid_4) or ( data_in_bpaddress_4) or ( address_in_rf) or ( data_in_bpaddress_4) or ( data_in_bpdata_4) or ( data_in_bpvalid_5) or ( data_in_bpaddress_5) or ( address_in_rf) or ( data_in_bpaddress_5) or ( data_in_bpdata_5) or ( data_in_bpvalid_6) or ( data_in_bpaddress_6) or ( address_in_rf) or ( data_in_bpaddress_6) or ( data_in_bpdata_6) or ( data_in_bpvalid_7) or ( data_in_bpaddress_7) or ( address_in_rf) or ( data_in_bpaddress_7) or ( data_in_bpdata_7) or ( data_in_bpvalid_8) or ( data_in_bpaddress_8) or ( address_in_rf) or ( data_in_bpaddress_8) or ( data_in_bpdata_8) or ( data_in_bpvalid_9) or ( data_in_bpaddress_9) or ( address_in_rf) or ( data_in_bpaddress_9) or ( data_in_bpdata_9) or ( data_in_bpvalid_10) or ( data_in_bpaddress_10) or ( address_in_rf) or ( data_in_bpaddress_10) or ( data_in_bpdata_10)
	 begin
	 if((( data_in_bpvalid_0 && (  data_in_bpaddress_0 ==  address_in_rf)) && (  data_in_bpaddress_0 != 5'b00000))) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  data_out =  data_in_bpdata_0;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 if((( data_in_bpvalid_1 && (  data_in_bpaddress_1 ==  address_in_rf)) && (  data_in_bpaddress_1 != 5'b00000))) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  data_out =  data_in_bpdata_1;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	 if((( data_in_bpvalid_2 && (  data_in_bpaddress_2 ==  address_in_rf)) && (  data_in_bpaddress_2 != 5'b00000))) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  data_out =  data_in_bpdata_2;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 if((( data_in_bpvalid_3 && (  data_in_bpaddress_3 ==  address_in_rf)) && (  data_in_bpaddress_3 != 5'b00000))) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  data_out =  data_in_bpdata_3;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 if((( data_in_bpvalid_4 && (  data_in_bpaddress_4 ==  address_in_rf)) && (  data_in_bpaddress_4 != 5'b00000))) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	 begin
	  data_out =  data_in_bpdata_4;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	 if((( data_in_bpvalid_5 && (  data_in_bpaddress_5 ==  address_in_rf)) && (  data_in_bpaddress_5 != 5'b00000))) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  data_out =  data_in_bpdata_5;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 if((( data_in_bpvalid_6 && (  data_in_bpaddress_6 ==  address_in_rf)) && (  data_in_bpaddress_6 != 5'b00000))) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  data_out =  data_in_bpdata_6;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 if((( data_in_bpvalid_7 && (  data_in_bpaddress_7 ==  address_in_rf)) && (  data_in_bpaddress_7 != 5'b00000))) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  data_out =  data_in_bpdata_7;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if((( data_in_bpvalid_8 && (  data_in_bpaddress_8 ==  address_in_rf)) && (  data_in_bpaddress_8 != 5'b00000))) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  data_out =  data_in_bpdata_8;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 if((( data_in_bpvalid_9 && (  data_in_bpaddress_9 ==  address_in_rf)) && (  data_in_bpaddress_9 != 5'b00000))) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  data_out =  data_in_bpdata_9;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 if((( data_in_bpvalid_10 && (  data_in_bpaddress_10 ==  address_in_rf)) && (  data_in_bpaddress_10 != 5'b00000))) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  data_out =  data_in_bpdata_10;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	 begin
	  data_out =  data_in_rf;
	  src_is_bp = 1'b0;
	 end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	 end
	  endmodule 

module bypass_select1 (address_in_rf , data_in_rf , data_in_bpaddress_0 , data_in_bpdata_0 , data_in_bpvalid_0 , data_in_bpaddress_1 , data_in_bpdata_1 , data_in_bpvalid_1 , data_in_bpaddress_2 , data_in_bpdata_2 , data_in_bpvalid_2 , data_in_bpaddress_3 , data_in_bpdata_3 , data_in_bpvalid_3 , data_in_bpaddress_4 , data_in_bpdata_4 , data_in_bpvalid_4 , data_in_bpaddress_5 , data_in_bpdata_5 , data_in_bpvalid_5 , data_in_bpaddress_6 , data_in_bpdata_6 , data_in_bpvalid_6 , data_in_bpaddress_7 , data_in_bpdata_7 , data_in_bpvalid_7 , data_in_bpaddress_8 , data_in_bpdata_8 , data_in_bpvalid_8 , data_in_bpaddress_9 , data_in_bpdata_9 , data_in_bpvalid_9 , data_in_bpaddress_10 , data_in_bpdata_10 , data_in_bpvalid_10 , data_out , src_is_bp );

	  input [4 : 0] address_in_rf;
	  input [31 : 0] data_in_rf;
	  input [4 : 0] data_in_bpaddress_0;
	  input [31 : 0] data_in_bpdata_0;
	  input  data_in_bpvalid_0;
	  input [4 : 0] data_in_bpaddress_1;
	  input [31 : 0] data_in_bpdata_1;
	  input  data_in_bpvalid_1;
	  input [4 : 0] data_in_bpaddress_2;
	  input [31 : 0] data_in_bpdata_2;
	  input  data_in_bpvalid_2;
	  input [4 : 0] data_in_bpaddress_3;
	  input [31 : 0] data_in_bpdata_3;
	  input  data_in_bpvalid_3;
	  input [4 : 0] data_in_bpaddress_4;
	  input [31 : 0] data_in_bpdata_4;
	  input  data_in_bpvalid_4;
	  input [4 : 0] data_in_bpaddress_5;
	  input [31 : 0] data_in_bpdata_5;
	  input  data_in_bpvalid_5;
	  input [4 : 0] data_in_bpaddress_6;
	  input [31 : 0] data_in_bpdata_6;
	  input  data_in_bpvalid_6;
	  input [4 : 0] data_in_bpaddress_7;
	  input [31 : 0] data_in_bpdata_7;
	  input  data_in_bpvalid_7;
	  input [4 : 0] data_in_bpaddress_8;
	  input [31 : 0] data_in_bpdata_8;
	  input  data_in_bpvalid_8;
	  input [4 : 0] data_in_bpaddress_9;
	  input [31 : 0] data_in_bpdata_9;
	  input  data_in_bpvalid_9;
	  input [4 : 0] data_in_bpaddress_10;
	  input [31 : 0] data_in_bpdata_10;
	  input  data_in_bpvalid_10;
	  output [31 : 0] data_out;
	  output src_is_bp ;
	 always @ ( data_in_bpvalid_0) or ( data_in_bpaddress_0) or ( address_in_rf) or ( data_in_bpaddress_0) or ( data_in_bpdata_0) or ( data_in_bpvalid_1) or ( data_in_bpaddress_1) or ( address_in_rf) or ( data_in_bpaddress_1) or ( data_in_bpdata_1) or ( data_in_bpvalid_2) or ( data_in_bpaddress_2) or ( address_in_rf) or ( data_in_bpaddress_2) or ( data_in_bpdata_2) or ( data_in_bpvalid_3) or ( data_in_bpaddress_3) or ( address_in_rf) or ( data_in_bpaddress_3) or ( data_in_bpdata_3) or ( data_in_bpvalid_4) or ( data_in_bpaddress_4) or ( address_in_rf) or ( data_in_bpaddress_4) or ( data_in_bpdata_4) or ( data_in_bpvalid_5) or ( data_in_bpaddress_5) or ( address_in_rf) or ( data_in_bpaddress_5) or ( data_in_bpdata_5) or ( data_in_bpvalid_6) or ( data_in_bpaddress_6) or ( address_in_rf) or ( data_in_bpaddress_6) or ( data_in_bpdata_6) or ( data_in_bpvalid_7) or ( data_in_bpaddress_7) or ( address_in_rf) or ( data_in_bpaddress_7) or ( data_in_bpdata_7) or ( data_in_bpvalid_8) or ( data_in_bpaddress_8) or ( address_in_rf) or ( data_in_bpaddress_8) or ( data_in_bpdata_8) or ( data_in_bpvalid_9) or ( data_in_bpaddress_9) or ( address_in_rf) or ( data_in_bpaddress_9) or ( data_in_bpdata_9) or ( data_in_bpvalid_10) or ( data_in_bpaddress_10) or ( address_in_rf) or ( data_in_bpaddress_10) or ( data_in_bpdata_10)
	 begin
	 if((( data_in_bpvalid_0 && (  data_in_bpaddress_0 ==  address_in_rf)) && (  data_in_bpaddress_0 != 5'b00000))) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  data_out =  data_in_bpdata_0;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 if((( data_in_bpvalid_1 && (  data_in_bpaddress_1 ==  address_in_rf)) && (  data_in_bpaddress_1 != 5'b00000))) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  data_out =  data_in_bpdata_1;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	 if((( data_in_bpvalid_2 && (  data_in_bpaddress_2 ==  address_in_rf)) && (  data_in_bpaddress_2 != 5'b00000))) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  data_out =  data_in_bpdata_2;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 if((( data_in_bpvalid_3 && (  data_in_bpaddress_3 ==  address_in_rf)) && (  data_in_bpaddress_3 != 5'b00000))) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  data_out =  data_in_bpdata_3;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 if((( data_in_bpvalid_4 && (  data_in_bpaddress_4 ==  address_in_rf)) && (  data_in_bpaddress_4 != 5'b00000))) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	 begin
	  data_out =  data_in_bpdata_4;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	 if((( data_in_bpvalid_5 && (  data_in_bpaddress_5 ==  address_in_rf)) && (  data_in_bpaddress_5 != 5'b00000))) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  data_out =  data_in_bpdata_5;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 if((( data_in_bpvalid_6 && (  data_in_bpaddress_6 ==  address_in_rf)) && (  data_in_bpaddress_6 != 5'b00000))) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  data_out =  data_in_bpdata_6;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 if((( data_in_bpvalid_7 && (  data_in_bpaddress_7 ==  address_in_rf)) && (  data_in_bpaddress_7 != 5'b00000))) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  data_out =  data_in_bpdata_7;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if((( data_in_bpvalid_8 && (  data_in_bpaddress_8 ==  address_in_rf)) && (  data_in_bpaddress_8 != 5'b00000))) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  data_out =  data_in_bpdata_8;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 if((( data_in_bpvalid_9 && (  data_in_bpaddress_9 ==  address_in_rf)) && (  data_in_bpaddress_9 != 5'b00000))) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  data_out =  data_in_bpdata_9;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 if((( data_in_bpvalid_10 && (  data_in_bpaddress_10 ==  address_in_rf)) && (  data_in_bpaddress_10 != 5'b00000))) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  data_out =  data_in_bpdata_10;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	 begin
	  data_out =  data_in_rf;
	  src_is_bp = 1'b0;
	 end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	 end
	  endmodule 

module bypass_select2 (address_in_rf , data_in_rf , data_in_bpaddress_0 , data_in_bpdata_0 , data_in_bpvalid_0 , data_in_bpaddress_1 , data_in_bpdata_1 , data_in_bpvalid_1 , data_in_bpaddress_2 , data_in_bpdata_2 , data_in_bpvalid_2 , data_in_bpaddress_3 , data_in_bpdata_3 , data_in_bpvalid_3 , data_in_bpaddress_4 , data_in_bpdata_4 , data_in_bpvalid_4 , data_in_bpaddress_5 , data_in_bpdata_5 , data_in_bpvalid_5 , data_in_bpaddress_6 , data_in_bpdata_6 , data_in_bpvalid_6 , data_in_bpaddress_7 , data_in_bpdata_7 , data_in_bpvalid_7 , data_in_bpaddress_8 , data_in_bpdata_8 , data_in_bpvalid_8 , data_in_bpaddress_9 , data_in_bpdata_9 , data_in_bpvalid_9 , data_in_bpaddress_10 , data_in_bpdata_10 , data_in_bpvalid_10 , data_out , src_is_bp );

	  input [4 : 0] address_in_rf;
	  input [31 : 0] data_in_rf;
	  input [4 : 0] data_in_bpaddress_0;
	  input [31 : 0] data_in_bpdata_0;
	  input  data_in_bpvalid_0;
	  input [4 : 0] data_in_bpaddress_1;
	  input [31 : 0] data_in_bpdata_1;
	  input  data_in_bpvalid_1;
	  input [4 : 0] data_in_bpaddress_2;
	  input [31 : 0] data_in_bpdata_2;
	  input  data_in_bpvalid_2;
	  input [4 : 0] data_in_bpaddress_3;
	  input [31 : 0] data_in_bpdata_3;
	  input  data_in_bpvalid_3;
	  input [4 : 0] data_in_bpaddress_4;
	  input [31 : 0] data_in_bpdata_4;
	  input  data_in_bpvalid_4;
	  input [4 : 0] data_in_bpaddress_5;
	  input [31 : 0] data_in_bpdata_5;
	  input  data_in_bpvalid_5;
	  input [4 : 0] data_in_bpaddress_6;
	  input [31 : 0] data_in_bpdata_6;
	  input  data_in_bpvalid_6;
	  input [4 : 0] data_in_bpaddress_7;
	  input [31 : 0] data_in_bpdata_7;
	  input  data_in_bpvalid_7;
	  input [4 : 0] data_in_bpaddress_8;
	  input [31 : 0] data_in_bpdata_8;
	  input  data_in_bpvalid_8;
	  input [4 : 0] data_in_bpaddress_9;
	  input [31 : 0] data_in_bpdata_9;
	  input  data_in_bpvalid_9;
	  input [4 : 0] data_in_bpaddress_10;
	  input [31 : 0] data_in_bpdata_10;
	  input  data_in_bpvalid_10;
	  output [31 : 0] data_out;
	  output src_is_bp ;
	 always @ ( data_in_bpvalid_0) or ( data_in_bpaddress_0) or ( address_in_rf) or ( data_in_bpaddress_0) or ( data_in_bpdata_0) or ( data_in_bpvalid_1) or ( data_in_bpaddress_1) or ( address_in_rf) or ( data_in_bpaddress_1) or ( data_in_bpdata_1) or ( data_in_bpvalid_2) or ( data_in_bpaddress_2) or ( address_in_rf) or ( data_in_bpaddress_2) or ( data_in_bpdata_2) or ( data_in_bpvalid_3) or ( data_in_bpaddress_3) or ( address_in_rf) or ( data_in_bpaddress_3) or ( data_in_bpdata_3) or ( data_in_bpvalid_4) or ( data_in_bpaddress_4) or ( address_in_rf) or ( data_in_bpaddress_4) or ( data_in_bpdata_4) or ( data_in_bpvalid_5) or ( data_in_bpaddress_5) or ( address_in_rf) or ( data_in_bpaddress_5) or ( data_in_bpdata_5) or ( data_in_bpvalid_6) or ( data_in_bpaddress_6) or ( address_in_rf) or ( data_in_bpaddress_6) or ( data_in_bpdata_6) or ( data_in_bpvalid_7) or ( data_in_bpaddress_7) or ( address_in_rf) or ( data_in_bpaddress_7) or ( data_in_bpdata_7) or ( data_in_bpvalid_8) or ( data_in_bpaddress_8) or ( address_in_rf) or ( data_in_bpaddress_8) or ( data_in_bpdata_8) or ( data_in_bpvalid_9) or ( data_in_bpaddress_9) or ( address_in_rf) or ( data_in_bpaddress_9) or ( data_in_bpdata_9) or ( data_in_bpvalid_10) or ( data_in_bpaddress_10) or ( address_in_rf) or ( data_in_bpaddress_10) or ( data_in_bpdata_10)
	 begin
	 if((( data_in_bpvalid_0 && (  data_in_bpaddress_0 ==  address_in_rf)) && (  data_in_bpaddress_0 != 5'b00000))) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  data_out =  data_in_bpdata_0;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 if((( data_in_bpvalid_1 && (  data_in_bpaddress_1 ==  address_in_rf)) && (  data_in_bpaddress_1 != 5'b00000))) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  data_out =  data_in_bpdata_1;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	 if((( data_in_bpvalid_2 && (  data_in_bpaddress_2 ==  address_in_rf)) && (  data_in_bpaddress_2 != 5'b00000))) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  data_out =  data_in_bpdata_2;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 if((( data_in_bpvalid_3 && (  data_in_bpaddress_3 ==  address_in_rf)) && (  data_in_bpaddress_3 != 5'b00000))) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  data_out =  data_in_bpdata_3;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 if((( data_in_bpvalid_4 && (  data_in_bpaddress_4 ==  address_in_rf)) && (  data_in_bpaddress_4 != 5'b00000))) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	 begin
	  data_out =  data_in_bpdata_4;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	 if((( data_in_bpvalid_5 && (  data_in_bpaddress_5 ==  address_in_rf)) && (  data_in_bpaddress_5 != 5'b00000))) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  data_out =  data_in_bpdata_5;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 if((( data_in_bpvalid_6 && (  data_in_bpaddress_6 ==  address_in_rf)) && (  data_in_bpaddress_6 != 5'b00000))) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  data_out =  data_in_bpdata_6;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 if((( data_in_bpvalid_7 && (  data_in_bpaddress_7 ==  address_in_rf)) && (  data_in_bpaddress_7 != 5'b00000))) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  data_out =  data_in_bpdata_7;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if((( data_in_bpvalid_8 && (  data_in_bpaddress_8 ==  address_in_rf)) && (  data_in_bpaddress_8 != 5'b00000))) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  data_out =  data_in_bpdata_8;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 if((( data_in_bpvalid_9 && (  data_in_bpaddress_9 ==  address_in_rf)) && (  data_in_bpaddress_9 != 5'b00000))) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  data_out =  data_in_bpdata_9;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 if((( data_in_bpvalid_10 && (  data_in_bpaddress_10 ==  address_in_rf)) && (  data_in_bpaddress_10 != 5'b00000))) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  data_out =  data_in_bpdata_10;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	 begin
	  data_out =  data_in_rf;
	  src_is_bp = 1'b0;
	 end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	 end
	  endmodule 

module bypass_select3 (address_in_rf , data_in_rf , data_in_bpaddress_0 , data_in_bpdata_0 , data_in_bpvalid_0 , data_in_bpaddress_1 , data_in_bpdata_1 , data_in_bpvalid_1 , data_in_bpaddress_2 , data_in_bpdata_2 , data_in_bpvalid_2 , data_in_bpaddress_3 , data_in_bpdata_3 , data_in_bpvalid_3 , data_in_bpaddress_4 , data_in_bpdata_4 , data_in_bpvalid_4 , data_in_bpaddress_5 , data_in_bpdata_5 , data_in_bpvalid_5 , data_in_bpaddress_6 , data_in_bpdata_6 , data_in_bpvalid_6 , data_in_bpaddress_7 , data_in_bpdata_7 , data_in_bpvalid_7 , data_in_bpaddress_8 , data_in_bpdata_8 , data_in_bpvalid_8 , data_in_bpaddress_9 , data_in_bpdata_9 , data_in_bpvalid_9 , data_in_bpaddress_10 , data_in_bpdata_10 , data_in_bpvalid_10 , data_out , src_is_bp );

	  input [4 : 0] address_in_rf;
	  input [31 : 0] data_in_rf;
	  input [4 : 0] data_in_bpaddress_0;
	  input [31 : 0] data_in_bpdata_0;
	  input  data_in_bpvalid_0;
	  input [4 : 0] data_in_bpaddress_1;
	  input [31 : 0] data_in_bpdata_1;
	  input  data_in_bpvalid_1;
	  input [4 : 0] data_in_bpaddress_2;
	  input [31 : 0] data_in_bpdata_2;
	  input  data_in_bpvalid_2;
	  input [4 : 0] data_in_bpaddress_3;
	  input [31 : 0] data_in_bpdata_3;
	  input  data_in_bpvalid_3;
	  input [4 : 0] data_in_bpaddress_4;
	  input [31 : 0] data_in_bpdata_4;
	  input  data_in_bpvalid_4;
	  input [4 : 0] data_in_bpaddress_5;
	  input [31 : 0] data_in_bpdata_5;
	  input  data_in_bpvalid_5;
	  input [4 : 0] data_in_bpaddress_6;
	  input [31 : 0] data_in_bpdata_6;
	  input  data_in_bpvalid_6;
	  input [4 : 0] data_in_bpaddress_7;
	  input [31 : 0] data_in_bpdata_7;
	  input  data_in_bpvalid_7;
	  input [4 : 0] data_in_bpaddress_8;
	  input [31 : 0] data_in_bpdata_8;
	  input  data_in_bpvalid_8;
	  input [4 : 0] data_in_bpaddress_9;
	  input [31 : 0] data_in_bpdata_9;
	  input  data_in_bpvalid_9;
	  input [4 : 0] data_in_bpaddress_10;
	  input [31 : 0] data_in_bpdata_10;
	  input  data_in_bpvalid_10;
	  output [31 : 0] data_out;
	  output src_is_bp ;
	 always @ ( data_in_bpvalid_0) or ( data_in_bpaddress_0) or ( address_in_rf) or ( data_in_bpaddress_0) or ( data_in_bpdata_0) or ( data_in_bpvalid_1) or ( data_in_bpaddress_1) or ( address_in_rf) or ( data_in_bpaddress_1) or ( data_in_bpdata_1) or ( data_in_bpvalid_2) or ( data_in_bpaddress_2) or ( address_in_rf) or ( data_in_bpaddress_2) or ( data_in_bpdata_2) or ( data_in_bpvalid_3) or ( data_in_bpaddress_3) or ( address_in_rf) or ( data_in_bpaddress_3) or ( data_in_bpdata_3) or ( data_in_bpvalid_4) or ( data_in_bpaddress_4) or ( address_in_rf) or ( data_in_bpaddress_4) or ( data_in_bpdata_4) or ( data_in_bpvalid_5) or ( data_in_bpaddress_5) or ( address_in_rf) or ( data_in_bpaddress_5) or ( data_in_bpdata_5) or ( data_in_bpvalid_6) or ( data_in_bpaddress_6) or ( address_in_rf) or ( data_in_bpaddress_6) or ( data_in_bpdata_6) or ( data_in_bpvalid_7) or ( data_in_bpaddress_7) or ( address_in_rf) or ( data_in_bpaddress_7) or ( data_in_bpdata_7) or ( data_in_bpvalid_8) or ( data_in_bpaddress_8) or ( address_in_rf) or ( data_in_bpaddress_8) or ( data_in_bpdata_8) or ( data_in_bpvalid_9) or ( data_in_bpaddress_9) or ( address_in_rf) or ( data_in_bpaddress_9) or ( data_in_bpdata_9) or ( data_in_bpvalid_10) or ( data_in_bpaddress_10) or ( address_in_rf) or ( data_in_bpaddress_10) or ( data_in_bpdata_10)
	 begin
	 if((( data_in_bpvalid_0 && (  data_in_bpaddress_0 ==  address_in_rf)) && (  data_in_bpaddress_0 != 5'b00000))) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  data_out =  data_in_bpdata_0;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 if((( data_in_bpvalid_1 && (  data_in_bpaddress_1 ==  address_in_rf)) && (  data_in_bpaddress_1 != 5'b00000))) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  data_out =  data_in_bpdata_1;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	 if((( data_in_bpvalid_2 && (  data_in_bpaddress_2 ==  address_in_rf)) && (  data_in_bpaddress_2 != 5'b00000))) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  data_out =  data_in_bpdata_2;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 if((( data_in_bpvalid_3 && (  data_in_bpaddress_3 ==  address_in_rf)) && (  data_in_bpaddress_3 != 5'b00000))) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  data_out =  data_in_bpdata_3;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 if((( data_in_bpvalid_4 && (  data_in_bpaddress_4 ==  address_in_rf)) && (  data_in_bpaddress_4 != 5'b00000))) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	 begin
	  data_out =  data_in_bpdata_4;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	 if((( data_in_bpvalid_5 && (  data_in_bpaddress_5 ==  address_in_rf)) && (  data_in_bpaddress_5 != 5'b00000))) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	 begin
	  data_out =  data_in_bpdata_5;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 if((( data_in_bpvalid_6 && (  data_in_bpaddress_6 ==  address_in_rf)) && (  data_in_bpaddress_6 != 5'b00000))) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	 begin
	  data_out =  data_in_bpdata_6;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	 if((( data_in_bpvalid_7 && (  data_in_bpaddress_7 ==  address_in_rf)) && (  data_in_bpaddress_7 != 5'b00000))) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  data_out =  data_in_bpdata_7;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if((( data_in_bpvalid_8 && (  data_in_bpaddress_8 ==  address_in_rf)) && (  data_in_bpaddress_8 != 5'b00000))) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  data_out =  data_in_bpdata_8;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 if((( data_in_bpvalid_9 && (  data_in_bpaddress_9 ==  address_in_rf)) && (  data_in_bpaddress_9 != 5'b00000))) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  data_out =  data_in_bpdata_9;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 if((( data_in_bpvalid_10 && (  data_in_bpaddress_10 ==  address_in_rf)) && (  data_in_bpaddress_10 != 5'b00000))) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  data_out =  data_in_bpdata_10;
	  src_is_bp = 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[21] <= branvar[21] + 1;   
	 begin
	  data_out =  data_in_rf;
	  src_is_bp = 1'b0;
	 end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	 end
	  endmodule 
