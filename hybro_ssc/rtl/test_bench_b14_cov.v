`timescale 1ns/10ps
module test_top;

reg clock;
reg reset;
reg [31:0] datai;

wire rd;
wire wr;
wire [19:0] addr;
wire [31:0] datao;
integer file_out;
reg [7:0] branvar_last[0:300];

integer i;
integer j;
integer k;
integer frame_num;
integer file_branch;
integer var_value;
integer ret_value;
b14 inst (
    .clock(clock),
    .reset(reset),
    .datai(datai[31:0]),
    .rd(rd),
    .wr(wr),
    .addr(addr[19:0]),
    .datao(datao[31:0])
    );
//define coverage point

covergroup cg1 @(posedge clock);
 mf: coverpoint inst.mf;
 cf: coverpoint inst.cf;
 ff: coverpoint inst.ff;
 df: coverpoint inst.df;
 s:  coverpoint inst.s;
 tail0: coverpoint inst.tail[3:0]
  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  }
 tail1: coverpoint inst.tail[7:4]
  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  }
 tail2: coverpoint inst.tail[11:8]
  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  }
 tail3: coverpoint inst.tail[15:12]
  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  }
 tail4: coverpoint inst.tail[19:16]
  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
  }
 mf_cf_ff_df_s: cross mf, cf, ff, df, s;
 ff_mf: cross ff, mf;
 df_ff: cross df, ff;
 tail: cross tail0, tail1, tail2, tail3, tail4;
 mf_tail: cross mf, tail0, tail1, tail2, tail3, tail4;
 cf_tail: cross cf, tail0, tail1, tail2, tail3, tail4;
 ff_tail: cross ff, tail0, tail1, tail2, tail3, tail4;
 df_tail: cross df, tail0, tail1, tail2, tail3, tail4;
 s_tail:  cross s,  tail0, tail1, tail2, tail3, tail4;
endgroup : cg1

cg1 cov_inst = new();
initial begin
#1
  clock = 1'b0;
  i = 0;
  frame_num=-1;

#400000;
$finish;
end

always 
begin
  #5 clock = ~clock;
end



always
begin
  #4;
  if(frame_num<0)
    reset = 1;
  else reset = 0;

     datai = $random;

  frame_num = frame_num + 1;
  if(frame_num == 9)
    frame_num = -1;
  #5
  #1;
end


endmodule
