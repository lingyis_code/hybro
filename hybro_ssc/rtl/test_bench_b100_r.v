`timescale 1ns/10ps
module test_top;

reg clock;
reg rst;

reg  ic_en;
reg  icqmem_cycstb_i;
reg  icqmem_ci_i;
reg  tagcomp_miss;
reg  biudata_valid;
reg  biudata_error;
reg [31 : 0] start_addr;
wire [31 : 0] saved_addr;
wire [3 : 0] icram_we;
wire biu_read ;
wire first_hit_ack ;
wire first_miss_ack ;
wire first_miss_err ;
wire burst ;
wire tag_we ;

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer random_pat_num;
integer file_branch;
integer var_value;
integer ret_value;
b100 inst (
 .clk(clock),
 .rst(rst),
 .ic_en(ic_en),
 .icqmem_cycstb_i(icqmem_cycstb_i),
 .icqmem_ci_i(icqmem_ci_i),
 .tagcomp_miss(tagcomp_miss),
 .biudata_valid(biudata_valid),
 .biudata_error(biudata_error),
 .start_addr(start_addr[31:0]),
 .saved_addr(saved_addr[31:0]),
 .icram_we(icram_we[3:0]),
 .biu_read(biu_read), 
 .first_hit_ack(first_hit_ack),
 .first_miss_ack(first_miss_ack),
 .first_miss_err(first_miss_err),
 .burst(burst),
 .tag_we(tag_we)
    );

initial begin
#1
  //start_build_cfg("source_rtl/b100_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  random_pat_num = 0;
  inst.state = 0;
//#10000000 $finish;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end


always
begin
  #4;
  if(frame_num == -1)
   inst.state = 0;
  var_value = -1;//get_var_assignment("rst",frame_num+1);
  if(var_value == -1)
     rst = $random;
  else 
     rst = var_value;

  var_value = -1;//get_var_assignment("ic_en",frame_num+1);
  if(var_value == -1)
     ic_en = $random;
  else 
     ic_en = var_value;
  
  var_value = -1;//get_var_assignment("icqmem_cycstb_i",frame_num+1);
  if(var_value == -1)
    icqmem_cycstb_i  = $random;
  else 
    icqmem_cycstb_i   = var_value;

  var_value = -1;//get_var_assignment("icqmem_ci_i",frame_num+1);
  if(var_value == -1)
    icqmem_ci_i  = $random;
  else 
    icqmem_ci_i   = var_value;

  var_value = -1;//get_var_assignment("tagcomp_miss",frame_num+1);
  if(var_value == -1)
    tagcomp_miss  = $random;
  else 
    tagcomp_miss  = var_value;

  var_value = -1;//get_var_assignment("biudata_valid",frame_num+1);
  if(var_value == -1)
     biudata_valid = $random;
  else 
     biudata_valid = var_value;

  var_value = -1;//get_var_assignment("biudata_error",frame_num+1);
  if(var_value == -1)
     biudata_error = $random;
  else 
     biudata_error = var_value;

  var_value = -1;//get_var_assignment("start_addr",frame_num+1);
  if(var_value == -1)
     start_addr[31:0] = $random;
  else 
     start_addr[31:0] = var_value;

  frame_num = frame_num + 1;
  #5
  if(frame_num==3)
   begin
     //ret_value=generate_nxt_pattern(frame_num);
     random_pat_num = random_pat_num + 1;
     if(random_pat_num==1000)
       $finish;
     ret_value = 1;
     if(ret_value==0)
       $finish;
     frame_num=-1;
   end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

