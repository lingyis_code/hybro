`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg rst;
//-------------------------
reg sig_ibuserr;
reg sig_dbuserr;
reg sig_illegal;
reg sig_align;
reg sig_range;
reg sig_dtlbmiss;
reg sig_dmmufault;
reg sig_int;
reg sig_syscall;
reg sig_trap;
reg sig_itlbmiss;
reg sig_immufault;
reg sig_tick;
reg branch_taken;
reg genpc_freeze;
reg id_freeze;
reg ex_freeze;
reg wb_freeze;
reg if_stall;
reg[31 : 0] if_pc;
reg [31 : 0] id_pc;
reg [29:0] lr_sav;
reg[31 : 0] datain;
reg[13 : 0] du_dsr;
reg epcr_we;
reg eear_we;
reg esr_we;
reg pc_we;
reg [31 : 0] epcr;
reg [31 : 0] eear;
reg [15 : 0] esr;
reg[15 : 0] to_sr;
reg sr_we;
reg[15 : 0] sr;
reg[31 : 0] lsu_addr;
reg flushpipe ;
reg extend_flush ;
reg [3 : 0] except_type;
reg except_start ;
reg except_started ;
reg [12 : 0] except_stop;
reg ex_void;
reg [31 : 0] spr_dat_ppc;
reg [31 : 0] spr_dat_npc;
reg abort_ex ;
reg icpu_ack_i;
reg icpu_err_i;
reg dcpu_ack_i;
reg dcpu_err_i;

//-------------------------

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;

b103 inst (
 .clk(clock),
 .rst(rst),
 .sig_ibuserr(sig_ibuserr),
 .sig_dbuserr(sig_dbuserr),
 .sig_illegal(sig_illegal),
 .sig_align(sig_align),
 .sig_range(sig_range),
 .sig_dtlbmiss(sig_dtlbmiss),
 .sig_dmmufault(sig_dmmufault),
 .sig_int(sig_int),
 .sig_syscall(sig_syscall),
 .sig_trap(sig_trap),
 .sig_itlbmiss(sig_itlbmiss),
 .sig_immufault(sig_immufault),
 .sig_tick(sig_tick),
 .branch_taken(branch_taken),
 .genpc_freeze(genpc_freeze),
 .id_freeze(id_freeze),
 .ex_freeze(ex_freeze),
 .wb_freeze(wb_freeze),
 .if_stall(if_stall),
 .if_pc(if_pc[31:0]),
 .id_pc(id_pc[31:0]),
 .lr_sav(lr_sav[29:0]),
 .datain(datain[31:0]),
 .du_dsr(du_dsr[13:0]),
 .epcr_we(epcr_we),
 .eear_we(eear_we),
 .esr_we(esr_we),
 .pc_we(pc_we),
 .epcr(epcr[31:0]),
 .eear(eear[31:0]),
 .esr(esr[15:0]),
 .to_sr(to_sr[15:0]),
 .sr_we(sr_we),
 .sr(sr[15:0]),
 .lsu_addr(lsu_addr[31:0]),
 .flushpipe (flushpipe),
 .extend_flush (extend_flush),
 .except_type(except_type[3:0]),
 .except_start (except_start),
 .except_started (except_started),
 .except_stop(except_stop[12:0]),
 .ex_void(ex_void),
 .spr_dat_ppc(spr_dat_ppc[31:0]),
 .spr_dat_npc(spr_dat_npc[31:0]),
 .abort_ex (abort_ex),
 .icpu_ack_i(icpu_ack_i),
 .icpu_err_i(icpu_err_i),
 .dcpu_ack_i(dcpu_ack_i),
 .dcpu_err_i(dcpu_err_i)
    );

initial begin
#1
  start_build_cfg("../rtl/b103_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num=0;
 //#10000000 $finish;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num == -1)
   begin
       inst.except_type = 4'h0;
	  inst.id_pc = 32'h0;
	  inst.ex_pc = 32'h0;
	  inst.wb_pc = 32'h0;
	  inst.epcr  = 32'h0;
	  inst.eear  = 32'h0;
	  inst.esr   = 16'h0;
	  inst.id_exceptflags=3'h0;
	  inst.ex_exceptflags=3'h0;
	  inst.state = 3'h0;
	  inst.extend_flush = 0;
	  inst.extend_flush_last =0;
	  inst.ex_dslot =0;
	  inst.delayed1_ex_dslot =0;
	  inst.delayed2_ex_dslot =0;
	  inst.delayed_iee = 3'h0;
	  inst.delayed_tee = 3'h0;

   end
  var_value = get_var_assignment("rst",frame_num+1,1);
  if(frame_num<0)
     rst = 1;
  else 
     rst = 0;

  var_value = get_var_assignment("sig_ibuserr",frame_num+1,1);
  if(var_value == -1)
    sig_ibuserr  = $random;
  else 
    sig_ibuserr  = var_value;
  
  var_value = get_var_assignment("sig_dbuserr",frame_num+1,1);
  if(var_value == -1)
    sig_dbuserr  = $random;
  else 
    sig_dbuserr   = var_value;

  var_value = get_var_assignment("sig_illegal",frame_num+1,1);
  if(var_value == -1)
    sig_illegal  = $random;
  else 
    sig_illegal   = var_value;

  var_value = get_var_assignment("sig_align",frame_num+1,1);
  if(var_value == -1)
    sig_align = $random;
  else 
    sig_align = var_value;

  var_value = get_var_assignment("sig_range",frame_num+1,4);
  if(var_value == -1)
    sig_range = $random;
  else 
    sig_range = var_value;



  var_value = get_var_assignment("sig_dtlbmiss",frame_num+1,1);
  if(var_value == -1)
     sig_dtlbmiss = $random;
  else 
     sig_dtlbmiss = var_value;

  var_value = get_var_assignment("sig_dmmufault",frame_num+1,1);
  if(var_value == -1)
     sig_dmmufault = $random;
  else 
     sig_dmmufault = var_value;

  var_value = get_var_assignment("sig_int",frame_num+1,1);
  if(var_value == -1)
     sig_int = $random;
  else 
     sig_int = var_value;

  var_value = get_var_assignment("sig_syscall",frame_num+1,1);
  if(var_value == -1)
    sig_syscall  = $random;
  else 
    sig_syscall  = var_value;

  var_value = get_var_assignment("sig_trap",frame_num+1,1);
  if(var_value == -1)
     sig_trap = $random;
  else 
     sig_trap  = var_value;
  var_value = get_var_assignment("sig_itlbmiss",frame_num+1,1);
  if(var_value == -1)
    sig_itlbmiss  = $random;
  else 
    sig_itlbmiss  = var_value;
  var_value = get_var_assignment("sig_immufault",frame_num+1,1);
  if(var_value == -1)
    sig_immufault  = $random;
  else 
    sig_immufault  = var_value;
  var_value = get_var_assignment("sig_tick",frame_num+1,1);
  if(var_value == -1)
     sig_tick = $random;
  else 
     sig_tick = var_value;
  var_value = get_var_assignment("branch_taken",frame_num+1,1);
  if(var_value == -1)
     branch_taken = $random;
  else 
     branch_taken = var_value;
  var_value = get_var_assignment("genpc_freeze",frame_num+1,1);
  if(var_value == -1)
     genpc_freeze = $random;
  else 
     genpc_freeze = var_value;
  var_value = get_var_assignment("id_freeze",frame_num+1,1);
  if(var_value == -1)
     id_freeze = $random;
  else 
     id_freeze = var_value;
  var_value = get_var_assignment("ex_freeze",frame_num+1,1);
  if(var_value == -1)
     ex_freeze = $random;
  else 
     ex_freeze = var_value;
  var_value = get_var_assignment("wb_freeze",frame_num+1,1);
  if(var_value == -1)
     wb_freeze = $random;
  else 
     wb_freeze = var_value;
  var_value = get_var_assignment("if_stall",frame_num+1,1);
  if(var_value == -1)
     if_stall = $random;
  else 
     if_stall = var_value;

  var_value = get_var_assignment("if_pc",frame_num+1,32);
  if(var_value == -1)
     if_pc = $random;
  else 
     if_pc = var_value;

  var_value = get_var_assignment("datain",frame_num+1,32);
  if(var_value == -1)
      datain= $random;
  else 
     datain = var_value;

  var_value = get_var_assignment("du_dsr",frame_num+1,14);
  if(var_value == -1)
     du_dsr = $random;
  else 
     du_dsr = var_value;

  var_value = get_var_assignment("epcr_we",frame_num+1,1);
  if(var_value == -1)
     epcr_we = $random;
  else 
     epcr_we = var_value;

  var_value = get_var_assignment("eear_we",frame_num+1,1);
  if(var_value == -1)
     eear_we = $random;
  else 
     eear_we = var_value;

  var_value = get_var_assignment("esr_we",frame_num+1,1);
  if(var_value == -1)
     esr_we = $random;
  else 
     esr_we = var_value;

  var_value = get_var_assignment("pc_we",frame_num+1,1);
  if(var_value == -1)
     pc_we = $random;
  else 
     pc_we = var_value;

  var_value = get_var_assignment("to_sr",frame_num+1,16);
  if(var_value == -1)
     to_sr = $random;
  else 
     to_sr = var_value;

  var_value = get_var_assignment("sr_we",frame_num+1,1);
  if(var_value == -1)
     sr_we = $random;
  else 
     sr_we = var_value;

  var_value = get_var_assignment("sr",frame_num+1,16);
  if(var_value == -1)
     sr = $random;
  else 
     sr = var_value;

  var_value = get_var_assignment("lsu_addr",frame_num+1,32);
  if(var_value == -1)
     lsu_addr = $random;
  else 
     lsu_addr = var_value;

  var_value = get_var_assignment("ex_void",frame_num+1,1);
  if(var_value == -1)
    ex_void  = $random;
  else 
    ex_void  = var_value;

  var_value = get_var_assignment("icpu_ack_i",frame_num+1,1);
  if(var_value == -1)
     icpu_ack_i = $random;
  else 
     icpu_ack_i = var_value;
  var_value = get_var_assignment("icpu_err_i",frame_num+1,1);
  if(var_value == -1)
     icpu_err_i = $random;
  else 
     icpu_err_i = var_value;
  var_value = get_var_assignment("dcpu_ack_i",frame_num+1,1);
  if(var_value == -1)
     dcpu_ack_i = $random;
  else 
     dcpu_ack_i = var_value;
  var_value = get_var_assignment("dcpu_err_i",frame_num+1,1);
  if(var_value == -1)
     dcpu_err_i = $random;
  else 
     dcpu_err_i = var_value;

  frame_num = frame_num + 1;
  #5
  if(frame_num==10)
   begin
     //ret_value=generate_nxt_pattern(frame_num);
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(76, 10, pat_num);
       $finish;
     end
     frame_num=-1;
   end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

