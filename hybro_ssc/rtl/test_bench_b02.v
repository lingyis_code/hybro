`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg reset;
reg linea;
reg u;

wire outp;
wire overflw;

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;
b02 inst (
    .clock(clock),
    .reset(reset),
    .linea(linea),
    .u(u)
    );

initial begin
#1
  start_build_cfg("../rtl/b02_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num = 0;
  inst.stato = 0;
//#10000000 $finish;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num == -1)
   inst.stato = 0;
  var_value = get_var_assignment("reset",frame_num+1,1);
  if(frame_num<0)
    reset = 1;
  else
    reset = 0;

  var_value = get_var_assignment("linea",frame_num+1,1);
  if(var_value == -1)
     linea = $random;
  else 
     linea = var_value;


  i = i + 3;
  frame_num = frame_num + 1;
  #5
  if(frame_num==10)
   begin
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(14, 40, pat_num);
       $finish;
     end
     frame_num=-1;
   end
   #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

