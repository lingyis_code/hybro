`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg  clock;
reg  reset;
reg  eoc;
reg  dsr;
reg [7 : 0] data_in;
wire soc ;
wire load_dato ;
wire add_mpx2 ;
wire mux_en ;
wire error ;
wire data_out ;
wire [3 : 0] canale;

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;

integer file_branch;
integer var_value;
integer ret_value;
b13 inst (
    .clock(clock),
    .reset(reset),
    .eoc(eoc),
    .dsr(dsr),
    .data_in(data_in[7:0]),
    .soc(soc),
    .load_dato(load_dato),
    .add_mpx2(add_mpx2),
    .mux_en(mux_en),
    .error(error),
    .data_out(data_out),
    .canale(canale[3:0])
    );

initial begin
#1
  start_build_cfg("../rtl/b13_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num = 0;
  inst.S1 = 0;
  inst.soc = 0;
  inst.canale = 0;
  inst.conta_tmp = 0;
  inst.send_data = 0;
  inst.load_dato = 0;
  inst.mux_en = 0;
  inst.S2 = 0;
  inst.rdy = 0;
  inst.add_mpx2 = 0;
  inst.mpx = 0;
  inst.shot = 0;
  inst.load = 0;
  inst.send = 0;
  inst.confirm = 0;
  inst.itfc_state = 0;
  inst.send_en = 0;
  inst.out_reg = 8'b00000000;
  inst.tre = 0;
  inst.error = 0;
  inst.tx_end = 0;
  inst.data_out = 0;
  inst.next_bit = 0;
  inst.tx_conta = 0;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num<0)
    reset = 1;
  else reset = 0;

  //$display("\n---frame_num = %d",frame_num);

  var_value = get_var_assignment("eoc",frame_num+1,1);
  if(var_value == -1)
     eoc = $random;
  else 
   begin
     eoc = var_value;
     //$display("\n---eoc = %d----",eoc);
   end
  
  var_value = get_var_assignment("dsr",frame_num+1,1);
  if(var_value == -1)
     dsr = $random;
  else 
   begin
     dsr = var_value;
     //$display("\n---dsr = %d----",dsr);
   end

  var_value = get_var_assignment("data_in",frame_num+1,8);
  if(var_value == -1)
     data_in = $random;
  else 
   begin
     data_in = var_value;
     //$display("\n---data_in = %d----",data_in);
   end
  i = i + 3;
  frame_num = frame_num + 1;
  #5
  if(frame_num==10)
   begin
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     if(ret_value==0)
       $finish;
     frame_num=-1;
   end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

