`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg reset;
reg [31:0] datai;

wire rd;
wire wr;
wire [19:0] addr;
wire [31:0] datao;
reg [7:0] branvar_last[0:300];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;
b14 inst (
    .clock(clock),
    .reset(reset),
    .datai(datai[31:0]),
    .rd(rd),
    .wr(wr),
    .addr(addr[19:0]),
    .datao(datao[31:0])
    );

//covergroup cg1 @(posedge clock);
// mf: coverpoint inst.mf;
// cf: coverpoint inst.cf;
// ff: coverpoint inst.ff;
// df: coverpoint inst.df;
// s:  coverpoint inst.s;
// tail0: coverpoint inst.tail[3:0]
//  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//  }
// tail1: coverpoint inst.tail[7:4]
//  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//  }
// tail2: coverpoint inst.tail[11:8]
//  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//  }
// tail3: coverpoint inst.tail[15:12]
//  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//  }
// tail4: coverpoint inst.tail[19:16]
//  { bins V[] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
//  }
// mf_cf_ff_df_s: cross mf, cf, ff, df, s;
// ff_mf: cross ff, mf;
// df_ff: cross df, ff;
// tail: cross tail0, tail1, tail2, tail3, tail4;
// mf_tail: cross mf, tail0, tail1, tail2, tail3, tail4;
// cf_tail: cross cf, tail0, tail1, tail2, tail3, tail4;
// ff_tail: cross ff, tail0, tail1, tail2, tail3, tail4;
// df_tail: cross df, tail0, tail1, tail2, tail3, tail4;
// s_tail:  cross s,  tail0, tail1, tail2, tail3, tail4;
//endgroup : cg1
//
//cg1 cov_inst = new();

initial begin
#1
  start_build_cfg("../rtl/b14_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num=0;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=300;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=300;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num<0)
    reset = 1;
  else reset = 0;

  var_value = get_var_assignment("datai",frame_num+1,32);
  if(var_value == -1)
     datai = $random;
  else
   begin 
     datai = var_value;
   end

  frame_num = frame_num + 1;
  #5
  if(frame_num==8)
   begin
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(216,8,pat_num);
       $finish;
     end
     frame_num=-1;
   end
  #1;
end

initial begin
  for(i=0;i<=300;i=i+1)
   inst.branvar[i]=8'h0;
end

endmodule
