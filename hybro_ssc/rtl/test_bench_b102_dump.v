`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg rst;
//-------------------------

reg    		wb_ack_i;	// normal termination
reg    		wb_err_i;	// termination w/ error
reg    		wb_rty_i;	// termination w/ retry
reg    [31:0]	wb_dat_i;	// input data bus
reg			wb_cyc_o;	// cycle valid output
reg	[31:0]	wb_adr_o;	// address bus outputs
reg			wb_stb_o;	// strobe output
reg			wb_we_o;	// indicates write transfer
reg	[3:0]		wb_sel_o;	// byte select outputs
reg	[31:0]	wb_dat_o;	// output data bus
reg			wb_cab_o;	// consecutive address burst
reg	[2:0]		wb_cti_o;	// cycle type identifier
reg	[1:0]		wb_bte_o;	// burst type extension

//
// Internal RISC interface
//
reg    [31:0]	biu_dat_i;	// input data bus
reg    [31:0]	biu_adr_i;	// address bus
reg    		biu_cyc_i;	// WB cycle
reg    		biu_stb_i;	// WB strobe
reg    		biu_we_i;	// WB write enable
reg    		biu_cab_i;	// CAB input
reg    [3:0]		biu_sel_i;	// byte selects
reg	[31:0]		biu_dat_o;	// output data bus
reg			biu_ack_o;	// ack output
reg			biu_err_o;	// err output

//-------------------------

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;

b102 inst (
 .clk(clock),
 .rst(rst),
 .wb_ack_i(wb_ack_i),	// normal termination
 .wb_err_i(wb_err_i),	// termination w/ error
 .wb_rty_i(wb_rty_i),	// termination w/ retry
 .wb_dat_i(wb_dat_i[31:0]),	// input data bus
 .wb_cyc_o(wb_cyc_o),	// cycle valid output
 .wb_adr_o(wb_adr_o[31:0]),	// address bus outputs
 .wb_stb_o(wb_stb_o),	// strobe output
 .wb_we_o (wb_we_o),	// indicates write transfer
 .wb_sel_o(wb_sel_o[3:0]),	// byte select outputs
 .wb_dat_o(wb_dat_o[31:0]),	// output data bus
 .wb_cab_o(wb_cab_o),	// consecutive address burst
 .wb_cti_o(wb_cti_o[2:0]),	// cycle type identifier
 .wb_bte_o(wb_bte_o[1:0]),	// burst type extension
 .biu_dat_i(biu_dat_i[31:0]),	// input data bus
 .biu_adr_i(biu_adr_i[31:0]),	// address bus
 .biu_cyc_i(biu_cyc_i),	// WB cycle
 .biu_stb_i(biu_stb_i),	// WB strobe
 .biu_we_i (biu_we_i),    // WB write enable
 .biu_cab_i(biu_cab_i),	// CAB input
 .biu_sel_i(biu_sel_i),	// byte selects
 .biu_dat_o(biu_dat_o),	// output data bus
 .biu_ack_o(biu_ack_o),	// ack output
 .biu_err_o(biu_err_o)	// err output
);

initial begin
#1
  start_build_cfg("../rtl/b102_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num=0;
  inst.wb_adr_o = 32'h0;
  inst.biu_dat_o = 32'h0;
  inst.wb_dat_o = 32'h0;
  inst.retry_cntr = 7'h0;
  inst.aborted_r = 1'b0;
  inst.wb_cyc_o =   1'b0;
  inst.wb_stb_o =   1'b0;
  inst.wb_we_o =   1'b0;
  inst.wb_sel_o =   4'b0000;
  inst.wb_cab_o =   1'b0;
  inst.long_ack_o =   1'b0;
  inst.long_err_o =   1'b0;
  inst.burst_len = 2'h0;
  inst.wb_cti_o = 3'h0;
//#10000000 $finish;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  var_value = get_var_assignment("rst",frame_num+1,1);

  if(frame_num<0)
     rst = 1;
  else 
     rst = 0;

  var_value = get_var_assignment("wb_ack_i",frame_num+1,1);
  if(var_value == -1)
    wb_ack_i  = $random;
  else 
    wb_ack_i  = var_value;
  
  var_value = get_var_assignment("wb_err_i",frame_num+1,1);
  if(var_value == -1)
    wb_err_i  = $random;
  else 
    wb_err_i   = var_value;

  var_value = get_var_assignment("wb_rty_i",frame_num+1,1);
  if(var_value == -1)
    wb_rty_i  = $random;
  else 
    wb_rty_i   = var_value;

  var_value = get_var_assignment("wb_dat_i",frame_num+1,32);
  if(var_value == -1)
    wb_dat_i  = $random;
  else 
    wb_dat_i  = var_value;

  var_value = get_var_assignment("biu_dat_i",frame_num+1,32);
  if(var_value == -1)
    biu_dat_i  = $random;
  else 
    biu_dat_i   = var_value;

  var_value = get_var_assignment("biu_adr_i",frame_num+1,32);
  if(var_value == -1)
    biu_adr_i  = $random;
  else 
    biu_adr_i   = var_value;

  var_value = get_var_assignment("biu_cyc_i",frame_num+1,1);
  if(var_value == -1)
    biu_cyc_i  = $random;
  else 
    biu_cyc_i   = var_value;

  var_value = get_var_assignment("biu_stb_i",frame_num+1,1);
  if(var_value == -1)
    biu_stb_i  = $random;
  else 
    biu_stb_i   = var_value;

  var_value = get_var_assignment("biu_we_i",frame_num+1,1);
  if(var_value == -1)
    biu_we_i  = $random;
  else 
    biu_we_i   = var_value;

  var_value = get_var_assignment("biu_cab_i",frame_num+1,1);
  if(var_value == -1)
    biu_cab_i  = $random;
  else 
    biu_cab_i   = var_value;

  var_value = get_var_assignment("biu_sel_i",frame_num+1,4);
  if(var_value == -1)
    biu_sel_i  = $random;
  else 
    biu_sel_i   = var_value;


  frame_num = frame_num + 1;
  #5
  if(frame_num==100)
   begin
     //ret_value=generate_nxt_pattern(frame_num);
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(38, 7, pat_num);
       $finish;
     end
     frame_num=-1;
   end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

