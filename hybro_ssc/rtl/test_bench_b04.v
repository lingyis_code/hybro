`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);


reg clock;
reg RESET;
reg RESTART;
reg AVERAGE;
reg ENABLE;
reg [7:0] DATA_IN;

wire [7:0] DATA_OUT; 

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;
b04 inst (
  .CLOCK(clock),
  .RESET(RESET),
  .RESTART(RESTART),
  .AVERAGE(AVERAGE),
  .ENABLE(ENABLE),
  .DATA_IN(DATA_IN),
  .DATA_OUT(DATA_OUT)
    );

initial begin
#1
  start_build_cfg("../rtl/b04_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num = 1;
  inst.stato = 0;
  inst.RMAX  = 0;
  inst.RMIN  = 0;
  inst.RLAST = 0;
  inst.REG1  = 0;
  inst.REG2  = 0;
  inst.REG3  = 0;
  inst.REG4  = 0;

//#10000000 $finish;
end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num == -1)
  begin
    inst.stato = 0;
    inst.RMAX  = 0;
    inst.RMIN  = 0;
    inst.RLAST = 0;
    inst.REG1  = 0;
    inst.REG2  = 0;
    inst.REG3  = 0;
    inst.REG4  = 0;
  end
  var_value = get_var_assignment("RESET",frame_num+1,1);
  //if(var_value == -1)
  //   RESET = $random;
  //else 
  //   RESET = var_value;
  if(frame_num<0)
    RESET = 1;
  else
    RESET = 0;

  var_value = get_var_assignment("RESTART",frame_num+1,1);
  if(var_value == -1)
     RESTART = $random;
  else 
     RESTART = var_value;

  var_value = get_var_assignment("AVERAGE",frame_num+1,1);
  if(var_value == -1)
     AVERAGE = $random;
  else 
     AVERAGE = var_value;

  var_value = get_var_assignment("ENABLE",frame_num+1,1);
  if(var_value == -1)
     ENABLE = $random;
  else 
     ENABLE = var_value;
 
  var_value = get_var_assignment("DATA_IN",frame_num+1,8);
  if(var_value == -1)
     DATA_IN = $random;
  else 
     DATA_IN = var_value;

  frame_num = frame_num + 1;
  #5
  if(frame_num==10)
   begin
     ret_value=generate_nxt_pattern(frame_num,pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(21, 7, pat_num);
       $finish;
     end
     frame_num=-1;
   end
  //if(i>6000)
  // begin
  //  #4;
  //  //solve_constraint();
  //  $finish;
  // end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

