`timescale 1ns/10ps

module fetch_stage (clk , reset , stall_in , branch_pc , branch_addr , branch_mispredict , icache_read_req_addr_o , icache_read_req_vld_o , icache_read_data_i , icache_read_data_vld_i , stall_out , fetch2decodeinstr0_out , fetch2decodeinstr1_out , fetch2decodepc1 , fetch2decodepc0 , fetch2decodepc1_incr , fetch2decodepc0_incr , fetch2decodeinstr0_valid , fetch2decodeinstr1_valid , fetch2decodebranch_taken , fetch2decodepredicted_pc , fetch2decodevalid );

	  input  clk;
	  input  reset;
	  input  stall_in;
	  input [31 : 0] branch_pc;
	  input [31 : 0] branch_addr;
	  input  branch_mispredict;
	  input [63 : 0] icache_read_data_i;
	  input  icache_read_data_vld_i;
	  output [31 : 0] icache_read_req_addr_o;
	  output icache_read_req_vld_o ;
	  output stall_out ;
	  output [31 : 0] fetch2decodeinstr0_out;
	  output [31 : 0] fetch2decodeinstr1_out;
	  output [31 : 0] fetch2decodepc1;
	  output [31 : 0] fetch2decodepc0;
	  output [31 : 0] fetch2decodepc1_incr;
	  output [31 : 0] fetch2decodepc0_incr;
	  output fetch2decodeinstr0_valid ;
	  output fetch2decodeinstr1_valid ;
	  output fetch2decodebranch_taken ;
	  output [31 : 0] fetch2decodepredicted_pc;
	  output fetch2decodevalid ;
	  reg [31 : 0] pc;
	  reg [7:0] branvar[0:100];
	  wire [31 : 0] instr0;
	  wire [31 : 0] instr1;
	  wire icache_hit;
	  wire icache_miss;
	  wire [31 : 0] icache_instr0;
	  wire [31 : 0] icache_instr1;
	  reg fetch_stall;
	  reg btb_hit;
	  //reg [31 : 0] btb_target_pc;
	  reg [31 : 0] btb_target;
	  reg [31 : 0] btb_pc;
	  reg  btb_valid;
	  //reg [31 : 0] predicted_pc;
	  //reg branch_taken;
	  wire [31 : 0] pc_plus_4;
	  wire [31 : 0] pc_plus_8;
	  //wire [31 : 0] next_pc;

          reg [31 : 0] fetch2decodeinstr0_out;
	  reg [31 : 0] fetch2decodeinstr1_out;
	  reg [31 : 0] fetch2decodepc1;
	  reg [31 : 0] fetch2decodepc0;
	  reg [31 : 0] fetch2decodepc1_incr;
	  reg [31 : 0] fetch2decodepc0_incr;
	  reg fetch2decodeinstr0_valid;
	  reg fetch2decodeinstr1_valid;
	  reg fetch2decodebranch_taken;
	  reg [31 : 0] fetch2decodepredicted_pc;
	  reg fetch2decodevalid ;
	  reg [31 : 0] icache_read_req_addr_o;
	  reg icache_read_req_vld_o ;
          //wire [31:0] lly_insert;


	 //assign icache_hit = icache_read_data_vld_i;
	 //assign icache_instr0 = icache_read_data_i[31:0];
	 //assign icache_instr1 = icache_read_data_i[63:32];
	 //assign icache_miss = (~ icache_read_data_vld_i);
	 assign icache_read_req_addr_o = pc;
	 //assign btb_target_pc = btb_target;
	 //assign fetch_stall = icache_miss;
	 assign stall_out = (~ icache_read_data_vld_i);
	 assign pc_plus_4 = (pc + 4);
	 assign pc_plus_8 = (pc + 8);
	 //assign branch_taken = btb_hit;
	 //assign btb_hit = (btb_valid && ( pc == btb_pc));
	 //assign next_pc = branch_mispredict ?  branch_pc :  predicted_pc;
         //assign lly_insert =  pc[2] ?  pc_plus_4 :  pc_plus_8;
	 //assign predicted_pc = btb_hit ?  btb_target :  (pc[2] ?  pc_plus_4 :  pc_plus_8);


	 always @ (posedge  clk) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  btb_valid <= 1'b0;
	  btb_pc <= 32'h0;
	  btb_target <= 32'h0;
	  fetch2decodevalid <= 1'b0;
	  fetch2decodeinstr0_valid <= 1'b0;
	  fetch2decodeinstr1_valid <= 1'b0;
	  pc <= 32'h7fffffff;
	  icache_read_req_vld_o <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 begin
	  icache_read_req_vld_o <= 1'b1;
	 if( branch_mispredict) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  btb_pc <=  branch_pc;
	  btb_target <=  branch_addr;
	  btb_valid <= 1'b1;
	 end
	  end
	 else 
	 begin  
	   branvar[3] <= branvar[3] + 1;   
	   end 
	 if( stall_in) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  fetch2decodeinstr0_out <=  fetch2decodeinstr0_out;
	  fetch2decodeinstr1_out <=  fetch2decodeinstr1_out;
	  fetch2decodepc1 <=  fetch2decodepc1;
	  fetch2decodepc0 <=  fetch2decodepc0;
	  fetch2decodepc1_incr <=  fetch2decodepc1_incr;
	  fetch2decodepc0_incr <=  fetch2decodepc0_incr;
	  fetch2decodeinstr0_valid <=  fetch2decodeinstr0_valid;
	  fetch2decodeinstr1_valid <=  fetch2decodeinstr1_valid;
	  fetch2decodebranch_taken <=  fetch2decodebranch_taken;
	  fetch2decodepredicted_pc <=  fetch2decodepredicted_pc;
	  fetch2decodevalid <=  fetch2decodevalid;
	  pc <=  pc;
	 end
	  end
	 else 
	   begin 
	   branvar[5] <= branvar[5] + 1;   
	 if( branch_mispredict) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  fetch2decodevalid <= 1'b0;
	  fetch2decodeinstr0_valid <= 1'b0;
	  fetch2decodeinstr1_valid <= 1'b0;
	  //pc <=  next_pc;
          pc <= branch_pc;
	 end
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 begin
	  fetch2decodevalid <=  icache_read_data_vld_i;
	  fetch2decodeinstr0_valid <= (!  pc[2]);
	  fetch2decodeinstr1_valid <=  icache_read_data_vld_i;
	  fetch2decodeinstr0_out <=  icache_read_data_i[31:0];
	  fetch2decodeinstr1_out <=  icache_read_data_i[63:32];
	  fetch2decodepc0 <=  pc;
	  fetch2decodepc1 <= (  pc[2] ?  pc :  pc_plus_4);
	  fetch2decodepc0_incr <=  pc_plus_4;
	  fetch2decodepc1_incr <= (  pc[2] ?  pc_plus_4 :  pc_plus_8);
	  fetch2decodebranch_taken <= 1'b0;
          if(btb_valid && ( pc == btb_pc))
           begin
	    branvar[8] <= branvar[8] + 1;   
            fetch2decodepredicted_pc <=  btb_target;
	    pc <=  btb_target;
           end
          else
           begin
	     branvar[9] <= branvar[9] + 1;   
             if(pc[2])
               begin
	         branvar[10] <= branvar[10] + 1;   
                 fetch2decodepredicted_pc <=  pc_plus_4;
	         pc <=  pc_plus_4;
               end
             else
               begin
	         branvar[11] <= branvar[11] + 1;   
                 fetch2decodepredicted_pc <=  pc_plus_8;
	         pc <=  pc_plus_8;
               end
           end
 
	 end
	  end
	  end
	 end
	  end
	 end
	  endmodule 
