`timescale 1ns/10ps
module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num, int bit_width);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg  clock;
reg  reset;
reg  start;
reg  r_button;
reg  g_button;
reg  key;
reg  test;
reg  rts;
reg  rtr;
reg  [3 : 0] v_in;
reg  cts ;
reg  ctr ;
reg  [3 : 0] v_out;

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;
b10 inst (
    .clock(clock),
    .reset(reset),
    .start(start),
    .r_button(r_button),
    .g_button(g_button),
    .key(key),
    .test(test),
    .rts(rts),
    .rtr(rtr),
    .v_in(v_in[3:0]),
    .cts(cts),
    .ctr(ctr),
    .v_out(v_out[3:0])
    );

initial begin
#1
  start_build_cfg("../rtl/b10_inst.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num=0;
  inst.stato = 0;
  inst.voto0 = 0;
  inst.voto1 = 0;
  inst.voto2 = 0;
  inst.voto3 = 0;
  inst.sign = 4'b0000;
  inst.last_g = 0;
  inst.last_r = 0;
  inst.cts = 0;
  inst.ctr = 0;
  inst.v_out = 4'b0000;

end

always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  for(j=0;j<=100;j=j+1)
    branvar_last[j]=inst.branvar[j];
  #7; 
end

always
begin
  #6;
  for(k=0;k<=100;k=k+1)
   begin
     if(branvar_last[k]!=inst.branvar[k])
       record_branch(k,frame_num); 
   end
  #4;
end

always
begin
  #4;
  if(frame_num == -1)
   inst.stato = 0;
  var_value = get_var_assignment("reset",frame_num+1,1);
  if(frame_num<0)
    reset = 1;
  else
    reset = 0;

  var_value = get_var_assignment("start",frame_num+1,1);
  if(var_value == -1)
     start = $random;
  else 
     start = var_value;
  
  var_value = get_var_assignment("r_button",frame_num+1,1);
  if(var_value == -1)
     r_button = $random;
  else 
     r_button = var_value;

  var_value = get_var_assignment("g_button",frame_num+1,1);
  if(var_value == -1)
     g_button = $random;
  else 
     g_button = var_value;

 var_value = get_var_assignment("key",frame_num+1,1);
  if(var_value == -1)
     key = $random;
  else 
     key = var_value;

 var_value = get_var_assignment("test",frame_num+1,1);
  if(var_value == -1)
     test = $random;
  else 
     test = var_value;

 var_value = get_var_assignment("rts",frame_num+1,1);
  if(var_value == -1)
     rts = $random;
  else 
     rts = var_value;

 var_value = get_var_assignment("rtr",frame_num+1,1);
  if(var_value == -1)
     rtr = $random;
  else 
     rtr = var_value;

 var_value = get_var_assignment("v_in",frame_num+1,2);
  if(var_value == -1)
     v_in = $random;
  else 
     v_in = var_value;


  frame_num = frame_num + 1;
  #5
  if(frame_num==10)
   begin
     ret_value=generate_nxt_pattern(frame_num,pat_num);
     pat_num = pat_num + 1; 
     if(ret_value==0)
     begin
       result_stat(42, 10, pat_num);
       $finish;
     end
     frame_num=-1;
   end
  #1;
end

initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end



endmodule

