////////////////////////////////////////////////////////////////////////////////
// scoreboard.v (Scoreboard)
// Converted from revision 3283
////////////////////////////////////////////////////////////////////////////////
//
//
////////////////////////////////////////////////////////////////////////////////

//typedef enum logic{
//  SB_INVALID  = 1'b0,
//  SB_READY  = 1'b1
//} scoreboard_t;

//parameter SB_READ_PORTS  = 4;
//parameter SB_WRITE_PORTS = 4;

////////////////////////////////////////////////////////////////////////////////
module scoreboard(
  // control
  clk, 
  reset,
  // source registers (read sb)
  src0_addr,
  src1_addr,
  src2_addr,
  src3_addr,
  // destination registers (read sb)
  dst0_addr,
  dst0_vld,
  dst1_addr,
  dst1_vld,
  // inputs from wb
  wb0_addr,
  wb0_vld,
  wb1_addr,
  wb1_vld,
  // ready outputs (true if ready)
  src0_rdy,
  src1_rdy,
  src2_rdy,
  src3_rdy,
  dst0_rdy,
  dst1_rdy
);

  // control
  input clk; 
  input reset;
  // source registers (read sb)
  input [4:0] src0_addr;
  input [4:0] src1_addr;
  input [4:0] src2_addr;
  input [4:0] src3_addr;
  // destination registers (read sb)
  input [4:0] dst0_addr;
  input dst0_vld;
  input [4:0] dst1_addr;
  input dst1_vld;
  // inputs from wb
  input [4:0] wb0_addr;
  input wb0_vld;
  input [4:0] wb1_addr;
  input wb1_vld;
  // ready outputs (true if ready)
  output src0_rdy;
  output src1_rdy;
  output src2_rdy;
  output src3_rdy;
  output dst0_rdy;
  output dst1_rdy;
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////
  reg sb[0:31];      // scoreboard bits
  reg next_sb     [0:4-1]; // next scoreboard bits
  reg [4:0] next_sb_addr[0:4-1]; // next scoreboard bits
  reg next_sb_vld [0:4-1]; // next scoreboard bits

////////////////////////////////////////////////////////////////////////////////
// *** local wires and regs ***
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// *** combinational logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_READ
  //////////////////////////////////////////////////////////////////////////////
  // 
  //////////////////////////////////////////////////////////////////////////////
  integer s;
  reg sb_read[0:4-1];

  assign dst0_rdy = sb[dst0_addr]!=1'b0;
  assign dst1_rdy = sb[dst1_addr]!=1'b0;// && (dst0_addr!=dst1_addr || !dst0_vld || !dst1_vld);

  assign src0_rdy = sb[src0_addr] || src0_addr == 5'b0;
  assign src1_rdy = sb[src1_addr] || src1_addr == 5'b0;
  assign src2_rdy = sb[src2_addr] || src2_addr == 5'b0;
  assign src3_rdy = sb[src3_addr] || src3_addr == 5'b0;

  assign next_sb_addr[0] = dst0_addr;
  assign next_sb_addr[1] = dst1_addr;
  assign next_sb_addr[2] = wb0_addr;
  assign next_sb_addr[3] = wb1_addr;
  //////////////////////////////////////////////////////////////////////////////
/*  always_comb begin: SB_READ

    for(s=0;s<4;s++) begin
      sb_read[s] = sb[s]; 
    end
  end
  */
  //////////////////////////////////////////////////////////////////////////////
 
  
  //////////////////////////////////////////////////////////////////////////////
  // SB_UNLOCK
  //////////////////////////////////////////////////////////////////////////////
  integer w;
  integer wp;
  always @ (dst0_vld or
			dst0_addr or
			sb or
			dst1_vld or
			dst1_addr or
			wb0_vld or
			wb1_vld) begin: SB_UNLOCK
    // decode/issue ports (lock) 
    next_sb[0] = 1'b0;
    if(dst0_vld && sb[dst0_addr]==1'b1) begin
      next_sb_vld[0] = 1'b1;
    end else begin
      next_sb_vld[0] = 1'b0;
    end
    next_sb[1] = 1'b0;
    if(dst1_vld && sb[dst1_addr]==1'b1) begin
      next_sb_vld[1] = 1'b1;
    end else begin
      next_sb_vld[1] = 1'b0;
    end
    // writeback ports (unlock)
    next_sb[2] = 1'b1;
    if(wb0_vld) begin
      next_sb_vld[2] = 1'b1;
    end else begin
      next_sb_vld[2] = 1'b0;
    end
    next_sb[3] = 1'b1;
    if(wb1_vld) begin
      next_sb_vld[3] = 1'b1;
    end else begin
      next_sb_vld[3] = 1'b0;
    end
  end
  //////////////////////////////////////////////////////////////////////////////
 

////////////////////////////////////////////////////////////////////////////////
// *** sequential logic ***
////////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  // SB_UPDATE
  //////////////////////////////////////////////////////////////////////////////
  // update the scoreboard
  // for reset
  integer i;
  //////////////////////////////////////////////////////////////////////////////
  always @ (posedge clk) begin: SB_UPDATE

    // reset
    if( reset == 1 ) begin
      for(i=0;i<32;i=i+1) begin
        sb[i] <= 1'b1; 
      end
    end

    // clock
    else begin

      // for each port, write update if enabled
      for(i=0;i<4;i=i+1) begin
        if(next_sb_vld[i]) begin
          sb[next_sb_addr[i]] <= next_sb[i];
        end
      end

    end

  end
  //////////////////////////////////////////////////////////////////////////////
 
  
endmodule
////////////////////////////////////////////////////////////////////////////////



