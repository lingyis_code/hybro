`timescale 1ns/10ps
module b10 (clock , reset , start , r_button , g_button , key , test , rts , rtr , v_in , cts , ctr , v_out );

	  input  clock;
	  input  reset;
	  input  start;
	  input  r_button;
	  input  g_button;
	  input  key;
	  input  test;
	  input  rts;
	  input  rtr;
	  input [3 : 0] v_in;
	  output cts ;
	  output ctr ;
	  output [3 : 0] v_out;
	  wire clock;
	  wire reset;
	  wire start;
	  wire r_button;
	  wire g_button;
	  wire key;
	  wire test;
	  wire rts;
	  wire rtr;
	  wire [3 : 0] v_in;
	  reg cts;
	  reg [7:0] branvar[0:100];
	  reg ctr;
	  reg [3 : 0] v_out;
	  reg [3 : 0] stato;
	  reg [3 : 0] sign;
	  reg voto0;
          reg voto1;
          reg voto2;
          reg voto3;
          reg last_g;
          reg last_r;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  stato <=  0;
	  voto0 <=  0;
	  voto1 <=  0;
	  voto2 <=  0;
	  voto3 <=  0;
	  sign <=  4'b0000;
	  last_g <=  0;
	  last_r <=  0;
	  cts <= 0;
	  ctr <= 0;
	  v_out <= 4'b0000;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case( stato)
	  0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	  voto0 <=  0;
	  voto1 <=  0;
	  voto2 <=  0;
	  voto3 <=  0;
	  cts <= 0;
	  ctr <= 0;
	 if((!  test)) 
	  begin 
	 branvar[3] <= branvar[3] + 1; 
	 begin
	  sign <=  4'b0000;
	  stato <=  9;
	 end
	  end
	 else 
	   begin 
	   branvar[4] <= branvar[4] + 1;   
	 begin
	  voto0 <=  0;
	  voto1 <=  0;
	  voto2 <=  0;
	  voto3 <=  0;
	  stato <=  1;
	 end
	  end
	 end
	  end
	  1: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 begin
	 if( start) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	 begin
	  voto0 <=  0;
	  voto1 <=  0;
	  voto2 <=  0;
	  voto3 <=  0;
	  stato <=  2;
	 end
	  end
	 else 
	 begin  
	   branvar[7] <= branvar[7] + 1;   
	   end 
	 if( rtr) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	  cts <= 1;
	  end
	 else 
	 begin  
	   branvar[9] <= branvar[9] + 1;   
	   end 
	 if((!  rtr)) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	  cts <= 0;
	  end
	 else 
	 begin  
	   branvar[11] <= branvar[11] + 1;   
	   end 
	 end
	  end
	  2: 
	    begin 
	   branvar[12] <= branvar[12] + 1;
	 begin
	 if((!  start)) 
	  begin 
	 branvar[13] <= branvar[13] + 1; 
	  stato <=  3;
	  end
	 else 
	   begin 
	   branvar[14] <= branvar[14] + 1;   
	 if( key) 
	  begin 
	 branvar[15] <= branvar[15] + 1; 
	 begin
	  voto0 <=   key;
	 if(( ( g_button ^  last_g) &  g_button)) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	  voto1 <=  (~  voto1);
	  end
	 else 
	 begin  
	   branvar[17] <= branvar[17] + 1;   
	   end 
	 if(( ( r_button ^  last_r) &  r_button)) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	  voto2 <=  (~  voto2);
	  end
	 else 
	 begin  
	   branvar[19] <= branvar[19] + 1;   
	   end 
	  last_g <=   g_button;
	  last_r <=   r_button;
	 end
	  end
	 else 
	   begin 
	   branvar[20] <= branvar[20] + 1;   
	 begin
	  voto0 <=  0;
	  voto1 <=  0;
	  voto2 <=  0;
	  voto3 <=  0;
	 end
	  end
	  end
	 end
	  end
	  3: 
	    begin 
	   branvar[21] <= branvar[21] + 1;
	 begin
	  voto3 <=  (( voto0 ^  voto1) ^  voto2);
	  stato <=  4;
	  voto0 <=  0;
	 end
	  end
	  4: 
	    begin 
	   branvar[22] <= branvar[22] + 1;
	 if( rtr) 
	  begin 
	 branvar[23] <= branvar[23] + 1; 
	 begin
	  v_out[0] <=  voto0;
	  v_out[1] <=  voto1;
	  v_out[2] <=  voto2;
	  v_out[3] <=  voto3;
	  cts <= 1;
	 if(((((!  voto0) &&  voto1) &&  voto2) && (!  voto3))) 
	  begin 
	 branvar[24] <= branvar[24] + 1; 
	  stato <=  8;
	  end
	 else 
	   begin 
	   branvar[25] <= branvar[25] + 1;   
	  stato <=  5;
	  end
	 end
	  end
	 else 
	 begin  
	   branvar[26] <= branvar[26] + 1;   
	   end 
	  end
	  5: 
	    begin 
	   branvar[27] <= branvar[27] + 1;
	 if((!  rts)) 
	  begin 
	 branvar[28] <= branvar[28] + 1; 
	 begin
	  ctr <= 1;
	  stato <=  6;
	 end
	  end
	 else 
	 begin  
	   branvar[29] <= branvar[29] + 1;   
	   end 
	  end
	  6: 
	    begin 
	   branvar[30] <= branvar[30] + 1;
	 if( rts) 
	  begin 
	 branvar[31] <= branvar[31] + 1; 
	 begin
	  voto0 <=   v_in[0];
	  voto1 <=   v_in[1];
	  voto2 <=   v_in[2];
	  voto3 <=   v_in[3];
	  ctr <= 0;
	  stato <=  7;
	 end
	  end
	 else 
	 begin  
	   branvar[32] <= branvar[32] + 1;   
	   end 
	  end
	  7: 
	    begin 
	   branvar[33] <= branvar[33] + 1;
	 if((!  rtr)) 
	  begin 
	 branvar[34] <= branvar[34] + 1; 
	 begin
	  cts <= 0;
	  stato <=  4;
	 end
	  end
	 else 
	 begin  
	   branvar[35] <= branvar[35] + 1;   
	   end 
	  end
	  8: 
	    begin 
	   branvar[36] <= branvar[36] + 1;
	 if((!  rtr)) 
	  begin 
	 branvar[37] <= branvar[37] + 1; 
	 begin
	  cts <= 0;
	  stato <=  1;
	 end
	  end
	 else 
	 begin  
	   branvar[38] <= branvar[38] + 1;   
	   end 
	  end
	  9: 
	    begin 
	   branvar[39] <= branvar[39] + 1;
	 begin
	  voto0 <=   v_in[0];
	  voto1 <=   v_in[1];
	  voto2 <=   v_in[2];
	  voto3 <=   v_in[3];
	  sign <=  4'b1000;
	 if(((( voto0 &&  voto1) &&  voto2) &&  voto3)) 
	  begin 
	 branvar[40] <= branvar[40] + 1; 
	  stato <=  10;
	  end
	 else 
	 begin  
	   branvar[41] <= branvar[41] + 1;   
	   end 
	 end
	  end
	  10: 
	    begin 
	   branvar[42] <= branvar[42] + 1;
	 begin
	  voto0 <=  ~sign[0];
	  voto0 <=  sign[1];
	  voto0 <=  sign[2];
	  voto0 <=  ~sign[3];
	  stato <=  4;
	 end
	  end
	 endcase
	  end
	 end

     `include "../rtl/b10.assert"

	  endmodule 
