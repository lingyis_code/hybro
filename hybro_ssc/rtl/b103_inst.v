`timescale 1ns/10ps
module b103 (clk , rst , sig_ibuserr , sig_dbuserr , sig_illegal , sig_align , sig_range , sig_dtlbmiss , sig_dmmufault , sig_int , sig_syscall , sig_trap , sig_itlbmiss , sig_immufault , sig_tick , branch_taken , genpc_freeze , id_freeze , ex_freeze , wb_freeze , if_stall , if_pc , id_pc , lr_sav , flushpipe , extend_flush , except_type , except_start , except_started , except_stop , ex_void , spr_dat_ppc , spr_dat_npc , datain , du_dsr , epcr_we , eear_we , esr_we , pc_we , epcr , eear , esr , sr_we , to_sr , sr , lsu_addr , abort_ex , icpu_ack_i , icpu_err_i , dcpu_ack_i , dcpu_err_i );

	  input  clk;
	  input  rst;
	  input  sig_ibuserr;
	  input  sig_dbuserr;
	  input  sig_illegal;
	  input  sig_align;
	  input  sig_range;
	  input  sig_dtlbmiss;
	  input  sig_dmmufault;
	  input  sig_int;
	  input  sig_syscall;
	  input  sig_trap;
	  input  sig_itlbmiss;
	  input  sig_immufault;
	  input  sig_tick;
	  input  branch_taken;
	  input  genpc_freeze;
	  input  id_freeze;
	  input  ex_freeze;
	  input  wb_freeze;
	  input  if_stall;
	  input [31 : 0] if_pc;
	  output [31 : 0] id_pc;
	  output [29 : 0] lr_sav;
	  input [31 : 0] datain;
	  input [13 : 0] du_dsr;
	  input  epcr_we;
	  input  eear_we;
	  input  esr_we;
	  input  pc_we;
	  output [31 : 0] epcr;
	  output [31 : 0] eear;
	  output [15 : 0] esr;
	  input [15 : 0] to_sr;
	  input  sr_we;
	  input [15 : 0] sr;
	  input [31 : 0] lsu_addr;
	  output flushpipe ;
	  output extend_flush ;
	  output [3 : 0] except_type;
	  output except_start ;
	  output except_started ;
	  output [12 : 0] except_stop;
	  input  ex_void;
	  output [31 : 0] spr_dat_ppc;
	  output [31 : 0] spr_dat_npc;
	  output abort_ex ;
	  input  icpu_ack_i;
	  input  icpu_err_i;
	  input  dcpu_ack_i;
	  input  dcpu_err_i;
	  reg [3 : 0] except_type;
	  reg [7:0] branvar[0:1000];
	  reg [31 : 0] id_pc;
       wire [31:0] temp;
       wire [31:0] temp1;
       wire [31:0] temp2;
	  reg [31 : 0] ex_pc;
	  reg [31 : 0] wb_pc;
	  reg [31 : 0] epcr;
	  reg [31 : 0] eear;
	  reg [15 : 0] esr;
	  reg [2 : 0] id_exceptflags;
	  reg [2 : 0] ex_exceptflags;
	  reg [2 : 0] state;
	  reg extend_flush;
	  reg extend_flush_last;
	  reg ex_dslot;
	  reg delayed1_ex_dslot;
	  reg delayed2_ex_dslot;
	  wire except_started;
	  wire [12 : 0] except_trig;
	  wire except_flushpipe;
	  reg [2 : 0] delayed_iee;
	  reg [2 : 0] delayed_tee;
	  wire int_pending;
	  wire tick_pending;
	 assign 
	  except_started = (  extend_flush &  except_start);
	 assign 
	  lr_sav = ex_pc[31:2];
	 assign 
	  spr_dat_ppc =  wb_pc;
	 assign 
	  spr_dat_npc = (  ex_void ?  id_pc :  ex_pc);
	 assign 
	  except_start = ( (  except_type != 0) &  extend_flush);
	 assign 
	  int_pending = ( ( ( ( ( (  sig_int &  sr[2]) &  delayed_iee[2]) & (~  ex_freeze)) & (~  branch_taken)) & (~  ex_dslot)) & (~  sr_we));
	 assign 
	  tick_pending = ( ( ( ( (  sig_tick &  sr[1]) & (~  ex_freeze)) & (~  branch_taken)) & (~  ex_dslot)) & (~  sr_we));
	 assign 
	  abort_ex = ( ( ( (  sig_dbuserr |  sig_dmmufault) |  sig_dtlbmiss) |  sig_align) |  sig_illegal);
	 assign 
	  except_trig = {(  tick_pending & (~  du_dsr[4])),(  int_pending & (~  du_dsr[7])),(  ex_exceptflags[1] & (~  du_dsr[9])),(  ex_exceptflags[0] & (~  du_dsr[3])),(  ex_exceptflags[2] & (~  du_dsr[1])),(  sig_illegal & (~  du_dsr[6])),(  sig_align & (~  du_dsr[5])),(  sig_dtlbmiss & (~  du_dsr[8])),(  sig_dmmufault & (~  du_dsr[2])),(  sig_dbuserr & (~  du_dsr[1])),(  sig_range & (~  du_dsr[10])),( (  sig_trap & (~  du_dsr[13])) & (~  ex_freeze)),( (  sig_syscall & (~  du_dsr[11])) & (~  ex_freeze))};
	 assign 
	  except_stop = {(  tick_pending &  du_dsr[4]),(  int_pending &  du_dsr[7]),(  ex_exceptflags[1] &  du_dsr[9]),(  ex_exceptflags[0] &  du_dsr[3]),(  ex_exceptflags[2] &  du_dsr[1]),(  sig_illegal &  du_dsr[6]),(  sig_align &  du_dsr[5]),(  sig_dtlbmiss &  du_dsr[8]),(  sig_dmmufault &  du_dsr[2]),(  sig_dbuserr &  du_dsr[1]),(  sig_range &  du_dsr[10]),( (  sig_trap &  du_dsr[13]) & (~  ex_freeze)),( (  sig_syscall &  du_dsr[11]) & (~  ex_freeze))};
	 assign 
	  flushpipe = ( (  except_flushpipe |  pc_we) |  extend_flush);
	 assign 
	  except_flushpipe =  (except_trig[12]||except_trig[11]||except_trig[10]||except_trig[9]||except_trig[8]||except_trig[7]||except_trig[6]||except_trig[5]||except_trig[4]||except_trig[3]||except_trig[2]||except_trig[1]||except_trig[0]) 
          && (~state[2]) &&(~state[1]) &&(~state[0]);

      assign 
        temp =  ex_dslot ?  wb_pc :  ex_pc;
      assign 
        temp1 =  ex_dslot ?  wb_pc :  id_pc;
      assign 
        temp2 =  ex_dslot ?  ex_pc :  id_pc;

	 always @ (posedge  clk or posedge  rst)
	 begin
	 if( rst) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  id_pc <= 32'h0;
	  id_exceptflags <= 3'b000;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 if( flushpipe) 
	  begin 
	 branvar[2] <= branvar[2] + 1; 
	 begin
	  id_pc <= 32'h0;
	  id_exceptflags <= 3'b000;
	 end
	  end
	 else 
	   begin 
	   branvar[3] <= branvar[3] + 1;   
	 if((!  id_freeze)) 
	  begin 
	 branvar[4] <= branvar[4] + 1; 
	 begin
	  id_pc <=  if_pc;
	  id_exceptflags <= { sig_ibuserr, sig_itlbmiss, sig_immufault};
	 end
	  end
	 else 
	 begin  
	   branvar[5] <= branvar[5] + 1;   
	   end 
	  end
	  end
	 end
	 always @ (posedge  rst or posedge  clk)
	 if( rst) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	  delayed_iee <= 3'b000;
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	 if((!  sr[2])) 
	  begin 
	 branvar[8] <= branvar[8] + 1; 
	  delayed_iee <= 3'b000;
	  end
	 else 
	   begin 
	   branvar[9] <= branvar[9] + 1;   
	  delayed_iee <= {delayed_iee[1:0],1'b1};
	  end
	  end
	 always @ (posedge  rst or posedge  clk)
	 if( rst) 
	  begin 
	 branvar[10] <= branvar[10] + 1; 
	  delayed_tee <= 3'b000;
	  end
	 else 
	   begin 
	   branvar[11] <= branvar[11] + 1;   
	 if((!  sr[1])) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	  delayed_tee <= 3'b000;
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	  delayed_tee <= {delayed_tee[1:0],1'b1};
	  end
	  end
	 always @ (posedge  clk or posedge  rst)
	 begin
	 if( rst) 
	  begin 
	 branvar[14] <= branvar[14] + 1; 
	 begin
	  ex_dslot <= 1'b0;
	  ex_pc <= 32'd0;
	  ex_exceptflags <= 3'b000;
	  delayed1_ex_dslot <= 1'b0;
	  delayed2_ex_dslot <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[15] <= branvar[15] + 1;   
	 if( flushpipe) 
	  begin 
	 branvar[16] <= branvar[16] + 1; 
	 begin
	  ex_dslot <= 1'b0;
	  ex_pc <= 32'h0;
	  ex_exceptflags <= 3'b000;
	  delayed1_ex_dslot <= 1'b0;
	  delayed2_ex_dslot <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[17] <= branvar[17] + 1;   
	 if(( (!  ex_freeze) &  id_freeze)) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	 begin
	  ex_dslot <= 1'b0;
	  ex_pc <=  id_pc;
	  ex_exceptflags <= 3'b000;
	  delayed1_ex_dslot <=  ex_dslot;
	  delayed2_ex_dslot <=  delayed1_ex_dslot;
	 end
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	 if((!  ex_freeze)) 
	  begin 
	 branvar[20] <= branvar[20] + 1; 
	 begin
	  ex_dslot <=  branch_taken;
	  ex_pc <=  id_pc;
	  ex_exceptflags <=  id_exceptflags;
	  delayed1_ex_dslot <=  ex_dslot;
	  delayed2_ex_dslot <=  delayed1_ex_dslot;
	 end
	  end
	 else 
	 begin  
	   branvar[21] <= branvar[21] + 1;   
	   end 
	  end
	  end
	  end
	 end
	 always @ (posedge  clk or posedge  rst)
	 begin
	 if( rst) 
	  begin 
	 branvar[22] <= branvar[22] + 1; 
	 begin
	  wb_pc <= 32'd0;
	 end
	  end
	 else 
	   begin 
	   branvar[23] <= branvar[23] + 1;   
	 if((!  wb_freeze)) 
	  begin 
	 branvar[24] <= branvar[24] + 1; 
	 begin
	  wb_pc <=  ex_pc;
	 end
	  end
	 else 
	 begin  
	   branvar[25] <= branvar[25] + 1;   
	   end 
	  end
	 end
	 always @ (posedge  clk or posedge  rst)
	 begin
	 if( rst) 
	  begin 
	 branvar[26] <= branvar[26] + 1; 
	 begin
	  state <= 0;
	  except_type <= 0;
	  extend_flush <= 1'b0;
	  epcr <= 32'b00000000000000000000000000000000;
	  eear <= 32'b00000000000000000000000000000000;
	  esr <= {1'b1,14'h0,1'b1};
	  extend_flush_last <= 1'b0;
	 end
	  end
	 else 
	   begin 
	   branvar[27] <= branvar[27] + 1;   
	 begin
	 case( state)
	  0: 
	    begin 
	   branvar[28] <= branvar[28] + 1;
	 if( except_flushpipe) 
	  begin 
	 branvar[29] <= branvar[29] + 1; 
	 begin
	  state <= 1;
	  extend_flush <= 1'b1;
	  esr <= (  sr_we ?  to_sr :  sr);
	 if( except_trig[12]) 
	  begin 
	 branvar[30] <= branvar[30] + 1; 
	 begin
	  except_type <= 4'h5;
	  epcr <= temp1;
      //(  ex_dslot ?  wb_pc : (  delayed1_ex_dslot ?  id_pc : (  delayed2_ex_dslot ?  id_pc :  id_pc)));
	 end
	  end
	 else 
	   begin 
	   branvar[31] <= branvar[31] + 1;   
	 if( except_trig[11]) 
	  begin 
	 branvar[32] <= branvar[32] + 1; 
	 begin
	  except_type <= 4'h8;
       epcr <= temp1;
	  //epcr <= (  ex_dslot ?  wb_pc : (  delayed1_ex_dslot ?  id_pc : (  delayed2_ex_dslot ?  id_pc :  id_pc)));
	 end
	  end
	 else 
	   begin 
	   branvar[33] <= branvar[33] + 1;   
	 if( except_trig[10]) 
	  begin 
	 branvar[34] <= branvar[34] + 1; 
	 begin
	  except_type <= 4'ha;
	  eear <=  ex_pc;
	  epcr <= temp;
       //(  ex_dslot ?  wb_pc :  ex_pc);
	 end
	  end
	 else 
	   begin 
	   branvar[35] <= branvar[35] + 1;   
	 if( except_trig[9]) 
	  begin 
	 branvar[36] <= branvar[36] + 1; 
	 begin
	  except_type <= 4'h4;
       eear <= temp2;
       epcr <= temp1;
	  //eear <= (  ex_dslot ?  ex_pc : (  delayed1_ex_dslot ?  id_pc : (  delayed2_ex_dslot ?  id_pc :  id_pc)));
	  //epcr <= (  ex_dslot ?  wb_pc : (  delayed1_ex_dslot ?  id_pc : (  delayed2_ex_dslot ?  id_pc :  id_pc)));
	 end
	  end
	 else 
	   begin 
	   branvar[37] <= branvar[37] + 1;   
	 if( except_trig[8]) 
	  begin 
	 branvar[38] <= branvar[38] + 1; 
	 begin
	  except_type <= 4'h2;
       eear <= temp;
       epcr <= temp;
	  //eear <= (  ex_dslot ?  wb_pc :  ex_pc);
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
	 end
	  end
	 else 
	   begin 
	   branvar[39] <= branvar[39] + 1;   
	 if( except_trig[7]) 
	  begin 
	 branvar[40] <= branvar[40] + 1; 
	 begin
	  except_type <= 4'h7;
	  eear <=  ex_pc;
       epcr <= temp;
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
	 end
	  end
	 else 
	   begin 
	   branvar[41] <= branvar[41] + 1;   
	 if( except_trig[6]) 
	  begin 
	 branvar[42] <= branvar[42] + 1; 
	 begin
	  except_type <= 4'h6;
	  eear <=  lsu_addr;
       epcr <= temp;
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
	 end
	  end
	 else 
	   begin 
	   branvar[43] <= branvar[43] + 1;   
	 if( except_trig[5]) 
	  begin 
	 branvar[44] <= branvar[44] + 1; 
	 begin
	  except_type <= 4'h9;
	  eear <=  lsu_addr;
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
       epcr <= temp;
	 end
	  end
	 else 
	   begin 
	   branvar[45] <= branvar[45] + 1;   
	 if( except_trig[4]) 
	  begin 
	 branvar[46] <= branvar[46] + 1; 
	 begin
	  except_type <= 4'h3;
	  eear <=  lsu_addr;
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
       epcr <= temp;
	 end
	  end
	 else 
	   begin 
	   branvar[47] <= branvar[47] + 1;   
	 if( except_trig[3]) 
	  begin 
	 branvar[48] <= branvar[48] + 1; 
	 begin
	  except_type <= 4'h2;
	  eear <=  lsu_addr;
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
       epcr <= temp;
	 end
	  end
	 else 
	   begin 
	   branvar[49] <= branvar[49] + 1;   
	 if( except_trig[2]) 
	  begin 
	 branvar[50] <= branvar[50] + 1; 
	 begin
	  except_type <= 4'hb;
	  epcr <= temp1;
  
	 end
	  end
	 else 
	   begin 
	   branvar[51] <= branvar[51] + 1;   
	 if( except_trig[1]) 
	  begin 
	 branvar[52] <= branvar[52] + 1; 
	 begin
	  except_type <= 4'he;
       epcr <= temp;
	  //epcr <= (  ex_dslot ?  wb_pc :  ex_pc);
	 end
	  end
	 else 
	   begin 
	   branvar[53] <= branvar[53] + 1;   
	 if( except_trig[0]) 
	  begin 
	 branvar[54] <= branvar[54] + 1; 
	 begin
	  except_type <= 4'hc;
       epcr <= temp1;   
	  //epcr <= (  ex_dslot ?  wb_pc : (  delayed1_ex_dslot ?  id_pc : (  delayed2_ex_dslot ?  id_pc :  id_pc)));
	 end
	  end
	 else 
	 begin  
	   branvar[55] <= branvar[55] + 1;   
	   end 
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	  end
	 end
	  end
	 else 
	   begin 
	   branvar[56] <= branvar[56] + 1;   
	 if( pc_we) 
	  begin 
	 branvar[57] <= branvar[57] + 1; 
	 begin
	  state <= 1;
	  extend_flush <= 1'b1;
	 end
	  end
	 else 
	   begin 
	   branvar[58] <= branvar[58] + 1;   
	 begin
	 if( epcr_we) 
	  begin 
	 branvar[59] <= branvar[59] + 1; 
	  epcr <=  datain;
	  end
	 else 
	 begin  
	   branvar[60] <= branvar[60] + 1;   
	   end 
	 if( eear_we) 
	  begin 
	 branvar[61] <= branvar[61] + 1; 
	  eear <=  datain;
	  end
	 else 
	 begin  
	   branvar[62] <= branvar[62] + 1;   
	   end 
	 if( esr_we) 
	  begin 
	 branvar[63] <= branvar[63] + 1; 
	  esr <= {1'b1,datain[14:0]};
	  end
	 else 
	 begin  
	   branvar[64] <= branvar[64] + 1;   
	   end 
	 end
	  end
	  end
	  end
	  1: 
	    begin 
	   branvar[65] <= branvar[65] + 1;
	 if(( (  icpu_ack_i |  icpu_err_i) |  genpc_freeze)) 
	  begin 
	 branvar[66] <= branvar[66] + 1; 
	  state <= 2;
	  end
	 else 
	 begin  
	   branvar[67] <= branvar[67] + 1;   
	   end 
	  end
	  2: 
	    begin 
	   branvar[68] <= branvar[68] + 1;
	 if((  except_type == 4'h0)) 
	  begin 
	 branvar[69] <= branvar[69] + 1; 
	 begin
	  state <= 0;
	  extend_flush <= 1'b0;
	  extend_flush_last <= 1'b0;
	  except_type <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[70] <= branvar[70] + 1;   
	  state <= 3;
	  end
	  end
	  3: 
	    begin 
	   branvar[71] <= branvar[71] + 1;
	 begin
	  state <= 4;
	 end
	  end
	  4: 
	    begin 
	   branvar[72] <= branvar[72] + 1;
	 begin
	  state <= 5;
	  extend_flush <= 1'b0;
	  extend_flush_last <= 1'b0;
	 end
	  end
	  5: 
	    begin 
	   branvar[73] <= branvar[73] + 1;
	 begin
	 if(((!  if_stall) && (!  id_freeze))) 
	  begin 
	 branvar[74] <= branvar[74] + 1; 
	 begin
	  state <= 0;
	  except_type <= 0;
	  extend_flush_last <= 1'b0;
	 end
	  end
	 else 
	 begin  
	   branvar[75] <= branvar[75] + 1;   
	   end 
	 end
	  end
	 endcase
	 end
	  end
	 end
//`include "../rtl/b103.true"
	  endmodule 
