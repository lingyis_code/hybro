`timescale 1ns/10ps

module test_top;

import "DPI-C" function void start_build_cfg(string vlg_file);
import "DPI-C" function void record_branch(int bran_num, int frame_num);
import "DPI-C" function int  generate_nxt_pattern(int unroll_frame_num, int pat_num);
import "DPI-C" function int  get_var_assignment(string var_name, int frame_num);
import "DPI-C" function void result_stat(int bran_num_max, int frame_num, int pattern_num);

reg clock;
reg reset;

reg [7:0] branvar_last[0:100];

integer i;
integer j;
integer k;
integer frame_num;
integer pat_num;
integer file_branch;
integer var_value;
integer ret_value;

reg  stall_in;
reg  [31:0] branch_pc;
reg  [31:0] branch_addr;
reg  branch_mispredict;
reg  [63:0] icache_read_data_i;
reg  icache_read_data_vld_i;
reg  [31:0]  icache_read_req_addr_o;
reg  icache_read_req_vld_o;
reg  stall_out;
reg  [31:0] fetch2decodeinstr0_out;
reg  [31:0] fetch2decodeinstr1_out;
reg  [31:0] fetch2decodepc1;
reg  [31:0] fetch2decodepc0;
reg  [31:0] fetch2decodepc1_incr;
reg  [31:0] fetch2decodepc0_incr;
reg         fetch2decodeinstr0_valid;
reg         fetch2decodeinstr1_valid;
reg         fetch2decodebranch_taken;
reg  [31:0] fetch2decodepredicted_pc;
reg         fetch2decodevalid;
reg         btb_valid;
reg  [31:0] btb_pc;
reg  [31:0] btb_target;
reg  [31:0] pc;

integer rand_f;
integer rand_cnt;

fetch_stage inst(
  .clk(clock),
  .reset(reset),
  .stall_in(stall_in),
  .branch_pc(branch_pc[31:0]),
  .branch_addr(branch_addr[31:0]),
  .branch_mispredict(branch_mispredict),
  .icache_read_req_addr_o(icache_read_req_addr_o[31:0]),
  .icache_read_req_vld_o(icache_read_req_vld_o),
  .icache_read_data_i(icache_read_data_i[63:0]),
  .icache_read_data_vld_i(icache_read_data_vld_i),
  .stall_out(stall_out),
  .fetch2decodeinstr0_out(fetch2decodeinstr0_out[31:0]),
  .fetch2decodeinstr1_out(fetch2decodeinstr1_out[31:0]),
  .fetch2decodepc1(fetch2decodepc1[31:0]),
  .fetch2decodepc0(fetch2decodepc0[31:0]),
  .fetch2decodepc1_incr(fetch2decodepc1_incr[31:0]),
  .fetch2decodepc0_incr(fetch2decodepc0_incr[31:0]),
  .fetch2decodeinstr0_valid(fetch2decodeinstr0_valid),
  .fetch2decodeinstr1_valid(fetch2decodeinstr1_valid),
  .fetch2decodebranch_taken(fetch2decodebranch_taken),
  .fetch2decodepredicted_pc(fetch2decodepredicted_pc[31:0]),
  .fetch2decodevalid(fetch2decodevalid)
);

initial begin
#1
  rand_f = 0;
  rand_cnt = 0;
  start_build_cfg("source_rtl/fetch.v");
  clock = 1'b0;
  i = 0;
  frame_num=-1;
  pat_num = 0;
end

initial begin
 $dumpfile("vectorfetch.vcd");
 $dumpvars(1,inst);
end


always 
begin
  #5 clock = ~clock;
end

always 
begin
  #3;
  if(rand_cnt == 0)
  begin
    for(j=0;j<=100;j=j+1)
      branvar_last[j]=inst.branvar[j];
  end
  #7; 
end

always
begin
  #6;
  if(rand_cnt == 0)
    begin
      for(k=0;k<=100;k=k+1)
       begin
         if(branvar_last[k]!=inst.branvar[k])
           record_branch(k,frame_num); 
       end
    end
  #4;
end

 
always
begin
  #4;
  if(frame_num == -1)
    begin
      inst.btb_valid  = 1'b0;
      inst.btb_pc     = 32'h0;
      inst.btb_target = 32'h0;
      inst.pc = 32'hFFFFFFF8;
      inst.icache_read_req_vld_o = 1'b0;
      inst.fetch2decodevalid        = 1'b0;
      inst.fetch2decodeinstr0_valid = 1'b0;
      inst.fetch2decodeinstr1_valid = 1'b0;
    end

  if(frame_num<0)
     reset = 1;
  else 
     reset = 0;

  var_value = get_var_assignment("stall_in",frame_num+1);
  if(var_value == -1)
    begin
     stall_in = $random;
     rand_f = 1;
    end
  else 
     stall_in = var_value;
  
  var_value = get_var_assignment("branch_pc",frame_num+1);
  if(var_value == -1)
    begin
      branch_pc = $random;
      //$display("\n+++++random branch_pc = %d",branch_pc);
      rand_f = 1;
    end
  else 
     branch_pc = var_value;

  var_value = get_var_assignment("branch_addr",frame_num+1);
  if(var_value == -1)
    begin
      branch_addr = $random;
      rand_f = 1;
    end
  else 
     branch_addr = var_value;

  var_value = get_var_assignment("branch_mispredict",frame_num+1);
  if(var_value == -1)
    begin
     branch_mispredict = $random;
     rand_f = 1;
    end
  else 
     branch_mispredict = var_value;
   
  var_value = get_var_assignment("icache_read_data_i",frame_num+1);
  if(var_value == -1)
    begin
     icache_read_data_i = $random;
     rand_f = 1;
    end
  else 
     icache_read_data_i = var_value;

  var_value = get_var_assignment("icache_read_data_vld_i",frame_num+1);
  if(var_value == -1)
    begin
      icache_read_data_vld_i = $random;
      rand_f = 1;
    end
  else 
     icache_read_data_vld_i = var_value;


  frame_num = frame_num + 1;
  
  //rand_f = 0;
  //$display("\n---frame_num = %d, rand_f = %d, rand_cnt = %d",frame_num, rand_f, rand_cnt);
  #5
  if((frame_num==4)&&(rand_f==1))
   begin
     rand_cnt = rand_cnt + 1;
   end
  #0
  if(rand_cnt > 50)
   begin
     rand_f = 0;
     rand_cnt = 0;
   end
  #0
  //$display("\n===frame_num = %d, rand_f = %d, rand_cnt = %d",frame_num, rand_f, rand_cnt);
  if((frame_num==4)&&(rand_f==0))
   begin
     ret_value=generate_nxt_pattern(frame_num, pat_num);
     pat_num = pat_num + 1;
     if(ret_value==0)
     begin
       result_stat(8, 5, pat_num);
       $finish;
     end
     frame_num=-1;
   end
  if((frame_num==4)&&(rand_f==1))
   begin
     frame_num = -1;
   end
  #1;
end



initial begin
  for(i=0;i<=100;i=i+1)
   inst.branvar[i]=8'h0;
end

endmodule
