`timescale 1ns/10ps
module b01 (clock , reset , line1 , line2 , outp , overflw );

	  input  clock;
	  input  reset;
	  input  line1;
	  input  line2;
	  output outp ;
	  output overflw ;
	  wire clock;
	  wire reset;
	  wire line1,line2;
	  reg outp;
	  reg [7:0] branvar[0:100];
	  reg overflw;
	  reg [2 : 0] stato;
	 always @ (posedge  clock) 
	 begin
	 if( reset) 
	  begin 
	 branvar[0] <= branvar[0] + 1; 
	 begin
	  stato <= 0;
	  outp <= 0;
	  overflw <= 0;
	 end
	  end
	 else 
	   begin 
	   branvar[1] <= branvar[1] + 1;   
	 case(stato)
	  3'h0: 
	    begin 
	   branvar[2] <= branvar[2] + 1;
	 begin
	 if(( line1 &&  line2)) 
	  begin 
	 branvar[3] <= branvar[3] + 1; 
	  stato <= 3'h4;
	  end
	 else 
	   begin 
	   branvar[4] <= branvar[4] + 1;   
	  stato <= 3'h1;
	  end
	  outp <= ( line1 ^  line2);
	  overflw <= 0;
	 end
	  end
	  3'h3: 
	    begin 
	   branvar[5] <= branvar[5] + 1;
	 begin
	 if(( line1 &&  line2)) 
	  begin 
	 branvar[6] <= branvar[6] + 1; 
	  stato <= 3'h4;
	  end
	 else 
	   begin 
	   branvar[7] <= branvar[7] + 1;   
	  stato <= 3'h1;
	  end
	  outp <= ( line1 ^  line2);
	  overflw <= 1;
	 end
	  end
	  3'h1: 
	    begin 
	   branvar[8] <= branvar[8] + 1;
	 begin
	 if(( line1 &&  line2)) 
	  begin 
	 branvar[9] <= branvar[9] + 1; 
	  stato <= 5;
	  end
	 else 
	   begin 
	   branvar[10] <= branvar[10] + 1;   
	  stato <= 2;
	  end
	  outp <= ( line1 ^  line2);
	  overflw <= 0;
	 end
	  end
	  4: 
	    begin 
	   branvar[11] <= branvar[11] + 1;
	 begin
	 if((  line1 ||  line2)) 
	  begin 
	 branvar[12] <= branvar[12] + 1; 
	  stato <= 5;
	  end
	 else 
	   begin 
	   branvar[13] <= branvar[13] + 1;   
	  stato <= 2;
	  end
	  outp <= (~ ( line1 ^  line2));
	  overflw <= 0;
	 end
	  end
	  2: 
	    begin 
	   branvar[14] <= branvar[14] + 1;
	 begin
	 if(( line1 &&  line2)) 
	  begin 
	 branvar[15] <= branvar[15] + 1; 
	  stato <= 7;
	  end
	 else 
	   begin 
	   branvar[16] <= branvar[16] + 1;   
	  stato <= 6;
	  end
	  outp <= ( line1 ^  line2);
	  overflw <= 0;
	 end
	  end
	  5: 
	    begin 
	   branvar[17] <= branvar[17] + 1;
	 begin
	 if((  line1 ||  line2)) 
	  begin 
	 branvar[18] <= branvar[18] + 1; 
	  stato <= 7;
	  end
	 else 
	   begin 
	   branvar[19] <= branvar[19] + 1;   
	  stato <= 6;
	  end
	  outp <= (~ ( line1 ^  line2));
	  overflw <= 0;
	 end
	  end
	  6: 
	    begin 
	   branvar[20] <= branvar[20] + 1;
	 begin
	 if(( line1 &&  line2)) 
	  begin 
	 branvar[21] <= branvar[21] + 1; 
	  stato <= 3;
	  end
	 else 
	   begin 
	   branvar[22] <= branvar[22] + 1;   
	  stato <= 0;
	  end
	  outp <= ( line1 ^  line2);
	  overflw <= 0;
	 end
	  end
	  7: 
	    begin 
	   branvar[23] <= branvar[23] + 1;
	 begin
	 if((  line1 ||  line2)) 
	  begin 
	 branvar[24] <= branvar[24] + 1; 
	  stato <= 3;
	  end
	 else 
	   begin 
	   branvar[25] <= branvar[25] + 1;   
	  stato <= 0;
	  end
	  outp <= (~ ( line1 ^  line2));
	  overflw <= 0;
	 end
	  end
	 endcase
	  end
	 end
`include "../rtl/b01.true"

	  endmodule 
