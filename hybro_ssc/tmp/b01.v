// b01.v
// Verilog version, rewritten by Li Shen, Aug 2002
// Institute of Computing Technology, Chinese Academy of Sciences
// The original VHDL version is b01.vhd from Politecnico di Torino
`timescale 1ns/10ps

module b01 (clock, reset, line1, line2, outp, overflw);
input clock;
input reset;
input line1; 
input line2;
output outp;
output overflw;
/////
wire clock;
wire reset;
wire line1; 
wire line2;
reg outp;
reg overflw;
/////
parameter a=0, b=1, c=2, e=3, f=4, g=5, wf0=6, wf1=7;
reg [2:0] stato;
/////
always @(posedge clock)
begin
  if (reset)
  begin
    stato=a;
    outp<=0;
    overflw<=0;
  end
  else
    case (stato)
    line1: begin
	 if (line1 && line2) stato=f;
	 else		     stato=b;
	 outp<=line1^line2;
	 overflw<=0;
       end
    e: begin
	 if (line1 && line2) stato=f;
	 else		     stato=b;
	 outp<=line1^line2;
	 overflw<=1;
       end
    b: begin
	 if (line1 && line2) stato=g;
         else		     stato=c;
	 outp<=line1^line2;
	 overflw<=0;
       end
    f: begin
	 if (line1 || line2) stato=g;
         else		     stato=c;
	 outp<=~(line1^line2);
	 overflw<=0;
       end
    c: begin
	 if (line1 && line2) stato=wf1;
         else		     stato=wf0;
	 outp<=line1^line2;
	 overflw<=0;
       end
    g: begin
	 if (line1 || line2) stato=wf1;
         else		     stato=wf0;
	 outp<=~(line1^line2);
	 overflw<=0;
       end
    wf0: begin
	   if (line1 && line2) stato=e;
           else		       stato=a;
 	   outp<=line1^line2;
	   overflw<=0;
	 end
    wf1: begin
	   if (line1 || line2) stato=e;
           else		       stato=a;
	   outp<=~(line1^line2);
	   overflw<=0;
	 end
    endcase
end
endmodule
