#include <stdio.h>
#include "c_interface.h"
#include "vlg2sExpr.h"

Expr var_expr[MAXFRAMES][MAX_VAR_IDX];
Expr cnst_exprs[MAX_CNST_NUM];

VC vc;
int constraint_number=0;
Expr res;

char ** ctx_buffer;
unsigned long * ctx_buf_length;

char * result_buf[MAX_VAR_NUMBER];
int result_buf_idx=0;

FILE * pattern_file;

typedef struct __expr_link
{
   Expr p_expr;
   struct __expr_link * next_expr;
} _expr_link;

_expr_link * expr_link_head = NULL;
_expr_link * expr_link_tail = NULL;

void add_to_expr_link(Expr p_cur_expr)
{
  _expr_link * p_expr_temp = (_expr_link *)malloc(sizeof(_expr_link)); 
  p_expr_temp->next_expr = NULL;
  p_expr_temp->p_expr = p_cur_expr;

  if(expr_link_head==NULL)
  {
    expr_link_head = p_expr_temp;
    expr_link_tail = p_expr_temp;
  }
  else
  {
    expr_link_tail->next_expr = p_expr_temp;
    expr_link_tail = p_expr_temp;
  }
}

void free_expr_link()
{
  _expr_link * p_expr_temp = expr_link_head;
  _expr_link * p_expr_temp1;
  while(p_expr_temp!=NULL)
  {
     p_expr_temp1 = p_expr_temp;
     p_expr_temp = p_expr_temp->next_expr;
#ifdef DEBUG_MODE
     //printf("\n--ready to free the expression--\n");
     vc_printExpr(vc,p_expr_temp1->p_expr);
#endif
     vc_DeleteExpr(p_expr_temp1->p_expr);
     free(p_expr_temp1);
  }
  expr_link_head = NULL;
  expr_link_tail = NULL;
}


int stp_initial(int idx_max,int frame_num_max){
  vc = vc_createValidityChecker();
  vc_setFlags(vc,'n');
  vc_setFlags(vc,'d');
#ifdef DEBUG_MODE
  vc_setFlags(vc,'p');
#endif
  char * p_var_name = (char *)malloc(MAXVARNAME*sizeof(char));
  constraint_number = 0;
  if((idx_max>=MAX_VAR_IDX) || (frame_num_max>=MAXFRAMES))
  {
     printf("\n\n------------Fatal Error------------\n\n");
     exit(-1);
  }
  for(int x_idx=0; x_idx<=(frame_num_max+1); x_idx++)
    for(int y_idx=0; y_idx<=idx_max-1; y_idx++)
    {
      //printf("\n%s_%d\n",current_var_table[y_idx].var_name,x_idx); 
      sprintf(p_var_name,"%s_%d",current_var_table[y_idx].var_name,x_idx); 
      //if((current_var_table[y_idx].width_start==0)&&(current_var_table[y_idx].width_end==0))
      //fprintf(cfg_out,"\n--the variable name is %s: the end width is %d===the start width is %d--\n",p_var_name,current_var_table[y_idx].width_end,current_var_table[y_idx].width_start);
      if((current_var_table[y_idx].width_end)>=(current_var_table[y_idx].width_start))
        var_expr[x_idx][y_idx] = vc_varExpr(vc, p_var_name, vc_bvType(vc,((1+current_var_table[y_idx].width_end)-(current_var_table[y_idx].width_start))));
      else if((current_var_table[y_idx].width_end)<(current_var_table[y_idx].width_start))
        var_expr[x_idx][y_idx] = vc_varExpr(vc, p_var_name, vc_bvType(vc,(1+(current_var_table[y_idx].width_start)-(current_var_table[y_idx].width_end))));
      add_to_expr_link(var_expr[x_idx][y_idx]);
    }
  for(int i=0;i<result_buf_idx;i++)
    free(result_buf[i]);
  free(p_var_name);
  result_buf_idx = 0;
}
int stp_destroy(int idx_max, int frame_num_max)
{
  //free_expr_link();
  vc_Destroy(vc);
}
int stp_solver()
{
  //Expr nresp1 = vc_varExpr(vc, "nresp1", vc_bv32Type(vc));
  //Expr packet_get_int0 = vc_varExpr(vc, "packet_get_int0", vc_bv32Type(vc));
  //Expr exprs[] = {
  //  // nresp1 == packet_get_int0
  //  vc_eqExpr(vc, nresp1, packet_get_int0),
  //  // nresp1 > 0
  //  vc_bvGtExpr(vc, nresp1, vc_bv32ConstExprFromInt(vc, 0))
  //};
#ifdef DEBUG_MODE
  printf("\n--1Start to solve the current constraints,constraint_numer = %d--\n",constraint_number);
#endif
  if(constraint_number == 1)
    res = cnst_exprs[0];
  else
  {
    res = vc_orExprN(vc, cnst_exprs, (constraint_number));
    add_to_expr_link(res);
    //+++++++++++++++++++++++++
    //$$$$vc_printExpr(vc, cnst_exprs[39]);
    //$$$$printf("\n+++++++++++++++++++++++\n");
    //$$$$vc_printExpr(vc, cnst_exprs[40]);
    //$$$$vc_query(vc, cnst_exprs[40]);

    //$$$$printf("\n-------------\n");
    //$$$$vc_printExpr(vc, cnst_exprs[12]);
    //$$$$printf("\n-------------\n");
    //$$$$exit(-1);
    //vc_query(vc, res);
    //exit(-1);
    //+++++++++++++++++++++++++

  }
  //printf("\n--2Start to solve the current constraints,constraint_numer = %d--\n",constraint_number);
  //vc_printExpr(vc, res);
  //printf("\n--3Start to solve the current constraints,constraint_numer = %d--\n",constraint_number);
  
  int x = vc_query(vc, res);
#ifdef DEBUG_MODE 
if(x==0)
  printf("\n current vc_query result = %d\n", x);
#endif
  //Expr cex = var_expr[1][1];
  //vc_printExpr(vc, ret_exp);
  //vc_printCounterExample(vc);

  //sscanf();
  //for(int i=0;i<MAX_VAR_NUMBER;i++)
  //  result_buf[]
  char single_char;
  char * buf_p;
  int line_start=0;
  int line_end;
  int sov_length;
  result_buf_idx=0;
  if(x==0)
  { 
    ctx_buf_length = (unsigned long *)malloc(sizeof(unsigned long));
    *ctx_buf_length = MAX_CTX_BUF_LENGTH;
    ctx_buffer = (char **)malloc(10*sizeof(char *));
    vc_printCounterExampleToBuffer(vc,ctx_buffer,ctx_buf_length);
    buf_p = *ctx_buffer;
#ifdef PATTERN_OUT
    pattern_file = fopen("pattern.file","a+");
#endif
    do{
     single_char = *buf_p;  
     if(single_char == '(')
      line_start = buf_p-(*ctx_buffer);
     if(single_char == ')')
     {
      line_end = buf_p-(*ctx_buffer);
      sov_length = line_end-line_start; 
      result_buf[result_buf_idx] = (char *)malloc((1+sov_length)*sizeof(char));
      strncpy(result_buf[result_buf_idx],(buf_p-sov_length+2),sov_length-4);
      result_buf[result_buf_idx][sov_length-4]='\0';
#ifdef PATTERN_OUT
     fprintf(pattern_file,"%s\n",result_buf[result_buf_idx]);
#endif
      result_buf_idx++; 
     }
     buf_p++; 
     //putchar(single_char);
    }while((line_start==0)||(single_char!=':'));
 #ifdef PATTERN_OUT
   if(x==0)
   {
     fprintf(pattern_file,"------------------------\n");
     fclose(pattern_file);
   }
#endif    

    free(*ctx_buffer); 
    free(ctx_buffer);
    free(ctx_buf_length);
  }
  
  return x;
  //vc_printCounterExample(vc);
  //void vc_printCounterExampleToBuffer(VC vc, char **buf,unsigned long *len);
 
  //Expr cex = vc_getCounterExample(vc, res);
  //vc_printExpr(vc, cex);
}

extern "C" int get_var_assignment(char * cur_var_name, int cur_frame_num)
//int get_var_assignment(char * cur_var_name, int cur_frame_num)
{
  char var_buf_temp[MAXNUMVARS];
  char value_temp[MAXNUMVARS];
  char c_eq;
  //int result_buf_idx=0;
  char * var_temp = (char *)malloc(MAXNUMVARS*sizeof(char)); 
  sprintf(var_temp,"%s_%d",cur_var_name,cur_frame_num);
  //printf("\n--%s--\n",var_temp);
  int return_int;
#ifdef RANDOM_GEN
   return -1;
#endif

  for(int sol_idx=0;sol_idx<result_buf_idx;sol_idx++)
  {
    sscanf(result_buf[sol_idx],"%s %c %s",var_buf_temp,&c_eq,value_temp);
    if(strcmp(var_temp,var_buf_temp)==0)
    {
      int value_len = strlen(value_temp);
      return_int =0;
      if(value_temp[1]=='b')
      {
        for(int bit_idx=2;bit_idx<value_len;bit_idx++)
        {
          if(value_temp[bit_idx]=='1')
            return_int = return_int*2 + 1;
          else
            return_int = return_int*2;
        }
      }
      else if(value_temp[1]=='x')
      {
        for(int bit_idx=2;bit_idx<value_len;bit_idx++)
        {
          if((value_temp[bit_idx]>='0')&&(value_temp[bit_idx]<='9'))
            return_int = return_int*16 + (value_temp[bit_idx]-'0');
          else
            return_int = return_int*16 + (value_temp[bit_idx]-'A'+10);
          //printf("return int is %lx", return_int);
         }
      }
      free(var_temp);
      return return_int; 
    }   
  }  
  free(var_temp);
  return -1;
}


//============================================
//============================================
//=========extend to support parsel sel=======
//============================================
//============================================

void stp_constraint_gen(_exptree_node * p_exptree_node,_exptree_node * p_exptree_node_branch,_var_node * p_var_node_reg,int frame_num, bool Is_if_branch,bool t_or_f)
{
#ifdef DEBUG_MODE
  fprintf(cfg_out,"\nstp-------------------calling the print constraints function:--------------------");
#endif    
  Expr temp_expr1;
  Expr temp_expr2;
  Expr temp_expr3;
  Expr temp_left;
  Expr temp_middle;
  Expr temp_right;
  Expr temp_top;
  Expr final_exp;
  int temp_bit_width; 
  int temp_idx;
  int temp_copy;
  if(p_var_node_reg!=NULL)
  {
#ifdef DEBUG_MODE
      fprintf(cfg_out,"\n stpP---at frame : x[i+1] <= x[i] \n");
#endif
    temp_expr1 = var_expr[frame_num+1][p_var_node_reg->var_id];
    temp_expr2 = var_expr[frame_num][p_var_node_reg->var_id];
    temp_top = vc_eqExpr(vc,temp_expr1, temp_expr2);
    add_to_expr_link(temp_top);
    cnst_exprs[constraint_number++] = vc_notExpr(vc,temp_top);
    add_to_expr_link(cnst_exprs[constraint_number-1]);
    return;
  }
  if((p_exptree_node!=NULL))
  {
    //fprintf(cfg_out,"\n-----stp here-----\n");
    if((p_exptree_node->number_string!=NULL)&&(Is_if_branch==false))
    {
#ifdef DEBUG_MODE
      fprintf(cfg_out,"\n stp---at frame %d : %s = %s \n",frame_num,p_exptree_node->number_string,p_exptree_node_branch->number_string);
#endif
      //=============================================
      if(p_exptree_node->tree_node_t!=CNST)
      {
#ifdef DEBUG_MODE
        fprintf(cfg_out, "\n--the width start is %d, width end is %d--\n",p_exptree_node->width_start,p_exptree_node->width_end);
#endif
        temp_expr2=var_expr[frame_num][p_exptree_node->var_table_idx];
        //if(p_exptree_node->width_start==p_exptree_node->width_end)
        //  temp_left = vc_bvBoolExtract_One(vc,temp_expr2, p_exptree_node->width_start);
        //else 
        //{ 
          temp_left = vc_bvExtract(vc,temp_expr2, p_exptree_node->width_start, p_exptree_node->width_end);
        //}
        add_to_expr_link(temp_left);
        //$$temp_left = temp_expr2;
#ifdef DEBUG_MODE
        fprintf(cfg_out, "\n--the width start is %d, width end is %d--\n",p_exptree_node->width_start,p_exptree_node->width_end);
#endif
        //vc_printExpr(vc,temp_left); 
      }

      if(p_exptree_node_branch->tree_node_t!=CNST)
      {
        temp_expr2=var_expr[frame_num][p_exptree_node_branch->var_table_idx]; 
        //if(p_exptree_node_branch->width_start==p_exptree_node_branch->width_end)
        //  temp_right = vc_bvBoolExtract_One(vc,temp_expr2, p_exptree_node_branch->width_start);
        //else
          temp_right = vc_bvExtract(vc,temp_expr2, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end);
        //$$temp_right = temp_expr2;
        //vc_printExpr(vc,temp_right); 
        add_to_expr_link(temp_right);
      }
      else       
      {
        temp_right = vc_bvConstExprFromInt(vc, (p_exptree_node->width_start-p_exptree_node->width_end)+1 ,atoi(p_exptree_node_branch->number_string));
        add_to_expr_link(temp_right);
        //vc_printExpr(vc,temp_right); 

        //if(((p_exptree_node_branch->width_start)-(p_exptree_node_branch->width_end))>(p_exptree_node->width_start-p_exptree_node->width_end))
        //temp_right = vc_bv32ConstExprFromInt(vc,  atoi(p_exptree_node_branch->number_string));
        //temp_right = vc_bvExtract(vc,temp_expr2,p_exptree_node->width_start,p_exptree_node->width_end) 
        //vc_bvConstExprFromInt(vc,(p_exptree_node->width_start-p_exptree_node->width_end)+1, atoi(p_exptree_node_branch->number_string));
      }

      //vc_printExpr(vc,temp_right);
      temp_expr1=vc_eqExpr(vc, temp_left, temp_right); 
      add_to_expr_link(temp_expr1);
      temp_expr2=vc_notExpr(vc,temp_expr1);
      add_to_expr_link(temp_expr2);

      //printf("vc_query result = %d\n", vc_query(vc, temp_expr2));
      //exit(-1);
      cnst_exprs[constraint_number++]= temp_expr2;
#ifdef DEBUG_MODE
      vc_printExpr(vc,temp_expr2);
      printf("---- branch add to the constraint stack-----\n"); 
#endif      
      if((p_exptree_node->tree_node_t!=PARSEL)&&(p_exptree_node->tree_node_t!=INDEX)&&(p_exptree_node->tree_node_t!=CNST)&&(p_exptree_node->tree_node_t!=VAR))
        stp_constraint_gen(NULL,p_exptree_node,NULL,frame_num,false,false);
      if((p_exptree_node_branch->tree_node_t!=PARSEL)&&(p_exptree_node_branch->tree_node_t!=INDEX)&&(p_exptree_node_branch->tree_node_t!=CNST)&&(p_exptree_node_branch->tree_node_t!=VAR))
        stp_constraint_gen(NULL,p_exptree_node_branch,NULL,frame_num,false,false);
 
      //if((p_exptree_node->left!=NULL)&&(p_exptree_node->left->tree_node_t!=PARSEL)&&(p_exptree_node->left->tree_node_t!=INDEX)&&(p_exptree_node->left->tree_node_t!=CNST)&&(p_exptree_node->left->tree_node_t!=VAR))
      //  stp_constraint_gen(NULL,p_exptree_node->left,frame_num,false,false);
      //if((p_exptree_node->middle!=NULL)&&(p_exptree_node->middle->tree_node_t!=PARSEL)&&(p_exptree_node->middle->tree_node_t!=INDEX)&&(p_exptree_node->middle->tree_node_t!=CNST)&&(p_exptree_node->middle->tree_node_t!=VAR))
      //  stp_constraint_gen(NULL,p_exptree_node->middle,frame_num,false,false);
      //if((p_exptree_node->right!=NULL)&&(p_exptree_node->right->tree_node_t!=PARSEL)&&(p_exptree_node->right->tree_node_t!=INDEX)&&(p_exptree_node->right->tree_node_t!=CNST)&&(p_exptree_node->right->tree_node_t!=VAR))
      //  stp_constraint_gen(NULL,p_exptree_node->right,frame_num,false,false);

    }
    else 
    { 
      stp_constraint_gen(NULL,p_exptree_node,NULL,frame_num,true,t_or_f);
    }
  }
  else if(p_exptree_node_branch!=NULL)
  {
#ifdef DEBUG_MODE
      fprintf(cfg_out,"\n stp--dat frame %d : %s , the type is %d\n",frame_num,p_exptree_node_branch->number_string,p_exptree_node_branch->tree_node_t);
#endif
      if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t!=CNST))
      {
        //fprintf(cfg_out, "\nenter the cat frame idx is %d\n",p_exptree_node_branch->left->var_table_idx);
        if(p_exptree_node_branch->tree_node_t == ASS_NBLK)
          temp_expr1 = var_expr[frame_num+1][p_exptree_node_branch->left->var_table_idx];
        else
          temp_expr1 = var_expr[frame_num][p_exptree_node_branch->left->var_table_idx];

        //if(p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)
        if((p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)&&(p_exptree_node_branch->tree_node_t!=ASS_NBLK)&&(p_exptree_node_branch->tree_node_t!=ASS_BLK))
          temp_left=vc_bvBoolExtract_One(vc,temp_expr1,p_exptree_node_branch->left->width_start);
        else
        {
          //vc_printExpr(vc,temp_expr1); 
          //fprintf(cfg_out,"\nleft start = %d, left end = %d",p_exptree_node_branch->left->width_start, p_exptree_node_branch->left->width_end); 
          temp_left= vc_bvExtract(vc,temp_expr1, p_exptree_node_branch->left->width_start, p_exptree_node_branch->left->width_end);
        }
        //fprintf(cfg_out, "\nenter the cat frame\n");
        add_to_expr_link(temp_left);
        //$$temp_left = temp_expr1;
        //vc_printExpr(vc,temp_left); 
        //printf("vc_query result = %d\n", vc_query(vc, temp_left));
        //exit(-1);
      }
      else if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t==CNST)&&((p_exptree_node_branch->tree_node_t==CONCAT)||(p_exptree_node_branch->tree_node_t==CATCOPY)))
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!
        int right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
        int left_operand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
        //if((p_exptree_node_branch->right!=NULL)&&(right_oprand_width<left_operand_width))
        //  temp_left = vc_bvConstExprFromInt(vc,right_oprand_width, atoi(p_exptree_node_branch->left->number_string));
        //else  
          temp_left = vc_bvConstExprFromInt(vc,left_operand_width, atoi(p_exptree_node_branch->left->number_string));
        add_to_expr_link(temp_left);
        //vc_printExpr(vc,temp_left); 
      }
      else if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t==CNST))
      {
        int right_oprand_width;
        if(p_exptree_node_branch->right!=NULL)
          right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
        else
          right_oprand_width = 1000000;
        int left_operand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
        if((p_exptree_node_branch->right!=NULL)&&(right_oprand_width<left_operand_width))
          temp_left = vc_bvConstExprFromInt(vc,right_oprand_width, atoi(p_exptree_node_branch->left->number_string));
        else  
          temp_left = vc_bvConstExprFromInt(vc,left_operand_width, atoi(p_exptree_node_branch->left->number_string));
        add_to_expr_link(temp_left);
        //vc_printExpr(vc,temp_left);  
      }
 

      if((p_exptree_node_branch->middle!=NULL)&&(p_exptree_node_branch->middle->tree_node_t!=CNST))
      {
        temp_expr2 = var_expr[frame_num][p_exptree_node_branch->middle->var_table_idx];
    
        if(p_exptree_node_branch->middle->width_start==p_exptree_node_branch->middle->width_end)
          temp_middle=vc_bvBoolExtract_One(vc,temp_expr2,p_exptree_node_branch->middle->width_start); 
        else 
          temp_middle= vc_bvExtract(vc,temp_expr2, p_exptree_node_branch->middle->width_start, p_exptree_node_branch->middle->width_end);
        add_to_expr_link(temp_middle);
        //$$temp_middle = temp_expr2;
        //vc_printExpr(vc,temp_middle); 
      }
      else if((p_exptree_node_branch->middle!=NULL)&&(p_exptree_node_branch->middle->tree_node_t==CNST))
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        temp_middle = vc_bvConstExprFromInt(vc,(p_exptree_node_branch->middle->width_start+1), atoi(p_exptree_node_branch->middle->number_string));
        add_to_expr_link(temp_middle);
        //vc_printExpr(vc,temp_middle); 
      }

      if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t!=CNST))
      {
        temp_expr3 = var_expr[frame_num][p_exptree_node_branch->right->var_table_idx];
        if((p_exptree_node_branch->right->width_start==p_exptree_node_branch->right->width_end)&&(p_exptree_node_branch->tree_node_t!=ASS_NBLK)&&(p_exptree_node_branch->tree_node_t!=ASS_BLK))
          temp_right=vc_bvBoolExtract_One(vc,temp_expr3,p_exptree_node_branch->right->width_start); 
        else 
          temp_right= vc_bvExtract(vc,temp_expr3, p_exptree_node_branch->right->width_start, p_exptree_node_branch->right->width_end);
        add_to_expr_link(temp_right);
        //$$temp_right = temp_expr3;
        //vc_printExpr(vc,temp_right); 
      }
      else if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t==CNST)&&(p_exptree_node_branch->tree_node_t==CONCAT))
      {
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        int right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
        int left_operand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
        //if((p_exptree_node_branch->left!=NULL)&&(right_oprand_width<left_operand_width))
          temp_right = vc_bvConstExprFromInt(vc,right_oprand_width, atoi(p_exptree_node_branch->right->number_string));
        add_to_expr_link(temp_right);
        //else
        //  temp_right = vc_bvConstExprFromInt(vc,left_operand_width, atoi(p_exptree_node_branch->right->number_string));
        //vc_printExpr(vc,temp_right); 
      }
      else if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t==CNST))
      {
        int right_oprand_width = p_exptree_node_branch->right->width_start- p_exptree_node_branch->right->width_end + 1;
        int left_operand_width = p_exptree_node_branch->left->width_start- p_exptree_node_branch->left->width_end + 1;
        //if((p_exptree_node_branch->left!=NULL)&&(right_oprand_width<left_operand_width))
        //  temp_right = vc_bvConstExprFromInt(vc,right_oprand_width, atoi(p_exptree_node_branch->right->number_string));
        //else
          temp_right = vc_bvConstExprFromInt(vc,left_operand_width, atoi(p_exptree_node_branch->right->number_string));
        add_to_expr_link(temp_right);
        //vc_printExpr(vc,temp_right); 
      }
     
      //get the left middle right expression
      //now build the entire expression
#ifdef DEBUG_MODE
      //fprintf(cfg_out, "\nenter the cat frame\n");
      if(t_or_f==false)
       fprintf(cfg_out,"\n stp--cat frame %d : %s , the type is %d\n",frame_num,p_exptree_node_branch->number_string,p_exptree_node_branch->tree_node_t);
      else 
       fprintf(cfg_out,"\n stp--cat frame %d : !%s \n",frame_num,p_exptree_node_branch->number_string);
#endif
      //if(p_exptree_node_branch->Is_top_node)
       
      switch(p_exptree_node_branch->tree_node_t)
      {
        case GEQ            :  //a>=b
          temp_top = vc_bvGeExpr(vc, temp_left, temp_right); 
          add_to_expr_link(temp_top);
          temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  //a>=b
          //$  final_exp = temp_top;
          //$}
          //$else 
          //${
            //temp=a>=b
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
            add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case LEQ            :   //a<=b
          temp_top = vc_bvLeExpr(vc,temp_left,temp_right); 
          add_to_expr_link(temp_top);
          temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
            add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break; 
        case LOGAND         :  //a&&b
          temp_top = vc_andExpr(vc,temp_left,temp_right);
          add_to_expr_link(temp_top);
          //printf("vc_query result = %d\n", vc_query(vc, temp_top));
          //exit(-1);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_top = vc_boolToBVExpr(vc,temp_top);
            add_to_expr_link(temp_top);
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
            add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          //printf("vc_query result = %d\n", vc_query(vc, final_exp));
          //exit(-1);
          break;
        case LOGOR         :  //a||b
          temp_top = vc_orExpr(vc,temp_left,temp_right);
          add_to_expr_link(temp_top);
            temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${ 
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case LOGEQ         :  //a==b
          temp_top = vc_eqExpr(vc,temp_left,temp_right);
          add_to_expr_link(temp_top);
          temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);

          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case LOGINEQ         :  //a!=b
          temp_expr1 = vc_eqExpr(vc,temp_left,temp_right);
          add_to_expr_link(temp_expr1);
          temp_top = vc_notExpr(vc,temp_expr1);
          add_to_expr_link(temp_top);
            temp_top = vc_boolToBVExpr(vc,temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case LSFT     :   // a<<b 
          int shift_operand;
          if((p_exptree_node_branch->right->tree_node_t!=CNST)) 
          {
            fprintf(cfg_out,"\nFatal error: the a<<b operation while the b is not constant\n");
            exit(-1);
          }
          else 
            shift_operand = atoi(p_exptree_node_branch->right->number_string);
          temp_top = vc_bvLeftShiftExpr(vc, shift_operand,temp_left);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case RSFT      :  //a>>b
          if((p_exptree_node_branch->right->tree_node_t!=CNST)) 
          {
            fprintf(cfg_out,"\nFatal error: the a<<b operation while the b is not constant\n");
            exit(-1);
          }
          else 
            int shift_operand = atoi(p_exptree_node_branch->right->number_string);
          temp_top = vc_bvRightShiftExpr(vc, shift_operand,temp_left);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break; 
        case ULOGNOT      :  //!a
          if(p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)
          {
            //bit operation
            temp_top = vc_notExpr(vc, temp_left);
          }
          else
          { 
            //bitwise operation  
            temp_top = vc_bvNotExpr(vc, temp_left);
          }
          add_to_expr_link(temp_top);
            temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //$  final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$}  
          break;
        case ULOGNEG       :  //~a
          if(p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)
          {
            //bit operation
            temp_top = vc_notExpr(vc, temp_left);
            add_to_expr_link(temp_top);
            //if(p_exptree_node_branch->left->tree_node_t!=VAR) 
            temp_top = vc_boolToBVExpr(vc,temp_top);
          }
          else
          { 
            //bitwise operation  
            temp_top = vc_bvNotExpr(vc, temp_left);
          }
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //$  final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
            //vc_printExpr(vc,final_exp);
            //vc_query(vc, final_exp);
            //exit(-1); 
 
          //$}  
          break;
        case ULOGAND         :  //&a
          break;
        case ULOGOR          :  //|a
          break; 
        case ULOGXOR         :  //^a
          break; 
        case ULOGNAND        : //~&a
          break;
        case ULOGNOR         : //~|a
          break;
        case ULOGXNOR        : //~^a
          break;
        case BADD            : //a+b
          temp_bit_width = 1+p_exptree_node_branch->width_start-p_exptree_node_branch->width_end;
          temp_top = vc_bvPlusExpr(vc,temp_bit_width,temp_left,temp_right);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case BSUB            : //a-b
          temp_bit_width = 1+p_exptree_node_branch->width_start-p_exptree_node_branch->width_end;
          temp_top = vc_bvMinusExpr(vc, temp_bit_width, temp_left, temp_right);
          add_to_expr_link(temp_top);
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 

          break;
        case BMUL            : //a*b
          temp_bit_width = 1+p_exptree_node_branch->width_start-p_exptree_node_branch->width_end;
          temp_top = vc_bvMultExpr(vc,temp_bit_width,temp_left,temp_right);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case BDIV            : //a/b
          temp_bit_width = 1+p_exptree_node_branch->width_start-p_exptree_node_branch->width_end;
          temp_top = vc_bvDivExpr(vc,temp_bit_width,temp_left,temp_right);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case GAR             : //a>b
          temp_top = vc_bvGtExpr(vc, temp_left, temp_right); 
          add_to_expr_link(temp_top);

          
          //++++++++++++++++++++++++++++
          //++vc_printExpr(vc, temp_top);
          //++vc_query(vc, temp_top);
          //++exit(-1);
          //++++++++++++++++++++++++++++
          temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top);
            //+++vc_printExpr(vc, temp_expr2);
            //+++vc_query(vc, temp_expr2);
            //+++exit(-1);
 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case LES             : //a<b
          temp_top = vc_bvLtExpr(vc, temp_left, temp_right); 
          add_to_expr_link(temp_top);
          temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //${
          //$  final_exp = temp_top;
          //$}
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$} 
          break;
        case SAND            : //a&b
          if(p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)
          {
            //bit operation
            temp_top = vc_andExpr(vc, temp_left, temp_right);
          add_to_expr_link(temp_top);
            temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          }
          else
          { 
            //bitwise operation  
            temp_top = vc_bvAndExpr(vc, temp_left, temp_right);
          add_to_expr_link(temp_top);
          }
          //$if(p_exptree_node_branch->Is_top_node)
          //$  final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$}  
          break;
        case SOR             : //a|b
          if(p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)
          {
            //bit operation
            temp_top = vc_orExpr(vc, temp_left, temp_right); 
          add_to_expr_link(temp_top);
            temp_top = vc_boolToBVExpr(vc,temp_top);
          add_to_expr_link(temp_top);
          }
          else
          { 
            //bitwise operation  
            temp_top = vc_bvOrExpr(vc, temp_left, temp_right);
          add_to_expr_link(temp_top);
          }
          //$if(p_exptree_node_branch->Is_top_node)
          //$  final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$}  

          break;
        case SXOR            : //a^b
         //bitwise operation 
         if(p_exptree_node_branch->left->width_start==p_exptree_node_branch->left->width_end)
         {
           temp_left = vc_boolToBVExpr(vc,temp_left);
           add_to_expr_link(temp_left);
           temp_right = vc_boolToBVExpr(vc,temp_right);
           add_to_expr_link(temp_right);
         }
         temp_top = vc_bvXorExpr(vc, temp_left, temp_right);
         add_to_expr_link(temp_top);
         temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
         temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
         add_to_expr_link(temp_expr2);
         final_exp = temp_expr2; 
          //$}  
         break;
        case LOGXNOR         : //a~^b
          temp_expr1 = vc_bvXorExpr(vc, temp_left, temp_right);
          add_to_expr_link(temp_expr1);
          temp_top = vc_bvNotExpr(vc, temp_expr1);
          add_to_expr_link(temp_top);
          //$if(p_exptree_node_branch->Is_top_node)
          //$   final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$}  
          break;
        case ASS_BLK         : //a=b
          temp_top = vc_eqExpr(vc,temp_left, temp_right);
          add_to_expr_link(temp_top);
          //temp_top = vc_boolToBVExpr(vc,temp_top);
          //temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          //temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          final_exp = temp_top; 
          break;
        case ASS_NBLK        : //a<=b
#ifdef DEBUG_MODE
          fprintf(cfg_out,"\n--in ASS_NBLK1: left: %s; right: %s-\n",p_exptree_node_branch->left->number_string,p_exptree_node_branch->right->number_string);
#endif
          temp_top = vc_eqExpr(vc,temp_left, temp_right);
          add_to_expr_link(temp_top);
          //if(strcmp(p_exptree_node_branch->right->number_string,"RESTART")==0)
          //{
          //printf("vc_query result = %d\n", vc_query(vc, temp_top));
          //exit(-1);
          //}
          //temp_top = vc_boolToBVExpr(vc,temp_top);
          //temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          final_exp = temp_top; 
          //final_exp = temp_top;

#ifdef DEBUG_MODE
          fprintf(cfg_out,"\n--in ASS_NBLK2--\n");
#endif
          break;
        case NAME_PORT       : //.a(b)
          temp_top = vc_eqExpr(vc,temp_left, temp_right);
          add_to_expr_link(temp_top);
          final_exp = temp_top;
          break;
        case POSEGE          : //
          break;
        case NEGEGE          : //
          break;
        case EDGE            : //
          break;
        case ITE_E           : // a? b : c
          temp_expr3 = vc_iteExpr(vc,temp_left, temp_middle,temp_right);
          add_to_expr_link(temp_expr3);     
          temp_expr2 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          temp_top = vc_eqExpr(vc,temp_expr2, temp_expr3);
          add_to_expr_link(temp_top);
          final_exp = temp_top;
          break;
        case PARSEL          :
          temp_expr3 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          if(p_exptree_node_branch->width_start==p_exptree_node_branch->width_end)
            temp_top=vc_bvBoolExtract_One(vc,temp_expr3,p_exptree_node_branch->width_start); 
          else 
            temp_top= vc_bvExtract(vc,temp_expr3, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end);
          add_to_expr_link(temp_top);
          final_exp = temp_top;
          break;
        case INDEX           :
          temp_expr3 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          temp_top=vc_bvBoolExtract_One(vc,temp_expr3,p_exptree_node_branch->width_start); 
          add_to_expr_link(temp_top);
          final_exp = temp_top;
          break;
        case VAR             :
          temp_expr3 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
          if(p_exptree_node_branch->width_start==p_exptree_node_branch->width_end)
            temp_top=vc_bvBoolExtract_One(vc,temp_expr3,p_exptree_node_branch->width_start); 
          else 
            temp_top= vc_bvExtract(vc,temp_expr3, p_exptree_node_branch->width_start, p_exptree_node_branch->width_end);
          add_to_expr_link(temp_top);
          final_exp = temp_top;
          //vc_printExpr(vc,final_exp);
          //printf("vc_query result = %d\n", vc_query(vc, final_exp));
          //exit(-1);
          break;
        case CONCAT          :
          if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t!=CNST)&&(p_exptree_node_branch->left->width_start == p_exptree_node_branch->left->width_end))
          {
            temp_left = vc_boolToBVExpr(vc, temp_left);
            add_to_expr_link(temp_left);
          }
          //else if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->width_start == p_exptree_node_branch->left->width_end))
         
          //fprintf(cfg_out,"\n--Here in Concat--\n"); 
            
          if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t!=CNST)&&(p_exptree_node_branch->right->width_start == p_exptree_node_branch->right->width_end))
          {
            temp_right = vc_boolToBVExpr(vc, temp_right);
            add_to_expr_link(temp_right);
          }
          //else
          //  temp_right = vc_boolToBVExpr(vc, temp_left);

          if(p_exptree_node_branch->right !=NULL)
          {
            temp_top = vc_bvConcatExpr(vc, temp_left, temp_right);
            add_to_expr_link(temp_top);
          }
          else
            temp_top = temp_left;
          //$if(p_exptree_node_branch->Is_top_node)
          //$  final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
          add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$}  
          break;
        case CATCOPY         :
          temp_copy = atoi(p_exptree_node_branch->left->number_string);
          if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->width_start == p_exptree_node_branch->right->width_end))
           {
            temp_right = vc_boolToBVExpr(vc, temp_right);
            add_to_expr_link(temp_right);
           }
          temp_top = temp_right;
          for(temp_idx=1;temp_idx<temp_copy;temp_idx++)
          {
            temp_top = vc_bvConcatExpr(vc,temp_right,temp_top);
            add_to_expr_link(temp_top);
          }
          //$if(p_exptree_node_branch->Is_top_node)
          //$  final_exp = temp_top;
          //$else 
          //${
            temp_expr1 = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
            temp_expr2 = vc_eqExpr(vc,temp_expr1,temp_top); 
            add_to_expr_link(temp_expr2);
            final_exp = temp_expr2; 
          //$}  
          break;
        default              :
          break;
      }
      if((p_exptree_node_branch->tree_node_t!=VAR)&&(p_exptree_node_branch->tree_node_t!=PARSEL)&&(p_exptree_node_branch->tree_node_t!=INDEX)&&(p_exptree_node_branch->tree_node_t!=CNST))
      {
        cnst_exprs[constraint_number++] = vc_notExpr(vc, final_exp);
        add_to_expr_link(cnst_exprs[constraint_number-1]);
 
#ifdef DEBUG_MODE
        vc_printExpr(vc,cnst_exprs[constraint_number-1]); 
#endif
      } 

      if((p_exptree_node_branch->Is_top_node)&&(Is_if_branch))
      {
#ifdef DEBUG_MODE
        printf("\n--a Top node--\n");
#endif
        temp_top = var_expr[frame_num][p_exptree_node_branch->var_table_idx];
        temp_top = vc_bvBoolExtract_One(vc,temp_top, p_exptree_node_branch->width_start);
        add_to_expr_link(temp_top);
        if(t_or_f == false)
        { 
          cnst_exprs[constraint_number++] = vc_notExpr(vc,temp_top);
          add_to_expr_link(cnst_exprs[constraint_number-1]);
        }
        else
          cnst_exprs[constraint_number++] = temp_top;
#ifdef DEBUG_MODE
        vc_printExpr(vc,cnst_exprs[constraint_number-1]); 
#endif
       }

      //printf("\n----start basic constraint-----\n");
      //vc_printExpr(vc,cnst_exprs[constraint_number-1]); 
#ifdef DEBUG_MODE
      printf("----add to the constraint stack-----\n");
#endif
    if((p_exptree_node_branch->left!=NULL)&&(p_exptree_node_branch->left->tree_node_t!=VAR)&&(p_exptree_node_branch->left->tree_node_t!=PARSEL)&&(p_exptree_node_branch->left->tree_node_t!=INDEX)&&(p_exptree_node_branch->left->tree_node_t!=CNST))
      stp_constraint_gen(NULL,p_exptree_node_branch->left,NULL,frame_num,false,false);
    if((p_exptree_node_branch->middle!=NULL)&&(p_exptree_node_branch->middle->tree_node_t!=VAR)&&(p_exptree_node_branch->middle->tree_node_t!=PARSEL)&&(p_exptree_node_branch->middle->tree_node_t!=INDEX)&&(p_exptree_node_branch->middle->tree_node_t!=CNST))
      stp_constraint_gen(NULL,p_exptree_node_branch->middle,NULL,frame_num,false,false);
    if((p_exptree_node_branch->right!=NULL)&&(p_exptree_node_branch->right->tree_node_t!=VAR)&&(p_exptree_node_branch->right->tree_node_t!=PARSEL)&&(p_exptree_node_branch->right->tree_node_t!=INDEX)&&(p_exptree_node_branch->right->tree_node_t!=CNST))
      stp_constraint_gen(NULL,p_exptree_node_branch->right,NULL,frame_num,false,false);
  }
  
}

 
