#ifndef _VLG_HEADER_H_
#define _VLG_HEADER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "vlgParser.h"
#include "vlg2sExpr.h"

#define MAXSTRLEN    8192
#define MAXBITNUM    32
#define MAXNUMVARS   8192

extern int  yylineno;
extern char *yyfile;
extern char *yytext;

extern FILE *yyin;

extern int yylex ();
extern void yyerror (char *str);
extern int yyparse (void);

struct __errStruct {
  int errorNumber;
  char errString[1024];
};

struct __errStruct make_error (int, char *);
int printError (struct __errStruct);

#endif //_VLG_HEADER_H_
